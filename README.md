<h3>Consultas Sistema de Trazabilidad Documental (CSTD)</h3><br>
 - Generador de Reportes en Excel y PDF a través de cálculos.<br>
 - Gestor Documental.<br>
 - Firma Avanzada de Documentos por medio de Api.<br>
 - Reportador de Indicadores.<br>
 - Consulta de Datos a través de Api´s.<br>
 - Consulta de Datos a través de Bases de Datos.<br>
 - Generador de enlaces.<br>
 - Envío automático de notificaciones.<br>
 - Envío de correos.<br>
<br>

<h3>Requisitos Del Sistema</h3><br>
<li>Laravel 5.8</li><br>
<li>Mysql</li>
 