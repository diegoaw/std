<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\MercadoPublico\Licitaciones\LicitacionesController;

class ActualizarLicitaciones extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'actualizar:licitaciones';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'verifica si existen licitaciones de compra nuevas y las importa a la base de datos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $act =  new  LicitacionesController();
        $act->actualizarLicitaciones();
    }
}
