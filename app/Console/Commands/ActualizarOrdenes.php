<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\MercadoPublico\Ordenes\OrdenesController;

class ActualizarOrdenes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'actualizar:ordenes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'verifica si existen ordenes de compra nuevas y las importa a la base de datos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $act =  new  OrdenesController();
        $act->actualizarOrdenes();
    }
}
