<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Sigc\ProgramasCalendarioEventosController;


class NotificacionesEventosSigcProgramas extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notificaciones:sigcprogramas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notificaciones Eventos Sigc Programas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $act =  new  ProgramasCalendarioEventosController();
        $act->notificacionEventos();
    }
}
