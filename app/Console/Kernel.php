<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\ActualizarOrdenes',
        'App\Console\Commands\ActualizarLicitaciones',
        'App\Console\Commands\NotificacionesEventosSigc',
        'App\Console\Commands\NotificacionesEventosSigcProgramas'

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
         $schedule->command('actualizar:ordenes')->everyMinute();
         $schedule->command('actualizar:licitaciones')->everyMinute();
         $schedule->command('notificaciones:sigc')->everyMinute();
         $schedule->command('notificaciones:sigcprogramas')->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
