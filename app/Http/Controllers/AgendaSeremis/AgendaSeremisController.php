<?php

namespace App\Http\Controllers\AgendaSeremis;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\AgendaSeremis\AgendaSeremis;
use App\Models\PyR\PyR;
use App\Models\AgendaSeremis\Region;
use App\Models\AgendaSeremis\PyrHasRegion;
use App\Models\AgendaSeremis\Comuna;
use App\User;
use Carbon\Carbon;
use App\Http\Requests\AgendaSeremis\AgendaSeremisRequest;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class AgendaSeremisController extends Controller
{

    public function index(Request $request)
    {
      if($request->get('FechaDesde')){
        //Session Fecha desde 
        session(['feDesde' => $request->get('FechaDesde')]);
        session(['feHasta' => $request->get('FechaHasta')]);
      }else{
        //Limpiar Variables Session
        session()->forget('feDesde');
        session()->forget('feHasta');
      }
      

       //$agendaSeremis = AgendaSeremis::get();
       $anno = Carbon::now()->format('Y');
       $fechaStringInicio = ($anno).'-01-01 00:00:00';
       $feDesde = Carbon::parse($fechaStringInicio);
       $feHasta =  Carbon::parse(Carbon::now()->format('Y-m-d').' 23:59:59');
       //$feHasta = $feHasta->addYear();
       $fechaInDesde = Carbon::parse($fechaStringInicio);
       $fechaInHasta = Carbon::now();
       //$fechaInHasta = $fechaInHasta->addYear();
       $fechas[1] = Date('Y-m-d', strtotime( '1970-01-01' ));
       $fechas[0] = Date('Y-m-d', strtotime('2030-12-31'));

        $ahora = Carbon::now()->format('Y-m-d');
        //$fechas = $fechas->addYear();


        if( $request->FechaDesde != null ) {
            $feDesde=(Carbon::parse($request->get('FechaDesde'))->format('dmY'));
            $feDesde= Carbon::parse(substr($feDesde, 0, 2).'-'.substr($feDesde, 2, 2).'-'.substr($feDesde, 4, 4).' 00:00:00');
        }
        if( $request->FechaHasta != null ) {
            $feHasta=(Carbon::parse($request->get('FechaHasta'))->format('dmY'));
            $feHasta= Carbon::parse(substr($feHasta, 0, 2).'-'.substr($feHasta, 2, 2).'-'.substr($feHasta, 4, 4).' 23:59:59');
        }

        $query = AgendaSeremis::query ();       
        $query->when(request('FechaDesde', false), function ($q, $feDesde) {
            return $q->where('dia','>=', $feDesde);
        });
        $query->when(request('FechaHasta', false), function ($q, $feHasta) {
            return $q->where('dia','<=', $feHasta);
        });
        $agendaSeremis = $query->orderby('created_at', 'desc')->get(); 
        //dd($agendaSeremis);
       
      return view('agendaseremis.agendaseremis')
      ->with('agendaSeremis' , $agendaSeremis)
      ->with('feDesde' ,$feDesde)
      ->with('feHasta' ,$feHasta)
      ->with('fechaInDesde' ,$fechaInDesde)
      ->with('fechaInHasta' ,$fechaInHasta)
      ->with('fechas' ,$fechas);
    }


    public function create()
    {
      $varNull = null;
      $selectSeremis[null] = 'Seleccione Seremi'; foreach (PyR::where('cargo','SECRETARIO (A) REGIONAL MINISTERIAL')->orwhere('unidad_desempeno', 'DEPARTAMENTO DE COORDINACIÓN DE SEREMIAS')->orderby('nombres', 'asc')->get() as $key2 => $value2) { $selectSeremis[$value2->id_wsad]=$value2->name;}
      $selectRegiones[null] = 'Seleccione Región'; foreach (Region::orderby('nombre', 'asc')->get() as $key0 => $value0) { $selectRegiones[$value0->id]=$value0->nombre;}
      $selectComunas[null] = 'Seleccione Comuna'; foreach (Comuna::orderby('nombre', 'asc')->get() as $key1 => $value1) { $selectComunas[$value1->id]=$value1->nombre;} 

      $fechas[1] = Date('Y-m-d', strtotime( '2020-01-01' ));
      $fechas[0] = Date('Y-m-d', strtotime('2030-12-31'));
      $fe_desde =Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
      $fe_hasta =Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));

      return view('agendaseremis.agendaseremis_crear')
      ->with('varNull' , $varNull)
      ->with('selectSeremis' , $selectSeremis)
      ->with('selectRegiones' , $selectRegiones)
      ->with('selectComunas' , $selectComunas) 
      ->with('fechas' , $fechas)
      ->with('fe_hasta' , $fe_hasta)
      ->with('fe_desde' , $fe_desde);
    }


     public function store(AgendaSeremisRequest $request)
    {      
      $seremi = $request->id_wsad;
      $region = $request->id_region;
      $comuna = $request->id_comuna;
      $dia = $request->dia;
      $hora = $request->hora;
      $lugar = $request->lugar;
      $nombreActividad = $request->nombre_actividad;
      $organizador = $request->organizador;
      $asistentes = $request->asistentes;
      $viatico = $request->viatico;
      $observaciones = $request->observaciones;

      $nuevaAgendaSeremis = new AgendaSeremis();
      $nuevaAgendaSeremis->id_wsad = $seremi;
      $nuevaAgendaSeremis->id_region = $region;
      $nuevaAgendaSeremis->id_comuna = $comuna;
      $nuevaAgendaSeremis->dia = Date('Ymd', strtotime( $request->dia));
      $nuevaAgendaSeremis->hora = $hora;
      $nuevaAgendaSeremis->lugar = $lugar;
      $nuevaAgendaSeremis->nombre_actividad = $nombreActividad;
      $nuevaAgendaSeremis->organizador = $organizador;
      $nuevaAgendaSeremis->asistentes = $asistentes;
      $nuevaAgendaSeremis->viatico = $viatico;
      $nuevaAgendaSeremis->observaciones = $observaciones;
      $nuevaAgendaSeremis->save();
      alert()->success('!','Cita creada en Agenda Seremis exitosamente');
      return redirect()->route('agendaseremis');
    }

    public function edit($id)
    {
      $editAgendaSeremis = AgendaSeremis::where('id' , $id)->first();
      $varNull = null;
      $selectSeremis[null] = 'Seleccione Seremi'; foreach (PyR::where('cargo','SECRETARIO (A) REGIONAL MINISTERIAL')->orwhere('unidad_desempeno', 'DEPARTAMENTO DE COORDINACIÓN DE SEREMIAS')->orderby('nombres', 'asc')->get() as $key2 => $value2) { $selectSeremis[$value2->id_wsad]=$value2->name;}
      $selectRegiones[null] = 'Seleccione Región'; foreach (Region::orderby('nombre', 'asc')->get() as $key0 => $value0) { $selectRegiones[$value0->id]=$value0->nombre;}
      $selectComunas[null] = 'Seleccione Comuna'; foreach (Comuna::orderby('nombre', 'asc')->get() as $key1 => $value1) { $selectComunas[$value1->id]=$value1->nombre;}
      $fechas[1] = Date('Y-m-d', strtotime( '2020-01-01' ));
      $fechas[0] = Date('Y-m-d', strtotime('2030-12-31'));
      $fe_desde =Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
      $fe_hasta =Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
      
      return view('agendaseremis.agendaseremis_editar')
      ->with('editAgendaSeremis' , $editAgendaSeremis)
      ->with('varNull' , $varNull)
      ->with('selectSeremis' , $selectSeremis)
      ->with('selectRegiones' , $selectRegiones)
      ->with('selectComunas' , $selectComunas)
      ->with('fechas' , $fechas)
      ->with('fe_hasta' , $fe_hasta)
      ->with('fe_desde' , $fe_desde);
    }


     public function update(AgendaSeremisRequest $request, $id)
    {
      $seremi = $request->id_wsad;
      $region = $request->id_region;
      $comuna = $request->id_comuna;
      $dia = $request->dia;
      $hora = $request->hora;
      $lugar = $request->lugar;
      $nombreActividad = $request->nombre_actividad;
      $organizador = $request->organizador;
      $asistentes = $request->asistentes;
      $viatico = $request->viatico;
      $observaciones = $request->observaciones;

      $editAgendaSeremis = AgendaSeremis::where('id' , $id)->first();
      $editAgendaSeremis->id_wsad = $seremi;
      $editAgendaSeremis->id_region = $region;
      $editAgendaSeremis->id_comuna = $comuna;
      $editAgendaSeremis->dia = Date('Ymd', strtotime( $request->dia));
      $editAgendaSeremis->hora = $hora;
      $editAgendaSeremis->lugar = $lugar;
      $editAgendaSeremis->nombre_actividad = $nombreActividad;
      $editAgendaSeremis->organizador = $organizador;
      $editAgendaSeremis->asistentes = $asistentes;
      $editAgendaSeremis->viatico = $viatico;
      $editAgendaSeremis->observaciones = $observaciones;
      $editAgendaSeremis->save();
      alert()->success('!','Cita editada de Agenda Seremis  exitosamente');
      return redirect()->route('agendaseremis');
    }

    public function destroy($id){
      
      $parlamentario = AgendaSeremis::where('id', $id)->first();
      
      $result = $parlamentario->delete();
          if ($result) {
            alert()->success('!','Cita eliminada de Agenda Seremis exitosamente');
            return redirect()->route('agendaseremis');
          }else{
              return response()->json(['success'=> 'false']);
          }
    }

    public function reporte(Request $request) {

      $tipoReporte = $request->imprimir;

     //Compruebo si existe Fecha Desde, si no, imprimir todos
     if(session()->has('feDesde')){
      //Prueba imprimir var session
      $feDesde = session('feDesde');
      $feHasta = session('feHasta');
      //return $feDesde." hasta ".$feHasta;
      $dataFinal = AgendaSeremis::whereBetween('dia', array($feDesde, $feHasta))->get();
    }else{
      $dataFinal = AgendaSeremis::get();
    }

    //$dataFinal = AgendaSeremis::get();

      switch ($tipoReporte) {
      case 'PDF':

      break;
      case 'XLS':
                  Excel::create("Agenda Seremis", function ($excel) use ($dataFinal) {
                  $excel->setTitle("Agenda Seremis");
                  $excel->sheet("Agenda Seremis", function ($sheet) use ($dataFinal) {
                    
                  $sheet->loadView('agendaseremis.agendaseremis_reporte_ex_excel')->with('dataFinal', $dataFinal);
              });
          })->download('xlsx');

              return back();

              break;
      default:
      break;
    }}

    public function TodasComunas(){
      $comunas = Comuna::get();
      $arrayComunas = [];
      foreach ($comunas as $key => $value) {
        $arrayComunas[$value->id] = $value->nombre;
      }
      return $arrayComunas;
    }

    public function getComunasForRegion($region){
        $regiones = PyrHasRegion::where('id_wsad', $region)->first();
        $comunas = Comuna::where('id_region' , $regiones->id_region)->orderby('id', 'asc')->get();
        $arrayComunas = [];
        foreach ($comunas as $key => $value) {
          $arrayComunas[$value->id] = $value->nombre;
        }
        return $arrayComunas;
    }

}
