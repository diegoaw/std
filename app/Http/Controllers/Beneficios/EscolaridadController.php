<?php

namespace App\Http\Controllers\Beneficios;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use GuzzleHttp\Client;
use App\Models\Beneficios\Escolaridad;


class EscolaridadController extends Controller
{

      public function escolaridad(Request $request){


          $tramites = [];
          $idTramiteFiltro = $request->nRegistro;
          $queryFilter = "where r.numero_tramite = $idTramiteFiltro ";
          
               $query = "SELECT  *
                   FROM
                  (select
                          tramite.id as numero_tramite ,
                          tramite.proceso_id as numero_proeceso ,
                          proceso.nombre as nombre_proceso ,
                          tramite.created_at as fecha_inicio ,
                          tramite.updated_at as fecha_ultima_modificacion ,
                          tramite.pendiente as pendiente,
                          tramite.ended_at as fecha_culmino,
                          tarea.nombre as etapa,
                          et.id as id_etapa,
                          dts.conteo as cantidad_campos
                          from tramite
                          INNER JOIN proceso ON tramite.proceso_id = proceso.id
                          INNER JOIN (SELECT * FROM etapa WHERE id IN (SELECT MAX(id) FROM etapa GROUP BY tramite_id) ORDER BY id DESC) et on et.tramite_id = tramite.id
                          INNER JOIN tarea on et.tarea_id = tarea.id
                          LEFT JOIN (SELECT  COUNT(id) as conteo, etapa_id FROM dato_seguimiento  group by etapa_id ) dts on dts.etapa_id = et.id
                          where tramite.proceso_id = 31  and tarea.nombre = 'Ingreso Solicitud Bono Escolaridad' and tramite.deleted_at is null and dts.conteo >3 
                          
          UNION ALL
          select
                          tramite.id as numero_tramite ,
                          tramite.proceso_id as numero_proeceso ,
                          proceso.nombre as nombre_proceso ,
                          tramite.created_at as fecha_inicio ,
                          tramite.updated_at as fecha_ultima_modificacion ,
                          tramite.pendiente as pendiente,
                          tramite.ended_at as fecha_culmino,
                          tarea.nombre as etapa,
                          et.id as id_etapa,
                          dts.conteo as cantidad_campos
                          from tramite
                          INNER JOIN proceso ON tramite.proceso_id = proceso.id
                          INNER JOIN (SELECT * FROM etapa WHERE id IN (SELECT MAX(id) FROM etapa GROUP BY tramite_id) ORDER BY id DESC) et on et.tramite_id = tramite.id
                          INNER JOIN tarea on et.tarea_id = tarea.id
                          LEFT JOIN (SELECT  COUNT(id) as conteo, etapa_id FROM dato_seguimiento  group by etapa_id ) dts on dts.etapa_id = et.id
                          where tramite.proceso_id = 31
                          and tramite.deleted_at is null
                          and tarea.nombre <> 'Ingreso Solicitud Bono Escolaridad') r ";
          
              ($idTramiteFiltro != null )? $query = $query.$queryFilter : $query = $query;
              $query = $query."order by r.numero_tramite DESC";
          
              $tramites = DB::connection('mysqlSimple')->select($query);
              return view('beneficios.escolaridad')
              ->with('tramitesD', $tramites)
              ->with('idTramiteFiltro', $idTramiteFiltro);
          
              }
              

          public function escolaridadShow($id)
          {
                        
              $client = new Client();
                    try {
                    $request = $client->get("https://simple.mindep.cl/backend/api/tramites/$id?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu " , ['verify' => false]);
                    $response = $request->getBody()->getContents();
                    $data =  json_decode($response);
                      }catch (BadResponseException   $e) {
                          $response = $e->getResponse();
                      }
              $data = $data->tramite;
       
              $etapasCont = count($data->etapas);

              $nuevaEscolaridad = new Escolaridad();
              $nuevaEscolaridad->id =  $data->id;
              $nuevaEscolaridad->fecha_inicio =  $data->fecha_inicio;
              $nuevaEscolaridad->fecha_termino =  $data->fecha_termino;
              $nuevaEscolaridad->fecha_modificacion =  $data->fecha_modificacion;
              $nuevaEscolaridad->estado =  $data->estado;
              
              $datos=$data->datos;            
if($datos){
               foreach ($datos as $key => $value2) {               

                     if(property_exists($value2, 'fecha_solicitud')){
                        $nuevaEscolaridad->fecha_solicitud =  $value2->fecha_solicitud;
                     }

                     if(property_exists($value2, 'seleccione')){
                       $nuevaEscolaridad->seleccione =  $value2->seleccione;
                     }

                     if(property_exists($value2, 'cargo')){
                       $nuevaEscolaridad->cargo =  $value2->cargo;
                     }

                     if(property_exists($value2, 'division')){
                       $nuevaEscolaridad->division =  $value2->division;
                     }

                     if(property_exists($value2, 'correo')){
                       $nuevaEscolaridad->correo =  $value2->correo;
                     }

                     if(property_exists($value2, 'rut')){
                       $nuevaEscolaridad->rut =  $value2->rut;
                     }

                     if(property_exists($value2, 'calidad_juridica')){
                       $nuevaEscolaridad->calidad_juridica =  $value2->calidad_juridica;
                     }

                     if(property_exists($value2, 'grado')){
                       $nuevaEscolaridad->grado =  $value2->grado;
                     }

                     if(property_exists($value2, 'fecha_ingreso')){
                       $nuevaEscolaridad->fecha_ingreso =  $value2->fecha_ingreso;
                     }
                    
                     if(property_exists($value2, 'nombre_hijo')){
                        $nuevaEscolaridad->nombre_hijo =  $value2->nombre_hijo;
                     }

                     if(property_exists($value2, 'run_hijo')){
                       $nuevaEscolaridad->run_hijo =  $value2->run_hijo;
                     }  
                     
                     if(property_exists($value2, 'fecha_n_hijo')){
                        $nuevaEscolaridad->fecha_n_hijo =  $value2->fecha_n_hijo;
                      }                       
                    
                     if(property_exists($value2, 'edad_hijo')){
                       $nuevaEscolaridad->edad_hijo =  $value2->edad_hijo;
                     }
                    
                     if(property_exists($value2, 'nivel_enseanza')){
                        $nuevaEscolaridad->nivel_enseanza =  $value2->nivel_enseanza;
                     }

                     if(property_exists($value2, 'certificado_nacimiento')){
                       $nuevaEscolaridad->certificado_nacimiento =  $value2->certificado_nacimiento->URL;
                     }
                     
                     if(property_exists($value2, 'alumno_regular')){
                        $nuevaEscolaridad->alumno_regular =  $value2->alumno_regular->URL;
                     }

                     if(property_exists($value2, 'declaracion_jurada')){
                        $nuevaEscolaridad->declaracion_jurada =  $value2->declaracion_jurada->URL;
                     }

                     if(property_exists($value2, 'vigente_servicio')){
                        $nuevaEscolaridad->vigente_servicio =  $value2->vigente_servicio;
                     }
                    
                     if(property_exists($value2, 'comprobante')){
                        $nuevaEscolaridad->comprobante = 'https://simple.mindep.cl/uploads/documentos/'.$value2->comprobante;
                     }
                    
                     if(property_exists($value2, 'fecha_revision')){
                        $nuevaEscolaridad->fecha_revision =  $value2->fecha_revision;
                     } 

                     if(property_exists($value2, 'cumple_requisitos')){
                        $nuevaEscolaridad->cumple_requisitos =  $value2->cumple_requisitos;
                     }

                     if(property_exists($value2, 'observaciones_correccion')){
                        $nuevaEscolaridad->observaciones_correccion =  $value2->observaciones_correccion;
                     }

                     if(property_exists($value2, 'fecha_correccion')){
                        $nuevaEscolaridad->fecha_correccion =  $value2->fecha_correccion;
                     }                      

                 }
                }

            return view('beneficios.escolaridad_detalle')
            ->with('data' , $nuevaEscolaridad);
          }


          
}