<?php

namespace App\Http\Controllers\Beneficios;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use GuzzleHttp\Client;
use App\Models\Beneficios\Maternidad;


class MaternidadController extends Controller
{

      public function maternidad(Request $request){


          $tramites = [];
          $idTramiteFiltro = $request->nRegistro;
          $queryFilter = "where r.numero_tramite = $idTramiteFiltro ";
          
               $query = "SELECT  *
                   FROM
                  (select
                          tramite.id as numero_tramite ,
                          tramite.proceso_id as numero_proeceso ,
                          proceso.nombre as nombre_proceso ,
                          tramite.created_at as fecha_inicio ,
                          tramite.updated_at as fecha_ultima_modificacion ,
                          tramite.pendiente as pendiente,
                          tramite.ended_at as fecha_culmino,
                          tarea.nombre as etapa,
                          et.id as id_etapa,
                          dts.conteo as cantidad_campos
                          from tramite
                          INNER JOIN proceso ON tramite.proceso_id = proceso.id
                          INNER JOIN (SELECT * FROM etapa WHERE id IN (SELECT MAX(id) FROM etapa GROUP BY tramite_id) ORDER BY id DESC) et on et.tramite_id = tramite.id
                          INNER JOIN tarea on et.tarea_id = tarea.id
                          LEFT JOIN (SELECT  COUNT(id) as conteo, etapa_id FROM dato_seguimiento  group by etapa_id ) dts on dts.etapa_id = et.id
                          where tramite.proceso_id = 22  and tarea.nombre = 'Ingreso | Beneficios Maternidad' and tramite.deleted_at is null and dts.conteo >2 
                          
          UNION ALL
          select
                          tramite.id as numero_tramite ,
                          tramite.proceso_id as numero_proeceso ,
                          proceso.nombre as nombre_proceso ,
                          tramite.created_at as fecha_inicio ,
                          tramite.updated_at as fecha_ultima_modificacion ,
                          tramite.pendiente as pendiente,
                          tramite.ended_at as fecha_culmino,
                          tarea.nombre as etapa,
                          et.id as id_etapa,
                          dts.conteo as cantidad_campos
                          from tramite
                          INNER JOIN proceso ON tramite.proceso_id = proceso.id
                          INNER JOIN (SELECT * FROM etapa WHERE id IN (SELECT MAX(id) FROM etapa GROUP BY tramite_id) ORDER BY id DESC) et on et.tramite_id = tramite.id
                          INNER JOIN tarea on et.tarea_id = tarea.id
                          LEFT JOIN (SELECT  COUNT(id) as conteo, etapa_id FROM dato_seguimiento  group by etapa_id ) dts on dts.etapa_id = et.id
                          where tramite.proceso_id = 22
                          and tramite.deleted_at is null
                          and tarea.nombre <> 'Ingreso | Beneficios Maternidad') r ";
          
              ($idTramiteFiltro != null )? $query = $query.$queryFilter : $query = $query;
              $query = $query."order by r.numero_tramite DESC";
          
              $tramites = DB::connection('mysqlSimple')->select($query);
              return view('beneficios.maternidad')
              ->with('tramitesD', $tramites)
              ->with('idTramiteFiltro', $idTramiteFiltro);
          
              }
              

          public function maternidadShow($id)
          {
                        
              $client = new Client();
                    try {
                    $request = $client->get("https://simple.mindep.cl/backend/api/tramites/$id?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu " , ['verify' => false]);
                    $response = $request->getBody()->getContents();
                    $data =  json_decode($response);
                      }catch (BadResponseException   $e) {
                          $response = $e->getResponse();
                      }
              $data = $data->tramite;
       
              $etapasCont = count($data->etapas);

              $nuevaMaternidad = new Maternidad();
              $nuevaMaternidad->id =  $data->id;
              $nuevaMaternidad->fecha_inicio =  $data->fecha_inicio;
              $nuevaMaternidad->fecha_termino =  $data->fecha_termino;
              $nuevaMaternidad->fecha_modificacion =  $data->fecha_modificacion;
              $nuevaMaternidad->estado =  $data->estado;
              
              $datos=$data->datos;            
if($datos){
               foreach ($datos as $key => $value2) {               

                     if(property_exists($value2, 'nombre_solicitante')){
                        $nuevaMaternidad->nombre_solicitante =  $value2->nombre_solicitante;
                     }

                     if(property_exists($value2, 'cargo_solicitante')){
                       $nuevaMaternidad->cargo_solicitante =  $value2->cargo_solicitante;
                     }

                     if(property_exists($value2, 'correo_solicitante')){
                       $nuevaMaternidad->correo_solicitante =  $value2->correo_solicitante;
                     }

                     if(property_exists($value2, 'dependencia_solicitante')){
                       $nuevaMaternidad->dependencia_solicitante =  $value2->dependencia_solicitante;
                     }

                     if(property_exists($value2, 'rut_solicitante')){
                       $nuevaMaternidad->rut_solicitante =  $value2->rut_solicitante;
                     }

                     if(property_exists($value2, 'calidad_juridica')){
                       $nuevaMaternidad->calidad_juridica =  $value2->calidad_juridica;
                     }

                     if(property_exists($value2, 'grado_solicitante')){
                       $nuevaMaternidad->grado_solicitante =  $value2->grado_solicitante;
                     }

                     if(property_exists($value2, 'nombre_menor')){
                       $nuevaMaternidad->nombre_menor =  $value2->nombre_menor;
                     }

                     if(property_exists($value2, 'rut_menor')){
                       $nuevaMaternidad->rut_menor =  $value2->rut_menor;
                     }
                    
                     if(property_exists($value2, 'fecha_n_menor')){
                        $nuevaMaternidad->fecha_n_menor =  $value2->fecha_n_menor;
                     }

                     if(property_exists($value2, 'certificado_nacimiento')){
                       $nuevaMaternidad->certificado_nacimiento =  $value2->certificado_nacimiento->URL;
                     }  
                     
                     if(property_exists($value2, 'declaracion_jurada')){
                        $nuevaMaternidad->declaracion_jurada =  $value2->declaracion_jurada->URL;
                      }                       
                    
                     if(property_exists($value2, 'que_beneficio_optara')){
                       $nuevaMaternidad->que_beneficio_optara =  $value2->que_beneficio_optara;
                     }
                    
                     if(property_exists($value2, 'goza_beneficio')){
                        $nuevaMaternidad->goza_beneficio =  $value2->goza_beneficio;
                     }

                     if(property_exists($value2, 'cual')){
                       $nuevaMaternidad->cual =  $value2->cual;
                     }
                     
                     if(property_exists($value2, 'modalidad')){
                        $nuevaMaternidad->modalidad =  $value2->modalidad;
                     }

                     if(property_exists($value2, 'modalidad_j')){
                        $nuevaMaternidad->modalidad_j =  $value2->modalidad_j;
                     }

                     if(property_exists($value2, 'nombre_sala')){
                        $nuevaMaternidad->nombre_sala =  $value2->nombre_sala;
                     }
                    
                     if(property_exists($value2, 'nombre_jardin')){
                        $nuevaMaternidad->nombre_jardin =  $value2->nombre_jardin;
                     }

                     if(property_exists($value2, 'rol_junji')){
                        $nuevaMaternidad->rol_junji =  $value2->rol_junji;
                     }

                     if(property_exists($value2, 'rol_junji_j')){
                        $nuevaMaternidad->rol_junji_j =  $value2->rol_junji_j;
                     }

                     if(property_exists($value2, 'nombre_del_directora')){
                        $nuevaMaternidad->nombre_del_directora =  $value2->nombre_del_directora;
                     }

                     if(property_exists($value2, 'rut_directora')){
                        $nuevaMaternidad->rut_directora =  $value2->rut_directora;
                     }

                     if(property_exists($value2, 'rut_directora_j')){
                        $nuevaMaternidad->rut_directora_j =  $value2->rut_directora_j;
                     }

                     if(property_exists($value2, 'nombre_rl')){
                        $nuevaMaternidad->nombre_rl =  $value2->nombre_rl;
                     }

                     if(property_exists($value2, 'nombre_rl_j')){
                        $nuevaMaternidad->nombre_rl_j =  $value2->nombre_rl_j;
                     } 

                     if(property_exists($value2, 'rut_rl')){
                        $nuevaMaternidad->rut_rl =  $value2->rut_rl;
                     } 
                    
                     if(property_exists($value2, 'rut_rl_j')){
                        $nuevaMaternidad->rut_rl_j =  $value2->rut_rl_j;
                     } 

                     if(property_exists($value2, 'direccion_establecimiento')){
                        $nuevaMaternidad->direccion_establecimiento =  $value2->direccion_establecimiento;
                     } 

                     if(property_exists($value2, 'direccion_establecimiento_j')){
                        $nuevaMaternidad->direccion_establecimiento_j =  $value2->direccion_establecimiento_j;
                     } 
                    
                     if(property_exists($value2, 'region_establecimiento')){
                        $nuevaMaternidad->region_establecimiento =  $value2->region_establecimiento->region;
                     } 

                     if(property_exists($value2, 'region_establecimiento_j')){
                        $nuevaMaternidad->region_establecimiento_j =  $value2->region_establecimiento_j->region;
                     } 

                     if(property_exists($value2, 'region_establecimiento')){
                        $nuevaMaternidad->comuna_establecimiento =  $value2->region_establecimiento->comuna;
                     } 

                     if(property_exists($value2, 'region_establecimiento_j')){
                        $nuevaMaternidad->comuna_establecimiento_j =  $value2->region_establecimiento_j->comuna;
                     } 

                     if(property_exists($value2, 'telefono_establecimiento')){
                        $nuevaMaternidad->telefono_establecimiento =  $value2->telefono_establecimiento;
                     } 

                     if(property_exists($value2, 'telefono_establecimiento_j')){
                        $nuevaMaternidad->telefono_establecimiento_j =  $value2->telefono_establecimiento_j;
                     } 

                     if(property_exists($value2, 'correo_establecimiento')){
                        $nuevaMaternidad->correo_establecimiento =  $value2->correo_establecimiento;
                     } 

                     if(property_exists($value2, 'correo_establecimiento_j')){
                        $nuevaMaternidad->correo_establecimiento_j =  $value2->correo_establecimiento_j;
                     } 

                     if(property_exists($value2, 'certificado_junji')){
                        $nuevaMaternidad->certificado_junji =  $value2->certificado_junji->URL;
                     } 

                     if(property_exists($value2, 'certificado_junji_j')){
                        $nuevaMaternidad->certificado_junji_j =  $value2->certificado_junji_j->URL;
                     } 

                     if(property_exists($value2, 'costos_asociados')){
                        $nuevaMaternidad->costos_asociados =  $value2->costos_asociados->URL;
                     } 

                     if(property_exists($value2, 'costos_asociados_j')){
                        $nuevaMaternidad->costos_asociados_j =  $value2->costos_asociados_j->URL;
                     }

                     if(property_exists($value2, 'antecedentes_legales')){
                        $nuevaMaternidad->antecedentes_legales =  $value2->antecedentes_legales->URL;
                     } 

                     if(property_exists($value2, 'antecedentes_legales_j')){
                        $nuevaMaternidad->antecedentes_legales_j =  $value2->antecedentes_legales_j->URL;
                     } 

                     if(property_exists($value2, 'lactancia')){
                        $nuevaMaternidad->lactancia =  $value2->lactancia;
                     } 

                     if(property_exists($value2, 'entre1')){
                        $nuevaMaternidad->entre1 =  $value2->entre1;
                     } 

                     if(property_exists($value2, 'y1')){
                        $nuevaMaternidad->y1 =  $value2->y1;
                     } 

                     if(property_exists($value2, 'entre2')){
                        $nuevaMaternidad->entre2 =  $value2->entre2;
                     } 

                     if(property_exists($value2, 'y2')){
                        $nuevaMaternidad->y2 =  $value2->y2;
                     } 

                     if(property_exists($value2, 'entrada')){
                        $nuevaMaternidad->entrada =  $value2->entrada;
                     } 

                     if(property_exists($value2, 'salida')){
                        $nuevaMaternidad->salida =  $value2->salida;
                     }

                     if(property_exists($value2, 'desde')){
                        $nuevaMaternidad->desde =  $value2->desde;
                     } 

                     if(property_exists($value2, 'hasta')){
                        $nuevaMaternidad->hasta =  $value2->hasta;
                     }

                     if(property_exists($value2, 'pago_excepcional')){
                        $nuevaMaternidad->pago_excepcional =  $value2->pago_excepcional;
                     }

                     if(property_exists($value2, 'condicion_medica')){
                        $nuevaMaternidad->condicion_medica =  $value2->condicion_medica->URL;
                     } 

                     if(property_exists($value2, 'resolucion_teletrabajo')){
                        $nuevaMaternidad->resolucion_teletrabajo =  $value2->resolucion_teletrabajo->URL;
                     }

                     if(property_exists($value2, 'declaracion_jurada_simple')){
                        $nuevaMaternidad->declaracion_jurada_simple =  $value2->declaracion_jurada_simple->URL;
                     }

                     if(property_exists($value2, 'comprobante_sala')){
                        $nuevaMaternidad->comprobante_sala = 'https://simple.mindep.cl/uploads/documentos/'.$value2->comprobante_sala;
                     }

                     if(property_exists($value2, 'comprobante_jardin')){
                        $nuevaMaternidad->comprobante_jardin = 'https://simple.mindep.cl/uploads/documentos/'.$value2->comprobante_jardin;
                     }

                     if(property_exists($value2, 'comprobante_excepcional')){
                        $nuevaMaternidad->comprobante_excepcional = 'https://simple.mindep.cl/uploads/documentos/'.$value2->comprobante_excepcional;
                     }

                     if(property_exists($value2, 'comprobante_sala_m1')){
                        $nuevaMaternidad->comprobante_sala_m1 = 'https://simple.mindep.cl/uploads/documentos/'.$value2->comprobante_sala_m1;
                     }

                     if(property_exists($value2, 'comprobante_jardin_m1')){
                        $nuevaMaternidad->comprobante_jardin_m1 = 'https://simple.mindep.cl/uploads/documentos/'.$value2->comprobante_jardin_m1;
                     }

                     if(property_exists($value2, 'comprobante_lactancia')){
                        $nuevaMaternidad->comprobante_lactancia = 'https://simple.mindep.cl/uploads/documentos/'.$value2->comprobante_lactancia;
                     }
                    
                     if(property_exists($value2, 'cumple')){
                        $nuevaMaternidad->cumple =  $value2->cumple;
                     } 

                     if(property_exists($value2, 'observaciones')){
                        $nuevaMaternidad->observaciones =  $value2->observaciones;
                     }

                     if(property_exists($value2, 'observacion_adicional')){
                        $nuevaMaternidad->observacion_adicional =  $value2->observacion_adicional;
                     }

                     if(property_exists($value2, 'observacion_rechazo')){
                        $nuevaMaternidad->observacion_rechazo =  $value2->observacion_rechazo;
                     }  
                     
                     if(property_exists($value2, 'adjuntar_resolucion')){
                        $nuevaMaternidad->adjuntar_resolucion =  $value2->adjuntar_resolucion->URL;
                     } 

                     if(property_exists($value2, 'declaracion_cuidado_menor_s')){
                        $nuevaMaternidad->declaracion_cuidado_menor =  $value2->declaracion_cuidado_menor->URL;
                     } 

                     if(property_exists($value2, 'declaracion_cuidado_menor')){
                        $nuevaMaternidad->declaracion_cuidado_menor =  $value2->declaracion_cuidado_menor->URL;
                     } 

                     if(property_exists($value2, 'fecha_inicio_beneficio')){
                        $nuevaMaternidad->fecha_inicio_beneficio =  $value2->fecha_inicio_beneficio;
                     } 

                     if(property_exists($value2, 'fecha_tramite')){
                        $nuevaMaternidad->fecha_tramite =  $value2->fecha_tramite;
                     } 
                     
                     if(property_exists($value2, 'adjunta_autorizacion_jef')){
                        $nuevaMaternidad->adjunta_autorizacion_jef =  $value2->adjunta_autorizacion_jef->URL;
                     }

                     if(property_exists($value2, 'asistencia_menor')){
                        $nuevaMaternidad->asistencia_menor =  $value2->asistencia_menor->URL;
                     }

                     if(property_exists($value2, 'asistencia_menor_j')){
                        $nuevaMaternidad->asistencia_menor_j =  $value2->asistencia_menor_j->URL;
                     }

                 }
                }

            return view('beneficios.maternidad_detalle')
            ->with('data' , $nuevaMaternidad);
          }


          
}