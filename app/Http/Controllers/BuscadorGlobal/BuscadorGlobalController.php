<?php

namespace App\Http\Controllers\BuscadorGlobal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Carbon\Carbon;



class BuscadorGlobalController extends Controller
{
   public function index(Request $request){
    
    $NRegistrovIew = '';
    $materiaView = '';
    /* fechas */ 
    $anno = Carbon::now()->format('Y');
    $fechaStringInicio = ($anno).'-01-01 00:00:00';
    $feDesde = Carbon::parse($fechaStringInicio);
    $feHasta =  Carbon::parse(Carbon::now()->format('Y-m-d').' 23:59:59');
    $fechaInDesde = Carbon::parse($fechaStringInicio);
    $fechaInHasta = Carbon::now();
    $fechas[1] = Date('Y-m-d', strtotime( '1970-01-01' ));
    $fechas[0] = Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));



    if( $request->FechaDesde != null ) {
        $feDesde=(Carbon::parse($request->get('FechaDesde'))->format('dmY'));
        $feDesde= Carbon::parse(substr($feDesde, 0, 2).'-'.substr($feDesde, 2, 2).'-'.substr($feDesde, 4, 4).' 00:00:00');
      }
      if( $request->FechaHasta != null ) {
         $feHasta=(Carbon::parse($request->get('FechaHasta'))->format('dmY'));
         $feHasta= Carbon::parse(substr($feHasta, 0, 2).'-'.substr($feHasta, 2, 2).'-'.substr($feHasta, 4, 4).' 23:59:59');
      }


    /* fechas */ 

    
    return view('buscador_global.buscador_global')
    ->with('feDesde' ,$feDesde)
    ->with('feHasta' ,$feHasta)
    ->with('fechaInDesde' ,$fechaInDesde)
    ->with('fechaInHasta' ,$fechaInHasta)
    ->with('fechas' ,$fechas)
    ->with('materiaView' , $materiaView)
    ->with('NRegistrovIew' , $NRegistrovIew);
    

    }

    public function buscar (Request $request){

    $numeroTramite = $request->nRegistro;
    $materiaBuscar = $request->materia;
    $fechaDesdeRecibe = $request->FechaDesde;
    $fechaHastaRecibe = $request->FechaHasta;
    /* buscar en : */
    $derivaciones =  $request->derivacion == 1? 1 : 0;
    $memos =  $request->memo == 1 ? 1 : 0;
    $oficios =  $request->oficio == 1 ? 1 : 0;
    $resoluciones =  $request->resolucion == 1 ? 1 : 0; 
    $compras =  $request->compra == 1 ? 1 : 0;
    $reconocimientos =  $request->reconocimiento == 1 ? 1 : 0;
    $patrocinios =  $request->patrocinio == 1 ? 1 : 0 ; 
    $reclamos =  $request->reclamo == 1 ? 1 : 0 ;
    $protocolos =  $request->procotolo == 1 ? 1 : 0 ; 
    $Historicos =  $request->historico == 1 ? 1 : 0 ;
    /* fin buscar en */


    dd($derivaciones);
     

        dd('materia ');
    } 


}


                     