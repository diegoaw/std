<?php

namespace App\Http\Controllers\cevs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use App\Models\Cevs\Cevs;
use App\Models\TramitesSimpleExterno\DatoSeguimiento;

class CevsController extends Controller
{
   public function index(Request $request){

     $tramites = [];
     $idTramiteFiltro = $request->nRegistro;
     $queryFilter = "and tramite.id = $idTramiteFiltro ";

        $query = "select

             tramite.id as numero_tramite ,
             tramite.proceso_id as numero_proeceso ,
             proceso.nombre as nombre_proceso ,
             tramite.created_at as fecha_inicio ,
             tramite.updated_at as fecha_ultima_modificacion ,
             tramite.deleted_at as fecha_eliminacion ,
             tramite.pendiente as pendiente,
             tramite.ended_at as fecha_culmino,
             tarea.nombre as etapa,
             et.id as id_etapa,
             dts.conteo as cantidad_campos
             from tramite
             INNER JOIN proceso ON tramite.proceso_id = proceso.id
             INNER JOIN (SELECT * FROM etapa WHERE id IN (SELECT MAX(id) FROM etapa GROUP BY tramite_id) ORDER BY id DESC) et on et.tramite_id = tramite.id
             INNER JOIN tarea on et.tarea_id = tarea.id
             LEFT JOIN (SELECT  COUNT(id) as conteo, etapa_id FROM dato_seguimiento  group by etapa_id ) dts on dts.etapa_id = et.id
             where tramite.proceso_id = 9
             and tramite.deleted_at is null
             and dts.conteo >6 ";

             ($idTramiteFiltro != null )? $query = $query.$queryFilter : $query = $query;
             $query = $query."order by tramite.id DESC";

             $tramites = DB::connection('mysqlSimpleExterno')->select($query);
//dd($tramites);

      
            foreach ($tramites as $ky => $val) {
              $val->detalles = DatoSeguimiento::where('etapa_id',  $val->id_etapa)->get(); 


              //dd($val->detalles);      
              }      
            




    return view('cevs.cevs')
    ->with('tramitesD', $tramites)
    ->with('idTramiteFiltro', $idTramiteFiltro);

    }

    public function show($id)
    {
        $client = new Client();
               try {
              $request = $client->get("https://tramites.mindep.cl/backend/api/tramites/$id?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu" , ['verify' => false]);
              $response = $request->getBody()->getContents();
              $data =  json_decode($response);
                }catch (BadResponseException   $e) {
                    $response = $e->getResponse();
                }
         $data = $data->tramite;
         $nuevoCevs = new Cevs();
         $nuevoCevs->id =  $data->id;
         $nuevoCevs->fecha_inicio =  $data->fecha_inicio;
         $nuevoCevs->fecha_termino =  $data->fecha_termino;
         $nuevoCevs->fecha_modificacion =  $data->fecha_modificacion;
         $nuevoCevs->estado =  $data->estado;

         $datos=$data->datos;
//dd($datos);
         foreach ($datos as $key => $value2) {
          // if($key == 1){
            //dd($value2);
           
           //}
              if(property_exists($value2, 'fecha')){
                $nuevoCevs->fecha =  $value2->fecha;
              }
              if(property_exists($value2, 'correo_electronico')){
                $nuevoCevs->correo_electronico =  $value2->correo_electronico;
              }
              if(property_exists($value2, 'encargado_proyecto_na')){
                $nuevoCevs->encargado_proyecto_na =  $value2->encargado_proyecto_na;
              }
              if(property_exists($value2, 'region_comuna')){
                $nuevoCevs->region =  $value2->region_comuna->region;
              }
              if(property_exists($value2, 'region_comuna')){
                $nuevoCevs->comuna =  $value2->region_comuna->comuna;
              }
              if(property_exists($value2, 'prototipo_cevs')){
                $nuevoCevs->prototipo_cevs =  $value2->prototipo_cevs;
              }
              if(property_exists($value2, 'direccion_cevs')){
                $nuevoCevs->direccion_cevs =  $value2->direccion_cevs;
              }
              //AREA
              if(property_exists($value2, 'select')){
                $nuevoCevs->select =  $value2->select;
              }
             
              //JURÍDICA
              if(property_exists($value2, 'certificado_dominio_vigente_tpm')){
                $nuevoCevs->certificado_dominio_vigente_tpm =  $value2->certificado_dominio_vigente_tpm;
              }
              if(property_exists($value2, 'certificado_hipotecas_tpm')){
                $nuevoCevs->certificado_hipotecas_tpm =  $value2->certificado_hipotecas_tpm;
              }
              if(property_exists($value2, 'autorizacion_consejo_municipal')){
                $nuevoCevs->autorizacion_consejo_municipal =  $value2->autorizacion_consejo_municipal;
              }
              if(property_exists($value2, 'otros_tpm')){
                $nuevoCevs->otros_tpm =  $value2->otros_tpm;
              }
              if(property_exists($value2, 'certificado_dominio_vigente_tpt')){
                $nuevoCevs->certificado_dominio_vigente_tpt =  $value2->certificado_dominio_vigente_tpt;
              }
              if(property_exists($value2, 'certificado_hipotecas_tpt')){
                $nuevoCevs->certificado_hipotecas_tpt =  $value2->certificado_hipotecas_tpt;
              }
              if(property_exists($value2, 'otros_tpt')){
                $nuevoCevs->otros_tpt =  $value2->otros_tpt;
              }
              if(property_exists($value2, 'copia_comodato_tpt')){
                $nuevoCevs->copia_comodato_tpt =  $value2->copia_comodato_tpt;
              }
              if(property_exists($value2, 'observaciones_area_juridica')){
                $nuevoCevs->observaciones_area_juridica =  $value2->observaciones_area_juridica;
              }
              if(property_exists($value2, 'documento_area_juridica')){
                $nuevoCevs->documento_area_juridica =  $value2->documento_area_juridica->URL;
              }
              //DISEÑO
              if(property_exists($value2, 'certificado_informes_dom')){
                $nuevoCevs->certificado_informes_dom =  $value2->certificado_informes_dom;
              }
              if(property_exists($value2, 'certificado_afectacion')){
                $nuevoCevs->certificado_afectacion =  $value2->certificado_afectacion;
              }
              if(property_exists($value2, 'copia_permisosrecepciones')){
                $nuevoCevs->copia_permisosrecepciones =  $value2->copia_permisosrecepciones;
              }
              if(property_exists($value2, 'cabida_preliminar')){
                $nuevoCevs->cabida_preliminar =  $value2->cabida_preliminar;
              }
              if(property_exists($value2, 'terreno_zonas_riesgo')){
                $nuevoCevs->terreno_zonas_riesgo =  $value2->terreno_zonas_riesgo;
              }
              if(property_exists($value2, 'situacion_plan_regulador')){
                $nuevoCevs->situacion_plan_regulador =  $value2->situacion_plan_regulador;
              }
              if(property_exists($value2, 'anexo_e')){
                $nuevoCevs->anexo_e =  $value2->anexo_e;
              }
              if(property_exists($value2, 'otros_archivos_diseno')){
                $nuevoCevs->otros_archivos_diseno =  $value2->otros_archivos_diseno;
              }
              if(property_exists($value2, 'documento_area_diseno')){
                $nuevoCevs->documento_area_diseno =  $value2->documento_area_diseno->URL;
              }
              if(property_exists($value2, 'observaciones_area_diseno')){
                $nuevoCevs->observaciones_area_diseno =  $value2->observaciones_area_diseno;
              }
              //ESPECIALIDADES
              if(property_exists($value2, 'informe_mecanica_suelos')){
                $nuevoCevs->informe_mecanica_suelos =  $value2->informe_mecanica_suelos;
              }
              if(property_exists($value2, 'preinforme_mecanica_suelos')){
                $nuevoCevs->preinforme_mecanica_suelos =  $value2->preinforme_mecanica_suelos;
              }
              if(property_exists($value2, 'levantamiento_topografico')){
                $nuevoCevs->levantamiento_topografico =  $value2->levantamiento_topografico;
              }
              if(property_exists($value2, 'factibilidad_electrica')){
                $nuevoCevs->factibilidad_electrica =  $value2->factibilidad_electrica;
              }
              if(property_exists($value2, 'factibilidad_agua_alcanta')){
                $nuevoCevs->factibilidad_agua_alcanta =  $value2->factibilidad_agua_alcanta;
              }
              if(property_exists($value2, 'factibilidad_gas')){
                $nuevoCevs->factibilidad_gas =  $value2->factibilidad_gas;
              }
              if(property_exists($value2, 'factibilidad_wifi')){
                $nuevoCevs->factibilidad_wifi =  $value2->factibilidad_wifi;
              }
              if(property_exists($value2, 'otros_archivos_especialidades')){
                $nuevoCevs->otros_archivos_especialidades =  $value2->otros_archivos_especialidades;
              }
              if(property_exists($value2, 'observaciones_area_especialidade')){
                $nuevoCevs->observaciones_area_especialidade =  $value2->observaciones_area_especialidade;
              }
              if(property_exists($value2, 'documento_area_especialidades')){
                $nuevoCevs->documento_area_especialidades =  $value2->documento_area_especialidades->URL;
              }
              //PREINVERSIÓN
              if(property_exists($value2, 'politica_comunal_deportiva')){
                $nuevoCevs->politica_comunal_deportiva =  $value2->politica_comunal_deportiva;
              }
              if(property_exists($value2, 'plan_desarrollo_comunal')){
                $nuevoCevs->plan_desarrollo_comunal =  $value2->plan_desarrollo_comunal;
              }
              if(property_exists($value2, 'listado_proyectos_deportivos')){
                $nuevoCevs->listado_proyectos_deportivos =  $value2->listado_proyectos_deportivos;
              }
              if(property_exists($value2, 'fotografia_recintos')){
                $nuevoCevs->fotografia_recintos =  $value2->fotografia_recintos;
              }
              if(property_exists($value2, 'mapas_redes_transporte')){
                $nuevoCevs->mapas_redes_transporte =  $value2->mapas_redes_transporte;
              }
              if(property_exists($value2, 'mapas_comunales')){
                $nuevoCevs->mapas_comunales =  $value2->mapas_comunales;
              }
              if(property_exists($value2, 'zona_no_infraestructura')){
                $nuevoCevs->zona_no_infraestructura =  $value2->zona_no_infraestructura;
              }
              if(property_exists($value2, 'grupo_no_atendido')){
                $nuevoCevs->grupo_no_atendido =  $value2->grupo_no_atendido;
              }
              if(property_exists($value2, 'identificar_demandas')){
                $nuevoCevs->identificar_demandas =  $value2->identificar_demandas;
              }
              if(property_exists($value2, 'localizacion_colegios')){
                $nuevoCevs->localizacion_colegios =  $value2->localizacion_colegios;
              }
              if(property_exists($value2, 'catastro_infraestructura')){
                $nuevoCevs->catastro_infraestructura =  $value2->catastro_infraestructura;
              }
              if(property_exists($value2, 'listado_de_organizaciones_deport')){
                $nuevoCevs->listado_de_organizaciones_deport =  $value2->listado_de_organizaciones_deport;
              }
              if(property_exists($value2, 'proximidad_del_terreno')){
                $nuevoCevs->proximidad_del_terreno =  $value2->proximidad_del_terreno;
              }
              if(property_exists($value2, 'documentacion_estudios')){
                $nuevoCevs->documentacion_estudios =  $value2->documentacion_estudios;
              }
              if(property_exists($value2, 'carta_compromiso_uso')){
                $nuevoCevs->carta_compromiso_uso =  $value2->carta_compromiso_uso;
              }
              if(property_exists($value2, 'segunda_alternativa_terreno')){
                $nuevoCevs->segunda_alternativa_terreno =  $value2->segunda_alternativa_terreno;
              }
              if(property_exists($value2, 'funcionamiento_instalaciones_dep')){
                $nuevoCevs->funcionamiento_instalaciones_dep =  $value2->funcionamiento_instalaciones_dep;
              }
              if(property_exists($value2, 'otros_archivos_preinversion')){
                $nuevoCevs->otros_archivos_preinversion =  $value2->otros_archivos_preinversion;
              }
              if(property_exists($value2, 'observaciones_area_preinversion')){
                $nuevoCevs->observaciones_area_preinversion =  $value2->observaciones_area_preinversion;
              }
              if(property_exists($value2, 'documento_area_preinversion')){
                $nuevoCevs->documento_area_preinversion =  $value2->documento_area_preinversion->URL;
              }
              //PASO 4
              if(property_exists($value2, 'confirmacion_de_documentos')){
                $nuevoCevs->confirmacion_de_documentos =  $value2->confirmacion_de_documentos;
              }
              if(property_exists($value2, 'adjuntar_archivo_conductor')){
                $nuevoCevs->adjuntar_archivo_conductor =  $value2->adjuntar_archivo_conductor->URL;
              }
              //COMPROBANTE
              if(property_exists($value2, 'comprobante')){
                $nuevoCevs->comprobante =  $value2->comprobante;
              }
            }
           
              return view('cevs.cevs_detalle')
             ->with('data' , $nuevoCevs);
    }
}
