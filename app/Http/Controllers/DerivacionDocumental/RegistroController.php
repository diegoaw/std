<?php

namespace App\Http\Controllers\DerivacionDocumental;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Mail;
use App\Models\DerivacionDocumental\Registros;
use App\Models\DerivacionDocumental\Distribuciones;
use App\Models\DerivacionDocumental\Derivaciones;
use App\Models\DerivacionDocumental\Visados;
use App\Models\DocumentosDigitales\TiposDocumentosDigitales;
use App\Models\DerivacionDocumental\Prioridad;
use App\Models\PyR\PyR;
use App\User;
use DB;


class RegistroController extends Controller
{

    public function index(Request $request)
    {
      $registros = Registros::get();

      return view('derivacion_documental.registros')
      ->with('registros' , $registros);
    }


    public function create()
    {
      $varNull = null;
      $selectTipoDocumento[null] = 'Seleccione Tipo Documento'; foreach (TiposDocumentosDigitales::orderby('id', 'asc')->get() as $key1 => $value1) { $selectTipoDocumento[$value1->id]=$value1->nombre;}
      $selectFuncionarios[null] = 'Seleccione Funcionario(s)'; foreach (PyR::whereNotNull('id_wsad')->orderby('id', 'asc')->get() as $key2 => $value2) { $selectFuncionarios[$value2->id_wsad]=$value2->name;}
      $selectFuncionariosDeri[null] = 'Seleccione Funcionario(s)'; foreach (PyR::whereNotNull('id_wsad')->orderby('id', 'asc')->get() as $key4 => $value4) { $selectFuncionariosDeri[$value4->id_wsad]=$value4->name;}
      $selectPrioridad[null] = 'Seleccione Prioridad'; foreach (Prioridad::orderby('id', 'asc')->get() as $key3 => $value3) { $selectPrioridad[$value3->id]=$value3->nombre;}
      $selectUsuarios = [];
      foreach (PyR::get() as $key5 => $value5){$selectUsuarios[$value5->email]=$value5->nombres.' '.$value5->apellido_paterno.' '.$value5->apellido_materno.' '.$value5->unidad_desempeno.' '.$value5->email; }

      $idUsuarioSesion = Auth::user()->id;

      return view('derivacion_documental.registro_crear')
      ->with('varNull' , $varNull)
      ->with('selectTipoDocumento' , $selectTipoDocumento)
      ->with('selectFuncionarios' , $selectFuncionarios)
      ->with('selectFuncionariosDeri' , $selectFuncionariosDeri)
      ->with('selectPrioridad' , $selectPrioridad)
      ->with('idUsuarioSesion' , $idUsuarioSesion)
      ->with('selectUsuarios' , $selectUsuarios);
      
    }


     public function store(Request $request)
    {
      $origen = $request->origen; /*radio*/
      $idUser = $request->id_user;
      $nombreRemi = $request->nombre_remi;
      $funcionRemi = $request->funcion_remi;
      $procedenciaRemi = $request->procedencia_remi;
      $relacionDocs = $request->relacion_docs; /*radio*/
      $tipoDocumento = $request->id_tipo_documento; /*Select tipo documento*/
      $folioRelaDocs = $request->n_folio_rela_docs;
      $tieneFolio = $request->tiene_folio; /*radio*/
      $folioDeriva = $request->n_folio_deriva;
      $asignacionFolio = $request->asignacion_folio; /*radio*/
      $materia = $request->materia;
      $filename = $request->filename;
      $tipoDistribucion = $request->tipo_distribucion; /*radio*/
      $docdigital = $request->docdigital; /*radio*/
      $ventanillaUnica = $request->ventanilla_unica; /*radio*/
      $observaciones = $request->observaciones; 
      $requiereRespuesta = $request->requiere_respuesta; /*radio*/
      $prioridad = $request->id_prioridad; 
      $destinatario = $request->id_destinatario; /*Select destinatario*/
      $destinatario2 = $request->correos_internos; /*Select destinatario*/

      

      $nuevoRegistro = new Registros();
      $nuevoRegistro->origen = $origen;

      if ($origen == 1) {
        $nuevoRegistro->id_user = $idUser;
      }

      if ($origen == 0) {
        $nuevoRegistro->nombre_remi = $nombreRemi;
        $nuevoRegistro->funcion_remi = $funcionRemi;
        $nuevoRegistro->procedencia_remi = $procedenciaRemi;
      }

      $nuevoRegistro->relacion_docs = $relacionDocs;
      $nuevoRegistro->id_tipo_documento = $tipoDocumento;
      $nuevoRegistro->n_folio_rela_docs = $folioRelaDocs;
      $nuevoRegistro->tiene_folio = $tieneFolio;
      $nuevoRegistro->n_folio_deriva = $folioDeriva;
      $nuevoRegistro->asignacion_folio = $asignacionFolio;
      $nuevoRegistro->materia = $materia;

      $nuevoRegistro->filename = $request->filename->getClientOriginalName();
      $upload = Storage::disk('s3_3')->put('GestionDocumental', $filename, 'public');
      $nuevoRegistro->url_s3 = 'https://documentos-digitales-std.s3.amazonaws.com/'.$upload;

      $nuevoRegistro->tipo_distribucion = $tipoDistribucion;
      $nuevoRegistro->docdigital = $docdigital;
      $nuevoRegistro->ventanilla_unica = $ventanillaUnica;
      $nuevoRegistro->observaciones = $observaciones;
      $nuevoRegistro->requiere_respuesta = $requiereRespuesta;
      $nuevoRegistro->id_prioridad = $prioridad;
      $nuevoRegistro->save();


      if ($tipoDistribucion == 'derivacion') {
        $nuevaDerivacion = new Derivaciones();
        $nuevaDerivacion->registro_id = $nuevoRegistro->id;
        $nuevaDerivacion->id_user = $destinatario;
        $nuevaDerivacion->save();
      } 

      if ($tipoDistribucion == 'distribucion') {
        foreach ($destinatario2 as $key => $value) {
          $nuevaDistribucion = new Distribuciones();
          $nuevaDistribucion->registro_id = $nuevoRegistro->id;
          $nuevaDistribucion->email = $value;
          $nuevaDistribucion->save();
        }
      }
      
      /* if ($tipoDistribucion == 'visado') {
        $nuevoVisado = new Visados();
        $nuevoVisado->registro_id = $nuevoRegistro->id;
        $nuevoVisado->id_user = destinatario;
        $nuevoVisado->save();
      } */

      alert()->success('!','Registro creado exitosamente');
      return redirect()->route('registros');
    }

}
