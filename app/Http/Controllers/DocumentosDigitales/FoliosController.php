<?php
namespace App\Http\Controllers\DocumentosDigitales;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Simple\TmpTramiteFolios;
use App\Models\DocumentosDigitales\CorrelativoDocumentosDigitales;
use App\Models\DocumentosDigitales\RegistroDocumentosDigitales;
use App\Models\DocumentosDigitales\Lugar;
use App\Models\DocumentosDigitales\TiposDocumentosDigitales;
use App\Models\Simple\DatoSeguimiento;
use App\Models\ParametrosSistemas\ParametrosSistema;
use DB;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Models\PyR\PyR;
use Mail;
use App\Mail\DistribucionDocumentoFirmadoEmail;
use App\Http\Requests\DocumentosDigitales\ReservarFolioRequest;
use App\Http\Requests\DocumentosDigitales\EditarReservarFolioRequest;
use App\Http\Requests\DocumentosDigitales\JustificarFolioRequest;
use App\Http\Requests\DocumentosDigitales\DistribuirFolioRequest;
use Maatwebsite\Excel\Facades\Excel;
use Dompdf\Dompdf;
use PDF;
use Carbon\Carbon;



class FoliosController extends Controller
{

    public function index(Request $request){
        set_time_limit(14200);
        $correlativo = $request->correlativo;
        $tramite = $request->tramite;
        $tipoDocumento = $request->tipo_documento;
        $ano = $request->ano;
        $estado = $request->estado;
        $materia = $request->materia;
        $estados = [null=>'Seleccione Estado' , 'Completado' => 'Completado'  , 'Incompleto'=>'Incompleto'];
        $anos = [null=>'Seleccione Año' ,'2020' => '2020', '2021' => '2021', '2022' => '2022'];
        $tiposDocumentos[null] = [null=>'Seleccione Tipo Documento' , 2 => 'Memorándums Electrónicos'  , 4=>'Oficios Electrónicos'];

        $correlativo = $request->correlativo;
        $tramite = $request->tramite;
        $tipoDocumento = $request->tipo_documento;
        $ano = $request->ano;
        $estado = $request->estado;



        if($correlativo == null &&  $tramite == null &&  $tipoDocumento == null && $ano== null){
          $storeProcedium = 'call folios()';
          $tramites = DB::connection('mysqlSimple')->select($storeProcedium);
        }
       

        
      
        $query = TmpTramitefolios::query ();
        $query->selectRaw("REPLACE(REPLACE(correlativo, '\"' , ''), '\\\' , '') as correlativo , reservado, tramite_id , proceso_id , pendiente , ended_at,  SUBSTRING(fecha, -5, 4) as fecha, etapas, http_code_correlativo, estatus , lugar");
        $query->where('correlativo', '!=' , null);
        $query->where('http_code_correlativo', '!=' , 404);
        $query->where('http_code_correlativo', '!=' , 429);
        $query->when(request('correlativo', false), function ($q, $correlativo) {
               $q->where('correlativo', 'LIKE', "%$correlativo%");
        });
        $query->when(request('tramite', false), function ($q, $tramite) {
          return $q->where('tramite_id', $tramite);
        });
        $query->when(request('tipo_documento', false), function ($q, $tipoDocumento) {
          return $q->where('proceso_id', $tipoDocumento);
        });
        $query->when(request('estado', false), function ($q, $estado) {
          return $q->where('estatus', $estado);
        });
        $query->when(request('ano', false), function ($q, $ano) {
          return $q->whereRaw('SUBSTRING(fecha, -5, 4) = '.$ano);
        });

         $listado = $query->get()->sortByDesc(function($q){
        return $q->fecha;
        });
        
        
        foreach ($listado as $llave => $valor) {
            $datoSeguimientoTramites = DatoSeguimiento::select('valor')->whereIn('etapa_id', explode(",", $valor->etapas))->whereIn('nombre', ['adjuntar_memo', 'adjuntar_ofi'])->first();
            $auxURL = json_encode($datoSeguimientoTramites);
                if($auxURL != 'null'){
                    $valor->anexos = (json_decode(json_decode($auxURL)->valor))->URL;
                }
            
            
        }


        return view('documentos_digitales.folios')
        ->with('estados' , $estados)
        ->with('anos' , $anos)
        ->with('tiposDocumentos' , $tiposDocumentos)
        ->with('listado' , $listado)
        ->with('correlativo' , $correlativo)
        ->with('tramite' , $tramite)
        ->with('estado' , $estado)
        ->with('ano' , $ano)
        ->with('materia' , $materia)
        ->with('tipoDocumento' , $tipoDocumento);

    }

    public function index2(Request $request){
        $correlativo = $request->correlativo;
        $tramite = $request->tramite;
        $tipoDocumento = $request->tipo_documento;
        $ano = $request->ano;
        $lugar = $request->lugar;
        $estado = $request->estado;
        $materia = $request->materia;
        $estados = [null=>'Seleccione Estado' , 'OCUPADO' => 'OCUPADO'  , 'APARTADO'=>'APARTADO'];
        $anos = [null=>'Seleccione Año' , '2022' => '2022'];
        $tiposDocumentos[null] = 'Sleccione Tipo Documento'; foreach (TiposDocumentosDigitales::where('estatus', 1)->get() as $key1 => $value1) { $tiposDocumentos[$value1->id]=$value1->nombre;} 
        $lugares[null] = 'Seleccione Lugar'; foreach (Lugar::get() as $key1 => $value1) { $lugares[$value1->nombre]=$value1->nombre;} 

        
      
        $correlativos =  CorrelativoDocumentosDigitales::get();

      //  dd($tipoDocumento);

     $query = CorrelativoDocumentosDigitales::query ();
      $query->when(request('correlativo', false), function ($q, $correlativo) {
            return $q->where('correlativo', $correlativo);
      });
      $query->when(request('tramite', false), function ($q, $tramite) {
        return $q->where('id_registro', $tramite);
      });
      $query->when(request('tipo_documento', false), function ($q, $tipoDocumento) {
        return $q->where('tipo_documento_id', $tipoDocumento);
      });
      $query->when(request('lugar', false), function ($q, $lugar) {
        return $q->where('lugar', $lugar);
      });
      $query->when(request('estado', false), function ($q, $estado) {
        return $q->where('estatus', $estado);
      });
      $query->when(request('ano', false), function ($q, $ano) {
        return $q->where('ano', $ano);
      });
      $query->when(request('materia'), function ($q, $materia) {
        return $q->where('materia', 'LIKE', "%$materia%");
      });
     $correlativos = $query->orderby('id' , 'desc')->get();
     
    

     return view('documentos_digitales.gestion_correlativos')
     ->with('estados' , $estados)
     ->with('anos' , $anos)
     ->with('lugares' , $lugares)
     ->with('tiposDocumentos' , $tiposDocumentos)
     ->with('listado' , $correlativos)
     ->with('correlativo' , $correlativo)
     ->with('tramite' , $tramite)
     ->with('estado' , $estado)
     ->with('ano' , $ano)
     ->with('materia' , $materia)
     ->with('lugar' , $lugar)
     ->with('tipoDocumento' , $tipoDocumento)
     
     
     ;
    }
    public function reserva(Request $request){
        $selectTipo[null] = 'Seleccione Tipo de Documento'; foreach (TiposDocumentosDigitales::where('estatus', 1)->orderby('id', 'asc')->get() as $key1 => $value1) { $selectTipo[$value1->id]=$value1->nombre;} 
        $selectLugar[null] = 'Seleccione Lugar'; foreach (Lugar::orderby('id', 'asc')->get() as $key2 => $value2) { $selectLugar[$value2->nombre]=$value2->nombre;} 
        $lugaresCentral[null] = 'Seleccione Lugar'; foreach (Lugar::whereIn('id' , [1])->get() as $key4 => $value4) { $lugaresCentral[$value4->nombre]=$value4->nombre;} 
        $lugaresRegiones[null] = 'Seleccione Lugar'; foreach (Lugar::whereNotIn('id' , [1])->get() as $key5 => $value5) { $lugaresRegiones[$value5->nombre]=$value5->nombre;}
        $TiposDocumentos = TiposDocumentosDigitales::get();
        $TiposDocumentos = $TiposDocumentos->pluck('nombre','id')->prepend('Seleccione', null);
        return view('documentos_digitales.reserva_folios')
        ->with('lugaresCentral' , $lugaresCentral)
        ->with('lugaresRegiones' , $lugaresRegiones)
        ->with('TiposDocumentos' , $selectTipo)
        ->with('selectLugar' , $selectLugar);
    }
    public function reservar(ReservarFolioRequest $request){
        $idTipoDocumento =  $request->tipo_documento;
        $cantidadFoliosReserva =  $request->n_folios;
        $descripcionPorque = $request->descripcion;
        $lugarId = $request->lugar;
        $lugar = Lugar::where('nombre', $lugarId)->first();
        
        $bucandoCorrelativo = CorrelativoDocumentosDigitales::where('tipo_documento_id' , $idTipoDocumento)->get();
        if($bucandoCorrelativo->count()>0){
            for ($i=0; $i < $cantidadFoliosReserva ; $i++) { 
                $maxValue = CorrelativoDocumentosDigitales::where('tipo_documento_id' , $idTipoDocumento)->max('correlativo');
                $nuevoRegistroReserva = New CorrelativoDocumentosDigitales(); 
                $nuevoRegistroReserva->tipo_documento_id = $idTipoDocumento;
                $nuevoRegistroReserva->correlativo = ($maxValue+1);
                $nuevoRegistroReserva->ano = date('Y');
                $nuevoRegistroReserva->id_user = Auth::user()->id;
                $nuevoRegistroReserva->estatus = 'APARTADO';
                $nuevoRegistroReserva->reservado = 1;
                $nuevoRegistroReserva->lugar = $lugar->nombre;
                $nuevoRegistroReserva->motivo_reserva = $descripcionPorque;
                $nuevoRegistroReserva->save();
            }
        }else{
            switch ($idTipoDocumento) {
                case 2:
                    $maxValue = ParametrosSistema::where('nombre' , 'correlativo_inicio_resoluciones')->first()->valor;
                    break;
                case 3:
                    $maxValue = ParametrosSistema::where('nombre' , 'correlativo_inicio_decretos')->first()->valor;
                    break;
                case 4:
                    $maxValue = ParametrosSistema::where('nombre' , 'correlativo_inicio_decretosexentos')->first()->valor;
                    break;
                case 5:
                    $maxValue = ParametrosSistema::where('nombre' , 'correlativo_inicio_decretossupremos')->first()->valor;
                    break; 
                case 6:
                    $maxValue = ParametrosSistema::where('nombre' , 'correlativo_inicio_oficios')->first()->valor;
                    break;
                case 16:
                    $maxValue = ParametrosSistema::where('nombre' , 'correlativo_inicio_memos')->first()->valor;
                    break; 
            }
        
            for ($i=0; $i < $cantidadFoliosReserva ; $i++) { 
                $maxValue = CorrelativoDocumentosDigitales::where('tipo_documento_id' , $idTipoDocumento)->max('correlativo')? CorrelativoDocumentosDigitales::where('tipo_documento_id' , $idTipoDocumento)->max('correlativo'):$maxValue;
                $nuevoRegistroReserva = New CorrelativoDocumentosDigitales(); 
                $nuevoRegistroReserva->tipo_documento_id = $idTipoDocumento;
                $nuevoRegistroReserva->correlativo = ($maxValue+1);
                $nuevoRegistroReserva->ano = date('Y');
                $nuevoRegistroReserva->id_user = Auth::user()->id;
                $nuevoRegistroReserva->estatus = 'APARTADO';
                $nuevoRegistroReserva->reservado = 1;
                $nuevoRegistroReserva->lugar = $lugar->nombre;
                $nuevoRegistroReserva->motivo_reserva = $descripcionPorque;
                $nuevoRegistroReserva->save();
            }
        }

        alert()->success('!','Apartado de Folio(s) exitoso(s)');
        return redirect()->route('folios2');
    }

    public function editfolio($id)
    {
      $editRegistroReserva = CorrelativoDocumentosDigitales::where('id' , $id)->first();
      $lugarEdit = Lugar::where('nombre' , $editRegistroReserva->lugar)->first();
        if($lugarEdit){
            $lugarEdit = $lugarEdit->id;
        }
      $selectLugar[null] = 'Seleccione Lugar'; foreach (Lugar::orderby('id', 'asc')->get() as $key2 => $value2) { $selectLugar[$value2->id]=$value2->nombre;} 
      
      return view('documentos_digitales.reserva_folio_editar')
      ->with('editRegistroReserva' , $editRegistroReserva)
      ->with('selectLugar' , $selectLugar)
      ->with('lugarEdit' , $lugarEdit);
    }


     public function updatefolio(EditarReservarFolioRequest $request, $id)
    {
      $lugarId = $request->lugar;
      $lugar = Lugar::where('id', $lugarId)->first();
      $motivoCambio = $request->motivo_cambio;
        
      $editRegistroReserva = CorrelativoDocumentosDigitales::where('id' , $id)->first();
      $editRegistroReserva->lugar = $lugar->nombre;
      $editRegistroReserva->motivo_cambio = $motivoCambio;
      $editRegistroReserva->save();
      alert()->success('!','Folio Editado Exitosamente');
      return redirect()->route('folios2');
    }
    
    
    public function ocupar($id)
    {
        
      $editRegistro = CorrelativoDocumentosDigitales::where('id' , $id)->first();
      
      return view('documentos_digitales.folio_ocupar')
      ->with('editRegistro' , $editRegistro);
    }


     public function ocupado(JustificarFolioRequest $request, $id)
    {
      $materia = $request->materia;
      $documento = $request->filename;
      $editRegistro =  CorrelativoDocumentosDigitales::where('id' , $id)->first();
      $editRegistro->estatus = 'OCUPADO';
      $editRegistro->materia = $materia;
      $upload = Storage::disk('s3_3')->put('archivos-correlativos', $documento, 'public');
      $editRegistro->documento = 'https://documentos-digitales-std.s3.us-west-2.amazonaws.com/'.$upload;
      $editRegistro->save();
      alert()->success('!','Folio Ocupado Exitosamente');
      return redirect()->route('folios2');
    }

    public function distribucion(Request $request, $id){
      $Registro = CorrelativoDocumentosDigitales::where('id' , $id)->first();
      $selectUsuarios = [];
      foreach (PyR::get() as $key1 => $value1){$selectUsuarios[$value1->email]=$value1->nombres.' '.$value1->apellido_paterno.' '.$value1->apellido_materno.' '.$value1->unidad_desempeno.' '.$value1->email; }
     
      return view('documentos_digitales.distribuir_documentos_digitales')
      ->with('selectUsuarios' , $selectUsuarios)
      ->with('Registro' , $Registro);
    }

    public function distribuir(DistribuirFolioRequest $request, $id){
      $correosInternos = $request->correos_internos;
      $correosExternos = $request->correos_externos;
      $valoresVacios = false;
      $docdigital = $request->docdigital;
      //$observacion = $request->distribucion_observacion;
      //$tipoDistribucion = $request->tipo_distribucion;

      /* if ($tipoDistribucion == 1) {
        if (!$request->correos_internos){
            return redirect()->back()->withErrors(['El Campo de Destinatarios Internos es requerido.']);
        }
        $correosInternos = $request->correos_internos;
        
        if (!$request->distribucion_observacion){
          return redirect()->back()->withErrors(['El Campos de Observaciones Distribución es requerido.']);
        }
        $observacion = $request->distribucion_observacion;
      } */


      if ($correosInternos) {
        $correosInternos = $request->correos_internos;
      }
      /* if ($observacion) {
        $observacion = $request->distribucion_observacion;
        
        $Registro->distribucion_observacion = $observacion;
        $Registro->save();
      } */


      foreach ($correosExternos as $llave5 => $valor5) {
        if($valor5){
          $valoresVacios = true;
        }
      }
      if ($correosExternos) {
      $correosExternos = $request->correos_externos;
      }
      /* if ($tipoDistribucion == 0 && $docdigital == 0) {
        if (!$valoresVacios){
            return redirect()->back()->withErrors(['El Campo de Destinatarios Externos es requerido.']);
        }
        $correosExternos = $request->correos_externos;
      } */

      $Registro = CorrelativoDocumentosDigitales::where('id' , $id)->first();
      //$Registro->tipo_distribucion = $tipoDistribucion;
      //$Registro->save();

      //if($tipoDistribucion==1){      
      //if($correosInternos[0]!=""){
        //$Registro->distribucion_observacion = $observacion;
        //$Registro->save();    
        if($correosInternos){
        if(count($correosInternos)>0){
        foreach ($correosInternos as $keyi => $valuei) {
          if($valuei!=""){
             Mail::to($valuei)->send(new DistribucionDocumentoFirmadoEmail($Registro));
          
            /*  $GuardarDatoRegistro = CorrelativoDocumentosDigitales::where('id' , $id)->first();
             //$GuardarDatoRegistro->tipo_distribucion = 1;
             $GuardarDatoRegistro->distribuido = 1;
             $GuardarDatoRegistro->reservado_distribucion = 0;             
             $GuardarDatoRegistro->save();

             $GuardarenRegitro = RegistroDocumentosDigitales::where('correlativo', $GuardarDatoRegistro->correlativo)->first();
             $GuardarenRegitro->distribuido = 1;
             $GuardarenRegitro->save(); */
          }
          }
        }
      }
      //}
    //}
    
   // if($tipoDistribucion==0){
    if($correosExternos){
        if(count($correosExternos)>0){
        foreach ($correosExternos as $keye => $valuee) {
          //if($valuee){
            if($valuee!=""){
             Mail::to($valuee)->send(new DistribucionDocumentoFirmadoEmail($Registro));
             /* $GuardarDatoRegistro = CorrelativoDocumentosDigitales::where('id' , $id)->first();
             //$GuardarDatoRegistro->tipo_distribucion = 0;
             $GuardarDatoRegistro->distribuido = 1;
             $GuardarDatoRegistro->reservado_distribucion = 0;
             $GuardarDatoRegistro->save();

             $GuardarenRegitro = RegistroDocumentosDigitales::where('correlativo', $GuardarDatoRegistro->correlativo)->first();
             $GuardarenRegitro->distribuido = 1;
             $GuardarenRegitro->save(); */
            }
          //}else{
             /* $GuardarDatoRegistro = CorrelativoDocumentosDigitales::where('id' , $id)->first();
             //$GuardarDatoRegistro->tipo_distribucion = 0;
             $GuardarDatoRegistro->distribuido = 1;
             $GuardarDatoRegistro->reservado_distribucion = 0;
             $GuardarDatoRegistro->save();
             
             if($GuardarDatoRegistro->id_registro != null){
              $GuardarenRegitro = RegistroDocumentosDigitales::where('correlativo', $GuardarDatoRegistro->correlativo)->first();
              $GuardarenRegitro->distribuido = 1;
              $GuardarenRegitro->save();
             } */
          //}
          }
        }
      }
      //}
     
      $GuardarDatoRegistro = CorrelativoDocumentosDigitales::where('id' , $id)->first();
      $GuardarDatoRegistro->distribuido = 1;
      $GuardarDatoRegistro->reservado_distribucion = 0; 
        

      if($docdigital){ 
        $GuardarDatoRegistro->docdigital = 1; 
      }else {
        $GuardarDatoRegistro->docdigital = 0;
      }
      
      $GuardarDatoRegistro->save();

      $GuardarenRegitro = RegistroDocumentosDigitales::where('id', $GuardarDatoRegistro->id_registro)->first();
      $GuardarenRegitro->distribuido = 1;
      $GuardarenRegitro->save();

      alert()->success('!','Distribución(es) Enviada(s) Exitosamente');
      return redirect()->route('folios2');
      
    }


    public function reporteFolios(Request $request) {

      $tipoReporte = $request->imprimir;
      $mes = (date("n")-1);
     // dd($mes);
      $foliosApartadosMes = CorrelativoDocumentosDigitales::where('reservado' , 1)->whereMonth('created_at', $mes)->get();
      //dd($foliosApartadosMes);

      switch ($tipoReporte) {
      case 'XLS':
                  Excel::create("FoliosReporte", function ($excel) use ($foliosApartadosMes) {
                  $excel->setTitle("FoliosReporte");
                  $excel->sheet("FoliosReporte", function ($sheet) use ($foliosApartadosMes) {
                  $sheet->loadView('documentos_digitales.reporte_folios_ex_excel')->with('foliosApartadosMes', $foliosApartadosMes);
              });
          })->download('xlsx');

              return back();

              break;
      default:
      break;

      }
      }

}