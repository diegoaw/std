<?php
 
namespace App\Http\Controllers\DocumentosDigitales;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\DocumentosDigitales\RegistroDocumentosDigitales;
use App\Models\DocumentosDigitales\TiposDocumentosDigitales;
use App\Models\DocumentosDigitales\Categoria;
use App\Models\DocumentosDigitales\Firmantes;
use App\Models\DocumentosDigitales\FirmantesSub;
use App\Models\DocumentosDigitales\VistosBuenos;
use App\Models\DocumentosDigitales\ListadoFirmantesRegistroDocumentosDigitales;
use App\Models\DocumentosDigitales\AnexosDocumentosDigitales;
use App\Models\DocumentosDigitales\EtapasDocumentosDigitales;
use App\Models\DocumentosDigitales\DerivacionRegistroDocumentosDigitales;
use App\Models\DocumentosDigitales\EstatusDerivacionRegistroDocumentosDigitales;
use App\Models\DocumentosDigitales\CorrelativoDocumentoUnidad;
use App\Models\DocumentosDigitales\CorrelativoDocumentosDigitales;
use App\Models\DocumentosDigitales\Adjuntos;
use App\Models\DocumentosDigitales\Plantillas;
use App\Models\PyR\PyR;
use App\Models\DocumentosDigitales\Lugar;
use App\Models\DocumentosDigitales\GaleriaFirmas;
use App\Models\Seguridad\Audits;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\FirmarDocumentos\JWT;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use Jenssegers\Date\Date;
use setasign\Fpdi\Tcpdf\Fpdi;
use App\Http\Controllers\Sigc\CalendarioController;
//use SimpleSoftwareIO\QrCode\Facades\QrCode;
use App\Http\Requests\DocumentosDigitales\RegistroDocumentoRequest;
use App\Http\Requests\DocumentosDigitales\ColaboracionDocumentoRequest;
use App\Http\Requests\DocumentosDigitales\ColaboracionDocumentoFirmanteRequest;
use App\Http\Requests\DocumentosDigitales\GenerarEnlaceRequest;
use App\Http\Requests\DocumentosDigitales\EditarGenerarEnlaceRequest;
use Mail;
use App\Mail\NotificacionNuevoRegistroFirmaEmail;
use App\Mail\NotificacionNuevoRegistroColaboracionEmail;
use App\Mail\NotificacionDocumentoDigitalFirmadoEmail;
use App\Mail\NotificacionDocumentoDigitalFirmadoDistribucionEmail;
use DB;

class RegistroDocumentosDigitalesController extends Controller
{

    public function indexOficios(Request $request)
    {
        $registrosRservados = RegistroDocumentosDigitales::orderby('id', 'desc')->where('id_tipos_documentos_digitales', 6)->get();
        $anno = Carbon::now()->format('Y');
        $fechaStringInicio = ($anno).'-01-01 00:00:00';
        $feDesde = Carbon::parse($fechaStringInicio);
        $feHasta =  Carbon::parse(Carbon::now()->format('Y-m-d').' 23:59:59');
        $fechaInDesde = Carbon::parse($fechaStringInicio);
        $fechaInHasta = Carbon::now();
        $fechas[1] = Date('Y-m-d', strtotime( '1970-01-01' ));
        $fechas[0] = Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
        $correlativo = $request->correlativo;
        $tramite = $request->tramite;
        $categoria = $request->categoria;
        $user = $request->user;
        $lugar = $request->lugar;
        $etapa = $request->etapa;
        $materia = $request->materia;
        $etapas[null] = 'Seleccione Etapa'; foreach (EtapasDocumentosDigitales::get() as $key1 => $value1) { $etapas[$value1->id]=$value1->nombre;} 
        $categorias[null] = 'Seleccione Categoría'; foreach (Categoria::get() as $key2 => $value2) { $categorias[$value2->id]=$value2->nombre;} 
        $users[null] = 'Seleccione Funcionario(a)'; foreach (User::orderby('name', 'asc')->get() as $key3 => $value3) { $users[$value3->id]=$value3->name;} 
        $lugares[null] = 'Seleccione Lugar'; foreach (Lugar::orderby('nombre', 'asc')->get() as $key4 => $value4) { $lugares[$value4->nombre]=$value4->nombre;} 
        


        if( $request->FechaDesde != null ) {
            $feDesde=(Carbon::parse($request->get('FechaDesde'))->format('dmY'));
            $feDesde= Carbon::parse(substr($feDesde, 0, 2).'-'.substr($feDesde, 2, 2).'-'.substr($feDesde, 4, 4).' 00:00:00');
        }
        if( $request->FechaHasta != null ) {
            $feHasta=(Carbon::parse($request->get('FechaHasta'))->format('dmY'));
            $feHasta= Carbon::parse(substr($feHasta, 0, 2).'-'.substr($feHasta, 2, 2).'-'.substr($feHasta, 4, 4).' 23:59:59');
        }

        $query = RegistroDocumentosDigitales::query ();
        $query->where('id_tipos_documentos_digitales', 6);
        $query->when(request('correlativo', false), function ($q, $correlativo) {
                return $q->where('correlativo', $correlativo);
        });
        $query->when(request('tramite', false), function ($q, $tramite) {
            return $q->where('id', $tramite);
        });
        $query->when(request('FechaDesde', false), function ($q, $feDesde) {
            return $q->where('created_at','>=', $feDesde);
        });
        $query->when(request('FechaHasta', false), function ($q, $feHasta) {
            return $q->where('created_at','<=', $feHasta);
        });
        $query->when(request('etapa', false), function ($q, $etapa) {
            return $q->where('id_etapas_documentos_digitales', $etapa);
        });
        $query->when(request('user', false), function ($q, $user) {
            return $q->where('id_user', $user);
        });
        $query->when(request('lugar', false), function ($q, $lugar) {
            return $q->where('lugar', $lugar);
        });
        $query->when(request('categoria', false), function ($q, $categoria) {
            return $q->where('id_categoria', $categoria);
        });
        $query->when(request('materia'), function ($q, $materia) {
            return $q->where('titulo', 'LIKE', "%$materia%");
        });
        $correlativos = $query->orderby('updated_at', 'desc')->paginate(30);

        

   
        return view('documentos_digitales.registro_documento_digitales_oficios')
        ->with('registrosRservados' ,$correlativos)
        ->with('feDesde' ,$feDesde)
        ->with('feHasta' ,$feHasta)
        ->with('fechaInDesde' ,$fechaInDesde)
        ->with('fechaInHasta' ,$fechaInHasta)
        ->with('fechas' ,$fechas)
        ->with('categorias' , $categorias)
        ->with('users' , $users)
        ->with('lugares' , $lugares)
        ->with('etapas' , $etapas)
        ->with('correlativo' , $correlativo)
        ->with('tramite' , $tramite)
        ->with('categoria' , $categoria)
        ->with('user' , $user)
        ->with('lugar' , $lugar)
        ->with('etapa' , $etapa)
        ->with('materia' , $materia);
    }

    public function indexResex(Request $request)
    {
        $registrosRservados = RegistroDocumentosDigitales::orderby('id', 'desc')->where('id_tipos_documentos_digitales', 2)->get();  
        $anno = Carbon::now()->format('Y');
        $fechaStringInicio = ($anno).'-01-01 00:00:00';
        $feDesde = Carbon::parse($fechaStringInicio);
        $feHasta =  Carbon::parse(Carbon::now()->format('Y-m-d').' 23:59:59');
        $fechaInDesde = Carbon::parse($fechaStringInicio);
        $fechaInHasta = Carbon::now();
        $fechas[1] = Date('Y-m-d', strtotime( '1970-01-01' ));
        $fechas[0] = Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
        $correlativo = $request->correlativo;
        $tramite = $request->tramite;
        $categoria = $request->categoria;
        $user = $request->user;
        $lugar = $request->lugar;
        $etapa = $request->etapa;
        $materia = $request->materia;
        $etapas[null] = 'Seleccione Etapa'; foreach (EtapasDocumentosDigitales::get() as $key1 => $value1) { $etapas[$value1->id]=$value1->nombre;} 
        $categorias[null] = 'Seleccione Categoría'; foreach (Categoria::get() as $key2 => $value2) { $categorias[$value2->id]=$value2->nombre;} 
        $users[null] = 'Seleccione Funcionario(a)'; foreach (User::orderby('name', 'asc')->get() as $key3 => $value3) { $users[$value3->id]=$value3->name;} 
        $lugares[null] = 'Seleccione Lugar'; foreach (Lugar::orderby('nombre', 'asc')->get() as $key4 => $value4) { $lugares[$value4->nombre]=$value4->nombre;} 
        


        if( $request->FechaDesde != null ) {
            $feDesde=(Carbon::parse($request->get('FechaDesde'))->format('dmY'));
            $feDesde= Carbon::parse(substr($feDesde, 0, 2).'-'.substr($feDesde, 2, 2).'-'.substr($feDesde, 4, 4).' 00:00:00');
        }
        if( $request->FechaHasta != null ) {
            $feHasta=(Carbon::parse($request->get('FechaHasta'))->format('dmY'));
            $feHasta= Carbon::parse(substr($feHasta, 0, 2).'-'.substr($feHasta, 2, 2).'-'.substr($feHasta, 4, 4).' 23:59:59');
        }

      // dd($feDesde, $feHasta);
        $query = RegistroDocumentosDigitales::query ();
        $query->where('id_tipos_documentos_digitales', 2);
        $query->when(request('correlativo', false), function ($q, $correlativo) {
                return $q->where('correlativo', $correlativo);
        });
        $query->when(request('tramite', false), function ($q, $tramite) {
            return $q->where('id', $tramite);
        });
        $query->when(request('FechaDesde', false), function ($q, $feDesde) {
            return $q->where('created_at','>=', $feDesde);
        });
        $query->when(request('FechaHasta', false), function ($q, $feHasta) {
            return $q->where('created_at','<=', $feHasta);
        });
        $query->when(request('etapa', false), function ($q, $etapa) {
            return $q->where('id_etapas_documentos_digitales', $etapa);
        });
        $query->when(request('categoria', false), function ($q, $categoria) {
            return $q->where('id_categoria', $categoria);
        });
        $query->when(request('lugar', false), function ($q, $lugar) {
            return $q->where('lugar', $lugar);
        });
        $query->when(request('user', false), function ($q, $user) {
            return $q->where('id_user', $user);
        });
        $query->when(request('materia'), function ($q, $materia) {
            return $q->where('titulo', 'LIKE', "%$materia%");
        });
        //$correlativos = $query->orderby('updated_at', 'desc')->get();  
        $correlativos = $query->orderby('updated_at', 'desc')->paginate(30); 

        return view('documentos_digitales.registro_documento_digitales_resex')
        ->with('registrosRservados' ,$correlativos)
        ->with('feDesde' ,$feDesde)
        ->with('feHasta' ,$feHasta)
        ->with('fechaInDesde' ,$fechaInDesde)
        ->with('fechaInHasta' ,$fechaInHasta)
        ->with('fechas' ,$fechas)
        ->with('categorias' , $categorias)
        ->with('users' , $users)
        ->with('lugares' , $lugares)
        ->with('etapas' , $etapas)
        ->with('correlativo' , $correlativo)
        ->with('tramite' , $tramite)
        ->with('categoria' , $categoria)
        ->with('user' , $user)
        ->with('lugar' , $lugar)
        ->with('etapa' , $etapa)
        ->with('materia' , $materia);
    }

    public function indexMemos(Request $request)
    {
        $registrosRservados = RegistroDocumentosDigitales::orderby('id', 'desc')->where('id_tipos_documentos_digitales', 16)->get();
        $anno = Carbon::now()->format('Y');
        $fechaStringInicio = ($anno).'-01-01 00:00:00';
        $feDesde = Carbon::parse($fechaStringInicio);
        $feHasta =  Carbon::parse(Carbon::now()->format('Y-m-d').' 23:59:59');
        $fechaInDesde = Carbon::parse($fechaStringInicio);
        $fechaInHasta = Carbon::now();
        $fechas[1] = Date('Y-m-d', strtotime( '1970-01-01' ));
        $fechas[0] = Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
        $correlativo = $request->correlativo;
        $tramite = $request->tramite;
        $categoria = $request->categoria;
        $user = $request->user;
        $lugar = $request->lugar;
        $etapa = $request->etapa;
        $materia = $request->materia;
        $etapas[null] = 'Seleccione Etapa'; foreach (EtapasDocumentosDigitales::get() as $key1 => $value1) { $etapas[$value1->id]=$value1->nombre;} 
        $categorias[null] = 'Seleccione Categoría'; foreach (Categoria::get() as $key2 => $value2) { $categorias[$value2->id]=$value2->nombre;} 
        $users[null] = 'Seleccione Funcionario(a)'; foreach (User::orderby('name', 'asc')->get() as $key3 => $value3) { $users[$value3->id]=$value3->name;} 
        $lugares[null] = 'Seleccione Lugar'; foreach (Lugar::orderby('nombre', 'asc')->get() as $key4 => $value4) { $lugares[$value4->nombre]=$value4->nombre;} 

        $registrosRservados2 = RegistroDocumentosDigitales::where('id_tipos_documentos_digitales', 16)->whereNotNull('correlativo')->get();
        
        
        foreach ($registrosRservados2 as $llave => $valor) {
            $correlativosRegistros = CorrelativoDocumentosDigitales::where('correlativo', $valor->correlativo)->first();
            $correlativosRegistro = $correlativosRegistros;
            //dd($correlativosRegistro);
        }
        
        $ahora = Carbon::now()->format('Y-m-d');


        if( $request->FechaDesde != null ) {
            $feDesde=(Carbon::parse($request->get('FechaDesde'))->format('dmY'));
            $feDesde= Carbon::parse(substr($feDesde, 0, 2).'-'.substr($feDesde, 2, 2).'-'.substr($feDesde, 4, 4).' 00:00:00');
        }
        if( $request->FechaHasta != null ) {
            $feHasta=(Carbon::parse($request->get('FechaHasta'))->format('dmY'));
            $feHasta= Carbon::parse(substr($feHasta, 0, 2).'-'.substr($feHasta, 2, 2).'-'.substr($feHasta, 4, 4).' 23:59:59');
        }

        $query = RegistroDocumentosDigitales::query ();
        $query->where('id_tipos_documentos_digitales', 16);
        $query->when(request('correlativo', false), function ($q, $correlativo) {
                return $q->where('correlativo', $correlativo);
        });
        $query->when(request('tramite', false), function ($q, $tramite) {
            return $q->where('id', $tramite);
        });
        $query->when(request('FechaDesde', false), function ($q, $feDesde) {
            return $q->where('created_at','>=', $feDesde);
        });
        $query->when(request('FechaHasta', false), function ($q, $feHasta) {
            return $q->where('created_at','<=', $feHasta);
        });
        $query->when(request('etapa', false), function ($q, $etapa) {
            return $q->where('id_etapas_documentos_digitales', $etapa);
        });
        $query->when(request('categoria', false), function ($q, $categoria) {
            return $q->where('id_categoria', $categoria);
        });
        $query->when(request('lugar', false), function ($q, $lugar) {
            return $q->where('lugar', $lugar);
        });
        $query->when(request('user', false), function ($q, $user) {
            return $q->where('id_user', $user);
        });
        $query->when(request('materia'), function ($q, $materia) {
            return $q->where('titulo', 'LIKE', "%$materia%");
        });
        //$correlativos = $query->orderby('created_at', 'desc')->get(); 
        $correlativos = $query->orderby('updated_at', 'desc')->paginate(30);
   
        return view('documentos_digitales.registro_documento_digitales_memos')
        ->with('registrosRservados' ,$correlativos)
        ->with('feDesde' ,$feDesde)
        ->with('feHasta' ,$feHasta)
        ->with('fechaInDesde' ,$fechaInDesde)
        ->with('fechaInHasta' ,$fechaInHasta)
        ->with('fechas' ,$fechas)
        ->with('categorias' , $categorias)
        ->with('users' , $users)
        ->with('lugares' , $lugares)
        ->with('etapas' , $etapas)
        ->with('correlativo' , $correlativo)
        ->with('tramite' , $tramite)
        ->with('categoria' , $categoria)
        ->with('user' , $user)
        ->with('lugar' , $lugar)
        ->with('etapa' , $etapa)
        ->with('materia' , $materia)
        ->with('correlativosRegistro' , $correlativosRegistro)
        ->with('ahora' , $ahora);
    }

    public function indexDecretosExentos(Request $request)
    {
        $registrosRservados = RegistroDocumentosDigitales::orderby('id', 'desc')->where('id_tipos_documentos_digitales', 4)->get();
        $anno = Carbon::now()->format('Y');
        $fechaStringInicio = ($anno).'-01-01 00:00:00';
        $feDesde = Carbon::parse($fechaStringInicio);
        $feHasta =  Carbon::parse(Carbon::now()->format('Y-m-d').' 23:59:59');
        $fechaInDesde = Carbon::parse($fechaStringInicio);
        $fechaInHasta = Carbon::now();
        $fechas[1] = Date('Y-m-d', strtotime( '1970-01-01' ));
        $fechas[0] = Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
        $correlativo = $request->correlativo;
        $tramite = $request->tramite;
        $categoria = $request->categoria;
        $user = $request->user;
        $lugar = $request->lugar;
        $etapa = $request->etapa;
        $materia = $request->materia;
        $etapas[null] = 'Seleccione Etapa'; foreach (EtapasDocumentosDigitales::get() as $key1 => $value1) { $etapas[$value1->id]=$value1->nombre;} 
        $categorias[null] = 'Seleccione Categoría'; foreach (Categoria::get() as $key2 => $value2) { $categorias[$value2->id]=$value2->nombre;} 
        $users[null] = 'Seleccione Funcionario(a)'; foreach (User::orderby('name', 'asc')->get() as $key3 => $value3) { $users[$value3->id]=$value3->name;} 
        $lugares[null] = 'Seleccione Lugar'; foreach (Lugar::orderby('nombre', 'asc')->get() as $key4 => $value4) { $lugares[$value4->nombre]=$value4->nombre;} 
        

        if( $request->FechaDesde != null ) {
            $feDesde=(Carbon::parse($request->get('FechaDesde'))->format('dmY'));
            $feDesde= Carbon::parse(substr($feDesde, 0, 2).'-'.substr($feDesde, 2, 2).'-'.substr($feDesde, 4, 4).' 00:00:00');
        }
        if( $request->FechaHasta != null ) {
            $feHasta=(Carbon::parse($request->get('FechaHasta'))->format('dmY'));
            $feHasta= Carbon::parse(substr($feHasta, 0, 2).'-'.substr($feHasta, 2, 2).'-'.substr($feHasta, 4, 4).' 23:59:59');
        }

        $query = RegistroDocumentosDigitales::query ();
        $query->where('id_tipos_documentos_digitales', 4);
        $query->when(request('correlativo', false), function ($q, $correlativo) {
                return $q->where('correlativo', $correlativo);
        });
        $query->when(request('tramite', false), function ($q, $tramite) {
            return $q->where('id', $tramite);
        });
        $query->when(request('FechaDesde', false), function ($q, $feDesde) {
            return $q->where('created_at','>=', $feDesde);
        });
        $query->when(request('FechaHasta', false), function ($q, $feHasta) {
            return $q->where('created_at','<=', $feHasta);
        });
        $query->when(request('etapa', false), function ($q, $etapa) {
            return $q->where('id_etapas_documentos_digitales', $etapa);
        });
        $query->when(request('categoria', false), function ($q, $categoria) {
            return $q->where('id_categoria', $categoria);
        });
        $query->when(request('lugar', false), function ($q, $lugar) {
            return $q->where('lugar', $lugar);
        });
        $query->when(request('user', false), function ($q, $user) {
            return $q->where('id_user', $user);
        });
        $query->when(request('materia'), function ($q, $materia) {
            return $q->where('titulo', 'LIKE', "%$materia%");
        });
        //$correlativos = $query->orderby('updated_at', 'desc')->get(); 
        $correlativos = $query->orderby('updated_at', 'desc')->paginate(30);
   
        return view('documentos_digitales.registro_documento_digitales_decretosexentos')
        ->with('registrosRservados' ,$correlativos)
        ->with('feDesde' ,$feDesde)
        ->with('feHasta' ,$feHasta)
        ->with('fechaInDesde' ,$fechaInDesde)
        ->with('fechaInHasta' ,$fechaInHasta)
        ->with('fechas' ,$fechas)
        ->with('categorias' , $categorias)
        ->with('users' , $users)
        ->with('lugares' , $lugares)
        ->with('etapas' , $etapas)
        ->with('correlativo' , $correlativo)
        ->with('tramite' , $tramite)
        ->with('categoria' , $categoria)
        ->with('user' , $user)
        ->with('lugar' , $lugar)
        ->with('etapa' , $etapa)
        ->with('materia' , $materia);
    }

    public function indexDecretosSupremos(Request $request)
    {
        $registrosRservados = RegistroDocumentosDigitales::orderby('id', 'desc')->where('id_tipos_documentos_digitales', 5)->get();
        $anno = Carbon::now()->format('Y');
        $fechaStringInicio = ($anno).'-01-01 00:00:00';
        $feDesde = Carbon::parse($fechaStringInicio);
        $feHasta =  Carbon::parse(Carbon::now()->format('Y-m-d').' 23:59:59');
        $fechaInDesde = Carbon::parse($fechaStringInicio);
        $fechaInHasta = Carbon::now();
        $fechas[1] = Date('Y-m-d', strtotime( '1970-01-01' ));
        $fechas[0] = Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
        $correlativo = $request->correlativo;
        $tramite = $request->tramite;
        $categoria = $request->categoria;
        $user = $request->user;
        $lugar = $request->lugar;
        $etapa = $request->etapa;
        $materia = $request->materia;
        $etapas[null] = 'Seleccione Etapa'; foreach (EtapasDocumentosDigitales::get() as $key1 => $value1) { $etapas[$value1->id]=$value1->nombre;} 
        $categorias[null] = 'Seleccione Categoría'; foreach (Categoria::get() as $key2 => $value2) { $categorias[$value2->id]=$value2->nombre;} 
        $users[null] = 'Seleccione Funcionario(a)'; foreach (User::orderby('name', 'asc')->get() as $key3 => $value3) { $users[$value3->id]=$value3->name;} 
        $lugares[null] = 'Seleccione Lugar'; foreach (Lugar::orderby('nombre', 'asc')->get() as $key4 => $value4) { $lugares[$value4->nombre]=$value4->nombre;} 
        

        if( $request->FechaDesde != null ) {
            $feDesde=(Carbon::parse($request->get('FechaDesde'))->format('dmY'));
            $feDesde= Carbon::parse(substr($feDesde, 0, 2).'-'.substr($feDesde, 2, 2).'-'.substr($feDesde, 4, 4).' 00:00:00');
        }
        if( $request->FechaHasta != null ) {
            $feHasta=(Carbon::parse($request->get('FechaHasta'))->format('dmY'));
            $feHasta= Carbon::parse(substr($feHasta, 0, 2).'-'.substr($feHasta, 2, 2).'-'.substr($feHasta, 4, 4).' 23:59:59');
        }

        $query = RegistroDocumentosDigitales::query ();
        $query->where('id_tipos_documentos_digitales', 5);
        $query->when(request('correlativo', false), function ($q, $correlativo) {
                return $q->where('correlativo', $correlativo);
        });
        $query->when(request('tramite', false), function ($q, $tramite) {
            return $q->where('id', $tramite);
        });
        $query->when(request('FechaDesde', false), function ($q, $feDesde) {
            return $q->where('created_at','>=', $feDesde);
        });
        $query->when(request('FechaHasta', false), function ($q, $feHasta) {
            return $q->where('created_at','<=', $feHasta);
        });
        $query->when(request('etapa', false), function ($q, $etapa) {
            return $q->where('id_etapas_documentos_digitales', $etapa);
        });
        $query->when(request('categoria', false), function ($q, $categoria) {
            return $q->where('id_categoria', $categoria);
        });
        $query->when(request('lugar', false), function ($q, $lugar) {
            return $q->where('lugar', $lugar);
        });
        $query->when(request('user', false), function ($q, $user) {
            return $q->where('id_user', $user);
        });
        $query->when(request('materia'), function ($q, $materia) {
            return $q->where('titulo', 'LIKE', "%$materia%");
        });
        //$correlativos = $query->orderby('updated_at', 'desc')->get(); 
        $correlativos = $query->orderby('updated_at', 'desc')->paginate(30);
   
        return view('documentos_digitales.registro_documento_digitales_decretossupremos')
        ->with('registrosRservados' ,$correlativos)
        ->with('feDesde' ,$feDesde)
        ->with('feHasta' ,$feHasta)
        ->with('fechaInDesde' ,$fechaInDesde)
        ->with('fechaInHasta' ,$fechaInHasta)
        ->with('fechas' ,$fechas)
        ->with('categorias' , $categorias)
        ->with('users' , $users)
        ->with('lugares' , $lugares)
        ->with('etapas' , $etapas)
        ->with('correlativo' , $correlativo)
        ->with('tramite' , $tramite)
        ->with('categoria' , $categoria)
        ->with('user' , $user)
        ->with('lugar' , $lugar)
        ->with('etapa' , $etapa)
        ->with('materia' , $materia);
    }

    public function indexDecretos(Request $request)
    {
        $registrosRservados = RegistroDocumentosDigitales::orderby('id', 'desc')->where('id_tipos_documentos_digitales', 3)->get();
        $anno = Carbon::now()->format('Y');
        $fechaStringInicio = ($anno).'-01-01 00:00:00';
        $feDesde = Carbon::parse($fechaStringInicio);
        $feHasta =  Carbon::parse(Carbon::now()->format('Y-m-d').' 23:59:59');
        $fechaInDesde = Carbon::parse($fechaStringInicio);
        $fechaInHasta = Carbon::now();
        $fechas[1] = Date('Y-m-d', strtotime( '1970-01-01' ));
        $fechas[0] = Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
        $correlativo = $request->correlativo;
        $tramite = $request->tramite;
        $categoria = $request->categoria;
        $user = $request->user;
        $lugar = $request->lugar;
        $etapa = $request->etapa;
        $materia = $request->materia;
        $etapas[null] = 'Seleccione Etapa'; foreach (EtapasDocumentosDigitales::get() as $key1 => $value1) { $etapas[$value1->id]=$value1->nombre;} 
        $categorias[null] = 'Seleccione Categoría'; foreach (Categoria::get() as $key2 => $value2) { $categorias[$value2->id]=$value2->nombre;} 
        $users[null] = 'Seleccione Funcionario(a)'; foreach (User::orderby('name', 'asc')->get() as $key3 => $value3) { $users[$value3->id]=$value3->name;} 
        $lugares[null] = 'Seleccione Lugar'; foreach (Lugar::orderby('nombre', 'asc')->get() as $key4 => $value4) { $lugares[$value4->nombre]=$value4->nombre;} 
       

        if( $request->FechaDesde != null ) {
            $feDesde=(Carbon::parse($request->get('FechaDesde'))->format('dmY'));
            $feDesde= Carbon::parse(substr($feDesde, 0, 2).'-'.substr($feDesde, 2, 2).'-'.substr($feDesde, 4, 4).' 00:00:00');
        }
        if( $request->FechaHasta != null ) {
            $feHasta=(Carbon::parse($request->get('FechaHasta'))->format('dmY'));
            $feHasta= Carbon::parse(substr($feHasta, 0, 2).'-'.substr($feHasta, 2, 2).'-'.substr($feHasta, 4, 4).' 23:59:59');
        }

        $query = RegistroDocumentosDigitales::query ();
        $query->where('id_tipos_documentos_digitales', 3);
        $query->when(request('correlativo', false), function ($q, $correlativo) {
                return $q->where('correlativo', $correlativo);
        });
        $query->when(request('tramite', false), function ($q, $tramite) {
            return $q->where('id', $tramite);
        });
        $query->when(request('FechaDesde', false), function ($q, $feDesde) {
            return $q->where('created_at','>=', $feDesde);
        });
        $query->when(request('FechaHasta', false), function ($q, $feHasta) {
            return $q->where('created_at','<=', $feHasta);
        });
        $query->when(request('etapa', false), function ($q, $etapa) {
            return $q->where('id_etapas_documentos_digitales', $etapa);
        });
        $query->when(request('categoria', false), function ($q, $categoria) {
            return $q->where('id_categoria', $categoria);
        });
        $query->when(request('lugar', false), function ($q, $lugar) {
            return $q->where('lugar', $lugar);
        });
        $query->when(request('user', false), function ($q, $user) {
            return $q->where('id_user', $user);
        });
        $query->when(request('materia'), function ($q, $materia) {
            return $q->where('titulo', 'LIKE', "%$materia%");
        });
        //$correlativos = $query->orderby('updated_at', 'desc')->get(); 
        $correlativos = $query->orderby('updated_at', 'desc')->paginate(30);
   
        return view('documentos_digitales.registro_documento_digitales_decretos')
        ->with('registrosRservados' ,$correlativos)
        ->with('feDesde' ,$feDesde)
        ->with('feHasta' ,$feHasta)
        ->with('fechaInDesde' ,$fechaInDesde)
        ->with('fechaInHasta' ,$fechaInHasta)
        ->with('fechas' ,$fechas)
        ->with('categorias' , $categorias)
        ->with('users' , $users)
        ->with('lugares' , $lugares)
        ->with('etapas' , $etapas)
        ->with('correlativo' , $correlativo)
        ->with('tramite' , $tramite)
        ->with('categoria' , $categoria)
        ->with('user' , $user)
        ->with('lugar' , $lugar)
        ->with('etapa' , $etapa)
        ->with('materia' , $materia);
    }

    public function indexlog(Request $request)
    {
          
        $log = Audits::orderby('auditable_id', 'asc')
                     ->where('event', 'updated')
                     ->whereIn('old_values', ['{"correlativo":null}', '{"estatus":0}'])
                     ->whereIn('auditable_type', ['App\Models\DocumentosDigitales\RegistroDocumentosDigitales', 'App\Models\DocumentosDigitales\ListadoFirmantesRegistroDocumentosDigitales'])
                     ->get();  

        return view('documentos_digitales.registro_documento_digitales_log')
        ->with('log' ,$log);
    }

    public function detallederivacionesofi(Request $request, $id)
    {
        $derivaciones = DerivacionRegistroDocumentosDigitales::where('id_registro_documentos_digitales', $id)->get();
        
        return view('documentos_digitales.registro_documento_digitales_detalles_derivaciones_oficios')
        ->with('derivaciones' , $derivaciones);
    }

    public function detallederivacionesresex(Request $request, $id)
    {
        $derivaciones = DerivacionRegistroDocumentosDigitales::where('id_registro_documentos_digitales', $id)->get();
        
        return view('documentos_digitales.registro_documento_digitales_detalles_derivaciones_resoluciones')
        ->with('derivaciones' , $derivaciones);
    }

    public function detallederivacionesmemo(Request $request, $id)
    {
        $derivaciones = DerivacionRegistroDocumentosDigitales::where('id_registro_documentos_digitales', $id)->get();
        
        return view('documentos_digitales.registro_documento_digitales_detalles_derivaciones_memos')
        ->with('derivaciones' , $derivaciones);
    }

    public function detallederivacionesdecretoexento(Request $request, $id)
    {
        $derivaciones = DerivacionRegistroDocumentosDigitales::where('id_registro_documentos_digitales', $id)->get();
        
        return view('documentos_digitales.registro_documento_digitales_detalles_derivaciones_decretoexento')
        ->with('derivaciones' , $derivaciones);
    }

    public function detallederivacionesdecretosupremo(Request $request, $id)
    {
        $derivaciones = DerivacionRegistroDocumentosDigitales::where('id_registro_documentos_digitales', $id)->get();
        
        return view('documentos_digitales.registro_documento_digitales_detalles_derivaciones_decretosupremo')
        ->with('derivaciones' , $derivaciones);
    }

    public function detallederivacionesdecreto(Request $request, $id)
    {
        $derivaciones = DerivacionRegistroDocumentosDigitales::where('id_registro_documentos_digitales', $id)->get();
        
        return view('documentos_digitales.registro_documento_digitales_detalles_derivaciones_decreto')
        ->with('derivaciones' , $derivaciones);
    }

    public function create ($idTipoDcoumento)
    {
        $varNull = null;
        $selectTipo[null] = 'Seleccione Tipo de Documento'; foreach (TiposDocumentosDigitales::orderby('id', 'asc')->get() as $key1 => $value1) { $selectTipo[$value1->id]=$value1->nombre;} 
        $selectUsuariosid[null] = 'Seleccione Destinatario(s)'; foreach (User::orderby('name', 'asc')->get() as $key2 => $value2) { $selectUsuariosid[$value2->id]=$value2->name;}
        $selectFirmantes[null] = 'Seleccione Firmante'; foreach (Firmantes::orderby('nombre', 'asc')->get() as $key3 => $value3) { $selectFirmantes[$value3->id_user]=$value3->nombres;}
        $selectFirmantesS[null] = 'Seleccione Firmante Subrogante'; foreach (FirmantesSub::orderby('nombre', 'asc')->get() as $key3 => $value3) { $selectFirmantesS[$value3->id_user]=$value3->nombress;}
        $selectLugar[null] = 'Seleccione Lugar'; foreach (Lugar::orderby('id', 'asc')->get() as $key5 => $value5) { $selectLugar[$value5->id]=$value5->nombre;} 
        $selectDestinatariosM[null] = 'Seleccione Destinatario Memo'; foreach (User::orderby('name', 'asc')->get() as $key6 => $value6) { $selectDestinatariosM[$value6->email]=$value6->name;}
        $usuarioIdStd = Auth::user()->id;
        
        $ahora = Carbon::now()->format('Y-m-d');
        $usuariofirmante = Firmantes::where('id_user', $usuarioIdStd)->first();
        $usuariofirmante2 = FirmantesSub::where('id_user', $usuarioIdStd)->first();
        if ($usuariofirmante == null && $usuariofirmante2 == null){
            $selectFoliosR[null] = 'Seleccione Folio Apartado'; foreach (CorrelativoDocumentosDigitales::where('tipo_documento_id', $idTipoDcoumento)->whereRaw('SUBSTRING(created_at, 1, 10) = '."'".$ahora."'")->where('estatus', 'APARTADO')->orderby('correlativo', 'asc')->get() as $key6 => $value6) { $selectFoliosR[$value6->correlativo]=$value6->nombrec;} 
        }
        if ($usuariofirmante != null) {
            $lugar = $usuariofirmante->lugar;
            if($lugar == 'Santiago'){
                $selectFoliosR[null] = 'Seleccione Folio Apartado'; foreach (CorrelativoDocumentosDigitales::where('tipo_documento_id', $idTipoDcoumento)->whereRaw('SUBSTRING(created_at, 1, 10) ='."'".$ahora."'")->where('estatus', 'APARTADO')->where('lugar', $lugar)->orderby('correlativo', 'asc')->get() as $key6 => $value6) { $selectFoliosR[$value6->correlativo]=$value6->nombrecc;}
            }else{
                $selectFoliosR[null] = 'Seleccione Folio Apartado'; foreach (CorrelativoDocumentosDigitales::where('tipo_documento_id', $idTipoDcoumento)->whereRaw('SUBSTRING(created_at, 1, 10) ='."'".$ahora."'")->where('estatus', 'APARTADO')->where('lugar', $lugar)->orderby('correlativo', 'asc')->get() as $key6 => $value6) { $selectFoliosR[$value6->correlativo]=$value6->nombrec;}
            }     
        }
        if ($usuariofirmante2 != null){
            $lugar = $usuariofirmante2->lugar;
            if($lugar == 'Santiago'){
                $selectFoliosR[null] = 'Seleccione Folio Apartado'; foreach (CorrelativoDocumentosDigitales::where('tipo_documento_id', $idTipoDcoumento)->whereRaw('SUBSTRING(created_at, 1, 10) ='."'".$ahora."'")->where('estatus', 'APARTADO')->where('lugar', $lugar)->orderby('correlativo', 'asc')->get() as $key6 => $value6) { $selectFoliosR[$value6->correlativo]=$value6->nombrecc;}
            }else{
                $selectFoliosR[null] = 'Seleccione Folio Apartado'; foreach (CorrelativoDocumentosDigitales::where('tipo_documento_id', $idTipoDcoumento)->whereRaw('SUBSTRING(created_at, 1, 10) ='."'".$ahora."'")->where('estatus', 'APARTADO')->where('lugar', $lugar)->orderby('correlativo', 'asc')->get() as $key6 => $value6) { $selectFoliosR[$value6->correlativo]=$value6->nombrec;}
            } 
        }

        
        $selectUsuarios = [];
        foreach (PyR::get() as $key1 => $value1){$selectUsuarios[$value1->email]=$value1->nombres.' '.$value1->apellido_paterno.' '.$value1->apellido_materno.' '.$value1->unidad_desempeno.' '.$value1->email; }        
        
        /*$categoriaDoc[NULL] = 'Seleccione Categoría';*/ 
        $categoriaDoc = [];
        foreach (Categoria::orderby('id', 'asc')->get() as $key4 => $value4) { $categoriaDoc[$value4->id]=$value4->nombre;} 
        

        return view('documentos_digitales.registro_documento_digitales_crear')
        ->with('varNull' , $varNull)
        ->with('selectTipo' , $selectTipo)
        ->with('selectUsuariosid' , $selectUsuariosid)
        ->with('selectFirmantes' , $selectFirmantes)
        ->with('selectFirmantesS' , $selectFirmantesS)
        ->with('selectUsuarios' , $selectUsuarios)
        ->with('categoriaDoc' , $categoriaDoc)
        ->with('selectLugar' , $selectLugar)
        ->with('selectFoliosR' , $selectFoliosR)
        ->with('idTipoDcoumento' ,$idTipoDcoumento)
        ->with('selectDestinatariosM' , $selectDestinatariosM)
        ;
    }

    public function store(RegistroDocumentoRequest $request)
    {
        $nombreDocVerificacionFirma = substr(md5(mt_rand()), 0, 32);
        $code = substr(md5(mt_rand()), 0, 8);

        $tipoDoc = $request->id_tipos_documentos_digitales;
        $fR = $request->fR;
        $foliosR = $request->foliosR;
        $nMemo = $request->n_memo;
        $memo =  $request->memo;
        $CateDoc = $request->id_categoria;
        $reservadoDistribucion = $request->reservado_distribucion;
        $titulo = $request->titulo;
        $descripcion = $request->descripcion;
        $destinatario_memo = $request->destinatario_memo;
        $derivar =  $request->derivar;
        $estatus = 1;
        $documento = $request->filename;
        $lugarId = $request->lugar;
        $distribuye = $request->distribuye;
        $correosDistri = $request->correos_internos;

       // $correosDistri = $request->all('correos_internos');

        //dd($correosDistri);


        $lugar = Lugar::where('id', $lugarId)->first();

        $nuevoRegistro = new RegistroDocumentosDigitales();
        
        $nuevoRegistro->id_tipos_documentos_digitales = $tipoDoc;
        $nuevoRegistro->id_categoria = $CateDoc;
        $nuevoRegistro->reservado_distribucion = $reservadoDistribucion;
        $nuevoRegistro->titulo = $titulo;
        $nuevoRegistro->descripcion = $descripcion;

        if ($destinatario_memo) {
            $nuevoRegistro->destinatario_memo = $destinatario_memo;
        }

        if($distribuye == 1){ 
            $nuevoRegistro->distribuye = $distribuye;
            if($correosDistri){
                $nuevoRegistro->correos_distri = json_encode($correosDistri);
            }
        }
        

        $nuevoRegistro->id_user = Auth::user()->id;
        $nuevoRegistro->estatus = $estatus;
        $nuevoRegistro->lugar = $lugar->nombre;
        $nuevoRegistro->code = $code;
        $nuevoRegistro->hash_autenticador = $nombreDocVerificacionFirma;
        $nuevoRegistro->save();


        if ($fR == 1) {
            $nuevoRegistro->correlativo = $foliosR;

            $editRegistroReserva = CorrelativoDocumentosDigitales::where('correlativo' , $nuevoRegistro->correlativo)->first();
            $editRegistroReserva->id_registro = $nuevoRegistro->id;
            $editRegistroReserva->estatus = 'OCUPADO';
            $editRegistroReserva->tipo_documento_id = $nuevoRegistro->id_tipos_documentos_digitales;
            $editRegistroReserva->materia = $nuevoRegistro->titulo;
            $editRegistroReserva->lugar = $nuevoRegistro->lugar;

            if($nuevoRegistro->id_categoria != 1){
                $editRegistroReserva->reservado_distribucion = $nuevoRegistro->reservado_distribucion;
            }

            $editRegistroReserva->save();
        }

        

        if ($memo == 1) {
            $nuevoRegistro->n_memo = $nMemo;
        }

        if ($derivar == 0) {
            $nuevoRegistro->id_etapas_documentos_digitales = EtapasDocumentosDigitales::where('nombre' , 'En Proceso Firma')->first()->id;
            $nuevoRegistro->derivacion = 0;
        }
        if ($derivar == 1) {
            $nuevoRegistro->id_etapas_documentos_digitales = EtapasDocumentosDigitales::where('nombre' , 'Colaboración')->first()->id;
            $nuevoRegistro->derivacion = 1;
        }
        if ($documento) {
            $nuevoRegistro->filename = $request->filename->getClientOriginalName();
            $path = $documento->store(null, 'nofirmadosNew');
            $url_documento_no_firmado = public_path().'/nofirmadosNew/'.$path;
            $url_documento_no_firmado_vista = '/nofirmadosNew/'.$path;
            $nuevoRegistro->url_s3 = $url_documento_no_firmado;
            $nuevoRegistro->url_vista = $url_documento_no_firmado_vista;                    
            } 
        $nuevoRegistro->save();
        

        

        
        $firmantes = $request->id_user_firmante;
        /* $sub = $request->sub;
        if ($sub == 1) {
            if (!$request->id_user_firmante_s){
                return redirect()->back()->withErrors(['El Campos de Seleccionar Firmante es requerido.']);
            }
            $firmantes = $request->id_user_firmante_s;
        } */
        $estatus2 = 0;
        $nuevoFirmante = new ListadoFirmantesRegistroDocumentosDigitales();
        $nuevoFirmante->id_registro_documento_digitales = $nuevoRegistro->id;
        $nuevoFirmante->id_user_firmante = $firmantes;
        $nuevoFirmante->id_user_crea_tabla = Auth::user()->id;
        $nuevoFirmante->estatus = $estatus2;
        $nuevoFirmante->sub = 0;
        /* if ($sub == 0) {
            $nuevoFirmante->sub = 0;
        }
        if ($sub == 1) {
            $nuevoFirmante->sub = 1;
        } */
        $nuevoFirmante->save();
        
        if ($derivar == 1) {
        $userDestino = $request->id_user_destino;
        $estatus4 = 0;
        foreach ($userDestino as $llave => $valor) {
            
            $nuevoUserDestino = new VistosBuenos();
            $nuevoUserDestino->id_user = $valor;
            if($nuevoUserDestino){
                $datosUsuarios = user::where('id', $nuevoUserDestino->id_user)->first();
                $nuevoUserDestino->email = $datosUsuarios->email;
                $nuevoUserDestino->nombre = $datosUsuarios->name;
            }
            $nuevoUserDestino->id_registro_documento_digitales = $nuevoRegistro->id;
            $nuevoUserDestino->estatus = $estatus4;
            $nuevoUserDestino->save();
            }
        
        $estatus4 = 1;
        $userDestino = $request->id_user_destino;
        $observacion = $request->observacion;
        $usuarioDestino = $request->id_user_destino;
        $nuevaDerivacion = new DerivacionRegistroDocumentosDigitales();
        $nuevaDerivacion->id_registro_documentos_digitales = $nuevoRegistro->id;
        $nuevaDerivacion->id_estatus_derivacion_registro_documentos_digitales = $estatus4;
        $nuevaDerivacion->id_user_emisor = Auth::user()->id;
        if($usuarioDestino){
           $estatusVB =  VistosBuenos::where('id_registro_documento_digitales', $nuevoRegistro->id)->where('estatus', 0)->first();
           $nuevaDerivacion->id_user_destino = $estatusVB->id_user;
        }
        $nuevaDerivacion->id_user_generador = $nuevoRegistro->id_user;
        $nuevaDerivacion->observacion = $observacion;
        $nuevaDerivacion->save(); 

        $estatus3 = 1;
        $documentoW = $request->filename2;
        $nuevoWord = new AnexosDocumentosDigitales();
        $nuevoWord->id_registro_documentos_digitales = $nuevoRegistro->id;
        $nuevoWord->id_derivacion = $nuevaDerivacion->id;
        $nuevoWord->estatus = $estatus3;
        if ($documentoW) {
            $nuevoWord->filename2 = $request->filename2->getClientOriginalName();
            $upload = Storage::disk('s3_3')->put('word-editable', $documentoW, 'public');
            $nuevoWord->url_s3 = 'https://documentos-digitales-std.s3.amazonaws.com/'.$upload;
            } 
        $nuevoWord->save();
        }

        if ($derivar == 1){
            $this->notificacionRegistroColaboracion($nuevoRegistro);
          }else{
            $this->notificacionRegistroFirma($nuevoRegistro);
          }

        $url = $request->input('url');
        alert()->success('!','Registro creado exitosamente');
        return redirect($url)->with('success', 'Registro creado exitosamente');
    }

    public function firmaspendientes(Request $request)
    {
        $codigoCU = $request->code;
        $stateCU = $request->state;
        $redirect = env('APP_ENV') == 'local' ? env('APP_URL') . env('CLAVEUNICA_REDIRECT') : secure_url(env('APP_URL') . env('CLAVEUNICA_REDIRECT'));
          $client = new Client();
            try {
              $request = $client->post("https://accounts.claveunica.gob.cl/openid/token", [
                'form_params' => [
                    'client_id' => env('CLAVEUNICA_CLIENT_ID'),
                    'client_secret' => env('CLAVEUNICA_SECRET_ID'),
                    'redirect_uri' => $redirect,
                    'grant_type' => 'authorization_code',
                    'code' =>  $codigoCU,
                    'state' =>  $stateCU
                ]
            ]);

            $response = $request->getBody()->getContents();
            $data =  json_decode($response);
           }catch (BadResponseException   $e) {
                    $response = $e->getResponse();
            }
        $access_token = $data->access_token;

        try {
        $client2 = new Client();
        $request2 = $client2->post('https://www.claveunica.gob.cl/openid/userinfo', [
            'headers' => [
                'Authorization' => 'Bearer '.$access_token,
            ],
        ]);
        $response2 = $request2->getBody()->getContents();
        $data2 =  json_decode($response2);
         }catch (BadResponseException   $e) {
            $response = $e->getResponse();
        }

        $correoElectronico = $data2->email;
        $rut = $data2->RolUnico->numero;
        $digitoVerificador = $data2->RolUnico->DV;
        //$nombre = $data2->name->nombres[0]." ".$data2->name->nombres[1]." ".$data2->name->apellidos[0]." ".$data2->name->apellidos[1];

        $dataDefinitiva = [];
        $idUsuario = Auth::user()->id;
        $rutFirmanteInterno = Firmantes::where('id_user', $idUsuario)->first();

        if (!$rutFirmanteInterno){
            return redirect()->to('/Inicio')->withErrors(['Estimado(a) usted no es un(a) Firmante']); 
        }else{
            if($rut == $rutFirmanteInterno->rut){
                $dataDefinitiva = ListadoFirmantesRegistroDocumentosDigitales::orderby('id_registro_documento_digitales', 'desc')->where('id_user_firmante' , $idUsuario)->where('estatus' , '0')->get();
                

                return view('documentos_digitales.firmas_pendientes')
                ->with('dataDefinitiva' , $dataDefinitiva);
            }else{
                return redirect()->to('/Inicio')->withErrors(['Estimado(a) el RUT Firmante no coincide con el RUT de Clave Única']); 
            }
        }
       
        
    }

    public function firmaspendientesdetalles($id)
    {
        $detallesFirmar = RegistroDocumentosDigitales::where('id' , $id)->first();
        $idUsuario = Auth::user()->id;
        $statusfirmante = ListadoFirmantesRegistroDocumentosDigitales::where('id_registro_documento_digitales', $detallesFirmar->id)->where('deleted_at', null)->where('id_user_firmante', $idUsuario)->first();

        return view('documentos_digitales.firmas_pendientes_detalles')
        ->with('detallesFirmar' , $detallesFirmar)
        ->with('idUsuario' , $idUsuario)
        ->with('statusfirmante' , $statusfirmante);

    }

    public function firmando(Request $request, $id){

        $qr = \QrCode::format('png')
        ->size(200)->errorCorrection('H')
        ->generate('https://std.mindep.cl/DocumentosDigitales/ValidarDocumentos');
        $output_file = time() . '.png';
        Storage::disk('imgQR')->put($output_file, $qr); //storage/app/public/img/qr-code/img-1557309130.png
 

        $statusfirmantes = ListadoFirmantesRegistroDocumentosDigitales::where('id' , $id)->first();
        $detallesFirmar = RegistroDocumentosDigitales::where('id' , $statusfirmantes->id_registro_documento_digitales)->first();

        $idRegistrado = CorrelativoDocumentosDigitales::where('id_registro' , $detallesFirmar->id)->where('tipo_documento_id',$detallesFirmar->id_tipos_documentos_digitales)->where('ano' , date("Y"))->first();
        DB::beginTransaction();
        try {
        if (!$idRegistrado){
            $correlativo = CorrelativoDocumentosDigitales::where('tipo_documento_id',$detallesFirmar->id_tipos_documentos_digitales)->where('ano' , date("Y"))->get()->max('correlativo');
            $correlativo = $correlativo+1;
            $correlativon = new CorrelativoDocumentosDigitales();
            $correlativon->ano = date("Y");
            $correlativon->tipo_documento_id = $detallesFirmar->id_tipos_documentos_digitales;
            $correlativon->id_registro = $detallesFirmar->id;
            $correlativon->reservado_distribucion = $detallesFirmar->reservado_distribucion;
            $correlativon->materia = $detallesFirmar->titulo;
            if($detallesFirmar->id_tipos_documentos_digitales == 16){
                $correlativon->tipo_distribucion = 1;
            }
            $correlativon->lugar = $detallesFirmar->lugar;
            $correlativon->correlativo = $correlativo;
            $correlativon->estatus = 'OCUPADO';
            $correlativon->save();
        }else{
            $correlativo = $idRegistrado->correlativo;
        }



            
            $correlativoConCeros = str_pad($correlativo, 3, "0", STR_PAD_LEFT);

            
            $detallesFirmar->correlativo = $correlativo;
            $detallesFirmar->save();


//OFI
        if($detallesFirmar->id_tipos_documentos_digitales == 6){
            if (!$idRegistrado){
                $mes1 = Carbon::now();
                $mesStringCorto = $mes1->format('M');
                $dia = $mes1->format('d');
                $ano = $mes1->format('Y');
                $mes = new CalendarioController();
                $mes = $mes->spanishMonth($mesStringCorto);
                if ($mes == 'Nov') {
                    $mes = 'Noviembre';
                }
                $fecha = $dia.' de '.$mes.' de '.$ano;
            }else{
                 $mes1 = Carbon::parse ($idRegistrado->created_at);
                 $mesStringCorto = $mes1->format('M');
                 $dia = $mes1->format('d');
                 $ano = $mes1->format('Y');
                 $mes = new CalendarioController();
                 $mes = $mes->spanishMonth($mesStringCorto);
                 if ($mes == 'Nov') {
                    $mes = 'Noviembre';
                }
                 $fecha = $dia.' de '.$mes.' de '.$ano;
            }
           

            // initiate FPDI
            $pdf = new FPDI();

            // get the page count
            $pageCount = $pdf->setSourceFile($detallesFirmar->url_s3);
            $pdf->setPrintHeader(false);
            $pdf->setPrintFooter(true);
            // iterate through all pages
            for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
                // import a page
                $templateId = $pdf->importPage($pageNo);
                // get the size of the imported page
                $size = $pdf->getTemplateSize($templateId);
                $pdf->setFooterData(array(0,64,0), array(255, 255, 255));
            
               //$pdf->setFooterData('RESOLUCIÓN EXENTA ELECTRÓNICA FIRMADA CON FIRMA AVANZADA');
                $pdf->SetFooterMargin(45);
                $pdf->SetFont('','',7); 
                
                // create a page (landscape or portrait depending on the imported page size)
                if ($size['width'] > $size['height']) {
                    $pdf->AddPage('L', array($size['width'], $size['height']));
                } else {
                    $pdf->AddPage('P', array($size['width'], $size['height']));
                }

                // use the imported page
                $pdf->useTemplate($templateId);
               
                $pdf->Image('img/logo-firmagob.gif', 161, 314, 15, 15, '', '', '', false, 300, '', false, false, false, false, false, false);
                $pdf->Image('imgQR/'.$output_file, 133, 314, 15, 15, '', '', '', false, 300, '', false, false, false, false, false, false);

                $html = '
               
                <table>
                <tbody align="center">
                <tr>                    
                    <td style="width:300px">
                        <table align="left">
                            <tbody>
                                <tr>
                                    <td style="width:90px"><b> NOMBRE</b></td>
                                    <td style="width:200px">'.' '.$statusfirmantes->username->nombre.'</td>
                                </tr>
                                <tr>
                                    <td style="width:90px"><b> DIVISIÓN / ÁREA</b></td>
                                    <td style="width:200px">'.' '.$statusfirmantes->username->division.'</td>
                                </tr>
                                <tr>
                                    <td style="width:90px"><b> FECHA</b></td>
                                    <td style="width:200px">'.' '.Carbon::now()->format("d-m-Y").'</td>
                                </tr>
                                <tr>
                                    <td style="width:90px"><b> ID VALIDACIÓN</b></td>
                                    <td style="width:200px">'.' '.$detallesFirmar->code.'</td>
                                </tr>
                                <tr>
                                    <td style="width:90px"><b> CÓDIGO VALIDACIÓN</b></td>
                                    <td style="width:200px">'.' '.$detallesFirmar->hash_autenticador.'</td>
                                </tr>
                                <tr>
                                    <td style="width:90px"><b> URL VALIDACIÓN</b></td>
                                    <td style="width:200px"> https://std.mindep.cl/DocumentosDigitales/ValidarDocumentos</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>                  
                    <td style="width:70px"></td>
                    <td style="width:70px"></td>
                </tr>
                </tbody>
                </table>
               ';

                $pdf->SetXY(20, 314);
                $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                //$pdf->SetFont('Helvetica');
                if ($pageNo == 1) {
                    $pdf->SetFont('','B',11); 
                    $pdf->SetXY(99, 27);
                    $pdf->Write(0, 'OFICIO ELECTRÓNICO N° '.$correlativoConCeros);
                    $pdf->SetXY(99, 32);
                    $pdf->Write(0, $detallesFirmar->lugar.', '.$fecha);
                }
                
            }
        
     
        $urlCorrelativo = str_replace('.pdf', '_ConCorrelativo.pdf', $detallesFirmar->url_s3);
        $detallesFirmar->url_correlativo = $urlCorrelativo;
        $detallesFirmar->save();

        // Output the new PDF
        $pdf->Output($urlCorrelativo, 'F');
        }

//RESEX
        if($detallesFirmar->id_tipos_documentos_digitales == 2){
            $contarMateria = strlen($detallesFirmar->titulo); 
            $rlineas = round(($contarMateria/38), 0);
           // dd($rlineas);
            $espacioarriba = 35;
            $linea = 8;
            $espaciosporlineas = ($rlineas*$linea);
            $pxY = $espacioarriba+$espaciosporlineas;
            $pxY2 = $espacioarriba+$espaciosporlineas+5;
            

            if (!$idRegistrado){
                $mes1 = Carbon::now();
                $mesStringCorto = $mes1->format('M');
                $dia = $mes1->format('d');
                $ano = $mes1->format('Y');
                $mes = new CalendarioController();
                $mes = $mes->spanishMonth($mesStringCorto);
                if ($mes == 'Nov') {
                    $mes = 'Noviembre';
                }
                $fecha = $dia.' de '.$mes.' de '.$ano;
            }else{
                 $mes1 = Carbon::parse ($idRegistrado->created_at);
                 $mesStringCorto = $mes1->format('M');
                 $dia = $mes1->format('d');
                 $ano = $mes1->format('Y');
                 $mes = new CalendarioController();
                 $mes = $mes->spanishMonth($mesStringCorto);
                 if ($mes == 'Nov') {
                    $mes = 'Noviembre';
                }
                 $fecha = $dia.' de '.$mes.' de '.$ano;
            }




            // initiate FPDI
            $pdf = new FPDI();

            // get the page count
            $pageCount = $pdf->setSourceFile($detallesFirmar->url_s3);
            $pdf->setPrintHeader(false);
            $pdf->setPrintFooter(true);
            // iterate through all pages
            for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
                // import a page
                $templateId = $pdf->importPage($pageNo);
                // get the size of the imported page
                $size = $pdf->getTemplateSize($templateId);
                $pdf->setFooterData(array(0,64,0), array(255, 255, 255));
            
               //$pdf->setFooterData('RESOLUCIÓN EXENTA ELECTRÓNICA FIRMADA CON FIRMA AVANZADA');
                $pdf->SetFooterMargin(45);
                $pdf->SetFont('','',7); 
                
                // create a page (landscape or portrait depending on the imported page size)
                if ($size['width'] > $size['height']) {
                    $pdf->AddPage('L', array($size['width'], $size['height']));
                } else {
                    $pdf->AddPage('P', array($size['width'], $size['height']));
                }

                // use the imported page
                $pdf->useTemplate($templateId);
               
                $pdf->Image('img/logo-firmagob.gif', 161, 314, 15, 15, '', '', '', false, 300, '', false, false, false, false, false, false);
                $pdf->Image('imgQR/'.$output_file, 133, 314, 15, 15, '', '', '', false, 300, '', false, false, false, false, false, false);

                $html = '
                
                <table>
                <tbody align="center">
                <tr>                    
                    <td style="width:300px">
                        <table align="left">
                            <tbody>
                                <tr>
                                    <td style="width:90px"><b> NOMBRE</b></td>
                                    <td style="width:200px">'.' '.$statusfirmantes->username->nombre.'</td>
                                </tr>
                                <tr>
                                    <td style="width:90px"><b> DIVISIÓN / ÁREA</b></td>
                                    <td style="width:200px">'.' '.$statusfirmantes->username->division.'</td>
                                </tr>
                                <tr>
                                    <td style="width:90px"><b> FECHA</b></td>
                                    <td style="width:200px">'.' '.Carbon::now()->format("d-m-Y").'</td>
                                </tr>
                                <tr>
                                    <td style="width:90px"><b> ID VALIDACIÓN</b></td>
                                    <td style="width:200px">'.' '.$detallesFirmar->code.'</td>
                                </tr>
                                <tr>
                                    <td style="width:90px"><b> CÓDIGO VALIDACIÓN</b></td>
                                    <td style="width:200px">'.' '.$detallesFirmar->hash_autenticador.'</td>
                                </tr>
                                <tr>
                                    <td style="width:90px"><b> URL VALIDACIÓN</b></td>
                                    <td style="width:200px"> https://std.mindep.cl/DocumentosDigitales/ValidarDocumentos</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>                  
                    <td style="width:80px"></td>
                    <td style="width:80px"></td>
                </tr>
                </tbody>
                </table>
               ';

                $pdf->SetXY(20, 314);
                $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                //$pdf->SetFont('Helvetica');
                if ($pageNo == 1) {
                    $pdf->SetFont('','B',11); 
                    $pdf->SetXY(99, 27);
                    $pdf->Write(0, 'RESOLUCIÓN EXENTA ELECTRÓNICA N° '.$correlativoConCeros);
                    $pdf->SetXY(99, 32);
                    $pdf->Write(0, $detallesFirmar->lugar.', '.$fecha);                    
                }
                
            }
        
     
        $urlCorrelativo = str_replace('.pdf', '_ConCorrelativo.pdf', $detallesFirmar->url_s3);
        $detallesFirmar->url_correlativo = $urlCorrelativo;
        $detallesFirmar->save();

        // Output the new PDF
        $pdf->Output($urlCorrelativo, 'F');
        }



//DECRETOEXENTO
        if($detallesFirmar->id_tipos_documentos_digitales == 4){
            $contarMateria = strlen($detallesFirmar->titulo); 
            $rlineas = round(($contarMateria/38), 0);
        // dd($rlineas);
            $espacioarriba = 35;
            $linea = 8;
            $espaciosporlineas = ($rlineas*$linea);
            $pxY = $espacioarriba+$espaciosporlineas;
            $pxY2 = $espacioarriba+$espaciosporlineas+5;
            

            if (!$idRegistrado){
                $mes1 = Carbon::now();
                $mesStringCorto = $mes1->format('M');
                $dia = $mes1->format('d');
                $ano = $mes1->format('Y');
                $mes = new CalendarioController();
                $mes = $mes->spanishMonth($mesStringCorto);
                if ($mes == 'Nov') {
                    $mes = 'Noviembre';
                }
                $fecha = $dia.' de '.$mes.' de '.$ano;
            }else{
                $mes1 = Carbon::parse ($idRegistrado->created_at);
                $mesStringCorto = $mes1->format('M');
                $dia = $mes1->format('d');
                $ano = $mes1->format('Y');
                $mes = new CalendarioController();
                $mes = $mes->spanishMonth($mesStringCorto);
                if ($mes == 'Nov') {
                    $mes = 'Noviembre';
                }
                $fecha = $dia.' de '.$mes.' de '.$ano;
            }




            // initiate FPDI
            $pdf = new FPDI();

            // get the page count
            $pageCount = $pdf->setSourceFile($detallesFirmar->url_s3);
            $pdf->setPrintHeader(false);
            $pdf->setPrintFooter(true);
            // iterate through all pages
            for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
                // import a page
                $templateId = $pdf->importPage($pageNo);
                // get the size of the imported page
                $size = $pdf->getTemplateSize($templateId);
                $pdf->setFooterData(array(0,64,0), array(255, 255, 255));
            
            //$pdf->setFooterData('DECRETO EXENTO ELECTRÓNICO FIRMADO CON FIRMA AVANZADA');
                $pdf->SetFooterMargin(45);
                $pdf->SetFont('','',7); 
                
                // create a page (landscape or portrait depending on the imported page size)
                if ($size['width'] > $size['height']) {
                    $pdf->AddPage('L', array($size['width'], $size['height']));
                } else {
                    $pdf->AddPage('P', array($size['width'], $size['height']));
                }

                // use the imported page
                $pdf->useTemplate($templateId);
            
                $pdf->Image('img/logo-firmagob.gif', 161, 314, 15, 15, '', '', '', false, 300, '', false, false, false, false, false, false);
                $pdf->Image('imgQR/'.$output_file, 133, 314, 15, 15, '', '', '', false, 300, '', false, false, false, false, false, false);

                $html = '
                
                <table>
                <tbody align="center">
                <tr>                    
                    <td style="width:300px">
                        <table align="left">
                            <tbody>
                                <tr>
                                    <td style="width:90px"><b> NOMBRE</b></td>
                                    <td style="width:200px">'.' '.$statusfirmantes->username->nombre.'</td>
                                </tr>
                                <tr>
                                    <td style="width:90px"><b> DIVISIÓN / ÁREA</b></td>
                                    <td style="width:200px">'.' '.$statusfirmantes->username->division.'</td>
                                </tr>
                                <tr>
                                    <td style="width:90px"><b> FECHA</b></td>
                                    <td style="width:200px">'.' '.Carbon::now()->format("d-m-Y").'</td>
                                </tr>
                                <tr>
                                    <td style="width:90px"><b> ID VALIDACIÓN</b></td>
                                    <td style="width:200px">'.' '.$detallesFirmar->code.'</td>
                                </tr>
                                <tr>
                                    <td style="width:90px"><b> CÓDIGO VALIDACIÓN</b></td>
                                    <td style="width:200px">'.' '.$detallesFirmar->hash_autenticador.'</td>
                                </tr>
                                <tr>
                                    <td style="width:90px"><b> URL VALIDACIÓN</b></td>
                                    <td style="width:200px"> https://std.mindep.cl/DocumentosDigitales/ValidarDocumentos</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>                  
                    <td style="width:80px"></td>
                    <td style="width:80px"></td>
                </tr>
                </tbody>
                </table>
            ';

                $pdf->SetXY(20, 314);
                $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                //$pdf->SetFont('Helvetica');
                if ($pageNo == 1) {
                    $pdf->SetFont('','B',11); 
                    $pdf->SetXY(99, 27);
                    $pdf->Write(0, 'DECRETO EXENTO ELECTRÓNICO N° '.$correlativoConCeros);
                    $pdf->SetXY(99, 32);
                    $pdf->Write(0, $detallesFirmar->lugar.', '.$fecha);                    
                }
                
            }


        $urlCorrelativo = str_replace('.pdf', '_ConCorrelativo.pdf', $detallesFirmar->url_s3);
        $detallesFirmar->url_correlativo = $urlCorrelativo;
        $detallesFirmar->save();

        // Output the new PDF
        $pdf->Output($urlCorrelativo, 'F');
        }



//DECRETOSUPREMO
        if($detallesFirmar->id_tipos_documentos_digitales == 5){
            $contarMateria = strlen($detallesFirmar->titulo); 
            $rlineas = round(($contarMateria/38), 0);
        // dd($rlineas);
            $espacioarriba = 35;
            $linea = 8;
            $espaciosporlineas = ($rlineas*$linea);
            $pxY = $espacioarriba+$espaciosporlineas;
            $pxY2 = $espacioarriba+$espaciosporlineas+5;
            

            if (!$idRegistrado){
                $mes1 = Carbon::now();
                $mesStringCorto = $mes1->format('M');
                $dia = $mes1->format('d');
                $ano = $mes1->format('Y');
                $mes = new CalendarioController();
                $mes = $mes->spanishMonth($mesStringCorto);
                if ($mes == 'Nov') {
                    $mes = 'Noviembre';
                } 
                $fecha = $dia.' de '.$mes.' de '.$ano;
            }else{
                $mes1 = Carbon::parse ($idRegistrado->created_at);
                $mesStringCorto = $mes1->format('M');
                $dia = $mes1->format('d');
                $ano = $mes1->format('Y');
                $mes = new CalendarioController();
                $mes = $mes->spanishMonth($mesStringCorto);
                if ($mes == 'Nov') {
                    $mes = 'Noviembre';
                } 
                $fecha = $dia.' de '.$mes.' de '.$ano;
            }




            // initiate FPDI
            $pdf = new FPDI();

            // get the page count
            $pageCount = $pdf->setSourceFile($detallesFirmar->url_s3);
            $pdf->setPrintHeader(false);
            $pdf->setPrintFooter(true);
            // iterate through all pages
            for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
                // import a page
                $templateId = $pdf->importPage($pageNo);
                // get the size of the imported page
                $size = $pdf->getTemplateSize($templateId);
                $pdf->setFooterData(array(0,64,0), array(255, 255, 255));
            
            //$pdf->setFooterData('DECRETO SUPREMO ELECTRÓNICO FIRMADO CON FIRMA AVANZADA');
                $pdf->SetFooterMargin(45);
                $pdf->SetFont('','',7); 
                
                // create a page (landscape or portrait depending on the imported page size)
                if ($size['width'] > $size['height']) {
                    $pdf->AddPage('L', array($size['width'], $size['height']));
                } else {
                    $pdf->AddPage('P', array($size['width'], $size['height']));
                }

                // use the imported page
                $pdf->useTemplate($templateId);
            
                $pdf->Image('img/logo-firmagob.gif', 161, 314, 15, 15, '', '', '', false, 300, '', false, false, false, false, false, false);
                $pdf->Image('imgQR/'.$output_file, 133, 314, 15, 15, '', '', '', false, 300, '', false, false, false, false, false, false);

                $html = '
                
                <table>
                <tbody align="center">
                <tr>                    
                    <td style="width:300px">
                        <table align="left">
                            <tbody>
                                <tr>
                                    <td style="width:90px"><b> NOMBRE</b></td>
                                    <td style="width:200px">'.' '.$statusfirmantes->username->nombre.'</td>
                                </tr>
                                <tr>
                                    <td style="width:90px"><b> DIVISIÓN / ÁREA</b></td>
                                    <td style="width:200px">'.' '.$statusfirmantes->username->division.'</td>
                                </tr>
                                <tr>
                                    <td style="width:90px"><b> FECHA</b></td>
                                    <td style="width:200px">'.' '.Carbon::now()->format("d-m-Y").'</td>
                                </tr>
                                <tr>
                                    <td style="width:90px"><b> ID VALIDACIÓN</b></td>
                                    <td style="width:200px">'.' '.$detallesFirmar->code.'</td>
                                </tr>
                                <tr>
                                    <td style="width:90px"><b> CÓDIGO VALIDACIÓN</b></td>
                                    <td style="width:200px">'.' '.$detallesFirmar->hash_autenticador.'</td>
                                </tr>
                                <tr>
                                    <td style="width:90px"><b> URL VALIDACIÓN</b></td>
                                    <td style="width:200px"> https://std.mindep.cl/DocumentosDigitales/ValidarDocumentos</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>                  
                    <td style="width:80px"></td>
                    <td style="width:80px"></td>
                </tr>
                </tbody>
                </table>
            ';

                $pdf->SetXY(20, 314);
                $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                //$pdf->SetFont('Helvetica');
                if ($pageNo == 1) {
                    $pdf->SetFont('','B',11); 
                    $pdf->SetXY(99, 27);
                    $pdf->Write(0, 'DECRETO SUPREMO ELECTRÓNICO N° '.$correlativoConCeros);
                    $pdf->SetXY(99, 32);
                    $pdf->Write(0, $detallesFirmar->lugar.', '.$fecha);                    
                }
                
            }


        $urlCorrelativo = str_replace('.pdf', '_ConCorrelativo.pdf', $detallesFirmar->url_s3);
        $detallesFirmar->url_correlativo = $urlCorrelativo;
        $detallesFirmar->save();

        // Output the new PDF
        $pdf->Output($urlCorrelativo, 'F');
        }



//DECRETOS
        if($detallesFirmar->id_tipos_documentos_digitales == 3){
            $contarMateria = strlen($detallesFirmar->titulo); 
            $rlineas = round(($contarMateria/38), 0);
        // dd($rlineas);
            $espacioarriba = 35;
            $linea = 8;
            $espaciosporlineas = ($rlineas*$linea);
            $pxY = $espacioarriba+$espaciosporlineas;
            $pxY2 = $espacioarriba+$espaciosporlineas+5;
            

            if (!$idRegistrado){
                $mes1 = Carbon::now();
                $mesStringCorto = $mes1->format('M');
                $dia = $mes1->format('d');
                $ano = $mes1->format('Y');
                $mes = new CalendarioController();
                $mes = $mes->spanishMonth($mesStringCorto);
                if ($mes == 'Nov') {
                    $mes = 'Noviembre';
                } 
                $fecha = $dia.' de '.$mes.' de '.$ano;
            }else{
                $mes1 = Carbon::parse ($idRegistrado->created_at);
                $mesStringCorto = $mes1->format('M');
                $dia = $mes1->format('d');
                $ano = $mes1->format('Y');
                $mes = new CalendarioController();
                $mes = $mes->spanishMonth($mesStringCorto);
                if ($mes == 'Nov') {
                    $mes = 'Noviembre';
                } 
                $fecha = $dia.' de '.$mes.' de '.$ano;
            }




            // initiate FPDI
            $pdf = new FPDI();

            // get the page count
            $pageCount = $pdf->setSourceFile($detallesFirmar->url_s3);
            $pdf->setPrintHeader(false);
            $pdf->setPrintFooter(true);
            // iterate through all pages
            for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
                // import a page
                $templateId = $pdf->importPage($pageNo);
                // get the size of the imported page
                $size = $pdf->getTemplateSize($templateId);
                $pdf->setFooterData(array(0,64,0), array(255, 255, 255));
            
            //$pdf->setFooterData('DECRETO ELECTRÓNICO FIRMADO CON FIRMA AVANZADA');
                $pdf->SetFooterMargin(45);
                $pdf->SetFont('','',7); 
                
                // create a page (landscape or portrait depending on the imported page size)
                if ($size['width'] > $size['height']) {
                    $pdf->AddPage('L', array($size['width'], $size['height']));
                } else {
                    $pdf->AddPage('P', array($size['width'], $size['height']));
                }

                // use the imported page
                $pdf->useTemplate($templateId);
            
                $pdf->Image('img/logo-firmagob.gif', 161, 314, 15, 15, '', '', '', false, 300, '', false, false, false, false, false, false);
                $pdf->Image('imgQR/'.$output_file, 133, 314, 15, 15, '', '', '', false, 300, '', false, false, false, false, false, false);

                $html = '
                
                <table>
                <tbody align="center">
                <tr>                    
                    <td style="width:300px">
                        <table align="left">
                            <tbody>
                                <tr>
                                    <td style="width:90px"><b> NOMBRE</b></td>
                                    <td style="width:200px">'.' '.$statusfirmantes->username->nombre.'</td>
                                </tr>
                                <tr>
                                    <td style="width:90px"><b> DIVISIÓN / ÁREA</b></td>
                                    <td style="width:200px">'.' '.$statusfirmantes->username->division.'</td>
                                </tr>
                                <tr>
                                    <td style="width:90px"><b> FECHA</b></td>
                                    <td style="width:200px">'.' '.Carbon::now()->format("d-m-Y").'</td>
                                </tr>
                                <tr>
                                    <td style="width:90px"><b> ID VALIDACIÓN</b></td>
                                    <td style="width:200px">'.' '.$detallesFirmar->code.'</td>
                                </tr>
                                <tr>
                                    <td style="width:90px"><b> CÓDIGO VALIDACIÓN</b></td>
                                    <td style="width:200px">'.' '.$detallesFirmar->hash_autenticador.'</td>
                                </tr>
                                <tr>
                                    <td style="width:90px"><b> URL VALIDACIÓN</b></td>
                                    <td style="width:200px"> https://std.mindep.cl/DocumentosDigitales/ValidarDocumentos</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>                  
                    <td style="width:80px"></td>
                    <td style="width:80px"></td>
                </tr>
                </tbody>
                </table>
            ';

                $pdf->SetXY(20, 314);
                $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                //$pdf->SetFont('Helvetica');
                if ($pageNo == 1) {
                    $pdf->SetFont('','B',11); 
                    $pdf->SetXY(99, 27);
                    $pdf->Write(0, 'DECRETO ELECTRÓNICO N° '.$correlativoConCeros);
                    $pdf->SetXY(99, 32);
                    $pdf->Write(0, $detallesFirmar->lugar.', '.$fecha);                    
                }
                
            }


        $urlCorrelativo = str_replace('.pdf', '_ConCorrelativo.pdf', $detallesFirmar->url_s3);
        $detallesFirmar->url_correlativo = $urlCorrelativo;
        $detallesFirmar->save();

        // Output the new PDF
        $pdf->Output($urlCorrelativo, 'F');
        }



//MEMOS
        if($detallesFirmar->id_tipos_documentos_digitales == 16){
        if (!$idRegistrado){
                $mes1 = Carbon::now();
                $mesStringCorto = $mes1->format('M');
                $dia = $mes1->format('d');
                $ano = $mes1->format('Y');
                $mes = new CalendarioController();
                $mes = $mes->spanishMonth($mesStringCorto);
                if ($mes == 'Nov') {
                    $mes = 'Noviembre';
                } 
                $fecha = $dia.' de '.$mes.' de '.$ano;
            }else{
                 $mes1 = Carbon::parse ($idRegistrado->created_at);
                 $mesStringCorto = $mes1->format('M');
                 $dia = $mes1->format('d');
                 $ano = $mes1->format('Y');
                 $mes = new CalendarioController();
                 $mes = $mes->spanishMonth($mesStringCorto);
                 if ($mes == 'Nov') {
                    $mes = 'Noviembre';
                } 
                 $fecha = $dia.' de '.$mes.' de '.$ano;
            }




            // initiate FPDI
            $pdf = new FPDI();

            // get the page count
            $pageCount = $pdf->setSourceFile($detallesFirmar->url_s3);
            $pdf->setPrintHeader(false);
            $pdf->setPrintFooter(true);
            // iterate through all pages
            for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
                // import a page
                $templateId = $pdf->importPage($pageNo);
                // get the size of the imported page
                $size = $pdf->getTemplateSize($templateId);
                $pdf->setFooterData(array(0,64,0), array(255, 255, 255));
            
               //$pdf->setFooterData('RESOLUCIÓN EXENTA ELECTRÓNICA FIRMADA CON FIRMA AVANZADA');
                $pdf->SetFooterMargin(45);
                $pdf->SetFont('','',7); 
                
                // create a page (landscape or portrait depending on the imported page size)
                if ($size['width'] > $size['height']) {
                    $pdf->AddPage('L', array($size['width'], $size['height']));
                } else {
                    $pdf->AddPage('P', array($size['width'], $size['height']));
                }

                // use the imported page
                $pdf->useTemplate($templateId);
               
                $pdf->Image('img/logo-firmagob.gif', 161, 314, 15, 15, '', '', '', false, 300, '', false, false, false, false, false, false);
                $pdf->Image('imgQR/'.$output_file, 133, 314, 15, 15, '', '', '', false, 300, '', false, false, false, false, false, false);

                $html = '
               
                <table>
                <tbody align="center">
                <tr>                    
                    <td style="width:300px">
                        <table align="left">
                            <tbody>
                                <tr>
                                    <td style="width:90px"><b> NOMBRE</b></td>
                                    <td style="width:200px">'.' '.$statusfirmantes->username->nombre.'</td>
                                </tr>
                                <tr>
                                    <td style="width:90px"><b> DIVISIÓN / ÁREA</b></td>
                                    <td style="width:200px">'.' '.$statusfirmantes->username->division.'</td>
                                </tr>
                                <tr>
                                    <td style="width:90px"><b> FECHA</b></td>
                                    <td style="width:200px">'.' '.Carbon::now()->format("d-m-Y").'</td>
                                </tr>
                                <tr>
                                    <td style="width:90px"><b> ID VALIDACIÓN</b></td>
                                    <td style="width:200px">'.' '.$detallesFirmar->code.'</td>
                                </tr>
                                <tr>
                                    <td style="width:90px"><b> CÓDIGO VALIDACIÓN</b></td>
                                    <td style="width:200px">'.' '.$detallesFirmar->hash_autenticador.'</td>
                                </tr>
                                <tr>
                                    <td style="width:90px"><b> URL VALIDACIÓN</b></td>
                                    <td style="width:200px"> https://std.mindep.cl/DocumentosDigitales/ValidarDocumentos</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>                  
                    <td style="width:70px"></td>
                    <td style="width:70px"></td>
                </tr>
                </tbody>
                </table>
               ';

                $pdf->SetXY(20, 314);
                $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                //$pdf->SetFont('Helvetica');
                if ($pageNo == 1) {
                    $pdf->SetFont('','B',11); 
                    $pdf->SetXY(99, 43);
                    $pdf->Write(0, 'MEMORÁNDUM ELECTRÓNICO');
                    $pdf->SetXY(99, 56);
                    $pdf->Write(0, 'FOLIO N° '.$correlativoConCeros);
                    $pdf->SetXY(99, 69);
                    $pdf->Write(0, $detallesFirmar->lugar.', '.$fecha);
                }
                
            }
        
     
        $urlCorrelativo = str_replace('.pdf', '_ConCorrelativo.pdf', $detallesFirmar->url_s3);
        $detallesFirmar->url_correlativo = $urlCorrelativo;
        $detallesFirmar->save();

        // Output the new PDF
        $pdf->Output($urlCorrelativo, 'F');
        }
        }catch (\Exception $e) {
        DB::rollback();
        alert()->error('!','Ha ocurrido un error');
        return redirect()->route('firmado_doc', $id);
          
        }



        $obtenerRut = $statusfirmantes->username->rut;
        $documento = $detallesFirmar->url_correlativo;
        $expirations = Carbon::now()->addMinute(5)->format('Y-m-d\TH:i:s');

        $entity='Subsecretaría del Deporte';
        $rut = $obtenerRut;   
        $expiration = $expirations;
        $purpose = 'Desatendido';
        $dataf = array(
            "entity" => $entity,
            "run" => $rut,
            "expiration" => $expiration,
            "purpose" => $purpose
        );
        $data =[];
        $data['token'] = JWT::encode($dataf, env('JWT_SECRET'));
        $url = env('JWT_URL_API_FIRMA_V2');
        $data['api_token_key'] = env('JWT_API_TOKEN_KEY');


            if (file_exists($documento)) {
                
                $nombreDocumentoR = $detallesFirmar->titulo;
                $extension = pathinfo(parse_url($documento, PHP_URL_PATH), PATHINFO_EXTENSION);
                $docNombre = bin2hex(random_bytes(25)).'.'.$extension;
        
                if ($documento) {
                    
        
                    $url_documento_no_firmado = $detallesFirmar->url_correlativo;
                    $descripcion = $detallesFirmar->descripcion;     
                    $base64 = base64_encode(file_get_contents($url_documento_no_firmado));
                    $checksumfile = hash_file("sha256" , $url_documento_no_firmado);
        
                            $data['files'] = array(array(
                                "description" => $descripcion,
                                "content-type" => "application/pdf",
                                "checksum" => $checksumfile,
                                "content" =>$base64
                            ));
                        try {
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                            curl_setopt($ch, CURLOPT_HEADER, FALSE);
                            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=UTF-8'));
                            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0');
                            curl_setopt($ch, CURLOPT_POST, TRUE);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
                        } catch(Exception $e) {
                            alert()->error('!','Ha ocurrido un error intente de nuevo');
                        }
        
                        try {
                            $result = curl_exec($ch);
                            $resultJson = json_decode($result);
                            $idSolicitudFirma = $resultJson->idSolicitud;
                            $datosDocumento = $resultJson->files[0];
                            $documentoB64 = $datosDocumento->content;
                            $statusB64 = $datosDocumento->status;
                            $documentStatus = $datosDocumento->documentStatus;

                           // dd($data, $dataf, $result, $resultJson);


                            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                            $err = curl_error($ch);
                            curl_close($ch);
        
                            if ($err != null)  {
                                Log::info('Error al ejecutar la primera peticion a FIRMA:\n\n', [
                                    'status' => $httpcode,
                                    'error' => $err
                                ]);
        
                                alert()->error('!','Ha ocurrido un error intente de nuevo');
                                //return redirect()->route('firmarDocumentos');
                            }

                            $file_path = public_path().'/firmadosNew/'.$docNombre; 
                            $file_path2 = '/firmadosNew/'.$docNombre;
                            //dd($documentoB64);
                            file_put_contents($file_path, base64_decode($documentoB64));                         
                            
                            
                            $detallesFirmar->url_firmado = $file_path2;

                           if ($idRegistrado) {
                            $idRegistrado->documento = $file_path2;
                            $idRegistrado->save();
                           }else{
                            $correlativon->documento = $file_path2;
                            $correlativon->save();
                           }
                           
                            $detallesFirmar->id_etapas_documentos_digitales = 4;
                            $detallesFirmar->save();
                            
                            $statusfirmantes->estatus = 1;
                            $statusfirmantes->save();
                            
        
                        } catch(Exception $e) {
                            Log::info('Error al ejecutar la primera peticion a Firma:\n\n', [
                                'error' => $e
                            ]);
        
                            alert()->error('!','Ha ocurrido un error intente de nuevo');
                            //return redirect()->route('firmarDocumentos');
                        }
                        
                    } 
        
                    if ( $detallesFirmar->url_firmado){
                        $this->notificacionDocumentoDigitalFirmado($detallesFirmar);

                        if($detallesFirmar->distribuye == 1){
                            $this->notificacionDocumentoDigitalFirmadoDistribucion($detallesFirmar);
                        }
                        
                    }
                
                alert()->success('!','Documento Digital Firmado');
                return redirect()->route('firmado_doc', $id);
        
                
            }else{

                alert()->error('!','Ha ocurrido un error con la firma intente de nuevo');
                return redirect()->route('firmado_doc', $id);
                
            }

       
    }

    public function firmado($id)
    {
        
        $registro = ListadoFirmantesRegistroDocumentosDigitales::select('id_registro_documento_digitales')->where('id' , $id)->first();
        $editRegistro = RegistroDocumentosDigitales::where('id' , $registro->id_registro_documento_digitales)->first();
        
        return view('documentos_digitales.firmado')
        ->with('editRegistro' , $editRegistro);
    }

    public function index2(Request $request)
    {

        $dataDefinitiva = [];
        $idUsuario = Auth::user()->id;
        $dataDefinitiva1 = DerivacionRegistroDocumentosDigitales::select('id_registro_documentos_digitales')->where('id_user_destino', $idUsuario)->get();      
        $dataDefinitiva1 = array_column($dataDefinitiva1->toArray(), 'id_registro_documentos_digitales');         
        $dataDefinitiva0 = VistosBuenos::select('id_registro_documento_digitales')->where('estatus', '0')->where('id_user', $idUsuario)->whereIn('id_registro_documento_digitales', $dataDefinitiva1)->get();   
        $dataDefinitiva0 = array_column($dataDefinitiva0->toArray(), 'id_registro_documento_digitales');  
      
        $dataDefinitiva = RegistroDocumentosDigitales::whereIn('id' , $dataDefinitiva0)->get(); 
        

        $anno = Carbon::now()->format('Y');
        $fechaStringInicio = ($anno).'-01-01 00:00:00';
        $feDesde = Carbon::parse($fechaStringInicio);
        $feHasta =  Carbon::parse(Carbon::now()->format('Y-m-d').' 23:59:59');
        $fechaInDesde = Carbon::parse($fechaStringInicio);
        $fechaInHasta = Carbon::now();
        $fechas[1] = Date('Y-m-d', strtotime( '1970-01-01' ));
        $fechas[0] = Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
        $tipoDocumento = $request->tipo_documento;
        $tramite = $request->tramite;
        $categoria = $request->categoria;
        $materia = $request->materia;
        $tiposDocumentos[null] = 'Sleccione Tipo Documento'; foreach (TiposDocumentosDigitales::get() as $key1 => $value1) { $tiposDocumentos[$value1->id]=$value1->nombre;} 
        $categorias[null] = 'Seleccione Categoría'; foreach (Categoria::get() as $key2 => $value2) { $categorias[$value2->id]=$value2->nombre;} 

        if( $request->FechaDesde != null ) {
            $feDesde=(Carbon::parse($request->get('FechaDesde'))->format('dmY'));
            $feDesde= Carbon::parse(substr($feDesde, 0, 2).'-'.substr($feDesde, 2, 2).'-'.substr($feDesde, 4, 4).' 00:00:00');
        }
        if( $request->FechaHasta != null ) {
            $feHasta=(Carbon::parse($request->get('FechaHasta'))->format('dmY'));
            $feHasta= Carbon::parse(substr($feHasta, 0, 2).'-'.substr($feHasta, 2, 2).'-'.substr($feHasta, 4, 4).' 23:59:59');
        }

      // dd($feDesde, $feHasta);
        $query = RegistroDocumentosDigitales::query ();
        $query->whereIn('id' , $dataDefinitiva0);
        $query->when(request('tramite', false), function ($q, $tramite) {
            return $q->where('id', $tramite);
        });
        $query->when(request('FechaDesde', false), function ($q, $feDesde) {
            return $q->where('created_at','>=', $feDesde);
        });
        $query->when(request('FechaHasta', false), function ($q, $feHasta) {
            return $q->where('created_at','<=', $feHasta);
        });
        $query->when(request('tipo_documento', false), function ($q, $tipoDocumento) {
            return $q->where('id_tipos_documentos_digitales', $tipoDocumento);
          });
        $query->when(request('categoria', false), function ($q, $categoria) {
            return $q->where('id_categoria', $categoria);
        });
        $query->when(request('materia'), function ($q, $materia) {
            return $q->where('titulo', 'LIKE', "%$materia%");
        });
        $correlativos = $query->get();   

        return view('documentos_digitales.derivacion_registro_documentos_digitales')
        ->with('dataDefinitiva' , $correlativos)
        ->with('feDesde' ,$feDesde)
        ->with('feHasta' ,$feHasta)
        ->with('fechaInDesde' ,$fechaInDesde)
        ->with('fechaInHasta' ,$fechaInHasta)
        ->with('fechas' ,$fechas)
        ->with('tiposDocumentos' , $tiposDocumentos)
        ->with('categorias' , $categorias)
        ->with('tramite' , $tramite)
        ->with('categoria' , $categoria)
        ->with('tipoDocumento' , $tipoDocumento)
        ->with('materia' , $materia);
        
    }

    public function editFirmante($id){
        
        $editRegistro = RegistroDocumentosDigitales::where('id' , $id)->first();
        $lugarEdit = Lugar::where('nombre' , $editRegistro->lugar)->first();
        if($lugarEdit){
            $lugarEdit = $lugarEdit->id;
        } 
        $documentoWord = AnexosDocumentosDigitales::where('id_registro_documentos_digitales', $editRegistro->id)->first();
        $datosDerivacion = DerivacionRegistroDocumentosDigitales::where('id_registro_documentos_digitales', $editRegistro->id)->first();
        $datosFirmante = ListadoFirmantesRegistroDocumentosDigitales::where('id_registro_documento_digitales', $editRegistro->id)->get();
        $datosVB = VistosBuenos::where('id_registro_documento_digitales', $editRegistro->id)->get();
    
        $varNull = null;
        $selectTipo[null] = 'Seleccione Tipo de Documento'; foreach (TiposDocumentosDigitales::orderby('id', 'asc')->get() as $key1 => $value1) { $selectTipo[$value1->id]=$value1->nombre;} 
        $selectUsuariosid[null] = 'Devolver Documento A'; foreach (User::orderby('name', 'asc')->get() as $key2 => $value2) { $selectUsuariosid[$value2->id]=$value2->name;}
        $selectFirmantes[null] = 'Seleccione Firmante'; foreach (Firmantes::orderby('nombre', 'asc')->get() as $key3 => $value3) { $selectFirmantes[$value3->id_user]=$value3->nombres;} 
        $selectFirmantesS[null] = 'Seleccione Firmante Subrogante'; foreach (FirmantesSub::orderby('nombre', 'asc')->get() as $key3 => $value3) { $selectFirmantesS[$value3->id_user]=$value3->nombress;}
        $selectLugar[null] = 'Seleccione Lugar'; foreach (Lugar::orderby('id', 'asc')->get() as $key2 => $value2) { $selectLugar[$value2->id]=$value2->nombre;} 
         
        $categoriaDoc = [];
        foreach (Categoria::orderby('id', 'asc')->get() as $key4 => $value4) { $categoriaDoc[$value4->id]=$value4->nombre;} 

        return view('documentos_digitales.derivacion_registro_documentos_digitales_editar_firmante')
        ->with('editRegistro' , $editRegistro)
        ->with('documentoWord' , $documentoWord)
        ->with('datosDerivacion' , $datosDerivacion)
        ->with('datosVB' , $datosVB)
        ->with('datosFirmante' , $datosFirmante)
        ->with('varNull' , $varNull)
        ->with('selectTipo' , $selectTipo)
        ->with('categoriaDoc' , $categoriaDoc)
        ->with('selectUsuariosid' , $selectUsuariosid)
        ->with('selectFirmantes' , $selectFirmantes)
        ->with('selectLugar' , $selectLugar)
        ->with('lugarEdit' , $lugarEdit)
        ->with('selectFirmantesS' , $selectFirmantesS);
    }

    public function updateFirmante (ColaboracionDocumentoFirmanteRequest $request, $id)
    {
        $CateDoc = $request->id_categoria;   
        $reservadoDistribucion = $request->reservado_distribucion;     
        $titulo = $request->titulo;
        $descripcion = $request->descripcion;
        $derivar =  $request->derivar;

        $word = $request->filename2;
        $derivObservacion = $request->observacion;
        $derivColaborador = $request->id_user_destino;
        if ($derivar == 1) {
            if (!$derivObservacion || !$derivColaborador ){
                return redirect()->back()->withErrors(['Los Campos de Observación y Colaborador son requeridos.']);
            }
        }
        $nMemo = $request->n_memo;
        $memo =  $request->memo;
        $derivar =  $request->derivar;
        $firmante = $request->firmante;
        $desti = $request->desti;
        $documento = $request->filename;
        $lugarId = $request->lugar;
        $lugar = Lugar::where('id', $lugarId)->first();

        $editRegistro = RegistroDocumentosDigitales::where('id' , $id)->first();
        $editRegistro->id_categoria = $CateDoc;
        $editRegistro->reservado_distribucion = $reservadoDistribucion;
        $editRegistro->titulo = $titulo;
        $editRegistro->lugar = $lugar->nombre;
        $editRegistro->descripcion = $descripcion;
        if ($memo == 1) {
            $editRegistro->n_memo = $nMemo;
        }
        if ($memo == 0) {
            $editRegistro->n_memo = null;
        }
        if ($derivar == 0) {
            $editRegistro->id_etapas_documentos_digitales = EtapasDocumentosDigitales::where('nombre' , 'En Proceso Firma')->first()->id;
        }
        if ($derivar == 1) {
            $editRegistro->id_etapas_documentos_digitales = EtapasDocumentosDigitales::where('nombre' , 'Colaboración')->first()->id;
        }
        if ($documento) {
            $editRegistro->filename = $request->filename->getClientOriginalName();
            $path = $documento->store(null, 'nofirmadosNew');
            $url_documento_no_firmado = public_path().'/nofirmadosNew/'.$path;
            $url_documento_no_firmado_vista = '/nofirmadosNew/'.$path;
            $editRegistro->url_s3 = $url_documento_no_firmado;
            $editRegistro->url_vista = $url_documento_no_firmado_vista;
            } 
        $editRegistro->save();


        
        if($editRegistro->correlativo){
            $editRegistroReserva = CorrelativoDocumentosDigitales::where('correlativo' , $editRegistro->correlativo)->first();
            if($editRegistro->id_categoria != 1){
                $editRegistroReserva->reservado_distribucion = $editRegistro->reservado_distribucion;
            }
    
            $editRegistroReserva->save();

        }
        
        


        $firmantes = $request->id_user_firmante;
        /* $sub = $request->sub;
        if ($sub == 1) {
            if (!$request->id_user_firmante_s){
                return redirect()->back()->withErrors(['El Campos de Seleccionar Firmante es requerido.']);
            }
            $firmantes = $request->id_user_firmante_s;
        } */
        $estatus2 = 0;
        if ($firmante == 1) {                
            $nuevoFirmante = new ListadoFirmantesRegistroDocumentosDigitales();
            $nuevoFirmante->id_registro_documento_digitales = $editRegistro->id;
            $nuevoFirmante->id_user_firmante = $firmantes;
            $nuevoFirmante->id_user_crea_tabla = Auth::user()->id;
            $nuevoFirmante->estatus = $estatus2;
            $nuevoFirmante->sub = 0;
           /*  if ($sub == 0) {
                $nuevoFirmante->sub = 0;
            }
            if ($sub == 1) {
                $nuevoFirmante->sub = 1;
            } */
            $nuevoFirmante->save();
        }

        
        /* if ($desti == 1) {
            $userDestino = $request->id_user_destino;
            $estatus5 = 0;
            foreach ($userDestino as $llave => $valor) {
                
                $nuevoUserDestino = new VistosBuenos();
                $nuevoUserDestino->id_user = $valor;
                if($nuevoUserDestino){
                    $datosUsuarios = user::where('id', $nuevoUserDestino->id_user)->first();
                    $nuevoUserDestino->email = $datosUsuarios->email;
                    $nuevoUserDestino->nombre = $datosUsuarios->name;
                }
                $nuevoUserDestino->id_registro_documento_digitales = $editRegistro->id;
                $nuevoUserDestino->estatus = $estatus5;
                $nuevoUserDestino->save();
            }
        } */
        

        if ($derivar == 1) {
            $userDestino = $request->id_user_destino;
            $estatus5 = 0;
            foreach ($userDestino as $llave => $valor) {
                
                $nuevoUserDestino = new VistosBuenos();
                $nuevoUserDestino->id_user = $valor;
                if($nuevoUserDestino){
                    $datosUsuarios = user::where('id', $nuevoUserDestino->id_user)->first();
                    $nuevoUserDestino->email = $datosUsuarios->email;
                    $nuevoUserDestino->nombre = $datosUsuarios->name;
                }
                $nuevoUserDestino->id_registro_documento_digitales = $editRegistro->id;
                $nuevoUserDestino->estatus = $estatus5;
                $nuevoUserDestino->save();
            }


            $estatus4 = 1;
            $userDestino = $request->id_user_destino;
            $observacion = $request->observacion;
            $usuarioDestino = $request->id_user_destino;
            $nuevaDerivacion = new DerivacionRegistroDocumentosDigitales();
            $nuevaDerivacion->id_registro_documentos_digitales = $editRegistro->id;
            $nuevaDerivacion->id_estatus_derivacion_registro_documentos_digitales = $estatus4;
            $nuevaDerivacion->id_user_emisor = Auth::user()->id;
            $estatusVB =  VistosBuenos::where('id_registro_documento_digitales', $editRegistro->id)->where('estatus', 0)->first();
            $nuevaDerivacion->id_user_destino = $estatusVB->id_user;            
            $nuevaDerivacion->id_user_generador = $editRegistro->id_user;
            $nuevaDerivacion->observacion = $observacion;
            $nuevaDerivacion->save();

            /* $estatus3 = 1;
            $documentoW = $request->filename2;    
            $nuevoWord = new AnexosDocumentosDigitales();
            $nuevoWord->id_registro_documentos_digitales = $editRegistro->id;
            $nuevoWord->id_derivacion = $nuevaDerivacion->id;
            $nuevoWord->estatus = $estatus3;
            if ($documentoW) {
                $nuevoWord->filename2 = $request->filename2->getClientOriginalName();
                $upload = Storage::disk('s3_3')->put('word-editable', $documentoW, 'public');
                $nuevoWord->url_s3 = 'https://documentos-digitales-std.s3.amazonaws.com/'.$upload;
                } 
            $nuevoWord->save(); */


            }

            if ($derivar == 1){
                $this->notificacionRegistroColaboracion($editRegistro);
              }else{
                $this->notificacionRegistroFirma($editRegistro);
              }
    
        
        alert()->success('!','Registro Rechazado exitosamente');
        return redirect()->route('inicio');
        
    }

    public function edit($id){
        
        $editRegistro = RegistroDocumentosDigitales::where('id' , $id)->first();
        $lugarEdit = Lugar::where('nombre' , $editRegistro->lugar)->first();
        if($lugarEdit){
            $lugarEdit = $lugarEdit->id;
        } 
        $documentoWord = AnexosDocumentosDigitales::where('id_registro_documentos_digitales', $editRegistro->id)->first();
        $datosDerivacion = DerivacionRegistroDocumentosDigitales::where('id_registro_documentos_digitales', $editRegistro->id)->first();
        $datosFirmante = ListadoFirmantesRegistroDocumentosDigitales::where('id_registro_documento_digitales', $editRegistro->id)->get();
        $datosVB = VistosBuenos::where('id_registro_documento_digitales', $editRegistro->id)->get();
    
        $varNull = null;
        $selectTipo[null] = 'Seleccione Tipo de Documento'; foreach (TiposDocumentosDigitales::orderby('id', 'asc')->get() as $key1 => $value1) { $selectTipo[$value1->id]=$value1->nombre;} 
        $selectUsuariosid[null] = 'Seleccione Destinatario(s)'; foreach (User::orderby('name', 'asc')->get() as $key2 => $value2) { $selectUsuariosid[$value2->id]=$value2->name;}
        $selectFirmantes[null] = 'Seleccione Firmante'; foreach (Firmantes::orderby('nombre', 'asc')->get() as $key3 => $value3) { $selectFirmantes[$value3->id_user]=$value3->nombres;} 
        $selectFirmantesS[null] = 'Seleccione Firmante Subrogante'; foreach (FirmantesSub::orderby('nombre', 'asc')->get() as $key3 => $value3) { $selectFirmantesS[$value3->id_user]=$value3->nombress;}
        $selectLugar[null] = 'Seleccione Lugar'; foreach (Lugar::orderby('id', 'asc')->get() as $key2 => $value2) { $selectLugar[$value2->id]=$value2->nombre;} 

        $categoriaDoc = [];
        foreach (Categoria::orderby('id', 'asc')->get() as $key4 => $value4) { $categoriaDoc[$value4->id]=$value4->nombre;} 

        return view('documentos_digitales.derivacion_registro_documentos_digitales_editar')
        ->with('editRegistro' , $editRegistro)
        ->with('documentoWord' , $documentoWord)
        ->with('datosDerivacion' , $datosDerivacion)
        ->with('datosVB' , $datosVB)
        ->with('datosFirmante' , $datosFirmante)
        ->with('varNull' , $varNull)
        ->with('selectTipo' , $selectTipo)
        ->with('categoriaDoc' , $categoriaDoc)
        ->with('selectUsuariosid' , $selectUsuariosid)
        ->with('selectFirmantes' , $selectFirmantes)
        ->with('selectLugar' , $selectLugar)
        ->with('lugarEdit' , $lugarEdit)
        ->with('selectFirmantesS' , $selectFirmantesS);
    }

    public function update (ColaboracionDocumentoRequest $request, $id)
    {
        $nMemo = $request->n_memo;
        $memo =  $request->memo;
        $CateDoc = $request->id_categoria;     
        $reservadoDistribucion = $request->reservado_distribucion;   
        $titulo = $request->titulo;
        $descripcion = $request->descripcion;
        $derivar =  $request->derivar;
        $firmante = $request->firmante;
        $desti = $request->desti;
        $documento = $request->filename;
        $lugarId = $request->lugar;
        $lugar = Lugar::where('id', $lugarId)->first();

        $editRegistro = RegistroDocumentosDigitales::where('id' , $id)->first();
        $editRegistro->id_categoria = $CateDoc;
        $editRegistro->reservado_distribucion = $reservadoDistribucion;
        $editRegistro->titulo = $titulo;
        $editRegistro->lugar = $lugar->nombre;
        $editRegistro->descripcion = $descripcion;
        if ($memo == 1) {
            $editRegistro->n_memo = $nMemo;
        }
        if ($memo == 0) {
            $editRegistro->n_memo = null;
        }
        if ($derivar == 0) {
            $editRegistro->id_etapas_documentos_digitales = EtapasDocumentosDigitales::where('nombre' , 'En Proceso Firma')->first()->id;
        }
        if ($derivar == 1) {
            $editRegistro->id_etapas_documentos_digitales = EtapasDocumentosDigitales::where('nombre' , 'Colaboración')->first()->id;
        }
        if ($documento) {
            $editRegistro->filename = $request->filename->getClientOriginalName();
            $path = $documento->store(null, 'nofirmadosNew');
            $url_documento_no_firmado = public_path().'/nofirmadosNew/'.$path;
            $url_documento_no_firmado_vista = '/nofirmadosNew/'.$path;
            $editRegistro->url_s3 = $url_documento_no_firmado;
            $editRegistro->url_vista = $url_documento_no_firmado_vista;
            } 
        $editRegistro->save();

        if($editRegistro->correlativo){
            $editRegistroReserva = CorrelativoDocumentosDigitales::where('correlativo' , $editRegistro->correlativo)->first();
            if($editRegistro->id_categoria != 1){
                $editRegistroReserva->reservado_distribucion = $editRegistro->reservado_distribucion;
            }
    
            $editRegistroReserva->save();

        }

        $estatus6 = 1;
        $editStatusVB = VistosBuenos::where('id_registro_documento_digitales', $editRegistro->id)->where('id_user', Auth::user()->id)->where('estatus', 0)->first();
        $editStatusVB->estatus = $estatus6;
        $editStatusVB->save();

        $estatusDerivacion = 2;
        $editEstatusDerivacion = DerivacionRegistroDocumentosDigitales::where('id_registro_documentos_digitales', $editRegistro->id)->where('id_user_destino', Auth::user()->id)->where('id_estatus_derivacion_registro_documentos_digitales', 1)->first();
        $editEstatusDerivacion->id_estatus_derivacion_registro_documentos_digitales = $estatusDerivacion;
        $editEstatusDerivacion->save();

        $firmantes = $request->id_user_firmante;
        /* $sub = $request->sub;
        if ($sub == 1) {
            if (!$firmantes){
                return redirect()->back()->withErrors(['El Campos de Seleccionar Firmante es requerido.']);
            }
        } */
        $estatus2 = 0;
        if ($firmante == 1) {                
            $nuevoFirmante = new ListadoFirmantesRegistroDocumentosDigitales();
            $nuevoFirmante->id_registro_documento_digitales = $editRegistro->id;
            $nuevoFirmante->id_user_firmante = $firmantes;
            $nuevoFirmante->id_user_crea_tabla = Auth::user()->id;
            $nuevoFirmante->estatus = $estatus2;
            $nuevoFirmante->sub = 0;
           /*  if ($sub == 0) {
                $nuevoFirmante->sub = 0;
            }
            if ($sub == 1) {
                $nuevoFirmante->sub = 1;
            } */
            $nuevoFirmante->save();
        }

        
        if ($desti == 1) {
            $userDestino = $request->id_user_destino;
            $estatus5 = 0;
            foreach ($userDestino as $llave => $valor) {
                
                $nuevoUserDestino = new VistosBuenos();
                $nuevoUserDestino->id_user = $valor;
                if($nuevoUserDestino){
                    $datosUsuarios = user::where('id', $nuevoUserDestino->id_user)->first();
                    $nuevoUserDestino->email = $datosUsuarios->email;
                    $nuevoUserDestino->nombre = $datosUsuarios->name;
                }
                $nuevoUserDestino->id_registro_documento_digitales = $editRegistro->id;
                $nuevoUserDestino->estatus = $estatus5;
                $nuevoUserDestino->save();
            }
        }
        

        if ($derivar == 1) {
            $estatus4 = 1;
            $userDestino = $request->id_user_destino;
            $observacion = $request->observacion;
            $usuarioDestino = $request->id_user_destino;
            $nuevaDerivacion = new DerivacionRegistroDocumentosDigitales();
            $nuevaDerivacion->id_registro_documentos_digitales = $editRegistro->id;
            $nuevaDerivacion->id_estatus_derivacion_registro_documentos_digitales = $estatus4;
            $nuevaDerivacion->id_user_emisor = Auth::user()->id;
            if($usuarioDestino){
                $estatusVB =  VistosBuenos::where('id_registro_documento_digitales', $editRegistro->id)->where('estatus', 0)->first();
                $nuevaDerivacion->id_user_destino = $estatusVB->id_user;
             }
            $nuevaDerivacion->id_user_generador = $editRegistro->id_user;
            $nuevaDerivacion->observacion = $observacion;
            $nuevaDerivacion->save();

            $estatus3 = 1;
            $documentoW = $request->filename2;    
            $nuevoWord = new AnexosDocumentosDigitales();
            $nuevoWord->id_registro_documentos_digitales = $editRegistro->id;
            $nuevoWord->id_derivacion = $nuevaDerivacion->id;
            $nuevoWord->estatus = $estatus3;
            if ($documentoW) {
                $nuevoWord->filename2 = $request->filename2->getClientOriginalName();
                $upload = Storage::disk('s3_3')->put('word-editable', $documentoW, 'public');
                $nuevoWord->url_s3 = 'https://documentos-digitales-std.s3.amazonaws.com/'.$upload;
                } 
            $nuevoWord->save();
            }
    
            if ($derivar == 1){
                $this->notificacionRegistroColaboracion($editRegistro);
              }else{
                $this->notificacionRegistroFirma($editRegistro);
              }

        alert()->success('!','Registro corregido exitosamente');
        return redirect()->route('registros_derivados');
        
    }


    public function corregirDoc($id)
    {
        
      $corregirDocumentoRegistro = RegistroDocumentosDigitales::where('id' , $id)->first();
      
      return view('documentos_digitales.corregir_documento')
      ->with('corregirDocumentoRegistro' , $corregirDocumentoRegistro);
    }


     public function corregidoDoc(Request $request, $id)
    {
        $documento = $request->filename;

        $corregirDocumentoRegistro = RegistroDocumentosDigitales::where('id' , $id)->first();

        if ($documento) {
            $corregirDocumentoRegistro->filename = $request->filename->getClientOriginalName();
            $path = $documento->store(null, 'nofirmadosNew');
            $url_documento_no_firmado = public_path().'/nofirmadosNew/'.$path;
            $url_documento_no_firmado_vista = '/nofirmadosNew/'.$path;
            $corregirDocumentoRegistro->url_s3 = $url_documento_no_firmado;
            $corregirDocumentoRegistro->url_vista = $url_documento_no_firmado_vista;
            } 

        $corregirDocumentoRegistro->save();

      $url = $request->input('url');
      alert()->success('!','Documento Cargado Exitosamente');
      return redirect($url)->with('success', 'Documento Cargado Exitosamente');
    }


    public function destroyFir(Request $request, $id){
      
        $tramite = ListadoFirmantesRegistroDocumentosDigitales::where('id', $id)->first();
        
        $result = $tramite->delete();
            if ($result) {
                return response()->json(['success'=>'true']);
            }else{
                return response()->json(['success'=> 'false']);
            }
    }

    public function destroyDes(Request $request, $id){
      
        $tramite2 = VistosBuenos::where('id', $id)->first();
        
        $result = $tramite2->delete();
            if ($result) {
                return response()->json(['success'=>'true']);
            }else{
                return response()->json(['success'=> 'false']);
            }
    }



    //VALIDADOR DE DOCUMENTOS
    public function indexvalidador()
    {
        return view('documentos_digitales.validar_documentos_digitaltes');
    }
    public function Validardoc(Request $request)
    {
       $id = $request->id;
       $codigo = $request->hash;
       $documento = RegistroDocumentosDigitales::where('code' , $id)->where('hash_autenticador' , $codigo)->first();
 
       if(!$documento){
            return redirect()->back()->withErrors(['No sentimos no existe documento con estos datos']); 
       }else{
        $listadoFirmantes = ListadoFirmantesRegistroDocumentosDigitales::where('id_registro_documento_digitales', $documento->id)->first();
         return view('documentos_digitales.documentos_digitaltes_validados')
         ->with('listadoFirmantes' , $listadoFirmantes)
         ->with('documento' , $documento);
       }

    }


    //CLAVE UNICA
    public function claveUnica()
    {
        return view('documentos_digitales.boton_clave_unica');
    }


    //NOTIFICACIONES
    public function notificacionRegistroFirma($nuevoRegistro){

        $registro = RegistroDocumentosDigitales::where('id' , $nuevoRegistro->id)->first();
        $listadoFirmantes = ListadoFirmantesRegistroDocumentosDigitales::where('id_registro_documento_digitales' , $registro->id)->first();
        $email = $listadoFirmantes->username->email;

    
        $datos = ['registro'=>$nuevoRegistro , 'listadoFirmantes'=>$listadoFirmantes];
        
        Mail::to($email)->send(new NotificacionNuevoRegistroFirmaEmail($datos));        

      }
      

      public function notificacionRegistroColaboracion($nuevoRegistro){

        $registro = RegistroDocumentosDigitales::where('id' , $nuevoRegistro->id)->first();
        $derivacion = DerivacionRegistroDocumentosDigitales::where('id_registro_documentos_digitales', $registro->id)->first();
        $email = $derivacion->userdestino->email;
        
        $datos = ['registro'=>$nuevoRegistro , 'derivacion'=>$derivacion];
        
        Mail::to($email)->send(new NotificacionNuevoRegistroColaboracionEmail($datos));        

      }

      public function notificacionDocumentoDigitalFirmado($detallesFirmar){

        $registro = RegistroDocumentosDigitales::where('id' , $detallesFirmar->id)->first();
        $listadoFirmantes = ListadoFirmantesRegistroDocumentosDigitales::where('id_registro_documento_digitales' , $registro->id)->first();
        $emailA = $listadoFirmantes->username->email;
        $emailC = 'gestiondocumental@mindep.cl';
        $usuarioRegitraDoc = RegistroDocumentosDigitales::where('id_user' , $registro->id_user)->first();
        $emailB = $usuarioRegitraDoc->Iduser->email;
        $emailD = $registro->destinatario_memo;
        
        if($emailD){
            $emails = [$emailA, $emailB, $emailC, $emailD];
        }else{
            $emails = [$emailA, $emailB, $emailC];
        }

        
        $datos = ['registro'=>$detallesFirmar, 'listadoFirmantes'=>$listadoFirmantes];
  
        foreach ($emails as $key => $correo) {
          Mail::to($correo)->send(new NotificacionDocumentoDigitalFirmadoEmail($datos));
        }
      }

      public function notificacionDocumentoDigitalFirmadoDistribucion($detallesFirmar){

        $registro = RegistroDocumentosDigitales::where('id' , $detallesFirmar->id)->first();
        $listadoFirmantes = ListadoFirmantesRegistroDocumentosDigitales::where('id_registro_documento_digitales' , $registro->id)->first();
        $emails = $registro->correos_distri;     
        
        $datos = ['registro'=>$detallesFirmar, 'listadoFirmantes'=>$listadoFirmantes];
  
        foreach ($emails as $key => $correo) {
          Mail::to($correo)->send(new NotificacionDocumentoDigitalFirmadoDistribucionEmail($datos));
        }
      }


      public function indexAdjunto(Request $request)
    {
        $dataDefinitiva = Adjuntos::get();
 
       return view('documentos_digitales.generar_enlace')
       ->with('dataDefinitiva' , $dataDefinitiva);
    }

    public function createAdjunto (){
        
        return view('documentos_digitales.generar_enlace_crear');
    }

    public function storeAdjunto (GenerarEnlaceRequest $request)
    {
        set_time_limit(14200);
        $descripcion = $request->descripcion;      
        $documento = $request->filename;
        $nuevoEnlace = new Adjuntos();
        $nuevoEnlace->descripcion = $descripcion;
        
        $nuevoEnlace->filename = $request->filename->getClientOriginalName();
        $upload = Storage::disk('s3_3')->put('adjuntos', $documento, 'public');
        $nuevoEnlace->url_aws = 'https://documentos-digitales-std.s3.amazonaws.com/'.$upload;
                   
        $nuevoEnlace->save();
        alert()->success('!','Enlace Generado Exitosamente');
        return redirect()->route('mostrar_enlace' , [ $nuevoEnlace->id]);
    }

    public function mostrarEnalaceAdjunto (Request $request , $idArchivo)
    {
        $datosEnlace = Adjuntos::where('id', $idArchivo)->first();

        return view('documentos_digitales.enlace_generado')
       ->with('datosEnlace' , $datosEnlace);
    }

    public function editAdjunto($id){
        
        $editEnlace = Adjuntos::where('id' , $id)->first();
        return view('documentos_digitales.generar_enlace_editar')
        ->with('editEnlace' , $editEnlace);
    }

    public function updateAdjunto (EditarGenerarEnlaceRequest $request, $id)
    {
        set_time_limit(14200);
        $descripcion = $request->descripcion;   
        $documento = $request->filename;
        $nuevoEnlace = Adjuntos::where('id' , $id)->first();
        $nuevoEnlace->descripcion = $descripcion;
        if ($documento) {
        $nuevoEnlace->filename = $request->filename->getClientOriginalName();
        $upload = Storage::disk('s3_3')->put('adjuntos', $documento, 'public');
        $nuevoEnlace->url_aws = 'https://documentos-digitales-std.s3.amazonaws.com/'.$upload;
        }         
        $nuevoEnlace->save();
        alert()->success('!','Enlace Editado Exitosamente');
        return redirect()->route('mostrar_enlace' , [ $nuevoEnlace->id]);
    }

    public function destroyAdjunto(Request $request, $id){
      
        $tramite = Adjuntos::where('id', $id)->first();
        
        $result = $tramite->delete();
            if ($result) {
                return response()->json(['success'=>'true']);
            }else{
                return response()->json(['success'=> 'false']);
            }
    }

    public function galeriaFirma()
  {
    
       $idSecretaria = Auth::user()->id;
       $firmas = GaleriaFirmas::where('id_secretaria', $idSecretaria)->get(); 

       return view('documentos_digitales.galeria_firmas')
       ->with('firmas' , $firmas);
  }
   
  public function recursosApoyo(Request $request)
  {
      return view('documentos_digitales.recursos_apoyo');
  }
    
  public function manuales(Request $request)
  {
      return view('documentos_digitales.manuales');
  }

  public function preguntas(Request $request)
  {
      return view('documentos_digitales.preguntas');
  }

  public function plantillasPersonalizadas(Request $request)
  {
        $plantillasMemo = Plantillas::where('tipo_doc', 'Memorándum Electrónico')->orderby('nombre', 'asc')->get();      
        $plantillasOficio = Plantillas::where('tipo_doc', 'Oficio Electrónico')->get();      
        $plantillasResolucion = Plantillas::where('tipo_doc', 'Resolución Exenta Electrónica')->get();      

      return view('documentos_digitales.plantillas_personalizadas')
      ->with('plantillasMemo' , $plantillasMemo)
      ->with('plantillasOficio' , $plantillasOficio)
      ->with('plantillasResolucion' , $plantillasResolucion);
  }

  public function plantillasGenerales(Request $request)
  {
      return view('documentos_digitales.plantillas_generales');
  }
}