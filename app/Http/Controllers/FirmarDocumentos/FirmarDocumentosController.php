<?php

namespace App\Http\Controllers\FirmarDocumentos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use App\Http\Controllers\FirmarDocumentos\JWT;
use Illuminate\Support\Facades\Log;
use App\User;
use Auth;
use App\Models\DocumentosDigitales\Firmantes;
use App\Models\DocumentosDigitales\FirmantesSub;
use Illuminate\Support\Facades\Storage;
use Session;
use App\Models\TipoFirma\TipoFirma;
use App\Models\TiposDocumentos\TiposDocumentos;
use App\Models\DocumentosFirmados\DocumentosFirmados;
use App\Models\DocumentosFirmadosFirmantes\DocumentosFirmadosFirmantes;
use App\Http\Requests\FirmarDocumentos\FirmarDocumentosRequest;
use iio\libmergepdf\Merger;
use Clegginabox\PDFMerger\PDFMerger;
use GuzzleHttp\Client;
use Redirect;
use Laravel\Socialite\Facades\Socialite;
use App\Socialite\Two\ClaveUnicaProvider;

class FirmarDocumentosController extends Controller
{

    public function index()
    {
    $documentosFirmados = DocumentosFirmados::orderby('id', 'desc')->get();
      return view('firmar_documentos.firmar_documentos')
      ->with('documentosFirmados' , $documentosFirmados); 
    }

     public function claveUnica1()
    {
             return view('firmar_documentos.boton_clave_unica');
    }

      public function redirectToProvider()
    {
        /*$client_id = env('CLAVEUNICA_CLIENT_ID');
        $redirect = env('APP_ENV') == 'local' ? env('APP_URL') . env('CLAVEUNICA_REDIRECT') : secure_url(env('APP_URL') . env('CLAVEUNICA_REDIRECT'));
        $response_type = 'code';
        $hash = substr(md5(mt_rand()), 0, 30);
        $scope = 'openid run name email';
        $url = "https://accounts.claveunica.gob.cl/openid/authorize?client_id=$client_id&redirect_uri=$redirect&response_type=$response_type&scope=$scope&state=$hash";
        return Redirect::to($url);*/
        return Socialite::driver('claveunica')->redirect();
    }
    public function handleProviderCallback()
    {
        $user = Socialite::driver('claveunica')->user();
    }
    public function crear00(Request $request)
    {
        //$usuarioSesion = Firmantes::where('id_user' , Auth::user()->id)->first();
        $usuarioSesion = User::where('id' , Auth::user()->id)->first();
        $datosRut = Firmantes::where('id_user' , $usuarioSesion->id)->first();
        $datosRutSub = FirmantesSub::where('id_user' , $usuarioSesion->id)->first();
       
        if($datosRut){
            $rut = $datosRut->rut;
            $digitoVerificador = $datosRut->digito_v;
            $nombre = $datosRut->nombre;
        }else{
            $rut = $datosRutSub->rut;
            $digitoVerificador = $datosRutSub->digito_v;
            $nombre = $datosRutSub->nombre;
        }

        $correoElectronico = $usuarioSesion->email;
        

        $selectFirma = [0 => 'Primera Firma' , 1 =>'Añadir Firma'];

        
             return view('firmar_documentos.selec_firma')
             ->with('selectFirma' , $selectFirma)
             ->with('correoElectronico' , $correoElectronico)
             ->with('rut' , $rut)
             ->with('digitoVerificador' , $digitoVerificador)
             ->with('nombre' , $nombre);
    }

 
    
    public function crear(Request  $request){

        $rut = $request->rut;
        $dv = $request->dv;
        $nombreFirmante = $request->nombre;
        $correo = $request->correo;

        $tipoFirmafirma = $request->select_firma;
        $tipoFirma = TipoFirma::get();
        $tipoFirma = $tipoFirma->pluck('nombre','id')->prepend('Seleccione', 0);
        $TiposDocumentos = TiposDocumentos::where('asignacion_opa' , 'no')->get();
        $TiposDocumentos = $TiposDocumentos->pluck('nombre','id')->prepend('Seleccione', null);
        $reservados = [null=>'Seleccione' , 1=>'si' , 0=>'no'];
        if($tipoFirmafirma == 1){
            $id_doct = $request->id_doc;
            $docSearc = DocumentosFirmados::where('code' , $id_doct)->first();
             if (!$id_doct){
               return redirect()->back()->withErrors(['es necesario el id del documento']);
            } 
             if (!$docSearc){
               return redirect()->back()->withErrors(['Id de documento incorrecto o no encontrado']);
            } 

            return view('firmar_documentos.add_firmar_documentos')
           ->with('rut' , $rut)
           ->with('dv' , $dv)
           ->with('nombreFirmante' , $nombreFirmante)
           ->with('correo' , $correo)
           ->with('docSearc' , $docSearc)
           ->with('id_doct' , $id_doct)
           ->with('tipoFirma' , $tipoFirma)
           ->with('reservados' , $reservados)
           ->with('TiposDocumentos' , $TiposDocumentos);

        }else{
           return view('firmar_documentos.crear_firmar_documentos')
           ->with('rut' , $rut)
           ->with('dv' , $dv)
           ->with('nombreFirmante' , $nombreFirmante)
           ->with('correo' , $correo)
           ->with('tipoFirma' , $tipoFirma)
           ->with('reservados' , $reservados)
           ->with('TiposDocumentos' , $TiposDocumentos);
        }

    
    }

    public function firmar (FirmarDocumentosRequest $request){

        $rut = $request->rut;
        $dv = $request->dv;
        $nombreFirmante = $request->nombreFirmante;
        $correo = $request->correo;
        
         if ($request->tipo_firma == 0){
               return redirect()->back()->withErrors(['Tipo de firma requerida']); 
        }
         if ($request->tipo_documento == 0){
               return redirect()->back()->withErrors(['Tipo de documento requerido']); 
        }
          
       if(!$request->file('contenido_principal')){
        return redirect()->back()->withErrors(['El archivo es requerido']); 
       }

        $purpose = TipoFirma::where('id' , $request->tipo_firma)->first()->valor; 
     
        $rut=$request->rut;
        $nombreDocumentoR = $request->nombre;
        $descripcionDocumentoR = $request->descripcion;
        $id_tipo_firma = $request->tipo_firma;
        $folio = $request->folio;
        $id_tipo_documento = $request->tipo_documento+10; //parche
        $reservado = 0;
             if ($request->get('reservado')==null){
                $reservado = 0;
            } else {
                $reservado = 1;
            }
            
        $user = User::findOrFail(Auth::user()->id);
        $disco = Storage::disk('nofirmados');
        $entity='Subsecretaría del Deporte';

        $extencion =  $request->file('contenido_principal')->getClientOriginalExtension();

        $docNombre = now()->format('Y_m_d_s').'_'.sha1($nombreDocumentoR).'.'.$request->file('contenido_principal')->getClientOriginalExtension();

        $path = $request->file('contenido_principal')->store(null, 'nofirmados');


        $url_documento_no_firmado = public_path().'/nofirmados/'.$path;
        
        $nombreDocVerificacionFirma = substr(md5(mt_rand()), 0, 32);
        $code = substr(md5(mt_rand()), 0, 8);
        $nomNoFir = substr(md5(mt_rand()), 0, 12);

        $dataView = array(
                        "codigo" => $code,
                        "fecha" => Carbon::now() ,
                        "hash"=>$nombreDocVerificacionFirma , 
                        "url"=>'std.mindep.cl/ValidarDocumentos'
                    );

        $ruta = public_path().'/verificacion_firma/'.$nombreDocVerificacionFirma.'.pdf';
        $d = \PDF::loadView('pdf.codigo_verificacion',compact('dataView'))->save($ruta);

        $fileArray= array($url_documento_no_firmado,$ruta);
                    $datadir = public_path()."/nofirmados/";
                    $outputName = $datadir.$nomNoFir.".pdf";

                    $cmd = "gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=$outputName ";
                    foreach($fileArray as $file) {
                        $cmd .= $file." ";
                    }
                    $result = Shell_exec($cmd);

        

        $encode = base64_encode(file_get_contents($outputName));
        $checksumfile = hash_file("sha256" , $outputName);
      

        $otp = NULL; 
        $file_path = public_path().'/firmados/'.$docNombre;

        try {
                $entity= $entity;
                $rut = $rut;
                $expiration = Carbon::now()->addMinute(5)->format('Y-m-d\TH:i:s');
                $purpose='Desatendido';

                $dataf = array(
                        "entity" => $entity,
                        "run" => $rut,
                        "expiration" => $expiration,
                        "purpose" => $purpose
                    );
        }catch(Exception $e) {
            alert()->error('!','Ha ocurrido un error intente de nuevo');
            return redirect()->route('firmarDocumentos');
        }

 try {
         $data['token'] = JWT::encode($dataf, env('JWT_SECRET'));
         $url = env('JWT_URL_API_FIRMA_V2');
         $data['api_token_key'] = env('JWT_API_TOKEN_KEY');
       
        $data['files'] = array(array(
                "description" => $nombreDocumentoR,
                "content-type" => "application/pdf",
                "checksum" => $checksumfile,
                "content" =>$encode
                //"layout" =>$layoutPeticion
            ));
        //dd(json_encode($data));
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=UTF-8'));
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0');
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

        } catch(Exception $e) {
            Log::info('Error al inicializar datos para la peticion del primer curl \n\n', [
                'error' => $e
            ]);

            alert()->error('!','Ha ocurrido un error intente de nuevo');
            return redirect()->route('firmarDocumentos');
        }
        try {
                    $result = curl_exec($ch);
                    //dd($result);

                            $resultJson = json_decode($result);
                            $idSolicitudFirma = $resultJson->idSolicitud;
                            $datosDocumento = $resultJson->files[0];
                            $documentoB64 = $datosDocumento->content;
                            $statusB64 = $datosDocumento->status;
                            $documentStatus = $datosDocumento->documentStatus;
                    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    $err = curl_error($ch);
                    curl_close($ch);

                    if ($err != null)  {
                        Log::info('Error al ejecutar la primera peticion a FIRMA:\n\n', [
                            'status' => $httpcode,
                            'error' => $err
                        ]);

                        alert()->error('!','Ha ocurrido un error intente de nuevo');
                       // return redirect()->route('firmarDocumentos');
                    }

                } catch(Exception $e) {
                    Log::info('Error al ejecutar la primera peticion a FIRMA:\n\n', [
                        'error' => $e
                    ]);

                    alert()->error('!','Ha ocurrido un error intente de nuevo');
                    //return redirect()->route('firmarDocumentos');
                }

        try {
                    file_put_contents($file_path, base64_decode($documentoB64));
                    $url_documento_firmado = $file_path;
                  


                    $nuevoDocumentoFirmado = new DocumentosFirmados();
                    $nuevoDocumentoFirmado->nombre = $nombreDocumentoR;
                    $nuevoDocumentoFirmado->descripcion = $descripcionDocumentoR;
                    $nuevoDocumentoFirmado->id_tipo_firma = $id_tipo_firma;
                    $nuevoDocumentoFirmado->rut_firmante = $rut;
                    $nuevoDocumentoFirmado->folio_documento = $folio;
                    $nuevoDocumentoFirmado->id_tipo_documento = $id_tipo_documento;
                    $nuevoDocumentoFirmado->reservado = $reservado;
                    $nuevoDocumentoFirmado->url_documento_no_firmado = $path;
                    $nuevoDocumentoFirmado->url_documento_firmado = $docNombre;
                    $nuevoDocumentoFirmado->autorizacion = 0;
                    $nuevoDocumentoFirmado->save();


                    $nuevoDocumentoFirmado->code = $code;
                    $nuevoDocumentoFirmado->hash_autenticador = $nombreDocVerificacionFirma;
                    $nuevoDocumentoFirmado->save();



                    $nuevoFirmante =  new DocumentosFirmadosFirmantes();
                    $nuevoFirmante->documento_firmado_id =  $nuevoDocumentoFirmado->id;
                    $nuevoFirmante->usuario_id =  auth()->user()->id;
                    $nuevoFirmante->documento_firmado_code = $code;
                    $nuevoFirmante->rut_firmante = $rut.'-'.$dv;
                    $nuevoFirmante->nombre_firmante = $nombreFirmante;
                    $nuevoFirmante->save();

                Log::info('El documento si ha sido firmado');

            
        } catch(Exception $e) {
            Log::info('Error al procesar peticion a FIRMA:\n\n', [
                'error' => $e
            ]);
            alert()->error('!','Ha ocurrido un error intente de nuevo');
            return redirect()->route('firmarDocumentos');
        }
   
        return response()->download($url_documento_firmado);
    }


    public function testDoc(){

        $qrcode = "std.mindep.cl/ValidarDocumentos";
        $pdf = \PDF::loadView('pdf.codigo_verificacion_prueba' , compact('qrcode'));
     return $pdf->download('ejemplo.pdf');
    }


     public function firmarv2 (FirmarDocumentosRequest $request)
     {
      $rut = $request->rut;
      $dv = $request->dv;
      $nombreFirmante = $request->nombreFirmante;
      $correo = $request->correo;


        $oldDoc = DocumentosFirmados::where('code' , $request->id_doc)->first();

         if ($request->tipo_firma == 0){
               return redirect()->back()->withErrors(['Tipo de firma requerida']); 
        }
  
   
       if(!$request->file('contenido_principal')){
        return redirect()->back()->withErrors(['El archivo es requerido']); 
       }

        $purpose = TipoFirma::where('id' , $request->tipo_firma)->first()->valor; 
        $rut=$request->rut;
        $nombreDocumentoR = $oldDoc->nombre;
        $descripcionDocumentoR = $oldDoc->descripcion;
        $id_tipo_firma = $oldDoc->id_tipo_firma;
        $folio = $oldDoc->folio_documento;
        $id_tipo_documento = $oldDoc->id_tipo_documento;
        $reservado = $oldDoc->reservado;

        $id_doc = $request->id_doc;
        $user = User::findOrFail(Auth::user()->id);
        $disco = Storage::disk('nofirmados');
        $entity='Subsecretaría del Deporte';

        $extencion =  $request->file('contenido_principal')->getClientOriginalExtension();

        $docNombre = sha1($nombreDocumentoR).'.'.$request->file('contenido_principal')->getClientOriginalExtension();

        $path = $request->file('contenido_principal')->store(null, 'nofirmados');

        
        $url_documento_no_firmado = public_path().'/nofirmados/'.$path;
       

        $nombreDocVerificacionFirma = substr(md5(mt_rand()), 0, 32);
        $code = substr(md5(mt_rand()), 0, 8);
        $nomNoFir = substr(md5(mt_rand()), 0, 12);

        /*$dataView = array(
                        "codigo" => $code,
                        "fecha" => Carbon::now() ,
                        "hash"=>$nombreDocVerificacionFirma , 
                        "url"=>'std.mindep.cl/ValidarDocumentos'
                    );*/

        //$ruta = public_path().'/verificacion_firma/'.$nombreDocVerificacionFirma.'.pdf';
        //$d = \PDF::loadView('pdf.codigo_verificacion',compact('dataView'))->save($ruta);

        //$fileArray= array($url_documento_no_firmado,$ruta);
                    $datadir = public_path()."/nofirmados";
                    $outputName = $datadir.$nomNoFir."pdf";

                    /*$cmd = "gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=$outputName ";
                    foreach($fileArray as $file) {
                        $cmd .= $file." ";
                    }
                    $result = Shell_exec($cmd);*/


        $encode = base64_encode(file_get_contents($url_documento_no_firmado));
        $checksumfile = hash_file("sha256" , $url_documento_no_firmado);
      

        $otp = NULL; 
        $file_path = public_path().'/firmados/'.$docNombre;

        try {
                $entity= $entity;
                $rut = $rut;
                $expiration = '2020-12-31T10:00:09';
                $purpose=$purpose;

                $dataf = array(
                        "entity" => $entity,
                        "run" => $rut,
                        "expiration" => $expiration,
                        "purpose" => $purpose
                    );
        }catch(Exception $e) {
            alert()->error('!','Ha ocurrido un error intente de nuevo');
            return redirect()->route('firmarDocumentos');
        }

 try {
         $data['token'] = JWT::encode($dataf, env('JWT_SECRET'));
         $url = env('JWT_URL_API_FIRMA');
         $data['api_token_key'] = env('JWT_API_TOKEN_KEY');
       
        $data['files'] = array(array(
                "description" => $nombreDocumentoR,
                "content-type" => "application/pdf",
                "checksum" => $checksumfile,
                "content" =>$encode,
            ));
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=UTF-8'));
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0');
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

        } catch(Exception $e) {
            Log::info('Error al inicializar datos para la peticion del primer curl \n\n', [
                'error' => $e
            ]);

            alert()->error('!','Ha ocurrido un error intente de nuevo');
            return redirect()->route('firmarDocumentos');
        }
        try {
                    $result = curl_exec($ch);
                    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    $err = curl_error($ch);
                    curl_close($ch);

                    if ($err != null)  {
                        Log::info('Error al ejecutar la primera peticion a FIRMA:\n\n', [
                            'status' => $httpcode,
                            'error' => $err
                        ]);

                        alert()->error('!','Ha ocurrido un error intente de nuevo');
                        return redirect()->route('firmarDocumentos');
                    }

                } catch(Exception $e) {
                    Log::info('Error al ejecutar la primera peticion a FIRMA:\n\n', [
                        'error' => $e
                    ]);

                    alert()->error('!','Ha ocurrido un error intente de nuevo');
                    return redirect()->route('firmarDocumentos');
                }



        
          try {
       
            $dataresult = json_decode($result);
            if(!isset($dataresult->session_token)){
                Log::info('Error al ejecutar la primera peticion a FIRMA: Token inválido\n\n');
                alert()->error('!','Ha ocurrido un error intente de nuevo');
                return redirect()->route('firmarDocumentos');
            }
            $session_token = $dataresult->session_token;

            Log::info('Continuando procedimiento de firma, segundo request => \n', [
                'run' => $rut,
                 'entity' => $entity,
            ]);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url . "/" . $session_token);

            if (is_null($otp)) {
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=UTF-8'));
            } else {
                $headers = [
                    'Content-Type: application/json; charset=utf-8',
                    'OTP: ' . $otp
                ];
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            }
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            // ejecutando segundo request
            $result = curl_exec($ch);

            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $err = curl_error($ch);
            curl_close($ch);

            if ($err != null)  {
                Log::info('Error al ejecutar la segunda peticion a FIRMA:\n\n', [
                    'status' => $httpcode,
                    'error' => $err
                ]);

                alert()->error('!','Ha ocurrido un error intente de nuevo');
                return redirect()->route('firmarDocumentos');
            }

        } catch(Exception $e) {
            Log::info('Error al ejecutar la primera peticion a FIRMA:\n\n', [
                'error' => $e
            ]);

            alert()->error('!','Ha ocurrido un error intente de nuevo');
            return redirect()->route('firmarDocumentos');
        }


        try {
            $dataresult = json_decode($result);
            $fileresult = $dataresult->files;
            $metadata = $dataresult->metadata;

            Log::info('Metadata Segunda peticion FIRMA:\n\n', [
                'metadata' => $metadata
            ]);

            if ($metadata->files_signed == 1) {
                foreach ($fileresult as $archivo) {
                    file_put_contents($file_path, base64_decode($archivo->content));
                    $url_documento_firmado = $file_path;
                  


                    $nuevoDocumentoFirmado = DocumentosFirmados::where('code' , $id_doc)->first();
                    $nuevoDocumentoFirmado->nombre = $oldDoc->nombre;
                    $nuevoDocumentoFirmado->descripcion = $oldDoc->descripcion;
                    $nuevoDocumentoFirmado->id_tipo_firma = $oldDoc->id_tipo_firma;
                    $nuevoDocumentoFirmado->rut_firmante = $oldDoc->rut_firmante;
                    $nuevoDocumentoFirmado->folio_documento = $oldDoc->folio_documento;
                    $nuevoDocumentoFirmado->id_tipo_documento = $oldDoc->id_tipo_documento;
                    $nuevoDocumentoFirmado->reservado = $oldDoc->reservado;
                    $nuevoDocumentoFirmado->url_documento_no_firmado = $path;
                    $nuevoDocumentoFirmado->url_documento_firmado = $docNombre;
                    $nuevoDocumentoFirmado->autorizacion = 0;
                    $nuevoDocumentoFirmado->save();


                    $nuevoDocumentoFirmado->code =$oldDoc->code; 
                    $nuevoDocumentoFirmado->hash_autenticador = $oldDoc->hash_autenticador;
                    $nuevoDocumentoFirmado->save();

                    $nuevoFirmante =  new DocumentosFirmadosFirmantes();
                    $nuevoFirmante->documento_firmado_id =  $nuevoDocumentoFirmado->id;
                    $nuevoFirmante->usuario_id =  auth()->user()->id;
                    $nuevoFirmante->documento_firmado_code = $id_doc;
                    $nuevoFirmante->rut_firmante = $rut.'-'.$dv;
                    $nuevoFirmante->nombre_firmante = $nombreFirmante;
                    $nuevoFirmante->save();

                }
                Log::info('El documento si ha sido firmado');

            } else {
                Log::info('El documento no ha sido firmado');

                alert()->error('!','Ha ocurrido un error intente de nuevo');
                return redirect()->route('firmarDocumentos');
            }
        } catch(Exception $e) {
            Log::info('Error al procesar la segunda peticion a FIRMA:\n\n', [
                'error' => $e
            ]);

            alert()->error('!','Ha ocurrido un error intente de nuevo');
            return redirect()->route('firmarDocumentos');
        }
   
  
    //return response()->download($file_path);
        return response()->download($url_documento_firmado);
    }
}
