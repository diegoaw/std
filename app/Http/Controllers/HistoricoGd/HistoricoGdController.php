<?php

namespace App\Http\Controllers\HistoricoGd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\HistoricoGd\HistoricoGd;
use App\Models\HistoricoGdArchivos\HistoricoGdArchivos;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;


class HistoricoGdController extends Controller
{
   public function index(Request $request){
    set_time_limit(14200);
    $Departamento = '';
    $Division = '';
    $Nombre = '';
    $Usuario = '';
    $Cargo = '';
    $Correo = '';
    $OrigendelDocto = '';
    $Remitente = '';
    $CargoRemitente = '';
    $Procedencia = '';
    $Tipo = '';
    $Numero = '';
    $Contenido = '';
    $TipoContenido = '';
    $Expediente = '';


    $query = HistoricoGd::query ();

      $query->when(request('Departamento'), function ($q, $Departamento) {
            return $q->where('Departamento', 'LIKE', "%$Departamento%");
        });
      $query->when(request('Division'), function ($q, $Division) {
            return $q->where('Division', 'LIKE', "%$Division%");
        });
       $query->when(request('Nombre'), function ($q, $Nombre) {
            return $q->where('Nombre', 'LIKE', "%$Nombre%");
        });
      $query->when(request('Usuario'), function ($q, $Usuario) {
            return $q->where('Usuario', 'LIKE', "%$Usuario%");
        });
      $query->when(request('Cargo'), function ($q, $Cargo) {
            return $q->where('Cargo', 'LIKE', "%$Cargo%");
        });
      $query->when(request('Correo'), function ($q, $Correo) {
            return $q->where('Correo', 'LIKE', "%$Correo%");
        });
      $query->when(request('OrigendelDocto'), function ($q, $OrigendelDocto) {
            return $q->where('OrigendelDocto', 'LIKE', "%$OrigendelDocto%");
        });
      $query->when(request('Remitente'), function ($q, $Remitente) {
            return $q->where('Remitente', 'LIKE', "%$Remitente%");
        });
      $query->when(request('CargoRemitente'), function ($q, $CargoRemitente) {
            return $q->where('CargoRemitente', 'LIKE', "%$CargoRemitente%");
        });
      $query->when(request('Procedencia'), function ($q, $Procedencia) {
            return $q->where('Procedencia', 'LIKE', "%$Procedencia%");
        });
      $query->when(request('Tipo'), function ($q, $Tipo) {
            return $q->where('Tipo', 'LIKE', "%$Tipo%");
        });
      $query->when(request('Numero'), function ($q, $Numero) {
            return $q->where('Numero', 'LIKE', "%$Numero%");
        });
      $query->when(request('Contenido'), function ($q, $Contenido) {
            return $q->where('Contenido', 'LIKE', "%$Contenido%");
        });
      $query->when(request('TipoContenido'), function ($q, $TipoContenido) {
            return $q->where('TipoContenido', 'LIKE', "%$TipoContenido%");
        });
      $query->when(request('Expediente'), function ($q, $Expediente) {
            return $q->where('Expediente', 'LIKE', "%$Expediente%");
        });



     /*$historico = $query->get()->sortByDesc(function($q){
     return $q->FechaCarpeta;
     });*/

        //$page = Input::get('page');
        //$posts = range(1, 20);

        //$historico = new Paginator($historico, 10, $page);
        //$historico->setPath('HistoricoGD');



      $historico = $query->orderBy('FechaCarpeta', 'DESC')->paginate(100);

       if( $request->Departamento != null ) {
        $Departamento = $request->Departamento;
       }
       if( $request->Division != null ) {
        $Division = $request->Division;
       }
       if( $request->Nombre != null ) {
        $Nombre = $request->Nombre;
       }
       if( $request->Usuario != null ) {
        $Usuario = $request->Usuario;
       }
       if( $request->Cargo != null ) {
        $Cargo = $request->Cargo;
       }
       if( $request->Correo != null ) {
        $Correo = $request->Correo;
       }
       if( $request->OrigendelDocto != null ) {
        $OrigendelDocto = $request->OrigendelDocto;
       }
       if( $request->Remitente != null ) {
        $Remitente = $request->Remitente;
       }
       if( $request->CargoRemitente != null ) {
        $CargoRemitente = $request->CargoRemitente;
       }
       if( $request->Procedencia != null ) {
        $Procedencia = $request->Procedencia;
       }
       if( $request->Tipo != null ) {
        $Tipo = $request->Tipo;
       }
       if( $request->Numero != null ) {
        $Numero = $request->Numero;
       }
       if( $request->Contenido != null ) {
        $Contenido = $request->Contenido;
       }
       if( $request->TipoContenido != null ) {
        $TipoContenido = $request->TipoContenido;
       }
       if( $request->Expediente != null ) {
        $Expediente = $request->Expediente;
       }


//dd($historico[1]->UrlArchivo);

    return view('historico_gd.historico_gd')
    ->with('historico',$historico)
    ->with('Departamento',$Departamento)
    ->with('Division',$Division)
    ->with('Nombre',$Nombre)
    ->with('Usuario',$Usuario)
    ->with('Cargo',$Cargo)
    ->with('Correo',$Correo)
    ->with('OrigendelDocto',$OrigendelDocto)
    ->with('Remitente',$Remitente)
    ->with('CargoRemitente',$CargoRemitente)
    ->with('Procedencia',$Procedencia)
    ->with('Tipo',$Tipo)
    ->with('Numero',$Numero)
    ->with('Contenido',$Contenido)
    ->with('TipoContenido',$TipoContenido)
    ->with('Expediente',$Expediente);
    }


    public function indexarchivos(Request $request)
    {
       set_time_limit(14200);
       $ano = '';
       $confidencialidad = '';
       $clasificacion = '';
       $materia = '';
       $fechaModificacion = '';
       $anno = Carbon::now()->format('Y');
       $fechaStringInicio = '2014-01-01 00:00:00';
       $feDesde = Carbon::parse($fechaStringInicio);
       $feHasta =  Carbon::parse(Carbon::now()->format('Y-m-d').' 23:59:59');
       $fechaInDesde = Carbon::parse($fechaStringInicio);
       $fechaInHasta = Carbon::now();
       $fechas[1] = Date('Y-m-d', strtotime( '1970-01-01' ));
       $fechas[0] = Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
       $archivoNombre = '';

       $selectAno[null] = 'SELECCIONE AÑO'; foreach (HistoricoGdArchivos::whereNotNull('ano')->orderby('id', 'desc')->distinct()->get() as $key0 => $value0) { $selectAno[$value0->ano]=$value0->ano;}
       $selectConfi[null] = 'SELECCIONE CONFIDENCIALIDAD'; foreach (HistoricoGdArchivos::whereNotNull('confidencialidad')->distinct()->get() as $key0 => $value0) { $selectConfi[$value0->confidencialidad]=$value0->confidencialidad;}
       $selectClasi[null] = 'SELECCIONE CLASIFICACION'; foreach (HistoricoGdArchivos::whereNotNull('clasificacion')->distinct()->get() as $key0 => $value0) { $selectClasi[$value0->clasificacion]=$value0->clasificacion;}


       if( $request->FechaDesde != null ) {
         $feDesde=(Carbon::parse($request->get('FechaDesde'))->format('dmY'));
         $feDesde= Carbon::parse(substr($feDesde, 0, 2).'-'.substr($feDesde, 2, 2).'-'.substr($feDesde, 4, 4).' 00:00:00');
       }
       if( $request->FechaHasta != null ) {
          $feHasta=(Carbon::parse($request->get('FechaHasta'))->format('dmY'));
          $feHasta= Carbon::parse(substr($feHasta, 0, 2).'-'.substr($feHasta, 2, 2).'-'.substr($feHasta, 4, 4).' 23:59:59');
       }

       $query = HistoricoGdArchivos::query ();

         $query->when(request('ano'), function ($q, $ano) {
               return $q->where('ano',$ano);
           });
         $query->when(request('confidencialidad'), function ($q, $confidencialidad) {
               return $q->where('confidencialidad', 'LIKE', "%$confidencialidad%");
           });
          $query->when(request('clasificacion'), function ($q, $clasificacion) {
               return $q->where('clasificacion', 'LIKE', "%$clasificacion%");
           });
         $query->when(request('materia'), function ($q, $materia) {
               return $q->where('materia', 'LIKE', "%$materia%");
           });
         $query->when(request('fechaModificacion'), function ($q, $fechaModificacion) {
               return $q->where('fecha_modificacion', 'LIKE', "%$fechaModificacion%");
           });
         $query->when($feDesde, function ($q, $feDesde ) {
                 return $q->where('fecha_modificacion','>=', $feDesde);
           });
         $query->when($feHasta, function ($q, $feHasta ) {
                 return $q->where('fecha_modificacion','<=', $feHasta);
             });
         $query->when(request('archivoNombre'), function ($q, $archivoNombre) {
               return $q->where('archivo_nombre', 'LIKE', "%$archivoNombre%");
           });

         $historico = $query->orderBy('ano', 'DESC')->paginate(100);

          if( $request->ano != null ) {
           $ano = $request->ano;
          }
          if( $request->confidencialidad != null ) {
           $confidencialidad = $request->confidencialidad;
          }
          if( $request->clasificacion != null ) {
           $clasificacion = $request->clasificacion;
          }
          if( $request->materia != null ) {
           $materia = $request->materia;
          }
          if( $request->fechaModificacion != null ) {
           $fechaModificacion = $request->fechaModificacion;
          }
          if( $request->archivoNombre != null ) {
           $archivoNombre = $request->archivoNombre;
          }

      return view('historico_gd.historico_gd_archivos')
      ->with('historico',$historico)
      ->with('ano',$ano)
      ->with('selectAno',$selectAno)
      ->with('selectClasi',$selectClasi)
      ->with('selectConfi',$selectConfi)
      ->with('confidencialidad',$confidencialidad)
      ->with('clasificacion',$clasificacion)
      ->with('materia',$materia)
      ->with('feDesde' ,$feDesde)
      ->with('feHasta' ,$feHasta)
      ->with('fechaInDesde' ,$fechaInDesde)
      ->with('fechaInHasta' ,$fechaInHasta)
      ->with('fechas' ,$fechas)
      ->with('fechaModificacion',$fechaModificacion)
      ->with('archivoNombre',$archivoNombre);
    }

    public function verificararchivo(Request $request)
    {
      //dd('aqui');
      $verificaArchivos = HistoricoGdArchivos::get();
      $client = new Client();
        foreach ($verificaArchivos as $key => $value) {
          try {
            $request = $client->get("https://historicogd.mindep.cl/$value->ruta");
            $response = $request->getBody()->getContents();
            //$response2 = $request->send();
            //dd($response->getStatusCode());
            $data =  json_decode($response);
            if ($data) {

              echo "https://historicogd.mindep.cl/$value->ruta";
              dd($response, $data);
            }
            //dd($data);
          }catch (BadResponseException   $e) {
            $response = $e->getResponse();
            dd($response);
        }
      }
  }


}
