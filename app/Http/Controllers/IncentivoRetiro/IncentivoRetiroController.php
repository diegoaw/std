<?php

namespace App\Http\Controllers\IncentivoRetiro;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use GuzzleHttp\Client;
use App\Models\IncentivoRetiro\IncentivoRetiro;


class IncentivoRetiroController extends Controller
{

      public function incentivoretiro(Request $request){


          $tramites = [];
          $idTramiteFiltro = $request->nRegistro;
          $queryFilter = "where r.numero_tramite = $idTramiteFiltro ";
          
               $query = "SELECT  *
                   FROM
                  (select
                          tramite.id as numero_tramite ,
                          tramite.proceso_id as numero_proeceso ,
                          proceso.nombre as nombre_proceso ,
                          tramite.created_at as fecha_inicio ,
                          tramite.updated_at as fecha_ultima_modificacion ,
                          tramite.pendiente as pendiente,
                          tramite.ended_at as fecha_culmino,
                          tarea.nombre as etapa,
                          et.id as id_etapa,
                          dts.conteo as cantidad_campos
                          from tramite
                          INNER JOIN proceso ON tramite.proceso_id = proceso.id
                          INNER JOIN (SELECT * FROM etapa WHERE id IN (SELECT MAX(id) FROM etapa GROUP BY tramite_id) ORDER BY id DESC) et on et.tramite_id = tramite.id
                          INNER JOIN tarea on et.tarea_id = tarea.id
                          LEFT JOIN (SELECT  COUNT(id) as conteo, etapa_id FROM dato_seguimiento  group by etapa_id ) dts on dts.etapa_id = et.id
                          where tramite.proceso_id = 32  and tarea.nombre = 'Solicitud Incentivo al Retiro' and tramite.deleted_at is null and dts.conteo >3 
                          
          UNION ALL
          select
                          tramite.id as numero_tramite ,
                          tramite.proceso_id as numero_proeceso ,
                          proceso.nombre as nombre_proceso ,
                          tramite.created_at as fecha_inicio ,
                          tramite.updated_at as fecha_ultima_modificacion ,
                          tramite.pendiente as pendiente,
                          tramite.ended_at as fecha_culmino,
                          tarea.nombre as etapa,
                          et.id as id_etapa,
                          dts.conteo as cantidad_campos
                          from tramite
                          INNER JOIN proceso ON tramite.proceso_id = proceso.id
                          INNER JOIN (SELECT * FROM etapa WHERE id IN (SELECT MAX(id) FROM etapa GROUP BY tramite_id) ORDER BY id DESC) et on et.tramite_id = tramite.id
                          INNER JOIN tarea on et.tarea_id = tarea.id
                          LEFT JOIN (SELECT  COUNT(id) as conteo, etapa_id FROM dato_seguimiento  group by etapa_id ) dts on dts.etapa_id = et.id
                          where tramite.proceso_id = 32
                          and tramite.deleted_at is null
                          and tarea.nombre <> 'Solicitud Incentivo al Retiro') r ";
          
              ($idTramiteFiltro != null )? $query = $query.$queryFilter : $query = $query;
              $query = $query."order by r.numero_tramite DESC";
          
              $tramites = DB::connection('mysqlSimple')->select($query);
              return view('incentivo_retiro.incentivo_retiro')
              ->with('tramitesD', $tramites)
              ->with('idTramiteFiltro', $idTramiteFiltro);
          
              }
              

          public function incentivoretiroShow($id)
          {
                        
              $client = new Client();
                    try {
                    $request = $client->get("https://simple.mindep.cl/backend/api/tramites/$id?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu " , ['verify' => false]);
                    $response = $request->getBody()->getContents();
                    $data =  json_decode($response);
                      }catch (BadResponseException   $e) {
                          $response = $e->getResponse();
                      }
              $data = $data->tramite;
       
              $etapasCont = count($data->etapas);

              $nuevaIncentivoRetiro = new IncentivoRetiro();
              $nuevaIncentivoRetiro->id =  $data->id;
              $nuevaIncentivoRetiro->fecha_inicio =  $data->fecha_inicio;
              $nuevaIncentivoRetiro->fecha_termino =  $data->fecha_termino;
              $nuevaIncentivoRetiro->fecha_modificacion =  $data->fecha_modificacion;
              $nuevaIncentivoRetiro->estado =  $data->estado;
              
              $datos=$data->datos;            
if($datos){
               foreach ($datos as $key => $value2) {               

                     if(property_exists($value2, 'fecha_solicitud')){
                        $nuevaIncentivoRetiro->fecha_solicitud =  $value2->fecha_solicitud;
                     }

                     if(property_exists($value2, 'seleccione')){
                       $nuevaIncentivoRetiro->seleccione =  $value2->seleccione;
                     }

                     if(property_exists($value2, 'cargo')){
                       $nuevaIncentivoRetiro->cargo =  $value2->cargo;
                     }

                     if(property_exists($value2, 'division')){
                       $nuevaIncentivoRetiro->division =  $value2->division;
                     }

                     if(property_exists($value2, 'correo')){
                       $nuevaIncentivoRetiro->correo =  $value2->correo;
                     }

                     if(property_exists($value2, 'rut')){
                       $nuevaIncentivoRetiro->rut =  $value2->rut;
                     }

                     if(property_exists($value2, 'calidad_juridica')){
                       $nuevaIncentivoRetiro->calidad_juridica =  $value2->calidad_juridica;
                     }

                     if(property_exists($value2, 'grado')){
                       $nuevaIncentivoRetiro->grado =  $value2->grado;
                     }

                     if(property_exists($value2, 'fecha_nacimiento')){
                       $nuevaIncentivoRetiro->fecha_nacimiento =  $value2->fecha_nacimiento;
                     }
                    
                     if(property_exists($value2, 'rango')){
                        $nuevaIncentivoRetiro->rango =  $value2->rango;
                     }

                     if(property_exists($value2, 'selec_previ')){
                       $nuevaIncentivoRetiro->selec_previ =  $value2->selec_previ;
                     }  
                     
                     if(property_exists($value2, 'nombre_afp_18')){
                        $nuevaIncentivoRetiro->nombre_afp_18 =  $value2->nombre_afp_18;
                      }                       
                    
                     if(property_exists($value2, 'nombre_afp')){
                       $nuevaIncentivoRetiro->nombre_afp =  $value2->nombre_afp;
                     }

                     if(property_exists($value2, 'afiliacion_afp_18')){
                       $nuevaIncentivoRetiro->afiliacion_afp_18 =  $value2->afiliacion_afp_18->URL;
                     }
                     
                     if(property_exists($value2, 'afiliacion_afp')){
                        $nuevaIncentivoRetiro->afiliacion_afp =  $value2->afiliacion_afp->URL;
                     }

                     if(property_exists($value2, 'cotixaciones_afp')){
                        $nuevaIncentivoRetiro->cotixaciones_afp =  $value2->cotixaciones_afp->URL;
                     }

                     if(property_exists($value2, 'certificado_antiguedad')){
                        $nuevaIncentivoRetiro->certificado_antiguedad =  $value2->certificado_antiguedad->URL;
                     }
                    
                     if(property_exists($value2, 'manifiesto')){
                        $nuevaIncentivoRetiro->manifiesto =  $value2->manifiesto;
                     } 

                     if(property_exists($value2, 'declaracion_jurada')){
                        $nuevaIncentivoRetiro->declaracion_jurada =  $value2->declaracion_jurada->URL;
                     }

                     if(property_exists($value2, 'carta_renuncia')){
                        $nuevaIncentivoRetiro->carta_renuncia =  $value2->carta_renuncia->URL;
                     }

                     if(property_exists($value2, 'genero')){
                        $nuevaIncentivoRetiro->genero =  $value2->genero;
                     } 

                     if(property_exists($value2, 'certificado_nacimiento')){
                        $nuevaIncentivoRetiro->certificado_nacimiento =  $value2->certificado_nacimiento->URL;
                     }

                     if(property_exists($value2, 'edad_f')){
                        $nuevaIncentivoRetiro->edad_f =  $value2->edad_f;
                     } 

                     if(property_exists($value2, 'edad_m')){
                        $nuevaIncentivoRetiro->edad_m =  $value2->edad_m;
                     } 

                     if(property_exists($value2, 'edad_o')){
                        $nuevaIncentivoRetiro->edad_o =  $value2->edad_o;
                     } 

                     if(property_exists($value2, 'formulario_firmado')){
                        $nuevaIncentivoRetiro->formulario_firmado =  $value2->formulario_firmado->URL;
                     } 
                    
                     if(property_exists($value2, 'comprobante')){
                        $nuevaIncentivoRetiro->comprobante = 'https://simple.mindep.cl/uploads/documentos/'.$value2->comprobante;
                     } 
                    
                     if(property_exists($value2, 'fecha_revision')){
                        $nuevaIncentivoRetiro->fecha_revision =  $value2->fecha_revision;
                     } 

                     if(property_exists($value2, 'cumple_requisitos')){
                        $nuevaIncentivoRetiro->cumple_requisitos =  $value2->cumple_requisitos;
                     }

                     if(property_exists($value2, 'observaciones_correccion')){
                        $nuevaIncentivoRetiro->observaciones_correccion =  $value2->observaciones_correccion;
                     }

                     if(property_exists($value2, 'fecha_correccion')){
                        $nuevaIncentivoRetiro->fecha_correccion =  $value2->fecha_correccion;
                     }                      
                   

                 }
                }

            return view('incentivo_retiro.incentivo_retiro_detalle')
            ->with('data' , $nuevaIncentivoRetiro);
          }


          
}