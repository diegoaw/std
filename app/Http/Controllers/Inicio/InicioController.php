<?php

namespace App\Http\Controllers\Inicio;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use App\Models\DocumentosDigitales\CorrelativoDocumentosDigitales;

class InicioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
      $ultimoMemo =  CorrelativoDocumentosDigitales::where('tipo_documento_id' ,16)->max('correlativo');
      $ultimoOficio = CorrelativoDocumentosDigitales::where('tipo_documento_id' ,6)->max('correlativo');
      $ultimaResolucion = CorrelativoDocumentosDigitales::where('tipo_documento_id' ,2)->max('correlativo');
      $ultimoDecreto = CorrelativoDocumentosDigitales::where('tipo_documento_id' ,3)->max('correlativo');
      $ultimoDecretoSupremo = CorrelativoDocumentosDigitales::where('tipo_documento_id' ,4)->max('correlativo');
      $ultimoDecretoExento = CorrelativoDocumentosDigitales::where('tipo_documento_id' ,5)->max('correlativo');
      
      
      return view('inicio.in_01_i_inicio')
      ->with('ultimoMemo' , $ultimoMemo)
      ->with('ultimoOficio' , $ultimoOficio)
      ->with('ultimaResolucion' , $ultimaResolucion)
      ->with('ultimoDecreto' , $ultimoDecreto)
      ->with('ultimoDecretoSupremo' , $ultimoDecretoSupremo)
      ->with('ultimoDecretoExento' , $ultimoDecretoExento);
    }
}
