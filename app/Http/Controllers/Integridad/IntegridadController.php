<?php

namespace App\Http\Controllers\Integridad;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use GuzzleHttp\Client;
use App\Models\Integridad\Consulta;
use App\Models\Integridad\Denuncia;


class IntegridadController extends Controller
{

      public function consulta(Request $request){


          $tramites = [];
          $idTramiteFiltro = $request->nRegistro;
          $queryFilter = "where r.numero_tramite = $idTramiteFiltro ";
          
               $query = "SELECT  *
                   FROM
                  (select
                          tramite.id as numero_tramite ,
                          tramite.proceso_id as numero_proeceso ,
                          proceso.nombre as nombre_proceso ,
                          tramite.created_at as fecha_inicio ,
                          tramite.updated_at as fecha_ultima_modificacion ,
                          tramite.pendiente as pendiente,
                          tramite.ended_at as fecha_culmino,
                          tarea.nombre as etapa,
                          et.id as id_etapa,
                          dts.conteo as cantidad_campos
                          from tramite
                          INNER JOIN proceso ON tramite.proceso_id = proceso.id
                          INNER JOIN (SELECT * FROM etapa WHERE id IN (SELECT MAX(id) FROM etapa GROUP BY tramite_id) ORDER BY id DESC) et on et.tramite_id = tramite.id
                          INNER JOIN tarea on et.tarea_id = tarea.id
                          LEFT JOIN (SELECT  COUNT(id) as conteo, etapa_id FROM dato_seguimiento  group by etapa_id ) dts on dts.etapa_id = et.id
                          where tramite.proceso_id = 20  and tarea.nombre = 'Formulario de Consultas sobre el Sistema de Integridad de la Subsecretaría del Deporte | Funcionario'
                          
          UNION ALL
          select
                          tramite.id as numero_tramite ,
                          tramite.proceso_id as numero_proeceso ,
                          proceso.nombre as nombre_proceso ,
                          tramite.created_at as fecha_inicio ,
                          tramite.updated_at as fecha_ultima_modificacion ,
                          tramite.pendiente as pendiente,
                          tramite.ended_at as fecha_culmino,
                          tarea.nombre as etapa,
                          et.id as id_etapa,
                          dts.conteo as cantidad_campos
                          from tramite
                          INNER JOIN proceso ON tramite.proceso_id = proceso.id
                          INNER JOIN (SELECT * FROM etapa WHERE id IN (SELECT MAX(id) FROM etapa GROUP BY tramite_id) ORDER BY id DESC) et on et.tramite_id = tramite.id
                          INNER JOIN tarea on et.tarea_id = tarea.id
                          LEFT JOIN (SELECT  COUNT(id) as conteo, etapa_id FROM dato_seguimiento  group by etapa_id ) dts on dts.etapa_id = et.id
                          where tramite.proceso_id = 20
                          and tarea.nombre <> 'Formulario de Consultas sobre el Sistema de Integridad de la Subsecretaría del Deporte | Funcionario') r ";
          
              ($idTramiteFiltro != null )? $query = $query.$queryFilter : $query = $query;
              $query = $query."order by r.numero_tramite DESC";
          
              $tramites = DB::connection('mysqlSimple')->select($query);
              return view('integridad.consulta')
              ->with('tramitesD', $tramites)
              ->with('idTramiteFiltro', $idTramiteFiltro);
          
              }
              

          public function consultaShow($id)
          {
                        
              $client = new Client();
                    try {
                    $request = $client->get("https://simple.mindep.cl/backend/api/tramites/$id?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu " , ['verify' => false]);
                    $response = $request->getBody()->getContents();
                    $data =  json_decode($response);
                      }catch (BadResponseException   $e) {
                          $response = $e->getResponse();
                      }
              $data = $data->tramite;
       
              $etapasCont = count($data->etapas);

              $nuevaConsulta = new Consulta();
              $nuevaConsulta->id =  $data->id;
              $nuevaConsulta->fecha_inicio =  $data->fecha_inicio;
              $nuevaConsulta->fecha_termino =  $data->fecha_termino;
              $nuevaConsulta->fecha_modificacion =  $data->fecha_modificacion;
              $nuevaConsulta->estado =  $data->estado;
              
              $datos=$data->datos;            

               foreach ($datos as $key => $value2) {
                    if(property_exists($value2, 'nombres_solicitante')){
                       $nuevaConsulta->nombres_solicitante =  $value2->nombres_solicitante;
                    }

                    if(property_exists($value2, 'apellidos_solicitante')){
                      $nuevaConsulta->apellidos_solicitante =  $value2->apellidos_solicitante;
                    }

                    if(property_exists($value2, 'correo_solicitante')){
                      $nuevaConsulta->correo_solicitante =  $value2->correo_solicitante;
                    }

                    if(property_exists($value2, 'rut')){
                      $nuevaConsulta->rut =  $value2->rut;
                    }

                    if(property_exists($value2, 'unidad_o_seremia')){
                      $nuevaConsulta->unidad_o_seremia =  $value2->unidad_o_seremia;
                    }

                    if(property_exists($value2, 'opcion_sexual')){
                      $nuevaConsulta->opcion_sexual =  $value2->opcion_sexual;
                    }

                    if(property_exists($value2, 'otra_cual')){
                      $nuevaConsulta->otra_cual =  $value2->otra_cual;
                    }

                    if(property_exists($value2, 'tipo_de_consulta')){
                      $nuevaConsulta->tipo_de_consulta =  $value2->tipo_de_consulta;
                    }

                    if(property_exists($value2, 'consulta')){
                      $nuevaConsulta->consulta =  $value2->consulta;
                    }
                    
                    if(property_exists($value2, 'antecedente_1')){
                       $nuevaConsulta->antecedente_1 =  $value2->antecedente_1->URL;
                    }

                    if(property_exists($value2, 'antecedente_2')){
                      $nuevaConsulta->antecedente_2 =  $value2->antecedente_2->URL;
                    }         
            
                    if(property_exists($value2, 'descargar')){
                      $nuevaConsulta->descargar = 'https://simple.mindep.cl/uploads/documentos/'.$value2->descargar;
                    }  
                    
                    if(property_exists($value2, 'fecha_de_respuesta')){
                      $nuevaConsulta->fecha_de_respuesta =  $value2->fecha_de_respuesta;
                    }
                    
                    if(property_exists($value2, 'respuesta')){
                       $nuevaConsulta->respuesta =  $value2->respuesta;
                    }

                    if(property_exists($value2, 'adicional')){
                      $nuevaConsulta->adicional =  $value2->adicional->URL;
                    } 
                 }

            return view('integridad.consulta_detalle')
            ->with('data' , $nuevaConsulta);
          }


          public function denuncia(Request $request){


            $tramites = [];
            $idTramiteFiltro = $request->nRegistro;
            $queryFilter = "where r.numero_tramite = $idTramiteFiltro ";
            
                 $query = "SELECT  *
                     FROM
                    (select
                            tramite.id as numero_tramite ,
                            tramite.proceso_id as numero_proeceso ,
                            proceso.nombre as nombre_proceso ,
                            tramite.created_at as fecha_inicio ,
                            tramite.updated_at as fecha_ultima_modificacion ,
                            tramite.pendiente as pendiente,
                            tramite.ended_at as fecha_culmino,
                            tarea.nombre as etapa,
                            et.id as id_etapa,
                            dts.conteo as cantidad_campos
                            from tramite
                            INNER JOIN proceso ON tramite.proceso_id = proceso.id
                            INNER JOIN (SELECT * FROM etapa WHERE id IN (SELECT MAX(id) FROM etapa GROUP BY tramite_id) ORDER BY id DESC) et on et.tramite_id = tramite.id
                            INNER JOIN tarea on et.tarea_id = tarea.id
                            LEFT JOIN (SELECT  COUNT(id) as conteo, etapa_id FROM dato_seguimiento  group by etapa_id ) dts on dts.etapa_id = et.id
                            where tramite.proceso_id = 19  and tarea.nombre = 'Registro de Denuncia | Funcionario'
                            
            UNION ALL
            select
                            tramite.id as numero_tramite ,
                            tramite.proceso_id as numero_proeceso ,
                            proceso.nombre as nombre_proceso ,
                            tramite.created_at as fecha_inicio ,
                            tramite.updated_at as fecha_ultima_modificacion ,
                            tramite.pendiente as pendiente,
                            tramite.ended_at as fecha_culmino,
                            tarea.nombre as etapa,
                            et.id as id_etapa,
                            dts.conteo as cantidad_campos
                            from tramite
                            INNER JOIN proceso ON tramite.proceso_id = proceso.id
                            INNER JOIN (SELECT * FROM etapa WHERE id IN (SELECT MAX(id) FROM etapa GROUP BY tramite_id) ORDER BY id DESC) et on et.tramite_id = tramite.id
                            INNER JOIN tarea on et.tarea_id = tarea.id
                            LEFT JOIN (SELECT  COUNT(id) as conteo, etapa_id FROM dato_seguimiento  group by etapa_id ) dts on dts.etapa_id = et.id
                            where tramite.proceso_id = 19
                            and tarea.nombre <> 'Registro de Denuncia | Funcionario') r ";
            
                ($idTramiteFiltro != null )? $query = $query.$queryFilter : $query = $query;
                $query = $query."order by r.numero_tramite DESC";
            
                $tramites = DB::connection('mysqlSimple')->select($query);
                return view('integridad.denuncia')
                ->with('tramitesD', $tramites)
                ->with('idTramiteFiltro', $idTramiteFiltro);
            
                }
                
  
            public function denunciaShow($id)
            {
                          
                $client = new Client();
                      try {
                      $request = $client->get("https://simple.mindep.cl/backend/api/tramites/$id?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu " , ['verify' => false]);
                      $response = $request->getBody()->getContents();
                      $data =  json_decode($response);
                        }catch (BadResponseException   $e) {
                            $response = $e->getResponse();
                        }
                $data = $data->tramite;
         
                $etapasCont = count($data->etapas);
  
                $nuevaDenuncia = new Denuncia();
                $nuevaDenuncia->id =  $data->id;
                $nuevaDenuncia->fecha_inicio =  $data->fecha_inicio;
                $nuevaDenuncia->fecha_termino =  $data->fecha_termino;
                $nuevaDenuncia->fecha_modificacion =  $data->fecha_modificacion;
                $nuevaDenuncia->estado =  $data->estado;
                
                $datos=$data->datos;      
               
  
                foreach ($datos as $key => $value2) {
                  if(property_exists($value2, 'nombres_solicitante')){
                     $nuevaDenuncia->nombres_solicitante =  $value2->nombres_solicitante;
                  }

                  if(property_exists($value2, 'apellidos_solicitante')){
                    $nuevaDenuncia->apellidos_solicitante =  $value2->apellidos_solicitante;
                  }

                  if(property_exists($value2, 'correo_solicitante')){
                    $nuevaDenuncia->correo_solicitante =  $value2->correo_solicitante;
                  }

                  if(property_exists($value2, 'rut')){
                    $nuevaDenuncia->rut =  $value2->rut;
                  }

                  if(property_exists($value2, 'unidad_o_seremia')){
                    $nuevaDenuncia->unidad_o_seremia =  $value2->unidad_o_seremia;
                  }

                  if(property_exists($value2, 'opcion_sexual')){
                    $nuevaDenuncia->opcion_sexual =  $value2->opcion_sexual;
                  }

                  if(property_exists($value2, 'indique')){
                    $nuevaDenuncia->indique =  $value2->indique;
                  }

                  if(property_exists($value2, 'telefono_movil')){
                    $nuevaDenuncia->telefono_movil =  $value2->telefono_movil;
                  }

                  if(property_exists($value2, 'direccion')){
                    $nuevaDenuncia->direccion =  $value2->direccion;
                  }

                  if(property_exists($value2, 'tipo_de_denuncia')){
                    $nuevaDenuncia->tipo_de_denuncia =  $value2->tipo_de_denuncia;
                  }

                  if(property_exists($value2, 'especifique')){
                    $nuevaDenuncia->especifique =  $value2->especifique;
                  }

                  if(property_exists($value2, 'fecha_inicio')){
                    $nuevaDenuncia->fecha_inicio =  $value2->fecha_inicio;
                  }

                  if(property_exists($value2, 'fecha_fin')){
                    $nuevaDenuncia->fecha_fin =  $value2->fecha_fin;
                  }
                      
                  if(property_exists($value2, 'archivo_de_respaldo_1')){
                    $nuevaDenuncia->archivo_de_respaldo_1 =  $value2->archivo_de_respaldo_1->URL;
                  }

                  if(property_exists($value2, 'archivo_de_respaldo_2')){
                    $nuevaDenuncia->archivo_de_respaldo_2 =  $value2->archivo_de_respaldo_2->URL;
                  }

                  if(property_exists($value2, 'archivo_de_respaldo_3')){
                    $nuevaDenuncia->archivo_de_respaldo_3 =  $value2->archivo_de_respaldo_3->URL;
                  }

                  if(property_exists($value2, 'descargar')){
                    $nuevaDenuncia->descargar = 'https://simple.mindep.cl/uploads/documentos/'.$value2->descargar;
                  } 

                  if(property_exists($value2, 'fecha_de_recepcion')){
                    $nuevaDenuncia->fecha_de_recepcion =  $value2->fecha_de_recepcion;
                  }

                  if(property_exists($value2, 'observacion')){
                    $nuevaDenuncia->observacion =  $value2->observacion;
                  }

                  if(property_exists($value2, 'fecha_de_respuesta')){
                    $nuevaDenuncia->fecha_de_respuesta =  $value2->fecha_de_respuesta;
                  }

                  if(property_exists($value2, 'resultado')){
                    $nuevaDenuncia->resultado =  $value2->resultado;
                  }

                  if(property_exists($value2, 'respuesta')){
                    $nuevaDenuncia->respuesta =  $value2->respuesta;
                  }

                  if(property_exists($value2, 'adjuntar_doc_final')){
                    $nuevaDenuncia->adjuntar_doc_final =  $value2->adjuntar_doc_final->URL;
                  }

                  if(property_exists($value2, 'comprobante_aprobacion')){
                    $nuevaDenuncia->descarcomprobante_aprobaciongar = 'https://simple.mindep.cl/uploads/documentos/'.$value2->comprobante_aprobacion;
                  }

                  if(property_exists($value2, 'comprobante_rechazo')){
                    $nuevaDenuncia->comprobante_rechazo = 'https://simple.mindep.cl/uploads/documentos/'.$value2->comprobante_rechazo;
                  }
                                       
                }
  
              return view('integridad.denuncia_detalle')
              ->with('data' , $nuevaDenuncia);
            }

}