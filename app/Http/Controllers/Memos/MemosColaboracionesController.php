<?php

namespace App\Http\Controllers\Memos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\DerivacionesDocumento\DerivacionesDocumento;
use App\Models\IngresoTramite\IngresoTramite;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ComboBoxCompra;
use App\Models\TiposDocumentos\TiposDocumentos;
use App\Models\Divisiones\Divisiones;
use App\Models\Memo\Memo;
use App\Models\MemoColaboraciones\MemoColaboraciones;
use DB;
use App\Models\File\File;
use Illuminate\Support\Facades\Auth;
use App\Models\UsuariosSimple\UsuariosSimple;


class MemosColaboracionesController extends Controller
{
   public function index(Request $request){
    set_time_limit(14200);
      $selectA = Divisiones::get()->pluck('nombre', 'nombre')->prepend('Todos', null);
      $requestNRegistro = null;
      $listado = [];
      $anno = Carbon::now()->format('Y');
      $fechaStringInicio = ($anno).'-01-01 00:00:00';
      $feDesde = Carbon::parse($fechaStringInicio);
      $feHasta =  Carbon::parse(Carbon::now()->format('Y-m-d').' 23:59:59');
      $fechaInDesde = Carbon::parse($fechaStringInicio);
      $fechaInHasta = Carbon::now();
      $fechas[1] = Date('Y-m-d', strtotime( '1970-01-01' ));
      $fechas[0] = Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
      $areRemitenteView = '';
      $remitenteView = '';
      $materiaView = '';
      $destinatarioView = '';
      $correlativoView = '';
      $divisionusuarios = Auth::user()->department;
      $divisiondestinatario = Auth::user()->department;
      $reservado = 'reservado';
      $esAdmin = false;
      $listadoNoreservado = collect([]);




          if( $request->FechaDesde != null ) {
            $feDesde=(Carbon::parse($request->get('FechaDesde'))->format('dmY'));
            $feDesde= Carbon::parse(substr($feDesde, 0, 2).'-'.substr($feDesde, 2, 2).'-'.substr($feDesde, 4, 4).' 00:00:00');
          }
          if( $request->FechaHasta != null ) {
             $feHasta=(Carbon::parse($request->get('FechaHasta'))->format('dmY'));
             $feHasta= Carbon::parse(substr($feHasta, 0, 2).'-'.substr($feHasta, 2, 2).'-'.substr($feHasta, 4, 4).' 23:59:59');
          }



      $query = Memo::query ();
      $query->when(request('nRegistro', false), function ($q, $numRegistro) { 
            return $q->where('tramite_id', $numRegistro);
        });
      $query->when(request('remitente'), function ($q, $remitente) { 
            return $q->where('remitente', 'LIKE', "%$remitente%");
        });
      $query->when(request('materia'), function ($q, $materia) { 
            return $q->where('materia', 'LIKE', "%$materia%");
        });
      $query->when(request('destinatario'), function ($q, $destinatario) { 
            return $q->where('destinatario', 'LIKE', "%$destinatario%");
        });
      $query->when($feDesde, function ($q, $feDesde ) { 
            return $q->where('created_at','>=', $feDesde);
        });
      $query->when($feHasta, function ($q, $feHasta ) { 
            return $q->where('created_at','<=', $feHasta);
        });

      foreach (auth()->user()->roles as $role) {
            if ($role->todos == 1) {
                $esAdmin=  true;
                break;
            }
        }
      /*if(!$esAdmin){

      $query->when(request('areaRemtiente', false), function ($q, $areaRemtiente) { 
            return $q->where('division', $areaRemtiente);
        });
      $query->when($divisionusuarios, function ($q, $divisionusuarios) { 
            return $q->where('division', $divisionusuarios);
        });
      $query->when($divisiondestinatario, function ($q, $divisiondestinatario) { 
            return $q->orwhere('division_destinatario', $divisiondestinatario);
        });
       $query->when($divisiondestinatario, function ($q, $divisiondestinatario) { 
            return $q->orwhere('division_destinatario', 'Según Distribución');
        });
       $query->when($reservado, function ($q, $reservado ) { 
            return $q->where('reservado', 0);
        });

    }*/
     $listado = $query->get()->sortByDesc(function($q){
     return $q->correlativo->correlativo;
     });


      
      
      $correlativoView = (int)$request->correlativo;
      foreach ($listado as $clave => $reg) {
        $reg->materia = str_replace('"Times New Roman",serif', ' ', $reg->materia);
        $reg->materia = str_replace('"Arial",sans-serif', ' ', $reg->materia);
        $reg->materia = str_replace('"Verdana",sans-serif', ' ', $reg->materia);
        if($request->correlativo != null ) {
        if($reg->correlativo->correlativo === $correlativoView)
        $listado = [$reg];    
      }
      }
      /*if(!$esAdmin){
      foreach ($listado as $clave => $reg) {

        if($reg->reservado == 0)
        $listadoNoreservado->push($reg);    
      }
      $listado = $listadoNoreservado;
      }

      $emailUsuarioSesion = auth()->user()->email;
      $nombrecargosimple = UsuariosSimple::where('email',$emailUsuarioSesion)->first()->nombre_cargo;
    $listado2 = Memo::where('destinatario', $nombrecargosimple )->orwhere('remitente', $nombrecargosimple )->where('reservado', 1)->get();
      foreach ($listado2 as $key => $value) {
        $listado->push($value);
      }
      $listado = $listado->all();
*/

      $emailUsuarioSesion = auth()->user()->email;
     $nombrecargo = UsuariosSimple::where('email',$emailUsuarioSesion)->first()->nombre_cargo;

     if( $request->nRegistro != null ) {
        $requestNRegistro = $request->nRegistro;
      }
        if($request->areaRemtiente != null ) {
        $areRemitenteView =  $request->areaRemtiente;
      }
      
      if($request->remitente != null ) {
        $remitenteView =  $request->remitente;
        
      }
      if($request->materia != null ) {
        $materiaView =  $request->materia;        
      }
      if($request->destinatario != null ) {
        $destinatarioView =  $request->destinatario;        
      }


      return view('memos.memos_colaboraciones')
      ->with('selectA' ,$selectA)
      ->with('areRemitenteView' ,$areRemitenteView)
      ->with('remitenteView' ,$remitenteView)
      ->with('materiaView' ,$materiaView)
      ->with('destinatarioView' ,$destinatarioView)
      ->with('correlativoView' ,$correlativoView)
      ->with('feDesde' ,$feDesde)
      ->with('feHasta' ,$feHasta)
      ->with('fechaInDesde' ,$fechaInDesde)
      ->with('fechaInHasta' ,$fechaInHasta)
      ->with('fechas' ,$fechas)
      ->with('requestNRegistro' , $requestNRegistro)
      ->with('listado' , $listado)
      ->with('nombrecargo', $nombrecargo)
      ->with('esAdmin', $esAdmin);


} 

public function cargarExcelDeparatamentos (){


      $array = collect(Excel::load(public_path('departamentos2.xlsx'), function($reader) {})->get());
      $arrayTransformado = [];
      $cantidad = count($array[0]);
     
      for ($i=0; $i < $cantidad; $i++) { 
        $arrayTransformado[$array[0][$i]['area']] = $array[0][$i]['area'];
       } 

     
        return (collect($arrayTransformado)->prepend('Todos', null));
      
    }

public function show(Request $request, $id){
    $segundoArray = [];
    $detalle = MemoColaboraciones::where('id', $id)->first();

if ($detalle->lista_colaboradores){
    $stringlistacolaboradores = $detalle->lista_colaboradores;
  
    $stringt1 = substr($detalle->lista_colaboradores, 1 , strlen($detalle->lista_colaboradores)-2);
    $primerArray = explode('],[', $stringt1);

   for ($i=1; $i < (count($primerArray)-1); $i++) { 
        $segundoArray[$i] = $primerArray[$i];
   }

     $detalle->lista_colaboradores_aux = implode('<br>', $segundoArray);
}else{
    $detalle->lista_colaboradores_aux = '';
}



         return view('memos.memo_colaboraciones_detalle')
            ->with('detalle', $detalle);  
}


}
