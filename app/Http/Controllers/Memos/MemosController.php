<?php
namespace App\Http\Controllers\Memos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\DerivacionesDocumento\DerivacionesDocumento;
use App\Models\IngresoTramite\IngresoTramite;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ComboBoxCompra;
use App\Models\TiposDocumentos\TiposDocumentos;
use App\Models\Divisiones\Divisiones;
use App\Models\Memo\Memo;
use App\Models\CorreoDistribucionProcesos\CorreoDistribucionProcesos;
use DB;
use App\Models\File\File;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Models\UsuariosSimple\UsuariosSimple;
use Mail;
use App\Mail\DistribucionMemoEmail;
use App\Mail\TestEmail;
use App\Models\UsuariosMindep\UsuariosMindep;
use App\Models\PyR\PyR;
use App\Models\Simple\TmpTramiteMemos;
use App\Models\Simple\Etapa;
use App\Models\Simple\DatoSeguimiento;



class MemosController extends Controller
{
   public function index(Request $request){
      set_time_limit(14200);
      $selectA = Divisiones::get()->pluck('nombre', 'nombre')->prepend('Todos', null);
      $requestNRegistro = null;
      $listado = [];
      $anno = Carbon::now()->format('Y');
      $fechaStringInicio = ($anno).'-01-01 00:00:00';
      $feDesde = Carbon::parse($fechaStringInicio);
      $feHasta =  Carbon::parse(Carbon::now()->format('Y-m-d').' 23:59:59');
      $fechaInDesde = Carbon::parse($fechaStringInicio);
      $fechaInHasta = Carbon::now();
      $fechas[1] = Date('Y-m-d', strtotime( '1970-01-01' ));
      $fechas[0] = Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
      $areRemitenteView = '';
      $remitenteView = '';
      $materiaView = '';
      $destinatarioView = '';
      $correlativoView = '';
      $divisionusuarios = Auth::user()->department;
      $divisiondestinatario = Auth::user()->department;
      $reservado = 'reservado';
      $esAdmin = false;
      $listadoNoreservado = collect([]);





          if( $request->FechaDesde != null ) {
            $feDesde=(Carbon::parse($request->get('FechaDesde'))->format('dmY'));
            $feDesde= Carbon::parse(substr($feDesde, 0, 2).'-'.substr($feDesde, 2, 2).'-'.substr($feDesde, 4, 4).' 00:00:00');
          }
          if( $request->FechaHasta != null ) {
             $feHasta=(Carbon::parse($request->get('FechaHasta'))->format('dmY'));
             $feHasta= Carbon::parse(substr($feHasta, 0, 2).'-'.substr($feHasta, 2, 2).'-'.substr($feHasta, 4, 4).' 23:59:59');
          }



      $query = Memo::query ();
      $query->when(request('nRegistro', false), function ($q, $numRegistro) {
            return $q->where('tramite_id', $numRegistro);
        });
      $query->when(request('remitente'), function ($q, $remitente) {
            return $q->where('remitente', 'LIKE', "%$remitente%");
        });
      $query->when(request('materia'), function ($q, $materia) {
            return $q->where('materia', 'LIKE', "%$materia%");
        });
      $query->when(request('destinatario'), function ($q, $destinatario) {
            return $q->where('destinatario', 'LIKE', "%$destinatario%");
        });
      $query->when($feDesde, function ($q, $feDesde ) {
            return $q->where('created_at','>=', $feDesde);
        });
      $query->when($feHasta, function ($q, $feHasta ) {
            return $q->where('created_at','<=', $feHasta);

        });

        foreach (auth()->user()->roles as $role) {
            if ($role->todos == 1) {
                $esAdmin=  true;
                break;
            }
        }
      /*if(!$esAdmin){
        $query->when($divisionusuarios, function ($q, $divisionusuarios) {
              return $q->where('division', $divisionusuarios);
          });
        $query->when($divisiondestinatario, function ($q, $divisiondestinatario) {
              return $q->orwhere('division_destinatario', $divisiondestinatario);
          });
        $query->when($divisiondestinatario, function ($q, $divisiondestinatario) {
              return $q->orwhere('division_destinatario', 'Según Distribución');
          });
        $query->when($reservado, function ($q, $reservado ) {
              return $q->where('reservado', 0);
          });
      }*/

        //$q->where('division',Auth::user()->department);

     $listado = $query->get()->sortByDesc(function($q){
     return $q->correlativo->correlativo;
     });

    // dd($listado[9]->remitente);



      $correlativoView = (int)$request->correlativo;
      foreach ($listado as $clave => $reg) {
        $reg->materia = str_replace('"Times New Roman",serif', ' ', $reg->materia);
        $reg->materia = str_replace('"Arial",sans-serif', ' ', $reg->materia);
        $reg->materia = str_replace('"Verdana",sans-serif', ' ', $reg->materia);
        if($request->correlativo != null ) {
        if($reg->correlativo->correlativo === $correlativoView)
        $listado = [$reg];
      }
      }

     /* if(!$esAdmin){
      foreach ($listado as $clave => $reg) {

        if($reg->reservado == 0)
        $listadoNoreservado->push($reg);
      }
      $listado = $listadoNoreservado;
      }*/

      /*$emailUsuarioSesion = auth()->user()->email;

      $listado2 = Memo::where('destinatario', $nombrecargosimple )->orwhere('remitente', $nombrecargosimple )->where('reservado', 1)->get();
      foreach ($listado2 as $key => $value) {
        $listado->push($value);
      }
      $listado = $listado->all();*/

     $emailUsuarioSesion = auth()->user()->email;
     $nombrecargo = UsuariosSimple::where('email',$emailUsuarioSesion)->first()->nombre_cargo;

     if( $request->nRegistro != null ) {
        $requestNRegistro = $request->nRegistro;
      }
        if($request->areaRemtiente != null ) {
        $areRemitenteView =  $request->areaRemtiente;
      }
      if($request->remitente != null ) {
        $remitenteView =  $request->remitente;
      }
      if($request->materia != null ) {
        $materiaView =  $request->materia;
      }
      if($request->destinatario != null ) {
        $destinatarioView =  $request->destinatario;
      }




      return view('memos.memos')
      ->with('selectA' ,$selectA)
      ->with('areRemitenteView' ,$areRemitenteView)
      ->with('remitenteView' ,$remitenteView)
      ->with('materiaView' ,$materiaView)
      ->with('destinatarioView' ,$destinatarioView)
      ->with('correlativoView' ,$correlativoView)
      ->with('feDesde' ,$feDesde)
      ->with('feHasta' ,$feHasta)
      ->with('fechaInDesde' ,$fechaInDesde)
      ->with('fechaInHasta' ,$fechaInHasta)
      ->with('fechas' ,$fechas)
      ->with('requestNRegistro' , $requestNRegistro)
      ->with('listado' , $listado)
      ->with('nombrecargo', $nombrecargo)
      ->with('esAdmin', $esAdmin);


}

public function cargarExcelDeparatamentos (){


      $array = collect(Excel::load(public_path('departamentos2.xlsx'), function($reader) {})->get());
      $arrayTransformado = [];
      $cantidad = count($array[0]);

      for ($i=0; $i < $cantidad; $i++) {
        $arrayTransformado[$array[0][$i]['area']] = $array[0][$i]['area'];
       }


        return (collect($arrayTransformado)->prepend('Todos', null));

    }

public function show(Request $request, $id){

    $detalle = Memo::where('id', $id)->first();
    $detalle->materia = str_replace('"Times New Roman",serif', ' ', $detalle->materia);
    $detalle->materia = str_replace('"Arial",sans-serif', ' ', $detalle->materia);
    $detalle->materia = str_replace('"Verdana",sans-serif', ' ', $detalle->materia);
    $detalle->texto = str_replace('"Arial",sans-serif', ' ', $detalle->texto);
    $detalle->texto = str_replace('"Times New Roman",serif', ' ', $detalle->texto);
    $detalle->distribucion = str_replace('"Arial",sans-serif', ' ', $detalle->distribucion);
    $detalle->distribucion = str_replace('"Times New Roman",serif', ' ', $detalle->distribucion);


         return view('memos.memo_detalle')
            ->with('detalle', $detalle);
}
public function envioCorreo($idMemo , $tramiteId){
      foreach (PyR::get() as $key1 => $value1){$selectUsuarios[$value1->email]=$value1->nombres.' '.$value1->apellido_paterno.' '.$value1->apellido_materno.' '.$value1->unidad_desempeno.' '.$value1->email; }
      $detalle = Memo::where('id', $idMemo)->first();
         return view('memos.memo_distribucion')
          ->with('selectUsuarios' , $selectUsuarios)
          ->with('detalle' , $detalle);
}
public function distribucionStd(Request $request , $idMemo){

      $correosInternos = $request->correos_internos;
      $correosExternos = $request->correos_externos;
      $datos = Memo::where('id', $idMemo)->first();
      if($correosInternos){
      if(count($correosInternos)>0){
      foreach ($correosInternos as $keyi => $valuei) {
           Mail::to($valuei)->send(new DistribucionMemoEmail($datos));
           $nuevoCorreoEnvado = new CorreoDistribucionProcesos ();
           $nuevoCorreoEnvado->proceso_simple_id = 2;
           $nuevoCorreoEnvado->usuario_id =  Auth::user()->id;
           $nuevoCorreoEnvado->tramite_id =  $datos->tramite_id;
           $nuevoCorreoEnvado->correo_destino = $valuei;
           $nuevoCorreoEnvado->save();
        }
        }
      }

      if(count($correosExternos)>0){
      foreach ($correosExternos as $keye => $valuee) {
        if($valuee){
           Mail::to($valuee)->send(new DistribucionMemoEmail($datos));
           $nuevoCorreoEnvado = new CorreoDistribucionProcesos ();
           $nuevoCorreoEnvado->proceso_simple_id = 2;
           $nuevoCorreoEnvado->usuario_id =  Auth::user()->id;
           $nuevoCorreoEnvado->tramite_id =  $datos->tramite_id;
           $nuevoCorreoEnvado->correo_destino = $valuee;
           $nuevoCorreoEnvado->save();
        }
        }
      }

      alert()->success('!','correos enviados exitosamente');
      return redirect()->route('Memosshow',  $idMemo);


}

public function index2(Request $request){

  set_time_limit(14200);
  $selectA = Divisiones::get()->pluck('nombre', 'nombre')->prepend('Todos', null);
  $requestNRegistro = null;
  $listado = [];
  $anno = Carbon::now()->format('Y');
  $fechaStringInicio = ($anno).'-01-01 00:00:00';
  $feDesde = Carbon::parse($fechaStringInicio);
  $feHasta =  Carbon::parse(Carbon::now()->format('Y-m-d').' 23:59:59');
  $fechaInDesde = Carbon::parse($fechaStringInicio);
  $fechaInHasta = Carbon::now();
  $fechas[1] = Date('Y-m-d', strtotime( '1970-01-01' ));
  $fechas[0] = Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
  $areRemitenteView = '';
  $remitenteView = '';
  $subroganteView = '';
  $fechaView = '';
  $materiaView = '';
  $destinatarioView = '';
  $correlativoView = '';
  $divisionusuarios = Auth::user()->department;
  $divisiondestinatario = Auth::user()->department;
  $reservado = 'reservado';
  $esAdmin = false;
  $listadoNoreservado = collect([]);


      if( $request->FechaDesde != null ) {
        $feDesde=(Carbon::parse($request->get('FechaDesde'))->format('dmY'));
        $feDesde= Carbon::parse(substr($feDesde, 0, 2).'-'.substr($feDesde, 2, 2).'-'.substr($feDesde, 4, 4).' 00:00:00');
      }
      if( $request->FechaHasta != null ) {
         $feHasta=(Carbon::parse($request->get('FechaHasta'))->format('dmY'));
         $feHasta= Carbon::parse(substr($feHasta, 0, 2).'-'.substr($feHasta, 2, 2).'-'.substr($feHasta, 4, 4).' 23:59:59');
      }

  $storeProcedium = 'call memoss()';
  $tramites = DB::connection('mysqlSimple')->select($storeProcedium);





  $query = TmpTramiteMemos::query ();
  $query->selectRaw("REPLACE(REPLACE(correlativo, '\"' , ''), '\\\' , '') as correlativo , reservado, tramite_id , proceso_id , pendiente , ended_at, fecha, etapas , http_code_correlativo ,materia , remitente, de_subrogante, division_remitente , destinatario , estatus , lugar , contenido , vistos , distribucion , anexos , anexo_a , anexo_b, anexo_c");
  $query->where('correlativo', '!=' , null);
  $query->where('http_code_correlativo', '!=' , 404);

  $query->when(request('nRegistro', false), function ($q, $numRegistro) {
        return $q->where('tramite_id', $numRegistro);
    });

  $query->when(request('remitente'), function ($q, $remitente) {
        return $q->where('remitente', 'LIKE', "%$remitente%");
    });

    $query->when(request('de_subrogante'), function ($q, $de_subrogante) {
          return $q->where('de_subrogante', 'LIKE', "%$de_subrogante%");
      });

    $query->when(request('correlativo'), function ($q, $correlativo) {
      return $q->where('correlativo', 'LIKE', "%$correlativo%");
    });

    $query->when(request('fecha'), function ($q, $fecha) {
          return $q->where('fecha', 'LIKE', "%$fecha%");
      });

  $query->when(request('materia'), function ($q, $materia) {
        return $q->where('materia', 'LIKE', "%$materia%");
    });

  $query->when(request('destinatario'), function ($q, $destinatario) {
        return $q->where('destinatario', 'LIKE', "%$destinatario%");
    });

  $query->when($feDesde, function ($q, $feDesde ) {
        return $q->where('created_at','>=', $feDesde);
    });

  $query->when($feHasta, function ($q, $feHasta ) {
        return $q->where('created_at','<=', $feHasta);

    });

    foreach (auth()->user()->roles as $role) {
        if ($role->todos == 1) {
            $esAdmin=  true;
            break;
        }
    }


 $listado = $query->get()->sortByDesc(function($q){
 return $q->correlativo;
 });


  foreach ($listado as $clave => $reg) {
    $reg->materia = str_replace('"Times New Roman",serif', ' ', $reg->materia);
    $reg->materia = str_replace('"Arial",sans-serif', ' ', $reg->materia);
    $reg->materia = str_replace('"Verdana",sans-serif', ' ', $reg->materia);
    $reg->materia = str_replace('"Calibri",sans-serif', ' ', $reg->materia);
    $reg->materia = str_replace('"Century Gothic",sans-serif', ' ', $reg->materia);
    $reg->materia = str_replace('",serif\"="">', ' ', $reg->materia);
    $reg->materia =  str_replace( 'style=\"font-family:\"Times New Roman\",serif\"', '' , $reg->materia);
    $reg->materia =  str_replace( 'style=\"font-family:\"Verdana\",serif\"', '' , $reg->materia);
    $reg->materia =  str_replace( 'style=\"font-family:\"Arial\",serif\"', '' , $reg->materia);
    $reg->materia =  str_replace( 'style=\"font-family:\"Calibri\",serif\"', '' , $reg->materia);
    $reg->materia =  str_replace( '<\/span>', '' , $reg->materia);
    $reg->materia =  str_replace( '<span>', '' , $reg->materia);
    $reg->materia =  str_replace( '<\/strong>', '' , $reg->materia);
    $reg->materia =  str_replace( '<strong>', '' , $reg->materia);
    $reg->materia =  str_replace( '<\/p>', '' , $reg->materia);
    $reg->materia =  str_replace( '<p>', '' , $reg->materia);
    $reg->materia =  str_replace( '<p style=\"margin-right:-19px; text-align:justify\">', '' , $reg->materia);
    $reg->materia =  str_replace( '<p style=\"margin-left:340px; margin-right:-19px; text-align:justify\">', '' , $reg->materia);
    $reg->materia =  str_replace( '<p style=\"text-align:justify\">', '' , $reg->materia);
    $reg->materia =  str_replace( '<div style=\"text-align:justify\">', '' , $reg->materia);
    $reg->materia =  str_replace( '<\/div>', '' , $reg->materia);
    $reg->materia =  str_replace( 'style=\"font-family:\"Century Gothic\",sans-serif\"', '' , $reg->materia);
    $reg->materia =  str_replace( '\u00a0', '' , $reg->materia);
  }



 $emailUsuarioSesion = auth()->user()->email;
 $nombrecargo = UsuariosSimple::where('email',$emailUsuarioSesion)->first()->nombre_cargo;

 if( $request->nRegistro != null ) {
    $requestNRegistro = $request->nRegistro;
  }
    if($request->areaRemtiente != null ) {
    $areRemitenteView =  $request->areaRemtiente;
  }
  if($request->remitente != null ) {
    $remitenteView =  $request->remitente;
  }
  if($request->de_subrogante != null ) {
    $subroganteView =  $request->de_subrogante;
  }
  if($request->fecha != null ) {
    $fechaView =  $request->fecha;
  }
  if($request->materia != null ) {
    $materiaView =  $request->materia;
  }
  if($request->destinatario != null ) {
    $destinatarioView =  $request->destinatario;
  }

  return view('memos.memoss')
  ->with('selectA' ,$selectA)
  ->with('areRemitenteView' ,$areRemitenteView)
  ->with('remitenteView' ,$remitenteView)
  ->with('subroganteView' ,$subroganteView)
  ->with('materiaView' ,$materiaView)
  ->with('fechaView' ,$fechaView)
  ->with('destinatarioView' ,$destinatarioView)
  ->with('correlativoView' ,$correlativoView)
  ->with('feDesde' ,$feDesde)
  ->with('feHasta' ,$feHasta)
  ->with('fechaInDesde' ,$fechaInDesde)
  ->with('fechaInHasta' ,$fechaInHasta)
  ->with('fechas' ,$fechas)
  ->with('requestNRegistro' , $requestNRegistro)
  ->with('listado' , $listado)
  ->with('nombrecargo', $nombrecargo)
  ->with('esAdmin', $esAdmin);


}
public function show2(Request $request, $id){

      $client = new Client();
        try {
          $request = $client->get("https://simple.mindep.cl/backend/api/tramites/$id?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu" , ['verify' => false]);
          $response = $request->getBody()->getContents();
          $data =  json_decode($response);
          }catch (BadResponseException   $e) {
              $response = $e->getResponse();
          }
      $data = $data->tramite;
      

      $detalle = new Memo();
      $detalle->tramite_id = $data->id;
      $detalle->proceso_id = $data->proceso_id;
      $detalle->fecha_inicio = $data->fecha_inicio;
      $detalle->fecha_modificacion = $data->fecha_modificacion;
      $detalle->fecha_termino = $data->fecha_termino;

      $datos=$data->datos;
      if($datos){
        foreach ($datos as $key => $value2) {

          if(property_exists($value2, 'adjuntar_memo')){
            $detalle->doc_memo = $value2->adjuntar_memo->URL;
          }

          if(property_exists($value2, 'anexo_a')){
            $detalle->doc_anexo_a = $value2->anexo_a->URL;
          }

          if(property_exists($value2, 'anexo_b')){
            $detalle->doc_anexo_b = $value2->anexo_b->URL;
          }

          if(property_exists($value2, 'anexo_c')){
            $detalle->doc_anexo_c = $value2->anexo_c->URL;
          }

          if(property_exists($value2, 'adjuntar_memo')){
            $detalle->doc_anexo_d = $value2->adjuntar_memo->URL;
          }

          if(property_exists($value2, 'anexos')){
            $detalle->anexos = $value2->anexos;
          }

          if(property_exists($value2, 'division')){
            $detalle->division = $value2->division;
          }

          if(property_exists($value2, 'lugar')){
            $detalle->lugar = $value2->lugar;
          }

          if(property_exists($value2, 'fecha')){
            $detalle->fecha = $value2->fecha;
          }

          if(property_exists($value2, 'para')){
            $detalle->destinatario = $value2->para;
          }

          if(property_exists($value2, 'de')){
            $detalle->remitente = $value2->de;
          }

          if(property_exists($value2, 'de_subrogante')){
            $detalle->remitente = $value2->de_subrogante;
          }

          if(property_exists($value2, 'asunto')){
            $detalle->materia = $value2->asunto;
          }

          if(property_exists($value2, 'texto')){
            $detalle->texto = $value2->texto;
          }

          if(property_exists($value2, 'vistos')){
            $detalle->vistos = $value2->vistos;
          }

          if(property_exists($value2, 'distribucion')){
            $detalle->distribucion = $value2->distribucion;
          }

          if(property_exists($value2, 'correlativo')){
            $detalle->correlativo = $value2->correlativo;
          }

          if(property_exists($value2, 'categoria')){
            $detalle->categoria = $value2->categoria;
            if( $value2->categoria != 'sin_categoria'){
              $detalle->reservado = 1;
            }
          }

        }
    }

  $detalle->materia = str_replace('"Times New Roman",serif', ' ', $detalle->materia);
  $detalle->materia = str_replace('"Arial",sans-serif', ' ', $detalle->materia);
  $detalle->materia = str_replace('"Verdana",sans-serif', ' ', $detalle->materia);
  $detalle->materia = str_replace('"Times New Roman",serif', ' ', $detalle->materia);
  $detalle->materia = str_replace('"Arial",sans-serif', ' ', $detalle->materia);
  $detalle->materia = str_replace('"Verdana",sans-serif', ' ', $detalle->materia);
  $detalle->materia = str_replace('"Calibri",sans-serif', ' ', $detalle->materia);
  $detalle->materia = str_replace('"Century Gothic",sans-serif', ' ', $detalle->materia);
  $detalle->materia = str_replace('",serif\"="">', ' ', $detalle->materia);
  $detalle->materia =  str_replace( 'style=\"font-family:\"Times New Roman\",serif\"', '' , $detalle->materia);
  $detalle->materia =  str_replace( 'style=\"font-family:\"Verdana\",serif\"', '' , $detalle->materia);
  $detalle->materia =  str_replace( 'style=\"font-family:\"Arial\",serif\"', '' , $detalle->materia);
  $detalle->materia =  str_replace( 'style=\"font-family:\"Calibri\",serif\"', '' , $detalle->materia);
  $detalle->materia =  str_replace( 'style=\"font-family:\"Century Gothic\",sans-serif\"', '' , $detalle->materia);
  $detalle->materia =  str_replace( '<\/span>', '' , $detalle->materia);
  $detalle->materia =  str_replace( '<span>', '' , $detalle->materia);
  $detalle->materia =  str_replace( '<\/strong>', '' , $detalle->materia);
  $detalle->materia =  str_replace( '<strong>', '' , $detalle->materia);
  $detalle->materia =  str_replace( '<\/p>', '' , $detalle->materia);
  $detalle->materia =  str_replace( '<p>', '' , $detalle->materia);
  $detalle->materia =  str_replace( '<p style=\"margin-right:-19px; text-align:justify\">', '' , $detalle->materia);
  $detalle->materia =  str_replace( '<p style=\"text-align:justify\">', '' , $detalle->materia);
  $detalle->materia =  str_replace( '<div style=\"text-align:justify\">', '' , $detalle->materia);
  $detalle->materia =  str_replace( '<\/div>', '' , $detalle->materia);

  $detalle->texto = str_replace('"Arial",sans-serif', ' ', $detalle->texto);
  $detalle->texto = str_replace('"Times New Roman",serif', ' ', $detalle->texto);
  $detalle->texto = str_replace('"Times New Roman",serif', ' ', $detalle->texto);
  $detalle->texto = str_replace('"Arial",sans-serif', ' ', $detalle->texto);
  $detalle->texto = str_replace('"Verdana",sans-serif', ' ', $detalle->texto);
  $detalle->texto = str_replace('"Calibri",sans-serif', ' ', $detalle->texto);
  $detalle->texto = str_replace('"Century Gothic",sans-serif', ' ', $detalle->texto);
  $detalle->texto = str_replace('",serif\"="">', ' ', $detalle->texto);
  $detalle->texto =  str_replace( 'style=\"font-family:\"Times New Roman\",serif\"', '' , $detalle->texto);
  $detalle->texto =  str_replace( 'style=\"font-family:\"Verdana\",serif\"', '' , $detalle->texto);
  $detalle->texto =  str_replace( 'style=\"font-family:\"Arial\",serif\"', '' , $detalle->texto);
  $detalle->texto =  str_replace( 'style=\"font-family:\"Calibri\",serif\"', '' , $detalle->texto);
  $detalle->texto =  str_replace( 'style=\"font-family:\"Century Gothic\",sans-serif\"', '' , $detalle->texto);
  $detalle->texto =  str_replace( '<\/span>', '' , $detalle->texto);
  $detalle->texto =  str_replace( '<span>', '' , $detalle->texto);
  $detalle->texto =  str_replace( '<\/strong>', '' , $detalle->texto);
  $detalle->texto =  str_replace( '<strong>', '' , $detalle->texto);
  $detalle->texto =  str_replace( '<\/p>', '' , $detalle->texto);
  $detalle->texto =  str_replace( '<p>', '' , $detalle->texto);
  $detalle->texto =  str_replace( '<p style=\"margin-right:-19px; text-align:justify\">', '' , $detalle->texto);
  $detalle->texto =  str_replace( '<p style=\"text-align:justify\">', '' , $detalle->texto);
  $detalle->texto =  str_replace( '<div style=\"text-align:justify\">', '' , $detalle->texto);
  $detalle->texto =  str_replace( '<\/div>', '' , $detalle->texto);

  $detalle->distribucion = str_replace('"Arial",sans-serif', ' ', $detalle->distribucion);
  $detalle->distribucion = str_replace('"Times New Roman",serif', ' ', $detalle->distribucion);
  $detalle->distribucion = str_replace('"Times New Roman",serif', ' ', $detalle->distribucion);
  $detalle->distribucion = str_replace('"Arial",sans-serif', ' ', $detalle->distribucion);
  $detalle->distribucion = str_replace('"Verdana",sans-serif', ' ', $detalle->distribucion);
  $detalle->distribucion = str_replace('"Calibri",sans-serif', ' ', $detalle->distribucion);
  $detalle->distribucion = str_replace('"Century Gothic",sans-serif', ' ', $detalle->distribucion);
  $detalle->distribucion = str_replace('",serif\"="">', ' ', $detalle->distribucion);
  $detalle->distribucion =  str_replace( 'style=\"font-family:\"Times New Roman\",serif\"', '' , $detalle->distribucion);
  $detalle->distribucion =  str_replace( 'style=\"font-family:\"Verdana\",serif\"', '' , $detalle->distribucion);
  $detalle->distribucion =  str_replace( 'style=\"font-family:\"Arial\",serif\"', '' , $detalle->distribucion);
  $detalle->distribucion =  str_replace( 'style=\"font-family:\"Calibri\",serif\"', '' , $detalle->distribucion);
  $detalle->distribucion =  str_replace( 'style=\"font-family:\"Century Gothic\",sans-serif\"', '' , $detalle->distribucion);
  $detalle->distribucion =  str_replace( '<\/span>', '' , $detalle->distribucion);
  $detalle->distribucion =  str_replace( '<span>', '' , $detalle->distribucion);
  $detalle->distribucion =  str_replace( '<\/strong>', '' , $detalle->distribucion);
  $detalle->distribucion =  str_replace( '<strong>', '' , $detalle->distribucion);
  $detalle->distribucion =  str_replace( '<\/p>', '' , $detalle->distribucion);
  $detalle->distribucion =  str_replace( '<p>', '' , $detalle->distribucion);
  $detalle->distribucion =  str_replace( '<p style=\"margin-right:-19px; text-align:justify\">', '' , $detalle->distribucion);
  $detalle->distribucion =  str_replace( '<p style=\"text-align:justify\">', '' , $detalle->distribucion);
  $detalle->distribucion =  str_replace( '<div style=\"text-align:justify\">', '' , $detalle->distribucion);
  $detalle->distribucion =  str_replace( '<\/div>', '' , $detalle->distribucion);

  $detalle->anexos = str_replace('"Times New Roman",serif', ' ', $detalle->anexos);
  $detalle->anexos = str_replace('"Arial",sans-serif', ' ', $detalle->anexos);
  $detalle->anexos = str_replace('"Verdana",sans-serif', ' ', $detalle->anexos);
  $detalle->anexos = str_replace('"Calibri",sans-serif', ' ', $detalle->anexos);
  $detalle->anexos = str_replace('"Century Gothic",sans-serif', ' ', $detalle->anexos);
  $detalle->anexos = str_replace('",serif\"="">', ' ', $detalle->anexos);
  $detalle->anexos =  str_replace( 'style=\"font-family:\"Times New Roman\",serif\"', '' , $detalle->anexos);
  $detalle->anexos =  str_replace( 'style=\"font-family:\"Verdana\",serif\"', '' , $detalle->anexos);
  $detalle->anexos =  str_replace( 'style=\"font-family:\"Arial\",serif\"', '' , $detalle->anexos);
  $detalle->anexos =  str_replace( 'style=\"font-family:\"Calibri\",serif\"', '' , $detalle->anexos);
  $detalle->anexos =  str_replace( '<\/span>', '' , $detalle->anexos);
  $detalle->anexos =  str_replace( '<span>', '' , $detalle->anexos);
  $detalle->anexos =  str_replace( '</span>', '' , $detalle->anexos);
  $detalle->anexos =  str_replace( '<\/strong>', '' , $detalle->anexos);
  $detalle->anexos =  str_replace( '<strong>', '' , $detalle->anexos);
  $detalle->anexos =  str_replace( '<\/p>', '' , $detalle->anexos);
  $detalle->anexos =  str_replace( '<p>', '' , $detalle->anexos);
  $detalle->anexos =  str_replace( '</p>', '' , $detalle->anexos);
  $detalle->anexos =  str_replace( '<p style=\"margin-right:-19px; text-align:justify\">', '' , $detalle->anexos);
  $detalle->anexos =  str_replace( '<p style=\"text-align:justify\">', '' , $detalle->anexos);
  $detalle->anexos =  str_replace( '<p style="text-align:justify">', '' , $detalle->anexos);
  $detalle->anexos =  str_replace( '<div style=\"text-align:justify\">', '' , $detalle->anexos);
  $detalle->anexos =  str_replace( '<\/div>', '' , $detalle->anexos);
  $detalle->anexos =  str_replace( '<span style="font-size:10pt">', '' , $detalle->anexos);
  $detalle->anexos =  str_replace( '<span style="font-family: ">', '' , $detalle->anexos);
  $detalle->anexos =  str_replace( '<p style="margin-right:-19px; text-align:justify">', '' , $detalle->anexos);
  $detalle->anexos =  str_replace( '<span style="font-size:11pt">', '' , $detalle->anexos);
  $detalle->anexos =  str_replace( '<span style="font-family:Calibri,sans-serif">', '' , $detalle->anexos);
  $detalle->anexos =  str_replace( '<span style="font-size:10.0pt">', '' , $detalle->anexos);



       return view('memos.memoss_detalle')
          ->with('detalle', $detalle);
}

public function envioCorreo2($tramiteId){

  foreach (PyR::get() as $key1 => $value1){$selectUsuarios[$value1->email]=$value1->nombres.' '.$value1->apellido_paterno.' '.$value1->apellido_materno.' '.$value1->unidad_desempeno.' '.$value1->email; }

          $client = new Client();
          try {
            $request = $client->get("https://simple.mindep.cl/backend/api/tramites/$tramiteId?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu" , ['verify' => false]);
            $response = $request->getBody()->getContents();
            $data =  json_decode($response);
            }catch (BadResponseException   $e) {
                $response = $e->getResponse();
            }
        $data = $data->tramite;

        $detalle = new Memo();
        $detalle->tramite_id = $data->id;
        $detalle->proceso_id = $data->proceso_id;
        $detalle->fecha_inicio = $data->fecha_inicio;
        $detalle->fecha_modificacion = $data->fecha_modificacion;
        $detalle->fecha_termino = $data->fecha_termino;

        $datos=$data->datos;
        if($datos){
          foreach ($datos as $key => $value2) {

            if(property_exists($value2, 'adjuntar_memo')){
              $detalle->doc_memo = $value2->adjuntar_memo->URL;
            }

            if(property_exists($value2, 'anexo_a')){
              $detalle->doc_anexo_a = $value2->anexo_a->URL;
            }

            if(property_exists($value2, 'anexo_b')){
              $detalle->doc_anexo_b = $value2->anexo_b->URL;
            }

            if(property_exists($value2, 'anexo_c')){
              $detalle->doc_anexo_c = $value2->anexo_c->URL;
            }

            if(property_exists($value2, 'adjuntar_memo')){
              $detalle->doc_anexo_d = $value2->adjuntar_memo->URL;
            }

            if(property_exists($value2, 'anexos')){
              $detalle->anexos = $value2->anexos;
            }

            if(property_exists($value2, 'division')){
              $detalle->division = $value2->division;
            }

            if(property_exists($value2, 'lugar')){
              $detalle->lugar = $value2->lugar;
            }

            if(property_exists($value2, 'fecha')){
              $detalle->fecha = $value2->fecha;
            }

            if(property_exists($value2, 'para')){
              $detalle->destinatario = $value2->para;
            }

            if(property_exists($value2, 'de')){
              $detalle->remitente = $value2->de;
            }

            if(property_exists($value2, 'de_subrogante')){
              $detalle->remitente = $value2->de_subrogante;
            }

            if(property_exists($value2, 'asunto')){
              $detalle->materia = $value2->asunto;
            }

            if(property_exists($value2, 'texto')){
              $detalle->texto = $value2->texto;
            }

            if(property_exists($value2, 'vistos')){
              $detalle->vistos = $value2->vistos;
            }

            if(property_exists($value2, 'distribucion')){
              $detalle->distribucion = $value2->distribucion;
            }

            if(property_exists($value2, 'correlativo')){
              $detalle->correlativo = $value2->correlativo;
            }

            if(property_exists($value2, 'categoria')){
              $detalle->categoria = $value2->categoria;
              if( $value2->categoria != 'sin_categoria'){
                $detalle->reservado = 1;
              }
            }

          }
        }

        $detalle->materia = str_replace('"Times New Roman",serif', ' ', $detalle->materia);
        $detalle->materia = str_replace('"Arial",sans-serif', ' ', $detalle->materia);
        $detalle->materia = str_replace('"Verdana",sans-serif', ' ', $detalle->materia);
        $detalle->materia = str_replace('"Times New Roman",serif', ' ', $detalle->materia);
        $detalle->materia = str_replace('"Arial",sans-serif', ' ', $detalle->materia);
        $detalle->materia = str_replace('"Verdana",sans-serif', ' ', $detalle->materia);
        $detalle->materia = str_replace('"Calibri",sans-serif', ' ', $detalle->materia);
        $detalle->materia = str_replace('"Century Gothic",sans-serif', ' ', $detalle->materia);
        $detalle->materia = str_replace('",serif\"="">', ' ', $detalle->materia);
        $detalle->materia =  str_replace( 'style=\"font-family:\"Times New Roman\",serif\"', '' , $detalle->materia);
        $detalle->materia =  str_replace( 'style=\"font-family:\"Verdana\",serif\"', '' , $detalle->materia);
        $detalle->materia =  str_replace( 'style=\"font-family:\"Arial\",serif\"', '' , $detalle->materia);
        $detalle->materia =  str_replace( 'style=\"font-family:\"Calibri\",serif\"', '' , $detalle->materia);
        $detalle->materia =  str_replace( 'style=\"font-family:\"Century Gothic\",sans-serif\"', '' , $detalle->materia);
        $detalle->materia =  str_replace( '<\/span>', '' , $detalle->materia);
        $detalle->materia =  str_replace( '<span>', '' , $detalle->materia);
        $detalle->materia =  str_replace( '<\/strong>', '' , $detalle->materia);
        $detalle->materia =  str_replace( '<strong>', '' , $detalle->materia);
        $detalle->materia =  str_replace( '<\/p>', '' , $detalle->materia);
        $detalle->materia =  str_replace( '<p>', '' , $detalle->materia);
        $detalle->materia =  str_replace( '<p style=\"margin-right:-19px; text-align:justify\">', '' , $detalle->materia);
        $detalle->materia =  str_replace( '<p style=\"text-align:justify\">', '' , $detalle->materia);
        $detalle->materia =  str_replace( '<div style=\"text-align:justify\">', '' , $detalle->materia);
        $detalle->materia =  str_replace( '<\/div>', '' , $detalle->materia);

        $detalle->texto = str_replace('"Arial",sans-serif', ' ', $detalle->texto);
        $detalle->texto = str_replace('"Times New Roman",serif', ' ', $detalle->texto);
        $detalle->texto = str_replace('"Times New Roman",serif', ' ', $detalle->texto);
        $detalle->texto = str_replace('"Arial",sans-serif', ' ', $detalle->texto);
        $detalle->texto = str_replace('"Verdana",sans-serif', ' ', $detalle->texto);
        $detalle->texto = str_replace('"Calibri",sans-serif', ' ', $detalle->texto);
        $detalle->texto = str_replace('"Century Gothic",sans-serif', ' ', $detalle->texto);
        $detalle->texto = str_replace('",serif\"="">', ' ', $detalle->texto);
        $detalle->texto =  str_replace( 'style=\"font-family:\"Times New Roman\",serif\"', '' , $detalle->texto);
        $detalle->texto =  str_replace( 'style=\"font-family:\"Verdana\",serif\"', '' , $detalle->texto);
        $detalle->texto =  str_replace( 'style=\"font-family:\"Arial\",serif\"', '' , $detalle->texto);
        $detalle->texto =  str_replace( 'style=\"font-family:\"Calibri\",serif\"', '' , $detalle->texto);
        $detalle->texto =  str_replace( 'style=\"font-family:\"Century Gothic\",sans-serif\"', '' , $detalle->texto);
        $detalle->texto =  str_replace( '<\/span>', '' , $detalle->texto);
        $detalle->texto =  str_replace( '<span>', '' , $detalle->texto);
        $detalle->texto =  str_replace( '<\/strong>', '' , $detalle->texto);
        $detalle->texto =  str_replace( '<strong>', '' , $detalle->texto);
        $detalle->texto =  str_replace( '<\/p>', '' , $detalle->texto);
        $detalle->texto =  str_replace( '<p>', '' , $detalle->texto);
        $detalle->texto =  str_replace( '<p style=\"margin-right:-19px; text-align:justify\">', '' , $detalle->texto);
        $detalle->texto =  str_replace( '<p style=\"text-align:justify\">', '' , $detalle->texto);
        $detalle->texto =  str_replace( '<div style=\"text-align:justify\">', '' , $detalle->texto);
        $detalle->texto =  str_replace( '<\/div>', '' , $detalle->texto);

        $detalle->distribucion = str_replace('"Arial",sans-serif', ' ', $detalle->distribucion);
        $detalle->distribucion = str_replace('"Times New Roman",serif', ' ', $detalle->distribucion);
        $detalle->distribucion = str_replace('"Times New Roman",serif', ' ', $detalle->distribucion);
        $detalle->distribucion = str_replace('"Arial",sans-serif', ' ', $detalle->distribucion);
        $detalle->distribucion = str_replace('"Verdana",sans-serif', ' ', $detalle->distribucion);
        $detalle->distribucion = str_replace('"Calibri",sans-serif', ' ', $detalle->distribucion);
        $detalle->distribucion = str_replace('"Century Gothic",sans-serif', ' ', $detalle->distribucion);
        $detalle->distribucion = str_replace('",serif\"="">', ' ', $detalle->distribucion);
        $detalle->distribucion =  str_replace( 'style=\"font-family:\"Times New Roman\",serif\"', '' , $detalle->distribucion);
        $detalle->distribucion =  str_replace( 'style=\"font-family:\"Verdana\",serif\"', '' , $detalle->distribucion);
        $detalle->distribucion =  str_replace( 'style=\"font-family:\"Arial\",serif\"', '' , $detalle->distribucion);
        $detalle->distribucion =  str_replace( 'style=\"font-family:\"Calibri\",serif\"', '' , $detalle->distribucion);
        $detalle->distribucion =  str_replace( 'style=\"font-family:\"Century Gothic\",sans-serif\"', '' , $detalle->distribucion);
        $detalle->distribucion =  str_replace( '<\/span>', '' , $detalle->distribucion);
        $detalle->distribucion =  str_replace( '<span>', '' , $detalle->distribucion);
        $detalle->distribucion =  str_replace( '<\/strong>', '' , $detalle->distribucion);
        $detalle->distribucion =  str_replace( '<strong>', '' , $detalle->distribucion);
        $detalle->distribucion =  str_replace( '<\/p>', '' , $detalle->distribucion);
        $detalle->distribucion =  str_replace( '<p>', '' , $detalle->distribucion);
        $detalle->distribucion =  str_replace( '<p style=\"margin-right:-19px; text-align:justify\">', '' , $detalle->distribucion);
        $detalle->distribucion =  str_replace( '<p style=\"text-align:justify\">', '' , $detalle->distribucion);
        $detalle->distribucion =  str_replace( '<div style=\"text-align:justify\">', '' , $detalle->distribucion);
        $detalle->distribucion =  str_replace( '<\/div>', '' , $detalle->distribucion);

        $detalle->anexos = str_replace('"Times New Roman",serif', ' ', $detalle->anexos);
        $detalle->anexos = str_replace('"Arial",sans-serif', ' ', $detalle->anexos);
        $detalle->anexos = str_replace('"Verdana",sans-serif', ' ', $detalle->anexos);
        $detalle->anexos = str_replace('"Calibri",sans-serif', ' ', $detalle->anexos);
        $detalle->anexos = str_replace('"Century Gothic",sans-serif', ' ', $detalle->anexos);
        $detalle->anexos = str_replace('",serif\"="">', ' ', $detalle->anexos);
        $detalle->anexos =  str_replace( 'style=\"font-family:\"Times New Roman\",serif\"', '' , $detalle->anexos);
        $detalle->anexos =  str_replace( 'style=\"font-family:\"Verdana\",serif\"', '' , $detalle->anexos);
        $detalle->anexos =  str_replace( 'style=\"font-family:\"Arial\",serif\"', '' , $detalle->anexos);
        $detalle->anexos =  str_replace( 'style=\"font-family:\"Calibri\",serif\"', '' , $detalle->anexos);
        $detalle->anexos =  str_replace( '<\/span>', '' , $detalle->anexos);
        $detalle->anexos =  str_replace( '<span>', '' , $detalle->anexos);
        $detalle->anexos =  str_replace( '</span>', '' , $detalle->anexos);
        $detalle->anexos =  str_replace( '<\/strong>', '' , $detalle->anexos);
        $detalle->anexos =  str_replace( '<strong>', '' , $detalle->anexos);
        $detalle->anexos =  str_replace( '<\/p>', '' , $detalle->anexos);
        $detalle->anexos =  str_replace( '<p>', '' , $detalle->anexos);
        $detalle->anexos =  str_replace( '</p>', '' , $detalle->anexos);
        $detalle->anexos =  str_replace( '<p style=\"margin-right:-19px; text-align:justify\">', '' , $detalle->anexos);
        $detalle->anexos =  str_replace( '<p style=\"text-align:justify\">', '' , $detalle->anexos);
        $detalle->anexos =  str_replace( '<p style="text-align:justify">', '' , $detalle->anexos);
        $detalle->anexos =  str_replace( '<div style=\"text-align:justify\">', '' , $detalle->anexos);
        $detalle->anexos =  str_replace( '<\/div>', '' , $detalle->anexos);
        $detalle->anexos =  str_replace( '<span style="font-size:10pt">', '' , $detalle->anexos);
        $detalle->anexos =  str_replace( '<span style="font-family: ">', '' , $detalle->anexos);
        $detalle->anexos =  str_replace( '<p style="margin-right:-19px; text-align:justify">', '' , $detalle->anexos);
        $detalle->anexos =  str_replace( '<span style="font-size:11pt">', '' , $detalle->anexos);
        $detalle->anexos =  str_replace( '<span style="font-family:Calibri,sans-serif">', '' , $detalle->anexos);
        $detalle->anexos =  str_replace( '<span style="font-size:10.0pt">', '' , $detalle->anexos);

     return view('memos.memoss_distribucion')
      ->with('selectUsuarios' , $selectUsuarios)
      ->with('detalle' , $detalle);
}

public function distribucionStd2(Request $request , $tramiteId){

  $correosInternos = $request->correos_internos;
  $correosExternos = $request->correos_externos;
  $observaciones = $request->observaciones;

  $client = new Client();
  try {
    $request = $client->get("https://simple.mindep.cl/backend/api/tramites/$tramiteId?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu" , ['verify' => false]);
    $response = $request->getBody()->getContents();
    $data =  json_decode($response);
    }catch (BadResponseException   $e) {
        $response = $e->getResponse();
    }
$data = $data->tramite;

$detalle = new Memo();
$detalle->tramite_id = $data->id;
$detalle->proceso_id = $data->proceso_id;
$detalle->fecha_inicio = $data->fecha_inicio;
$detalle->fecha_modificacion = $data->fecha_modificacion;
$detalle->fecha_termino = $data->fecha_termino;

$datos=$data->datos;
if($datos){
  foreach ($datos as $key => $value2) {

    if(property_exists($value2, 'adjuntar_memo')){
      $detalle->doc_memo = $value2->adjuntar_memo->URL;
    }

    if(property_exists($value2, 'anexo_a')){
      $detalle->doc_anexo_a = $value2->anexo_a->URL;
    }

    if(property_exists($value2, 'anexo_b')){
      $detalle->doc_anexo_b = $value2->anexo_b->URL;
    }

    if(property_exists($value2, 'anexo_c')){
      $detalle->doc_anexo_c = $value2->anexo_c->URL;
    }

    if(property_exists($value2, 'adjuntar_memo')){
      $detalle->doc_anexo_d = $value2->adjuntar_memo->URL;
    }

    if(property_exists($value2, 'anexos')){
      $detalle->anexos = $value2->anexos;
    }

    if(property_exists($value2, 'division')){
      $detalle->division = $value2->division;
    }

    if(property_exists($value2, 'lugar')){
      $detalle->lugar = $value2->lugar;
    }

    if(property_exists($value2, 'fecha')){
      $detalle->fecha = $value2->fecha;
    }

    if(property_exists($value2, 'para')){
      $detalle->destinatario = $value2->para;
    }

    if(property_exists($value2, 'de')){
      $detalle->remitente = $value2->de;
    }

    if(property_exists($value2, 'de_subrogante')){
      $detalle->remitente = $value2->de_subrogante;
    }

    if(property_exists($value2, 'asunto')){
      $detalle->materia = $value2->asunto;
    }

    if(property_exists($value2, 'texto')){
      $detalle->texto = $value2->texto;
    }

    if(property_exists($value2, 'vistos')){
      $detalle->vistos = $value2->vistos;
    }

    if(property_exists($value2, 'distribucion')){
      $detalle->distribucion = $value2->distribucion;
    }

    if(property_exists($value2, 'correlativo')){
      $detalle->correlativo = $value2->correlativo;
    }

    if(property_exists($value2, 'categoria')){
      $detalle->categoria = $value2->categoria;
      if( $value2->categoria != 'sin_categoria'){
        $detalle->reservado = 1;
      }
    }

  }
}

$detalle->materia = str_replace('"Times New Roman",serif', ' ', $detalle->materia);
$detalle->materia = str_replace('"Arial",sans-serif', ' ', $detalle->materia);
$detalle->materia = str_replace('"Verdana",sans-serif', ' ', $detalle->materia);
$detalle->texto = str_replace('"Arial",sans-serif', ' ', $detalle->texto);
$detalle->texto = str_replace('"Times New Roman",serif', ' ', $detalle->texto);
$detalle->distribucion = str_replace('"Arial",sans-serif', ' ', $detalle->distribucion);
$detalle->distribucion = str_replace('"Times New Roman",serif', ' ', $detalle->distribucion);

$dat = ['detalle'=>$detalle , 'observaciones'=>$observaciones];

  if($correosInternos){
  if(count($correosInternos)>0){
  foreach ($correosInternos as $keyi => $valuei) {
       Mail::to($valuei)->send(new DistribucionMemoEmail($dat));
       $nuevoCorreoEnvado = new CorreoDistribucionProcesos();
       $nuevoCorreoEnvado->proceso_simple_id = 2;
       $nuevoCorreoEnvado->usuario_id =  Auth::user()->id;
       $nuevoCorreoEnvado->tramite_id =  $detalle->tramite_id;
       $nuevoCorreoEnvado->correo_destino = $valuei;
       $nuevoCorreoEnvado->observaciones = $observaciones;
       $nuevoCorreoEnvado->save();
    }
    }
  }

  if(count($correosExternos)>0){
  foreach ($correosExternos as $keye => $valuee) {
    if($valuee){
       Mail::to($valuee)->send(new DistribucionMemoEmail($dat));
       $nuevoCorreoEnvado = new CorreoDistribucionProcesos();
       $nuevoCorreoEnvado->proceso_simple_id = 2;
       $nuevoCorreoEnvado->usuario_id =  Auth::user()->id;
       $nuevoCorreoEnvado->tramite_id =  $detalle->tramite_id;
       $nuevoCorreoEnvado->correo_destino = $valuee;
       $nuevoCorreoEnvado->observaciones = $observaciones;
       $nuevoCorreoEnvado->save();
    }
    }
  }

  alert()->success('!','correos enviados exitosamente');
  return redirect()->route('Memosshow2',  $tramiteId);


}


}
