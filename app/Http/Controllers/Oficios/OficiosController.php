<?php

namespace App\Http\Controllers\Oficios;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\DerivacionesDocumento\DerivacionesDocumento;
use App\Models\IngresoTramite\IngresoTramite;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ComboBoxCompra;
use App\Models\TiposDocumentos\TiposDocumentos;
use App\Models\Divisiones\Divisiones;
use App\Models\Oficio\Oficio;
use App\Models\OficioColaboracion\OficioColaboracion;
use DB;
use App\Models\File\File;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Models\UsuariosSimple\UsuariosSimple;
use App\Models\TramitesSimple\TramitesSimple;
use Mail;
use App\Mail\DistribucionOficioEmail;
use App\Mail\TestEmail;
use App\Models\UsuariosMindep\UsuariosMindep;
use App\Models\PyR\PyR;
use App\Models\CorreoDistribucionProcesos\CorreoDistribucionProcesos;
use App\Models\Simple\TmpTramiteOficios;
use App\Models\Simple\Etapa;
use App\Models\Simple\DatoSeguimiento;



class OficiosController extends Controller
{
   public function index(Request $request){
      set_time_limit(14200);
      $selectA = Divisiones::get()->pluck('nombre', 'nombre')->prepend('Todos', null);
      $requestNRegistro = null;
      $listado = [];
      $anno = Carbon::now()->format('Y');
      $fechaStringInicio = ($anno).'-01-01 00:00:00';
      $feDesde = Carbon::parse($fechaStringInicio);
      $feHasta =  Carbon::parse(Carbon::now()->format('Y-m-d').' 23:59:59');
      $fechaInDesde = Carbon::parse($fechaStringInicio);
      $fechaInHasta = Carbon::now();
      $fechas[1] = Date('Y-m-d', strtotime( '1970-01-01' ));
      $fechas[0] = Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
      $areRemitenteView = '';
      $remitenteView = '';
      $materiaView = '';
      $destinatarioView = '';
      $correlativoView = '';
      $divisionusuarios = Auth::user()->department;
      $divisiondestinatario = Auth::user()->department;
      $reservado = 'reservado';
      $esAdmin = false;
      $listadoNoreservado = collect([]);




          if( $request->FechaDesde != null ) {
            $feDesde=(Carbon::parse($request->get('FechaDesde'))->format('dmY'));
            $feDesde= Carbon::parse(substr($feDesde, 0, 2).'-'.substr($feDesde, 2, 2).'-'.substr($feDesde, 4, 4).' 00:00:00');
          }
          if( $request->FechaHasta != null ) {
             $feHasta=(Carbon::parse($request->get('FechaHasta'))->format('dmY'));
             $feHasta= Carbon::parse(substr($feHasta, 0, 2).'-'.substr($feHasta, 2, 2).'-'.substr($feHasta, 4, 4).' 23:59:59');
          }



      $query = Oficio::query ();
      $query->when(request('nRegistro', false), function ($q, $numRegistro) {
            return $q->where('tramite_id', $numRegistro);
        });
      $query->when(request('remitente'), function ($q, $remitente) {
            return $q->where('remitente', 'LIKE', "%$remitente%");
        });
      $query->when(request('materia'), function ($q, $materia) {
            return $q->where('materia', 'LIKE', "%$materia%");
        });
      $query->when(request('destinatario'), function ($q, $destinatario) {
            return $q->where('destinatario', 'LIKE', "%$destinatario%");
        });
      $query->when($feDesde, function ($q, $feDesde ) {
            return $q->where('created_at','>=', $feDesde);
        });
      $query->when($feHasta, function ($q, $feHasta ) {
            return $q->where('created_at','<=', $feHasta);
        });

      foreach (auth()->user()->roles as $role) {
            if ($role->todos == 1) {
                $esAdmin=  true;
                break;
            }
        }
      /*if(!$esAdmin){

      $query->when(request('areaRemtiente', false), function ($q, $areaRemtiente) {
            return $q->where('division', $areaRemtiente);
        });
      $query->when($divisionusuarios, function ($q, $divisionusuarios) {
            return $q->where('division', $divisionusuarios);
        });
      $query->when($divisiondestinatario, function ($q, $divisiondestinatario) {
            return $q->orwhere('division_destinatario', $divisiondestinatario);
        });
      $query->when($reservado, function ($q, $reservado ) {
              return $q->where('reservado', 0);
          });

    }*/
    if(!$esAdmin){
     $listado = $query->get()->sortByDesc(function($q){
     return $q->correlativo->correlativo;
     });
   } else {
    $listado = $query->withTrashed()->get()->sortByDesc(function($q){
     return $q->correlativo->correlativo;
     });
   }


      //if($request->correlativo != null ) {

      $correlativoView = (int)$request->correlativo;
      foreach ($listado as $clave => $reg) {
        $reg->materia = str_replace('"Times New Roman",serif', ' ', $reg->materia);
        $reg->materia = str_replace('"Arial",sans-serif', ' ', $reg->materia);
       if($reg->correlativo != ''){
        if($reg->correlativo->correlativo === $correlativoView)
        $listado = [$reg];
      }
      }
      //}
     /* if(!$esAdmin){
      foreach ($listado as $clave => $reg) {

        if($reg->reservado == 0)
        $listadoNoreservado->push($reg);
      }
      $listado = $listadoNoreservado;
      }

      $emailUsuarioSesion = auth()->user()->email;
      $nombrecargosimple = UsuariosSimple::where('email',$emailUsuarioSesion)->first()->nombre_cargo;
      $listado2 = Oficio::where('remitente', $nombrecargosimple )->where('reservado', 1)->get();
      foreach ($listado2 as $key => $value) {
        $listado->push($value);
      }

$listado = $listado->all();*/

     $emailUsuarioSesion = auth()->user()->email;
     $nombrecargo = UsuariosSimple::where('email',$emailUsuarioSesion)->first()->nombre_cargo;

     if( $request->nRegistro != null ) {
        $requestNRegistro = $request->nRegistro;
      }
        if($request->areaRemtiente != null ) {
        $areRemitenteView =  $request->areaRemtiente;
      }
      if($request->remitente != null ) {
        $remitenteView =  $request->remitente;

      }
      if($request->materia != null ) {
        $materiaView =  $request->materia;
      }
      if($request->destinatario != null ) {
        $destinatarioView =  $request->destinatario;
      }


      return view('oficio.oficio')
      ->with('selectA' ,$selectA)
      ->with('areRemitenteView' ,$areRemitenteView)
      ->with('remitenteView' ,$remitenteView)
      ->with('materiaView' ,$materiaView)
      ->with('destinatarioView' ,$destinatarioView)
      ->with('correlativoView' ,$correlativoView)
      ->with('feDesde' ,$feDesde)
      ->with('feHasta' ,$feHasta)
      ->with('fechaInDesde' ,$fechaInDesde)
      ->with('fechaInHasta' ,$fechaInHasta)
      ->with('fechas' ,$fechas)
      ->with('requestNRegistro' , $requestNRegistro)
      ->with('listado' , $listado)
      ->with('nombrecargo', $nombrecargo)
      ->with('esAdmin', $esAdmin);


}

public function cargarExcelDeparatamentos (){


      $array = collect(Excel::load(public_path('departamentos2.xlsx'), function($reader) {})->get());
      $arrayTransformado = [];
      $cantidad = count($array[0]);

      for ($i=0; $i < $cantidad; $i++) {
        $arrayTransformado[$array[0][$i]['area']] = $array[0][$i]['area'];
       }


        return (collect($arrayTransformado)->prepend('Todos', null));

    }

public function show(Request $request, $id){
    $detalle = Oficio::where('id', $id)->withTrashed()->first();

    $detalle->materia = str_replace('"Times New Roman",serif', ' ', $detalle->materia);
    $detalle->materia = str_replace('"Arial",sans-serif', ' ', $detalle->materia);
    $detalle->antecedentes = str_replace('"Times New Roman",serif', ' ', $detalle->antecedentes);
    $detalle->antecedentes = str_replace('"Arial",sans-serif', ' ', $detalle->antecedentes);
    $detalle->contenido = str_replace('"Arial",sans-serif', ' ', $detalle->contenido);
    $detalle->contenido = str_replace('"Times New Roman",serif', ' ', $detalle->contenido);
    $detalle->distribucion = str_replace('"Arial",sans-serif', ' ', $detalle->distribucion);
    $detalle->distribucion = str_replace('"Times New Roman",serif', ' ', $detalle->distribucion);


         return view('oficio.oficio_detalle')
            ->with('detalle', $detalle);
}

public function destroy(Request $request, $id){
        $tramite = TramitesSimple::FindOrFail($id);
        $tramite1 = Oficio::where('tramite_id', $id)->first();
        $colaboraciones = OficioColaboracion::where('tramite_id', $id)->get();
        foreach ($colaboraciones as $key => $value) {
          $value->delete();
        }
        $result1 = $tramite1->delete();
        $result = $tramite->delete();
            if ($result) {
                return response()->json(['success'=>'true']);
            }else{
                return response()->json(['success'=> 'false']);
            }
    }

  public function restaurar (Request $request, $id){
        $tramite = TramitesSimple::where('id', $id)-> withTrashed() ->first();
        $tramite1 = Oficio::where('tramite_id', $id)-> withTrashed()->first();
        $colaboraciones = OficioColaboracion::where('tramite_id', $id)-> withTrashed()->get();
        foreach ($colaboraciones as $key => $value) {
          $value->restore();
        }
        $result1 = $tramite1->restore();
        $result = $tramite->restore();
        alert()->success('!','Trámite restaurado satisfactoriamente');
        return redirect()->route('Oficios');
    }

    public function envioCorreo($idOficio , $tramiteId){
          foreach (PyR::get() as $key1 => $value1){$selectUsuarios[$value1->email]=$value1->nombres.' '.$value1->apellido_paterno.' '.$value1->apellido_materno.' '.$value1->unidad_desempeno.' '.$value1->email; }
          $detalle = Oficio::where('id', $idOficio)->first();
             return view('oficio.oficio_distribucion')
              ->with('selectUsuarios' , $selectUsuarios)
              ->with('detalle' , $detalle);
    }
    public function distribucionStd(Request $request , $idOficio){

          $correosInternos = $request->correos_internos;
          $correosExternos = $request->correos_externos;
          $datos = Oficio::where('id', $idOficio)->first();
          if($correosInternos){
          foreach ($correosInternos as $keyi => $valuei) {
               Mail::to($valuei)->send(new DistribucionOficioEmail($datos));
               $nuevoCorreoEnvado = new CorreoDistribucionProcesos ();
               $nuevoCorreoEnvado->proceso_simple_id = 4;
               $nuevoCorreoEnvado->usuario_id =  Auth::user()->id;
               $nuevoCorreoEnvado->tramite_id =  $datos->tramite_id;
               $nuevoCorreoEnvado->correo_destino = $valuei;
               $nuevoCorreoEnvado->save();
            }
          }

          if(count($correosExternos)>0){
          foreach ($correosExternos as $keye => $valuee) {
            if($valuee){
               Mail::to($valuee)->send(new DistribucionOficioEmail($datos));
               $nuevoCorreoEnvado = new CorreoDistribucionProcesos ();
               $nuevoCorreoEnvado->proceso_simple_id = 2;
               $nuevoCorreoEnvado->usuario_id =  Auth::user()->id;
               $nuevoCorreoEnvado->tramite_id =  $datos->tramite_id;
               $nuevoCorreoEnvado->correo_destino = $valuei;
               $nuevoCorreoEnvado->save();
            }
            }
          }

          alert()->success('!','correos enviados exitosamente');
          return redirect()->route('Oficiosshow',  $idOficio);


    }


    public function index2(Request $request){

      set_time_limit(14200);
      $selectA = Divisiones::get()->pluck('nombre', 'nombre')->prepend('Todos', null);
      $requestNRegistro = null;
      $listado = [];
      $anno = Carbon::now()->format('Y');
      $fechaStringInicio = ($anno).'-01-01 00:00:00';
      $feDesde = Carbon::parse($fechaStringInicio);
      $feHasta =  Carbon::parse(Carbon::now()->format('Y-m-d').' 23:59:59');
      $fechaInDesde = Carbon::parse($fechaStringInicio);
      $fechaInHasta = Carbon::now();
      $fechas[1] = Date('Y-m-d', strtotime( '1970-01-01' ));
      $fechas[0] = Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
      $areRemitenteView = '';
      $remitenteView = '';
      $subroganteView = '';
      $fechaView = '';
      $materiaView = '';
      $destinatarioView = '';
      $correlativoView = '';
      $divisionusuarios = Auth::user()->department;
      $divisiondestinatario = Auth::user()->department;
      $reservado = 'reservado';
      $esAdmin = false;
      $listadoNoreservado = collect([]);


          if( $request->FechaDesde != null ) {
            $feDesde=(Carbon::parse($request->get('FechaDesde'))->format('dmY'));
            $feDesde= Carbon::parse(substr($feDesde, 0, 2).'-'.substr($feDesde, 2, 2).'-'.substr($feDesde, 4, 4).' 00:00:00');
          }
          if( $request->FechaHasta != null ) {
             $feHasta=(Carbon::parse($request->get('FechaHasta'))->format('dmY'));
             $feHasta= Carbon::parse(substr($feHasta, 0, 2).'-'.substr($feHasta, 2, 2).'-'.substr($feHasta, 4, 4).' 23:59:59');
          }

      $storeProcedium = 'call oficios()';
      $tramites = DB::connection('mysqlSimple')->select($storeProcedium);


      $query = TmpTramiteOficios::query ();
      $query->selectRaw("REPLACE(REPLACE(correlativo, '\"' , ''), '\\\' , '') as correlativo , reservado, tramite_id , proceso_id , pendiente , ended_at, fecha, fecha_inicio, etapas , http_code_correlativo, remitente, de_subrogante, division_remitente , desde, destinatario , estatus , lugar , ant, mat, contenido , vistos , distribucion , anexos , anexo_a , anexo_b, anexo_c");
      $query->where('correlativo', '!=' , null);
      $query->where('http_code_correlativo', '!=' , 404);
      $query->where('http_code_correlativo', '!=' , 429);

      $query->when(request('nRegistro', false), function ($q, $numRegistro) {
            return $q->where('tramite_id', $numRegistro);
        });

      $query->when(request('remitente'), function ($q, $remitente) {
            return $q->where('remitente', 'LIKE', "%$remitente%");
        });

        $query->when(request('de_subrogante'), function ($q, $de_subrogante) {
              return $q->where('de_subrogante', 'LIKE', "%$de_subrogante%");
          });

        $query->when(request('correlativo'), function ($q, $correlativo) {
          return $q->where('correlativo', 'LIKE', "%$correlativo%");
        });

        $query->when(request('fecha'), function ($q, $fecha) {
              return $q->where('fecha', 'LIKE', "%$fecha%");
          });

      $query->when(request('materia'), function ($q, $mat) {
            return $q->where('mat', 'LIKE', "%$mat%");
        });

      $query->when(request('destinatario'), function ($q, $destinatario) {
            return $q->where('destinatario', 'LIKE', "%$destinatario%");
        });

      $query->when($feDesde, function ($q, $feDesde ) {
            return $q->where('created_at','>=', $feDesde);
        });

      $query->when($feHasta, function ($q, $feHasta ) {
            return $q->where('created_at','<=', $feHasta);

        });

        foreach (auth()->user()->roles as $role) {
            if ($role->todos == 1) {
                $esAdmin=  true;
                break;
            }
        }


     $listado = $query->get()->sortByDesc(function($q){
     return $q->correlativo;
     });


      foreach ($listado as $clave => $reg) {
        $reg->mat = str_replace('"Times New Roman",serif', ' ', $reg->mat);
        $reg->mat = str_replace('"Arial",sans-serif', ' ', $reg->mat);
        $reg->mat = str_replace('"Verdana",sans-serif', ' ', $reg->mat);
        $reg->mat = str_replace('"Calibri",sans-serif', ' ', $reg->mat);
        $reg->mat = str_replace('"Century Gothic",sans-serif', ' ', $reg->mat);
        $reg->mat = str_replace('",serif\"="">', ' ', $reg->mat);
        $reg->mat =  str_replace( 'style=\"font-family:\"Times New Roman\",serif\"', '' , $reg->mat);
        $reg->mat =  str_replace( 'style=\"font-family:\"Verdana\",serif\"', '' , $reg->mat);
        $reg->mat =  str_replace( 'style=\"font-family:\"Arial\",serif\"', '' , $reg->mat);
        $reg->mat =  str_replace( 'style=\"font-family:\"Calibri\",serif\"', '' , $reg->mat);
        $reg->mat =  str_replace( '<\/span>', '' , $reg->mat);
        $reg->mat =  str_replace( '<span>', '' , $reg->mat);
        $reg->mat =  str_replace( '<\/strong>', '' , $reg->mat);
        $reg->mat =  str_replace( '<strong>', '' , $reg->mat);
        $reg->mat =  str_replace( '<\/p>', '' , $reg->mat);
        $reg->mat =  str_replace( '<p>', '' , $reg->mat);
        $reg->mat =  str_replace( '<p style=\"margin-right:-19px; text-align:justify\">', '' , $reg->mat);
        $reg->mat =  str_replace( '<p style=\"margin-left:340px; margin-right:-19px; text-align:justify\">', '' , $reg->mat);
        $reg->mat =  str_replace( '<p style=\"text-align:justify\">', '' , $reg->mat);
        $reg->mat =  str_replace( '<div style=\"text-align:justify\">', '' , $reg->mat);
        $reg->mat =  str_replace( '<\/div>', '' , $reg->mat);
        $reg->mat =  str_replace( 'style=\"font-family:\"Century Gothic\",sans-serif\"', '' , $reg->mat);
        $reg->mat =  str_replace( '\u00a0', '' , $reg->mat);
        $reg->mat =  str_replace( '\r\n\r\n\r\n\r\n\r\n\r\n', '' , $reg->mat);
        $reg->mat =  str_replace( '\r\n', ' ' , $reg->mat);
        $reg->mat =  str_replace( '\r\n\r\n\r\n\r\n', '' , $reg->mat);
        $reg->mat =  str_replace( '<\/b>', '' , $reg->mat);
        $reg->mat =  str_replace( '""="">', '' , $reg->mat);




        $reg->destinatario = str_replace('"Times New Roman",serif', ' ', $reg->destinatario);
        $reg->destinatario = str_replace('"Arial",sans-serif', ' ', $reg->destinatario);
        $reg->destinatario = str_replace('"Verdana",sans-serif', ' ', $reg->destinatario);
        $reg->destinatario = str_replace('"Calibri",sans-serif', ' ', $reg->destinatario);
        $reg->destinatario = str_replace('"Century Gothic",sans-serif', ' ', $reg->destinatario);
        $reg->destinatario = str_replace('",serif\"="">', ' ', $reg->destinatario);
        $reg->destinatario =  str_replace( 'style=\"font-family:\"Times New Roman\",serif\"', '' , $reg->destinatario);
        $reg->destinatario =  str_replace( 'style=\"font-family:\"Verdana\",serif\"', '' , $reg->destinatario);
        $reg->destinatario =  str_replace( 'style=\"font-family:\"Arial\",serif\"', '' , $reg->destinatario);
        $reg->destinatario =  str_replace( 'style=\"font-family:\"Calibri\",serif\"', '' , $reg->destinatario);
        $reg->destinatario =  str_replace( '<\/span>', '' , $reg->destinatario);
        $reg->destinatario =  str_replace( '<span>', '' , $reg->destinatario);
        $reg->destinatario =  str_replace( '<\/strong>', '' , $reg->destinatario);
        $reg->destinatario =  str_replace( '<strong>', '' , $reg->destinatario);
        $reg->destinatario =  str_replace( '<\/p>', '' , $reg->destinatario);
        $reg->destinatario =  str_replace( '<p>', '' , $reg->destinatario);
        $reg->destinatario =  str_replace( '<p style=\"margin-right:-19px; text-align:justify\">', '' , $reg->destinatario);
        $reg->destinatario =  str_replace( '<p style=\"margin-left:340px; margin-right:-19px; text-align:justify\">', '' , $reg->destinatario);
        $reg->destinatario =  str_replace( '<p style=\"text-align:justify\">', '' , $reg->destinatario);
        $reg->destinatario =  str_replace( '<div style=\"text-align:justify\">', '' , $reg->destinatario);
        $reg->destinatario =  str_replace( '<\/div>', '' , $reg->destinatario);
        $reg->destinatario =  str_replace( 'style=\"font-family:\"Century Gothic\",sans-serif\"', '' , $reg->destinatario);
        $reg->destinatario =  str_replace( '\u00a0', '' , $reg->destinatario);
        $reg->destinatario =  str_replace( '&Ntilde;', 'Ñ' , $reg->destinatario);
        $reg->destinatario =  str_replace( '&ntilde;', 'ñ' , $reg->destinatario);
        $reg->destinatario =  str_replace( '&Aacute;', 'Á' , $reg->destinatario);
        $reg->destinatario =  str_replace( '&Eacute;', 'É' , $reg->destinatario);
        $reg->destinatario =  str_replace( '&Iacute;', 'Í' , $reg->destinatario);
        $reg->destinatario =  str_replace( '&Oacute;', 'Ó' , $reg->destinatario);
        $reg->destinatario =  str_replace( '&Uacute;', 'Ú' , $reg->destinatario);
        $reg->destinatario =  str_replace( '&eacute;', 'é' , $reg->destinatario);
        $reg->destinatario =  str_replace( '&aacute;', 'á' , $reg->destinatario);
        $reg->destinatario =  str_replace( '&iacute;', 'í' , $reg->destinatario);
        $reg->destinatario =  str_replace( '&oacute;', 'ó' , $reg->destinatario);
        $reg->destinatario =  str_replace( '&uacute;', 'ú' , $reg->destinatario);
        $reg->destinatario =  str_replace( '<BR>', ' ' , $reg->destinatario);
        $reg->destinatario =  str_replace( '<br>', ' ' , $reg->destinatario);



        $reg->division_remitente =  str_replace( '&Ntilde;', 'Ñ' , $reg->division_remitente);
        $reg->division_remitente =  str_replace( '&ntilde;', 'ñ' , $reg->division_remitente);
        $reg->division_remitente =  str_replace( '&Aacute;', 'Á' , $reg->division_remitente);
        $reg->division_remitente =  str_replace( '&Eacute;', 'É' , $reg->division_remitente);
        $reg->division_remitente =  str_replace( '&Iacute;', 'Í' , $reg->division_remitente);
        $reg->division_remitente =  str_replace( '&Oacute;', 'Ó' , $reg->division_remitente);
        $reg->division_remitente =  str_replace( '&Uacute;', 'Ú' , $reg->division_remitente);
        $reg->division_remitente =  str_replace( '&eacute;', 'é' , $reg->division_remitente);
        $reg->division_remitente =  str_replace( '&aacute;', 'á' , $reg->division_remitente);
        $reg->division_remitente =  str_replace( '&iacute;', 'í' , $reg->division_remitente);
        $reg->division_remitente =  str_replace( '&oacute;', 'ó' , $reg->division_remitente);
        $reg->division_remitente =  str_replace( '&uacute;', 'ú' , $reg->division_remitente);
      }



     $emailUsuarioSesion = auth()->user()->email;
     $nombrecargo = UsuariosSimple::where('email',$emailUsuarioSesion)->first()->nombre_cargo;

     if( $request->nRegistro != null ) {
        $requestNRegistro = $request->nRegistro;
      }
        if($request->areaRemtiente != null ) {
        $areRemitenteView =  $request->areaRemtiente;
      }
      if($request->remitente != null ) {
        $remitenteView =  $request->remitente;
      }
      if($request->de_subrogante != null ) {
        $subroganteView =  $request->de_subrogante;
      }
      if($request->fecha != null ) {
        $fechaView =  $request->fecha;
      }
      if($request->materia != null ) {
        $materiaView =  $request->materia;
      }
      if($request->destinatario != null ) {
        $destinatarioView =  $request->destinatario;
      }

      return view('oficio.oficios')
      ->with('selectA' ,$selectA)
      ->with('areRemitenteView' ,$areRemitenteView)
      ->with('remitenteView' ,$remitenteView)
      ->with('subroganteView' ,$subroganteView)
      ->with('materiaView' ,$materiaView)
      ->with('fechaView' ,$fechaView)
      ->with('destinatarioView' ,$destinatarioView)
      ->with('correlativoView' ,$correlativoView)
      ->with('feDesde' ,$feDesde)
      ->with('feHasta' ,$feHasta)
      ->with('fechaInDesde' ,$fechaInDesde)
      ->with('fechaInHasta' ,$fechaInHasta)
      ->with('fechas' ,$fechas)
      ->with('requestNRegistro' , $requestNRegistro)
      ->with('listado' , $listado)
      ->with('nombrecargo', $nombrecargo)
      ->with('esAdmin', $esAdmin);


    }
    public function show2(Request $request, $id){

          $client = new Client();
            try {
              $request = $client->get("https://simple.mindep.cl/backend/api/tramites/$id?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu" , ['verify' => false]);
              $response = $request->getBody()->getContents();
              $data =  json_decode($response);
              }catch (BadResponseException   $e) {
                  $response = $e->getResponse();
              }
          $data = $data->tramite;

          $detalle = new Oficio();
          $detalle->tramite_id = $data->id;
          $detalle->proceso_id = $data->proceso_id;
          $detalle->fecha_inicio = $data->fecha_inicio;
          $detalle->fecha_modificacion = $data->fecha_modificacion;
          $detalle->fecha_termino = $data->fecha_termino;

          $datos=$data->datos;
          if($datos){
            foreach ($datos as $key => $value2) {

              if(property_exists($value2, 'adjuntar_ofi')){
                $detalle->doc_oficio = $value2->adjuntar_ofi->URL;
              }

              if(property_exists($value2, 'anexo_a')){
                $detalle->doc_anexo_a = $value2->anexo_a->URL;
              }

              if(property_exists($value2, 'anexo_b')){
                $detalle->doc_anexo_b = $value2->anexo_b->URL;
              }

              if(property_exists($value2, 'anexo_c')){
                $detalle->doc_anexo_c = $value2->anexo_c->URL;
              }

              if(property_exists($value2, 'adjuntar_ofi')){
                $detalle->doc_anexo_d = $value2->adjuntar_ofi->URL;
              }

              if(property_exists($value2, 'division')){
                $detalle->division = $value2->division;
              }

              if(property_exists($value2, 'lugar')){
                $detalle->lugar = $value2->lugar;
              }

              if(property_exists($value2, 'fecha_inicio')){
                $detalle->fecha = $value2->fecha_inicio;
              }

              if(property_exists($value2, 'desde')){
                $detalle->desde = $value2->desde;
              }

              if(property_exists($value2, 'a_ext')){
                $detalle->destinatario = $value2->a_ext;
              }

              if(property_exists($value2, 'remitente')){
                $detalle->remitente = $value2->remitente;
              }

              if(property_exists($value2, 'de_subrogante')){
                $detalle->remitente = $value2->de_subrogante;
              }

              if(property_exists($value2, 'ant')){
                $detalle->antecedentes = $value2->ant;
              }

              if(property_exists($value2, 'mat')){
                $detalle->materia = $value2->mat;
              }

              if(property_exists($value2, 'contenido')){
                $detalle->contenido = $value2->contenido;
              }

              if(property_exists($value2, 'vistos_responsabilidad')){
                $detalle->vistos_responsabilidad = $value2->vistos_responsabilidad;
              }

              if(property_exists($value2, 'distribucion')){
                $detalle->distribucion = $value2->distribucion;
              }

              if(property_exists($value2, 'correlativo')){
                $detalle->correlativo = $value2->correlativo;
              }

              if(property_exists($value2, 'distribuido')){
                $detalle->documento_distribuido = $value2->distribuido;
              }



              if(property_exists($value2, 'categoria')){
                $detalle->categoria = $value2->categoria;
                if( $value2->categoria != 'sin_categoria'){
                  $detalle->reservado = 1;
                }
              }

            }
        }

      $detalle->materia = str_replace('"Times New Roman",serif', ' ', $detalle->materia);
      $detalle->materia = str_replace('"Arial",sans-serif', ' ', $detalle->materia);
      $detalle->materia = str_replace('"Verdana",sans-serif', ' ', $detalle->materia);
      $detalle->materia = str_replace('"Times New Roman",serif', ' ', $detalle->materia);
      $detalle->materia = str_replace('"Arial",sans-serif', ' ', $detalle->materia);
      $detalle->materia = str_replace('"Verdana",sans-serif', ' ', $detalle->materia);
      $detalle->materia = str_replace('"Calibri",sans-serif', ' ', $detalle->materia);
      $detalle->materia = str_replace('"Century Gothic",sans-serif', ' ', $detalle->materia);
      $detalle->materia = str_replace('",serif\"="">', ' ', $detalle->materia);
      $detalle->materia =  str_replace( 'style=\"font-family:\"Times New Roman\",serif\"', '' , $detalle->materia);
      $detalle->materia =  str_replace( 'style=\"font-family:\"Verdana\",serif\"', '' , $detalle->materia);
      $detalle->materia =  str_replace( 'style=\"font-family:\"Arial\",serif\"', '' , $detalle->materia);
      $detalle->materia =  str_replace( 'style=\"font-family:\"Calibri\",serif\"', '' , $detalle->materia);
      $detalle->materia =  str_replace( 'style=\"font-family:\"Century Gothic\",sans-serif\"', '' , $detalle->materia);
      $detalle->materia =  str_replace( '<\/span>', '' , $detalle->materia);
      $detalle->materia =  str_replace( '<span>', '' , $detalle->materia);
      $detalle->materia =  str_replace( '<\/strong>', '' , $detalle->materia);
      $detalle->materia =  str_replace( '<strong>', '' , $detalle->materia);
      $detalle->materia =  str_replace( '<\/p>', '' , $detalle->materia);
      $detalle->materia =  str_replace( '<p>', '' , $detalle->materia);
      $detalle->materia =  str_replace( '<p style=\"margin-right:-19px; text-align:justify\">', '' , $detalle->materia);
      $detalle->materia =  str_replace( '<p style=\"text-align:justify\">', '' , $detalle->materia);
      $detalle->materia =  str_replace( '<div style=\"text-align:justify\">', '' , $detalle->materia);
      $detalle->materia =  str_replace( '<\/div>', '' , $detalle->materia);
      $detalle->materia =  str_replace( 'style=\"font-family:\"Century Gothic\",sans-serif\"', '' , $detalle->materia);
      $detalle->materia =  str_replace( '\u00a0', '' , $detalle->materia);
      $detalle->materia =  str_replace( '\r\n\r\n\r\n\r\n\r\n\r\n', '' , $detalle->materia);
      $detalle->materia =  str_replace( '\r\n', ' ' , $detalle->materia);
      $detalle->materia =  str_replace( '\r\n\r\n\r\n\r\n', '' , $detalle->materia);
      $detalle->materia =  str_replace( '<\/b>', '' , $detalle->materia);
      $detalle->materia =  str_replace( '""="">', '' , $detalle->materia);

      $detalle->antecedentes = str_replace('"Times New Roman",serif', ' ', $detalle->antecedentes);
      $detalle->antecedentes = str_replace('"Arial",sans-serif', ' ', $detalle->antecedentes);
      $detalle->antecedentes = str_replace('"Verdana",sans-serif', ' ', $detalle->antecedentes);
      $detalle->antecedentes = str_replace('"Times New Roman",serif', ' ', $detalle->antecedentes);
      $detalle->antecedentes = str_replace('"Arial",sans-serif', ' ', $detalle->antecedentes);
      $detalle->antecedentes = str_replace('"Verdana",sans-serif', ' ', $detalle->antecedentes);
      $detalle->antecedentes = str_replace('"Calibri",sans-serif', ' ', $detalle->antecedentes);
      $detalle->antecedentes = str_replace('"Century Gothic",sans-serif', ' ', $detalle->antecedentes);
      $detalle->antecedentes = str_replace('",serif\"="">', ' ', $detalle->antecedentes);
      $detalle->antecedentes =  str_replace( 'style=\"font-family:\"Times New Roman\",serif\"', '' , $detalle->antecedentes);
      $detalle->antecedentes =  str_replace( 'style=\"font-family:\"Verdana\",serif\"', '' , $detalle->antecedentes);
      $detalle->antecedentes =  str_replace( 'style=\"font-family:\"Arial\",serif\"', '' , $detalle->antecedentes);
      $detalle->antecedentes =  str_replace( 'style=\"font-family:\"Calibri\",serif\"', '' , $detalle->antecedentes);
      $detalle->antecedentes =  str_replace( 'style=\"font-family:\"Century Gothic\",sans-serif\"', '' , $detalle->antecedentes);
      $detalle->antecedentes =  str_replace( '<\/span>', '' , $detalle->antecedentes);
      $detalle->antecedentes =  str_replace( '<span>', '' , $detalle->antecedentes);
      $detalle->antecedentes =  str_replace( '<\/strong>', '' , $detalle->antecedentes);
      $detalle->antecedentes =  str_replace( '<strong>', '' , $detalle->antecedentes);
      $detalle->antecedentes =  str_replace( '<\/p>', '' , $detalle->antecedentes);
      $detalle->antecedentes =  str_replace( '<p>', '' , $detalle->antecedentes);
      $detalle->antecedentes =  str_replace( '<p style=\"margin-right:-19px; text-align:justify\">', '' , $detalle->antecedentes);
      $detalle->antecedentes =  str_replace( '<p style=\"text-align:justify\">', '' , $detalle->antecedentes);
      $detalle->antecedentes =  str_replace( '<div style=\"text-align:justify\">', '' , $detalle->antecedentes);
      $detalle->antecedentes =  str_replace( '<\/div>', '' , $detalle->antecedentes);
      $detalle->antecedentes =  str_replace( 'style=\"font-family:\"Century Gothic\",sans-serif\"', '' , $detalle->antecedentes);
      $detalle->antecedentes =  str_replace( '\u00a0', '' , $detalle->antecedentes);
      $detalle->antecedentes =  str_replace( '\r\n\r\n\r\n\r\n\r\n\r\n', '' , $detalle->antecedentes);
      $detalle->antecedentes =  str_replace( '\r\n', ' ' , $detalle->antecedentes);
      $detalle->antecedentes =  str_replace( '\r\n\r\n\r\n\r\n', '' , $detalle->antecedentes);
      $detalle->antecedentes =  str_replace( '<\/b>', '' , $detalle->antecedentes);
      $detalle->antecedentes =  str_replace( '""="">', '' , $detalle->antecedentes);

      $detalle->destinatario = str_replace('"Times New Roman",serif', ' ', $detalle->destinatario);
      $detalle->destinatario = str_replace('"Arial",sans-serif', ' ', $detalle->destinatario);
      $detalle->destinatario = str_replace('"Verdana",sans-serif', ' ', $detalle->destinatario);
      $detalle->destinatario = str_replace('"Calibri",sans-serif', ' ', $detalle->destinatario);
      $detalle->destinatario = str_replace('"Century Gothic",sans-serif', ' ', $detalle->destinatario);
      $detalle->destinatario = str_replace('",serif\"="">', ' ', $detalle->destinatario);
      $detalle->destinatario =  str_replace( 'style=\"font-family:\"Times New Roman\",serif\"', '' , $detalle->destinatario);
      $detalle->destinatario =  str_replace( 'style=\"font-family:\"Verdana\",serif\"', '' , $detalle->destinatario);
      $detalle->destinatario =  str_replace( 'style=\"font-family:\"Arial\",serif\"', '' , $detalle->destinatario);
      $detalle->destinatario =  str_replace( 'style=\"font-family:\"Calibri\",serif\"', '' , $detalle->destinatario);
      $detalle->destinatario =  str_replace( '<\/span>', '' , $detalle->destinatario);
      $detalle->destinatario =  str_replace( '<span>', '' , $detalle->destinatario);
      $detalle->destinatario =  str_replace( '<\/strong>', '' , $detalle->destinatario);
      $detalle->destinatario =  str_replace( '<strong>', '' , $detalle->destinatario);
      $detalle->destinatario =  str_replace( '<\/p>', '' , $detalle->destinatario);
      $detalle->destinatario =  str_replace( '<p>', '' , $detalle->destinatario);
      $detalle->destinatario =  str_replace( '<p style=\"margin-right:-19px; text-align:justify\">', '' , $detalle->destinatario);
      $detalle->destinatario =  str_replace( '<p style=\"margin-left:340px; margin-right:-19px; text-align:justify\">', '' , $detalle->destinatario);
      $detalle->destinatario =  str_replace( '<p style=\"text-align:justify\">', '' , $detalle->destinatario);
      $detalle->destinatario =  str_replace( '<div style=\"text-align:justify\">', '' , $detalle->destinatario);
      $detalle->destinatario =  str_replace( '<\/div>', '' , $detalle->destinatario);
      $detalle->destinatario =  str_replace( 'style=\"font-family:\"Century Gothic\",sans-serif\"', '' , $detalle->destinatario);
      $detalle->destinatario =  str_replace( '\u00a0', '' , $detalle->destinatario);
      $detalle->destinatario =  str_replace( '&Ntilde;', 'Ñ' , $detalle->destinatario);
      $detalle->destinatario =  str_replace( '&ntilde;', 'ñ' , $detalle->destinatario);
      $detalle->destinatario =  str_replace( '&Aacute;', 'Á' , $detalle->destinatario);
      $detalle->destinatario =  str_replace( '&Eacute;', 'É' , $detalle->destinatario);
      $detalle->destinatario =  str_replace( '&Iacute;', 'Í' , $detalle->destinatario);
      $detalle->destinatario =  str_replace( '&Oacute;', 'Ó' , $detalle->destinatario);
      $detalle->destinatario =  str_replace( '&Uacute;', 'Ú' , $detalle->destinatario);
      $detalle->destinatario =  str_replace( '&eacute;', 'é' , $detalle->destinatario);
      $detalle->destinatario =  str_replace( '&aacute;', 'á' , $detalle->destinatario);
      $detalle->destinatario =  str_replace( '&iacute;', 'í' , $detalle->destinatario);
      $detalle->destinatario =  str_replace( '&oacute;', 'ó' , $detalle->destinatario);
      $detalle->destinatario =  str_replace( '&uacute;', 'ú' , $detalle->destinatario);
      $detalle->destinatario =  str_replace( '<BR>', ' ' , $detalle->destinatario);
      $detalle->destinatario =  str_replace( '<br>', ' ' , $detalle->destinatario);

      $detalle->division =  str_replace( '&Ntilde;', 'Ñ' , $detalle->division);
      $detalle->division =  str_replace( '&ntilde;', 'ñ' , $detalle->division);
      $detalle->division =  str_replace( '&Aacute;', 'Á' , $detalle->division);
      $detalle->division =  str_replace( '&Eacute;', 'É' , $detalle->division);
      $detalle->division =  str_replace( '&Iacute;', 'Í' , $detalle->division);
      $detalle->division =  str_replace( '&Oacute;', 'Ó' , $detalle->division);
      $detalle->division =  str_replace( '&Uacute;', 'Ú' , $detalle->division);
      $detalle->division =  str_replace( '&eacute;', 'é' , $detalle->division);
      $detalle->division =  str_replace( '&aacute;', 'á' , $detalle->division);
      $detalle->division =  str_replace( '&iacute;', 'í' , $detalle->division);
      $detalle->division =  str_replace( '&oacute;', 'ó' , $detalle->division);
      $detalle->division =  str_replace( '&uacute;', 'ú' , $detalle->division);

      $detalle->desde =  str_replace( '&Ntilde;', 'Ñ' , $detalle->desde);
      $detalle->desde =  str_replace( '&ntilde;', 'ñ' , $detalle->desde);
      $detalle->desde =  str_replace( '&Aacute;', 'Á' , $detalle->desde);
      $detalle->desde =  str_replace( '&Eacute;', 'É' , $detalle->desde);
      $detalle->desde =  str_replace( '&Iacute;', 'Í' , $detalle->desde);
      $detalle->desde =  str_replace( '&Oacute;', 'Ó' , $detalle->desde);
      $detalle->desde =  str_replace( '&Uacute;', 'Ú' , $detalle->desde);
      $detalle->desde =  str_replace( '&eacute;', 'é' , $detalle->desde);
      $detalle->desde =  str_replace( '&aacute;', 'á' , $detalle->desde);
      $detalle->desde =  str_replace( '&iacute;', 'í' , $detalle->desde);
      $detalle->desde =  str_replace( '&oacute;', 'ó' , $detalle->desde);
      $detalle->desde =  str_replace( '&uacute;', 'ú' , $detalle->desde);

      $detalle->contenido = str_replace('"Arial",sans-serif', ' ', $detalle->contenido);
      $detalle->contenido = str_replace('"Times New Roman",serif', ' ', $detalle->contenido);
      $detalle->contenido = str_replace('"Times New Roman",serif', ' ', $detalle->contenido);
      $detalle->contenido = str_replace('"Arial",sans-serif', ' ', $detalle->contenido);
      $detalle->contenido = str_replace('"Verdana",sans-serif', ' ', $detalle->contenido);
      $detalle->contenido = str_replace('"Calibri",sans-serif', ' ', $detalle->contenido);
      $detalle->contenido = str_replace('"Century Gothic",sans-serif', ' ', $detalle->contenido);
      $detalle->contenido = str_replace('",serif\"="">', ' ', $detalle->contenido);
      $detalle->contenido =  str_replace( 'style=\"font-family:\"Times New Roman\",serif\"', '' , $detalle->contenido);
      $detalle->contenido =  str_replace( 'style=\"font-family:\"Verdana\",serif\"', '' , $detalle->contenido);
      $detalle->contenido =  str_replace( 'style=\"font-family:\"Arial\",serif\"', '' , $detalle->contenido);
      $detalle->contenido =  str_replace( 'style=\"font-family:\"Calibri\",serif\"', '' , $detalle->contenido);
      $detalle->contenido =  str_replace( 'style=\"font-family:\"Century Gothic\",sans-serif\"', '' , $detalle->contenido);
      $detalle->contenido =  str_replace( '<\/span>', '' , $detalle->contenido);
      $detalle->contenido =  str_replace( '<span>', '' , $detalle->contenido);
      $detalle->contenido =  str_replace( '<\/strong>', '' , $detalle->contenido);
      $detalle->contenido =  str_replace( '<strong>', '' , $detalle->contenido);
      $detalle->contenido =  str_replace( '<\/p>', '' , $detalle->contenido);
      $detalle->contenido =  str_replace( '<p>', '' , $detalle->contenido);
      $detalle->contenido =  str_replace( '<p style=\"margin-right:-19px; text-align:justify\">', '' , $detalle->contenido);
      $detalle->contenido =  str_replace( '<p style=\"text-align:justify\">', '' , $detalle->contenido);
      $detalle->contenido =  str_replace( '<div style=\"text-align:justify\">', '' , $detalle->contenido);
      $detalle->contenido =  str_replace( '<\/div>', '' , $detalle->contenido);

      $detalle->distribucion = str_replace('"Arial",sans-serif', ' ', $detalle->distribucion);
      $detalle->distribucion = str_replace('"Times New Roman",serif', ' ', $detalle->distribucion);
      $detalle->distribucion = str_replace('"Times New Roman",serif', ' ', $detalle->distribucion);
      $detalle->distribucion = str_replace('"Arial",sans-serif', ' ', $detalle->distribucion);
      $detalle->distribucion = str_replace('"Verdana",sans-serif', ' ', $detalle->distribucion);
      $detalle->distribucion = str_replace('"Calibri",sans-serif', ' ', $detalle->distribucion);
      $detalle->distribucion = str_replace('"Century Gothic",sans-serif', ' ', $detalle->distribucion);
      $detalle->distribucion = str_replace('",serif\"="">', ' ', $detalle->distribucion);
      $detalle->distribucion =  str_replace( 'style=\"font-family:\"Times New Roman\",serif\"', '' , $detalle->distribucion);
      $detalle->distribucion =  str_replace( 'style=\"font-family:\"Verdana\",serif\"', '' , $detalle->distribucion);
      $detalle->distribucion =  str_replace( 'style=\"font-family:\"Arial\",serif\"', '' , $detalle->distribucion);
      $detalle->distribucion =  str_replace( 'style=\"font-family:\"Calibri\",serif\"', '' , $detalle->distribucion);
      $detalle->distribucion =  str_replace( 'style=\"font-family:\"Century Gothic\",sans-serif\"', '' , $detalle->distribucion);
      $detalle->distribucion =  str_replace( '<\/span>', '' , $detalle->distribucion);
      $detalle->distribucion =  str_replace( '<span>', '' , $detalle->distribucion);
      $detalle->distribucion =  str_replace( '<\/strong>', '' , $detalle->distribucion);
      $detalle->distribucion =  str_replace( '<strong>', '' , $detalle->distribucion);
      $detalle->distribucion =  str_replace( '<\/p>', '' , $detalle->distribucion);
      $detalle->distribucion =  str_replace( '<p>', '' , $detalle->distribucion);
      $detalle->distribucion =  str_replace( '<p style=\"margin-right:-19px; text-align:justify\">', '' , $detalle->distribucion);
      $detalle->distribucion =  str_replace( '<p style=\"text-align:justify\">', '' , $detalle->distribucion);
      $detalle->distribucion =  str_replace( '<div style=\"text-align:justify\">', '' , $detalle->distribucion);
      $detalle->distribucion =  str_replace( '<\/div>', '' , $detalle->distribucion);


           return view('oficio.oficios_detalle')
              ->with('detalle', $detalle);
    }

    public function envioCorreo2($tramiteId){

      foreach (PyR::get() as $key1 => $value1){$selectUsuarios[$value1->email]=$value1->nombres.' '.$value1->apellido_paterno.' '.$value1->apellido_materno.' '.$value1->unidad_desempeno.' '.$value1->email; }

              $client = new Client();
              try {
                $request = $client->get("https://simple.mindep.cl/backend/api/tramites/$tramiteId?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu" , ['verify' => false]);
                $response = $request->getBody()->getContents();
                $data =  json_decode($response);
                }catch (BadResponseException   $e) {
                    $response = $e->getResponse();
                }
            $data = $data->tramite;

            $detalle = new Oficio();
            $detalle->tramite_id = $data->id;
            $detalle->proceso_id = $data->proceso_id;
            $detalle->fecha_inicio = $data->fecha_inicio;
            $detalle->fecha_modificacion = $data->fecha_modificacion;
            $detalle->fecha_termino = $data->fecha_termino;

            $datos=$data->datos;
            if($datos){
              foreach ($datos as $key => $value2) {

                if(property_exists($value2, 'adjuntar_ofi')){
                  $detalle->doc_oficio = $value2->adjuntar_ofi->URL;
                }

                if(property_exists($value2, 'anexo_a')){
                  $detalle->doc_anexo_a = $value2->anexo_a->URL;
                }

                if(property_exists($value2, 'anexo_b')){
                  $detalle->doc_anexo_b = $value2->anexo_b->URL;
                }

                if(property_exists($value2, 'anexo_c')){
                  $detalle->doc_anexo_c = $value2->anexo_c->URL;
                }

                if(property_exists($value2, 'adjuntar_ofi')){
                  $detalle->doc_anexo_d = $value2->adjuntar_ofi->URL;
                }

                if(property_exists($value2, 'division')){
                  $detalle->division = $value2->division;
                }

                if(property_exists($value2, 'lugar')){
                  $detalle->lugar = $value2->lugar;
                }

                if(property_exists($value2, 'fecha_inicio')){
                  $detalle->fecha = $value2->fecha_inicio;
                }

                if(property_exists($value2, 'desde')){
                  $detalle->desde = $value2->desde;
                }

                if(property_exists($value2, 'a_ext')){
                  $detalle->destinatario = $value2->a_ext;
                }

                if(property_exists($value2, 'remitente')){
                  $detalle->remitente = $value2->remitente;
                }

                if(property_exists($value2, 'de_subrogante')){
                  $detalle->remitente = $value2->de_subrogante;
                }

                if(property_exists($value2, 'ant')){
                  $detalle->antecedentes = $value2->ant;
                }

                if(property_exists($value2, 'mat')){
                  $detalle->materia = $value2->mat;
                }

                if(property_exists($value2, 'contenido')){
                  $detalle->contenido = $value2->contenido;
                }

                if(property_exists($value2, 'vistos_responsabilidad')){
                  $detalle->vistos_responsabilidad = $value2->vistos_responsabilidad;
                }

                if(property_exists($value2, 'distribucion')){
                  $detalle->distribucion = $value2->distribucion;
                }

                if(property_exists($value2, 'correlativo')){
                  $detalle->correlativo = $value2->correlativo;
                }

                if(property_exists($value2, 'categoria')){
                  $detalle->categoria = $value2->categoria;
                  if( $value2->categoria != 'sin_categoria'){
                    $detalle->reservado = 1;
                  }
                }

              }
          }

          $detalle->materia = str_replace('"Times New Roman",serif', ' ', $detalle->materia);
          $detalle->materia = str_replace('"Arial",sans-serif', ' ', $detalle->materia);
          $detalle->materia = str_replace('"Verdana",sans-serif', ' ', $detalle->materia);
          $detalle->materia = str_replace('"Times New Roman",serif', ' ', $detalle->materia);
          $detalle->materia = str_replace('"Arial",sans-serif', ' ', $detalle->materia);
          $detalle->materia = str_replace('"Verdana",sans-serif', ' ', $detalle->materia);
          $detalle->materia = str_replace('"Calibri",sans-serif', ' ', $detalle->materia);
          $detalle->materia = str_replace('"Century Gothic",sans-serif', ' ', $detalle->materia);
          $detalle->materia = str_replace('",serif\"="">', ' ', $detalle->materia);
          $detalle->materia =  str_replace( 'style=\"font-family:\"Times New Roman\",serif\"', '' , $detalle->materia);
          $detalle->materia =  str_replace( 'style=\"font-family:\"Verdana\",serif\"', '' , $detalle->materia);
          $detalle->materia =  str_replace( 'style=\"font-family:\"Arial\",serif\"', '' , $detalle->materia);
          $detalle->materia =  str_replace( 'style=\"font-family:\"Calibri\",serif\"', '' , $detalle->materia);
          $detalle->materia =  str_replace( 'style=\"font-family:\"Century Gothic\",sans-serif\"', '' , $detalle->materia);
          $detalle->materia =  str_replace( '<\/span>', '' , $detalle->materia);
          $detalle->materia =  str_replace( '<span>', '' , $detalle->materia);
          $detalle->materia =  str_replace( '<\/strong>', '' , $detalle->materia);
          $detalle->materia =  str_replace( '<strong>', '' , $detalle->materia);
          $detalle->materia =  str_replace( '<\/p>', '' , $detalle->materia);
          $detalle->materia =  str_replace( '<p>', '' , $detalle->materia);
          $detalle->materia =  str_replace( '<p style=\"margin-right:-19px; text-align:justify\">', '' , $detalle->materia);
          $detalle->materia =  str_replace( '<p style=\"text-align:justify\">', '' , $detalle->materia);
          $detalle->materia =  str_replace( '<div style=\"text-align:justify\">', '' , $detalle->materia);
          $detalle->materia =  str_replace( '<\/div>', '' , $detalle->materia);
          $detalle->materia =  str_replace( 'style=\"font-family:\"Century Gothic\",sans-serif\"', '' , $detalle->materia);
          $detalle->materia =  str_replace( '\u00a0', '' , $detalle->materia);
          $detalle->materia =  str_replace( '\r\n\r\n\r\n\r\n\r\n\r\n', '' , $detalle->materia);
          $detalle->materia =  str_replace( '\r\n', ' ' , $detalle->materia);
          $detalle->materia =  str_replace( '\r\n\r\n\r\n\r\n', '' , $detalle->materia);
          $detalle->materia =  str_replace( '<\/b>', '' , $detalle->materia);
          $detalle->materia =  str_replace( '""="">', '' , $detalle->materia);

          $detalle->antecedentes = str_replace('"Times New Roman",serif', ' ', $detalle->antecedentes);
          $detalle->antecedentes = str_replace('"Arial",sans-serif', ' ', $detalle->antecedentes);
          $detalle->antecedentes = str_replace('"Verdana",sans-serif', ' ', $detalle->antecedentes);
          $detalle->antecedentes = str_replace('"Times New Roman",serif', ' ', $detalle->antecedentes);
          $detalle->antecedentes = str_replace('"Arial",sans-serif', ' ', $detalle->antecedentes);
          $detalle->antecedentes = str_replace('"Verdana",sans-serif', ' ', $detalle->antecedentes);
          $detalle->antecedentes = str_replace('"Calibri",sans-serif', ' ', $detalle->antecedentes);
          $detalle->antecedentes = str_replace('"Century Gothic",sans-serif', ' ', $detalle->antecedentes);
          $detalle->antecedentes = str_replace('",serif\"="">', ' ', $detalle->antecedentes);
          $detalle->antecedentes =  str_replace( 'style=\"font-family:\"Times New Roman\",serif\"', '' , $detalle->antecedentes);
          $detalle->antecedentes =  str_replace( 'style=\"font-family:\"Verdana\",serif\"', '' , $detalle->antecedentes);
          $detalle->antecedentes =  str_replace( 'style=\"font-family:\"Arial\",serif\"', '' , $detalle->antecedentes);
          $detalle->antecedentes =  str_replace( 'style=\"font-family:\"Calibri\",serif\"', '' , $detalle->antecedentes);
          $detalle->antecedentes =  str_replace( 'style=\"font-family:\"Century Gothic\",sans-serif\"', '' , $detalle->antecedentes);
          $detalle->antecedentes =  str_replace( '<\/span>', '' , $detalle->antecedentes);
          $detalle->antecedentes =  str_replace( '<span>', '' , $detalle->antecedentes);
          $detalle->antecedentes =  str_replace( '<\/strong>', '' , $detalle->antecedentes);
          $detalle->antecedentes =  str_replace( '<strong>', '' , $detalle->antecedentes);
          $detalle->antecedentes =  str_replace( '<\/p>', '' , $detalle->antecedentes);
          $detalle->antecedentes =  str_replace( '<p>', '' , $detalle->antecedentes);
          $detalle->antecedentes =  str_replace( '<p style=\"margin-right:-19px; text-align:justify\">', '' , $detalle->antecedentes);
          $detalle->antecedentes =  str_replace( '<p style=\"text-align:justify\">', '' , $detalle->antecedentes);
          $detalle->antecedentes =  str_replace( '<div style=\"text-align:justify\">', '' , $detalle->antecedentes);
          $detalle->antecedentes =  str_replace( '<\/div>', '' , $detalle->antecedentes);
          $detalle->antecedentes =  str_replace( 'style=\"font-family:\"Century Gothic\",sans-serif\"', '' , $detalle->antecedentes);
          $detalle->antecedentes =  str_replace( '\u00a0', '' , $detalle->antecedentes);
          $detalle->antecedentes =  str_replace( '\r\n\r\n\r\n\r\n\r\n\r\n', '' , $detalle->antecedentes);
          $detalle->antecedentes =  str_replace( '\r\n', ' ' , $detalle->antecedentes);
          $detalle->antecedentes =  str_replace( '\r\n\r\n\r\n\r\n', '' , $detalle->antecedentes);
          $detalle->antecedentes =  str_replace( '<\/b>', '' , $detalle->antecedentes);
          $detalle->antecedentes =  str_replace( '""="">', '' , $detalle->antecedentes);

          $detalle->destinatario = str_replace('"Times New Roman",serif', ' ', $detalle->destinatario);
          $detalle->destinatario = str_replace('"Arial",sans-serif', ' ', $detalle->destinatario);
          $detalle->destinatario = str_replace('"Verdana",sans-serif', ' ', $detalle->destinatario);
          $detalle->destinatario = str_replace('"Calibri",sans-serif', ' ', $detalle->destinatario);
          $detalle->destinatario = str_replace('"Century Gothic",sans-serif', ' ', $detalle->destinatario);
          $detalle->destinatario = str_replace('",serif\"="">', ' ', $detalle->destinatario);
          $detalle->destinatario =  str_replace( 'style=\"font-family:\"Times New Roman\",serif\"', '' , $detalle->destinatario);
          $detalle->destinatario =  str_replace( 'style=\"font-family:\"Verdana\",serif\"', '' , $detalle->destinatario);
          $detalle->destinatario =  str_replace( 'style=\"font-family:\"Arial\",serif\"', '' , $detalle->destinatario);
          $detalle->destinatario =  str_replace( 'style=\"font-family:\"Calibri\",serif\"', '' , $detalle->destinatario);
          $detalle->destinatario =  str_replace( '<\/span>', '' , $detalle->destinatario);
          $detalle->destinatario =  str_replace( '<span>', '' , $detalle->destinatario);
          $detalle->destinatario =  str_replace( '<\/strong>', '' , $detalle->destinatario);
          $detalle->destinatario =  str_replace( '<strong>', '' , $detalle->destinatario);
          $detalle->destinatario =  str_replace( '<\/p>', '' , $detalle->destinatario);
          $detalle->destinatario =  str_replace( '<p>', '' , $detalle->destinatario);
          $detalle->destinatario =  str_replace( '<p style=\"margin-right:-19px; text-align:justify\">', '' , $detalle->destinatario);
          $detalle->destinatario =  str_replace( '<p style=\"margin-left:340px; margin-right:-19px; text-align:justify\">', '' , $detalle->destinatario);
          $detalle->destinatario =  str_replace( '<p style=\"text-align:justify\">', '' , $detalle->destinatario);
          $detalle->destinatario =  str_replace( '<div style=\"text-align:justify\">', '' , $detalle->destinatario);
          $detalle->destinatario =  str_replace( '<\/div>', '' , $detalle->destinatario);
          $detalle->destinatario =  str_replace( 'style=\"font-family:\"Century Gothic\",sans-serif\"', '' , $detalle->destinatario);
          $detalle->destinatario =  str_replace( '\u00a0', '' , $detalle->destinatario);
          $detalle->destinatario =  str_replace( '&Ntilde;', 'Ñ' , $detalle->destinatario);
          $detalle->destinatario =  str_replace( '&ntilde;', 'ñ' , $detalle->destinatario);
          $detalle->destinatario =  str_replace( '&Aacute;', 'Á' , $detalle->destinatario);
          $detalle->destinatario =  str_replace( '&Eacute;', 'É' , $detalle->destinatario);
          $detalle->destinatario =  str_replace( '&Iacute;', 'Í' , $detalle->destinatario);
          $detalle->destinatario =  str_replace( '&Oacute;', 'Ó' , $detalle->destinatario);
          $detalle->destinatario =  str_replace( '&Uacute;', 'Ú' , $detalle->destinatario);
          $detalle->destinatario =  str_replace( '&eacute;', 'é' , $detalle->destinatario);
          $detalle->destinatario =  str_replace( '&aacute;', 'á' , $detalle->destinatario);
          $detalle->destinatario =  str_replace( '&iacute;', 'í' , $detalle->destinatario);
          $detalle->destinatario =  str_replace( '&oacute;', 'ó' , $detalle->destinatario);
          $detalle->destinatario =  str_replace( '&uacute;', 'ú' , $detalle->destinatario);
          $detalle->destinatario =  str_replace( '<BR>', ' ' , $detalle->destinatario);
          $detalle->destinatario =  str_replace( '<br>', ' ' , $detalle->destinatario);

          $detalle->division =  str_replace( '&Ntilde;', 'Ñ' , $detalle->division);
          $detalle->division =  str_replace( '&ntilde;', 'ñ' , $detalle->division);
          $detalle->division =  str_replace( '&Aacute;', 'Á' , $detalle->division);
          $detalle->division =  str_replace( '&Eacute;', 'É' , $detalle->division);
          $detalle->division =  str_replace( '&Iacute;', 'Í' , $detalle->division);
          $detalle->division =  str_replace( '&Oacute;', 'Ó' , $detalle->division);
          $detalle->division =  str_replace( '&Uacute;', 'Ú' , $detalle->division);
          $detalle->division =  str_replace( '&eacute;', 'é' , $detalle->division);
          $detalle->division =  str_replace( '&aacute;', 'á' , $detalle->division);
          $detalle->division =  str_replace( '&iacute;', 'í' , $detalle->division);
          $detalle->division =  str_replace( '&oacute;', 'ó' , $detalle->division);
          $detalle->division =  str_replace( '&uacute;', 'ú' , $detalle->division);

          $detalle->desde =  str_replace( '&Ntilde;', 'Ñ' , $detalle->desde);
          $detalle->desde =  str_replace( '&ntilde;', 'ñ' , $detalle->desde);
          $detalle->desde =  str_replace( '&Aacute;', 'Á' , $detalle->desde);
          $detalle->desde =  str_replace( '&Eacute;', 'É' , $detalle->desde);
          $detalle->desde =  str_replace( '&Iacute;', 'Í' , $detalle->desde);
          $detalle->desde =  str_replace( '&Oacute;', 'Ó' , $detalle->desde);
          $detalle->desde =  str_replace( '&Uacute;', 'Ú' , $detalle->desde);
          $detalle->desde =  str_replace( '&eacute;', 'é' , $detalle->desde);
          $detalle->desde =  str_replace( '&aacute;', 'á' , $detalle->desde);
          $detalle->desde =  str_replace( '&iacute;', 'í' , $detalle->desde);
          $detalle->desde =  str_replace( '&oacute;', 'ó' , $detalle->desde);
          $detalle->desde =  str_replace( '&uacute;', 'ú' , $detalle->desde);

          $detalle->contenido = str_replace('"Arial",sans-serif', ' ', $detalle->contenido);
          $detalle->contenido = str_replace('"Times New Roman",serif', ' ', $detalle->contenido);
          $detalle->contenido = str_replace('"Times New Roman",serif', ' ', $detalle->contenido);
          $detalle->contenido = str_replace('"Arial",sans-serif', ' ', $detalle->contenido);
          $detalle->contenido = str_replace('"Verdana",sans-serif', ' ', $detalle->contenido);
          $detalle->contenido = str_replace('"Calibri",sans-serif', ' ', $detalle->contenido);
          $detalle->contenido = str_replace('"Century Gothic",sans-serif', ' ', $detalle->contenido);
          $detalle->contenido = str_replace('",serif\"="">', ' ', $detalle->contenido);
          $detalle->contenido =  str_replace( 'style=\"font-family:\"Times New Roman\",serif\"', '' , $detalle->contenido);
          $detalle->contenido =  str_replace( 'style=\"font-family:\"Verdana\",serif\"', '' , $detalle->contenido);
          $detalle->contenido =  str_replace( 'style=\"font-family:\"Arial\",serif\"', '' , $detalle->contenido);
          $detalle->contenido =  str_replace( 'style=\"font-family:\"Calibri\",serif\"', '' , $detalle->contenido);
          $detalle->contenido =  str_replace( 'style=\"font-family:\"Century Gothic\",sans-serif\"', '' , $detalle->contenido);
          $detalle->contenido =  str_replace( '<\/span>', '' , $detalle->contenido);
          $detalle->contenido =  str_replace( '<span>', '' , $detalle->contenido);
          $detalle->contenido =  str_replace( '<\/strong>', '' , $detalle->contenido);
          $detalle->contenido =  str_replace( '<strong>', '' , $detalle->contenido);
          $detalle->contenido =  str_replace( '<\/p>', '' , $detalle->contenido);
          $detalle->contenido =  str_replace( '<p>', '' , $detalle->contenido);
          $detalle->contenido =  str_replace( '<p style=\"margin-right:-19px; text-align:justify\">', '' , $detalle->contenido);
          $detalle->contenido =  str_replace( '<p style=\"text-align:justify\">', '' , $detalle->contenido);
          $detalle->contenido =  str_replace( '<div style=\"text-align:justify\">', '' , $detalle->contenido);
          $detalle->contenido =  str_replace( '<\/div>', '' , $detalle->contenido);

          $detalle->distribucion = str_replace('"Arial",sans-serif', ' ', $detalle->distribucion);
          $detalle->distribucion = str_replace('"Times New Roman",serif', ' ', $detalle->distribucion);
          $detalle->distribucion = str_replace('"Times New Roman",serif', ' ', $detalle->distribucion);
          $detalle->distribucion = str_replace('"Arial",sans-serif', ' ', $detalle->distribucion);
          $detalle->distribucion = str_replace('"Verdana",sans-serif', ' ', $detalle->distribucion);
          $detalle->distribucion = str_replace('"Calibri",sans-serif', ' ', $detalle->distribucion);
          $detalle->distribucion = str_replace('"Century Gothic",sans-serif', ' ', $detalle->distribucion);
          $detalle->distribucion = str_replace('",serif\"="">', ' ', $detalle->distribucion);
          $detalle->distribucion =  str_replace( 'style=\"font-family:\"Times New Roman\",serif\"', '' , $detalle->distribucion);
          $detalle->distribucion =  str_replace( 'style=\"font-family:\"Verdana\",serif\"', '' , $detalle->distribucion);
          $detalle->distribucion =  str_replace( 'style=\"font-family:\"Arial\",serif\"', '' , $detalle->distribucion);
          $detalle->distribucion =  str_replace( 'style=\"font-family:\"Calibri\",serif\"', '' , $detalle->distribucion);
          $detalle->distribucion =  str_replace( 'style=\"font-family:\"Century Gothic\",sans-serif\"', '' , $detalle->distribucion);
          $detalle->distribucion =  str_replace( '<\/span>', '' , $detalle->distribucion);
          $detalle->distribucion =  str_replace( '<span>', '' , $detalle->distribucion);
          $detalle->distribucion =  str_replace( '<\/strong>', '' , $detalle->distribucion);
          $detalle->distribucion =  str_replace( '<strong>', '' , $detalle->distribucion);
          $detalle->distribucion =  str_replace( '<\/p>', '' , $detalle->distribucion);
          $detalle->distribucion =  str_replace( '<p>', '' , $detalle->distribucion);
          $detalle->distribucion =  str_replace( '<p style=\"margin-right:-19px; text-align:justify\">', '' , $detalle->distribucion);
          $detalle->distribucion =  str_replace( '<p style=\"text-align:justify\">', '' , $detalle->distribucion);
          $detalle->distribucion =  str_replace( '<div style=\"text-align:justify\">', '' , $detalle->distribucion);
          $detalle->distribucion =  str_replace( '<\/div>', '' , $detalle->distribucion);

         return view('oficio.oficios_distribucion')
          ->with('selectUsuarios' , $selectUsuarios)
          ->with('detalle' , $detalle);
    }

    public function distribucionStd2(Request $request , $tramiteId){

      $correosInternos = $request->correos_internos;
      $correosExternos = $request->correos_externos;
      $observaciones = $request->observaciones;
      $client = new Client();
      try {
        $request = $client->get("https://simple.mindep.cl/backend/api/tramites/$tramiteId?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu" , ['verify' => false]);
        $response = $request->getBody()->getContents();
        $data =  json_decode($response);
        }catch (BadResponseException   $e) {
            $response = $e->getResponse();
        }
    $data = $data->tramite;

    $detalle = new Oficio();
    $detalle->tramite_id = $data->id;
    $detalle->proceso_id = $data->proceso_id;
    $detalle->fecha_inicio = $data->fecha_inicio;
    $detalle->fecha_modificacion = $data->fecha_modificacion;
    $detalle->fecha_termino = $data->fecha_termino;

    $datos=$data->datos;
    if($datos){
      foreach ($datos as $key => $value2) {

        if(property_exists($value2, 'adjuntar_ofi')){
          $detalle->doc_oficio = $value2->adjuntar_ofi->URL;
        }

        if(property_exists($value2, 'anexo_a')){
          $detalle->doc_anexo_a = $value2->anexo_a->URL;
        }

        if(property_exists($value2, 'anexo_b')){
          $detalle->doc_anexo_b = $value2->anexo_b->URL;
        }

        if(property_exists($value2, 'anexo_c')){
          $detalle->doc_anexo_c = $value2->anexo_c->URL;
        }

        if(property_exists($value2, 'adjuntar_ofi')){
          $detalle->doc_anexo_d = $value2->adjuntar_ofi->URL;
        }

        if(property_exists($value2, 'division')){
          $detalle->division = $value2->division;
        }

        if(property_exists($value2, 'lugar')){
          $detalle->lugar = $value2->lugar;
        }

        if(property_exists($value2, 'fecha_inicio')){
          $detalle->fecha = $value2->fecha_inicio;
        }

        if(property_exists($value2, 'desde')){
          $detalle->desde = $value2->desde;
        }

        if(property_exists($value2, 'a_ext')){
          $detalle->destinatario = $value2->a_ext;
        }

        if(property_exists($value2, 'remitente')){
          $detalle->remitente = $value2->remitente;
        }

        if(property_exists($value2, 'de_subrogante')){
          $detalle->remitente = $value2->de_subrogante;
        }

        if(property_exists($value2, 'ant')){
          $detalle->antecedentes = $value2->ant;
        }

        if(property_exists($value2, 'mat')){
          $detalle->materia = $value2->mat;
        }

        if(property_exists($value2, 'contenido')){
          $detalle->contenido = $value2->contenido;
        }

        if(property_exists($value2, 'vistos_responsabilidad')){
          $detalle->vistos_responsabilidad = $value2->vistos_responsabilidad;
        }

        if(property_exists($value2, 'distribucion')){
          $detalle->distribucion = $value2->distribucion;
        }

        if(property_exists($value2, 'correlativo')){
          $detalle->correlativo = $value2->correlativo;
        }

        if(property_exists($value2, 'categoria')){
          $detalle->categoria = $value2->categoria;
          if( $value2->categoria != 'sin_categoria'){
            $detalle->reservado = 1;
          }
        }

      }
  }

    $detalle->materia = str_replace('"Times New Roman",serif', ' ', $detalle->materia);
    $detalle->materia = str_replace('"Arial",sans-serif', ' ', $detalle->materia);
    $detalle->materia = str_replace('"Verdana",sans-serif', ' ', $detalle->materia);
    $detalle->contenido = str_replace('"Arial",sans-serif', ' ', $detalle->contenido);
    $detalle->contenido = str_replace('"Times New Roman",serif', ' ', $detalle->contenido);
    $detalle->distribucion = str_replace('"Arial",sans-serif', ' ', $detalle->distribucion);
    $detalle->distribucion = str_replace('"Times New Roman",serif', ' ', $detalle->distribucion);

    $dat = ['detalle'=>$detalle , 'observaciones'=>$observaciones];

      if($correosInternos){
      if(count($correosInternos)>0){
      foreach ($correosInternos as $keyi => $valuei) {
           Mail::to($valuei)->send(new DistribucionOficioEmail($dat));
           $nuevoCorreoEnvado = new CorreoDistribucionProcesos ();
           $nuevoCorreoEnvado->proceso_simple_id = 4;
           $nuevoCorreoEnvado->usuario_id =  Auth::user()->id;
           $nuevoCorreoEnvado->tramite_id =  $detalle->tramite_id;
           $nuevoCorreoEnvado->correo_destino = $valuei;
           $nuevoCorreoEnvado->observaciones = $observaciones;
           $nuevoCorreoEnvado->save();
        }
        }
      }
     

      if(count($correosExternos)>0){
      foreach ($correosExternos as $keye => $valuee) {
        if($valuee){
           Mail::to($valuee)->send(new DistribucionOficioEmail($dat));
           $nuevoCorreoEnvado = new CorreoDistribucionProcesos ();
           $nuevoCorreoEnvado->proceso_simple_id = 4;
           $nuevoCorreoEnvado->usuario_id =  Auth::user()->id;
           $nuevoCorreoEnvado->tramite_id =  $detalle->tramite_id;
           $nuevoCorreoEnvado->correo_destino = $valuee;
           $nuevoCorreoEnvado->observaciones = $observaciones;
           $nuevoCorreoEnvado->save();
        }
        }
      }

      alert()->success('!','correos enviados exitosamente');
      return redirect()->route('Oficioshow2',  $tramiteId);


    }


    }
