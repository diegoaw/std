<?php

namespace App\Http\Controllers\Opa;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Opa\OpaEntradaSalida;
use App\Models\Opa\OpaTipo;
use App\Models\Opa\OpaClasificacion;
use App\Models\Opa\OpaTipoDocumentacion;
use App\Models\Opa\OpaModalidadDocumento;
use App\Models\Opa\OpaPlataformaDocumento;
use App\Http\Requests\OPA\OpaRequest;
use Carbon\Carbon;
use App\Mail\OpaNotificacionEntradaEncomiendaEmail;
use App\Mail\OpaNotificacionSalidaEncomiendaEmail;
use App\Mail\OpaNotificacionEntradaDocumentacionEmail;
use App\Mail\OpaNotificacionSalidaDocumentacionEmail;
use App\Mail\OpaNotificacionRetiradaEncomiendaEmail;
use App\Mail\OpaNotificacionEnviadaDocumentacionEmail;
use Mail;

class OpaController extends Controller
{
    public function index(Request $request)
    {
      $dataDefinitiva = [];
      $dataDefinitiva = OpaEntradaSalida::get();

      return view('opa.opa')
      ->with('dataDefinitiva' , $dataDefinitiva);
    }

    public function create()
    {
      $radioTipo = OpaTipo::get();
      $radioClasificacion = OpaClasificacion::get();
      $radioTipoDoc = OpaTipoDocumentacion::get();
      $radioModalidad = OpaModalidadDocumento::get();
      $radioPlataforma = OpaPlataformaDocumento::get();
      return view('opa.opa_crear_ingreso_salida')
      ->with('radioClasificacion' , $radioClasificacion)
      ->with('radioTipoDoc' , $radioTipoDoc)
      ->with('radioModalidad' , $radioModalidad)
      ->with('radioPlataforma' , $radioPlataforma)
      ->with('radioTipo' , $radioTipo);
    }


     public function store(OpaRequest $request)
    {
      $tipo = $request->id_opa_tipo;
      $clasificacion = $request->id_opa_clasificacion;
      $clasificacion2 = $request->id_opa_clasificacion2;
      $tipoDoc = $request->id_opa_tipo_documentacion;
      $modalidad = $request->id_opa_modalidad_documento;
      $plataforma = $request->id_opa_plataforma_documento;
      $ndoc = $request->n_doc;
      $de = $request->de;
      $para = $request->para;
      $de_correo = $request->de_correo;
      $para_correo = $request->para_correo;
      $descripcion = $request->descripcion;
      if ($modalidad == 2 || $tipoDoc == 2) {
        $estado = 'Completado';
        $fechacierre = Carbon::now();
      }else{
        $estado = 'En Proceso';
        $fechacierre = NULL;
      }
      $altaprioridad = $request->get('alta_prioridad');
      $empaquetado = $request->get('empaquetado');
      $enruta = $request->get('en_ruta');
      $nuevoOpa = new OpaEntradaSalida();
      $nuevoOpa->id_opa_tipo = $tipo;
      if ($clasificacion) {
        $nuevoOpa->id_opa_clasificacion = $clasificacion;
      }
      if ($clasificacion2) {
        $nuevoOpa->id_opa_clasificacion = $clasificacion2;
      }
      $nuevoOpa->id_opa_tipo_documentacion = $tipoDoc;
      $nuevoOpa->id_opa_modalidad_documento = $modalidad;
      $nuevoOpa->id_opa_plataforma_documento = $plataforma;
      $nuevoOpa->n_doc = $ndoc;
      $nuevoOpa->de = $de;
      $nuevoOpa->de_correo = $de_correo;
      $nuevoOpa->para = $para;
      $nuevoOpa->para_correo = $para_correo;
      $nuevoOpa->descripcion = $descripcion;
      $nuevoOpa->estado = $estado;
      $nuevoOpa->alta_prioridad = $altaprioridad;
      $nuevoOpa->empaquetado = $empaquetado;
      $nuevoOpa->en_ruta = $enruta;
      $nuevoOpa->fecha_cierre = $fechacierre;
      $nuevoOpa->save();

      if($tipo == 1 && $nuevoOpa->id_opa_clasificacion == 1){
        if ($nuevoOpa){
          $this->NotificacionEntradaEncomienda($nuevoOpa);
        }
      }

      if($tipo == 2  && $nuevoOpa->id_opa_clasificacion == 1){
        if ($nuevoOpa){
          $this->NotificacionEntradaDocumentacion($nuevoOpa);
        }
      }

      if($tipo == 1 && $nuevoOpa->id_opa_clasificacion == 2){
        if ($nuevoOpa){
          $this->NotificacionSalidaEncomienda($nuevoOpa);
        }
      }

      if($tipo == 2 && $nuevoOpa->id_opa_clasificacion == 2){
        if ($nuevoOpa){
          $this->NotificacionSalidaDocumentacion($nuevoOpa);
        }
      }

      alert()->success('!','Creado exitosamente');
      return redirect()->route('opa');
    }

    public function NotificacionEntradaEncomienda($nuevoOpa){

      $opaEntrada = OpaEntradaSalida::where('id' , $nuevoOpa->id)->first();
      $para = $opaEntrada->para_correo;
      $datos = $opaEntrada;

      Mail::to($para)->send(new OpaNotificacionEntradaEncomiendaEmail($datos));
    }

    public function NotificacionEntradaDocumentacion($nuevoOpa){

      $opaEntrada = OpaEntradaSalida::where('id' , $nuevoOpa->id)->first();
      $para = $opaEntrada->para_correo;
      $datos = $opaEntrada;

      Mail::to($para)->send(new OpaNotificacionEntradaDocumentacionEmail($datos));
    }

    public function NotificacionSalidaEncomienda($nuevoOpa){

      $opaSalida = OpaEntradaSalida::where('id' , $nuevoOpa->id)->first();
      $de = $opaSalida->de_correo;
      $datos = $opaSalida;

      Mail::to($de)->send(new OpaNotificacionSalidaEncomiendaEmail($datos));
    }

    public function NotificacionSalidaDocumentacion($nuevoOpa){

      $opaSalida = OpaEntradaSalida::where('id' , $nuevoOpa->id)->first();
      $de = $opaSalida->de_correo;
      $datos = $opaSalida;

      Mail::to($de)->send(new OpaNotificacionSalidaDocumentacionEmail($datos));
    }


    public function edit($id)
    {
      $editOpa = OpaEntradaSalida::where('id' , $id)->first();
      $radioTipo = OpaTipo::get();
      $radioClasificacion = OpaClasificacion::get();
      $radioTipoDoc = OpaTipoDocumentacion::get();
      $radioModalidad = OpaModalidadDocumento::get();
      $radioPlataforma = OpaPlataformaDocumento::get();
      return view('opa.opa_editar_ingreso_salida')
      ->with('editOpa' , $editOpa)
      ->with('radioClasificacion' , $radioClasificacion)
      ->with('radioTipoDoc' , $radioTipoDoc)
      ->with('radioModalidad' , $radioModalidad)
      ->with('radioPlataforma' , $radioPlataforma)
      ->with('radioTipo' , $radioTipo);
    }


     public function update(Request $request, $id)
    {

      $retirado = $request->get('retirado');
      $enviado = $request->get('enviado');
      $estado = 'Completado';
      $fechacierre = Carbon::now();
      $nuevoOpa = OpaEntradaSalida::where('id' , $id)->first();
      $nuevoOpa->retirado = $retirado;
      $nuevoOpa->enviado = $enviado;
      $nuevoOpa->estado = $estado;
      $nuevoOpa->fecha_cierre = $fechacierre;
      $nuevoOpa->save();

      if($nuevoOpa->id_opa_tipo == 1){
        if ($nuevoOpa){
          $this->NotificacionRetiradaEncomienda($nuevoOpa);
        }
      }

      if($nuevoOpa->id_opa_tipo == 2){
        if ($nuevoOpa){
          $this->NotificacionEnviadaDocumentacion($nuevoOpa);
        }
      }

      alert()->success('!','Actualizado exitosamente');
      return redirect()->route('opa');
    }

    public function NotificacionRetiradaEncomienda($nuevoOpa){

      $opaSalida = OpaEntradaSalida::where('id' , $nuevoOpa->id)->first();

      if ($opaSalida->id_opa_clasificacion == 2) {
        $de = $opaSalida->de_correo;
        $datos = $opaSalida;
        Mail::to($de)->send(new OpaNotificacionRetiradaEncomiendaEmail($datos));
      }

      if ($opaSalida->id_opa_clasificacion == 1) {
        $para = $opaSalida->para_correo;
        $datos = $opaSalida;
        Mail::to($para)->send(new OpaNotificacionRetiradaEncomiendaEmail($datos));
      }

    }

    public function NotificacionEnviadaDocumentacion($nuevoOpa){

      $opaSalida = OpaEntradaSalida::where('id' , $nuevoOpa->id)->first();

      if ($opaSalida->id_opa_clasificacion == 2) {
        $de = $opaSalida->de_correo;
        $datos = $opaSalida;
        Mail::to($de)->send(new OpaNotificacionEnviadaDocumentacionEmail($datos));
      }

      if ($opaSalida->id_opa_clasificacion == 1) {
        $para = $opaSalida->para_correo;
        $datos = $opaSalida;
        Mail::to($para)->send(new OpaNotificacionEnviadaDocumentacionEmail($datos));
      }

    }


}
