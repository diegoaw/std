<?php

namespace App\Http\Controllers\Parlamentarios;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Parlamentarios\Parlamentarios;
use Carbon\Carbon;
use App\Http\Requests\Parlamentarios\ParlamentariosRequest;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Illuminate\Support\Facades\Storage;

class ParlamentariosController extends Controller
{

    public function index(Request $request)
    {

      if($request->get('FechaDesde')){
        //Session Fecha desde 
        session(['feDesde' => $request->get('FechaDesde')]);
        session(['feHasta' => $request->get('FechaHasta')]);
      }else{
        //Limpiar Variables Session
        session()->forget('feDesde');
        session()->forget('feHasta');
      }
      

       //$parlamentarios = Parlamentarios::get();
       $anno = Carbon::now()->format('Y');
       $fechaStringInicio = ($anno).'-01-01 00:00:00';
       $feDesde = Carbon::parse($fechaStringInicio);
       $feHasta =  Carbon::parse(Carbon::now()->format('Y-m-d').' 23:59:59');
       //$feHasta = $feHasta->addYear();
       $fechaInDesde = Carbon::parse($fechaStringInicio);
       $fechaInHasta = Carbon::now();
       //$fechaInHasta = $fechaInHasta->addYear();
       $fechas[1] = Date('Y-m-d', strtotime( '1970-01-01' ));
       $fechas[0] = Date('Y-m-d', strtotime('2030-12-31'));

        $ahora = Carbon::now()->format('Y-m-d');
        //$fechas = $fechas->addYear();


        if( $request->FechaDesde != null ) {
            $feDesde=(Carbon::parse($request->get('FechaDesde'))->format('dmY'));
            $feDesde= Carbon::parse(substr($feDesde, 0, 2).'-'.substr($feDesde, 2, 2).'-'.substr($feDesde, 4, 4).' 00:00:00');
        }
        if( $request->FechaHasta != null ) {
            $feHasta=(Carbon::parse($request->get('FechaHasta'))->format('dmY'));
            $feHasta= Carbon::parse(substr($feHasta, 0, 2).'-'.substr($feHasta, 2, 2).'-'.substr($feHasta, 4, 4).' 23:59:59');
        }

        $query = Parlamentarios::query ();       
        $query->when(request('FechaDesde', false), function ($q, $feDesde) {
            return $q->where('fecha_realizacion','>=', $feDesde);
        });
        $query->when(request('FechaHasta', false), function ($q, $feHasta) {
            return $q->where('fecha_realizacion','<=', $feHasta);
        });
        $parlamentarios = $query->orderby('created_at', 'desc')->get(); 
        //dd($parlamentarios);

      return view('parlamentarios.parlamentarios')
      ->with('parlamentarios' , $parlamentarios)
      ->with('feDesde' ,$feDesde)
      ->with('feHasta' ,$feHasta)
      ->with('fechaInDesde' ,$fechaInDesde)
      ->with('fechaInHasta' ,$fechaInHasta)
      ->with('fechas' ,$fechas);
    }


    public function create()
    {
      $varNull = null;
      $fechas[1] = Date('Y-m-d', strtotime( '2020-01-01' ));
      $fechas[0] = Date('Y-m-d', strtotime('2030-12-31'));
      $fe_desde =Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
      $fe_hasta =Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));

      $client = new Client();
      try {
        $request = $client->get("https://restcountries.com/v2/all" , ['verify' => false]);
        $response = $request->getBody()->getContents();
        $data =  json_decode($response);
        }catch (BadResponseException   $e) {
            $response = $e->getResponse();
        }  
        
        
      $selectPais[null] = 'Seleccione País'; foreach ($data as $key => $value) {

        $pais = $value->name;
        $apostrofe = $value->alpha2Code;
        
        { $selectPais[$value->name]=$value->name;}} 


      return view('parlamentarios.parlamentarios_crear')
      ->with('selectPais' , $selectPais)
      ->with('varNull' , $varNull)
      ->with('fechas' , $fechas)
      ->with('fe_hasta' , $fe_hasta)
      ->with('fe_desde' , $fe_desde);
    }


     public function store(ParlamentariosRequest $request)
    {
      //$ministerio = $request->ministerio;
      $nombreActividad = $request->nombre_actividad;
      $pais = $request->pais;
      //$ciudad = $request->ciudad;
      $fechaRealizacion = $request->fecha_realizacion;
      $parlamentarioInvitado = $request->parlamentario_invitado;
      $institucionConvoca = $request->institucion_convoca;
      $fechaInicio = $request->fecha_inicio;
      $fechaFin = $request->fecha_fin;
      $observacion = $request->observacion;

      $nuevoParlamento = new Parlamentarios();
      $nuevoParlamento->ministerio = 'Ministerio del Deporte';
      $nuevoParlamento->nombre_actividad = $nombreActividad;
      $nuevoParlamento->pais = $pais;
      //$nuevoParlamento->ciudad = $ciudad;
      $nuevoParlamento->fecha_realizacion = Date('Ymd', strtotime( $request->fecha_realizacion ));
      $nuevoParlamento->parlamentario_invitado = $parlamentarioInvitado;
      $nuevoParlamento->institucion_convoca = $institucionConvoca;
      $nuevoParlamento->fecha_inicio = Date('Ymd', strtotime( $request->fecha_inicio ));
      $nuevoParlamento->fecha_fin = Date('Ymd', strtotime( $request->fecha_fin ));
      $nuevoParlamento->observacion = $observacion;
      $nuevoParlamento->save();
      alert()->success('!','Viaje Parlamentario creado exitosamente');
      return redirect()->route('parlamentarios');
    }

    public function edit($id)
    {
      $varNull = null;
      $editParlamento = Parlamentarios::where('id' , $id)->first();
      $fechas[1] = Date('Y-m-d', strtotime( '2020-01-01' ));
      $fechas[0] = Date('Y-m-d', strtotime('2030-12-31'));
      $fe_desde =Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
      $fe_hasta =Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));

      $client = new Client();
      try {
        $request = $client->get("https://restcountries.com/v2/all" , ['verify' => false]);
        $response = $request->getBody()->getContents();
        $data =  json_decode($response);
        }catch (BadResponseException   $e) {
            $response = $e->getResponse();
        }  
        
        
      $selectPais[null] = 'Seleccione País'; foreach ($data as $key => $value) {

        $pais = $value->name;
        $apostrofe = $value->alpha2Code;
        
        { $selectPais[$value->name]=$value->name;}} 

      return view('parlamentarios.parlamentarios_editar')
      ->with('editParlamento' , $editParlamento)
      ->with('selectPais' , $selectPais)
      ->with('varNull' , $varNull)
      ->with('fechas' , $fechas)
      ->with('fe_hasta' , $fe_hasta)
      ->with('fe_desde' , $fe_desde);
    }


     public function update(ParlamentariosRequest $request, $id)
    {
      //$ministerio = $request->ministerio;
      $nombreActividad = $request->nombre_actividad;
      $pais = $request->pais;
      //$ciudad = $request->ciudad;
      $fechaRealizacion = $request->fecha_realizacion;
      $parlamentarioInvitado = $request->parlamentario_invitado;
      $institucionConvoca = $request->institucion_convoca;
      $fechaInicio = $request->fecha_inicio;
      $fechaFin = $request->fecha_fin;
      $observacion = $request->observacion;

      $editParlamento = Parlamentarios::where('id' , $id)->first();
      //$editParlamento->ministerio = $ministerio;
      $editParlamento->nombre_actividad = $nombreActividad;
      $editParlamento->pais = $pais;
      //$editParlamento->ciudad = $ciudad;
      $editParlamento->fecha_realizacion = Date('Ymd', strtotime( $request->fecha_realizacion ));
      $editParlamento->parlamentario_invitado = $parlamentarioInvitado;
      $editParlamento->institucion_convoca = $institucionConvoca;
      $editParlamento->fecha_inicio = Date('Ymd', strtotime( $request->fecha_inicio ));
      $editParlamento->fecha_fin = Date('Ymd', strtotime( $request->fecha_fin ));
      $editParlamento->observacion = $observacion;
      $editParlamento->save();
      alert()->success('!','Viaje Parlamentario editado exitosamente');
      return redirect()->route('parlamentarios');
    }

    public function destroy($id){
      
      $parlamentario = Parlamentarios::where('id', $id)->first();
      
      $result = $parlamentario->delete();
          if ($result) {
            alert()->success('!','Viaje Parlamentario eliminado exitosamente');
            return redirect()->route('parlamentarios');
          }else{
              return response()->json(['success'=> 'false']);
          }
    }

    public function reporte(Request $request) {

      $tipoReporte = $request->imprimir;

      //Compruebo si existe Fecha Desde, si no, imprimir todos
      if(session()->has('feDesde')){
        //Prueba imprimir var session
        $feDesde = session('feDesde');
        $feHasta = session('feHasta');
        //return $feDesde." hasta ".$feHasta;
        $dataFinal = Parlamentarios::whereBetween('fecha_realizacion', array($feDesde, $feHasta))->get();
      }else{
        $dataFinal = Parlamentarios::get();
      }

     // $dataFinal = Parlamentarios::get();

      switch ($tipoReporte) {
      case 'PDF':

      break;
      case 'XLS':
                  Excel::create("Reporte", function ($excel) use ($dataFinal) {
                  $excel->setTitle("Reporte");
                  $excel->sheet("Reporte", function ($sheet) use ($dataFinal) {
                    
                    $sheet->loadView('parlamentarios.parlamentarios_reporte_ex_excel')->with('dataFinal', $dataFinal);
              });
          })->download('xlsx');

              return back();

              break;
      default:
      break;
    }}

}
