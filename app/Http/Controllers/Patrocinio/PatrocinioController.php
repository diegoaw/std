<?php

namespace App\Http\Controllers\Patrocinio;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use App\Models\Patrocinio\Patrocinio;


class PatrocinioController extends Controller
{
   public function index(Request $request){

     $tramites = [];
     $idTramiteFiltro = $request->nRegistro;
     $queryFilter = "and tramite.id = $idTramiteFiltro ";

        $query = "SELECT  *
        FROM
       (select
        tramite.id as numero_tramite ,
        tramite.proceso_id as numero_proeceso ,
        proceso.nombre as nombre_proceso ,
        tramite.created_at as fecha_inicio ,
        tramite.updated_at as fecha_ultima_modificacion ,
        tramite.pendiente as pendiente,
        tramite.ended_at as fecha_culmino,
        tarea.nombre as etapa,
        et.id as id_etapa,
        dts.conteo as cantidad_campos
        from tramite
        INNER JOIN proceso ON tramite.proceso_id = proceso.id
        INNER JOIN (SELECT * FROM etapa WHERE id IN (SELECT MAX(id) FROM etapa GROUP BY tramite_id) ORDER BY id DESC) et on et.tramite_id = tramite.id
        INNER JOIN tarea on et.tarea_id = tarea.id
        LEFT JOIN (SELECT  COUNT(id) as conteo, etapa_id FROM dato_seguimiento  group by etapa_id ) dts on dts.etapa_id = et.id
        where tramite.proceso_id = 3  and tarea.nombre = 'Solicitud de Patrocinio' and tramite.deleted_at is null
        and dts.conteo >1
        
        UNION ALL
select
                tramite.id as numero_tramite ,
                tramite.proceso_id as numero_proeceso ,
                proceso.nombre as nombre_proceso ,
                tramite.created_at as fecha_inicio ,
                tramite.updated_at as fecha_ultima_modificacion ,
                tramite.pendiente as pendiente,
                tramite.ended_at as fecha_culmino,
                tarea.nombre as etapa,
                et.id as id_etapa,
                dts.conteo as cantidad_campos
                from tramite
                INNER JOIN proceso ON tramite.proceso_id = proceso.id
                INNER JOIN (SELECT * FROM etapa WHERE id IN (SELECT MAX(id) FROM etapa GROUP BY tramite_id) ORDER BY id DESC) et on et.tramite_id = tramite.id
                INNER JOIN tarea on et.tarea_id = tarea.id
                LEFT JOIN (SELECT  COUNT(id) as conteo, etapa_id FROM dato_seguimiento  group by etapa_id ) dts on dts.etapa_id = et.id
                where tramite.proceso_id = 3
                and tramite.deleted_at is null
                and tarea.nombre <> 'Solicitud de Patrocinio') r ";

($idTramiteFiltro != null )? $query = $query.$queryFilter : $query = $query;
$query = $query."order by r.numero_tramite DESC";

            /*  ($idTramiteFiltro != null )? $query = $query.$queryFilter : $query = $query;
             $query = $query."order by tramite.id DESC"; */

             $tramites = DB::connection('mysqlSimpleExterno')->select($query);
//dd($tramites);

    return view('patrocinio.patrocinio')
    ->with('tramitesD', $tramites)
    ->with('idTramiteFiltro', $idTramiteFiltro);

    }

    public function show($id)
    {
        $client = new Client();
               try {
              $request = $client->get("https://tramites.mindep.cl/backend/api/tramites/$id?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu" , ['verify' => false]);
              $response = $request->getBody()->getContents();
              $data =  json_decode($response);
                }catch (BadResponseException   $e) {
                    $response = $e->getResponse();
                }
         $data = $data->tramite;
         $nuevoPatrocinio = new Patrocinio();
         $nuevoPatrocinio->id =  $data->id;
         $nuevoPatrocinio->fecha_inicio =  $data->fecha_inicio;
         $nuevoPatrocinio->fecha_termino =  $data->fecha_termino;
         $nuevoPatrocinio->fecha_modificacion =  $data->fecha_modificacion;
         $nuevoPatrocinio->estado =  $data->estado;

         $datos=$data->datos;

         foreach ($datos as $key => $value2) {
             if(property_exists($value2, 'nombre')){
                $nuevoPatrocinio->nombre =  $value2->nombre;
              }
              if(property_exists($value2, 'telefono')){
                 $nuevoPatrocinio->telefono =  $value2->telefono;
              }
              if(property_exists($value2, 'comuna')){
                  $nuevoPatrocinio->region =  $value2->comuna->region;
              }
              if(property_exists($value2, 'comuna')){
                   $nuevoPatrocinio->comuna =  $value2->comuna->comuna;
              }
              if(property_exists($value2, 'direccion')){
                    $nuevoPatrocinio->direccion =  $value2->direccion;
              }
              if(property_exists($value2, 'email')){
                    $nuevoPatrocinio->email =  $value2->email;
              }
              if(property_exists($value2, 'descripcion')){
                    $nuevoPatrocinio->descripcion =  $value2->descripcion;
              }
              if(property_exists($value2, 'objetivos')){
                    $nuevoPatrocinio->objetivos =  $value2->objetivos;
              }
              if(property_exists($value2, 'fines_de_lucro')){
                    $nuevoPatrocinio->fines_de_lucro =  $value2->fines_de_lucro;
              }
              if(property_exists($value2, 'libre_acceso')){
                    $nuevoPatrocinio->libre_acceso =  $value2->libre_acceso;
              }
              if(property_exists($value2, 'si_requiere_pago')){
                    $nuevoPatrocinio->si_requiere_pago =  $value2->si_requiere_pago;
              }
              if(property_exists($value2, 'lugar_de_desarrollo')){
                    $nuevoPatrocinio->lugar_de_desarrollo =  $value2->lugar_de_desarrollo;
              }
              if(property_exists($value2, 'fecha_de_desarrollo')){
                    $nuevoPatrocinio->fecha_de_desarrollo =  $value2->fecha_de_desarrollo;
              }
              if(property_exists($value2, 'postulando_a_otros_beneficios')){
                    $nuevoPatrocinio->postulando_a_otros_beneficios =  $value2->postulando_a_otros_beneficios;
              }
              if(property_exists($value2, 'capacidad_de_atraer_demanda')){
                    $nuevoPatrocinio->capacidad_de_atraer_demanda =  $value2->capacidad_de_atraer_demanda;
              }
              if(property_exists($value2, 'posicionamiento_del_pais')){
                    $nuevoPatrocinio->posicionamiento_del_pais =  $value2->posicionamiento_del_pais;
              }
              if(property_exists($value2, 'carta_ministra')){
                    $nuevoPatrocinio->carta_ministra =  $value2->carta_ministra->URL;
              }
              if(property_exists($value2, 'tipo_de_persona')){
                    $nuevoPatrocinio->tipo_de_persona =  $value2->tipo_de_persona;
              }
              if(property_exists($value2, 'fotocopia_cedula')){
                    $nuevoPatrocinio->fotocopia_cedula =  $value2->fotocopia_cedula->URL;
              }
              if(property_exists($value2, 'tipo_de_organizacion')){
                    $nuevoPatrocinio->tipo_de_organizacion =  $value2->tipo_de_organizacion;
              }
              if(property_exists($value2, '1_a')){
                    $nuevoPatrocinio->{"1_a"} =  $value2->{"1_a"}->URL;
              }
              if(property_exists($value2, '2_a')){
                    $nuevoPatrocinio->{"2_a"} =  $value2->{"2_a"}->URL;
              }
              if(property_exists($value2, '3_a')){
                    $nuevoPatrocinio->{"3_a"} =  $value2->{"3_a"};
              }
              if(property_exists($value2, '4_a')){
                    $nuevoPatrocinio->{"4_a"} =  $value2->{"4_a"};
              }
              if(property_exists($value2, '1_b')){
                    $nuevoPatrocinio->{"1_b"} =  $value2->{"1_b"}->URL;
              }
              if(property_exists($value2, '2_b')){
                    $nuevoPatrocinio->{"2_b"} =  $value2->{"2_b"}->URL;
              }
              if(property_exists($value2, '3_b')){
                    $nuevoPatrocinio->{"3_b"} =  $value2->{"3_b"}->URL;
              }
              if(property_exists($value2, '4_b')){
                    $nuevoPatrocinio->{"4_b"} =  $value2->{"4_b"}->URL;
              }
              if(property_exists($value2, '5_b')){
                    $nuevoPatrocinio->{"5_b"} =  $value2->{"5_b"}->URL;
              }
              if(property_exists($value2, '6_b')){
                    $nuevoPatrocinio->{"6_b"} =  $value2->{"6_b"}->URL;
              }
              if(property_exists($value2, '7_b')){
                    $nuevoPatrocinio->{"7_b"} =  $value2->{"7_b"}->URL;
              }
              if(property_exists($value2, '1_c')){
                    $nuevoPatrocinio->{"1_c"} =  $value2->{"1_c"}->URL;
              }
              if(property_exists($value2, '2_c')){
                    $nuevoPatrocinio->{"2_c"} =  $value2->{"2_c"}->URL;
              }
              if(property_exists($value2, '3_c')){
                    $nuevoPatrocinio->{"3_c"} =  $value2->{"3_c"}->URL;
              }
              if(property_exists($value2, '4_c')){
                    $nuevoPatrocinio->{"4_c"} =  $value2->{"4_c"}->URL;
              }
              if(property_exists($value2, '5_c')){
                    $nuevoPatrocinio->{"5_c"} =  $value2->{"5_c"}->URL;
              }
              if(property_exists($value2, '6_c')){
                    $nuevoPatrocinio->{"6_c"} =  $value2->{"6_c"}->URL;
              }
              if(property_exists($value2, '7_c')){
                    $nuevoPatrocinio->{"7_c"} =  $value2->{"7_c"}->URL;
              }
              if(property_exists($value2, '1_d')){
                    $nuevoPatrocinio->{"1_d"} =  $value2->{"1_d"}->URL;
              }
              if(property_exists($value2, '2_d')){
                    $nuevoPatrocinio->{"2_d"} =  $value2->{"2_d"}->URL;
              }
              if(property_exists($value2, '3_d')){
                    $nuevoPatrocinio->{"3_d"} =  $value2->{"3_d"}->URL;
              }
              if(property_exists($value2, '4_d')){
                    $nuevoPatrocinio->{"4_d"} =  $value2->{"4_d"}->URL;
              }
              if(property_exists($value2, '1_e')){
                    $nuevoPatrocinio->{"1_e"} =  $value2->{"1_e"}->URL;
              }
              if(property_exists($value2, '2_e')){
                    $nuevoPatrocinio->{"2_e"} =  $value2->{"2_e"}->URL;
              }
              if(property_exists($value2, '1_f')){
                    $nuevoPatrocinio->{"1_f"} =  $value2->{"1_f"}->URL;
              }
              if(property_exists($value2, '2_f')){
                    $nuevoPatrocinio->{"2_f"} =  $value2->{"2_f"}->URL;
              }
              if(property_exists($value2, '3_f')){
                    $nuevoPatrocinio->{"3_f"} =  $value2->{"3_f"}->URL;
              }
              if(property_exists($value2, '4_f')){
                    $nuevoPatrocinio->{"4_f"} =  $value2->{"4_f"}->URL;
              }
              if(property_exists($value2, '5_f')){
                    $nuevoPatrocinio->{"5_f"} =  $value2->{"5_f"}->URL;
              }
              if(property_exists($value2, '1_g')){
                    $nuevoPatrocinio->{"1_g"} =  $value2->{"1_g"}->URL;
              }
              if(property_exists($value2, '2_g')){
                    $nuevoPatrocinio->{"2_g"} =  $value2->{"2_g"}->URL;
              }
              if(property_exists($value2, '3_g')){
                    $nuevoPatrocinio->{"3_g"} =  $value2->{"3_g"}->URL;
              }
              if(property_exists($value2, 'evento')){
                    $nuevoPatrocinio->evento =  $value2->evento;
              }
              if(property_exists($value2, 'ficha_de_solicitud')){
                    $nuevoPatrocinio->ficha_de_solicitud =  $value2->ficha_de_solicitud->URL;
              }
          }

//dd($nuevoPatrocinio);
       return view('patrocinio.patrocinio_detalle')
      ->with('data' , $nuevoPatrocinio);
    }


}
