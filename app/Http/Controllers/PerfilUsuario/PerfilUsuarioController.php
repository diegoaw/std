<?php

namespace App\Http\Controllers\PerfilUsuario;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\PerfilUsuario\ChangePasswordPerfilURequest;
use App\Models\UsuariosSimple\UsuariosSimple;

class PerfilUsuarioController extends Controller
{
    public function index(){
        $user =Auth::user() ;
        $userSimple = UsuariosSimple::where('usuario' , $user->email)->first();
        return view('perfil_usuario.pu01_i_perfil')
        ->with('user', $user)
        ->with('userSimple', $userSimple);

    }
    public function change_Paswword(Request $Request, $id){
                $user = UsuariosSimple::FindOrFail($id);
                return view('perfil_usuario.pu02_c_cambiarcontrasena')
                        ->with('user', $user);

    }
    public function update_Pasword (ChangePasswordPerfilURequest $request, $id){
        $usuario = UsuariosSimple::FindOrFail($id);

        $data = $request->all();
     
        $user =UsuariosSimple::FindOrFail($id);
        if(!Hash::check($data['old_password'], $user->password)){
           return redirect()->back()->withErrors('La contraseña actual no coincide');
        }else{
            $usuario->password =Hash::make($request->get('password'));
            $usuario->save();
            alert()->success('Contraseña actualizada satisfactoriamente');
            return redirect()->route('PerfilUsuario.index');
        }
       
    }
    public function change_Image($id){
        $user = UsuariosSimple::findOrFail($id);
        return view('perfil_usuario.se22_e_cambiar_image')
        ->with('user', $user);
    }
    public function update_Image(Request $request, $id){
        $user = User::findOrFail($id);
        /* Obtenemos una instancia del disco virtual */
        $disco = Storage::disk('avatars');
        /* Obtenemos los datos de la imágen */
        $imagen_nombre = sha1($user->id).'.'.$request->file('logotipo')->getClientOriginalExtension();
        $imagen_contenido = file_get_contents($request->file('logotipo')->getRealPath());
        /* Eliminamos la imagen anterior en caso de existir */
        if ($disco->exists($imagen_nombre)) {
            $disco->delete($imagen_nombre);
        }
        $guardar = $disco->put($imagen_nombre, $imagen_contenido);
        if ($guardar == true) {
            $user->logotipo = $imagen_nombre;
            $user->save();
        }
        alert()->success('Imagen actualizada satisfactoriamente');
        return redirect()->route('PerfilUsuario.index');
    }

}
