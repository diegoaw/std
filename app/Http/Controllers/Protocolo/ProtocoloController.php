<?php

namespace App\Http\Controllers\Protocolo;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use App\Models\Protocolo\Protocolo;


class ProtocoloController extends Controller
{
   public function index(Request $request){

     $tramites = [];
     $idTramiteFiltro = $request->nRegistro;
     $queryFilter = "and tramite.id = $idTramiteFiltro ";

        $query = "select

             tramite.id as numero_tramite ,
             tramite.proceso_id as numero_proeceso ,
             proceso.nombre as nombre_proceso ,
             tramite.created_at as fecha_inicio ,
             tramite.updated_at as fecha_ultima_modificacion ,
             tramite.deleted_at as fecha_eliminacion ,
             tramite.pendiente as pendiente,
             tramite.ended_at as fecha_culmino,
             tarea.nombre as etapa,
             et.id as id_etapa,
             dts.conteo as cantidad_campos
             from tramite
             INNER JOIN proceso ON tramite.proceso_id = proceso.id
             INNER JOIN (SELECT * FROM etapa WHERE id IN (SELECT MIN(id) FROM etapa GROUP BY tramite_id) ORDER BY id DESC) et on et.tramite_id = tramite.id
             INNER JOIN tarea on et.tarea_id = tarea.id
             LEFT JOIN (SELECT  COUNT(id) as conteo, etapa_id FROM dato_seguimiento  group by etapa_id ) dts on dts.etapa_id = et.id
             where tramite.proceso_id = 15
             and tramite.deleted_at is null
             and dts.conteo >1 ";

             ($idTramiteFiltro != null )? $query = $query.$queryFilter : $query = $query;
             $query = $query."order by tramite.id DESC";

             $tramites = DB::connection('mysqlSimpleExterno')->select($query);
//dd($tramites);

    return view('protocolo.protocolo')
    ->with('tramitesD', $tramites)
    ->with('idTramiteFiltro', $idTramiteFiltro);

    }

    public function show($id)
    {
        $client = new Client();
               try {
              $request = $client->get("https://tramites.mindep.cl/backend/api/tramites/$id?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu" , ['verify' => false]);
              $response = $request->getBody()->getContents();
              $data =  json_decode($response);
                }catch (BadResponseException   $e) {
                    $response = $e->getResponse();
                }
         $data = $data->tramite;
         $nuevoProtocolo = new Protocolo();
         $nuevoProtocolo->id =  $data->id;
         $nuevoProtocolo->fecha_inicio =  $data->fecha_inicio;
         $nuevoProtocolo->fecha_termino =  $data->fecha_termino;
         $nuevoProtocolo->fecha_modificacion =  $data->fecha_modificacion;
         $nuevoProtocolo->estado =  $data->estado;

         $datos=$data->datos;

         foreach ($datos as $key => $value2) {
             if(property_exists($value2, 'nombrecompleto')){
                $nuevoProtocolo->nombrecompleto =  $value2->nombrecompleto;
              }
              if(property_exists($value2, 'email')){
                 $nuevoProtocolo->email =  $value2->email;
              }
              if(property_exists($value2, 'telefono')){
                  $nuevoProtocolo->telefono =  $value2->telefono;
              }
              if(property_exists($value2, 'mensaje')){
                    $nuevoProtocolo->mensaje =  $value2->mensaje;
              }
              if(property_exists($value2, 'archivo_adjuntar')){
                    $nuevoProtocolo->archivo_adjuntar =  $value2->archivo_adjuntar->URL;
              }
              if(property_exists($value2, 'mensaje_respuesta')){
                    $nuevoProtocolo->mensaje_respuesta =  $value2->mensaje_respuesta;
              }
              if(property_exists($value2, 'adjuntar_archivo')){
                    $nuevoProtocolo->adjuntar_archivo =  $value2->adjuntar_archivo->URL;
              }
            }
              return view('protocolo.protocolo_detalle')
             ->with('data' , $nuevoProtocolo);
    }

}
