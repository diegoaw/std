<?php

namespace App\Http\Controllers\Reclamos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use App\Models\Reclamos\Reclamos;


class ReclamosController extends Controller
{
   public function index(Request $request){

     $tramites = [];
     $idTramiteFiltro = $request->nRegistro;
     $queryFilter = "and tramite.id = $idTramiteFiltro ";

        $query = "select

             tramite.id as numero_tramite ,
             tramite.proceso_id as numero_proeceso ,
             proceso.nombre as nombre_proceso ,
             tramite.created_at as fecha_inicio ,
             tramite.updated_at as fecha_ultima_modificacion ,
             tramite.deleted_at as fecha_eliminacion ,
             tramite.pendiente as pendiente,
             tramite.ended_at as fecha_culmino,
             et.id as id_etapa,
             dts.conteo as cantidad_campos
             from tramite
             INNER JOIN proceso ON tramite.proceso_id = proceso.id
             INNER JOIN (SELECT * FROM etapa WHERE id IN (SELECT MAX(id) FROM etapa GROUP BY tramite_id) ORDER BY id DESC) et on et.tramite_id = tramite.id
             INNER JOIN tarea on et.tarea_id = tarea.id
             LEFT JOIN (SELECT  COUNT(id) as conteo, etapa_id FROM dato_seguimiento  group by etapa_id ) dts on dts.etapa_id = et.id
             where tramite.proceso_id = 11
             and tramite.deleted_at is null
             and dts.conteo >1 ";

             ($idTramiteFiltro != null )? $query = $query.$queryFilter : $query = $query;
             $query = $query."order by tramite.id DESC";

             $tramites = DB::connection('mysqlSimpleExterno')->select($query);
//dd($tramites);

    return view('reclamos.reclamos')
    ->with('tramitesD', $tramites)
    ->with('idTramiteFiltro', $idTramiteFiltro);

    }
    public function show($id)
    {
        $client = new Client();
               try {
              $request = $client->get("https://tramites.mindep.cl/backend/api/tramites/$id?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu" , ['verify' => false]);
              $response = $request->getBody()->getContents();
              $data =  json_decode($response);
                }catch (BadResponseException   $e) {
                    $response = $e->getResponse();
                }
         $data = $data->tramite;
         $nuevoReclamos = new Reclamos();
         $nuevoReclamos->id =  $data->id;
         $nuevoReclamos->fecha_inicio =  $data->fecha_inicio;
         $nuevoReclamos->fecha_termino =  $data->fecha_termino;
         $nuevoReclamos->fecha_modificacion =  $data->fecha_modificacion;
         $nuevoReclamos->estado =  $data->estado;

         $datos=$data->datos;

         foreach ($datos as $key => $value2) {
             if(property_exists($value2, 'servicio_bien')){
                $nuevoReclamos->servicio_bien =  $value2->servicio_bien;
              }
              if(property_exists($value2, 'nombre')){
                 $nuevoReclamos->nombre =  $value2->nombre;
              }
              if(property_exists($value2, 'region')){
                  $nuevoReclamos->region =  $value2->region->region;
              }
              if(property_exists($value2, 'region')){
                   $nuevoReclamos->comuna =  $value2->region->comuna;
              }
              if(property_exists($value2, 'numero')){
                    $nuevoReclamos->numero =  $value2->numero;
              }
              if(property_exists($value2, 'correo_electronico')){
                    $nuevoReclamos->correo_electronico =  $value2->correo_electronico;
              }
              if(property_exists($value2, 'mensaje')){
                    $nuevoReclamos->mensaje =  $value2->mensaje;
              }
            }
              return view('reclamos.reclamos_detalle')
             ->with('data' , $nuevoReclamos);
}


}
