<?php

namespace App\Http\Controllers\Reconocimiento;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use App\Models\Reconocimiento\Reconocimiento;
use App\Models\TramitesSimpleExterno\TramitesSimpleExterno;
use App\Mail\NotificacionTramiteIncompletoReconocimientoEmail;
use Mail;


class ReconocimientoController extends Controller
{
   public function index(Request $request){


$tramites = [];
$idTramiteFiltro = $request->nRegistro;
$queryFilter = "where r.numero_tramite = $idTramiteFiltro ";

     $query = "SELECT  *
         FROM
        (select
                tramite.id as numero_tramite ,
                tramite.proceso_id as numero_proeceso ,
                proceso.nombre as nombre_proceso ,
                tramite.created_at as fecha_inicio ,
                tramite.updated_at as fecha_ultima_modificacion ,
                tramite.pendiente as pendiente,
                tramite.ended_at as fecha_culmino,
                tarea.nombre as etapa,
                et.id as id_etapa,
                dts.conteo as cantidad_campos
                from tramite
                INNER JOIN proceso ON tramite.proceso_id = proceso.id
                INNER JOIN (SELECT * FROM etapa WHERE id IN (SELECT MAX(id) FROM etapa GROUP BY tramite_id) ORDER BY id DESC) et on et.tramite_id = tramite.id
                INNER JOIN tarea on et.tarea_id = tarea.id
                LEFT JOIN (SELECT  COUNT(id) as conteo, etapa_id FROM dato_seguimiento  group by etapa_id ) dts on dts.etapa_id = et.id
                where tramite.proceso_id = 1  and tarea.nombre = '1-Solicitud de Reconocimiento' and tramite.deleted_at is null
                and dts.conteo >4 
UNION ALL
select
                tramite.id as numero_tramite ,
                tramite.proceso_id as numero_proeceso ,
                proceso.nombre as nombre_proceso ,
                tramite.created_at as fecha_inicio ,
                tramite.updated_at as fecha_ultima_modificacion ,
                tramite.pendiente as pendiente,
                tramite.ended_at as fecha_culmino,
                tarea.nombre as etapa,
                et.id as id_etapa,
                dts.conteo as cantidad_campos
                from tramite
                INNER JOIN proceso ON tramite.proceso_id = proceso.id
                INNER JOIN (SELECT * FROM etapa WHERE id IN (SELECT MAX(id) FROM etapa GROUP BY tramite_id) ORDER BY id DESC) et on et.tramite_id = tramite.id
                INNER JOIN tarea on et.tarea_id = tarea.id
                LEFT JOIN (SELECT  COUNT(id) as conteo, etapa_id FROM dato_seguimiento  group by etapa_id ) dts on dts.etapa_id = et.id
                where tramite.proceso_id = 1
                and tramite.deleted_at is null
                and tarea.nombre <> '1-Solicitud de Reconocimiento') r ";

    ($idTramiteFiltro != null )? $query = $query.$queryFilter : $query = $query;
    $query = $query."order by r.numero_tramite DESC";

    $tramites = DB::connection('mysqlSimpleExterno')->select($query);
    return view('reconocimiento.reconocimiento')
    ->with('tramitesD', $tramites)
    ->with('idTramiteFiltro', $idTramiteFiltro);

    }

    public function show($id)
    {
        $client = new Client();
               try {
              $request = $client->get("https://tramites.mindep.cl/backend/api/tramites/$id?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu" , ['verify' => false]);
              $response = $request->getBody()->getContents();
              $data =  json_decode($response);
                }catch (BadResponseException   $e) {
                    $response = $e->getResponse();
                }
         $data = $data->tramite;
         $nuevoReconocimiento = new Reconocimiento();
         $nuevoReconocimiento->id =  $data->id;
         $nuevoReconocimiento->fecha_inicio =  $data->fecha_inicio;
         $nuevoReconocimiento->fecha_termino =  $data->fecha_termino;
         $nuevoReconocimiento->fecha_modificacion =  $data->fecha_modificacion;
         $nuevoReconocimiento->estado =  $data->estado;

         $datos=$data->datos;

         foreach ($datos as $key => $value2) {
             if(property_exists($value2, 'tipo_de_persona')){
                $nuevoReconocimiento->tipo_de_persona =  $value2->tipo_de_persona;
              }
             if(property_exists($value2, 'apellidos_solicitante')){
                $nuevoReconocimiento->apellidos_solicitante =  $value2->apellidos_solicitante;
              }
              if(property_exists($value2, 'email_solicitante')){
                 $nuevoReconocimiento->email_solicitante =  $value2->email_solicitante;
              }
              if(property_exists($value2, 'fecha_solicitud')){
                  $nuevoReconocimiento->fecha_solicitud =  $value2->fecha_solicitud;
              }
              if(property_exists($value2, 'nombres_solicitante')){
                   $nuevoReconocimiento->nombres_solicitante =  $value2->nombres_solicitante;
              }
              if(property_exists($value2, 'rut_solicitante')){
                    $nuevoReconocimiento->rut_solicitante =  $value2->rut_solicitante;
              }
              if(property_exists($value2, 'personalidad_juridica')){
                $nuevoReconocimiento->personalidad_juridica =  $value2->personalidad_juridica;
              }
              if(property_exists($value2, 'domicilio')){
                $nuevoReconocimiento->domicilio =  $value2->domicilio;
              }
              if(property_exists($value2, 'region')){
                $nuevoReconocimiento->region =  $value2->region->region;
              }
              if(property_exists($value2, 'region')){
                $nuevoReconocimiento->comuna =  $value2->region->comuna;
              }
              if(property_exists($value2, 'mision')){
                $nuevoReconocimiento->mision =  $value2->mision;
              }
              if(property_exists($value2, 'vision')){
                $nuevoReconocimiento->vision =  $value2->vision;
              }
              if(property_exists($value2, 'objetivos')){
                $nuevoReconocimiento->objetivos =  $value2->objetivos;
              }
              if(property_exists($value2, 'rut_del_representante_legal')){
                $nuevoReconocimiento->rut_del_representante_legal =  $value2->rut_del_representante_legal;
              }
              if(property_exists($value2, 'nombre_del_representante_legal')){
                $nuevoReconocimiento->nombre_del_representante_legal =  $value2->nombre_del_representante_legal;
              }
              if(property_exists($value2, 'carta')){
                $nuevoReconocimiento->carta =  $value2->carta->URL;
              }
              if(property_exists($value2, 'documentos')){
                $nuevoReconocimiento->documentos =  $value2->documentos->URL;
              }
              if(property_exists($value2, 'condiciones')){
                $nuevoReconocimiento->condiciones =  $value2->condiciones;
              }
          } 


       return view('reconocimiento.reconocimiento_detalle')
      ->with('data' , $nuevoReconocimiento);
    }


    //ELIMINAR TRÁMITE
    public function destroy($id)
    {

      $tramite = TramitesSimpleExterno::where('id', $id)->first();
      $result = $tramite->delete();
          if ($result) {
              return response()->json(['success'=>'true']);
          }else{
              return response()->json(['success'=> 'false']);
          }
    }

    //NORIFICACIÓN AL USUARIO
    public function notificacion($tramiteid)
    {
    

      $tramite = $tramiteid;

      $client = new Client();
               try {
              $request = $client->get("https://tramites.mindep.cl/backend/api/tramites/$tramite?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu" , ['verify' => false]);
              $response = $request->getBody()->getContents();
              $data =  json_decode($response);
                }catch (BadResponseException   $e) {
                    $response = $e->getResponse();
                }
         $data = $data->tramite;
         $nuevoReconocimiento = new Reconocimiento();
         $nuevoReconocimiento->id =  $data->id;
         $nuevoReconocimiento->fecha_inicio =  $data->fecha_inicio;
         $nuevoReconocimiento->fecha_termino =  $data->fecha_termino;
         $nuevoReconocimiento->fecha_modificacion =  $data->fecha_modificacion;
         $nuevoReconocimiento->estado =  $data->estado;

         $datos=$data->datos;

         foreach ($datos as $key => $value2) {
             if(property_exists($value2, 'apellidos_solicitante')){
                $nuevoReconocimiento->apellidos_solicitante =  $value2->apellidos_solicitante;
              }
              if(property_exists($value2, 'email_solicitante')){
                 $nuevoReconocimiento->email_solicitante =  $value2->email_solicitante;
              }
              if(property_exists($value2, 'fecha_solicitud')){
                  $nuevoReconocimiento->fecha_solicitud =  $value2->fecha_solicitud;
              }
              if(property_exists($value2, 'nombres_solicitante')){
                   $nuevoReconocimiento->nombres_solicitante =  $value2->nombres_solicitante;
              }
          } 

      $correo = $nuevoReconocimiento->email_solicitante;

      Mail::to($correo)->send(new NotificacionTramiteIncompletoReconocimientoEmail($nuevoReconocimiento));
      
    /*  $nuevoCorreo = new CorreoNotificaciones();
      $nuevoCorreo->tipo_notificaciones = 'Reconocimiento trámite incompleto';
      $nuevoCorreo->destintario = $correo;
      $nuevoCorreo->save(); */
    
      alert()->success('!','Notificación enviada exitosamente');
      return redirect()->back();
    }

}
