<?php

namespace App\Http\Controllers\RequerimientosCompras;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Client;
use App\Models\RequerimientoCompras\RequerimientoCompras;
use App\Models\RequerimientoCompras\ActaRecepcionConforme;
use App\Models\TramitesSimple\TramitesSimple;
use App\Models\File\File;
use App\Models\Simple\HasGrupo;
use App\Models\Simple\Grupos;
use App\Models\UsuariosSimple\UsuariosSimple;
use App\Models\Divisiones\Divisiones;
use App\Models\Simple\TmpTramiteCompras;
use App\Models\Simple\Etapa;
use App\Models\Simple\DatoSeguimiento;



class RequerimientosComprasController extends Controller
{

    public function index(Request $request)
    {
        $vistaId = null;
        $asignadoA = null;
        $tituloC = '';
        $controladorId2 = $controladorId = $request->id;
        if(strpos($controladorId, ',')){
          $controladorId = explode(',' , $controladorId);
        }
        $divisionControlador  =  $request->division;
        $asignadoControlador  =  $request->asignado;
        $estadoControlador  =  $request->estado;
        $tituloControlador =  $request->titulo;
        $dataProcesada = [];
        $dataFinal = [];
        $dataFiltrada = [];
        $selectD = Divisiones::get()->pluck('nombre', 'nombre')->prepend('Todos', null);
        $selectEstado = [''=>'Seleccione Estado','En Trámite'=>'En Trámite','Completado'=>'Completado','Rechazado'=>'Rechazado','Cancelado'=>'Cancelado'];
        $divisionView = '';
        $estadoView = '';

         $client = new Client();

              try {
              $request = $client->get("https://simple.mindep.cl/backend/api/procesos/5/tramites?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu&maxResults=20" , ['verify' => false]);
              $response = $request->getBody()->getContents();
              $data =  json_decode($response);
                }catch (BadResponseException   $e) {
                    $response = $e->getResponse();
                }

              $data1 = $data;
              while ($data1->tramites->nextPageToken != null) {
                try {
                      $client1 = new Client();
                      $request1 = $client1->get("https://simple.mindep.cl/backend/api/procesos/5/tramites?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu&maxResults=20&pageToken=".$data1->tramites->nextPageToken , ['verify' => false]);
                      $response1 = $request1->getBody()->getContents();
                      $data1 =  json_decode($response1);
                        }catch (BadResponseException   $er) {
                            $response1 = $er->getResponse();
                        }

                       foreach ($data1->tramites->items as $key3 => $value3) {
                         $data->tramites->items[count($data->tramites->items)+($key3)] = $value3;
                       }

              }

        $dataProcesada = $data->tramites->items;

        foreach ($dataProcesada as $key => $value) {
            $nuevoRegistroCompras = new RequerimientoCompras();
            $nuevoRegistroCompras->id= $value->id;
            $nuevoRegistroCompras->estado= $value->estado;
            $nuevoRegistroCompras->fecha_inicio= $value->fecha_inicio;
            $nuevoRegistroCompras->fecha_termino= $value->fecha_termino;
            $fechaInicio = new Carbon($nuevoRegistroCompras->fecha_inicio);
            if ($value->estado == 'pendiente' && $value->fecha_termino == null) {
               $fechaTermino = Carbon::now();
            }else{
              $fechaTermino = new Carbon($nuevoRegistroCompras->fecha_termino);
            }
            
            //$diferenciaFechas = $fechaInicio->diffInHours($fechaTermino) . ':' . $fechaInicio->diff($fechaTermino)->format('%I:%S');
            $diferenciaFechas = $fechaInicio->diffInDays($fechaTermino);
            //dd($diferenciaFechas);
            $nuevoRegistroCompras->tiempo_transcurrido = $diferenciaFechas;



            $array_datos = $value->datos;

            if($array_datos){

                foreach ($array_datos as $key2 => $value2) {
                    if(property_exists($value2, 'fecha_requerimiento')){
                        $nuevoRegistroCompras->fecha_requerimiento = $value2->fecha_requerimiento;
                    }
                    if(property_exists($value2, 'con_cargo_a')){
                        $nuevoRegistroCompras->con_cargo_a = $value2->con_cargo_a;
                    }
                    if(property_exists($value2, 'titulo_compra')){
                      $nuevoRegistroCompras->titulo_compra = $value2->titulo_compra;
                  }
                    if(property_exists($value2, 'unidad_requirente')){
                        $nuevoRegistroCompras->unidad_requirente = $value2->unidad_requirente;
                    }
                    if(property_exists($value2, 'pasaje_aereo')){
                        $nuevoRegistroCompras->pasaje_aereo = $value2->pasaje_aereo;
                    }
                    if(property_exists($value2, 'monto')){
                        $nuevoRegistroCompras->monto = $value2->monto;
                    }
                    if(property_exists($value2, 'listado_requerimiento')){

                        $nuevoRegistroCompras->listado_requerimiento = $value2->listado_requerimiento;
                    }
                }
              }
              if(property_exists($value, 'etapas')){
                $array_etapas = $value->etapas;
                    if(count($array_etapas)>0){
                      $ultimaEtapa = $array_etapas[0];

                      if(property_exists($ultimaEtapa, 'usuario_asignado')){
                        $nuevoRegistroCompras->usuario_asignado_etapa = $ultimaEtapa->usuario_asignado->email;
                      }

                      if(property_exists($ultimaEtapa, 'tarea')){
                        $objetoEtapa = $ultimaEtapa->tarea;
                        $nuevoRegistroCompras->etapa_actual = $objetoEtapa->nombre;
                      }

                      if($value->estado == 'completado'){
                        if($nuevoRegistroCompras->etapa_actual == '1.5-MINDEP - Revisión SEREMI' || 
                        $nuevoRegistroCompras->etapa_actual == '2 - MINDEP - Formulario de Revisión de Compra - Jefe Finanzas (Revisión 1)' ||
                        $nuevoRegistroCompras->etapa_actual == '1.5-MINDEP - Revisión Jefe División o Encargado de Unidad' ||
                        $nuevoRegistroCompras->etapa_actual == '4 - MINDEP - Formulario de Revisión de Compra - Jefe Finanzas (Revisión 2)' ||
                        $nuevoRegistroCompras->etapa_actual == '6 - MINDEP - Formulario de Revisión de Compra - Jefe DAF' ||
                        $nuevoRegistroCompras->etapa_actual == '6.5 - MINDEP - Revisión de Compra - Gabinete Subsecretario' ||
                        $nuevoRegistroCompras->etapa_actual == '3-MINDEP - Formulario de Revisión de Compra - Jefe Finanzas (Revisión 1)' ||
                        $nuevoRegistroCompras->etapa_actual == '2-MINDEP - Revisión de Compra - Jefe de Servicio'){
                          $nuevoRegistroCompras->estatusVista = 'Rechazado';
                        }
                        if($nuevoRegistroCompras->etapa_actual =='7 - MINDEP - Formulario de Revisión de Compra - Compras' ){
                          $nuevoRegistroCompras->estatusVista = 'Cancelado';
                        }
                        if($nuevoRegistroCompras->etapa_actual == '8 - MINDEP - Formulario Adjuntar Orden de Compra' || 
                          $nuevoRegistroCompras->etapa_actual == '11 - MINDEP - Firma Acta de Recepción Conforme Mensual' ||
                          $nuevoRegistroCompras->etapa_actual == '11 - MINDEP - Firma Acta de Recepción Conforme' ||
                          $nuevoRegistroCompras->etapa_actual == '9 - MINDEP - Ingreso Datos de Facturas Mensual' ||
                          $nuevoRegistroCompras->etapa_actual == '11-MINDEP - Datos de Factura' ||
                          $nuevoRegistroCompras->etapa_actual == '10 - MINDEP - Formulario Acta de Recepción Conforme Mensual - Usuario' ||
                          $nuevoRegistroCompras->etapa_actual == '10-MINDEP - Formulario Adjuntar Orden de Compra' ||
                          $nuevoRegistroCompras->etapa_actual == '9 - MINDEP - Ingreso Datos de Factura'){
                          $nuevoRegistroCompras->estatusVista = 'Completado';
                          }          
                      }elseif ($value->estado == 'pendiente'){
                        $nuevoRegistroCompras->estatusVista = 'En Trámite';
                      }

                      if(property_exists($ultimaEtapa, 'fecha_modificacion')){
                       $nuevoRegistroCompras->fecha_modificacion = $ultimaEtapa->fecha_modificacion;
                      }

                    }
              }
        $dataFinal[$key] = $nuevoRegistroCompras;
        }

    if ($controladorId != null || $divisionControlador != null || $asignadoControlador != null || $estadoControlador != null || $tituloControlador != null) {
//dd('entra aqui',$controladorId, $divisionControlador, $asignadoControlador, $estadoControlador);

foreach ($dataFinal as $keyN => $registroN) {
          if ($controladorId != null) {
            if (is_array($controladorId)){
                foreach ($controladorId as $llaveids  => $valoresIds) {
                  if ($registroN->id == $valoresIds) {
                    $dataFiltrada[$keyN] = $registroN;
                  }
                }
              }else{
              if ($registroN->id == $controladorId) {
                $dataFiltrada[$keyN] = $registroN;
              }
            } 
          }
            if ($divisionControlador != null) {
                if ($registroN->unidad_requirente == $divisionControlador) {
                  $dataFiltrada[$keyN] = $registroN;
                }
            }
            if ($asignadoControlador != null) {
              if ($registroN->usuario_asignado_etapa == $asignadoControlador) {
                $dataFiltrada[$keyN] = $registroN;
              }
            }
            if ($estadoControlador != null) {
              if ($registroN->estatusVista == $estadoControlador) {
                $dataFiltrada[$keyN] = $registroN;
              }
            }
            if ($tituloControlador != null) {
              if (str_contains($registroN->titulo_compra ,  $tituloControlador)) {
                $dataFiltrada2[$keyN] = $registroN;
              }
            }
        }

     $dataFinal = $dataFiltrada;
  
    }
   
//$dataFinal = collect($dataFinal)->where('unidad_requirente', $divisionControlador);


//PROCESO NUEVO DE COMPRA
$vistaId2 = null;
//$controladorId2 = $request->id;
$dataProcesada2 = [];
$dataFinal2 = [];
$dataFiltrada2 = [];
$contadorTramites = 0;
$auxDatosTodos = [];

 $client2 = new Client();

      try {
      $request2 = $client2->get("https://simple.mindep.cl/backend/api/procesos/16/tramites?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu&maxResults=20" , ['verify' => false]);
      $response2 = $request2->getBody()->getContents();
      $data2 =  json_decode($response2);
    }catch (BadResponseException   $e2) {
            $response2 = $e2->getResponse();
        }
        $contadorTramites = count($data2->tramites->items)+$contadorTramites;
        foreach ($data2->tramites->items as $keyyyy => $valueeee) {
          $auxDatosTodos[] = $valueeee;
        }
      $data12 = $data2;
      while ($data12->tramites->nextPageToken != null) {
        try {
              $client12 = new Client();
              $request12 = $client12->get("https://simple.mindep.cl/backend/api/procesos/16/tramites?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu&maxResults=20&pageToken=".$data12->tramites->nextPageToken , ['verify' => false]);
              $response12 = $request12->getBody()->getContents();
              $data12 =  json_decode($response12);
            }catch (BadResponseException   $er2) {
                    $response12 = $er2->getResponse();
                }
              
                $contadorTramites = count($data12->tramites->items)+$contadorTramites;
                
                

               foreach ($data12->tramites->items as $key32 => $value32) {
                  $auxDatosTodos[] = $value32;
                 $data2->tramites->items[count($data2->tramites->items)+($key32)] = $value32;
               }

      }
     //dd(count($auxDatosTodos));

//$dataProcesada2 = $data2->tramites->items;
$dataProcesada2 = $auxDatosTodos;
foreach ($dataProcesada2 as $key2 => $value2) {
    $nuevoRegistroCompras2 = new RequerimientoCompras();
    $nuevoRegistroCompras2->id= $value2->id;
    $nuevoRegistroCompras2->estado= $value2->estado;
    $nuevoRegistroCompras2->fecha_inicio= $value2->fecha_inicio;
    $nuevoRegistroCompras2->fecha_termino= $value2->fecha_termino;
    $fechaInicio2 = new Carbon($nuevoRegistroCompras2->fecha_inicio);
    if ($value2->estado == 'pendiente' && $value2->fecha_termino == null) {
       $fechaTermino2 = Carbon::now();
    }else{
      $fechaTermino2 = new Carbon($nuevoRegistroCompras2->fecha_termino);
    }
    //$diferenciaFechas = $fechaInicio->diffInHours($fechaTermino) . ':' . $fechaInicio->diff($fechaTermino)->format('%I:%S');
    $diferenciaFechas2 = $fechaInicio2->diffInDays($fechaTermino2);
    //dd($diferenciaFechas);
    $nuevoRegistroCompras2->tiempo_transcurrido = $diferenciaFechas2;



    $array_datos2 = $value2->datos;

    if($array_datos2){

        foreach ($array_datos2 as $key22 => $value22) {
            if(property_exists($value22, 'fecha_requerimiento')){
                $nuevoRegistroCompras2->fecha_requerimiento = $value22->fecha_requerimiento;
            }
            if(property_exists($value22, 'con_cargo_a')){
                $nuevoRegistroCompras2->con_cargo_a = $value22->con_cargo_a;
            }
            if(property_exists($value22, 'titulo_compra')){
              $nuevoRegistroCompras2->titulo_compra = $value22->titulo_compra;
          }
            if(property_exists($value22, 'unidad_requirente')){
                $nuevoRegistroCompras2->unidad_requirente = $value22->unidad_requirente;
            }
            if(property_exists($value22, 'pasaje_aereo')){
                $nuevoRegistroCompras2->pasaje_aereo = $value22->pasaje_aereo;
            }
            if(property_exists($value22, 'monto')){
                $nuevoRegistroCompras2->monto = $value22->monto;
            }
            if(property_exists($value22, 'listado_requerimiento')){

                $nuevoRegistroCompras2->listado_requerimiento = $value22->listado_requerimiento;
            }
        }
      }
      if(property_exists($value2, 'etapas')){
        $array_etapas2 = $value2->etapas;
            if(count($array_etapas2)>0){
              $ultimaEtapa2 = $array_etapas2[0];

              if(property_exists($ultimaEtapa2, 'usuario_asignado')){
                $nuevoRegistroCompras2->usuario_asignado_etapa = $ultimaEtapa2->usuario_asignado->email;
              }

              if(property_exists($ultimaEtapa2, 'tarea')){
                $objetoEtapa2 = $ultimaEtapa2->tarea;
                $nuevoRegistroCompras2->etapa_actual = $objetoEtapa2->nombre;
              }
              if($value2->estado == 'completado'){
                if($nuevoRegistroCompras2->etapa_actual == '1.5-MINDEP - Revisión SEREMI' || 
                $nuevoRegistroCompras2->etapa_actual == '2 - MINDEP - Formulario de Revisión de Compra - Jefe Finanzas (Revisión 1)' ||
                $nuevoRegistroCompras2->etapa_actual == '1.5-MINDEP - Revisión Jefe División o Encargado de Unidad' ||
                $nuevoRegistroCompras2->etapa_actual == '4 - MINDEP - Formulario de Revisión de Compra - Jefe Finanzas (Revisión 2)' ||
                $nuevoRegistroCompras2->etapa_actual == '6 - MINDEP - Formulario de Revisión de Compra - Jefe DAF' ||
                $nuevoRegistroCompras2->etapa_actual == '6.5 - MINDEP - Revisión de Compra - Gabinete Subsecretario' ||
                $nuevoRegistroCompras2->etapa_actual == '3-MINDEP - Formulario de Revisión de Compra - Jefe Finanzas (Revisión 1)' ||
                $nuevoRegistroCompras2->etapa_actual == '2-MINDEP - Revisión de Compra - Jefe de Servicio'){
                $nuevoRegistroCompras2->estatusVista = 'Rechazado';
                }
                if($nuevoRegistroCompras2->etapa_actual =='7 - MINDEP - Formulario de Revisión de Compra - Compras' ){
                  $nuevoRegistroCompras2->estatusVista = 'Cancelado';
                }
                if($nuevoRegistroCompras2->etapa_actual == '8 - MINDEP - Formulario Adjuntar Orden de Compra' || 
                  $nuevoRegistroCompras2->etapa_actual == '11 - MINDEP - Firma Acta de Recepción Conforme Mensual' ||
                  $nuevoRegistroCompras2->etapa_actual == '11 - MINDEP - Firma Acta de Recepción Conforme' ||
                  $nuevoRegistroCompras2->etapa_actual == '9 - MINDEP - Ingreso Datos de Facturas Mensual' ||
                  $nuevoRegistroCompras2->etapa_actual == '11-MINDEP - Datos de Factura' ||
                  $nuevoRegistroCompras2->etapa_actual == '10 - MINDEP - Formulario Acta de Recepción Conforme Mensual - Usuario' ||
                  $nuevoRegistroCompras2->etapa_actual == '10-MINDEP - Formulario Adjuntar Orden de Compra' ||
                  $nuevoRegistroCompras2->etapa_actual == '9 - MINDEP - Ingreso Datos de Factura'){
                  $nuevoRegistroCompras2->estatusVista = 'Completado';
                  }          
              }elseif ($value2->estado == 'pendiente'){
                $nuevoRegistroCompras2->estatusVista = 'En Trámite';
              }
              if(property_exists($ultimaEtapa2, 'fecha_modificacion')){
               $nuevoRegistroCompras2->fecha_modificacion = $ultimaEtapa2->fecha_modificacion;
              }

            }
      }
$dataFinal2[$key2] = $nuevoRegistroCompras2;
}


if ($controladorId != null || $divisionControlador != null || $asignadoControlador != null || $estadoControlador != null || $tituloControlador != null) {
    foreach ($dataFinal2 as $keyN2 => $registroN2) {
      if ($controladorId != null) {
          if (is_array($controladorId)){
              foreach ($controladorId as $llaveids  => $valoresIds) {
                if ($registroN2->id ==  $valoresIds) {
                  $dataFiltrada2[$keyN2] = $registroN2;
                }
              }
            }else{
            if ($registroN2->id == $controladorId) {
              $dataFiltrada2[$keyN2] = $registroN2;
            }
          } 
        }
        if ($divisionControlador != null) {
            if ($registroN2->unidad_requirente == $divisionControlador) {
              $dataFiltrada2[$keyN2] = $registroN2;
            }
        }
        if ($asignadoControlador != null) {
          if ($registroN2->usuario_asignado_etapa == $asignadoControlador) {
            $dataFiltrada2[$keyN2] = $registroN2;
          }
        }
        if ($estadoControlador != null) {
          if ($registroN2->estatusVista == $estadoControlador) {
            $dataFiltrada2[$keyN2] = $registroN2;
          }
        }
        if ($tituloControlador != null) {
          if (str_contains($registroN2->titulo_compra ,  $tituloControlador)) {
            $dataFiltrada2[$keyN2] = $registroN2;
          }
        }
    }
  $dataFinal2 = $dataFiltrada2;
}
/*$dataFinal2 = collect($dataFinal2)->where('unidad_requirente', $divisionControlador);
$dataFinal2 = collect($dataFinal2)->where('usuario_asignado_etapa', $asignadoControlador);
$dataFinal2 = collect($dataFinal2)->where('estado', $estadoControlador); */

$dataFinal = collect($dataFinal);
$dataFinal2 = collect($dataFinal2);

//$dataFinal = array_merge($dataFinal2, $dataFinal);
//dd($dataFinal);
    $dataFinal = $dataFinal2->merge($dataFinal);
 
    return view('requerimientos_compras.requerimientos_compras')
      ->with('dataFinal' ,$dataFinal)
      ->with('vistaId' ,$vistaId)
      ->with('selectD' ,$selectD)
      ->with('selectEstado' ,$selectEstado)
      ->with('divisionView' ,$divisionView)
      ->with('estadoView' ,$estadoView)
      ->with('asignadoA' ,$asignadoA)
      ->with('tituloC' , $tituloC)
      //->with('dataFinal2' ,$dataFinal2)
      ->with('vistaId2' ,$vistaId2);

    }

    public function show($id)
    {
        $client = new Client();

               try {
              $request = $client->get("https://simple.mindep.cl/backend/api/tramites/$id?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu" , ['verify' => false]);
              $response = $request->getBody()->getContents();
              $data =  json_decode($response);
                }catch (BadResponseException   $e) {
                    $response = $e->getResponse();
                }
         $data = $data->tramite;
         $nuevoRegistroCompras = new RequerimientoCompras();
         $nuevoRegistroCompras->id =  $data->id;
         $nuevoRegistroCompras->fecha_inicio =  $data->fecha_inicio;
         $nuevoRegistroCompras->fecha_termino =  $data->fecha_termino;
         $nuevoRegistroCompras->fecha_modificacion =  $data->fecha_modificacion;
         $nuevoRegistroCompras->estado =  $data->estado;
         $nuevoRegistroCompras->etapas =  $data->etapas;




         $datos=$data->datos;
         if($datos){
         foreach ($datos as $key => $value2) {

           if(property_exists($value2, 'certificado_documento')){
            $nuevoRegistroCompras->certificado_documento_descarga = 'https://simple.mindep.cl/uploads/documentos/'.$value2->certificado_documento;
            }
            if(property_exists($value2, 'act_rc')){
              $documento = File::where('tramite_id', $data->id)->where('filename', $value2->act_rc)->first();
              if($documento->validez == NULL){
                  $url_certificado2 = 'https://simple.mindep.cl/documentos/get/0/'.$documento->filename.'/2?id='.$documento->id.'&token='.$documento->llave;
                  $nuevoRegistroCompras->act_rc_descarga = $url_certificado2;
              }else{
                  $url_certificado2 = 'https://simple.mindep.cl/documentos/get/'.$documento->validez.'/'.$documento->filename.'/2?id='.$documento->id.'&token='.$documento->llave;
                  $nuevoRegistroCompras->act_rc_descarga = $url_certificado2;
              }
             }
             if(property_exists($value2, 'adjunta_antecedentes')){
                $nuevoRegistroCompras->adjunta_antecedentes =  $value2->adjunta_antecedentes;
              }
              if(property_exists($value2, 'apellidos_solicitante')){
                $nuevoRegistroCompras->apellidos_solicitante =  $value2->apellidos_solicitante;
              }
               if(property_exists($value2, 'archivos_digital_adicional_2')){
                $nuevoRegistroCompras->archivos_digital_adicional_2 =  $value2->archivos_digital_adicional_2->URL;
              }
               if(property_exists($value2, 'archivos_digitales_1')){
                $nuevoRegistroCompras->archivos_digitales_1 =  $value2->archivos_digitales_1->URL;
              }
               if(property_exists($value2, 'autorizacion_jefe_de_servicio')){
                $nuevoRegistroCompras->autorizacion_jefe_de_servicio =  $value2->autorizacion_jefe_de_servicio;
              }
               if(property_exists($value2, 'bases_tecnicas')){
                $nuevoRegistroCompras->bases_tecnicas =  $value2->bases_tecnicas->URL;
              }
               if(property_exists($value2, 'compra_por')){
                $nuevoRegistroCompras->compra_por =  $value2->compra_por;
              }
               if(property_exists($value2, 'con_cargo_a')){
                $nuevoRegistroCompras->con_cargo_a =  $value2->con_cargo_a;
              }
               if(property_exists($value2, 'correo_solicitante')){
                $nuevoRegistroCompras->correo_solicitante =  $value2->correo_solicitante;
              }
               if(property_exists($value2, 'cotizaciones')){
                $nuevoRegistroCompras->cotizaciones =  $value2->cotizaciones->URL;
              }
               if(property_exists($value2, 'fecha_de_revision')){
                $nuevoRegistroCompras->fecha_de_revision =  $value2->fecha_de_revision;
              }
               if(property_exists($value2, 'fecha_requerimiento')){
                $nuevoRegistroCompras->fecha_requerimiento =  $value2->fecha_requerimiento;
              }
                 if(property_exists($value2, 'fecha_vuelo')){
                $nuevoRegistroCompras->fecha_vuelo =  $value2->fecha_vuelo;
              }
                 if(property_exists($value2, 'fecha2')){
                $nuevoRegistroCompras->fecha2 =  $value2->fecha2;
              }
                 if(property_exists($value2, 'item_plan_de_compras')){
                $nuevoRegistroCompras->item_plan_de_compras =  $value2->item_plan_de_compras;
              }
                 if(property_exists($value2, 'justificacion_td')){
                $nuevoRegistroCompras->justificacion_td =  $value2->justificacion_td->URL;
              }
                 if(property_exists($value2, 'monto')){
                $nuevoRegistroCompras->monto =  $value2->monto;
              }

                   if(property_exists($value2, 'nombres_solicitante')){
                $nuevoRegistroCompras->nombres_solicitante =  $value2->nombres_solicitante;
              }

                  if(property_exists($value2, 'objeto')){
                $nuevoRegistroCompras->objeto =  $value2->objeto;
              }

                 if(property_exists($value2, 'observaciones')){
                $nuevoRegistroCompras->observaciones =  $value2->observaciones;
              }

                if(property_exists($value2, 'observaciones_jefe_de_servicio')){
                $nuevoRegistroCompras->observaciones_jefe_de_servicio =  $value2->observaciones_jefe_de_servicio;
              }

              if(property_exists($value2, 'revision_jf')){
                $nuevoRegistroCompras->revision_jf =  $value2->revision_jf;
              }

              if(property_exists($value2, 'observaciones_jf')){
                $nuevoRegistroCompras->observaciones_jf =  $value2->observaciones_jf;
              }

              if(property_exists($value2, 'accion_a_tomar')){
                $nuevoRegistroCompras->accion_a_tomar =  $value2->accion_a_tomar;
              }

              if(property_exists($value2, 'observaciones_cont_presu')){
                $nuevoRegistroCompras->observaciones_cont_presu =  $value2->observaciones_cont_presu;
              }

              if(property_exists($value2, 'aprueba_solicitud_certificado')){
                $nuevoRegistroCompras->aprueba_solicitud_certificado =  $value2->aprueba_solicitud_certificado;
              }

              if(property_exists($value2, 'observaciones_jf2')){
                $nuevoRegistroCompras->observaciones_jf2 =  $value2->observaciones_jf2;
              }

              if(property_exists($value2, 'certificado_documento')){
                $nuevoRegistroCompras->certificado_documento =  $value2->certificado_documento;
              }

              if(property_exists($value2, 'daf')){
                $nuevoRegistroCompras->daf =  $value2->daf;
              }

              if(property_exists($value2, 'observacion_jefedaf')){
                $nuevoRegistroCompras->observacion_jefedaf =  $value2->observacion_jefedaf;
              }

              if(property_exists($value2, 'cumple_los_requisitos')){
                $nuevoRegistroCompras->cumple_los_requisitos =  $value2->cumple_los_requisitos;
              }

              if(property_exists($value2, 'tipo_de_compra')){
                $nuevoRegistroCompras->tipo_de_compra =  $value2->tipo_de_compra;
              }

              if(property_exists($value2, 'observacion_compras')){
                $nuevoRegistroCompras->observacion_compras =  $value2->observacion_compras;
              }

                 if(property_exists($value2, 'pasaje_aereo')){
                $nuevoRegistroCompras->pasaje_aereo =  $value2->pasaje_aereo;
              }

                 if(property_exists($value2, 'plan_de_compras')){
                $nuevoRegistroCompras->plan_de_compras =  $value2->plan_de_compras;
              }

               if(property_exists($value2, 'planos')){
                $nuevoRegistroCompras->planos =  $value2->planos->URL;
              }

               if(property_exists($value2, 'solicitante')){
                $nuevoRegistroCompras->solicitante =  $value2->solicitante;
              }

               if(property_exists($value2, 'unidad_requirente')){
                $nuevoRegistroCompras->unidad_requirente =  $value2->unidad_requirente;
              }

              if(property_exists($value2, 'listado_requerimiento')){
                $nuevoRegistroCompras->listado_requerimiento =  $value2->listado_requerimiento;
              }

              if(property_exists($value2, 'numero_de_orcom')){
                $nuevoRegistroCompras->numero_de_orcom =  $value2->numero_de_orcom;
              }

               if(property_exists($value2, 'orden_de_compra')){
                $nuevoRegistroCompras->orden_de_compra =  $value2->orden_de_compra->URL;
              }

              if(property_exists($value2, 'bases_de_doc_li')){
                $nuevoRegistroCompras->bases_de_doc_li =  $value2->bases_de_doc_li->URL;
              }

               if(property_exists($value2, 'bases_de_doc_tra')){
                $nuevoRegistroCompras->bases_de_doc_tra =  $value2->bases_de_doc_tra->URL;
              }
              if(property_exists($value2, 'fecha_orden_compra')){
                $nuevoRegistroCompras->fecha_orden_compra =  $value2->fecha_orden_compra;
              }
              if(property_exists($value2, 'fecha_factura')){
                $nuevoRegistroCompras->fecha_factura =  $value2->fecha_factura;
              }
              if(property_exists($value2, 'n_factura')){
                $nuevoRegistroCompras->n_factura =  $value2->n_factura;
              }

               if(property_exists($value2, 'adjuntar_factura')){
                $nuevoRegistroCompras->adjuntar_factura =  $value2->adjuntar_factura->URL;
              }

              if(property_exists($value2, 'archivos_adicionales')){
                $nuevoRegistroCompras->archivos_adicionales =  $value2->archivos_adicionales->URL;
              }
              if(property_exists($value2, 'seremi_rev')){
                $nuevoRegistroCompras->seremi_rev =  $value2->seremi_rev;
              }
              if(property_exists($value2, 'observaciones_seremi')){
                $nuevoRegistroCompras->observaciones_seremi =  $value2->observaciones_seremi;
              }
              if(property_exists($value2, 'jefatura_rev')){
                $nuevoRegistroCompras->jefatura_rev =  $value2->jefatura_rev;
              }
              if(property_exists($value2, 'observaciones_jefatura')){
                $nuevoRegistroCompras->observaciones_jefatura =  $value2->observaciones_jefatura;
              }
              if(property_exists($value2, 'origen_requerimiento')){
                $nuevoRegistroCompras->origen_requerimiento =  $value2->origen_requerimiento;
              }
              if(property_exists($value2, 'archivo_a')){
                $nuevoRegistroCompras->archivo_a =  $value2->archivo_a->URL;
              }
              if(property_exists($value2, 'archivo_b')){
                $nuevoRegistroCompras->archivo_b =  $value2->archivo_b->URL;
              }
              if(property_exists($value2, 'archivo_c')){
                $nuevoRegistroCompras->archivo_c =  $value2->archivo_c->URL;
              }
              if(property_exists($value2, 'archivo_d')){
                $nuevoRegistroCompras->archivo_d =  $value2->archivo_d->URL;
              }
              if(property_exists($value2, 'archivo_e')){
                $nuevoRegistroCompras->archivo_e =  $value2->archivo_e->URL;
              }
              if(property_exists($value2, 'descargar')){
                $nuevoRegistroCompras->descargar = 'https://simple.mindep.cl/uploads/documentos/'.$value2->descargar;
               }  

              

          }
         }

//dd($nuevoRegistroCompras);
       return view('requerimientos_compras.requerimientos_compras_detalle')
      ->with('data' ,$nuevoRegistroCompras);
    }

    public function dashboard()
    {
          $cantidadRechazados = 0;
          $cantidadAprobados = 0;
          $cantidadenTramite = 0;
      
        
          $cantidadenTramite = TmpTramiteCompras::where('estatus_requerimiento', 'Pendiente')->get()->count();
          $cantidadRechazados = TmpTramiteCompras::where('estatus_requerimiento', 'Rechazado')->get()->count();
          $cantidadAprobados = TmpTramiteCompras::where('estatus_requerimiento', 'Aprobado')->get()->count();

          $divisiones = Divisiones::get();

          foreach ($divisiones as $llave => $valor) {
            
            $cantidadTramitesPendientes = TmpTramiteCompras::where('unidad_requirente', 'LIKE' , "%$valor->nombre%")->where('estatus_requerimiento', 'Pendiente')->get();
            $valor->cantidadTramitesPendientes =  $cantidadTramitesPendientes->count();
            $valor->idsTramitesPendientes = implode(',' ,$cantidadTramitesPendientes->pluck('tramite_id')->toArray());

            $cantidadTramitesAprobado = TmpTramiteCompras::where('unidad_requirente', 'LIKE' , "%$valor->nombre%")->where('estatus_requerimiento', 'Aprobado')->get();
            $valor->cantidadTramitesAprobado =  $cantidadTramitesAprobado->count();
            $valor->idsTramitesAprobado = implode(',' ,$cantidadTramitesAprobado->pluck('tramite_id')->toArray());

            $cantidadTramitesRechazados = TmpTramiteCompras::where('unidad_requirente', 'LIKE' , "%$valor->nombre%")->where('estatus_requerimiento', 'Rechazado')->get();
            $valor->cantidadTramitesRechazados =  $cantidadTramitesRechazados->count();
            $valor->idsTramitesRechazados = implode(',' ,$cantidadTramitesRechazados->pluck('tramite_id')->toArray());

          }

      return view('requerimientos_compras.dashboard_requerimientos_compras')
      ->with('divisiones', $divisiones)
      ->with('cantidadenTramite', $cantidadenTramite)
      ->with('cantidadRechazados', $cantidadRechazados)
      ->with('cantidadAprobados', $cantidadAprobados);
      ;

    }

    public function reporteCompras(Request $request, $tipoReporte) {

      $vistaId = null;
      $controladorId2 = $controladorId = $request->id;
      $dataProcesada = [];
      $dataFinal = [];
      $dataFiltrada = [];

       $client = new Client();

            try {
            $request = $client->get("https://simple.mindep.cl/backend/api/procesos/5/tramites?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu&maxResults=20" , ['verify' => false]);
            $response = $request->getBody()->getContents();
            $data =  json_decode($response);
              }catch (BadResponseException   $e) {
                  $response = $e->getResponse();
              }

            $data1 = $data;
            while ($data1->tramites->nextPageToken != null) {
              try {
                    $client1 = new Client();
                    $request1 = $client1->get("https://simple.mindep.cl/backend/api/procesos/5/tramites?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu&maxResults=20&pageToken=".$data1->tramites->nextPageToken , ['verify' => false]);
                    $response1 = $request1->getBody()->getContents();
                    $data1 =  json_decode($response1);
                      }catch (BadResponseException   $er) {
                          $response1 = $er->getResponse();
                      }

                     foreach ($data1->tramites->items as $key3 => $value3) {
                       $data->tramites->items[count($data->tramites->items)+($key3)] = $value3;
                     }

            }

      $dataProcesada = $data->tramites->items;

      foreach ($dataProcesada as $key => $value) {
          $nuevoRegistroCompras = new RequerimientoCompras();
          $nuevoRegistroCompras->id= $value->id;
          $nuevoRegistroCompras->estado= $value->estado;
          $nuevoRegistroCompras->fecha_inicio= $value->fecha_inicio;
          $nuevoRegistroCompras->fecha_termino= $value->fecha_termino;
          $fechaInicio = new Carbon($nuevoRegistroCompras->fecha_inicio);
          if ($value->estado == 'pendiente' && $value->fecha_termino == null) {
             $fechaTermino = Carbon::now();
          }else{
            $fechaTermino = new Carbon($nuevoRegistroCompras->fecha_termino);
          }
          //$diferenciaFechas = $fechaInicio->diffInHours($fechaTermino) . ':' . $fechaInicio->diff($fechaTermino)->format('%I:%S');
          $diferenciaFechas = $fechaInicio->diffInDays($fechaTermino);
          //dd($diferenciaFechas);
          $nuevoRegistroCompras->tiempo_transcurrido = $diferenciaFechas;



          $array_datos = $value->datos;

          if($array_datos){

              foreach ($array_datos as $key2 => $value2) {
                  if(property_exists($value2, 'fecha_requerimiento')){
                      $nuevoRegistroCompras->fecha_requerimiento = $value2->fecha_requerimiento;
                  }
                  if(property_exists($value2, 'con_cargo_a')){
                      $nuevoRegistroCompras->con_cargo_a = $value2->con_cargo_a;
                  }
                  if(property_exists($value2, 'unidad_requirente')){
                      $nuevoRegistroCompras->unidad_requirente = $value2->unidad_requirente;
                  }
                  if(property_exists($value2, 'pasaje_aereo')){
                      $nuevoRegistroCompras->pasaje_aereo = $value2->pasaje_aereo;
                  }
                  if(property_exists($value2, 'monto')){
                      $nuevoRegistroCompras->monto = $value2->monto;
                  }
                  if(property_exists($value2, 'listado_requerimiento')){

                      $nuevoRegistroCompras->listado_requerimiento = $value2->listado_requerimiento;
                  }
              }
            }
            if(property_exists($value, 'etapas')){
              $array_etapas = $value->etapas;
                  if(count($array_etapas)>0){
                    $ultimaEtapa = $array_etapas[0];

                    if(property_exists($ultimaEtapa, 'usuario_asignado')){
                      $nuevoRegistroCompras->usuario_asignado_etapa = $ultimaEtapa->usuario_asignado->email;
                    }

                    if(property_exists($ultimaEtapa, 'tarea')){
                      $objetoEtapa = $ultimaEtapa->tarea;
                      $nuevoRegistroCompras->etapa_actual = $objetoEtapa->nombre;
                    }
                    if(property_exists($ultimaEtapa, 'fecha_modificacion')){
                     $nuevoRegistroCompras->fecha_modificacion = $ultimaEtapa->fecha_modificacion;
                    }

                  }
            }
      $dataFinal[$key] = $nuevoRegistroCompras;
      }

  if ($controladorId != null) {

      foreach ($dataFinal as $keyN => $registroN) {
        if ($registroN->id == $controladorId) {
          $dataFiltrada[$keyN] = $registroN;
        }
      }
   $dataFinal = $dataFiltrada;
 }



//PROCESO NUEVO DE COMPRA
$vistaId2 = null;
//$controladorId2 = $request->id;
$dataProcesada2 = [];
$dataFinal2 = [];
$dataFiltrada2 = [];

$client2 = new Client();

    try {
    $request2 = $client2->get("https://simple.mindep.cl/backend/api/procesos/16/tramites?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu&maxResults=20" , ['verify' => false]);
    $response2 = $request2->getBody()->getContents();
    $data2 =  json_decode($response2);
  }catch (BadResponseException   $e2) {
          $response2 = $e2->getResponse();
      }

    $data12 = $data2;
    while ($data12->tramites->nextPageToken != null) {
      try {
            $client12 = new Client();
            $request12 = $client12->get("https://simple.mindep.cl/backend/api/procesos/16/tramites?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu&maxResults=20&pageToken=".$data12->tramites->nextPageToken , ['verify' => false]);
            $response12 = $request12->getBody()->getContents();
            $data12 =  json_decode($response12);
          }catch (BadResponseException   $er2) {
                  $response12 = $er2->getResponse();
              }

             foreach ($data12->tramites->items as $key32 => $value32) {
               $data2->tramites->items[count($data2->tramites->items)+($key32)] = $value32;
             }

    }

$dataProcesada2 = $data2->tramites->items;

foreach ($dataProcesada2 as $key2 => $value2) {
  $nuevoRegistroCompras2 = new RequerimientoCompras();
  $nuevoRegistroCompras2->id= $value2->id;
  $nuevoRegistroCompras2->estado= $value2->estado;
  $nuevoRegistroCompras2->fecha_inicio= $value2->fecha_inicio;
  $nuevoRegistroCompras2->fecha_termino= $value2->fecha_termino;
  $fechaInicio2 = new Carbon($nuevoRegistroCompras2->fecha_inicio);
  if ($value2->estado == 'pendiente' && $value2->fecha_termino == null) {
     $fechaTermino2 = Carbon::now();
  }else{
    $fechaTermino2 = new Carbon($nuevoRegistroCompras2->fecha_termino);
  }
  //$diferenciaFechas = $fechaInicio->diffInHours($fechaTermino) . ':' . $fechaInicio->diff($fechaTermino)->format('%I:%S');
  $diferenciaFechas2 = $fechaInicio2->diffInDays($fechaTermino2);
  //dd($diferenciaFechas);
  $nuevoRegistroCompras2->tiempo_transcurrido = $diferenciaFechas2;



  $array_datos2 = $value2->datos;

  if($array_datos2){

      foreach ($array_datos2 as $key22 => $value22) {
          if(property_exists($value22, 'fecha_requerimiento')){
              $nuevoRegistroCompras2->fecha_requerimiento = $value22->fecha_requerimiento;
          }
          if(property_exists($value22, 'con_cargo_a')){
              $nuevoRegistroCompras2->con_cargo_a = $value22->con_cargo_a;
          }
          if(property_exists($value22, 'unidad_requirente')){
              $nuevoRegistroCompras2->unidad_requirente = $value22->unidad_requirente;
          }
          if(property_exists($value22, 'pasaje_aereo')){
              $nuevoRegistroCompras2->pasaje_aereo = $value22->pasaje_aereo;
          }
          if(property_exists($value22, 'monto')){
              $nuevoRegistroCompras2->monto = $value22->monto;
          }
          if(property_exists($value22, 'listado_requerimiento')){

              $nuevoRegistroCompras2->listado_requerimiento = $value22->listado_requerimiento;
          }
      }
    }
    if(property_exists($value2, 'etapas')){
      $array_etapas2 = $value2->etapas;
          if(count($array_etapas2)>0){
            $ultimaEtapa2 = $array_etapas2[0];

            if(property_exists($ultimaEtapa2, 'usuario_asignado')){
              $nuevoRegistroCompras2->usuario_asignado_etapa = $ultimaEtapa2->usuario_asignado->email;
            }

            if(property_exists($ultimaEtapa2, 'tarea')){
              $objetoEtapa2 = $ultimaEtapa2->tarea;
              $nuevoRegistroCompras2->etapa_actual = $objetoEtapa2->nombre;
            }
            if(property_exists($ultimaEtapa2, 'fecha_modificacion')){
             $nuevoRegistroCompras2->fecha_modificacion = $ultimaEtapa2->fecha_modificacion;
            }

          }
    }
$dataFinal2[$key2] = $nuevoRegistroCompras2;
}

if ($controladorId2 != null) {

foreach ($dataFinal2 as $keyN2 => $registroN2) {
if ($registroN2->id == $controladorId2) {
  $dataFiltrada2[$keyN2] = $registroN2;
}
}
$dataFinal2 = $dataFiltrada2;
}

$dataFinal = array_merge($dataFinal2, $dataFinal);



      switch ($tipoReporte) {
      case 'PDF':

      break;
      case 'XLS':
                  Excel::create("ReporteCompras", function ($excel) use ($dataFinal) {
                  $excel->setTitle("ReporteCompras");
                  $excel->sheet("ReporteCompras", function ($sheet) use ($dataFinal) {
                  $sheet->loadView('requerimientos_compras.requerimientos_compras_ex_excel')->with('dataFinal', $dataFinal);
              });
          })->download('xlsx');

              return back();

              break;
      default:
      break;
    }}

    public function destroy($id){

            $tramite = TramitesSimple::where('id', $id)->first();
            $result = $tramite->delete();
                if ($result) {
                    return response()->json(['success'=>'true']);
                }else{
                    return response()->json(['success'=> 'false']);
                }
        }

  public function acta(Request $request){

          $tramites = [];
          $tramiteFiltrado = [];
          $idTramiteFiltro = $request->nRegistro;
          $tituloFiltro = $request->titulo;


          $queryFilter = "where r.numero_tramite = $idTramiteFiltro ";
          
               $query = "SELECT  *
                   FROM
                  (select
                          tramite.id as numero_tramite ,
                          tramite.proceso_id as numero_proeceso ,
                          proceso.nombre as nombre_proceso ,
                          tramite.created_at as fecha_inicio ,
                          tramite.updated_at as fecha_ultima_modificacion ,
                          tramite.pendiente as pendiente,
                          tramite.ended_at as fecha_culmino,
                          tarea.nombre as etapa,
                          et.id as id_etapa,
                          dts.conteo as cantidad_campos
                          from tramite
                          INNER JOIN proceso ON tramite.proceso_id = proceso.id
                          INNER JOIN (SELECT * FROM etapa WHERE id IN (SELECT MAX(id) FROM etapa GROUP BY tramite_id) ORDER BY id DESC) et on et.tramite_id = tramite.id
                          INNER JOIN tarea on et.tarea_id = tarea.id
                          LEFT JOIN (SELECT  COUNT(id) as conteo, etapa_id FROM dato_seguimiento  group by etapa_id ) dts on dts.etapa_id = et.id
                          where tramite.proceso_id = 18  and tarea.nombre = '1 - MINDEP - Formulario Datos de Recepción Conforme' and tramite.deleted_at is null
                          
          UNION ALL
          select
                          tramite.id as numero_tramite ,
                          tramite.proceso_id as numero_proeceso ,
                          proceso.nombre as nombre_proceso ,
                          tramite.created_at as fecha_inicio ,
                          tramite.updated_at as fecha_ultima_modificacion ,
                          tramite.pendiente as pendiente,
                          tramite.ended_at as fecha_culmino,
                          tarea.nombre as etapa,
                          et.id as id_etapa,
                          dts.conteo as cantidad_campos
                          from tramite
                          INNER JOIN proceso ON tramite.proceso_id = proceso.id
                          INNER JOIN (SELECT * FROM etapa WHERE id IN (SELECT MAX(id) FROM etapa GROUP BY tramite_id) ORDER BY id DESC) et on et.tramite_id = tramite.id
                          INNER JOIN tarea on et.tarea_id = tarea.id
                          LEFT JOIN (SELECT  COUNT(id) as conteo, etapa_id FROM dato_seguimiento  group by etapa_id ) dts on dts.etapa_id = et.id
                          where tramite.proceso_id = 18 and tramite.deleted_at is null
                          and tarea.nombre <> '1 - MINDEP - Formulario Datos de Recepción Conforme') r ";
          
              ($idTramiteFiltro != null )? $query = $query.$queryFilter : $query = $query;
              $query = $query."order by r.numero_tramite DESC";
          
              $tramites = DB::connection('mysqlSimple')->select($query);
              foreach ($tramites as $llave2 => $valor2) {
                $etapas = Etapa::Select('id')->where('tramite_id' , $valor2->numero_tramite)->get()->pluck('id');
                $datoSeguimientoTitulo = DatoSeguimiento::whereIn('etapa_id', $etapas)->where('nombre', 'titulocompra')->first();
                
                if ($datoSeguimientoTitulo) {
                  $datoSeguimientoTitulo = $datoSeguimientoTitulo->valor;
                  $datoSeguimientoTitulo = str_replace( '"', '' , $datoSeguimientoTitulo);
                  $datoSeguimientoTitulo = str_replace( '\u00e1', 'á' , $datoSeguimientoTitulo);
                  $datoSeguimientoTitulo = str_replace( '\u00f3', 'ó' , $datoSeguimientoTitulo);
                  $datoSeguimientoTitulo = str_replace( '\u00d1', 'Ñ' , $datoSeguimientoTitulo);
                  $datoSeguimientoTitulo = str_replace( '\u00c1', 'Á' , $datoSeguimientoTitulo);
                  $datoSeguimientoTitulo = str_replace( '\u2019', '’' , $datoSeguimientoTitulo);
                  $datoSeguimientoTitulo = str_replace( '\u00fa', 'ú' , $datoSeguimientoTitulo);
                  $datoSeguimientoTitulo = str_replace( '\u00e9', 'é' , $datoSeguimientoTitulo);
                  $datoSeguimientoTitulo = str_replace( '\u00ed', 'í' , $datoSeguimientoTitulo);
                  $datoSeguimientoTitulo = str_replace( '\u00b0', '°' , $datoSeguimientoTitulo);
                  }

                  $valor2->tituloActa = $datoSeguimientoTitulo;
                  
                  if ($tituloFiltro) {
                    //if(strpos($datoSeguimientoTitulo, $tituloFiltro)){
                      if (str_contains($datoSeguimientoTitulo,  $tituloFiltro)) {
                      $tramiteFiltrado[] = $valor2; 
                    }
                  }

                  
              }
              if ($tituloFiltro) {
              $tramites = $tramiteFiltrado;              
              }

              return view('requerimientos_compras.acta_recepcion_conforme')
              ->with('tramitesD', $tramites)
              ->with('idTramiteFiltro', $idTramiteFiltro)
              ->with('tituloFiltro', $tituloFiltro);
          
  }
              

  public function actaShow($id){
                        
              $client = new Client();
                    try {
                    $request = $client->get("https://simple.mindep.cl/backend/api/tramites/$id?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu " , ['verify' => false]);
                    $response = $request->getBody()->getContents();
                    $data =  json_decode($response);
                      }catch (BadResponseException   $e) {
                          $response = $e->getResponse();
                      }
              $data = $data->tramite;
       
              $etapasCont = count($data->etapas);

              $nuevaActa = new ActaRecepcionConforme();
              $nuevaActa->id =  $data->id;
              $nuevaActa->fecha_inicio =  $data->fecha_inicio;
              $nuevaActa->fecha_termino =  $data->fecha_termino;
              $nuevaActa->fecha_modificacion =  $data->fecha_modificacion;
              $nuevaActa->estado =  $data->estado;
              
              $datos=$data->datos;

              
            

              foreach ($datos as $key => $value2) {
                   if(property_exists($value2, 'acta_recepcion')){
                      $nuevaActa->acta_recepcion =  $value2->acta_recepcion;
                    }
                    if(property_exists($value2, 'tramite')){
                      $nuevaActa->tramite =  $value2->tramite;
                      $nuevaActa->tramite =  str_replace( 'Trámite: ', '' , $nuevaActa->tramite);
                    }
                    if(property_exists($value2, 'titulocompra')){
                      $nuevaActa->titulocompra =  $value2->titulocompra;
                    }
                    if(property_exists($value2, 'adjuntar_certificado')){
                      $nuevaActa->adjuntar_certificado =  $value2->adjuntar_certificado->URL;
                    }
                    if(property_exists($value2, 'orden_de_compra')){
                      $nuevaActa->orden_de_compra =  $value2->orden_de_compra->URL;
                    }
                    if(property_exists($value2, 'bases_de_doc_li')){
                      $nuevaActa->bases_de_doc_li =  $value2->bases_de_doc_li->URL;
                    }
                    if(property_exists($value2, 'bases_de_doc_tra')){
                      $nuevaActa->bases_de_doc_tra =  $value2->bases_de_doc_tra->URL;
                    } 
                    if(property_exists($value2, 'adjuntar_factura')){
                      $nuevaActa->adjuntar_factura =  $value2->adjuntar_factura->URL;
                    }
                    if(property_exists($value2, 'api_area')){
                      $nuevaActa->api_area =  $value2->api_area;
                    }
                    if(property_exists($value2, 'observaciones')){
                      $nuevaActa->observaciones =  $value2->observaciones;
                    }
                    if(property_exists($value2, 'nombre_de_proveedor')){
                      $nuevaActa->nombre_de_proveedor =  $value2->nombre_de_proveedor;
                    }
                    if(property_exists($value2, 'numero_de_orcom')){
                      $nuevaActa->numero_de_orcom =  $value2->numero_de_orcom;
                    }
                    if(property_exists($value2, 'fecha_orden_compra')){
                      $nuevaActa->fecha_orden_compra =  $value2->fecha_orden_compra;
                    }
                    if(property_exists($value2, 'n_factura')){
                      $nuevaActa->n_factura =  $value2->n_factura;
                    }
                    if(property_exists($value2, 'fecha_factura')){
                      $nuevaActa->fecha_factura =  $value2->fecha_factura;
                    }
                    if(property_exists($value2, 'monto_factura')){
                      $nuevaActa->monto_factura =  $value2->monto_factura;
                    }
                    if(property_exists($value2, 'listado_requerimiento')){
                      $nuevaActa->listado_requerimiento =  $value2->listado_requerimiento;
                    }
                    if(property_exists($value2, 'declaracion')){
                      $nuevaActa->declaracion =  $value2->declaracion;
                    }
                    if(property_exists($value2, 'dependencia')){
                      $nuevaActa->dependencia =  $value2->dependencia;
                    }
                    if(property_exists($value2, 'subrogante')){
                      $nuevaActa->subrogante =  $value2->subrogante;
                    }
                    if(property_exists($value2, 'firma')){
                      $nuevaActa->firma =  $value2->firma;
                      $grupos = Grupos::where('id', $nuevaActa->firma)->first();
                      $nuevaActa->nombreGrupo = $grupos->nombre;
                    }
                    if(property_exists($value2, 'firma_sub')){
                      $nuevaActa->firma_sub =  $value2->firma_sub;
                      $grupos = Grupos::where('id', $nuevaActa->firma_sub)->first();
                      $nuevaActa->nombreGrupoSub = $grupos->nombre;
                    }
                    if(property_exists($value2, 'archivo_a')){
                      $nuevaActa->archivo_a =  $value2->archivo_a->URL;
                    }
                    if(property_exists($value2, 'archivo_b')){
                      $nuevaActa->archivo_b =  $value2->archivo_b->URL;
                    }
                    if(property_exists($value2, 'archivo_c')){
                      $nuevaActa->archivo_c =  $value2->archivo_c->URL;
                    }
                    if(property_exists($value2, 'archivo_d')){
                      $nuevaActa->archivo_d =  $value2->archivo_d->URL;
                    }
                    if(property_exists($value2, 'archivo_e')){
                      $nuevaActa->archivo_e =  $value2->archivo_e->URL;
                    }
                    if(property_exists($value2, 'act_rc')){
                      $nuevaActa->act_rc_descarga = 'https://simple.mindep.cl/uploads/documentos/'.$value2->act_rc;
                     }                  
                }

            return view('requerimientos_compras.acta_recepcion_conforme_detalle')
            ->with('data' , $nuevaActa);
  }

  public function destroyActas($id){

    $tramiteActas = TramitesSimple::where('id', $id)->first();
    $result = $tramiteActas->delete();
        if ($result) {
            return response()->json(['success'=>'true']);
        }else{
            return response()->json(['success'=> 'false']);
        }
}

}
