<?php

namespace App\Http\Controllers\ResolucionesExentas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\DerivacionesDocumento\DerivacionesDocumento;
use App\Models\IngresoTramite\IngresoTramite;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ComboBoxCompra;
use App\Models\TiposDocumentos\TiposDocumentos;
use App\Models\Divisiones\Divisiones;
use App\Models\ResolucionExenta\ResolucionExenta;
use DB;
use App\Models\File\File;
use Illuminate\Support\Facades\Auth;
use App\Models\UsuariosSimple\UsuariosSimple;
use App\Models\TramitesSimple\TramitesSimple;
use App\Mail\DistribucionResolucionEmail;
use App\Models\UsuariosMindep\UsuariosMindep;
use App\Models\CorreoDistribucionProcesos\CorreoDistribucionProcesos;
use Mail;





class ResolucionesExentasController extends Controller
{
   public function index(Request $request){
      $selectA = Divisiones::get()->pluck('nombre', 'nombre')->prepend('Todos', null);
      $requestNRegistro = null;
      $listado = [];
      $anno = Carbon::now()->format('Y');
      $fechaStringInicio = ($anno).'-01-01 00:00:00';
      $feDesde = Carbon::parse($fechaStringInicio);
      $feHasta =  Carbon::parse(Carbon::now()->format('Y-m-d').' 23:59:59');
      $fechaInDesde = Carbon::parse($fechaStringInicio);
      $fechaInHasta = Carbon::now();
      $fechas[1] = Date('Y-m-d', strtotime( '1970-01-01' ));
      $fechas[0] = Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
      $areRemitenteView = '';
      $remitenteView = '';
      $referenciaView = '';
      $correlativoView = '';
      $divisionusuarios = Auth::user()->department;
      $divisiondestinatario = Auth::user()->department;
      $reservado = 'reservado';
      $esAdmin = false;
      $listadoNoreservado = collect([]);



          if( $request->FechaDesde != null ) {
            $feDesde=(Carbon::parse($request->get('FechaDesde'))->format('dmY'));
            $feDesde= Carbon::parse(substr($feDesde, 0, 2).'-'.substr($feDesde, 2, 2).'-'.substr($feDesde, 4, 4).' 00:00:00');
          }
          if( $request->FechaHasta != null ) {
             $feHasta=(Carbon::parse($request->get('FechaHasta'))->format('dmY'));
             $feHasta= Carbon::parse(substr($feHasta, 0, 2).'-'.substr($feHasta, 2, 2).'-'.substr($feHasta, 4, 4).' 23:59:59');
          }



      $query = ResolucionExenta::query ();
      $query->when(request('nRegistro', false), function ($q, $numRegistro) { 
            return $q->where('tramite_id', $numRegistro);
        });
      $query->when(request('remitente'), function ($q, $remitente) { 
            return $q->where('remitente', 'LIKE', "%$remitente%");
        });
      $query->when(request('referencia'), function ($q, $referencia) { 
            return $q->where('referencia', 'LIKE', "%$referencia%");
        });
      $query->when($feDesde, function ($q, $feDesde ) { 
            return $q->where('created_at','>=', $feDesde);
        });
      $query->when($feHasta, function ($q, $feHasta ) { 
            return $q->where('created_at','<=', $feHasta);
        });

      foreach (auth()->user()->roles as $role) {
            if ($role->todos == 1) {
                $esAdmin=  true;
                break;
            }
        }
   /*   if(!$esAdmin){

      $query->when(request('areaRemtiente', false), function ($q, $areaRemtiente) { 
            return $q->where('division', $areaRemtiente);
        });
      $query->when($divisionusuarios, function ($q, $divisionusuarios) { 
            return $q->where('division', $divisionusuarios);
        });
      $query->when($divisiondestinatario, function ($q, $divisiondestinatario) { 
            return $q->orwhere('division_destinatario', $divisiondestinatario);
        });
      $query->when($reservado, function ($q, $reservado ) { 
            return $q->where('reservado', 0);
        });
}*/

     $listado = $query->get()->sortByDesc(function($q){
     return $q->fecha_folio;
     });

      if($request->correlativo != null ) {
      
      $correlativoView = (int)$request->correlativo;
      foreach ($listado as $clave => $reg) {
       if($reg->correlativo != ''){
        if($reg->correlativo->correlativo === $correlativoView)
        $listado = [$reg];    
      }
      }
      }
      /*if(!$esAdmin){
      foreach ($listado as $clave => $reg) {

        if($reg->reservado == 0)
        $listadoNoreservado->push($reg);    
      }
      $listado = $listadoNoreservado;
      }

      $emailUsuarioSesion = auth()->user()->email;
      $nombrecargosimple = UsuariosSimple::where('email',$emailUsuarioSesion)->first()->nombre_cargo;
    $listado2 = ResolucionExenta::where('remitente', $nombrecargosimple )->where('reservado', 1)->get();
      foreach ($listado2 as $key => $value) {
        $listado->push($value);
      }*/

     $emailUsuarioSesion = auth()->user()->email;
     $nombrecargo = UsuariosSimple::where('email',$emailUsuarioSesion)->first()->nombre_cargo;


     if( $request->nRegistro != null ) {
        $requestNRegistro = $request->nRegistro;
      }
        if($request->areaRemtiente != null ) {
        $areRemitenteView =  $request->areaRemtiente;
      }
      if($request->remitente != null ) {
        $remitenteView =  $request->remitente;
        
      }
      if($request->referencia != null ) {
        $referenciaView =  $request->referencia;        
      }


      return view('resolucionexenta.resolucionexenta')
      ->with('selectA' ,$selectA)
      ->with('areRemitenteView' ,$areRemitenteView)
      ->with('remitenteView' ,$remitenteView)
      ->with('referenciaView' ,$referenciaView)
      ->with('correlativoView' ,$correlativoView)
      ->with('feDesde' ,$feDesde)
      ->with('feHasta' ,$feHasta)
      ->with('fechaInDesde' ,$fechaInDesde)
      ->with('fechaInHasta' ,$fechaInHasta)
      ->with('fechas' ,$fechas)
      ->with('requestNRegistro' , $requestNRegistro)
      ->with('listado' , $listado)
      ->with('nombrecargo', $nombrecargo)
      ->with('esAdmin', $esAdmin);


} 

public function cargarExcelDeparatamentos (){


      $array = collect(Excel::load(public_path('departamentos2.xlsx'), function($reader) {})->get());
      $arrayTransformado = [];
      $cantidad = count($array[0]);
     
      for ($i=0; $i < $cantidad; $i++) { 
        $arrayTransformado[$array[0][$i]['area']] = $array[0][$i]['area'];
       } 

     
        return (collect($arrayTransformado)->prepend('Todos', null));
      
    }

public function show(Request $request, $id){

    $detalle = ResolucionExenta::where('id', $id)->first();
         return view('resolucionexenta.resolucionexenta_detalle')
            ->with('detalle', $detalle);  
}
public function envioCorreo($idMemo , $tramiteId){
      foreach (UsuariosMindep::get() as $key1 => $value1){$selectUsuarios[$value1->email]=$value1->nombres.' '.$value1->apellido_p.' '.$value1->apellido_m.' '.$value1->departamento.' '.$value1->email; }
      $detalle = ResolucionExenta::where('id', $idMemo)->first();
         return view('resolucionexenta.resolucionexenta_distribucion')
          ->with('selectUsuarios' , $selectUsuarios)
          ->with('detalle' , $detalle);
}
public function distribucionStd(Request $request , $idMemo){

      $correosInternos = $request->correos_internos;
      $correosExternos = $request->correos_externos;
      $datos = ResolucionExenta::where('id', $idMemo)->first();
      if($correosInternos){
      foreach ($correosInternos as $keyi => $valuei) {
           Mail::to($valuei)->send(new DistribucionResolucionEmail($datos));
           $nuevoCorreoEnvado = new CorreoDistribucionProcesos ();
           $nuevoCorreoEnvado->proceso_simple_id = 3;
           $nuevoCorreoEnvado->usuario_id =  Auth::user()->id;
           $nuevoCorreoEnvado->tramite_id =  $datos->tramite_id;
           $nuevoCorreoEnvado->correo_destino = $valuei;
           $nuevoCorreoEnvado->save();
        }
      }

      if(count($correosExternos)>0){
      foreach ($correosExternos as $keye => $valuee) {
        if($valuee){
           Mail::to($valuee)->send(new DistribucionResolucionEmail($datos));
           $nuevoCorreoEnvado = new CorreoDistribucionProcesos ();
           $nuevoCorreoEnvado->proceso_simple_id = 3;
           $nuevoCorreoEnvado->usuario_id =  Auth::user()->id;
           $nuevoCorreoEnvado->tramite_id =  $datos->tramite_id;
           $nuevoCorreoEnvado->correo_destino = $valuee;
           $nuevoCorreoEnvado->save();
        }
        }
      }

      alert()->success('!','correos enviados exitosamente');
      return redirect()->route('ResolucionesExentasshow',  $idMemo);


}


}
