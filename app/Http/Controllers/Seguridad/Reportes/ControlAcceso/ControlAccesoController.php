<?php

namespace App\Http\Controllers\Seguridad\Reportes\ControlAcceso;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use Config;
use SPDF;

class ControlAccesoController extends Controller
{

    protected $extension;
    protected $temporary_folder;

    public function __construct()
    {
            $this->extension = Config::get('constants.reportes.caract_extension');
            $this->temporary_folder = Config::get('constants.reportes.carac_temporary_folder');
    }

    public function index()
    {
        $logs = DB::table('audits')
        ->where('url', 'LIKE', '%login')
        ->orWhere('url', 'LIKE', '%logout')
        ->leftJoin('users', 'users.id', '=', 'audits.user_id')
        ->orderBy('audits.id', 'DESC')
        ->select('audits.*', 'users.name as username', 'users.email as user_email')
        ->paginate(15);

        for($x = 0; $x < count($logs); $x++) {
            $logs[$x]->action = strpos($logs[$x]->url, 'login') ? 'Inicio de sesión' : 'Cierre de sesión';
        }


        return view('seguridad.reportes.control_acceso.se12_i_controlacceso')
        ->with('logs', $logs);
    }

    public function reporteAcceso($tipoReporte) {
        //constantes
        $logs = DB::table('audits')
        ->where('url', 'LIKE', '%login')
        ->orWhere('url', 'LIKE', '%logout')
        ->leftJoin('users', 'users.id', '=', 'audits.user_id')
        ->orderBy('audits.id', 'DESC')
        ->select('audits.*', 'users.name as username', 'users.email as user_email')
        ->get();

        for($x = 0; $x < count($logs); $x++) {
            $logs[$x]->action = strpos($logs[$x]->url, 'login') ? 'Inicio de sesión' : 'Cierre de sesión';
        }

        $fecha_actual_slh = Carbon::now()->format('d/m/Y H:i:s a'); //fecha DD/MM/YYYY
        $fecha_actual_flg = Carbon::now()->format('YmdHis'); //fecha YYYYMMDDHHMMSS

        $param = array(
        'fecha_actual' => $fecha_actual_slh,
        'logs' =>$logs
    );

        //tiempo limite para generacion de reporte
        set_time_limit(120);

        //toma en cuenta el tipo de reporte seleccionado
    switch ($tipoReporte) {
    case 'PDF': //construcción PDF
            $headerHtml = \View::make('seguridad.reportes.control_acceso.se_re_02_pd_pdfheader', $param)
        ->render();
        //$footerHtml = view()->make('pdf.footer')->render();
        $pdf = SPDF::loadView('seguridad.reportes.control_acceso.se_re_03_pd_pdfbody', $param)
        ->setOption('header-html', $headerHtml)
        ->setOption('margin-top', '40mm')
        ->setTemporaryFolder($this->temporary_folder);
        $pdf->setPaper('a4', 'portrait');
        return $pdf->download('SeguridadReportesControlAcceso'.$fecha_actual_flg.'.'.$this->extension->pdf);

    break;
    case 'XLS': //construccion excel
                Excel::create("SeguridadReportesControlAcceso", function ($excel) use ($logs) {
                    $excel->setTitle("SeguridadReportesControlAcceso");
                    $excel->sheet("ControlAcceso", function ($sheet) use ($logs) {
                        $sheet->loadView('seguridad.reportes.control_acceso.se17_ex_excel')->with('logs', $logs);
                    });
                })->download($this->extension->excel);

            return back();

            break;
    default:
    break;
    }

    }


}
