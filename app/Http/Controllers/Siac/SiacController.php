<?php

namespace App\Http\Controllers\Siac;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use App\Models\Siac\Siac;
use App\Models\TramitesSimpleExterno\TramitesSimpleExterno;
use App\Mail\NotificacionTramiteIncompletoSiacEmail;
use Mail;


class SiacController extends Controller
{
   public function index(Request $request){


$tramites = [];
$idTramiteFiltro = $request->nRegistro;
$queryFilter = "where r.numero_tramite = $idTramiteFiltro ";

     $query = "SELECT  *
                FROM
                (select
                        tramite.id as numero_tramite ,
                        tramite.proceso_id as numero_proeceso ,
                        proceso.nombre as nombre_proceso ,
                        tramite.created_at as fecha_inicio ,
                        tramite.updated_at as fecha_ultima_modificacion ,
                        tramite.pendiente as pendiente,
                        tramite.ended_at as fecha_culmino,
                        tarea.nombre as etapa,
                        et.id as id_etapa,
                        dts.conteo as cantidad_campos
                        from tramite
                        INNER JOIN proceso ON tramite.proceso_id = proceso.id
                        INNER JOIN (SELECT * FROM etapa WHERE id IN (SELECT MAX(id) FROM etapa GROUP BY tramite_id) ORDER BY id DESC) et on et.tramite_id = tramite.id
                        INNER JOIN tarea on et.tarea_id = tarea.id
                        LEFT JOIN (SELECT  COUNT(id) as conteo, etapa_id FROM dato_seguimiento  group by etapa_id ) dts on dts.etapa_id = et.id
                        where tramite.proceso_id = 23  and tarea.nombre = 'SIAC - Ingreso Solicitudes' and tramite.deleted_at is null
                        
            UNION ALL
            select
                        tramite.id as numero_tramite ,
                        tramite.proceso_id as numero_proeceso ,
                        proceso.nombre as nombre_proceso ,
                        tramite.created_at as fecha_inicio ,
                        tramite.updated_at as fecha_ultima_modificacion ,
                        tramite.pendiente as pendiente,
                        tramite.ended_at as fecha_culmino,
                        tarea.nombre as etapa,
                        et.id as id_etapa,
                        dts.conteo as cantidad_campos
                        from tramite
                        INNER JOIN proceso ON tramite.proceso_id = proceso.id
                        INNER JOIN (SELECT * FROM etapa WHERE id IN (SELECT MAX(id) FROM etapa GROUP BY tramite_id) ORDER BY id DESC) et on et.tramite_id = tramite.id
                        INNER JOIN tarea on et.tarea_id = tarea.id
                        LEFT JOIN (SELECT  COUNT(id) as conteo, etapa_id FROM dato_seguimiento  group by etapa_id ) dts on dts.etapa_id = et.id
                        where tramite.proceso_id = 23
                        and tramite.deleted_at is null
                        and tarea.nombre <> 'SIAC - Ingreso Solicitudes') r ";

    ($idTramiteFiltro != null )? $query = $query.$queryFilter : $query = $query;
    $query = $query."order by r.numero_tramite DESC";

    $tramites = DB::connection('mysqlSimpleExterno')->select($query);
    return view('siac.siac')
    ->with('tramitesD', $tramites)
    ->with('idTramiteFiltro', $idTramiteFiltro);

    }

    public function show($id)
    {
        $client = new Client();
               try {
              $request = $client->get("https://tramites.mindep.cl/backend/api/tramites/$id?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu" , ['verify' => false]);
              $response = $request->getBody()->getContents();
              $data =  json_decode($response);
                }catch (BadResponseException   $e) {
                    $response = $e->getResponse();
                }
         $data = $data->tramite;
         $nuevoSiac = new Siac();
         $nuevoSiac->id =  $data->id;
         $nuevoSiac->fecha_inicio =  $data->fecha_inicio;
         $nuevoSiac->fecha_termino =  $data->fecha_termino;
         $nuevoSiac->fecha_modificacion =  $data->fecha_modificacion;
         $nuevoSiac->estado =  $data->estado;

         $datos=$data->datos;

         foreach ($datos as $key => $value2) {
              if(property_exists($value2, 'origen_requerimiento')){
                $nuevoSiac->origen_requerimiento =  $value2->origen_requerimiento;
              }
             if(property_exists($value2, 'cual_origen')){
                $nuevoSiac->cual_origen =  $value2->cual_origen;
              }
              if(property_exists($value2, 'tipificacion')){
                $nuevoSiac->tipificacion =  $value2->tipificacion;
              }
              if(property_exists($value2, 'detalle_solicitud')){
                $nuevoSiac->detalle_solicitud =  $value2->detalle_solicitud;
              }
              if(property_exists($value2, 'nombre_completo')){
                $nuevoSiac->nombre_completo =  $value2->nombre_completo;
              }
              if(property_exists($value2, 'nacionalidad')){
                $nuevoSiac->nacionalidad =  $value2->nacionalidad;
              }
              if(property_exists($value2, 'indique_nacionalidad')){
                $nuevoSiac->indique_nacionalidad =  $value2->indique_nacionalidad;
              }
              if(property_exists($value2, 'pasaporte')){
                $nuevoSiac->pasaporte =  $value2->pasaporte;
              }
              if(property_exists($value2, 'rut')){
                $nuevoSiac->rut =  $value2->rut;
              }
              if(property_exists($value2, 'pais_residencia')){
                $nuevoSiac->pais_residencia =  $value2->pais_residencia;
              }
              if(property_exists($value2, 'region')){
                $nuevoSiac->region =  $value2->region->region;
              }
              if(property_exists($value2, 'region')){
                $nuevoSiac->comuna =  $value2->region->comuna;
              }
              if(property_exists($value2, 'calle_casa')){
                $nuevoSiac->calle_casa =  $value2->calle_casa;
              }
              if(property_exists($value2, 'direccion_completa')){
                $nuevoSiac->direccion_completa =  $value2->direccion_completa;
              }
              if(property_exists($value2, 'telefono')){
                $nuevoSiac->telefono =  $value2->telefono;
              }
              if(property_exists($value2, 'correo2')){
                $nuevoSiac->correo2 =  $value2->correo2;
              }
              if(property_exists($value2, 'zona')){
                $nuevoSiac->zona =  $value2->zona;
              }
              if(property_exists($value2, 'nivel_educacional')){
                $nuevoSiac->nivel_educacional =  $value2->nivel_educacional;
              }
              if(property_exists($value2, 'ocupacion')){
                $nuevoSiac->ocupacion =  $value2->ocupacion;
              }
              if(property_exists($value2, 'participa_organizacion')){
                $nuevoSiac->participa_organizacion =  $value2->participa_organizacion;
              }
              if(property_exists($value2, 'cual_organizacion')){
                $nuevoSiac->cual_organizacion =  $value2->cual_organizacion;
              }
              if(property_exists($value2, 'respuesta_mediante')){
                $nuevoSiac->respuesta_mediante =  $value2->respuesta_mediante;
              }
              if(property_exists($value2, 'correo')){
                $nuevoSiac->correo =  $value2->correo;
              }
              if(property_exists($value2, 'direccion_carta')){
                $nuevoSiac->direccion_carta =  $value2->direccion_carta;
              }
          } 


       return view('siac.siac_detalle')
      ->with('data' , $nuevoSiac);
    }


    //ELIMINAR TRÁMITE
    public function destroy($id)
    {

      $tramite = TramitesSimpleExterno::where('id', $id)->first();
      $result = $tramite->delete();
          if ($result) {
              return response()->json(['success'=>'true']);
          }else{
              return response()->json(['success'=> 'false']);
          }
    }

    //NORIFICACIÓN AL USUARIO
    public function notificacion($tramiteid)
    {
    

      $tramite = $tramiteid;

      $client = new Client();
               try {
              $request = $client->get("https://tramites.mindep.cl/backend/api/tramites/$tramite?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu" , ['verify' => false]);
              $response = $request->getBody()->getContents();
              $data =  json_decode($response);
                }catch (BadResponseException   $e) {
                    $response = $e->getResponse();
                }
         $data = $data->tramite;
         $nuevoSiac = new Siac();
         $nuevoSiac->id =  $data->id;
         $nuevoSiac->fecha_inicio =  $data->fecha_inicio;
         $nuevoSiac->fecha_termino =  $data->fecha_termino;
         $nuevoSiac->fecha_modificacion =  $data->fecha_modificacion;
         $nuevoSiac->estado =  $data->estado;

         $datos=$data->datos;

         foreach ($datos as $key => $value2) {
            if(property_exists($value2, 'origen_requerimiento')){
                $nuevoSiac->origen_requerimiento =  $value2->origen_requerimiento;
              }
             if(property_exists($value2, 'cual_origen')){
                $nuevoSiac->cual_origen =  $value2->cual_origen;
              }
              if(property_exists($value2, 'tipificacion')){
                 $nuevoSiac->tipificacion =  $value2->tipificacion;
              }
              if(property_exists($value2, 'detalle_solicitud')){
                  $nuevoSiac->detalle_solicitud =  $value2->detalle_solicitud;
              }
              if(property_exists($value2, 'correo2')){
                $nuevoSiac->correo2 =  $value2->correo2;
              }
          } 

      $correo = $nuevoSiac->correo2;

      Mail::to($correo)->send(new NotificacionTramiteIncompletoSiacEmail($nuevoSiac));
      
      /*$nuevoCorreo = new CorreoNotificaciones();
      $nuevoCorreo->tipo_notificaciones = 'Siac trámite incompleto';
      $nuevoCorreo->destintario = $correo;
      $nuevoCorreo->save();*/
    
      alert()->success('!','Notificación enviada exitosamente');
      return redirect()->back();
    }

}
