<?php

namespace App\Http\Controllers\Sigc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Sigc\Ambitos;
use App\Models\Sigc\AmbitosTabla;
use App\Http\Requests\SIGC\AmbitosRequest;

class AmbitosController extends Controller
{

    public function index(Request $request)
    {
      //dd($request->nombre);
      /*   set_time_limit(14200);
        /* $dataDefinitiva = [];
        $dataFiltrada = [];


        $client = new Client();

            try {
              $request = $client->get("http://10.100.23.13/webserviceautenticateactived/public/api/Get/Ambitos" , ['verify' => false]);
              $response = $request->getBody()->getContents();
              $data =  json_decode($response);
            }catch (BadResponseException   $e) {
              $response = $e->getResponse();
          }



//dd($data2);
       foreach ($data as $key => $value) {

        $objeto = new Ambitos();
        $objeto->id = $value->id;
        $objeto->nombre = $value->nombre;
        $dataDefinitiva[$key] = $objeto;
       }   */

       $dataDefinitiva = [];

       $requestnombreAmbito = $request->nombre;

       $dataDefinitiva = Ambitos::get();

      return view('sigc.sigc_ambitos')
      ->with('dataDefinitiva' , $dataDefinitiva)
      ->with('requestnombreAmbito' , $requestnombreAmbito);
    }


    public function create()
    {
      return view('sigc.sigc_crear_ambito');
    }


     public function store(AmbitosRequest $request)
    {
      $nombre = $request->nombre;
      $nuevoAmbito = new AmbitosTabla();
      $nuevoAmbito->nombre = $nombre;
      $nuevoAmbito->save();
      alert()->success('!','Ámbito creado exitosamente');
      return redirect()->route('sigc_ambitos');
    }

    public function edit($id)
    {
      $editAmbito = AmbitosTabla::where('id' , $id)->first();
      return view('sigc.sigc_editar_ambito')
      ->with('editAmbito' , $editAmbito);
    }


     public function update(AmbitosRequest $request, $id)
    {
      $nombre = $request->nombre;
      $nuevoAmbito = AmbitosTabla::where('id' , $id)->first();
      $nuevoAmbito->nombre = $nombre;
      $nuevoAmbito->save();
      alert()->success('!','Ámbito editado exitosamente');
      return redirect()->route('sigc_ambitos');
    }


}
