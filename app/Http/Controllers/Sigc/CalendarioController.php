<?php

namespace App\Http\Controllers\Sigc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Sigc\Calendario;
use App\Models\Sigc\CalendarioTabla;
use App\Models\Sigc\Eventos;
use App\Models\Sigc\Meses;
use App\Models\Sigc\Indicadores;
use App\Models\Sigc\Notificaciones;
use App\Models\Sigc\CorreoNotificaciones;
use Carbon\Carbon;
use App\Mail\NotificacionCalendarioCrearEmail;
use App\Mail\NotificacionCalendarioEditarEmail;
use App\Mail\TestEmail;
use App\Mail\NotificacionEventos;
use Mail;
use Illuminate\Support\Facades\Auth;
use App\Models\Sigc\Users;
use DB;

class CalendarioController extends Controller
{

    public function index()
    {
        $dataDefinitivaA = Calendario::where('ano' , (date("Y")))->orderby('mes' , 'desc')->get();
        $dataDefinitivaO = Calendario::where('ano' , '<=' , date("Y")-1)->orderby('id' , 'desc')->paginate(12);

      return view('sigc.sigc_calendario')
      ->with('dataDefinitivaA' , $dataDefinitivaA)
      ->with('dataDefinitivaO' , $dataDefinitivaO);
    }

        public static function calendarMonth($month , $dataCompleta){

      $dataConmpleta = $dataCompleta;
      $arrayEventos = [];
      $evento = [];
      foreach ($dataConmpleta as $llave => $valor) {

          if($valor->mes ==  ((int)date("m"))-1){

            $nuevoEvento =  new Eventos();
            $nuevoEvento->titulo = 'REPORTAR';
            $nuevoEvento->fecha = $valor->rep_desde;
            $nuevoEvento->color = 'success';
            $arrayEventos[0] = $nuevoEvento;

            $nuevoEvento =  new Eventos();
            $nuevoEvento->titulo = 'REPORTAR';
            $nuevoEvento->fecha = $valor->rep_hasta;
            $nuevoEvento->color = 'success';
            $arrayEventos[1] = $nuevoEvento;

            $nuevoEvento =  new Eventos();
            $nuevoEvento->titulo = 'REVISIÓN';
            $nuevoEvento->fecha = $valor->rev_desde;
            $nuevoEvento->color = 'warning';
            $arrayEventos[2] = $nuevoEvento;

            $nuevoEvento =  new Eventos();
            $nuevoEvento->titulo = 'REVISIÓN';
            $nuevoEvento->fecha = $valor->rev_hasta;
            $nuevoEvento->color = 'warning';
            $arrayEventos[3] = $nuevoEvento;

            $nuevoEvento =  new Eventos();
            $nuevoEvento->titulo = 'CORRECIÓN';
            $nuevoEvento->fecha = $valor->corrige_desde;
            $nuevoEvento->color = 'danger';
            $arrayEventos[4] = $nuevoEvento;

            $nuevoEvento =  new Eventos();
            $nuevoEvento->titulo = 'CORRECIÓN';
            $nuevoEvento->fecha = $valor->corrige_hasta;
            $nuevoEvento->color = 'danger';
            $arrayEventos[5] = $nuevoEvento;

            $nuevoEvento =  new Eventos();
            $nuevoEvento->titulo = 'REVISIÓN CORRECIÓN';
            $nuevoEvento->fecha = $valor->rev_correccion_desde;
            $nuevoEvento->color = 'primary';
            $arrayEventos[6] = $nuevoEvento;

            $nuevoEvento =  new Eventos();
            $nuevoEvento->titulo = 'REVISIÓN CORRECIÓN';
            $nuevoEvento->fecha = $valor->rev_correccion_hasta;
            $nuevoEvento->color = 'primary';
            $arrayEventos[7] = $nuevoEvento;
          }

      }



      $mes = $month;
      $daylast =  date("Y-m-d", strtotime("last day of ".$mes));
      $fecha      =  date("Y-m-d", strtotime("first day of ".$mes));
      $daysmonth  =  date("d", strtotime($fecha));
      $montmonth  =  date("m", strtotime($fecha));
      $yearmonth  =  date("Y", strtotime($fecha));
      $nuevaFecha = mktime(0,0,0,$montmonth,$daysmonth,$yearmonth);
      $diaDeLaSemana = date("w", $nuevaFecha);
      $nuevaFecha = $nuevaFecha - ($diaDeLaSemana*24*3600);
      $dateini = date ("Y-m-d",$nuevaFecha);
      $semana1 = date("W",strtotime($fecha));
      $semana2 = date("W",strtotime($daylast));
      if (date("m", strtotime($mes))==12) {
          $semana = 5;
      }
      else {
        $semana = ($semana2-$semana1)+1;
      }

      $datafecha = $dateini;
      $calendario = array();
      $iweek = 0;
      while ($iweek < $semana):
          $iweek++;
          $weekdata = [];
          for ($iday=0; $iday < 7 ; $iday++){

            $datafecha = date("Y-m-d",strtotime($datafecha."+ 1 day"));
            $datanew['mes'] = date("M", strtotime($datafecha));
            $datanew['dia'] = date("d", strtotime($datafecha));
            $datanew['fecha'] = $datafecha;


           /* foreach ($arrayEventos as $llave2 => $valor2) {
              if ($valor2->fecha  == $datafecha){
                echo 'entra'.$datafecha;
                $evento = [$valor2];

              }
            }*/
        //    dd($datafecha >=  $arrayEventos[0]->fechas && $datafecha <=  $arrayEventos[1]);
            if ($datafecha >=  $arrayEventos[0]->fecha && $datafecha <=  $arrayEventos[1]->fecha){
              $evento = [$arrayEventos[0]];
            }

            if ($datafecha >=  $arrayEventos[2]->fecha && $datafecha <=  $arrayEventos[3]->fecha){
              $evento = [$arrayEventos[2]];
            }

            if ($datafecha >=  $arrayEventos[4]->fecha && $datafecha <=  $arrayEventos[5]->fecha){
              $evento = [$arrayEventos[4]];
            }

            if ($datafecha >=  $arrayEventos[6]->fecha && $datafecha <=  $arrayEventos[7]->fecha){
              $evento = [$arrayEventos[6]];
            }

            $datanew['evento'] = $evento;//Evento::where("fecha",$datafecha)->get();
            $evento = [];
            array_push($weekdata,$datanew);
          }
          $dataweek['semana'] = $iweek;
          $dataweek['datos'] = $weekdata;
          array_push($calendario,$dataweek);

      endwhile;

      $nextmonth = date("Y-M",strtotime($mes."+ 1 month"));
      $lastmonth = date("Y-M",strtotime($mes."- 1 month"));
      $month = date("M",strtotime($mes));
      $yearmonth = date("Y",strtotime($mes));
      $data = array(
        'next' => $nextmonth,
        'month'=> $month,
        'year' => $yearmonth,
        'last' => $lastmonth,
        'calendar' => $calendario,
      );

      return $data;
    }
    public static function spanishMonth($month)
    {

        $mes = $month;
        if ($month=="Jan") {
          $mes = "Enero";
        }
        elseif ($month=="Feb")  {
          $mes = "Febrero";
        }
        elseif ($month=="Mar")  {
          $mes = "Marzo";
        }
        elseif ($month=="Apr") {
          $mes = "Abril";
        }
        elseif ($month=="May") {
          $mes = "Mayo";
        }
        elseif ($month=="Jun") {
          $mes = "Junio";
        }
        elseif ($month=="Jul") {
          $mes = "Julio";
        }
        elseif ($month=="Aug") {
          $mes = "Agosto";
        }
        elseif ($month=="Sep") {
          $mes = "Septiembre";
        }
        elseif ($month=="Oct") {
          $mes = "Octubre";
        }
        elseif ($month=="Oct") {
          $mes = "December";
        }
        elseif ($month=="Dec") {
          $mes = "Diciembre";
        }
        else {
          $mes = $month;
        }
        return $mes;
    }
    public function create(){
      $selectAno = [''=>'SELECCIONE AÑO','2020'=>'2020','2021'=>'2021','2022'=>'2022','2023'=>'2023','2024'=>'2024','2025'=>'2025'];
    //  $selectMes = [''=>'Seleccione mes','1'=>'Enero','2'=>'Febrero', '3'=>'Marzo' , '4'=>'Abril' , '5'=>'mayo' , '6'=>'junio' , '7'=>'Julio' , '8'=>'Agosto' , '9'=>'septiembre' , '10'=>'Octubre' , '11'=>'Noviembre' , '12'=>'Diciembre'];
      $selectMes[null] = 'SELECCIONE MES'; foreach (Meses::orderby('id', 'asc')->get() as $key1 => $value1) { $selectMes[$value1->id]=$value1->nombre;}
      $fechas[1] = Date('Y-m-d', strtotime( '2020-01-01' ));
      $fechas[0] = Date('Y-m-d', strtotime('2025-12-31'));
      $fe_desde =Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
      $fe_hasta =Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
        return view('sigc.sigc_calendario_create')
         ->with('fechas' , $fechas)
         ->with('fe_hasta' , $fe_hasta)
        ->with('fe_desde' , $fe_desde)
        ->with('selectMes' , $selectMes)
        ->with('selectAno' , $selectAno);
    }
    public function store(Request $request){

      $exist = CalendarioTabla::where('ano' , $request->ano)->where('mes' , $request->mes)->get();

        if(count($exist)>0){
          return redirect()->back()->withErrors(['Ya existe el registro para este mes y año']);
        }

        if (Date('Ymd', strtotime( $request->FechaReporteDesde )) > Date('Ymd', strtotime( $request->FechaReporteHasta )) || Date('Ymd', strtotime( $request->FechaReporteDesde )) > Date('Ymd', strtotime( $request->FechaRevisonDesde )) || Date('Ymd', strtotime( $request->FechaReporteDesde )) > Date('Ymd', strtotime( $request->FechaRevisonHasta )) ||  Date('Ymd', strtotime( $request->FechaReporteDesde )) > Date('Ymd', strtotime( $request->FechaCorreccionDesde )) ||  Date('Ymd', strtotime( $request->FechaReporteDesde )) > Date('Ymd', strtotime( $request->FechaCorreccionHasta )) ||  Date('Ymd', strtotime( $request->FechaReporteDesde )) > Date('Ymd', strtotime( $request->FechaRevisionCorreccionDesde )) ||  Date('Ymd', strtotime( $request->FechaReporteDesde )) > Date('Ymd', strtotime( $request->FechaRevisionCorreccionHasta )) ){
               return redirect()->back()->withErrors([' Error Fecha de Reporte desde Verifique']);
        }

        if ( Date('Ymd', strtotime( $request->FechaReporteHasta )) > Date('Ymd', strtotime( $request->FechaRevisonDesde )) || Date('Ymd', strtotime( $request->FechaReporteHasta )) > Date('Ymd', strtotime( $request->FechaRevisonHasta )) ||  Date('Ymd', strtotime( $request->FechaReporteHasta )) > Date('Ymd', strtotime( $request->FechaCorreccionDesde )) ||  Date('Ymd', strtotime( $request->FechaReporteHasta )) > Date('Ymd', strtotime( $request->FechaCorreccionHasta )) ||  Date('Ymd', strtotime( $request->FechaReporteHasta )) > Date('Ymd', strtotime( $request->FechaRevisionCorreccionDesde )) ||  Date('Ymd', strtotime( $request->FechaReporteHasta )) > Date('Ymd', strtotime( $request->FechaRevisionCorreccionHasta )) ){
              return redirect()->back()->withErrors([' Error Fecha de Reporte hasta Verifique']);
        }

        if ( Date('Ymd', strtotime( $request->FechaRevisonDesde )) > Date('Ymd', strtotime( $request->FechaRevisonHasta )) || Date('Ymd', strtotime( $request->FechaRevisonDesde )) > Date('Ymd', strtotime( $request->FechaCorreccionDesde )) ||  Date('Ymd', strtotime( $request->FechaRevisonDesde )) > Date('Ymd', strtotime( $request->FechaCorreccionHasta  )) ||   Date('Ymd', strtotime( $request->FechaRevisonDesde )) > Date('Ymd', strtotime( $request->FechaRevisionCorreccionDesde )) ||  Date('Ymd', strtotime( $request->FechaRevisonDesde )) > Date('Ymd', strtotime( $request->FechaRevisionCorreccionHasta )) ){
              return redirect()->back()->withErrors([' Error Fecha Revision desde Verifique']);
        }

        if ( Date('Ymd', strtotime( $request->FechaRevisonHasta )) > Date('Ymd', strtotime( $request->FechaCorreccionDesde ))  ||  Date('Ymd', strtotime( $request->FechaRevisonHasta )) > Date('Ymd', strtotime( $request->FechaCorreccionHasta ))  ||  Date('Ymd', strtotime( $request->FechaRevisonHasta )) > Date('Ymd', strtotime( $request->FechaRevisionCorreccionDesde ))  || Date('Ymd', strtotime( $request->FechaRevisonHasta )) > Date('Ymd', strtotime( $request->FechaRevisionCorreccionHasta )) ) {

             return redirect()->back()->withErrors([' Error Fecha Revision hasta Verifique']);

        }

        if ( Date('Ymd', strtotime( $request->FechaCorreccionDesde )) > Date('Ymd', strtotime( $request->FechaCorreccionHasta )) || Date('Ymd', strtotime( $request->FechaCorreccionDesde )) > Date('Ymd', strtotime( $request->FechaRevisionCorreccionDesde )) ||   Date('Ymd', strtotime( $request->FechaCorreccionDesde )) > Date('Ymd', strtotime( $request->FechaRevisionCorreccionHasta )) ) {
            return redirect()->back()->withErrors([' Error Fecha correccion desde  Verifique']);
        }

        if ( Date('Ymd', strtotime( $request->FechaCorreccionHasta )) > Date('Ymd', strtotime( $request->FechaRevisionCorreccionDesde )) || Date('Ymd', strtotime( $request->FechaCorreccionHasta )) > Date('Ymd', strtotime( $request->FechaRevisionCorreccionHasta )) ) {

            return redirect()->back()->withErrors([' Error Fecha correccion hasta Verifique']);
        }

        if ( Date('Ymd', strtotime( $request->FechaCorreccionHasta )) > Date('Ymd', strtotime( $request->FechaRevisionCorreccionDesde )) ){
            return redirect()->back()->withErrors([' Error Fecha revision correccion desde Verifique']);

        }

        $nuevoEvento = new CalendarioTabla();
        $nuevoEvento->ano =$request->ano;
        $nuevoEvento->mes =$request->mes;
        $nuevoEvento->rep_desde =Date('Ymd', strtotime( $request->FechaReporteDesde ));
        $nuevoEvento->rep_hasta =Date('Ymd', strtotime( $request->FechaReporteHasta ));
        $nuevoEvento->rev_desde =Date('Ymd', strtotime( $request->FechaRevisonDesde ));
        $nuevoEvento->rev_hasta =Date('Ymd', strtotime( $request->FechaRevisonHasta ));
        $nuevoEvento->corrige_desde =Date('Ymd', strtotime( $request->FechaCorreccionDesde ));
        $nuevoEvento->corrige_hasta =Date('Ymd', strtotime( $request->FechaCorreccionHasta ));
        $nuevoEvento->rev_correccion_desde =Date('Ymd', strtotime( $request->FechaRevisionCorreccionDesde ));
        $nuevoEvento->rev_correccion_hasta =Date('Ymd', strtotime( $request->FechaRevisionCorreccionHasta ));
        $nuevoEvento->save();


        $calendario_id = $nuevoEvento->id;
        $mesNombre = Meses::where('id', $request->mes)->first()->nombre;

        $title1 = 'Reportar';
        $description1 = 'Reportabilidad Mes de '.$mesNombre.' de '.$nuevoEvento->ano;
        $etapa_id1 = 1;

        $nuevoEvento1 = new Eventos();
        $nuevoEvento1->title = $title1;
        $nuevoEvento1->description = $description1;
        $nuevoEvento1->start_date = Date('Ymd', strtotime( $request->FechaReporteDesde ));
        $nuevoEvento1->end_date = Date('Ymd', strtotime( $request->FechaReporteHasta ));
        $nuevoEvento1->calendario_id = $calendario_id;
        $nuevoEvento1->etapa_id = $etapa_id1;
        $nuevoEvento1->save();

        $title2 = 'Revisión';
        $description2 = 'Revisión Mes de '.$mesNombre.' de '.$nuevoEvento->ano;
        $etapa_id2 = 2;

        $nuevoEvento2 = new Eventos();
        $nuevoEvento2->title = $title2;
        $nuevoEvento2->description = $description2;
        $nuevoEvento2->start_date = Date('Ymd', strtotime( $request->FechaRevisonDesde ));
        $nuevoEvento2->end_date = Date('Ymd', strtotime( $request->FechaRevisonHasta ));
        $nuevoEvento2->calendario_id = $calendario_id;
        $nuevoEvento2->etapa_id = $etapa_id2;
        $nuevoEvento2->save();

        $title3 = 'Corrección';
        $description3 = 'Corrección Mes de '.$mesNombre.' de '.$nuevoEvento->ano;
        $etapa_id3 = 3;

        $nuevoEvento3 = new Eventos();
        $nuevoEvento3->title = $title3;
        $nuevoEvento3->description = $description3;
        $nuevoEvento3->start_date = Date('Ymd', strtotime( $request->FechaCorreccionDesde ));
        $nuevoEvento3->end_date = Date('Ymd', strtotime( $request->FechaCorreccionHasta ));
        $nuevoEvento3->calendario_id = $calendario_id;
        $nuevoEvento3->etapa_id = $etapa_id3;
        $nuevoEvento3->save();

        $title4 = 'Revisión Corrección';
        $description4 = 'Revisión de Corrección Mes de '.$mesNombre.' de '.$nuevoEvento->ano;
        $etapa_id4 = 4;

        $nuevoEvento4 = new Eventos();
        $nuevoEvento4->title = $title4;
        $nuevoEvento4->description = $description4;
        $nuevoEvento4->start_date = Date('Ymd', strtotime( $request->FechaRevisionCorreccionDesde ));
        $nuevoEvento4->end_date = Date('Ymd', strtotime( $request->FechaRevisionCorreccionHasta ));
        $nuevoEvento4->calendario_id = $calendario_id;
        $nuevoEvento4->etapa_id = $etapa_id4;
        $nuevoEvento4->save();

        if ($nuevoEvento){
          $this->notificacionCrear($nuevoEvento);
        }

        alert()->success('!','evento creado exitosamente');
        return redirect()->route('sigc_calendario');

    }

     public function notificacionCrear($nuevoEvento){
       $emails = [Auth::user()->email,'daniela.jimenez@mindep.cl'];
       $datos = $nuevoEvento;

       foreach ($emails as $key => $correo) {
         Mail::to($correo)->send(new NotificacionCalendarioCrearEmail($datos));
         $notificacionesId = 1;
         $nuevoCorreo = new CorreoNotificaciones();
         $nuevoCorreo->id_notificaciones = $notificacionesId;
         $nuevoCorreo->destintario = $correo;
         $nuevoCorreo->save();
       }


     }

      public function edit($id){
      $editCalendario = CalendarioTabla::where('id' , $id)->first();
      $selectAno = [''=>'SELECCIONE AÑO','2020'=>'2020','2021'=>'2021','2022'=>'2022','2023'=>'2023','2024'=>'2024','2025'=>'2025'];
      //$selectMes = [''=>'Seleccione mes','1'=>'Enero','2'=>'Febrero', '3'=>'Marzo' , '4'=>'Abril' , '5'=>'mayo' , '6'=>'junio' , '7'=>'Julio' , '8'=>'Agosto' , '9'=>'septiembre' , '10'=>'Octubre' , '11'=>'Noviembre' , '12'=>'Diciembre'];
      $selectMes[null] = 'SELECCIONE MES'; foreach (Meses::orderby('id', 'asc')->get() as $key1 => $value1) { $selectMes[$value1->id]=$value1->nombre;}
      $fechas[1] = Date('Y-m-d', strtotime( '2020-01-01' ));
      $fechas[0] = Date('Y-m-d', strtotime('2025-12-31'));
      $fe_desde =Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
      $fe_hasta =Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
        return view('sigc.sigc_calendario_edit')
         ->with('editCalendario' , $editCalendario)
         ->with('fechas' , $fechas)
         ->with('fe_hasta' , $fe_hasta)
        ->with('fe_desde' , $fe_desde)
        ->with('selectMes' , $selectMes)
        ->with('selectAno' , $selectAno);
    }

    public function update (Request $request, $id){

      $exist = CalendarioTabla::where('id', '!=', $id)->where('ano' , $request->ano)->where('mes' , $request->mes)->get();

       if(count($exist)>0){
          return redirect()->back()->withErrors(['Ya existe el registro para este mes y año']);
        }

        if (Date('Ymd', strtotime( $request->FechaReporteDesde )) > Date('Ymd', strtotime( $request->FechaReporteHasta )) || Date('Ymd', strtotime( $request->FechaReporteDesde )) > Date('Ymd', strtotime( $request->FechaRevisonDesde )) || Date('Ymd', strtotime( $request->FechaReporteDesde )) > Date('Ymd', strtotime( $request->FechaRevisonHasta )) ||  Date('Ymd', strtotime( $request->FechaReporteDesde )) > Date('Ymd', strtotime( $request->FechaCorreccionDesde )) ||  Date('Ymd', strtotime( $request->FechaReporteDesde )) > Date('Ymd', strtotime( $request->FechaCorreccionHasta )) ||  Date('Ymd', strtotime( $request->FechaReporteDesde )) > Date('Ymd', strtotime( $request->FechaRevisionCorreccionDesde )) ||  Date('Ymd', strtotime( $request->FechaReporteDesde )) > Date('Ymd', strtotime( $request->FechaRevisionCorreccionHasta )) ){
               return redirect()->back()->withErrors([' Error Fecha de Reporte desde Verifique']);
        }

        if ( Date('Ymd', strtotime( $request->FechaReporteHasta )) > Date('Ymd', strtotime( $request->FechaRevisonDesde )) || Date('Ymd', strtotime( $request->FechaReporteHasta )) > Date('Ymd', strtotime( $request->FechaRevisonHasta )) ||  Date('Ymd', strtotime( $request->FechaReporteHasta )) > Date('Ymd', strtotime( $request->FechaCorreccionDesde )) ||  Date('Ymd', strtotime( $request->FechaReporteHasta )) > Date('Ymd', strtotime( $request->FechaCorreccionHasta )) ||  Date('Ymd', strtotime( $request->FechaReporteHasta )) > Date('Ymd', strtotime( $request->FechaRevisionCorreccionDesde )) ||  Date('Ymd', strtotime( $request->FechaReporteHasta )) > Date('Ymd', strtotime( $request->FechaRevisionCorreccionHasta )) ){
              return redirect()->back()->withErrors([' Error Fecha de Reporte hasta Verifique']);
        }

        if ( Date('Ymd', strtotime( $request->FechaRevisonDesde )) > Date('Ymd', strtotime( $request->FechaRevisonHasta )) || Date('Ymd', strtotime( $request->FechaRevisonDesde )) > Date('Ymd', strtotime( $request->FechaCorreccionDesde )) ||  Date('Ymd', strtotime( $request->FechaRevisonDesde )) > Date('Ymd', strtotime( $request->FechaCorreccionHasta  )) ||   Date('Ymd', strtotime( $request->FechaRevisonDesde )) > Date('Ymd', strtotime( $request->FechaRevisionCorreccionDesde )) ||  Date('Ymd', strtotime( $request->FechaRevisonDesde )) > Date('Ymd', strtotime( $request->FechaRevisionCorreccionHasta )) ){
              return redirect()->back()->withErrors([' Error Fecha Revision desde Verifique']);
        }

        if ( Date('Ymd', strtotime( $request->FechaRevisonHasta )) > Date('Ymd', strtotime( $request->FechaCorreccionDesde ))  ||  Date('Ymd', strtotime( $request->FechaRevisonHasta )) > Date('Ymd', strtotime( $request->FechaCorreccionHasta ))  ||  Date('Ymd', strtotime( $request->FechaRevisonHasta )) > Date('Ymd', strtotime( $request->FechaRevisionCorreccionDesde ))  || Date('Ymd', strtotime( $request->FechaRevisonHasta )) > Date('Ymd', strtotime( $request->FechaRevisionCorreccionHasta )) ) {

             return redirect()->back()->withErrors([' Error Fecha Revision hasta Verifique']);

        }

        if ( Date('Ymd', strtotime( $request->FechaCorreccionDesde )) > Date('Ymd', strtotime( $request->FechaCorreccionHasta )) || Date('Ymd', strtotime( $request->FechaCorreccionDesde )) > Date('Ymd', strtotime( $request->FechaRevisionCorreccionDesde )) ||   Date('Ymd', strtotime( $request->FechaCorreccionDesde )) > Date('Ymd', strtotime( $request->FechaRevisionCorreccionHasta )) ) {
            return redirect()->back()->withErrors([' Error Fecha correccion desde  Verifique']);
        }

        if ( Date('Ymd', strtotime( $request->FechaCorreccionHasta )) > Date('Ymd', strtotime( $request->FechaRevisionCorreccionDesde )) || Date('Ymd', strtotime( $request->FechaCorreccionHasta )) > Date('Ymd', strtotime( $request->FechaRevisionCorreccionHasta )) ) {

            return redirect()->back()->withErrors([' Error Fecha correccion hasta Verifique']);
        }

        if ( Date('Ymd', strtotime( $request->FechaCorreccionHasta )) > Date('Ymd', strtotime( $request->FechaRevisionCorreccionDesde )) ){
            return redirect()->back()->withErrors([' Error Fecha revision correccion desde Verifique']);

        }

        $nuevoEvento = CalendarioTabla::where('id' , $id)->first();
        $nuevoEvento->ano =$request->ano;
        $nuevoEvento->mes =$request->mes;
        $nuevoEvento->rep_desde =Date('Ymd', strtotime( $request->FechaReporteDesde ));
        $nuevoEvento->rep_hasta =Date('Ymd', strtotime( $request->FechaReporteHasta ));
        $nuevoEvento->rev_desde =Date('Ymd', strtotime( $request->FechaRevisonDesde ));
        $nuevoEvento->rev_hasta =Date('Ymd', strtotime( $request->FechaRevisonHasta ));
        $nuevoEvento->corrige_desde =Date('Ymd', strtotime( $request->FechaCorreccionDesde ));
        $nuevoEvento->corrige_hasta =Date('Ymd', strtotime( $request->FechaCorreccionHasta ));
        $nuevoEvento->rev_correccion_desde =Date('Ymd', strtotime( $request->FechaRevisionCorreccionDesde ));
        $nuevoEvento->rev_correccion_hasta =Date('Ymd', strtotime( $request->FechaRevisionCorreccionHasta ));
        $nuevoEvento->save();

        $calendario_id = $nuevoEvento->id;
        $mesNombre = Meses::where('id', $request->mes)->first()->nombre;

        $title1 = 'Reportar';
        $description1 = 'Reportabilidad Mes de '.$mesNombre.' de '.$nuevoEvento->ano;
        $etapa_id1 = 1;

        $nuevoEvento1 = Eventos::where('calendario_id' , $id)->where('etapa_id', 1)->first();
        $nuevoEvento1->start_date = Date('Ymd', strtotime( $request->FechaReporteDesde ));
        $nuevoEvento1->end_date = Date('Ymd', strtotime( $request->FechaReporteHasta ));
        $nuevoEvento1->save();

        $title2 = 'Revisión';
        $description2 = 'Revisión Mes de '.$mesNombre.' de '.$nuevoEvento->ano;
        $etapa_id2 = 2;

        $nuevoEvento2 = Eventos::where('calendario_id' , $id)->where('etapa_id', 2)->first();
        $nuevoEvento2->start_date = Date('Ymd', strtotime( $request->FechaRevisonDesde ));
        $nuevoEvento2->end_date = Date('Ymd', strtotime( $request->FechaRevisonHasta ));
        $nuevoEvento2->save();

        $title3 = 'Corrección';
        $description3 = 'Corrección Mes de '.$mesNombre.' de '.$nuevoEvento->ano;
        $etapa_id3 = 3;

        $nuevoEvento3 = Eventos::where('calendario_id' , $id)->where('etapa_id', 3)->first();
        $nuevoEvento3->start_date = Date('Ymd', strtotime( $request->FechaCorreccionDesde ));
        $nuevoEvento3->end_date = Date('Ymd', strtotime( $request->FechaCorreccionHasta ));
        $nuevoEvento3->save();

        $title4 = 'Revisión Corrección';
        $description4 = 'Revisión de Corrección Mes de '.$mesNombre.' de '.$nuevoEvento->ano;
        $etapa_id4 = 4;

        $nuevoEvento4 = Eventos::where('calendario_id' , $id)->where('etapa_id', 4)->first();
        $nuevoEvento4->start_date = Date('Ymd', strtotime( $request->FechaRevisionCorreccionDesde ));
        $nuevoEvento4->end_date = Date('Ymd', strtotime( $request->FechaRevisionCorreccionHasta ));
        $nuevoEvento4->save();

        if ($nuevoEvento){
          $this->notificacionEditar($nuevoEvento);
        }

        alert()->success('!','Evento editado exitosamente');
        return redirect()->route('sigc_calendario');

    }

    public function notificacionEditar($nuevoEvento2){
      $emails = [Auth::user()->email,'daniela.jimenez@mindep.cl'];
      $datos = $nuevoEvento2;

      foreach ($emails as $key => $correo) {
        Mail::to($correo)->send(new NotificacionCalendarioEditarEmail($datos));
        $notificacionesId = 2;
        $nuevoCorreo = new CorreoNotificaciones();
        $nuevoCorreo->id_notificaciones = $notificacionesId;
        $nuevoCorreo->destintario = $correo;
        $nuevoCorreo->save();
      }
      }




    public function notificacionEventos(){
      $hoy = Carbon::now()->format('Y-m-d');
      $hoy = $hoy.' 00:00:00';
      $hoy2 = Carbon::now()->format('Y-m-d');
      $eventos = Eventos::where('start_date' , $hoy)->get();

    if ($eventos->count()>0) {
      foreach ($eventos as $key => $evento) {
      $datos = ['evento'=>$evento];
      $mesActual  =  date('n');
      $anoActual = date("Y");
      if($mesActual >= 2 ){
        $mesConsulta = $mesActual -1 ;
      }else{
        $mesConsulta = 12;
      }

      if ($mesConsulta == 12 ){
         $anoConsulta = $anoActual - 1;
      }else{
        $anoConsulta = $anoActual;
      }

      $planificacionJ = 'daniela.jimenez@mindep.cl';
      Mail::to($planificacionJ)->send(new NotificacionEventos($datos));

      $usuariosRep = Indicadores::where('reportador_id', '!=', null)->where('ano', $anoConsulta)->get();
      foreach ($usuariosRep as $key => $value) {
        $usuarioReportador = Users::where('id', $value->reportador_id)->first()->email;
        $consultaEnvioEvento = CorreoNotificaciones::where('destintario' , $usuarioReportador)->where(DB::raw(' SUBSTRING(created_at, 1 , 10 )'),$hoy2)->get();

        if(count($consultaEnvioEvento)==0){

          sleep(1);
          Mail::to($usuarioReportador)->send(new NotificacionEventos($datos));
          $notificacionesId = 3;
          $nuevoCorreo = new CorreoNotificaciones();
          $nuevoCorreo->id_notificaciones = $notificacionesId;
          $nuevoCorreo->destintario = $usuarioReportador;
          $nuevoCorreo->save();
        }
      }


      $usuariosRepSup = Indicadores::where('reportadores_id', '!=', null)->where('ano', $anoConsulta)->get();
      foreach ($usuariosRepSup as $key => $value) {
        $usuarioReportadorSup = Users::where('id', $value->reportadores_id)->first()->email;
        $consultaEnvioEvento = CorreoNotificaciones::where('destintario' , $usuarioReportadorSup)->where(DB::raw(' SUBSTRING(created_at, 1 , 10 )'),$hoy2)->get();
        if(count($consultaEnvioEvento)==0){

        sleep(1);
        Mail::to($usuarioReportadorSup)->send(new NotificacionEventos($datos));
        $notificacionesId = 3;
        $nuevoCorreo1 = new CorreoNotificaciones();
        $nuevoCorreo1->id_notificaciones = $notificacionesId;
        $nuevoCorreo1->destintario = $usuarioReportadorSup;
        $nuevoCorreo1->save();
      }
      }


      $usuariosRev = Indicadores::where('revisor_id', '!=', null)->where('ano', $anoConsulta)->get();

      foreach ($usuariosRev as $key => $value) {
        $usuarioRevisor = Users::where('id', $value->revisor_id)->first()->email;
        $consultaEnvioEvento = CorreoNotificaciones::where('destintario' , $usuarioRevisor)->where(DB::raw(' SUBSTRING(created_at, 1 , 10 )'),$hoy2)->get();
        if(count($consultaEnvioEvento)==0){

        sleep(1);
        Mail::to($usuarioRevisor)->send(new NotificacionEventos($datos));
        $notificacionesId = 3;
        $nuevoCorreo2 = new CorreoNotificaciones();
        $nuevoCorreo2->id_notificaciones = $notificacionesId;
        $nuevoCorreo2->destintario = $usuarioRevisor;
        $nuevoCorreo2->save();
      }
      }


      $usuariosRevSup = Indicadores::where('responsable_id', '!=', null)->where('ano', $anoConsulta)->get();
      foreach ($usuariosRevSup as $key => $value) {
        $usuarioRevisorSup = Users::where('id', $value->responsable_id)->first()->email;
        $consultaEnvioEvento = CorreoNotificaciones::where('destintario' , $usuarioRevisorSup)->where(DB::raw(' SUBSTRING(created_at, 1 , 10 )'),$hoy2)->get();

        if(count($consultaEnvioEvento)==0){
          
        sleep(1);
        Mail::to($usuarioRevisorSup)->send(new NotificacionEventos($datos));
        $notificacionesId = 3;
        $nuevoCorreo3 = new CorreoNotificaciones();
        $nuevoCorreo3->id_notificaciones = $notificacionesId;
        $nuevoCorreo3->destintario = $usuarioRevisorSup;
        $nuevoCorreo3->save();
      }
      }

     }
   }
    }

}
