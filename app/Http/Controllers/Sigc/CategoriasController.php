<?php

namespace App\Http\Controllers\Sigc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Sigc\Categorias;
use App\Models\Sigc\CategoriasTabla;
use App\Http\Requests\SIGC\CategoriasRequest;

class CategoriasController extends Controller
{

    public function index(Request $request)
    {
      //dd($request->nombre);
      /*  set_time_limit(14200);
        $dataDefinitiva = [];
        $dataFiltrada = [];


        $client = new Client();

            try {
              $request = $client->get("http://10.100.23.13/webserviceautenticateactived/public/api/Get/Categorias" , ['verify' => false]);
              $response = $request->getBody()->getContents();
              $data =  json_decode($response);
            }catch (BadResponseException   $e) {
              $response = $e->getResponse();
          }



//dd($data2);
       foreach ($data as $key => $value) {

        $objeto = new Categorias();
        $objeto->id = $value->id;
        $objeto->nombre = $value->nombre;
        $dataDefinitiva[$key] = $objeto;
       }   */

       $dataDefinitiva = [];

       $requestnombreCategoria = $request->nombre;

       $dataDefinitiva = Categorias::get();


      return view('sigc.sigc_categorias')
      ->with('dataDefinitiva' , $dataDefinitiva)
      ->with('requestnombreCategoria' , $requestnombreCategoria);
    }

    public function create()
    {
      return view('sigc.sigc_crear_categoria');
    }

    public function store(CategoriasRequest $request)
    {
      $nombre = $request->nombre;
      $nuevaCategoria = new CategoriasTabla();
      $nuevaCategoria->nombre = $nombre;
      $nuevaCategoria->save();
      alert()->success('!','Categoría creada exitosamente');
      return redirect()->route('sigc_categorias');
    }

    public function edit($id)
    {
      $editCategoria = CategoriasTabla::where('id' , $id)->first();
      return view('sigc.sigc_editar_categoria')
      ->with('editCategoria' , $editCategoria);
    }

    public function update(CategoriasRequest $request, $id)
    {
      $nombre = $request->nombre;
      $nuevaCategoria = CategoriasTabla::where('id' , $id)->first();
      $nuevaCategoria->nombre = $nombre;
      $nuevaCategoria->save();
      alert()->success('!','Categoría editada exitosamente');
      return redirect()->route('sigc_categorias');
    }


}
