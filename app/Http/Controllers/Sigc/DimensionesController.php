<?php

namespace App\Http\Controllers\Sigc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Sigc\Dimensiones;
use App\Models\Sigc\DimensionesTabla;
use App\Http\Requests\SIGC\DimensionesRequest;

class DimensionesController extends Controller
{

    public function index(Request $request)
    {
      //dd($request->nombre);
        /*  set_time_limit(14200);
        $dataDefinitiva = [];
        $dataFiltrada = [];


        $client = new Client();

            try {
              $request = $client->get("http://10.100.23.13/webserviceautenticateactived/public/api/Get/Dimensiones" , ['verify' => false]);
              $response = $request->getBody()->getContents();
              $data =  json_decode($response);
            }catch (BadResponseException   $e) {
              $response = $e->getResponse();
          }



//dd($data2);
       foreach ($data as $key => $value) {

        $objeto = new Dimensiones();
        $objeto->id = $value->id;
        $objeto->nombre = $value->nombre;
        $dataDefinitiva[$key] = $objeto;
       }  */

       $dataDefinitiva = [];

       $requestnombreDimension = $request->nombre;

       $dataDefinitiva = Dimensiones::get();

      return view('sigc.sigc_dimensiones')
      ->with('dataDefinitiva' , $dataDefinitiva)
      ->with('requestnombreDimension' , $requestnombreDimension);
    }

     public function create()
    {
      return view('sigc.sigc_crear_dimension');
    }

    public function store(DimensionesRequest $request)
    {
      $nombre = $request->nombre;
      $nuevaDimension = new DimensionesTabla();
      $nuevaDimension->nombre = $nombre;
      $nuevaDimension->save();
      alert()->success('!','Dimensión creada exitosamente');
      return redirect()->route('sigc_dimensiones');
    }

    public function edit($id)
   {
     $editDimension = DimensionesTabla::where('id' , $id)->first();
     return view('sigc.sigc_editar_dimension')
     ->with('editDimension' , $editDimension);
   }

   public function update(DimensionesRequest $request, $id)
   {
     $nombre = $request->nombre;
     $nuevaDimension = DimensionesTabla::where('id' , $id)->first();
     $nuevaDimension->nombre = $nombre;
     $nuevaDimension->save();
     alert()->success('!','Dimensión editada exitosamente');
     return redirect()->route('sigc_dimensiones');
   }


}
