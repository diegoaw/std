<?php

namespace App\Http\Controllers\Sigc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Sigc\DocumentoTipos;
use App\Http\Requests\SIGC\DocumentoTiposRequest;

class DocumentoTiposController extends Controller
{

    public function index(Request $request)
          {
            $dataDefinitiva = [];
            $dataDefinitiva = DocumentoTipos::orderby('ano' , 'desc')->get();
            return view('sigc.sigc_documento_tipos')
            ->with('dataDefinitiva' , $dataDefinitiva);
          }

    public function create()
          {
            $selectAno = [''=>'Seleccione Año','2020'=>'2020','2021'=>'2021'];
            return view('sigc.sigc_crear_documento_tipo')
            ->with('selectAno' , $selectAno);
          }

    public function store(DocumentoTiposRequest $request)
         {
           $nombre = $request->nombre;
           $ano = $request->ano;
           $nuevoDocumentoTipo = new DocumentoTipos();
           $nuevoDocumentoTipo->nombre = $nombre;
           $nuevoDocumentoTipo->ano = $ano;
           $nuevoDocumentoTipo->save();
           alert()->success('!','Tipo Documento creado exitosamente');
           return redirect()->route('sigc_documento_tipos');
         }

    public function edit($id)
         {
           $selectAno = [''=>'Seleccione Año','2020'=>'2020','2021'=>'2021'];
           $editDocumentoTipos = DocumentoTipos::where('id' , $id)->first();
           return view('sigc.sigc_editar_documento_tipo')
           ->with('selectAno' , $selectAno)
           ->with('editDocumentoTipos' , $editDocumentoTipos);
         }

    public function update(DocumentoTiposRequest $request, $id)
         {
           $nombre = $request->nombre;
           $ano = $request->ano;
           $nuevoDocumentoTipo = DocumentoTipos::where('id' , $id)->first();
           $nuevoDocumentoTipo->nombre = $nombre;
           $nuevoDocumentoTipo->ano = $ano;
           $nuevoDocumentoTipo->save();
           alert()->success('!','Tipo Documento editado exitosamente');
           return redirect()->route('sigc_documento_tipos');
         }

}
