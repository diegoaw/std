<?php

namespace App\Http\Controllers\Sigc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Sigc\Documentos;
use App\Models\Sigc\DocumentoTipos;

class DocumentosController extends Controller
{

    public function index(Request $request)
          {
            $dataDefinitiva = [];
            $dataDefinitiva = Documentos::orderby('ano' , 'desc')->get();
            return view('sigc.sigc_documentos')
            ->with('dataDefinitiva' , $dataDefinitiva);
          }

    public function create()
          {
            $selectAno = [''=>'Seleccione Año','2020'=>'2020','2021'=>'2021'];
            $selectTipo[null] = 'SELECCIONE TIPO'; foreach (DocumentoTipos::where('ano' ,'>=' , date("Y"))->orderby('id', 'desc')->get() as $key3 => $value3) { $selectTipo[$value3->id]=$value3->nombrea;}
            return view('sigc.sigc_crear_documento')
            ->with('selectAno' , $selectAno)
            ->with('selectTipo' , $selectTipo);
          }

    public function store(DocumentosRequest $request)
         {
           $nombre = $request->nombre;
           $tipoDocumento = $request->documento_tipo_id;
           $ano = $request->ano;
           $nuevoDocumento = new Documentos();
           $nuevoDocumento->nombre = $nombre;
           $nuevoDocumento->documento_tipo_id = $tipoDocumento;
           $nuevoDocumento->ano = $ano;
           $nuevoDocumento->save();
           alert()->success('!','Documento creado exitosamente');
           return redirect()->route('sigc_documentos');
         }

    public function edit($id)
         {
           $selectAno = [''=>'Seleccione Año','2020'=>'2020','2021'=>'2021'];
           $selectTipo[null] = 'SELECCIONE TIPO'; foreach (DocumentoTipos::where('ano' ,'>=' , date("Y"))->orderby('id', 'desc')->get() as $key3 => $value3) { $selectTipo[$value3->id]=$value3->nombrea;}
           $editDocumentos = Documentos::where('id' , $id)->first();
           return view('sigc.sigc_editar_documento')
           ->with('selectAno' , $selectAno)
           ->with('selectTipo' , $selectTipo)
           ->with('editDocumentos' , $editDocumentos);
         }

    public function update(DocumentosRequest $request, $id)
         {
           $nombre = $request->nombre;
           $tipoDocumento = $request->documento_tipo_id;
           $ano = $request->ano;
           $nuevoDocumento = Documentos::where('id' , $id)->first();
           $nuevoDocumento->nombre = $nombre;
           $nuevoDocumento->documento_tipo_id = $tipoDocumento;
           $nuevoDocumento->ano = $ano;
           $nuevoDocumento->save();
           alert()->success('!','Documento editado exitosamente');
           return redirect()->route('sigc_documentos');
         }

}
