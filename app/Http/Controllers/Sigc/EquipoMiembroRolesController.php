<?php

namespace App\Http\Controllers\Sigc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Sigc\EquipoMiembroRoles;
use App\Http\Requests\SIGC\EquipoMiembroRolesRequest;

class EquipoMiembroRolesController extends Controller
{

    public function index(Request $request)
    {
       $dataDefinitiva = [];
       $requestnombreEquipoMiembroRoles = $request->nombre;
       $dataDefinitiva = EquipoMiembroRoles::get();

      return view('sigc.sigc_equipo_miembro_roles')
      ->with('dataDefinitiva' , $dataDefinitiva)
      ->with('requestnombreEquipoMiembroRoles' , $requestnombreEquipoMiembroRoles);
    }


    public function create()
    {
      return view('sigc.sigc_crear_equipo_miembro_roles');
    }


     public function store(EquipoMiembroRolesRequest $request)
    {
      $nombre = $request->nombre;
      $nuevoEquipoMiembroRol = new EquipoMiembroRoles();
      $nuevoEquipoMiembroRol->nombre = $nombre;
      $nuevoEquipoMiembroRol->save();
      alert()->success('!','Equipo Miembro Rol creado exitosamente');
      return redirect()->route('sigc_equipo_miembro_roles');
    }

    public function edit($id)
    {
      $editEquipoMiembroRol = EquipoMiembroRoles::where('id' , $id)->first();
      return view('sigc.sigc_editar_equipo_miembro_roles')
      ->with('editEquipoMiembroRol' , $editEquipoMiembroRol);
    }


     public function update(EquipoMiembroRolesRequest $request, $id)
    {
      $nombre = $request->nombre;
      $nuevoEquipoMiembroRol = EquipoMiembroRoles::where('id' , $id)->first();
      $nuevoEquipoMiembroRol->nombre = $nombre;
      $nuevoEquipoMiembroRol->save();
      alert()->success('!','Equipo Miembro Rol editado exitosamente');
      return redirect()->route('sigc_equipo_miembro_roles');
    }


}
