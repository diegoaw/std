<?php

namespace App\Http\Controllers\Sigc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Sigc\Equipos;
use App\Models\Sigc\EquipoMiembroRoles;
use App\Models\Sigc\EquipoMiembros;
use App\Models\Sigc\Users;
use App\Models\Sigc\EquiposTabla;
use App\Http\Requests\SIGC\EquiposRequest;

class EquiposController extends Controller
{

    public function index(Request $request)
    {
       $dataDefinitiva = [];

       $requestnombreEquipo = $request->nombre;
       $requestanoEquipo = $request->ano;
       $dataDefinitiva = Equipos::orderby('ano' , 'desc')->get();

      return view('sigc.sigc_equipos')
      ->with('dataDefinitiva' , $dataDefinitiva)
      ->with('requestnombreEquipo' , $requestnombreEquipo)
      ->with('requestanoEquipo' , $requestanoEquipo);
    }

    public function create()
    {
      $selectAno = [''=>'Seleccione Año','2021'=>'2021','2022'=>'2022'];

      return view('sigc.sigc_crear_equipo')
      ->with('selectAno' , $selectAno);
    }

    public function store(EquiposRequest $request)
    {
      $nombre = $request->nombre;
      $ano = $request->ano;
      $nuevoEquipo = new EquiposTabla();
      $nuevoEquipo->nombre = $nombre;
      $nuevoEquipo->ano = $ano;
      $nuevoEquipo->save();
      alert()->success('!','Equipo creado exitosamente');
      return redirect()->route('sigc_equipos');
    }

    public function edit($id)
    {
      $selectAno = [''=>'Seleccione Año','2021'=>'2021','2022'=>'2022'];
      $editEquipo = EquiposTabla::where('id' , $id)->first();
      return view('sigc.sigc_editar_equipo')
      ->with('selectAno' , $selectAno)
      ->with('editEquipo' , $editEquipo);
    }

    public function update(EquiposRequest $request, $id)
    {
      $nombre = $request->nombre;
      $ano = $request->ano;
      $nuevoEquipo = EquiposTabla::where('id' , $id)->first();
      $nuevoEquipo->nombre = $nombre;
      $nuevoEquipo->ano = $ano;
      $nuevoEquipo->save();
      alert()->success('!','Equipo editado exitosamente');
      return redirect()->route('sigc_equipos');
    }

    public function agregarmiembro($id)
    {
      $selectUser[null] = 'SELECCIONE USUARIO'; foreach (Users::orderby('id', 'asc')->get() as $key1 => $value1) { $selectUser[$value1->id]=$value1->nombrec;}
      $selectRol[null] = 'SELECCIONE ROL'; foreach (EquipoMiembroRoles::orderby('id', 'asc')->get() as $key1 => $value1) { $selectRol[$value1->id]=$value1->nombre;}
      $equipo = EquiposTabla::where('id' , $id)->first();
      return view('sigc.sigc_agregar_miembro')
      ->with('equipo', $equipo)
      ->with('selectUser', $selectUser)
      ->with('selectRol', $selectRol);
    }

    public function guardarmiembro( Request $request, $id)
    {
      $user = $request->user;
      $rol = $request->rol;
      $estado = 1;
      $nuevoMiembro = new EquipoMiembros();
      $nuevoMiembro->equipo_id = $id;
      $nuevoMiembro->user_id = $user;
      $nuevoMiembro->equipo_miembro_rol_id = $rol;
      $nuevoMiembro->estado = $estado;
      $nuevoMiembro->save();
      alert()->success('!','Miembro agregado exitosamente');
      return redirect()->route('sigc_equipos');
    }

}
