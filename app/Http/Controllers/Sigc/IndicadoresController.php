<?php

namespace App\Http\Controllers\Sigc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Sigc\Indicadores;
use App\Models\Sigc\Tipos;
use App\Models\Sigc\Categorias;
use App\Models\Sigc\Unidades;
use App\Models\Sigc\Productos;
use App\Models\Sigc\Ambitos;
use App\Models\Sigc\Dimensiones;
use App\Models\Sigc\Divisiones;
use App\Models\Sigc\Equipos;
use App\Models\Sigc\Users;
use App\Http\Requests\SIGC\IndicadoresRequest;
use App\Models\Sigc\Calendario;
use App\Models\Sigc\Mediciones;
use App\Mail\NotificacionNoReportoEmail;
use Illuminate\Support\Facades\Auth;
use App\Models\Sigc\CorreoNotificaciones;
use Mail;


class IndicadoresController extends Controller
{

    public function index(Request $request)
    {

        set_time_limit(14200);
        $dataDefinitiva = [];

        $tipoView = $request->tipo;
        $equipoView = $request->equipo;
        $requestnombreIndicador = $request->nombre;
        $anoView = $request->ano? $request->ano: date("Y");
        $divisionView = $request->divisionId;
        $indicadorView = $request->indicador;

        $selectAno[null] = 'SELECCIONE AÑO'; foreach (Indicadores::whereNotNull('ano')->orderby('ano', 'desc')->distinct()->get() as $key0 => $value0) { $selectAno[$value0->ano]=$value0->ano;}
        $selectTipo[null] = 'SELECCIONE TIPO'; foreach (Tipos::orderby('ano', 'desc')->get() as $key1 => $value1) { $selectTipo[$value1->id]=$value1->aliasc;}
        $selectEquipo[null] = 'SELECCIONE EQUIPO'; foreach (Equipos::orderby('ano', 'desc')->get() as $key2 => $value2) { $selectEquipo[$value2->id]=$value2->nombrecompleto;}
        $selectDivision[null] = 'SELECCIONE DIVISIÓN'; foreach (Divisiones::orderby('id', 'asc')->get() as $key7 => $value7) { $selectDivision[$value7->id]=$value7->nombre;}        
        $selectIndicador[null] = "SELECCIONE INDICADOR"; foreach (Indicadores::orderby('id', 'desc')->get() as $llave => $value8) {  $selectIndicador[$value8->id] = $value8->NombreCompleto;}
        //$selectIndicador[null] = "SELECCIONE INDICADOR"; foreach (Mediciones::orderby('indicador_id', 'desc')->get() as $llave => $valor) {  $selectIndicador[$valor->indicador_id] = $valor->Nombrecompletos;}


          $query = Indicadores::query();
          $query->where('ano' , '>=', (date("Y")-1));
         
          $query->when($tipoView, function ($q, $tipoView) {
                return $q->where('tipo_id', $tipoView);
          });
          $query->when($equipoView, function ($q, $equipoView) {
                return $q->where('equipo_id', $equipoView);
          });
          $query->when($indicadorView, function ($q, $indicadorView) {
            return $q->where('id', $indicadorView);
          });
          ;
          $query->when($divisionView, function ($q, $divisionView) {
                return $q->where('division_id', $divisionView);
          });
          $query->orderby('id', 'desc');
          $dataDefinitiva =  $query->get();
         
          //dd($indicadorView);
          if(!($anoView == date("Y"))){
            $query1 = Indicadores::query();
            $query1->when($anoView, function ($q, $anoView) {
                  return $q->where('ano', $anoView);
            });
          
            $query1->when($tipoView, function ($q, $tipoView) {
                  return $q->where('tipo_id', $tipoView);
            });
            $query1->when($equipoView, function ($q, $equipoView) {
                  return $q->where('equipo_id', $equipoView);
            });
            $query1->when($indicadorView, function ($q, $indicadorView) {
              return $q->where('id', $indicadorView);
            });
            $query1->when($divisionView, function ($q, $divisionView) {
                  return $q->where('division_id', $divisionView);
            });
            $query1->orderby('id', 'desc');
            $dataDefinitivaO =  $query1->paginate(25);
          }else{
            $dataDefinitivaO = [];
          }


      return view('sigc.sigc_indicadores')
      ->with('dataDefinitiva' , $dataDefinitiva)
      ->with('dataDefinitivaO' , $dataDefinitivaO)
      ->with('selectTipo' , $selectTipo)
      ->with('selectDivision' , $selectDivision)
      ->with('selectAno' , $selectAno)
      ->with('tipoView' , $tipoView)
      ->with('anoView' , $anoView)
      ->with('selectEquipo' , $selectEquipo)
      ->with('equipoView' , $equipoView)
      ->with('divisionView' , $divisionView)
      ->with('indicadorView' , $indicadorView)
      ->with('selectIndicador' , $selectIndicador);
    }

   public function create (){
      $varNull = null;
      $selectCategoria[null] = 'SELECCIONE CATEGORIA'; foreach (Categorias::orderby('id', 'desc')->get() as $key0 => $value0) { $selectCategoria[$value0->id]=$value0->nombre;}
      $selectUnidad[null] = 'SELECCIONE UNIDAD'; foreach (Unidades::orderby('id', 'desc')->get() as $key1 => $value1) { $selectUnidad[$value1->id]=$value1->nombre;}
      $selectAno = [null=>'SELECCIONE AÑO' , '2021'=> 2021, '2022'=> 2022];
      $selectTipo[null] = 'SELECCIONE TIPO'; foreach (Tipos::where('ano' ,'>=' , date("Y"))->orderby('id', 'desc')->get() as $key3 => $value3) { $selectTipo[$value3->id]=$value3->aliasc;}
      $selectProducto[null] = 'SELECCIONE PRODUCTO'; foreach (Productos::where('ano' ,'>=' , date("Y"))->orderby('id', 'desc')->get() as $key4 => $value4) { $selectProducto[$value4->id]=$value4->nombre;}
      $selectAmbito[null] = 'SELECCIONE AMBITO'; foreach (Ambitos::orderby('id', 'desc')->get() as $key5 => $value5) { $selectAmbito[$value5->id]=$value5->nombre;}
      $selectDimension[null] = 'SELECCIONE DIMENSION'; foreach (Dimensiones::orderby('id', 'desc')->get() as $key6 => $value6) { $selectDimension[$value6->id]=$value6->nombre;}
      $selectEquipo[null] = 'SELECCIONE EQUIPO'; foreach (Equipos::where('ano' ,'>=' , date("Y"))->orderby('id', 'desc')->get() as $key5 => $value5) { $selectEquipo[$value5->id]=$value5->nombrecompleto;}
      $selectRut[null] = 'SELECCIONE USUARIO2'; foreach (Users::where('persona_id', '!=', null)->where('estado', 1)->orderby('nombres', 'asc')->get() as $key6 => $value6) { $selectRut[$value6->persona_id]=$value6->rut;}
      $selectUsuariosid[null] = 'SELECCIONE USUARIO'; foreach (Users::where('id', '!=', null)->where('estado', 1)->orderby('nombres', 'asc')->get() as $key6 => $value6) { $selectUsuariosid[$value6->id]=$value6->usuariosid;}
      $selectDivision[null] = 'SELECCIONE DIVISIÓN'; foreach (Divisiones::where('id', '!=', null)->orderby('id', 'asc')->get() as $key7 => $value7) { $selectDivision[$value7->id]=$value7->nombre;}
      $selectFormula[null] = 'SELECCIONE FORMULA DEL INDICADOR'; $selectFormula['$numerador/$denominador']='$numerador/$denominador'; $selectFormula['($numerador/$denominador)*100']='($numerador/$denominador)*100';



      return view('sigc.sigc_crear_indicadores')
      ->with('selectCategoria' , $selectCategoria)
      ->with('varNull' , $varNull)
      ->with('selectUnidad' , $selectUnidad)
      ->with('selectAno' , $selectAno)
      ->with('selectTipo' , $selectTipo)
      ->with('selectProducto' , $selectProducto)
      ->with('selectAmbito' , $selectAmbito)
      ->with('selectDimension' , $selectDimension)
      ->with('selectEquipo' , $selectEquipo)
      ->with('selectRut' , $selectRut)
      ->with('selectFormula' , $selectFormula)
      ->with('selectUsuariosid' , $selectUsuariosid)
      ->with('selectDivision' , $selectDivision);
   }

   public function store (IndicadoresRequest $request){


      $ano = $request->ano;
      $nombreIndicador = $request->nombre;
      $numeradorIndicador = $request->numerador;
      $categoriaIndicador = $request->categoria;
        if($categoriaIndicador == 1 ){
          if(!$request->denominador){
            return redirect()->back()->withErrors(['Categoria fija es requerido el denominador']);
          }
        $denominadorIndicador = $request->denominador;
      }
      $multiplicadorIndicador = $request->multiplicador;
      $ponderadorIndicador = $request->ponderador;
      $metaIndicador = $request->meta;
      $unidadIndicador = $request->unidad;
      $medioVerificacionIndicador = $request->medio_verificacion;
      $notasIndicador = $request->notas;
      $productoIndicador = $request->producto;
      $tipoIndicador = $request->tipo;
      $divisionIndicador = $request->divisionId;
      $ambitoIndicador = $request->ambito;
      $dimensionIndicador = $request->dimension;
      $equipoIndicador = $request->equipo;
      $reportertId = $request->reportertId;
      $reportersId = $request->reportersId;
      $responsabletId = $request->responsabletId;
      $revisorId = $request->revisorId;
      $denominadorIndicador =  $request->denominador;
      $fomulaIndicador = $request->formula;

      $nuevoIndicador = new Indicadores ();
      $nuevoIndicador->ano = $ano;
      $nuevoIndicador->nombre = $nombreIndicador;
      $nuevoIndicador->numerador = $numeradorIndicador;
      if($categoriaIndicador == 1){
        $nuevoIndicador->denominador =  $denominadorIndicador;
      }
      $nuevoIndicador->multiplicador =  $multiplicadorIndicador;
      $nuevoIndicador->ponderador =  $ponderadorIndicador;
      $nuevoIndicador->meta = $metaIndicador;
      $nuevoIndicador->formula = $fomulaIndicador;
      $nuevoIndicador->unidad_id = $unidadIndicador;
      $nuevoIndicador->medio_verif = $medioVerificacionIndicador;
      $nuevoIndicador->notas =  $notasIndicador ;
      $nuevoIndicador->producto_id =  $productoIndicador;
      $nuevoIndicador->ambito_id =  $ambitoIndicador;
      $nuevoIndicador->dimension_id =  $dimensionIndicador;
      $nuevoIndicador->tipo_id = $tipoIndicador;
      $nuevoIndicador->division_id = $divisionIndicador;
      $nuevoIndicador->equipo_id =  $equipoIndicador;
      $nuevoIndicador->categoria_id = $categoriaIndicador;

      $nuevoIndicador->reportador_id = $reportertId;
      $nuevoIndicador->reportadores_id = $reportersId; /* suplenete del reportador*/

      $nuevoIndicador->revisor_id = $revisorId;
      $nuevoIndicador->responsable_id = $responsabletId; /* suplenete del revisor*/

      $nuevoIndicador->save();

      alert()->success('!','Indicador creado exitosamente');
      return redirect()->route('sigc_indicadores');
   }


   public function edit(Request $request , $id){

      $indicador = Indicadores::where('id' , $id)->first();

      $selectCategoria[null] = 'SELECCIONE CATEGORIA'; foreach (Categorias::orderby('id', 'desc')->get() as $key0 => $value0) { $selectCategoria[$value0->id]=$value0->nombre;}
      $selectUnidad[null] = 'SELECCIONE UNIDAD'; foreach (Unidades::orderby('id', 'desc')->get() as $key1 => $value1) { $selectUnidad[$value1->id]=$value1->nombre;}
      $selectAno = [null=>'SELECCIONE AÑO' , '2021'=> 2021, '2022'=> 2022];
      $selectTipo[null] = 'SELECCIONE TIPO'; foreach (Tipos::where('ano' ,'>=' , date("Y"))->orderby('id', 'desc')->get() as $key3 => $value3) { $selectTipo[$value3->id]=$value3->aliasc;}
      $selectProducto[null] = 'SELECCIONE PRODUCTO'; foreach (Productos::where('ano' ,'>=' , date("Y"))->orderby('id', 'desc')->get() as $key4 => $value4) { $selectProducto[$value4->id]=$value4->nombre;}
      $selectAmbito[null] = 'SELECCIONE AMBITO'; foreach (Ambitos::orderby('id', 'desc')->get() as $key5 => $value5) { $selectAmbito[$value5->id]=$value5->nombre;}
      $selectDimension[null] = 'SELECCIONE DIMENSION'; foreach (Dimensiones::orderby('id', 'desc')->get() as $key6 => $value6) { $selectDimension[$value6->id]=$value6->nombre;}
      $selectEquipo[null] = 'SELECCIONE EQUIPO'; foreach (Equipos::where('ano' ,'>=' , date("Y"))->orderby('id', 'desc')->get() as $key5 => $value5) { $selectEquipo[$value5->id]=$value5->nombrecompleto;}
      $selectRut[null] = 'SELECCIONE USUARIO'; foreach (Users::where('persona_id', '!=', null)->orderby('nombres', 'asc')->get() as $key6 => $value6) { $selectRut[$value6->persona_id]=$value6->rut;}
      $selectUsuariosid[null] = 'SELECCIONE USUARIO'; foreach (Users::where('id', '!=', null)->orderby('nombres', 'asc')->get() as $key6 => $value6) { $selectUsuariosid[$value6->id]=$value6->usuariosid;}
      $selectDivision[null] = 'SELECCIONE DIVISIÓN'; foreach (Divisiones::where('id', '!=', null)->orderby('id', 'asc')->get() as $key7 => $value7) { $selectDivision[$value7->id]=$value7->nombre;}
      $selectFormula[null] = 'SELECCIONE FORMULA DEL INDICADOR'; $selectFormula['$numerador/$denominador']='$numerador/$denominador'; $selectFormula['($numerador/$denominador)*100']='($numerador/$denominador)*100';

      return view('sigc.sigc_editar_indicadores')
      ->with('indicador' , $indicador)
      ->with('selectCategoria' , $selectCategoria)
      ->with('selectUnidad' , $selectUnidad)
      ->with('selectAno' , $selectAno)
      ->with('selectTipo' , $selectTipo)
      ->with('selectProducto' , $selectProducto)
      ->with('selectAmbito' , $selectAmbito)
      ->with('selectDimension' , $selectDimension)
      ->with('selectEquipo' , $selectEquipo)
      ->with('selectRut' , $selectRut)
      ->with('selectFormula' , $selectFormula)
      ->with('selectUsuariosid' , $selectUsuariosid)
      ->with('selectDivision' , $selectDivision);
   }

      public function update (IndicadoresRequest $request , $id){


      $ano = $request->ano;
      $nombreIndicador = $request->nombre;
      $numeradorIndicador = $request->numerador;
      $categoriaIndicador = $request->categoria;
      if($categoriaIndicador == 1 ){
          if(!$request->denominador){
            return redirect()->back()->withErrors(['Categoria fija es requerido el denominador']);
          }
        $denominadorIndicador = $request->denominador;
      }
      $multiplicadorIndicador = $request->multiplicador;
      $ponderadorIndicador = $request->ponderador;
      $metaIndicador = $request->meta;
      $unidadIndicador = $request->unidad;
      $medioVerificacionIndicador = $request->medio_verificacion;
      $notasIndicador = $request->notas;
      $productoIndicador = $request->producto;
      $tipoIndicador = $request->tipo;
      $divisionIndicador = $request->divisionId;
      $ambitoIndicador = $request->ambito;
      $dimensionIndicador = $request->dimension;
      $equipoIndicador = $request->equipo;
      $reportertId = $request->reportertId;
      $reportersId = $request->reportersId;
      $responsabletId = $request->responsabletId;
      $revisorId = $request->revisorId;
      $fomulaIndicador = $request->formula;


      $nuevoIndicador = Indicadores::where('id' , $id)->first();
      $nuevoIndicador->ano = $ano;
      $nuevoIndicador->nombre = $nombreIndicador;
      $nuevoIndicador->numerador = $numeradorIndicador;
        if($categoriaIndicador == 1 ){
          $nuevoIndicador->denominador =  $denominadorIndicador;
        }
      $nuevoIndicador->multiplicador =  $multiplicadorIndicador;
      $nuevoIndicador->formula = $fomulaIndicador;
      $nuevoIndicador->ponderador =  $ponderadorIndicador;
      $nuevoIndicador->meta = $metaIndicador;
      $nuevoIndicador->unidad_id = $unidadIndicador;
      $nuevoIndicador->medio_verif = $medioVerificacionIndicador;
      $nuevoIndicador->notas =  $notasIndicador ;
      $nuevoIndicador->producto_id =  $productoIndicador;
      $nuevoIndicador->ambito_id =  $ambitoIndicador;
      $nuevoIndicador->dimension_id =  $dimensionIndicador;
      $nuevoIndicador->tipo_id = $tipoIndicador;
      $nuevoIndicador->division_id = $divisionIndicador;
      $nuevoIndicador->equipo_id =  $equipoIndicador;
      $nuevoIndicador->categoria_id = $categoriaIndicador;

      $nuevoIndicador->reportador_id = $reportertId;
      $nuevoIndicador->reportadores_id = $reportersId; /* suplenete del reportador*/

      $nuevoIndicador->revisor_id = $revisorId;
      $nuevoIndicador->responsable_id = $responsabletId; /* suplenete del revisor*/

      $nuevoIndicador->save();
      alert()->success('!','Indicador editado exitosamente');
      return redirect()->route('sigc_indicadores');
   }
   public function habilitarSuplencia($id){
      $indicador = Indicadores::where('id' , $id)->first();
      $indicador->habilitar_suplencia = 1;
      $indicador->save();
      alert()->success('!','Suplencia para reporte y revision habilitada Exitosamente');
      return redirect()->route('sigc_indicadores');
   }
   public function deshabilitarSuplencia($id){
      $indicador = Indicadores::where('id' , $id)->first();
      $indicador->habilitar_suplencia = 0;
      $indicador->save();
      alert()->success('!','Suplencia para reporte y revision deshabilitada Exitosamente');
      return redirect()->route('sigc_indicadores');
   }

   public function reportabilidad(Request $request)
   {

       $dataDefinitiva = [];
       //$dataDefinitiva = Indicadores::where('ano' , '>=', date("Y"))->get();
       $dataDefinitiva = Indicadores::where('ano' , '>=', '2022')->get();

       $mesActual  =  date('n');
       $anoActual = date("Y");
       if($mesActual >= 2 ){
         $mesConsulta = $mesActual -1 ;
       }else{
         $mesConsulta = 12;
       }

       if ($mesConsulta == 12 ){
          $anoConsulta = $anoActual - 1;
       }else{
         $anoConsulta = $anoActual;
       }

       $idCalendarioConsulta = Calendario::where('mes' , $mesConsulta)->where('ano' , $anoConsulta)->first();


       foreach ($dataDefinitiva as $llaveind => $valorind) {
         $existMedicion = Mediciones::where('indicador_id' , $valorind->id)->where('calendario_id' , $idCalendarioConsulta->id)->first();

         if($existMedicion){
           $valorind->reportado = true;
           $valorind->estatus_medicion = $existMedicion->MedicionEstado->nombre;
         }else{
           $valorind->reportado = false;
         }
       }

     return view('sigc.sigc_reportabilidad')
     ->with('dataDefinitiva' , $dataDefinitiva);
   }

   public function notificacionNoReporto($id){

       $indicador = Indicadores::where('id', $id)->first();
       $usuario = Users::where('id',$indicador->reportador_id)->first();
       $emails = [$usuario->email,Auth::user()->email];


       foreach ($emails as $key => $correo) {
         Mail::to($correo)->send(new NotificacionNoReportoEmail($indicador));
         $notificacionesId = 8;
         $nuevoCorreo = new CorreoNotificaciones();
         $nuevoCorreo->id_notificaciones = $notificacionesId;
         $nuevoCorreo->destintario = $correo;
         $nuevoCorreo->save();
       }
       alert()->success('!','Notificación enviada exitosamente');
       return redirect()->route('sigc_reportabilidad');

     }
     public function getTipoPorAno($ano){
      $tipos = Tipos::where('ano' , $ano)->get();
      $arrayTipos = [];
      foreach ($tipos as $key => $value) {
        $arrayTipos[$value->id] = $value->ano.' '.$value->id.' '.$value->alias;
    }
    return $arrayTipos;
    }
    
    public function getEquipoPorAno($ano){
      $equipos = Equipos::where('ano' , $ano)->get();
      $arrayEquipos = [];
      foreach ($equipos as $key => $value) {
        $arrayEquipos[$value->id] = $value->ano.' - '.$value->id.' - '.$value->nombre;
      }
      return $arrayEquipos;
    }

    public function getIndicadoresSelect($ano, $tipo , $equipo){

      $anoView = $ano == 0 ? null : $ano;
      $tipoView = $tipo == 0 ? null : $tipo;
      $equipoView = $equipo == 0 ? null : $equipo;

      $query = Indicadores::query();
      $query->when($anoView, function ($q, $anoView) {
            return $q->where('ano', $anoView);
      });
      $query->when($tipoView, function ($q, $tipoView) {
            return $q->where('tipo_id', $tipoView);
      });
      $query->when($equipoView, function ($q, $equipoView) {
            return $q->where('equipo_id', $equipoView);
      });

      $query->orderby('id', 'desc');
      $indicadores =  $query->get();

      $arrayIndicadores = [];
      foreach ($indicadores as $key => $value) {
        $arrayIndicadores[$value->id] = $value->ano.' - '.$value->id.' - '.$value->nombre;
      }
      return $arrayIndicadores;
    }

}
