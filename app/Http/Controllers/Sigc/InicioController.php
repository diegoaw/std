<?php

namespace App\Http\Controllers\Sigc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Sigc\Indicadores;
use App\Models\Sigc\Tipos;
use App\Models\Sigc\Equipos;
use App\Models\Sigc\Mediciones;
use App\Models\Sigc\Divisiones;
use App\Models\Sigc\Sigc_ind_med_prenafeta;
use App\Models\Sigc\Condiciones_formulas_indicadores;
use Carbon\Carbon;
use App\Models\Sigc\Eventos;
use App\Models\Sigc\Calendario;
use App\Http\Controllers\Sigc\CalendarioController;
use Maatwebsite\Excel\Facades\Excel;
use Dompdf\Dompdf;
use PDF;


class InicioController extends Controller
{

    public function index(Request $request)
          {

            $month = date("Y-m");
            $data = $this->calendar_month($month);
            $mes = $data['month'];


            // obtener mes en espanol
            $mespanish = new CalendarioController();
            $mespanish = $mespanish->spanishMonth($mes);
            $mes = $data['month'];

            if(date("n") == 1){
              $mesO = 12;
              $anoO =  (date("Y")-1);
            }else{
              $mesO = (date("n")-1);
              $anoO =  date("Y");
            }
            //provivional
           // $mesO = 2;
            $query1 = Eventos::query();
            $query1->whereHas('Calendario', function ($q) use ($mesO) {  $q->where('mes' ,  $mesO); });
            $query1->whereHas('Calendario', function ($q) use ($anoO) {  $q->where('ano' ,  $anoO); });
            $eventosMes = $query1->get();

            /*********************indicadores tabla*********************/
            $dataDefinitiva = [];
            $dataDefinitivaAx = [];

            $requestnombre = $request->nombre;
            $tipoView = $request->tipo;
            $tipoNombre = Tipos::where('id' , $tipoView)->first();
            $equipoView = $request->equipo;
            $equipoNombre = Equipos::where('id' , $equipoView)->first();
            $requestnombreIndicador = $request->nombre;
            $divisionView = $request->divisionId;
            $divisionNombre = Divisiones::where('id' , $divisionView)->first();
            $mesActual  =  date('n');

            if($mesActual == 1){
              $ano  = (date("Y")-1);
            }else{
              $ano  = $anoActual = (date("Y"));
            }


            $selectTipo[null] = 'SELECCIONE TIPO'; foreach (Tipos::where('ano' , $anoO)->orderby('id', 'desc')->get() as $key1 => $value1) { $selectTipo[$value1->id]=$value1->aliasc;}
            $selectEquipo[null] = 'SELECCIONE EQUIPO'; foreach (Equipos::where('ano' , $anoO)->orderby('id', 'desc')->get() as $key2 => $value2) { $selectEquipo[$value2->id]=$value2->nombrecompleto;}
            $selectDivision[null] = 'SELECCIONE DIVISIÓN'; foreach (Divisiones::orderby('id', 'asc')->get() as $key7 => $value7) { $selectDivision[$value7->id]=$value7->nombre;}

            $calendariosAnual = Calendario::select('id' ,'mes')->where('ano' , $ano)->get();
            $arregloGraficaCumplimiento = [];
            $arregloGraficaPonderador = [];

            $query = Indicadores::query();
            $query->where('ano' , '=', $ano);
            $query->when($tipoView, function ($q, $tipoView) {
                  return $q->where('tipo_id', $tipoView);
            });
            $query->when($equipoView, function ($q, $equipoView) {
                  return $q->where('equipo_id', $equipoView);
            });
            $query->when($requestnombreIndicador, function ($q, $requestnombreIndicador) {
                  return $q->where('nombre','like' ,"%$requestnombreIndicador%");
            });
            $query->when($divisionView, function ($q, $divisionView) {
                  return $q->where('division_id', $divisionView);
            });
            $query->orderby('id', 'desc');
            $dataDefinitiva =  $query->get();

            $calendarioMesActual = Calendario::where('ano' , $anoO)->where('mes',$mesO)->first();

            foreach ($dataDefinitiva as $key0 => $value0) {
              $value0->graf_rtdlda = 'rr'.$value0->id;

              $medicionMesActual = Mediciones::where('indicador_id' , $value0->id)->where('calendario_id' , $calendarioMesActual->id)->first();
              $value0->reportada = $medicionMesActual? true: false;
              $ponderacionMesActualDatos = Sigc_ind_med_prenafeta::where('Indicador_Id', $value0->id)->where('Medicion_Mes_Reportado', $this->getMEsRodrigo($mesO))->first();
              if($ponderacionMesActualDatos){
              $ponderacionMesActual = $ponderacionMesActualDatos->CALCULO_AVANCE_PONDERADO;
              $value0->ponderadormesactual = $ponderacionMesActual;
              }else{
                $value0->ponderadormesactual = 0;
              }
              if($medicionMesActual){
                $value0->estado = $medicionMesActual->MedicionEstado->nombre;
              }
            $formulaCumplimiento =$value0->formula_cumplimiento;
            $formulaPonderador =$value0->formula_ponderador;


          //    foreach($calendariosAnual as $indiceCalendario => $valorCalendario ){
                  $medicion = Mediciones::where('indicador_id' , $value0->id)->where('calendario_id' , $calendarioMesActual->id)->first();
                  $resultado = 0;
                  $resultadoP=0;
                  if($medicion){
                    $casoEspecial01 = 22.32;
                    $casoEspecial02  = 12;
                    $numerador = $medicion->valor_numerador;
                    $medicionAprobada = $medicion->medicion_estado_id == 1 ? true: false;
                    $meta = $value0->meta;
                    $ponderadorOriginal = $value0->ponderador;
                    $ponderador = ($value0->ponderador/100);
                    $ponderador_d = $ponderador;
                    $porcentaje = 100;
                    $multiplicador = $value0->multiplicador;
                    $pasarCalculo = true;
                    $pasarCalculoMeta = true;
                    $pasarCalculoCumplimiento = true;
                    $c1 = 51;
                    $sinformula = 0;
                    if($value0->categoria_id == 1){
                        $denominador = $value0->denominador;
                    }else{
                        $denominador = $medicion->denominador;
                    }

                    $validacionesIndicador3 = Condiciones_formulas_indicadores::where('tipo_formula' , 'formula_cumplimineto')->where('id_indicador' , $value0->id)->where('estatus', 1)->get();

                    if(count($validacionesIndicador3)>0){
                        foreach($validacionesIndicador3 as $indiceValidacion3 => $valorValidacion3 ){
                            eval('$resultadoTempVal3 =' .$valorValidacion3->validacion.';');
                            if(!$resultadoTempVal3){
                                $pasarCalculoCumplimiento = false;
                                break;
                            }
                        }
                    }


                    if($value0->formula_cumplimiento && $pasarCalculoCumplimiento){
                      $formulaCumplimiento = $value0->formula_cumplimiento;
                      if($formulaCumplimiento == '$porcentaje'){
                        $value0->formula_cumplimiento_string = "($numerador/$denominador)*$multiplicador";
                      }else{
                        $auxiliar = strval($value0->formula_cumplimiento);

                        $auxiliar = str_replace ('$denominador' , $denominador , $auxiliar);
                        $auxiliar= str_replace ('$numerador' , $numerador , $auxiliar);
                        $auxiliar= str_replace ('$multiplicador' , $multiplicador , $auxiliar);
                        $auxiliar= str_replace ('$meta' , $meta , $auxiliar);
                        $auxiliar= str_replace ('$porcentaje' , $porcentaje , $auxiliar);

                        $value0->formula_cumplimiento_string = $auxiliar;
                      }


                    }else{
                      $formulaCumplimiento = 0;
                    }

                    eval('$resultado =' .$formulaCumplimiento.';');

                    $arregloGraficaCumplimiento[$value0->id][$calendarioMesActual->mes] = round( $resultado, 2);

                    $validacionesIndicador = Condiciones_formulas_indicadores::where('tipo_formula' , 'formula_ponderador')->where('id_indicador' , $value0->id)->where('estatus', 1)->get();
                    foreach($validacionesIndicador as $indiceValidacion => $valorValidacion ){
                        eval('$resultadoTempVal =' .$valorValidacion->validacion.';');
                        if(!$resultadoTempVal){
                            $pasarCalculo = false;
                            break;
                        }
                    }


                    if($value0->formula_ponderador  && $pasarCalculo){
                      $formulaPonderador = $value0->formula_ponderador;
                    }else{
                      $formulaPonderador = 0;
                    }

                     eval('$resultadoP =' .$formulaPonderador.';');
                     $value0->ponderador_calculado = number_format($this->numberFormatPrecision($resultadoP, 1, "."), 1, ",", ".") ;  //se cambia a uno por solicitud de Alejandra

                    $validacionesIndicador2 = Condiciones_formulas_indicadores::where('tipo_formula' , 'formula_meta')->where('id_indicador' , $value0->id)->where('estatus', 1)->get();
                    if(count($validacionesIndicador2)>0){
                        foreach($validacionesIndicador2 as $indiceValidacion2 => $valorValidacion2 ){
                            eval('$resultadoTempVal2 =' .$valorValidacion2->validacion.';');
                            if(!$resultadoTempVal2){
                                $pasarCalculoMeta = false;
                                break;
                            }
                        }
                    }
                    if($value0->formula_calculo_meta && $pasarCalculoMeta){
                      $formulaMeta = $value0->formula_calculo_meta;
                    }else{
                      $formulaMeta = 0;
                    }
                     eval('$resultadoM =' .$formulaMeta.';');
                     $value0->meta_calculada = round($resultadoM, 1) ;

                  }else{
                    $arregloGraficaCumplimiento[$value0->id][$calendarioMesActual->mes] =  round($resultado, 2);
                    $arregloGraficaPonderador[$value0->id][$calendarioMesActual->mes] = round($resultadoP, 2);
                  }

            }


            return view('sigc.sigc_inicio')
            ->with('arregloGraficaCumplimiento' , $arregloGraficaCumplimiento)
            ->with('mesO' , $mesO)
            ->with('anoO' , $anoO)
            ->with('data' , $data)
            ->with('eventosMes' , $eventosMes)
            ->with('mes' , $mes)
            ->with('mespanish' , $mespanish)
            ->with('dataDefinitiva' , $dataDefinitiva)
            ->with('requestnombreAmbito' , $requestnombre)
            ->with('dataDefinitiva' , $dataDefinitiva)
            ->with('selectTipo' , $selectTipo)
            ->with('tipoView' , $tipoView)
            ->with('selectEquipo' , $selectEquipo)
            ->with('equipoView' , $equipoView)
            ->with('requestnombreIndicador' , $requestnombreIndicador)
            ->with('selectDivision' , $selectDivision)
            ->with('divisionView' , $divisionView)
            ->with('tipoNombre', $tipoNombre)
            ->with('equipoNombre', $equipoNombre)
            ->with('divisionNombre', $divisionNombre);
          }

          public function index_month(Request $request, $month){

                       //;
                       $data = $this->calendar_month($month);

                       // dd($data);
                        $mes = $data['month'];
                        // obtener mes en espanol
                        //$mespanish = '';
                        $mes = $data['month'];
                        $mespanish = new CalendarioController();
                        $mespanish = $mespanish->spanishMonth($mes);
                        if(date("n") == 1){
                          $mesO = 12;
                          $anoO =  (date("Y")-1);
                        }else{
                          $mesO = (date("n")-1);
                          $anoO =  date("Y");
                        }

                        $query1 = Eventos::query();
                        $query1->whereHas('Calendario', function ($q) use ($mesO) {  $q->where('mes' ,  $mesO); });
                        $query1->whereHas('Calendario', function ($q) use ($anoO) {  $q->where('ano' ,  $anoO); });
                        $eventosMes = $query1->get();

                        /*********************indicadores tabla*********************/


                        $dataDefinitiva = [];
                        $requestnombre = $request->nombre;
                        $tipoView = $request->tipo;
                        $tipoNombre = Tipos::where('id' , $tipoView)->first();
                        $equipoView = $request->equipo;
                        $equipoNombre = Equipos::where('id' , $equipoView)->first();
                        $requestnombreIndicador = $request->nombre;
                        $divisionView = $request->divisionId;
                        $divisionNombre = Divisiones::where('id' , $divisionView)->first();
                        $mesActual  =  date('n');

                        if($mesActual == 1){
                          $ano  = (date("Y")-1);
                        }else{
                          $ano  = $anoActual = (date("Y"));
                        }


                        $selectTipo[null] = 'SELECCIONE TIPO'; foreach (Tipos::where('ano' , $anoO)->orderby('id', 'desc')->get() as $key1 => $value1) { $selectTipo[$value1->id]=$value1->aliasc;}
                        $selectEquipo[null] = 'SELECCIONE EQUIPO'; foreach (Equipos::where('ano' , $anoO)->orderby('id', 'desc')->get() as $key2 => $value2) { $selectEquipo[$value2->id]=$value2->nombrecompleto;}
                        $selectDivision[null] = 'SELECCIONE DIVISIÓN'; foreach (Divisiones::orderby('id', 'asc')->get() as $key7 => $value7) { $selectDivision[$value7->id]=$value7->nombre;}

                        $calendariosAnual = Calendario::select('id' ,'mes')->where('ano' , $ano)->get();
                        $arregloGraficaCumplimiento = [];
                        $arregloGraficaPonderador = [];

                        $query = Indicadores::query();
                        $query->where('ano' , '=', $ano);
                        $query->when($tipoView, function ($q, $tipoView) {
                              return $q->where('tipo_id', $tipoView);
                        });
                        $query->when($equipoView, function ($q, $equipoView) {
                              return $q->where('equipo_id', $equipoView);
                        });
                        $query->when($requestnombreIndicador, function ($q, $requestnombreIndicador) {
                              return $q->where('nombre','like' ,"%$requestnombreIndicador%");
                        });
                        $query->orderby('id', 'desc');
                        $dataDefinitiva =  $query->get();

                        $calendarioMesActual = Calendario::where('ano' , $anoO)->where('mes',$mesO)->first();

                        foreach ($dataDefinitiva as $key0 => $value0) {
                          $value0->graf_rtdlda = 'rr'.$value0->id;
                          // $value0->mediciones_apobadas = $this->conteoMedicionesPorEstatus($value0->id , 1 , $ano);
                          //
                          // $value0->mediciones_rechazadas = $this->conteoMedicionesPorEstatus($value0->id , 6 , $ano);
                          // $value0->total = 12;
                          // $value0->sin_medir = ($value0->total - ($value0->mediciones_apobadas+$value0->mediciones_rechazadas));
                          // $value0->total_reportadas = ($value0->mediciones_apobadas + $value0->mediciones_rechazadas);
                          //
                          // $value0->porcentage_aprobadas =  (($value0->mediciones_apobadas * 100)/$value0->total);
                          // $value0->porcentage_rechazadas =  (($value0->mediciones_rechazadas * 100)/$value0->total);
                          // $value0->porcentage_sin_medir =  (($value0->sin_medir * 100)/$value0->total);

                          $medicionMesActual = Mediciones::where('indicador_id' , $value0->id)->where('calendario_id' , $calendarioMesActual->id)->first();
                          $value0->reportada = $medicionMesActual? true: false;
                          $ponderacionMesActualDatos = Sigc_ind_med_prenafeta::where('Indicador_Id', $value0->id)->where('Medicion_Mes_Reportado', $this->getMEsRodrigo($mesO))->first();
                          if($ponderacionMesActualDatos){
                          $ponderacionMesActual = $ponderacionMesActualDatos->CALCULO_AVANCE_PONDERADO;
                          $value0->ponderadormesactual = $ponderacionMesActual;
                          }else{
                            $value0->ponderadormesactual = 0;
                          }
                          if($medicionMesActual){
                            $value0->estado = $medicionMesActual->MedicionEstado->nombre;
                          }
                          $formulaCumplimiento =$value0->formula_cumplimiento;
                          $formulaPonderador =$value0->formula_ponderador;


                          //    foreach($calendariosAnual as $indiceCalendario => $valorCalendario ){
                                  $medicion = Mediciones::where('indicador_id' , $value0->id)->where('calendario_id' , $calendarioMesActual->id)->first();
                                  $resultado = 0;
                                  $resultadoP=0;
                                  if($medicion){
                                    $casoEspecial01 = 22.32;
                                    $casoEspecial02  = 12;
                                    $numerador = $medicion->valor_numerador;
                                    $medicionAprobada = $medicion->medicion_estado_id == 1 ? true: false;
                                    $meta = $value0->meta;
                                    $ponderadorOriginal = $value0->ponderador;
                                    $ponderador = ($value0->ponderador/100);
                                    $ponderador_d = $ponderador;
                                    $porcentaje = 100;
                                    $multiplicador = $value0->multiplicador;
                                    $pasarCalculo = true;
                                    $pasarCalculoMeta = true;
                                    $pasarCalculoCumplimiento = true;
                                    $c1 = 51;
                                    $sinformula = 0;
                                    if($value0->categoria_id == 1){
                                        $denominador = $value0->denominador;
                                    }else{
                                        $denominador = $medicion->denominador;
                                    }

                                    $validacionesIndicador3 = Condiciones_formulas_indicadores::where('tipo_formula' , 'formula_cumplimineto')->where('id_indicador' , $value0->id)->where('estatus', 1)->get();

                                    if(count($validacionesIndicador3)>0){
                                        foreach($validacionesIndicador3 as $indiceValidacion3 => $valorValidacion3 ){
                                            eval('$resultadoTempVal3 =' .$valorValidacion3->validacion.';');
                                            if(!$resultadoTempVal3){
                                                $pasarCalculoCumplimiento = false;
                                                break;
                                            }
                                        }
                                    }
                                    if($value0->formula_cumplimiento && $pasarCalculoCumplimiento){
                                      $formulaCumplimiento = $value0->formula_cumplimiento;
                                      if($formulaCumplimiento == '$porcentaje'){
                                        $value0->formula_cumplimiento_string = "($numerador/$denominador)*$multiplicador";
                                      }else{
                                        $auxiliar = strval($value0->formula_cumplimiento);

                                        $auxiliar = str_replace ('$denominador' , $denominador , $auxiliar);
                                        $auxiliar= str_replace ('$numerador' , $numerador , $auxiliar);
                                        $auxiliar= str_replace ('$multiplicador' , $multiplicador , $auxiliar);
                                        $auxiliar= str_replace ('$meta' , $meta , $auxiliar);
                                        $auxiliar= str_replace ('$porcentaje' , $porcentaje , $auxiliar);

                                        $value0->formula_cumplimiento_string = $auxiliar;
                                      }


                                    }else{
                                      $formulaCumplimiento = 0;
                                    }


                                    eval('$resultado =' .$formulaCumplimiento.';');

                                    $arregloGraficaCumplimiento[$value0->id][$calendarioMesActual->mes] = round( $resultado, 2);

                                    $validacionesIndicador = Condiciones_formulas_indicadores::where('tipo_formula' , 'formula_ponderador')->where('id_indicador' , $value0->id)->where('estatus', 1)->get();
                                    foreach($validacionesIndicador as $indiceValidacion => $valorValidacion ){
                                        eval('$resultadoTempVal =' .$valorValidacion->validacion.';');
                                        if(!$resultadoTempVal){
                                            $pasarCalculo = false;
                                            break;
                                        }
                                    }


                                    if($value0->formula_ponderador  && $pasarCalculo){
                                      $formulaPonderador = $value0->formula_ponderador;
                                    }else{
                                      $formulaPonderador = 0;
                                    }

                                     eval('$resultadoP =' .$formulaPonderador.';');
                                     $value0->ponderador_calculado = number_format($this->numberFormatPrecision($resultadoP, 1, "."), 1, ",", ".") ;

                                    $validacionesIndicador2 = Condiciones_formulas_indicadores::where('tipo_formula' , 'formula_meta')->where('id_indicador' , $value0->id)->where('estatus', 1)->get();
                                    if(count($validacionesIndicador2)>0){
                                        foreach($validacionesIndicador2 as $indiceValidacion2 => $valorValidacion2 ){
                                            eval('$resultadoTempVal2 =' .$valorValidacion2->validacion.';');
                                            if(!$resultadoTempVal2){
                                                $pasarCalculoMeta = false;
                                                break;
                                            }
                                        }
                                    }
                                    if($value0->formula_calculo_meta && $pasarCalculoMeta){
                                      $formulaMeta = $value0->formula_calculo_meta;
                                    }else{
                                      $formulaMeta = 0;
                                    }
                                     eval('$resultadoM =' .$formulaMeta.';');
                                     $value0->meta_calculada = round($resultadoM, 1) ;

                                  }else{
                                    $arregloGraficaCumplimiento[$value0->id][$calendarioMesActual->mes] =  round($resultado, 2);
                                    $arregloGraficaPonderador[$value0->id][$calendarioMesActual->mes] = round($resultadoP, 2);
                                  }

                            }

                        //dd( $dataDefinitiva );
                        return view('sigc.sigc_inicio')
                        ->with('anoO' , $anoO)
                        ->with('arregloGraficaCumplimiento' , $arregloGraficaCumplimiento)
                        ->with('selectDivision' , $selectDivision)
                        ->with('divisionView' , $divisionView)
                        ->with('mesO' , $mesO)
                        ->with('data' , $data)
                        ->with('eventosMes' , $eventosMes)
                        ->with('mes' , $mes)
                        ->with('mespanish' , $mespanish)
                        ->with('dataDefinitiva' , $dataDefinitiva)
                        ->with('requestnombreAmbito' , $requestnombre)
                        ->with('dataDefinitiva' , $dataDefinitiva)
                        ->with('selectTipo' , $selectTipo)
                        ->with('tipoView' , $tipoView)
                        ->with('selectEquipo' , $selectEquipo)
                        ->with('equipoView' , $equipoView)
                        ->with('tipoNombre', $tipoNombre)
                        ->with('equipoNombre', $equipoNombre)
                        ->with('divisionNombre', $divisionNombre)
                        ->with('requestnombreIndicador' , $requestnombreIndicador);

          }

      public function conteoMedicionesPorEstatus($indicador , $estatus , $ano){
            $query = Mediciones::query();
            $query->where('indicador_id',$indicador);
            $query->where('medicion_estado_id',$estatus);
            $query->whereHas('Calendario', function ($q) use ($ano) {  $q->where('ano' ,  $ano); });
            $conteo =  $query->get()->count();
          return  $conteo;
      }

      public static function calendar_month($month){
        //$mes = date("Y-m");
        $mes = $month;
        //sacar el ultimo de dia del mes
        $daylast =  date("Y-m-d", strtotime("last day of ".$mes));
        //sacar el dia de dia del mes
        $fecha      =  date("Y-m-d", strtotime("first day of ".$mes));

        $daysmonth  =  date("d", strtotime($fecha));
        $montmonth  =  date("m", strtotime($fecha));
        $yearmonth  =  date("Y", strtotime($fecha));
        // sacar el lunes de la primera semana
        $nuevaFecha = mktime(0,0,0,$montmonth,$daysmonth,$yearmonth);
        $diaDeLaSemana = date("w", $nuevaFecha);
        $nuevaFecha = $nuevaFecha - ($diaDeLaSemana*24*3600); //Restar los segundos totales de los dias transcurridos de la semana
        $dateini = date ("Y-m-d",$nuevaFecha);
        //$dateini = date("Y-m-d",strtotime($dateini."+ 1 day"));
        // numero de primer semana del mes
        $semana1 = date("W",strtotime($fecha));
        $semana1 = 1;
        // numero de ultima semana del mes
        $semana2 = date("W",strtotime($daylast));
          //  dd($semana1 , $fecha , $semana2 , $daylast);
        // semana todal del mes
        // en caso si es diciembre
        if (date("m", strtotime($mes))==12) {
            $semana = 5;
        }
        else {
          $semana = ($semana2-$semana1)+2;
        }

        // semana todal del mes

        $datafecha = $dateini;
        $calendario = array();
        $iweek = 0;

        while ($iweek < $semana):
            $iweek++;
         //   ddd( $iweek);
            //echo "Semana $iweek <br>";
            //
            $weekdata = [];
         //   dd($weekdata);
            for ($iday=0; $iday < 7 ; $iday++){
              // code...
              $datafecha = date("Y-m-d",strtotime($datafecha."+ 1 day"));
              $datanew['mes'] = date("M", strtotime($datafecha));
              $datanew['dia'] = date("d", strtotime($datafecha));
              $datanew['fecha'] = $datafecha;
              //AGREGAR CONSULTAS EVENTO

              $datanew['evento'] = Eventos::whereRaw("SUBSTRING(`start_date` , 1, 10) ="."'$datafecha'")->get();

              if(Eventos::whereRaw("SUBSTRING(`start_date` , 1, 10) ="."'$datafecha'")->get()->count()>0){
                $auxConsultaAnterior = Eventos::whereRaw("SUBSTRING(`start_date` , 1, 10) ="."'$datafecha'")->get();
              }else{
                $rangoF =  Eventos::whereRaw("'$datafecha'BETWEEN SUBSTRING(`start_date` , 1, 10) AND SUBSTRING(`end_date` , 1, 10)")->get();
                if($rangoF->count()>0){
                  $datanew['evento'] = $auxConsultaAnterior;
                }
              }

              array_push($weekdata,$datanew);
            }
            $dataweek['semana'] = $iweek;
            $dataweek['datos'] = $weekdata;
            //$datafecha['horario'] = $datahorario;
            array_push($calendario,$dataweek);
        endwhile;
        $nextmonth = date("Y-M",strtotime($mes."+ 1 month"));
        $lastmonth = date("Y-M",strtotime($mes."- 1 month"));
        $month = date("M",strtotime($mes));
        $yearmonth = date("Y",strtotime($mes));
        //$month = date("M",strtotime("2019-03"));
        $data = array(
          'next' => $nextmonth,
          'month'=> $month,
          'year' => $yearmonth,
          'last' => $lastmonth,
          'calendar' => $calendario,
        );
        return $data;
      }


        public function reporteIndicadores(Request $request) {

          if(date("n") == 1){
            $mesO = 12;
            $anoO =  (date("Y")-1);
          }else{
            $mesO = (date("n")-1);
            $anoO =  date("Y");
          }

          $mesActual  =  date('n');

          if($mesActual == 1){
            $ano  = (date("Y")-1);
          }else{
            $ano  = $anoActual = (date("Y"));
          }


          $tipoView = $request->tipo1;
          $equipoView = $request->equipo1;
          $requestnombreIndicador = $request->nombre1;
          $divisionView = $request->division1;


          $query = Indicadores::query();
          $query->where('ano' , '=', $ano);
          $query->when($tipoView, function ($q, $tipoView) {
                return $q->where('tipo_id', $tipoView);
          });
          $query->when($equipoView, function ($q, $equipoView) {
                return $q->where('equipo_id', $equipoView);
          });
          $query->when($requestnombreIndicador, function ($q, $requestnombreIndicador) {
                return $q->where('nombre','like' ,"%$requestnombreIndicador%");
          });
          $query->when($divisionView, function ($q, $divisionView) {
                return $q->where('division_id', $divisionView);
          });
          $query->orderby('id', 'desc');

          $indicadores =  $query->get();

          $tipoReporte = $request->imprimir;

          $calendariosAnual = Calendario::select('id' ,'mes')->where('ano' , $ano)->get();
          $calendarioMesActual = Calendario::where('ano' , $anoO)->where('mes',$mesO)->first();
          $arregloGraficaCumplimiento = [];
          $arregloGraficaPonderador = [];

          foreach ($indicadores as $key0 => $value0) {
            $value0->graf_rtdlda = 'rr'.$value0->id;
            // $value0->mediciones_apobadas = $this->conteoMedicionesPorEstatus($value0->id , 1 , $ano);
            //
            // $value0->mediciones_rechazadas = $this->conteoMedicionesPorEstatus($value0->id , 6 , $ano);
            // $value0->total = 12;
            // $value0->sin_medir = ($value0->total - ($value0->mediciones_apobadas+$value0->mediciones_rechazadas));
            // $value0->total_reportadas = ($value0->mediciones_apobadas + $value0->mediciones_rechazadas);
            //
            // $value0->porcentage_aprobadas =  (($value0->mediciones_apobadas * 100)/$value0->total);
            // $value0->porcentage_rechazadas =  (($value0->mediciones_rechazadas * 100)/$value0->total);
            // $value0->porcentage_sin_medir =  (($value0->sin_medir * 100)/$value0->total);

            $medicionMesActual = Mediciones::where('indicador_id' , $value0->id)->where('calendario_id' , $calendarioMesActual->id)->first();
            $value0->reportada = $medicionMesActual? true: false;
            $ponderacionMesActualDatos = Sigc_ind_med_prenafeta::where('Indicador_Id', $value0->id)->where('Medicion_Mes_Reportado', $this->getMEsRodrigo($mesO))->first();
            if($ponderacionMesActualDatos){
            $ponderacionMesActual = $ponderacionMesActualDatos->CALCULO_AVANCE_PONDERADO;
            $value0->ponderadormesactual = $ponderacionMesActual;
            }else{
              $value0->ponderadormesactual = 0;
            }
            if($medicionMesActual){
              $value0->estado = $medicionMesActual->MedicionEstado->nombre;
            }
            $formulaCumplimiento =$value0->formula_cumplimiento;
            $formulaPonderador =$value0->formula_ponderador;


            //    foreach($calendariosAnual as $indiceCalendario => $valorCalendario ){
                    $medicion = Mediciones::where('indicador_id' , $value0->id)->where('calendario_id' , $calendarioMesActual->id)->first();
                    $resultado = 0;
                    $resultadoP=0;
                    if($medicion){
                      $casoEspecial01 = 22.32;
                      $casoEspecial02  = 12;
                      $numerador = $medicion->valor_numerador;
                      $medicionAprobada = $medicion->medicion_estado_id == 1 ? true: false;
                      $meta = $value0->meta;
                      $ponderadorOriginal = $value0->ponderador;
                      $ponderador = ($value0->ponderador/100);
                      $ponderador_d = $ponderador;
                      $porcentaje = 100;
                      $multiplicador = $value0->multiplicador;
                      $pasarCalculo = true;
                      $pasarCalculoMeta = true;
                      $pasarCalculoCumplimiento = true;
                      $c1 = 51;
                      $sinformula = 0;
                      if($value0->categoria_id == 1){
                          $denominador = $value0->denominador;
                      }else{
                          $denominador = $medicion->denominador;
                      }

                      $validacionesIndicador3 = Condiciones_formulas_indicadores::where('tipo_formula' , 'formula_cumplimineto')->where('id_indicador' , $value0->id)->where('estatus', 1)->get();

                      if(count($validacionesIndicador3)>0){
                          foreach($validacionesIndicador3 as $indiceValidacion3 => $valorValidacion3 ){
                              eval('$resultadoTempVal3 =' .$valorValidacion3->validacion.';');
                              if(!$resultadoTempVal3){
                                  $pasarCalculoCumplimiento = false;
                                  break;
                              }
                          }
                      }


                      if($value0->formula_cumplimiento && $pasarCalculoCumplimiento){
                        $formulaCumplimiento = $value0->formula_cumplimiento;
                        if($formulaCumplimiento == '$porcentaje'){
                          $value0->formula_cumplimiento_string = "($numerador/$denominador)*$multiplicador";
                        }else{
                          $auxiliar = strval($value0->formula_cumplimiento);

                          $auxiliar = str_replace ('$denominador' , $denominador , $auxiliar);
                          $auxiliar= str_replace ('$numerador' , $numerador , $auxiliar);
                          $auxiliar= str_replace ('$multiplicador' , $multiplicador , $auxiliar);
                          $auxiliar= str_replace ('$meta' , $meta , $auxiliar);
                          $auxiliar= str_replace ('$porcentaje' , $porcentaje , $auxiliar);

                          $value0->formula_cumplimiento_string = $auxiliar;
                        }


                      }else{
                        $formulaCumplimiento = 0;
                      }
                      eval('$resultado =' .$formulaCumplimiento.';');

                      $arregloGraficaCumplimiento[$value0->id][$calendarioMesActual->mes] = round( $resultado, 2);

                      $validacionesIndicador = Condiciones_formulas_indicadores::where('tipo_formula' , 'formula_ponderador')->where('id_indicador' , $value0->id)->where('estatus', 1)->get();
                      foreach($validacionesIndicador as $indiceValidacion => $valorValidacion ){
                          eval('$resultadoTempVal =' .$valorValidacion->validacion.';');
                          if(!$resultadoTempVal){
                              $pasarCalculo = false;
                              break;
                          }
                      }


                      if($value0->formula_ponderador  && $pasarCalculo){
                        $formulaPonderador = $value0->formula_ponderador;
                      }else{
                        $formulaPonderador = 0;
                      }

                       eval('$resultadoP =' .$formulaPonderador.';');
                       $value0->ponderador_calculado = round($resultadoP) ;
                       $value0->ponderador_calculado_otro = number_format($this->numberFormatPrecision($resultadoP, 1, "."), 1, ",", ".") ;
                       
                      $validacionesIndicador2 = Condiciones_formulas_indicadores::where('tipo_formula' , 'formula_meta')->where('id_indicador' , $value0->id)->where('estatus', 1)->get();
                      if(count($validacionesIndicador2)>0){
                          foreach($validacionesIndicador2 as $indiceValidacion2 => $valorValidacion2 ){
                              eval('$resultadoTempVal2 =' .$valorValidacion2->validacion.';');
                              if(!$resultadoTempVal2){
                                  $pasarCalculoMeta = false;
                                  break;
                              }
                          }
                      }
                      if($value0->formula_calculo_meta && $pasarCalculoMeta){
                        $formulaMeta = $value0->formula_calculo_meta;
                      }else{
                        $formulaMeta = 0;
                      }
                       eval('$resultadoM =' .$formulaMeta.';');
                       $value0->meta_calculada = round($resultadoM, 1) ;

                    }else{
                      $arregloGraficaCumplimiento[$value0->id][$calendarioMesActual->mes] =  round($resultado, 2);
                      $arregloGraficaPonderador[$value0->id][$calendarioMesActual->mes] = round($resultadoP, 2);
                    }

              }


          $indicadores = $indicadores->groupby('tipo_id');

          $inidicadoreAux = [];
          foreach ($indicadores as $key => $tipos) {
              $inidicadoreAux[Tipos::where('id' , $key)->first()->nombre.' '.Tipos::where('id' , $key)->first()->alias] = $indicadores[$key]->groupby('equipo_id');
              $arrayAuxiliarEquipo = $indicadores[$key]->groupby('equipo_id');
              $arrayAuxiliarEquipoTranformado = [];
              foreach ($arrayAuxiliarEquipo as $indiceequipo => $valorOindicador) {
                $arrayAuxiliarEquipoTranformado[Equipos::where('id' , $indiceequipo)->first()->nombre] =   $arrayAuxiliarEquipo[$indiceequipo];
              }
                $inidicadoreAux[Tipos::where('id' , $key)->first()->nombre.' '.Tipos::where('id' , $key)->first()->alias] = $arrayAuxiliarEquipoTranformado;
          }
          $indicadores = $inidicadoreAux;
      //  dd($inidicadores);

          switch ($tipoReporte) {
          case 'PDF':

            $data = [
              'indicadores' => $indicadores,
              'arregloGraficaCumplimiento' =>$arregloGraficaCumplimiento,
              'mesO' => $mesO,
              'nombreMes' => $this->getMEsRodrigo($mesO)
          ];
//dd($data);
            $pdf = PDF::loadView('sigc.sigc_reportes_indicadores_pdf', $data)
            ->setPaper('legal', 'portrait');
            return $pdf->download('SIGC_INDICADORES.pdf'); //construcción PDF

          break;
          case 'XLS':
                $nombreMes = $this->getMEsRodrigo($mesO);

                      Excel::create("IndicadoresReporte", function ($excel) use ($indicadores , $arregloGraficaCumplimiento, $mesO, $nombreMes) {
                      $excel->setTitle("IndicadoresReporte");
                      $excel->sheet("IndicadoresReporte", function ($sheet) use ($indicadores , $arregloGraficaCumplimiento, $mesO, $nombreMes) {
                      $sheet->loadView('sigc.sigc_reporte_indicadores_ex_excel')->with('indicadores', $indicadores)->with('arregloGraficaCumplimiento', $arregloGraficaCumplimiento)->with('mesO', $mesO)->with('nombreMes', $nombreMes);
                  });
              })->download('xlsx');

                  return back();

                  break;
          default:
          break;
          }

          }

    public function getMEsRodrigo($mes){

    switch ($mes) {
    case 1:
        return 'Enero';
        break;
    case 2:
        return 'Febrero';
        break;
    case 3:
        return 'Marzo';
        break;
    case 4:
        return 'Abril';
        break;
    case 5:
         return 'Mayo';
         break;
    case 6:
          return 'Junio';
          break;
    case 7:
          return 'Julio';
          break;
    case 8:
          return 'Agosto';
          break;
    case 9:
          return 'Septiembre';
          break;
    case 10:
          return 'Octubre';
          break;
    case 11:
          return 'Noviembre';
          break;
    case 12:
          return 'Diciembre';
          break;

    default:
      return "No encontrado";
     }

    }

    
    public function TodosEquipos(){
      $equipos = Equipos::where('ano' , 2022)->orderby('id', 'desc')->get();
      $arrayEquipos = [];
      foreach ($equipos as $key => $value) {
        $arrayEquipos[$value->id] = $value->ano.' - '.$value->id.' - '.$value->nombre;
      }
      return $arrayEquipos;
    }

    public function getEquiposForTipo($tipo){
        $indicadoresPT = Indicadores::select('equipo_id')->where('tipo_id' , $tipo)->get()->pluck('equipo_id')->toArray();
        $equipos = Equipos::where('ano' , 2022)->whereIn('id' , $indicadoresPT)->orderby('id', 'desc')->get();
        $arrayEquipos = [];
        foreach ($equipos as $key => $value) {
          $arrayEquipos[$value->id] = $value->ano.' - '.$value->id.' - '.$value->nombre;
        }
        return $arrayEquipos;
    }

    public function TodasDivisiones(){
      $divisiones = Divisiones::get();
      $arrayDiv = [];
      foreach ($divisiones as $key => $value) {
        $arrayDiv[$value->id] = $value->ano.''.$value->id.' - '.$value->nombre;
      }
      return $arrayDiv;
    }
    
    public function getDivisionesForTipo($tipo){
      $indicadoresPT = Indicadores::select('division_id')->where('tipo_id' , $tipo)->get()->pluck('division_id')->toArray();
      $divisiones = Divisiones::whereIn('id' , $indicadoresPT)->get();
      $arrayDiv = [];
      foreach ($divisiones as $key => $value) {
        $arrayDiv[$value->id] = $value->ano.''.$value->id.' - '.$value->nombre;
      }
      return $arrayDiv;
    }
     
    public function getDivisionesForEquipo($equipo){
      $indicadoresPT = Indicadores::select('division_id')->where('equipo_id' , $equipo)->get()->pluck('division_id')->toArray();
      $divisiones = Divisiones::whereIn('id' , $indicadoresPT)->get();
      $arrayDiv = [];
      foreach ($divisiones as $key => $value) {
        $arrayDiv[$value->id] = $value->ano.''.$value->id.' - '.$value->nombre;
      }
      return $arrayDiv;
    }
    
    function numberFormatPrecision($number, $precision = 2, $separator = '.')
      {
          $numberParts = explode($separator, $number);
          $response = $numberParts[0];
          if(count($numberParts)>1){
              $response .= $separator;
              $response .= substr($numberParts[1], 0, $precision);
          }
          return $response;
      }


}
