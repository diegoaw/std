<?php

namespace App\Http\Controllers\Sigc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Sigc\Mediciones;
use App\Models\Sigc\MedicionEventos;
use App\Models\Sigc\MedicionHistoricos;
use Carbon\Carbon;
use App\Models\Sigc\Indicadores;
use App\Models\Sigc\Tipos;
use App\Models\Sigc\Equipos;
use App\Models\Sigc\Meses;
use App\Models\Sigc\MedicionArchivos;


class MedicionesController extends Controller
{

    public function index(Request $request)
    {
      set_time_limit(14200);
        $dataDefinitiva = [];
        $dataFiltrada = [];

        $tipoView = $request->tipo;
        $equipoView = $request->equipo;
        $mesView = $request->mes;
        $indicadorView = $request->indicador;
        $anoView = $request->ano;
        $selectAno[null] = 'SELECCIONE AÑO'; foreach (Indicadores::whereNotNull('ano')->orderby('ano', 'desc')->distinct()->get() as $key0 => $value0) { $selectAno[$value0->ano]=$value0->ano;}
        $selectMes[null] = 'SELECCIONE MES'; foreach (Meses::get() as $key3 => $value3) { $selectMes[$value3->id]=$value3->mesnombre;}
        $selectTipo[null] = 'SELECCIONE TIPO'; foreach (Tipos::orderby('id', 'desc')->get() as $key1 => $value1) { $selectTipo[$value1->id]=$value1->aliasc;}
        $selectEquipo[null] = 'SELECCIONE EQUIPO'; foreach (Equipos::orderby('id', 'desc')->get() as $key2 => $value2) { $selectEquipo[$value2->id]=$value2->nombrecompleto;}
        //$selectIndicador[null] = "SELECCIONE INDICADOR"; foreach (Indicadores::orderby('id', 'desc')->get() as $llave => $valor) {  $selectIndicador[$valor->id] = $valor->Nombrecompleto;}
        $selectIndicador[null] = "SELECCIONE INDICADOR"; foreach (Mediciones::orderby('indicador_id', 'desc')->get() as $llave => $valor) {  $selectIndicador[$valor->indicador_id] = $valor->Nombrecompletos;}

        $query = Mediciones::query();
        $query->whereHas('Indicador', function ($q) { $q->where('ano' , date("Y"));  });
        $query->when($indicadorView, function ($q, $indicadorView) {
              return $q->where('indicador_id', $indicadorView);
        });
        $query->when($equipoView, function ($q, $equipoView) {
              return $q->whereHas('Indicador', function ($q) use ($equipoView) {  $q->where('equipo_id' ,  $equipoView); });
        });
        $query->when($tipoView, function ($q, $tipoView) {
              return $q->whereHas('Indicador', function ($q) use ($tipoView) {  $q->where('tipo_id' ,  $tipoView); });
        });
        $query->when($mesView, function ($q, $mesView) {
              return $q->whereHas('Calendario', function ($q) use ($mesView) {  $q->where('mes' ,  $mesView); });
        });
        $query->orderby('id', 'desc');
        $dataDefinitiva =  $query->paginate(29);



        $query1 = Mediciones::query();
          if($anoView){
              $query1->whereHas('Indicador', function ($q) use ($anoView) { $q->where('ano', $anoView ); });
          }else{
            $query1->whereHas('Indicador', function ($q) { $q->where('ano' , (date("Y")-1));  });
          }

        $query1->when($indicadorView, function ($q, $indicadorView) {
              return $q->where('indicador_id', $indicadorView);
        });
        $query1->when($equipoView, function ($q, $equipoView) {
              return $q->whereHas('Indicador', function ($q) use ($equipoView) {  $q->where('equipo_id' ,  $equipoView); });
        });
        $query1->when($tipoView, function ($q, $tipoView) {
              return $q->whereHas('Indicador', function ($q) use ($tipoView) {  $q->where('tipo_id' ,  $tipoView); });
        });
        $query1->when($mesView, function ($q, $mesView) {
              return $q->whereHas('Calendario', function ($q) use ($mesView) {  $q->where('mes' ,  $mesView); });
        });
        $query1->orderby('id', 'desc');
        $dataDefinitivaO =  $query1->paginate(12);

        //$dataDefinitiva = Mediciones::whereHas('Indicador', function ($q) { $q->where('ano' , date("Y"));  })->orderby('id', 'desc')->get();
        //$dataDefinitivaO = Mediciones::whereHas('Indicador', function ($q) { $q->where('ano', '<' , date("Y"));  })->orderby('id', 'desc')->get();


      return view('sigc.sigc_mediciones')
      ->with('dataDefinitiva' , $dataDefinitiva)
      ->with('dataDefinitivaO' , $dataDefinitivaO)
      ->with('selectTipo' , $selectTipo)
      ->with('selectAno' , $selectAno)
      ->with('selectMes' , $selectMes)
      ->with('selectEquipo' , $selectEquipo)
      ->with('tipoView' , $tipoView)
      ->with('anoView' , $anoView)
      ->with('mesView' , $mesView)
      ->with('equipoView' , $equipoView)
      ->with('indicadorView' , $indicadorView)
      ->with('selectIndicador' , $selectIndicador);
    }

    public function detalle($id)
    {
        $client = new Client();

               try {
              $request = $client->get("http://10.100.23.13/webserviceautenticateactived/public/api/Get/Mediciones/Detalle/$id" , ['verify' => false]);
              $response = $request->getBody()->getContents();
              $data =  json_decode($response);
                }catch (BadResponseException   $e) {
                    $response = $e->getResponse();
                }
        if ($data->fecha <'2020-11-01 00:00:00.000'){
          $data->plataforma = 'v';
        }else{
          $data->plataforma = 'n';
        }

        //$data->filename = str_replace('"' , $data->filename);

       return view('sigc.sigc_mediciones_detalle')
      ->with('data' ,$data);
    }

    public function historico(Request $request)
    {
        set_time_limit(14200);
        $dataDefinitiva = [];
        $dataSinFiltro = [];
        $dataFiltrada = [];
        $mesView = $request->mes;
        $selectMes = [''=>'Seleccione Mes','enero'=>'enero','febrero'=>'febrero','marzo'=>'marzo','abril'=>'abril','mayo'=>'mayo','junio'=>'junio','julio'=>'julio','agosto'=>'agosto','septiembre'=>'septiembre','octubre'=>'octubre','noviembre'=>'noviembre','diciembre'=>'diciembre'];
        $tipoView = $request->tipo;
        $selectTipo [0] = 'SELECCIONE TIPO INDICADOR';
        $equipoView = $request->equipo;
        $selectEquipo [0] = 'SELECCIONE EQUIPO';


        $client = new Client();

            try {
              $request = $client->get("http://10.100.23.13/webserviceautenticateactived/public/api/Get/Mediciones" , ['verify' => false]);
              $response = $request->getBody()->getContents();
              $data =  json_decode($response);
            }catch (BadResponseException   $e) {
              $response = $e->getResponse();
          }

          $client2 = new Client();

            try {
              $request2 = $client2->get("http://10.100.23.13/webserviceautenticateactived/public/api/Get/TiposIndicadoresT" , ['verify' => false]);
              $response2 = $request2->getBody()->getContents();
              $data2 =  json_decode($response2);
             // $selectTipo = $data2;
            }catch (BadResponseException   $e) {
              $response2 = $e->getResponse();
          }

        foreach ($data2 as $llaveS => $valueS) {
           $selectTipo[$valueS->valor] = $valueS->etiqueta;
        }


        $client3 = new Client();

            try {
              $request3 = $client3->get("http://10.100.23.13/webserviceautenticateactived/public/api/Get/EquiposA" , ['verify' => false]);
              $response3 = $request3->getBody()->getContents();
              $data3 =  json_decode($response3);
            }catch (BadResponseException   $e) {
              $response3 = $e->getResponse();
          }

           foreach ($data3 as $llaveS => $valueS) {
           $selectEquipo[$valueS->id] = $valueS->nombre;
        }


       foreach ($data as $key => $value) {

        $objeto = new Mediciones();
        $objeto->id = $value->id;
        $objeto->calendario_id = $value->calendario_id;
        $objeto->indicador_id = $value->indicador_id;
        $objeto->fecha = $value->fecha;
        setlocale(LC_ALL, 'es_ES');
        // $mes = $fecha->formatLocalized('%B');// mes en idioma español
        $objeto->mes = Carbon::parse($value->fecha)->formatLocalized('%B');
        $objeto->valor_numerador = $value->valor_numerador;
        $objeto->analisis = $value->analisis;
        $objeto->acciones = $value->acciones;
        $objeto->medicion_estado_id = $value->medicion_estado_id;
        $objeto->revision_fecha = $value->revision_fecha;
        $objeto->revision_obs = $value->revision_obs;
        $objeto->denominador = $value->denominador;
        $objeto->nombre_indicador = $value->nombre_indicador;
        $objeto->nombre_medicion_estado = $value->nombre_medicion_estado;
        $objeto->indicadores_tipo_id = $value->indicador->tipo_id;
        $objeto->indicadores_equipo_id = $value->indicador->equipo_id;
        $dataFiltrada[$key] = $objeto;
       }

       $dataDefinitiva = collect($dataFiltrada);
       //dd($dataDefinitiva);

        if ($mesView != null && $mesView != '' ) {
         $dataDefinitiva = $dataDefinitiva->where('mes', $mesView);
        }
        if ($tipoView != null && $tipoView != 0 ) {
         $dataDefinitiva = $dataDefinitiva->where('indicadores_tipo_id', $tipoView);
        }
        if ($equipoView != null && $equipoView != 0 ) {
         $dataDefinitiva = $dataDefinitiva->where('indicadores_equipo_id', $equipoView);
        }
        $dataSinFiltro = $dataDefinitiva;
        $dataDefinitiva = $dataDefinitiva->sortByDesc('indicador_id')->groupBy('indicador_id');



      return view('sigc.sigc_mediciones_historico')
      ->with('dataDefinitiva' , $dataDefinitiva)
      ->with('selectMes' , $selectMes)
      ->with('mesView' , $mesView)
      ->with('selectTipo' , $selectTipo)
      ->with('tipoView' , $tipoView)
      ->with('selectEquipo' , $selectEquipo)
      ->with('equipoView' , $equipoView)
      ->with('dataSinFiltro' , $dataSinFiltro);
    }

    public function historico_detalle($id)
    {
        $client = new Client();

               try {
              $request = $client->get("http://10.100.23.13/webserviceautenticateactived/public/api/Get/Mediciones/Detalle/$id" , ['verify' => false]);
              $response = $request->getBody()->getContents();
              $data =  json_decode($response);
                }catch (BadResponseException   $e) {
                    $response = $e->getResponse();
                }
        if ($data->fecha <'2020-11-01 00:00:00.000'){
          $data->plataforma = 'v';
        }else{
          $data->plataforma = 'n';
        }

        //$data->filename = str_replace('"' , $data->filename);

       return view('sigc.sigc_mediciones_historico_detalle')
      ->with('data' ,$data);
    }


    public function estado($id)
    {
     $dataDefinitiva = [];
     $dataDefinitiva = Mediciones::where('id', $id)->first();
     $dataDefinitivaHisto = MedicionHistoricos::where('medicion_id' , $id)->get();


    return view('sigc.sigc_mediciones_estado')
    ->with('dataDefinitiva' , $dataDefinitiva)
    ->with('dataDefinitivaHisto' , $dataDefinitivaHisto);

    }

    public function report()
    {
      return view('sigc.sigc_mediciones_reportar');
    }

    public function reported(MedicionesRequest $request)
   {
     //$nombre = $request->nombre;
     //$nuevoAmbito = new AmbitosTabla();
     //$nuevoAmbito->nombre = $nombre;
     //$nuevoAmbito->save();
     //alert()->success('!','Reporte creado exitosamente');
     return redirect()->route('sigc_mediciones');
   }
   public function descargar($id){

      $datosArchivo = MedicionArchivos::where('id' , $id)->first();
      $file = $datosArchivo->filename_aws;
      $filename = str_replace('"' , '' , $datosArchivo->filename);

      header("Content-type: application/octet-stream");
      header("Content-Type: application/force-download");
      header("Content-Disposition: attachment; filename=\"$filename\"\n");
      readfile($file);
   }
   public function getTipoPorAno($ano){
    $tipos = Tipos::where('ano' , $ano)->get();
    $arrayTipos = [];
    foreach ($tipos as $key => $value) {
      $arrayTipos[$value->id] = $value->ano.' '.$value->id.' '.$value->alias;
  }
  return $arrayTipos;
 }

 public function getEquipoPorAno($ano){
  $equipos = Equipos::where('ano' , $ano)->get();
  $arrayEquipos = [];
  foreach ($equipos as $key => $value) {
    $arrayEquipos[$value->id] = $value->ano.' - '.$value->id.' - '.$value->nombre;
  }
  return $arrayEquipos;
}

public function getIndicadoresSelect($ano, $tipo , $equipo){

      $anoView = $ano == 0 ? null : $ano;
      $tipoView = $tipo == 0 ? null : $tipo;
      $equipoView = $equipo == 0 ? null : $equipo;

      $query = Indicadores::query();
      $query->when($anoView, function ($q, $anoView) {
            return $q->where('ano', $anoView);
      });
      $query->when($tipoView, function ($q, $tipoView) {
            return $q->where('tipo_id', $tipoView);
      });
      $query->when($equipoView, function ($q, $equipoView) {
            return $q->where('equipo_id', $equipoView);
      });

      $query->orderby('id', 'desc');
      $indicadores =  $query->get();

      $arrayIndicadores = [];
      foreach ($indicadores as $key => $value) {
        $arrayIndicadores[$value->id] = $value->ano.' - '.$value->id.' - '.$value->nombre;
      }
      return $arrayIndicadores;
    }

}
