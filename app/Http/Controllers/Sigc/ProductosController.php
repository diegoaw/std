<?php

namespace App\Http\Controllers\Sigc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Sigc\Productos;
use App\Models\Sigc\ProductosTabla;
use App\Http\Requests\SIGC\ProductosRequest;

class ProductosController extends Controller
{

    public function index(Request $request)
    {
       $dataDefinitiva = [];

       $requestnombreProducto = $request->nombre;
       $requestanoProducto = $request->ano;
       $requestordenProducto = $request->orden;
       $dataDefinitiva = Productos::orderby('ano' , 'desc')->get();


      return view('sigc.sigc_productos')
      ->with('dataDefinitiva' , $dataDefinitiva)
      ->with('requestnombreProducto' , $requestnombreProducto)
      ->with('requestanoProducto' , $requestanoProducto)
      ->with('requestordenProducto' , $requestordenProducto);
    }

    public function create()
    {
      $selectAno = [''=>'Seleccione Año','2021'=>'2021','2022'=>'2022'];

      return view('sigc.sigc_crear_producto')
      ->with('selectAno' , $selectAno);
    }

     public function store(ProductosRequest $request)
    {
      $nombre = $request->nombre;
      $ano = $request->ano;
      $orden = $request->orden;
      $nuevoProducto = new ProductosTabla();
      $nuevoProducto->nombre = $nombre;
      $nuevoProducto->ano = $ano;
      $nuevoProducto->orden = $orden;
      $nuevoProducto->save();
      alert()->success('!','Producto creado exitosamente');
      return redirect()->route('sigc_productos');
    }

    public function edit($id)
    {
      $selectAno = [''=>'Seleccione Año','2021'=>'2021','2022'=>'2022'];
      $editProducto = ProductosTabla::where('id' , $id)->first();
      return view('sigc.sigc_editar_producto')
      ->with('selectAno' , $selectAno)
      ->with('editProducto' , $editProducto);
    }

     public function update(ProductosRequest $request, $id)
    {
      $nombre = $request->nombre;
      $ano = $request->ano;
      $orden = $request->orden;
      $nuevoProducto = ProductosTabla::where('id' , $id)->first();
      $nuevoProducto->nombre = $nombre;
      $nuevoProducto->ano = $ano;
      $nuevoProducto->orden = $orden;
      $nuevoProducto->save();
      alert()->success('!','Producto editado exitosamente');
      return redirect()->route('sigc_productos');
    }

}
