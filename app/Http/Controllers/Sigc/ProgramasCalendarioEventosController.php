<?php

namespace App\Http\Controllers\Sigc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Sigc\Meses;
use Carbon\Carbon;
use App\Models\Sigc\ProgramasCalendarioEventos;
use App\Models\Sigc\ProgramasEventos;
use App\Models\Sigc\ProgramasEventosFechas;
use App\Models\Sigc\ProgramasMesesReportes;
use App\Http\Requests\SIGC\CalendarioProgramasRequest;
use Illuminate\Support\Facades\Auth;
use App\Models\Sigc\Users;
use DB;
use App\Models\Sigc\Notificaciones;
use App\Models\Sigc\CorreoNotificaciones;
use App\Mail\TestEmail;
use Mail;
use App\Mail\NotificacionNuevoCalendarioEventoProgramaSocialesEmail;
use App\Mail\NotificacionNuevaEdicionCalendarioEventoProgramaSocialesEmail;
use App\Mail\NotificacionEventosProgramasSocialesEmail;


class ProgramasCalendarioEventosController extends Controller
{

    public function index(){
        $dataDefinitivaA = ProgramasCalendarioEventos::where('ano' , (date("Y")))->orderby('mes' , 'desc')->get();
        $dataDefinitivaO = ProgramasCalendarioEventos::where('ano' , '<=' , date("Y")-1)->orderby('id' , 'desc')->paginate(12);

      return view('sigc.programas_calendario_eventos')
      ->with('dataDefinitivaA' , $dataDefinitivaA)
      ->with('dataDefinitivaO' , $dataDefinitivaO);
    }

    public static function spanishMonth($month){
        $mes = $month;
        if ($month=="Jan") {
          $mes = "Enero";
        }
        elseif ($month=="Feb")  {
          $mes = "Febrero";
        }
        elseif ($month=="Mar")  {
          $mes = "Marzo";
        }
        elseif ($month=="Apr") {
          $mes = "Abril";
        }
        elseif ($month=="May") {
          $mes = "Mayo";
        }
        elseif ($month=="Jun") {
          $mes = "Junio";
        }
        elseif ($month=="Jul") {
          $mes = "Julio";
        }
        elseif ($month=="Aug") {
          $mes = "Agosto";
        }
        elseif ($month=="Sep") {
          $mes = "Septiembre";
        }
        elseif ($month=="Oct") {
          $mes = "Octubre";
        }
        elseif ($month=="Oct") {
          $mes = "December";
        }
        elseif ($month=="Dec") {
          $mes = "Diciembre";
        }
        else {
          $mes = $month;
        }
        return $mes;
    }

    public function create(){
      $selectAno = [''=>'SELECCIONE AÑO','2022'=>'2022', '2023'=>'2023','2024'=>'2024','2025'=>'2025'];
      //$selectMes = [''=>'Seleccione mes','1'=>'Enero','2'=>'Febrero', '3'=>'Marzo' , '4'=>'Abril' , '5'=>'mayo' , '6'=>'junio' , '7'=>'Julio' , '8'=>'Agosto' , '9'=>'septiembre' , '10'=>'Octubre' , '11'=>'Noviembre' , '12'=>'Diciembre'];
      $selectMes[null] = 'SELECCIONE MES'; foreach (ProgramasMesesReportes::orderby('id', 'asc')->get() as $key1 => $value1) { $selectMes[$value1->id]=$value1->nombre;}
      $selectEvento[null] = 'SELECCIONE EVENTO'; foreach (ProgramasEventos::orderby('id', 'asc')->get() as $key2 => $value2) { $selectEvento[$value2->id]=$value2->nombre;}
      $fechas[1] = Date('Y-m-d', strtotime( '2022-01-01' ));
      $fechas[0] = Date('Y-m-d', strtotime('2025-12-31'));
      $fe_desde =Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
      $fe_hasta =Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
        return view('sigc.programas_calendario_eventos_create')
        ->with('fechas' , $fechas)
        ->with('fe_hasta' , $fe_hasta)
        ->with('fe_desde' , $fe_desde)
        ->with('selectMes' , $selectMes)
        ->with('selectEvento' , $selectEvento)
        ->with('selectAno' , $selectAno);
    }

    public function store(CalendarioProgramasRequest $request){

        $nuevoCalendarioEvento = new ProgramasCalendarioEventos();
        $nuevoCalendarioEvento->ano =$request->ano;
        $nuevoCalendarioEvento->mes =$request->mes;
        $nuevoCalendarioEvento->id_programas_eventos =$request->id_programas_eventos;
        $nuevoCalendarioEvento->fecha_inicio =Date('Ymd', strtotime( $request->fecha_inicio ));
        $nuevoCalendarioEvento->fecha_fin =Date('Ymd', strtotime( $request->fecha_fin ));
        $nuevoCalendarioEvento->save();

        $calendario = $nuevoCalendarioEvento->id;
        $evento = ProgramasEventos::where('id', $nuevoCalendarioEvento->id_programas_eventos)->first();
        $title = $evento->nombre;
        $description = $evento->descripcion;
        $class = $evento->class;
        $etapa_id = $nuevoCalendarioEvento->id_programas_eventos;

        $nuevoEvento = new ProgramasEventosFechas();
        $nuevoEvento->title = $title;
        $nuevoEvento->description = $description;
        $nuevoEvento->start_date = Date('Ymd', strtotime( $request->fecha_inicio ));
        $nuevoEvento->end_date = Date('Ymd', strtotime( $request->fecha_fin ));
        $nuevoEvento->programas_calendario_eventos_id = $calendario;
        $nuevoEvento->class = $class;
        $nuevoEvento->etapa_id = $etapa_id;
        $nuevoEvento->save();


         if ($nuevoCalendarioEvento){
          $this->notificacionCrearCalendario($nuevoCalendarioEvento);
        } 

        alert()->success('!','Calendario Evento creado exitosamente');
        return redirect()->route('programas_calendario_eventos');
    }

     public function notificacionCrearCalendario($nuevoCalendarioEvento){
       $emails = ['belen.pavez@ind.cl','jorge.vergara@ind.cl','nicolas.prenafeta@mindep.cl','javier.saez@mindep.cl'];
       $datos = $nuevoCalendarioEvento;

       foreach ($emails as $key => $correo) {
         Mail::to($correo)->send(new NotificacionNuevoCalendarioEventoProgramaSocialesEmail($datos));
         $notificacionesId = 13;
         $nuevoCorreo = new CorreoNotificaciones();
         $nuevoCorreo->id_notificaciones = $notificacionesId;
         $nuevoCorreo->destintario = $correo;
         $nuevoCorreo->save();
       }
     } 

      public function edit($id){
      $editCalendario = ProgramasCalendarioEventos::where('id' , $id)->first();
      $selectAno = [''=>'SELECCIONE AÑO','2022'=>'2022', '2023'=>'2023','2024'=>'2024','2025'=>'2025'];
      //$selectMes = [''=>'Seleccione mes','1'=>'Enero','2'=>'Febrero', '3'=>'Marzo' , '4'=>'Abril' , '5'=>'mayo' , '6'=>'junio' , '7'=>'Julio' , '8'=>'Agosto' , '9'=>'septiembre' , '10'=>'Octubre' , '11'=>'Noviembre' , '12'=>'Diciembre'];
      $selectMes[null] = 'SELECCIONE MES'; foreach (ProgramasMesesReportes::orderby('id', 'asc')->get() as $key1 => $value1) { $selectMes[$value1->id]=$value1->nombre;}
      $selectEvento[null] = 'SELECCIONE EVENTO'; foreach (ProgramasEventos::orderby('id', 'asc')->get() as $key2 => $value2) { $selectEvento[$value2->id]=$value2->nombre;}
      $fechas[1] = Date('Y-m-d', strtotime( '2022-01-01' ));
      $fechas[0] = Date('Y-m-d', strtotime('2025-12-31'));
      $fe_desde =Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
      $fe_hasta =Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
        return view('sigc.programas_calendario_eventos_editar')
        ->with('editCalendario' , $editCalendario)
        ->with('fechas' , $fechas)
        ->with('fe_hasta' , $fe_hasta)
        ->with('fe_desde' , $fe_desde)
        ->with('selectMes' , $selectMes)
        ->with('selectEvento' , $selectEvento)
        ->with('selectAno' , $selectAno);
    }

    public function update (CalendarioProgramasRequest $request, $id){
      
        $EditarCalendarioEvento = ProgramasCalendarioEventos::where('id' , $id)->first();
        $EditarCalendarioEvento->ano =$request->ano;
        $EditarCalendarioEvento->mes =$request->mes;
        $EditarCalendarioEvento->id_programas_eventos =$request->id_programas_eventos;
        $EditarCalendarioEvento->fecha_inicio =Date('Ymd', strtotime( $request->fecha_inicio ));
        $EditarCalendarioEvento->fecha_fin =Date('Ymd', strtotime( $request->fecha_fin ));
        $EditarCalendarioEvento->save();

        $editarEvento = ProgramasEventosFechas::where('programas_calendario_eventos_id' , $id)->first();
        $editarEvento->start_date = Date('Ymd', strtotime( $request->fecha_inicio ));
        $editarEvento->end_date = Date('Ymd', strtotime( $request->fecha_fin ));
        $editarEvento->save(); 

         if ($EditarCalendarioEvento){
          $this->notificacionEditarCalendario($EditarCalendarioEvento);
        } 

        alert()->success('!','Calendario Evento editado exitosamente');
        return redirect()->route('programas_calendario_eventos');
    }

   public function notificacionEditarCalendario($EditarCalendarioEvento){
      $emails = ['belen.pavez@ind.cl','jorge.vergara@ind.cl','nicolas.prenafeta@mindep.cl','javier.saez@mindep.cl'];
      $datos = $EditarCalendarioEvento;

      foreach ($emails as $key => $correo) {
        Mail::to($correo)->send(new NotificacionNuevaEdicionCalendarioEventoProgramaSocialesEmail($datos));
        $notificacionesId = 14;
        $nuevoCorreo = new CorreoNotificaciones();
        $nuevoCorreo->id_notificaciones = $notificacionesId;
        $nuevoCorreo->destintario = $correo;
        $nuevoCorreo->save();
      }
      } 


      public function notificacionEventos(){
        $hoy = Carbon::now()->format('Y-m-d');
        $hoy = $hoy.' 00:00:00';
        $hoy2 = Carbon::now()->format('Y-m-d');
        $eventos = ProgramasEventosFechas::where('start_date' , $hoy)->get();
  
        if ($eventos->count()>0) {
          foreach ($eventos as $key => $evento) {
            $datos = ['evento'=>$evento];
            $mesActual  =  date('n');
            $anoActual = date("Y");
      
           $emails = ['belen.pavez@ind.cl','jorge.vergara@ind.cl','nicolas.prenafeta@mindep.cl','javier.saez@mindep.cl','daniela.jimenez@mindep.cl'];
           // $emails = ['daniela.jimenez@mindep.cl'];
            
            foreach ($emails as $key => $correo) {
              sleep(1);
              Mail::to($correo)->send(new NotificacionEventosProgramasSocialesEmail($datos));
              $notificacionesId = 15;
              $nuevoCorreo = new CorreoNotificaciones();
              $nuevoCorreo->id_notificaciones = $notificacionesId;
              $nuevoCorreo->destintario = $correo;
              $nuevoCorreo->save();
            }
          }
        }
      }


      public function destroy(Request $request, $id){
        $eventoCalendario = ProgramasCalendarioEventos::where('id', $id)->first();
        $eventoFecha = ProgramasEventosFechas::where('programas_calendario_eventos_id', $id)->first();
        $eventoFecha->delete();
        $result = $eventoCalendario->delete();
          if ($result) {
              return response()->json(['success'=>'true']);
          }else{
              return response()->json(['success'=> 'false']);
          }
      }
}
