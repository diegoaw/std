<?php

namespace App\Http\Controllers\Sigc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Sigc\Programas;
use App\Models\Sigc\ProgramasComponentes;
use App\Http\Requests\SIGC\ComponentesProgramasRequest;

class ProgramasComponentesController extends Controller
{

    public function index(Request $request){

      /* $dataDefinitivaA = ProgramasComponentes::get();

      return view('sigc.programas_componentes')
      ->with('dataDefinitivaA' , $dataDefinitivaA); */

       $dataDefinitivaA = ProgramasComponentes::where('ano' , (date("Y")))->orderby('id' , 'desc')->get();
      $dataDefinitivaO = ProgramasComponentes::where('ano' , '<=' , date("Y")-1)->orderby('id' , 'desc')->paginate(12);

      return view('sigc.programas_componentes')
      ->with('dataDefinitivaA' , $dataDefinitivaA)
      ->with('dataDefinitivaO' , $dataDefinitivaO); 
      
    }


    public function create(){
      $selectAno = [''=>'SELECCIONE AÑO','2022'=>'2022', '2023'=>'2023'];
      $selectPrograma[null]  = 'SELECCIONE PROGRAMA'; foreach (Programas::orderby('id', 'desc')->get()as $key1 => $value1) { $selectPrograma[$value1->id]=$value1->nombre;}
      
      return view('sigc.programas_componentes_crear')
      ->with('selectPrograma' , $selectPrograma)
      ->with('selectAno' , $selectAno);
    }


     public function store(ComponentesProgramasRequest $request){
      $ano = $request->ano;
      $nombre = $request->nombre;
      $programa = $request->id_programa;
      $unidad = $request->unidad;
      $efectivo1 = $request->efectivo_1;
      $efectivo2 = $request->efectivo_2;
      $nuevoComponente = new ProgramasComponentes();
      $nuevoComponente->ano = $ano;
      $nuevoComponente->nombre = $nombre;
      $nuevoComponente->id_programa = $programa;
      $nuevoComponente->unidad = $unidad;
      $nuevoComponente->efectivo_1 = $efectivo1;
      $nuevoComponente->efectivo_2 = $efectivo2;
      $nuevoComponente->save();
      alert()->success('!','Componente creado exitosamente');
      return redirect()->route('programas_componentes');
    }

    public function edit($id){
      $editComponente = ProgramasComponentes::where('id' , $id)->first();
      $selectAno = [''=>'SELECCIONE AÑO','2022'=>'2022', '2023'=>'2023'];
      $selectPrograma[null]  = 'SELECCIONE PROGRAMA'; foreach (Programas::orderby('id', 'desc')->get()as $key1 => $value1) { $selectPrograma[$value1->id]=$value1->nombre;}
      
      return view('sigc.programas_componentes_editar')
      ->with('editComponente' , $editComponente)
      ->with('selectAno' , $selectAno)
      ->with('selectPrograma' , $selectPrograma);
    }


     public function update(ComponentesProgramasRequest $request, $id){
      $ano = $request->ano;
      $nombre = $request->nombre;
      $programa = $request->id_programa;
      $unidad = $request->unidad;
      $efectivo1 = $request->efectivo_1;
      $efectivo2 = $request->efectivo_2;
      $editComponente = ProgramasComponentes::where('id' , $id)->first();
      $editComponente->ano = $ano;
      $editComponente->nombre = $nombre;
      $editComponente->id_programa = $programa;
      $editComponente->unidad = $unidad;
      $editComponente->efectivo_1 = $efectivo1;
      $editComponente->efectivo_2 = $efectivo2;
      $editComponente->save();
      alert()->success('!','Componente editado exitosamente');
      return redirect()->route('programas_componentes');
    }

    public function destroy(Request $request, $id){
      $Componente = ProgramasComponentes::where('id', $id)->first();

      $result = $Componente->delete();
        if ($result) {
            return response()->json(['success'=>'true']);
        }else{
            return response()->json(['success'=> 'false']);
        }
    }

}
