<?php

namespace App\Http\Controllers\Sigc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Sigc\Programas;
use App\Http\Requests\SIGC\ProgramasRequest;

class ProgramasController extends Controller
{

    public function index(Request $request){

      /* $dataDefinitivaA = Programas::get();

      return view('sigc.programas')
      ->with('dataDefinitivaA' , $dataDefinitivaA); */

      $dataDefinitivaA = Programas::where('ano' , (date("Y")))->orderby('id' , 'asc')->get();
      $dataDefinitivaO = Programas::where('ano' , '<=' , date("Y")-1)->orderby('id' , 'asc')->paginate(12);

      return view('sigc.programas')
      ->with('dataDefinitivaA' , $dataDefinitivaA)
      ->with('dataDefinitivaO' , $dataDefinitivaO); 
      
    }

    public function create(){

      $selectAno = [''=>'SELECCIONE AÑO','2022'=>'2022', '2023'=>'2023'];
      
      return view('sigc.programas_crear')
      ->with('selectAno' , $selectAno);
    }


     public function store(ProgramasRequest $request){
      $ano = $request->ano;
      $nombre = $request->nombre;
      $proposito = $request->proposito;
      $anoInicio = $request->ano_inicio;
      $nuevoPrograma = new Programas();
      $nuevoPrograma->ano = $ano;
      $nuevoPrograma->nombre = $nombre;
      $nuevoPrograma->proposito = $proposito;
      $nuevoPrograma->ano_inicio = $anoInicio;
      $nuevoPrograma->save();
      alert()->success('!','Programa creado exitosamente');
      return redirect()->route('programas');
    }

    public function edit($id){
      $editPrograma = Programas::where('id' , $id)->first();
      $selectAno = [''=>'SELECCIONE AÑO','2022'=>'2022', '2023'=>'2023'];
      
      return view('sigc.programas_editar')
      ->with('selectAno' , $selectAno)
      ->with('editPrograma' , $editPrograma);
    }


     public function update(ProgramasRequest $request, $id){
      $ano = $request->ano;
      $nombre = $request->nombre;
      $proposito = $request->proposito;
      $anoInicio = $request->ano_inicio;
      $editPrograma = Programas::where('id' , $id)->first();
      $editPrograma->ano = $ano;
      $editPrograma->nombre = $nombre;
      $editPrograma->proposito = $proposito;
      $editPrograma->ano_inicio = $anoInicio;
      $editPrograma->save();
      alert()->success('!','Programa editado exitosamente');
      return redirect()->route('programas');
    }

    public function destroy(Request $request, $id){
      $Programa = Programas::where('id', $id)->first();

      $result = $Programa->delete();
        if ($result) {
            return response()->json(['success'=>'true']);
        }else{
            return response()->json(['success'=> 'false']);
        }
    }

}
