<?php

namespace App\Http\Controllers\Sigc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Sigc\ProgramasEventos;
use App\Models\Sigc\Users;
use App\Http\Requests\SIGC\EventosProgramasRequest;

class ProgramasEventosController extends Controller
{

    public function index(Request $request)
    {

       $dataDefinitiva = ProgramasEventos::get();

      return view('sigc.programas_eventos')
      ->with('dataDefinitiva' , $dataDefinitiva);
    }


    public function create()
    {
      $varNull = null;
      $selectColor = [''=>'SELECCIONE COLOR EVENTO','badge badge-primary'=>'Azul', 'badge badge-warning'=>'Amarillo', 'badge badge-success'=>'Verde', 'badge badge-danger'=>'Rojo'];
      $selectEncargado[null] = 'SELECCIONE ENCARGADO'; foreach (Users::where('id', '!=', null)->where('estado', 1)->orderby('nombres', 'asc')->get() as $key1 => $value1) { $selectEncargado[$value1->id]=$value1->usuariosid;}
      $selectSuplente[null] = 'SELECCIONE SUPLENTE'; foreach (Users::where('id', '!=', null)->where('estado', 1)->orderby('nombres', 'asc')->get() as $key2 => $value2) { $selectSuplente[$value2->id]=$value2->usuariosid;}
      return view('sigc.programas_eventos_crear')
      ->with('varNull' , $varNull)
      ->with('selectColor' , $selectColor)
      ->with('selectEncargado' , $selectEncargado)
      ->with('selectSuplente' , $selectSuplente);
    }


     public function store(EventosProgramasRequest $request)
    {
      $nombre = $request->nombre;
      $descripcion = $request->descripcion;
      $class = $request->class;
      $encargado = $request->encargado;
      $suplente = $request->suplente;
      $nuevoEvento = new ProgramasEventos();
      $nuevoEvento->nombre = $nombre;
      $nuevoEvento->descripcion = $descripcion;
      $nuevoEvento->class = $class;
      $nuevoEvento->encargado = $encargado;
      $nuevoEvento->suplente = $suplente;
      $nuevoEvento->save();
      alert()->success('!','Evento creado exitosamente');
      return redirect()->route('programas_eventos');
    }

    public function edit($id)
    {
      $editEvento = ProgramasEventos::where('id' , $id)->first();
      $varNull = null;
      $selectColor = [''=>'SELECCIONE COLOR EVENTO','badge badge-primary'=>'Azul', 'badge badge-warning'=>'Amarillo', 'badge badge-success'=>'Verde', 'badge badge-danger'=>'Rojo'];
      $selectEncargado[null] = 'SELECCIONE ENCARGADO'; foreach (Users::where('id', '!=', null)->where('estado', 1)->orderby('nombres', 'asc')->get() as $key1 => $value1) { $selectEncargado[$value1->id]=$value1->usuariosid;}
      $selectSuplente[null] = 'SELECCIONE SUPLENTE'; foreach (Users::where('id', '!=', null)->where('estado', 1)->orderby('nombres', 'asc')->get() as $key2 => $value2) { $selectSuplente[$value2->id]=$value2->usuariosid;}
      return view('sigc.programas_eventos_editar')
      ->with('varNull' , $varNull)
      ->with('editEvento' , $editEvento)
      ->with('selectSuplente' , $selectSuplente)
      ->with('selectEncargado' , $selectEncargado)
      ->with('selectColor' , $selectColor);
    }


     public function update(EventosProgramasRequest $request, $id)
    {
      $nombre = $request->nombre;
      $descripcion = $request->descripcion;
      $class = $request->class;
      $encargado = $request->encargado;
      $suplente = $request->suplente;
      $editEvento = ProgramasEventos::where('id' , $id)->first();
      $editEvento->nombre = $nombre;
      $editEvento->descripcion = $descripcion;
      $editEvento->class = $class;
      $editEvento->encargado = $encargado;
      $editEvento->suplente = $suplente;
      $editEvento->save();
      alert()->success('!','Evento editado exitosamente');
      return redirect()->route('programas_eventos');
    }

    public function destroy(Request $request, $id){
      $evento = ProgramasEventos::where('id', $id)->first();

      $result = $evento->delete();
        if ($result) {
            return response()->json(['success'=>'true']);
        }else{
            return response()->json(['success'=> 'false']);
        }
    }

    public function habilitarSuplencia($id){
      $evento = ProgramasEventos::where('id' , $id)->first();
      $evento->habilitar_suplencia = 1;
      $evento->save();
      alert()->success('!','Suplencia para evento habilitada Exitosamente');
      return redirect()->route('programas_eventos');
   }
   public function deshabilitarSuplencia($id){
      $evento = ProgramasEventos::where('id' , $id)->first();
      $evento->habilitar_suplencia = 0;
      $evento->save();
      alert()->success('!','Suplencia para evento deshabilitada Exitosamente');
      return redirect()->route('programas_eventos');
   }

}
