<?php

namespace App\Http\Controllers\Sigc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Sigc\Programas;
use App\Models\Sigc\ProgramasPresupuesto;
use App\Http\Requests\SIGC\GastosProgramasRequest;

class ProgramasGastosController extends Controller
{

    public function index(Request $request){

      /* $dataDefinitivaA = ProgramasPresupuesto::get();

      return view('sigc.programas_presupuesto')
      ->with('dataDefinitivaA' , $dataDefinitivaA); */

      $dataDefinitivaA = ProgramasPresupuesto::where('ano' , (date("Y")))->orderby('id' , 'asc')->get();
      $dataDefinitivaO = ProgramasPresupuesto::where('ano' , '<=' , date("Y")-1)->orderby('id' , 'asc')->paginate(12);

      return view('sigc.programas_presupuesto')
      ->with('dataDefinitivaA' , $dataDefinitivaA)
      ->with('dataDefinitivaO' , $dataDefinitivaO);
      
    }


    public function create(){
      $selectAno = [''=>'SELECCIONE AÑO','2022'=>'2022', '2023'=>'2023'];
      $selectPrograma[null]  = 'SELECCIONE PROGRAMA'; foreach (Programas::orderby('id', 'desc')->get()as $key1 => $value1) { $selectPrograma[$value1->id]=$value1->nombre;}
      
      return view('sigc.programas_presupuesto_crear')
      ->with('selectPrograma' , $selectPrograma)
      ->with('selectAno' , $selectAno);
    }


     public function store(GastosProgramasRequest $request){
      $ano = $request->ano;
      $programa = $request->id_programa;
      $fecha = $request->fecha;
      $ley = $request->ley;
      $vigente = $request->vigente;
      $ejecutados = $request->ejecutados;
      $nuevoPresupuesto = new ProgramasPresupuesto();
      $nuevoPresupuesto->ano = $ano;
      $nuevoPresupuesto->id_programa = $programa;
      $nuevoPresupuesto->fecha = $fecha;
      $nuevoPresupuesto->ley = $ley;
      $nuevoPresupuesto->vigente = $vigente;
      $nuevoPresupuesto->ejecutados = $ejecutados;
      if ($fecha != date("Y")) {
        $nuevoPresupuesto->ejecucion = round(($ejecutados/$vigente)*100, 2);
      }
      $nuevoPresupuesto->save();
      alert()->success('!','Presupuesto creado exitosamente');
      return redirect()->route('programas_presupuesto');
    }

    public function edit($id){
      $editPresupuesto = ProgramasPresupuesto::where('id' , $id)->first();
      $selectAno = [''=>'SELECCIONE AÑO','2022'=>'2022', '2023'=>'2023'];
      $selectPrograma[null]  = 'SELECCIONE PROGRAMA'; foreach (Programas::orderby('id', 'desc')->get()as $key1 => $value1) { $selectPrograma[$value1->id]=$value1->nombre;}
      
      return view('sigc.programas_presupuesto_editar')
      ->with('editPresupuesto' , $editPresupuesto)
      ->with('selectAno' , $selectAno)
      ->with('selectPrograma' , $selectPrograma);
    }


     public function update(GastosProgramasRequest $request, $id){
      $ano = $request->ano;
      $programa = $request->id_programa;
      $fecha = $request->fecha;
      $ley = $request->ley;
      $vigente = $request->vigente;
      $ejecutados = $request->ejecutados;
      $editPresupuesto = ProgramasPresupuesto::where('id' , $id)->first();
      $editPresupuesto->ano = $ano;
      $editPresupuesto->id_programa = $programa;
      $editPresupuesto->fecha = $fecha;
      $editPresupuesto->ley = $ley;
      $editPresupuesto->vigente = $vigente;
      $editPresupuesto->ejecutados = $ejecutados;
      if ($fecha != date("Y")) {
      $editPresupuesto->ejecucion = round(($ejecutados/$vigente)*100, 2);
      }
      $editPresupuesto->save();
      alert()->success('!','Presupuesto editado exitosamente');
      return redirect()->route('programas_presupuesto');
    }

    public function destroy(Request $request, $id){
      $presupuesto = ProgramasPresupuesto::where('id', $id)->first();

      $result = $presupuesto->delete();
        if ($result) {
            return response()->json(['success'=>'true']);
        }else{
            return response()->json(['success'=> 'false']);
        }
    }

}
