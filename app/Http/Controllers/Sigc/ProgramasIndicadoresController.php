<?php

namespace App\Http\Controllers\Sigc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Sigc\ProgramasIndicadores;
use App\Models\Sigc\Programas;
use App\Models\Sigc\ProgramasComponentes;
use App\Http\Requests\SIGC\IndicadoresProgramasRequest;

class ProgramasIndicadoresController extends Controller
{

    public function index(Request $request){

      /* $dataDefinitivaA = ProgramasIndicadores::get();

      return view('sigc.programas_indicadores')
      ->with('dataDefinitivaA' , $dataDefinitivaA); */

      $dataDefinitivaA = ProgramasIndicadores::where('ano' , (date("Y")))->orderby('id' , 'desc')->get();
      $dataDefinitivaO = ProgramasIndicadores::where('ano' , '<=' , date("Y")-1)->orderby('id' , 'desc')->paginate(12);

      return view('sigc.programas_indicadores')
      ->with('dataDefinitivaA' , $dataDefinitivaA)
      ->with('dataDefinitivaO' , $dataDefinitivaO);
      
    }


    public function create(){
      $selectAno = [''=>'SELECCIONE AÑO','2022'=>'2022', '2023'=>'2023'];
      $selectPrograma[null]  = 'SELECCIONE PROGRAMA'; foreach (Programas::orderby('id', 'desc')->get()as $key1 => $value1) { $selectPrograma[$value1->id]=$value1->nombre;}
      $selectComponente[null]  = 'SELECCIONE COMPONENTE';foreach (ProgramasComponentes::orderby('id', 'desc')->get() as $key2 => $value2) { $selectComponente[$value2->id]=$value2->nombre;}
      $selectProposito[null]  = 'SELECCIONE PROPÓSITO';foreach (Programas::orderby('id', 'desc')->get() as $key3 => $value3) { $selectProposito[$value3->id]=$value3->proposito;}
      
      return view('sigc.programas_indicadores_crear')
      ->with('selectAno' , $selectAno)
      ->with('selectPrograma' , $selectPrograma)
      ->with('selectComponente' , $selectComponente)
      ->with('selectProposito' , $selectProposito);
    }


     public function store(IndicadoresProgramasRequest $request){
      $nombre = $request->nombre;
      $ano = $request->ano;
      $programa = $request->id_programa;
      $componente = $request->id_componente;
      $efectivo1 = $request->efectivo_1;
      $efectivo2 = $request->efectivo_2;
      $proposito = $request->proposito;
      $nuevoIndicador = new ProgramasIndicadores();
      $nuevoIndicador->nombre = $nombre;
      $nuevoIndicador->ano = $ano;
      $nuevoIndicador->id_programa = $programa;

      if ($proposito == 0 ) {
        $nuevoIndicador->id_componente = $componente;
        $nuevoIndicador->id_proposito = 0;
      }else{
        $nuevoIndicador->id_componente = 0;
        $nuevoIndicador->id_proposito = 1;
      }

      $nuevoIndicador->efectivo_1 = $efectivo1;
      $nuevoIndicador->efectivo_2 = $efectivo2;
      $nuevoIndicador->save();
      alert()->success('!','Indicador creado exitosamente');
      return redirect()->route('programas_indicadores');
    }

    public function edit($id){
      $editIndicador = ProgramasIndicadores::where('id' , $id)->first();
      $selectAno = [''=>'SELECCIONE AÑO', '2022'=>'2022', '2023'=>'2023'];
      $selectPrograma[null]  = 'SELECCIONE PROGRAMA'; foreach (Programas::orderby('id', 'desc')->get()as $key1 => $value1) { $selectPrograma[$value1->id]=$value1->nombre;}
      $selectComponente[null]  = 'SELECCIONE COMPONENTE';foreach (ProgramasComponentes::orderby('id', 'desc')->get() as $key2 => $value2) { $selectComponente[$value2->id]=$value2->nombre;}
      $selectProposito[null]  = 'SELECCIONE PROPÓSITO';foreach (Programas::orderby('id', 'desc')->get() as $key3 => $value3) { $selectProposito[$value3->id]=$value3->proposito;}

      return view('sigc.programas_indicadores_editar')
      ->with('editIndicador' , $editIndicador)
      ->with('selectAno' , $selectAno)
      ->with('selectPrograma' , $selectPrograma)
      ->with('selectComponente' , $selectComponente)
      ->with('selectProposito' , $selectProposito);
    }


     public function update(IndicadoresProgramasRequest $request, $id){
      $nombre = $request->nombre;
      $ano = $request->ano;
      $programa = $request->id_programa;
      $componente = $request->id_componente;
      $efectivo1 = $request->efectivo_1;
      $efectivo2 = $request->efectivo_2;
      $proposito = $request->proposito;
      $editIndicador = ProgramasIndicadores::where('id' , $id)->first();
      $editIndicador->nombre = $nombre;
      $editIndicador->ano = $ano;
      $editIndicador->id_programa = $programa;

      if ($proposito == 0 ) {
        $editIndicador->id_componente = $componente;
        $editIndicador->id_proposito = 0;
      }else{
        $editIndicador->id_componente = 0;
        $editIndicador->id_proposito = 1;
      }

      $editIndicador->efectivo_1 = $efectivo1;
      $editIndicador->efectivo_2 = $efectivo2;
      $editIndicador->save();
      alert()->success('!','Indicador editado exitosamente');
      return redirect()->route('programas_indicadores');
    }

    public function destroy(Request $request, $id){
      $indicador = ProgramasIndicadores::where('id', $id)->first();

      $result = $indicador->delete();
        if ($result) {
            return response()->json(['success'=>'true']);
        }else{
            return response()->json(['success'=> 'false']);
        }
    }

}
