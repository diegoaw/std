<?php

namespace App\Http\Controllers\Sigc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Carbon\Carbon;
use App\Models\Sigc\ProgramasEventosFechas;
use App\Models\Sigc\ProgramasCalendarioEventos;
use App\Models\Sigc\ProgramasIndicadores;
use App\Http\Controllers\Sigc\CalendarioController;
use App\Models\Sigc\Programas;
use App\Models\Sigc\ProgramasComponentes;
use Maatwebsite\Excel\Facades\Excel;
use Dompdf\Dompdf;
use PDF;


class ProgramasInicioController extends Controller
{

    public function index(Request $request)
          {
            $month = date("Y-m");
            $data = $this->calendar_month($month);
            $mes = $data['month'];

            // obtener mes en espanol
            $mespanish = new CalendarioController();
            $mespanish = $mespanish->spanishMonth($mes);
            $mes = $data['month'];
            $mesActual = date("n");
            $start = Carbon::now()->startOfMonth();
            $end = Carbon::now()->endOfMonth();
            //dd($end);

            if(date("n") == 1){
              $mesO = 12;
              $anoO =  (date("Y")-1);
            }else{
              $mesO = (date("n")-1);
              $anoO =  date("Y");
            }
            //provivional
           // $mesO = 2;
            $query1 = ProgramasEventosFechas::query();
            $query1->whereHas('Calendario', function ($q) use ($start, $end) {  $q->whereBetween('fecha_inicio' ,  [$start, $end]); });
            $query1->whereHas('Calendario', function ($q) use ($anoO) {  $q->where('ano' ,  $anoO); });
            $eventosMes = $query1->get();

            //dd($mesO);

            /*********************indicadores tabla*********************/

            $dataDefinitiva = ProgramasIndicadores::where('ano', $anoO)->get();

            $programaView = $request->programa;
            $programaNombre = Programas::where('id' , $programaView)->first();
            $componenteView = $request->componente;
            $componenteNombre = ProgramasComponentes::where('id' , $componenteView)->first();
            $propositoView = $request->proposito;
            $propositoNombre = Programas::where('id' , $propositoView)->first();
            
            $selectPrograma[null]  = 'SELECCIONE PROGRAMA'; foreach (Programas::orderby('id', 'desc')->get()as $key1 => $value1) { $selectPrograma[$value1->id]=$value1->nombre;}
            $selectComponente[null]  = 'SELECCIONE COMPONENTE';foreach (ProgramasComponentes::orderby('id', 'desc')->get() as $key2 => $value2) { $selectComponente[$value2->id]=$value2->nombre;}
            $selectProposito[null]  = 'SELECCIONE PROPÓSITO'; foreach (Programas::orderby('id', 'desc')->get()as $key3 => $value3) { $selectProposito[$value3->id]=$value3->proposito;}

            $query = ProgramasIndicadores::query();
            $query->when($programaView, function ($q, $programaView) {
                  return $q->where('id_programa', $programaView);
            });
            $query->when($componenteView, function ($q, $componenteView) {
                  return $q->where('id_componente', $componenteView);
            });
            $query->when($propositoView, function ($q, $propositoView) {
              return $q->where('id_programa', $propositoView)->where('id_proposito', 1);
            });
          
            
            $query->orderby('id', 'desc');
            $dataDefinitiva =  $query->get();

            return view('sigc.programas_inicio')
            ->with('anoO' , $anoO)
            ->with('mesO' , $mesO)
            ->with('data' , $data)
            ->with('eventosMes' , $eventosMes)
            ->with('mes' , $mes)
            ->with('mespanish' , $mespanish)
            ->with('programaView' , $programaView)
            ->with('programaNombre' , $programaNombre)
            ->with('componenteView' , $componenteView)
            ->with('componenteNombre' , $componenteNombre)
            ->with('propositoView' , $propositoView)
            ->with('propositoNombre' , $propositoNombre)
            ->with('selectPrograma' , $selectPrograma)
            ->with('selectComponente' , $selectComponente)
            ->with('selectProposito' , $selectProposito)
            ->with('dataDefinitiva' , $dataDefinitiva);
          }

          public function index_month(Request $request, $month){

                       //;
                       $data = $this->calendar_month($month);

                       // dd($data);
                        $mes = $data['month'];
                        // obtener mes en espanol
                        //$mespanish = '';
                        $mes = $data['month'];
                        $mespanish = new CalendarioController();
                        $mespanish = $mespanish->spanishMonth($mes);
                        if(date("n") == 1){
                          $mesO = 12;
                          $anoO =  (date("Y")-1);
                        }else{
                          $mesO = (date("n")-1);
                          $anoO =  date("Y");
                        }

                        $query1 = ProgramasEventosFechas::query();
                        $query1->whereHas('Calendario', function ($q) use ($mesO) {  $q->where('mes' ,  $mesO); });
                        $query1->whereHas('Calendario', function ($q) use ($anoO) {  $q->where('ano' ,  $anoO); });
                        $eventosMes = $query1->get();

                        /*********************indicadores tabla*********************/
                        $dataDefinitiva = ProgramasIndicadores::where('ano', $anoO)->get();

                        
                        $programaView = $request->programa;
                        $programaNombre = Programas::where('id' , $programaView)->first();
                        $componenteView = $request->componente;
                        $componenteNombre = ProgramasComponentes::where('id' , $componenteView)->first();
                        $propositoView = $request->proposito;
                        $propositoNombre = Programas::where('id' , $propositoView)->first();
                    
                        $selectPrograma[null]  = 'SELECCIONE PROGRAMA'; foreach (Programas::orderby('id', 'desc')->get()as $key1 => $value1) { $selectPrograma[$value1->id]=$value1->nombre;}
                        $selectComponente[null]  = 'SELECCIONE COMPONENTE';foreach (ProgramasComponentes::orderby('id', 'desc')->get() as $key2 => $value2) { $selectComponente[$value2->id]=$value2->nombre;}
                        $selectProposito[null]  = 'SELECCIONE PROPÓSITO'; foreach (Programas::orderby('id', 'desc')->get()as $key3 => $value3) { $selectProposito[$value3->id]=$value3->proposito;}
                        
                        $query = ProgramasIndicadores::query();
                        $query->when($programaView, function ($q, $programaView) {
                          return $q->where('id_programa', $programaView);
                        });
                        $query->when($componenteView, function ($q, $componenteView) {
                          return $q->where('id_componente', $componenteView);
                        });
                        $query->when($propositoView, function ($q, $propositoView) {
                          return $q->where('id_programa', $propositoView)->where('id_proposito', 1);
                        });
                        $query->orderby('id', 'desc');
                        $dataDefinitiva =  $query->get();

                        return view('sigc.programas_inicio')
                        ->with('anoO' , $anoO)
                        ->with('mesO' , $mesO)
                        ->with('data' , $data)
                        ->with('eventosMes' , $eventosMes)
                        ->with('mes' , $mes)
                        ->with('mespanish' , $mespanish)
                        ->with('programaView' , $programaView)
                        ->with('programaNombre' , $programaNombre)
                        ->with('componenteView' , $componenteView)
                        ->with('componenteNombre' , $componenteNombre)
                        ->with('propositoView' , $propositoView)
                        ->with('propositoNombre' , $propositoNombre)
                        ->with('selectPrograma' , $selectPrograma)
                        ->with('selectComponente' , $selectComponente)
                        ->with('selectProposito' , $selectProposito)
                        ->with('dataDefinitiva' , $dataDefinitiva);
          }

      

      public static function calendar_month($month){
        $auxConsultaAnterior = [];
        //$mes = date("Y-m");
        $mes = $month;
        //sacar el ultimo de dia del mes
        $daylast =  date("Y-m-d", strtotime("last day of ".$mes));
        //sacar el dia de dia del mes
        $fecha      =  date("Y-m-d", strtotime("first day of ".$mes));


        $daysmonth  =  date("d", strtotime($fecha));
        $montmonth  =  date("m", strtotime($fecha));
        $yearmonth  =  date("Y", strtotime($fecha));
        // sacar el lunes de la primera semana
        $nuevaFecha = mktime(0,0,0,$montmonth,$daysmonth,$yearmonth);
        $diaDeLaSemana = date("w", $nuevaFecha);
        $nuevaFecha = $nuevaFecha - ($diaDeLaSemana*24*3600); //Restar los segundos totales de los dias transcurridos de la semana
        $dateini = date ("Y-m-d",$nuevaFecha);
        //$dateini = date("Y-m-d",strtotime($dateini."+ 1 day"));
        // numero de primer semana del mes
        $semana1 = date("W",strtotime($fecha));
        $semana1 = 1;
        // numero de ultima semana del mes
        $semana2 = date("W",strtotime($daylast));
          //  dd($semana1 , $fecha , $semana2 , $daylast);
        // semana todal del mes
        // en caso si es diciembre
        if (date("m", strtotime($mes))==12) {
            $semana = 5;
        }
        else {
          $semana = ($semana2-$semana1)+2;
        }

        // semana todal del mes

        $datafecha = $dateini;
        $calendario = array();
        $iweek = 0;

        while ($iweek < $semana):
            $iweek++;
         //   ddd( $iweek);
            //echo "Semana $iweek <br>";
            //
            $weekdata = [];
         //   dd($weekdata);
            for ($iday=0; $iday < 7 ; $iday++){
              // code...
              $datafecha = date("Y-m-d",strtotime($datafecha."+ 1 day"));
              $datanew['mes'] = date("M", strtotime($datafecha));
              $datanew['dia'] = date("d", strtotime($datafecha));
              $datanew['fecha'] = $datafecha;
              //AGREGAR CONSULTAS EVENTO
              
              $datanew['evento'] = ProgramasEventosFechas::whereRaw("SUBSTRING(`start_date` , 1, 10) ="."'$datafecha'")->get();
             // $auxConsultaAnterior = [];
              if(ProgramasEventosFechas::whereRaw("SUBSTRING(`start_date` , 1, 10) ="."'$datafecha'")->get()->count()>0){
                $auxConsultaAnterior = ProgramasEventosFechas::whereRaw("SUBSTRING(`start_date` , 1, 10) ="."'$datafecha'")->get();
              }else{
                $rangoF =  ProgramasEventosFechas::whereRaw("'$datafecha'BETWEEN SUBSTRING(`start_date` , 1, 10) AND SUBSTRING(`end_date` , 1, 10)")->get();
                if($rangoF->count()>0){
                  $datanew['evento'] = $auxConsultaAnterior;
                }
              }

              array_push($weekdata,$datanew);
            }
            $dataweek['semana'] = $iweek;
            $dataweek['datos'] = $weekdata;
            //$datafecha['horario'] = $datahorario;
            array_push($calendario,$dataweek);
        endwhile;
        $nextmonth = date("Y-M",strtotime($mes."+ 1 month"));
        $lastmonth = date("Y-M",strtotime($mes."- 1 month"));
        $month = date("M",strtotime($mes));
        $yearmonth = date("Y",strtotime($mes));
        //$month = date("M",strtotime("2019-03"));
        $data = array(
          'next' => $nextmonth,
          'month'=> $month,
          'year' => $yearmonth,
          'last' => $lastmonth,
          'calendar' => $calendario,
        );
        return $data;
      }

    public function getMEsRodrigo($mes){

    switch ($mes) {
    case 1:
        return 'Enero';
        break;
    case 2:
        return 'Febrero';
        break;
    case 3:
        return 'Marzo';
        break;
    case 4:
        return 'Abril';
        break;
    case 5:
         return 'Mayo';
         break;
    case 6:
          return 'Junio';
          break;
    case 7:
          return 'Julio';
          break;
    case 8:
          return 'Agosto';
          break;
    case 9:
          return 'Septiembre';
          break;
    case 10:
          return 'Octubre';
          break;
    case 11:
          return 'Noviembre';
          break;
    case 12:
          return 'Diciembre';
          break;

    default:
      return "No encontrado";
     }

    }

    public function TodosComponentes(){
      set_time_limit(20000);
      $componentes = ProgramasComponentes::get();
      $arrayComp = [];
      foreach ($componentes as $key => $value) {
        $arrayComp[$value->id] = $value->nombre;
      }
      return $arrayComp;
    }

    public function TodosPropositos(){
      set_time_limit(20000);
      $proposito = Programas::get();
      $arrayProp = [];
      foreach ($proposito as $key => $value) {
        $arrayProp[$value->id] = $value->proposito;
      }
      return $arrayProp;
    }
    
    public function getComponentesForPrograma($programa){
      set_time_limit(20000);
      $componentesP = ProgramasComponentes::select('id')->where('id_programa' , $programa)->get()->pluck('id')->toArray();
      $componentes = ProgramasComponentes::whereIn('id' , $componentesP)->get();
   
      $arrayComp = [];
      foreach ($componentes as $key => $value) {
        $arrayComp[$value->id] = $value->nombre;
      }
      return $arrayComp;
    }

    public function getPropositoForPrograma($programa){
      set_time_limit(20000);
      $propositoP = Programas::select('nombre')->where('id' , $programa)->get()->pluck('nombre')->toArray();
      $propositos = Programas::whereIn('nombre' , $propositoP)->get();
   
      $arrayProp = [];
      foreach ($propositos as $key => $value) {
        $arrayProp[$value->id] = $value->proposito;
      }
      return $arrayProp;
    }
    
    function numberFormatPrecision($number, $precision = 2, $separator = '.')
      {
          $numberParts = explode($separator, $number);
          $response = $numberParts[0];
          if(count($numberParts)>1){
              $response .= $separator;
              $response .= substr($numberParts[1], 0, $precision);
          }
          return $response;
      }


}
