<?php

namespace App\Http\Controllers\Sigc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Sigc\ProgramasMesesReportes;
use App\Http\Requests\SIGC\ProgramasMesesReportesRequest;

class ProgramasMesesReportesController extends Controller
{

    public function index(Request $request){

      /* $dataDefinitivaA = Programas::get();

      return view('sigc.programas')
      ->with('dataDefinitivaA' , $dataDefinitivaA); */

      $dataDefinitivaA = ProgramasMesesReportes::where('ano' , (date("Y")))->orderby('id' , 'desc')->get();
      $dataDefinitivaO = ProgramasMesesReportes::where('ano' , '<=' , date("Y")-1)->orderby('id' , 'desc')->paginate(12);

      return view('sigc.programas_mes')
      ->with('dataDefinitivaA' , $dataDefinitivaA)
      ->with('dataDefinitivaO' , $dataDefinitivaO); 
      
    }

    public function create(){

      $selectAno = [''=>'SELECCIONE AÑO','2022'=>'2022', '2023'=>'2023'];
      
      return view('sigc.programas_mes_crear')
      ->with('selectAno' , $selectAno);
    }


     public function store(ProgramasMesesReportesRequest $request){
      $ano = $request->ano;
      $nombre = $request->nombre;
      $nuevoProgramaMeses = new ProgramasMesesReportes();
      $nuevoProgramaMeses->ano = $ano;
      $nuevoProgramaMeses->nombre = $nombre;
      $nuevoProgramaMeses->save();
      alert()->success('!','Mes de Reporte creado exitosamente');
      return redirect()->route('programas_mes');
    }

    public function edit($id){
      $editProgramaMeses = ProgramasMesesReportes::where('id' , $id)->first();
      $selectAno = [''=>'SELECCIONE AÑO','2022'=>'2022', '2023'=>'2023'];
      
      return view('sigc.programas_mes_editar')
      ->with('selectAno' , $selectAno)
      ->with('editProgramaMeses' , $editProgramaMeses);
    }


     public function update(ProgramasMesesReportesRequest $request, $id){
      $ano = $request->ano;
      $nombre = $request->nombre;
      $editProgramaMeses = ProgramasMesesReportes::where('id' , $id)->first();
      $editProgramaMeses->ano = $ano;
      $editProgramaMeses->nombre = $nombre;
      $editProgramaMeses->save();
      alert()->success('!','Mes de Reporte editado exitosamente');
      return redirect()->route('programas_mes');
    }

    public function destroy(Request $request, $id){
      $programaMeses = ProgramasMesesReportes::where('id', $id)->first();

      $result = $programaMeses->delete();
        if ($result) {
            return response()->json(['success'=>'true']);
        }else{
            return response()->json(['success'=> 'false']);
        }
    }

}
