<?php

namespace App\Http\Controllers\Sigc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Sigc\Programas;
use App\Models\Sigc\ProgramasCobertura;
use App\Http\Requests\SIGC\PoblacionProgramasRequest;

class ProgramasPoblacionController extends Controller
{

  public function index(Request $request){

    /* $dataDefinitivaA = ProgramasCobertura::get();

    return view('sigc.programas_poblacion')
    ->with('dataDefinitivaA' , $dataDefinitivaA); */

    $dataDefinitivaA = ProgramasCobertura::where('ano' , (date("Y")))->orderby('id' , 'desc')->get();
    $dataDefinitivaO = ProgramasCobertura::where('ano' , '<=' , date("Y")-1)->orderby('id' , 'desc')->paginate(12);

    return view('sigc.programas_poblacion')
    ->with('dataDefinitivaA' , $dataDefinitivaA)
    ->with('dataDefinitivaO' , $dataDefinitivaO); 
    
  }


  public function create(){
    $selectAno = [''=>'SELECCIONE AÑO','2022'=>'2022', '2023'=>'2023'];
    $selectPrograma[null]  = 'SELECCIONE PROGRAMA'; foreach (Programas::orderby('id', 'desc')->get()as $key1 => $value1) { $selectPrograma[$value1->id]=$value1->nombre;}
    
    return view('sigc.programas_poblacion_crear')
    ->with('selectPrograma' , $selectPrograma)
    ->with('selectAno' , $selectAno);
  }


   public function store(PoblacionProgramasRequest $request){
    $ano = $request->ano;
    $nombre = $request->nombre;
    $programa = $request->id_programa;
    $unidad = $request->unidad;
    $efectivo1 = $request->efectivo_1;
    $efectivo2 = $request->efectivo_2;
    $efectivo3 = $request->efectivo_3;
    $nuevaPoblacion = new ProgramasCobertura();
    $nuevaPoblacion->ano = $ano;
    $nuevaPoblacion->nombre = $nombre;
    $nuevaPoblacion->id_programa = $programa;
    $nuevaPoblacion->unidad = $unidad;
    $nuevaPoblacion->efectivo_1 = $efectivo1;
    $nuevaPoblacion->efectivo_2 = $efectivo2;
    $nuevaPoblacion->efectivo_3 = $efectivo3;
    $nuevaPoblacion->save();
    alert()->success('!','Población creada exitosamente');
    return redirect()->route('programas_poblacion');
  }

  public function edit($id){
    $editPoblacion = ProgramasCobertura::where('id' , $id)->first();
    $selectAno = [''=>'SELECCIONE AÑO','2022'=>'2022', '2023'=>'2023'];
    $selectPrograma[null]  = 'SELECCIONE PROGRAMA'; foreach (Programas::orderby('id', 'desc')->get()as $key1 => $value1) { $selectPrograma[$value1->id]=$value1->nombre;}
    
    return view('sigc.programas_poblacion_editar')
    ->with('editPoblacion' , $editPoblacion)
    ->with('selectAno' , $selectAno)
    ->with('selectPrograma' , $selectPrograma);
  }


   public function update(PoblacionProgramasRequest $request, $id){
    $ano = $request->ano;
    $nombre = $request->nombre;
    $programa = $request->id_programa;
    $unidad = $request->unidad;
    $efectivo1 = $request->efectivo_1;
    $efectivo2 = $request->efectivo_2;
    $efectivo3 = $request->efectivo_3;
    $editPoblacion = ProgramasCobertura::where('id' , $id)->first();
    $editPoblacion->ano = $ano;
    $editPoblacion->nombre = $nombre;
    $editPoblacion->id_programa = $programa;
    $editPoblacion->unidad = $unidad;
    $editPoblacion->efectivo_1 = $efectivo1;
    $editPoblacion->efectivo_2 = $efectivo2;
    $editPoblacion->efectivo_3 = $efectivo3;
    $editPoblacion->save();
    alert()->success('!','Población editada exitosamente');
    return redirect()->route('programas_poblacion');
  }

  public function destroy(Request $request, $id){
    $poblacion = ProgramasCobertura::where('id', $id)->first();

    $result = $poblacion->delete();
      if ($result) {
          return response()->json(['success'=>'true']);
      }else{
          return response()->json(['success'=> 'false']);
      }
  }

}
