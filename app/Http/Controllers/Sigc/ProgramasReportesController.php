<?php

namespace App\Http\Controllers\Sigc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Sigc\ProgramasReportes;
use App\Models\Sigc\ProgramasCalendarioEventos;
use App\Models\Sigc\Programas;
use App\Models\Sigc\ProgramasComponentes;
use App\Models\Sigc\ProgramasIndicadores;
use App\Models\Sigc\ProgramasCobertura;
use App\Models\Sigc\ProgramasPresupuesto;
use App\Models\Sigc\ProgramasMesesReportes;
use App\Models\Sigc\ProgramasEstados;
use App\Models\Sigc\ProgramasEventos;
use App\Models\Sigc\Users as UsersSigc;
use Carbon\Carbon;
use Auth;
use App\Mail\TestEmail;
use Mail;
use App\Mail\NotificacionNuevaCorreccionReporteProgramaSocialesEmail;
use App\Mail\NotificacionNuevaRevisionCorreccionReporteProgramaSocialesEmail;
use App\Mail\NotificacionNuevaRevisionReporteProgramaSocialesEmail;
use App\Mail\NotificacionNuevoReporteProgramaSocialesEmail;
use App\Models\Sigc\Notificaciones;
use App\Models\Sigc\CorreoNotificaciones;


class ProgramasReportesController extends Controller
{

    public function programas(Request $request){

      $dataDefinitiva = Programas::where('ano' , (date("Y")))->orderby('id' , 'desc')->get();
      $usuarioEmailStd = Auth::user();
      $usuarioSigc = UsersSigc::where('email' , $usuarioEmailStd->email)->first();
      $habitarreporte = true;

      $hoy = Carbon::now()->format('Y-m-d');
      //dd(now()->format('Y-m-d'));
      $eventoHoy = ProgramasCalendarioEventos::where('ano' , date("Y")) ->whereRaw('SUBSTRING(now() , 1, 10) BETWEEN fecha_inicio AND fecha_fin')->first();
      if($eventoHoy){
        $nombreEvento  = $eventoHoy->evento->nombre;
        $puedenEditar = $eventoHoy->evento->editar?true:false;
      }

      foreach ($dataDefinitiva as $key => $value) {
        $value->habReporte = true;
        $value->habEnviarReporte = false;
        $value->habResion = false;
        $value->habCorregir = false;
        $value->habRevisionCorreccion = false;
        if($eventoHoy){        
          $consultaSihayReporte = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , $value->id)->where('id_calendario' , $eventoHoy->id)->where('id_mes_reporte', $eventoHoy->mes)->first();
          $consultaSihayEnviar = ProgramasReportes::where('id_estado', 4)->where('ano' , (date("Y")))->where('id_programa' , $value->id)->where('id_calendario' , $eventoHoy->id)->where('id_mes_reporte', $eventoHoy->mes)->first();
          $consultaSihayResion  =  ProgramasReportes::where('id_estado', 7)->where('ano' , (date("Y")))->where('id_programa' , $value->id)->where('id_mes_reporte', $eventoHoy->mes)->first();
          $consultaSihayCorregir = ProgramasReportes::where('id_estado', 2)->where('ano' , (date("Y")))->where('id_programa' , $value->id)->where('id_mes_reporte', $eventoHoy->mes)->first();
          $consultaSihayRevionCorregir = ProgramasReportes::where('id_estado', 5)->where('ano' , (date("Y")))->where('id_programa' , $value->id)->where('id_mes_reporte', $eventoHoy->mes)->first();
        }else{
          $consultaSihayReporte = true;
          $consultaSihayEnviar = false;
          $consultaSihayResion  = false;
          $consultaSihayCorregir = false;
          $consultaSihayRevionCorregir = false;
        }

        $value->habReporte = $consultaSihayReporte?false:true;
        $value->habEnviarReporte = $consultaSihayEnviar?true:false;
        $value->habResion = $consultaSihayResion?true:false;
        $value->habCorregir  = $consultaSihayCorregir?true:false;
        $value->habRevisionCorreccion = $consultaSihayRevionCorregir?true:false;
      }
      return view('sigc.programas_reportar_usuario')
      ->with('dataDefinitiva' , $dataDefinitiva)
      ->with('usuarioSigc' , $usuarioSigc)
      ->with('eventoHoy' , $eventoHoy);
      
    }


    public function datosPro($idEvento , $idPrograma){

      $dataDefinitivaA = Programas::where('ano' , (date("Y")))->where('id' , $idPrograma)->orderby('id' , 'asc')->get();
      $idProgramaReportar  = $dataDefinitivaA->first()->id;
      $eventoReporte = ProgramasCalendarioEventos::where('id' , $idEvento)->first();
      if($eventoReporte){
        $mesReporte = $eventoReporte->meses->nombre;
      }
      $dataDefinitivaB = ProgramasComponentes::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->orderby('id' , 'asc')->get();
      $dataDefinitivaC = ProgramasIndicadores::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->orderby('id_proposito' , 'desc')->get();
      $dataDefinitivaD = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->orderby('id' , 'asc')->get();

      $obj = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('cod_interno' , 'obj')->first();
      $bnf = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('cod_interno' , 'bnf')->first();
      foreach ($dataDefinitivaD as $keyco => $valueco) {
        if ($valueco->cod_interno =='t'){
          for ($i=1; $i < 4; $i++) { 
          $resultado = 0;
          $formula = $valueco->f_t;
          $d_obj = $obj['efectivo_'.$i];
          $d_bnf = $bnf['efectivo_'.$i];
          eval('$resultado =' .$formula.';');
          $valueco['efectivo_'.$i] = round($resultado, 2);
          

          }
        }
      }
     
      $dataDefinitivaE = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->first();
      $dataDefinitivaF = ProgramasMesesReportes::where('ano' , (date("Y")))->orderby('id' , 'asc')->get();

      return view('sigc.programas_reportar')
      ->with('dataDefinitivaA' , $dataDefinitivaA)
      ->with('dataDefinitivaB' , $dataDefinitivaB)
      ->with('dataDefinitivaC' , $dataDefinitivaC)
      ->with('dataDefinitivaD' , $dataDefinitivaD)
      ->with('dataDefinitivaE' , $dataDefinitivaE)
      ->with('dataDefinitivaF' , $dataDefinitivaF)
      ->with('idProgramaReportar' , $idProgramaReportar)
      ->with('eventoReporte' , $eventoReporte)
      ->with('mesReporte' , $mesReporte);
      
    }


    public function reportarPrograma(Request $request , $idPrograma){

      $idReporteEstado = ProgramasEstados::where('nombre' , 'Borrador')->first()->id;
      $observacionReportador = $request->observacion_rep;

      $calendario = $request->id_calendarioc;
      $mes = $request->id_mesc;
      $componente = $request->id_componente;
      $resultado = $request->reporte_componente;
      $ano = $request->anoc;
      foreach ($componente as $llave => $valor) {
        $nuevoReporte = new ProgramasReportes();
        $nuevoReporte->id_calendario = $calendario[$llave];
        $nuevoReporte->id_mes_reporte = $mes[$llave];
        $nuevoReporte->id_programa = $idPrograma;
        $nuevoReporte->id_componente = $valor;
        $nuevoReporte->id_reportador = Auth::user()->id;
        $nuevoReporte->resultado = $resultado[$llave];
        $nuevoReporte->ano = $ano[$llave];
        $nuevoReporte->id_estado = $idReporteEstado;

        if($observacionReportador){
          $nuevoReporte->observacion_rep = $observacionReportador;
         }

        $nuevoReporte->save();
      }

      $calendario2 = $request->id_calendarioi;
      $mes2 = $request->id_mesi;
      $indicador = $request->id_indicador;
      $numerador = $request->numerador;
      $denominador = $request->denominador;
      $ano2 = $request->anoi;
      foreach ($indicador as $llave2 => $valor2) {
        $nuevoReporte2 = new ProgramasReportes();
        $nuevoReporte2->id_calendario = $calendario2[$llave2];
        $nuevoReporte2->id_mes_reporte = $mes2[$llave2];
        $nuevoReporte2->id_programa = $idPrograma;
        $nuevoReporte2->id_indicador = $valor2;
        $nuevoReporte2->id_reportador = Auth::user()->id;
        $nuevoReporte2->numerador = $numerador[$llave2];
        $nuevoReporte2->denominador = $denominador[$llave2];
        if($denominador[$llave2] == 0 || $denominador[$llave2] == null){
          $nuevoReporte2->resultado = 0;
        }else{
          $nuevoReporte2->resultado = round(($numerador[$llave2]/$denominador[$llave2])*100, 2);
        }        
        $nuevoReporte2->ano = $ano2[$llave2];
        $nuevoReporte2->id_estado = $idReporteEstado;

        if($observacionReportador){
          $nuevoReporte2->observacion_rep = $observacionReportador;
         }

        $nuevoReporte2->save();
      }


      $objs = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('cod_interno' , 'obj')->first();
      $obj = $objs->efectivo_3;
      $calendario3 = $request->id_calendariop;
      $mes3 = $request->id_mesp;
      $cobertura = $request->id_cobertura;
      $numerador3 = $request->reporte_cobertura;
      $ano3 = $request->anop;
      $nuevoReporte3 = new ProgramasReportes();
      $nuevoReporte3->id_calendario = $calendario3;
      $nuevoReporte3->id_mes_reporte = $mes3;
      $nuevoReporte3->id_programa = $idPrograma;
      $nuevoReporte3->id_cobertura = $cobertura;
      $nuevoReporte3->id_reportador = Auth::user()->id;
      $nuevoReporte3->numerador = $numerador3;
      if($obj == 0 || $obj == null){
        $nuevoReporte3->resultado = 0;
      }else{
        $nuevoReporte3->resultado = round(($numerador3/$obj)*100, 2);
      }       
      $nuevoReporte3->ano = $ano3;
      $nuevoReporte3->id_estado = $idReporteEstado;

      if($observacionReportador){
        $nuevoReporte3->observacion_rep = $observacionReportador;
        }
      $nuevoReporte3->save();

      $bnObj = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('cod_interno' , 't')->first();
      $t = $bnObj->id;
      $nuevoReporte5 = new ProgramasReportes();
      $nuevoReporte5->id_calendario = $calendario3;
      $nuevoReporte5->id_mes_reporte = $mes3;
      $nuevoReporte5->id_programa = $idPrograma;
      $nuevoReporte5->id_cobertura = $t;
      $nuevoReporte5->id_reportador = Auth::user()->id;
      $nuevoReporte5->numerador = $numerador3;
      if($obj == 0 || $obj == null){
        $nuevoReporte5->resultado = 0;
      }else{
        $nuevoReporte5->resultado = round(($numerador3/$obj)*100, 2);
      }   
      $nuevoReporte5->ano = $ano3;
      $nuevoReporte5->id_estado = $idReporteEstado;

      if($observacionReportador){
        $nuevoReporte5->observacion_rep = $observacionReportador;
        }
      $nuevoReporte5->save();
      

      $calendario4 = $request->id_calendariog;
      $mes4 = $request->id_mesg;
      $gasto = $request->id_gasto;
      $ejecutados = $request->reporte_ejecutados;
      $vigente = $request->reporte_vigente;
      $ano4 = $request->anog;
      $nuevoReporte4 = new ProgramasReportes();
      $nuevoReporte4->id_calendario = $calendario4;
      $nuevoReporte4->id_mes_reporte = $mes4;
      $nuevoReporte4->id_programa = $idPrograma;
      $nuevoReporte4->id_presupuesto = $gasto;
      $nuevoReporte4->id_reportador = Auth::user()->id;
      $nuevoReporte4->numerador = $vigente;
      $nuevoReporte4->denominador = $ejecutados;
      if($vigente == 0 || $vigente == null){
        $nuevoReporte4->resultado = 0;
      }else{
        $nuevoReporte4->resultado = round(($ejecutados/$vigente)*100, 2);
      }       
      $nuevoReporte4->ano = $ano4;
      $nuevoReporte4->id_estado = $idReporteEstado;

      if($observacionReportador){
        $nuevoReporte4->observacion_rep = $observacionReportador;
        }

      $nuevoReporte4->save();
    

      alert()->success('!','Borrador de Reporte creado exitosamente');
      return redirect()->route('programas_reportar_usuario');

    }


    public function datosProEditar($idEvento , $idPrograma){

      $dataDefinitivaA = Programas::where('ano' , (date("Y")))->where('id' , $idPrograma)->orderby('id' , 'asc')->get();
      $idProgramaReportar  = $dataDefinitivaA->first()->id;
      $eventoReporte = ProgramasCalendarioEventos::where('id' , $idEvento)->first();
      $eventoReporteMes = ProgramasCalendarioEventos::Select('mes')->where('id' , $idEvento)->get();
      $dataDefinitivaF = ProgramasMesesReportes::where('ano' , (date("Y")))->orderby('id' , 'asc')->get();
      $dataDefinitivaF2 = ProgramasMesesReportes::whereIn('id' , $eventoReporteMes)->where('ano' , (date("Y")))->orderby('id' , 'asc')->get();
      $dataDefinitivaF3 = ProgramasMesesReportes::whereIn('id' , $eventoReporteMes)->where('ano' , (date("Y")))->orderby('id' , 'asc')->get();
      if($eventoReporte){
        $mesReporte = $eventoReporte->meses->nombre;
        $mesReporteId = $eventoReporte->meses->id;
      }

      $dataDefinitivaB = ProgramasComponentes::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->orderby('id' , 'asc')->get();
        foreach ($dataDefinitivaF2 as $keymesB => $valuemesB) {
            foreach ($dataDefinitivaB as $keyB => $valorB) {
              $resultPormes = ProgramasReportes::where('id_programa' , $idPrograma)->where('id_mes_reporte', $valuemesB->id)->where('id_componente', $valorB->id)->first();
              $valorB[str_replace(' ' ,'',$valuemesB->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
            }
        }

        $dataDefinitivaC = ProgramasIndicadores::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->orderby('id_proposito' , 'desc')->get();
        foreach ($dataDefinitivaF2 as $keymesC => $valuemesC) {
          foreach ($dataDefinitivaC as $keyC => $valorC) {
            $resultPormes = ProgramasReportes::where('id_programa' , $idPrograma)->where('id_mes_reporte', $valuemesC->id)->where('id_indicador', $valorC->id)->first();
            $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_resultado'] =  $resultPormes ? $resultPormes->resultado : null;
            $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_numerador'] =  $resultPormes ? $resultPormes->numerador : null;
            $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_denominador'] =  $resultPormes ? $resultPormes->denominador : null;
          }          
        }
        

        $dataDefinitivaD = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->orderby('id' , 'asc')->get();
        $obj = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('cod_interno' , 'obj')->first();
        $bnf = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('cod_interno' , 'bnf')->first();
      foreach ($dataDefinitivaD as $keyco => $valueco) {
        if ($valueco->cod_interno =='t'){
          for ($i=1; $i < 4; $i++) { 
          $resultado = 0;
          $formula = $valueco->f_t;
          $d_obj = $obj['efectivo_'.$i];
          $d_bnf = $bnf['efectivo_'.$i];
          eval('$resultado =' .$formula.';');
          $valueco['efectivo_'.$i] = round($resultado, 2);
          

          }
        }
      }
        foreach ($dataDefinitivaF2 as $keymesF => $valuemesF) {
          foreach ($dataDefinitivaD as $keyD => $valorD) {
            $resultPormes = ProgramasReportes::where('id_programa' , $idPrograma)->where('id_mes_reporte', $valuemesF->id)->where('id_cobertura', $valorD->id)->first();
            $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_numeradorp'] =  $resultPormes ? $resultPormes->numerador : null;
            $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_resultadop'] =  $resultPormes ? $resultPormes->resultado : null;

            /* if ($valorD->cod_interno =='t'){
              $obj = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('cod_interno' , 'obj')->first();
              $bnf = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('cod_interno' , 'bnf')->first();
              $d_obj = $obj->efectivo_3;
              $bnfReporte = ProgramasReportes::where('id_programa' , $idPrograma)->where('id_mes_reporte', $valuemesF->id)->where('id_cobertura', $bnf->id)->first();
              $d_bnf = $bnfReporte->resultado;
              $resultado = 0;
              $formula = $valorD->f_t;
              eval('$resultado =' .$formula.';');
              $valorD[str_replace(' ' ,'',$valuemesF->nombre)] = $resultado;
            } */
          } 
        }
        
        $dataDefinitivaE2 = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->first();

      $dataDefinitivaE = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF2 as $keymesG => $valuemesG) {
        foreach ($dataDefinitivaE as $keyE => $valorE) {
          $resultPormes = ProgramasReportes::where('id_programa' , $idPrograma)->where('id_mes_reporte', $valuemesG->id)->where('id_presupuesto', $valorE->id)->first();
          $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_vigente'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecutados'] =  $resultPormes ? $resultPormes->denominador : null;
          $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecucion'] =  $resultPormes ? $resultPormes->resultado : null;
          /* $d_eje = $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecutados'];
          $d_vi = $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_vigente'];
          $resultado = 0;
          $formula = $valorE->f_ejecucion;
          eval('$resultado =' .$formula.';');
          $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_resultado'] = $resultado; */

        }
      }


      $reporte = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('id_mes_reporte', $mesReporteId)->first();
      $observacionReporte = $reporte->observacion_rep;
      $observacion = $reporte->observacion;
      

      return view('sigc.programas_editar_reporte')
      ->with('dataDefinitivaA' , $dataDefinitivaA)
      ->with('dataDefinitivaB' , $dataDefinitivaB)
      ->with('dataDefinitivaC' , $dataDefinitivaC)
      ->with('dataDefinitivaD' , $dataDefinitivaD)
      ->with('dataDefinitivaE' , $dataDefinitivaE)
      ->with('dataDefinitivaE2' , $dataDefinitivaE2)
      ->with('dataDefinitivaF' , $dataDefinitivaF)
      ->with('dataDefinitivaF2' , $dataDefinitivaF2)
      ->with('idProgramaReportar' , $idProgramaReportar)
      ->with('eventoReporte' , $eventoReporte)
      ->with('observacion' , $observacion)
      ->with('observacionReporte' , $observacionReporte)
      ->with('mesReporte' , $mesReporte)
      ->with('resultPormes' , $resultPormes);

    }


    public function editarReportePrograma(Request $request , $idPrograma){

      $observacionReportador = $request->observacion_rep;
      $idPrograma = $idPrograma;

      $componente = $request->id_componente;
      $resultado = $request->reporte_componente;
      $mes = $request->id_mesc;
      foreach ($componente as $llave => $valor) {
        $editReporte = ProgramasReportes::whereIn('id_componente' , [$valor])->where('id_programa' , $idPrograma)->where('id_mes_reporte' , $mes)->first();
        $editReporte->resultado = $resultado[$llave];

        if($observacionReportador){
          $editReporte->observacion_rep = $observacionReportador;
          }
        $editReporte->save();
      }

      $indicador = $request->id_indicador;
      $numerador = $request->numerador;
      $denominador = $request->denominador;
      $mes2 = $request->id_mesi;
      foreach ($indicador as $llave2 => $valor2) {
        $editReporte2 = ProgramasReportes::whereIn('id_indicador' , [$valor2])->where('id_programa' , $idPrograma)->where('id_mes_reporte' , $mes2)->first();
        //if($editReporte2 == null){dd($editReporte2, $valor2 , $idPrograma, $mes2); }
          $editReporte2->numerador = $numerador[$llave2];
          $editReporte2->denominador = $denominador[$llave2];
          if($denominador[$llave2] == 0 || $denominador[$llave2] == null){
            $editReporte2->resultado = 0;
          }else{
            $editReporte2->resultado = round(($numerador[$llave2]/$denominador[$llave2])*100, 2);
          }  
          
  
          if($observacionReportador){
            $editReporte2->observacion_rep = $observacionReportador;
            }
          $editReporte2->save();
        }
       
      


      $cobertura = $request->id_cobertura;
      $objs = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('cod_interno' , 'obj')->first();
      $obj = $objs->efectivo_3;
      $numerador3 = $request->reporte_cobertura;
      $resultado3 = $request->reporte_resultadot;
      $mes3 = $request->id_mesp;
      foreach ($cobertura as $llave3 => $valor3) {
      $editReporte3 = ProgramasReportes::whereIn('id_cobertura' , [$valor3])->where('id_programa' , $idPrograma)->where('id_mes_reporte' , $mes3)->first();
      $editReporte3->numerador = $numerador3[$llave3];
      if($obj == 0 || $obj == null){
        $editReporte3->resultado = 0;
      }else{
        $editReporte3->resultado = round(($numerador3[$llave3]/$obj)*100, 2);
      }   
      

      if($observacionReportador){
        $editReporte3->observacion_rep = $observacionReportador;
        }
      $editReporte3->save();
    }

      $bnf = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('cod_interno' , 'bnf')->first();
      $repBnf = ProgramasReportes::where('id_cobertura', $bnf->id)->where('id_programa' , $idPrograma)->where('id_mes_reporte', $mes3)->first();
      $num = $repBnf->numerador;
      $resul = $repBnf->resultado;
      
      $t = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('cod_interno' , 't')->first();
      $repT = ProgramasReportes::where('id_cobertura', $t->id)->where('id_programa' , $idPrograma)->where('id_mes_reporte', $mes3)->first();
      $coberturaT = $repT->id_cobertura;
      $editReporte5 = ProgramasReportes::where('id_cobertura' , $coberturaT)->where('id_programa' , $idPrograma)->where('id_mes_reporte' , $mes3)->first();
      $editReporte5->numerador = $num;
      $editReporte5->resultado = $resul;

      if($observacionReportador){
        $editReporte5->observacion_rep = $observacionReportador;
        }
      $editReporte5->save();
    

      $gastos = $request->id_gastos;
      $mes4 = $request->id_mesg;
      $ejecutados = $request->reporte_ejecutados;
      $vigente = $request->reporte_vigente;
      $ejecucion = $request->reporte_resultados;
      foreach ($gastos as $llave4 => $valor4) {
        $editReporte4 = ProgramasReportes::whereIn('id_presupuesto' , [$valor4])->where('id_programa' , $idPrograma)->where('id_mes_reporte' , $mes4)->first();
        $editReporte4->numerador = $vigente[$llave4];
        $editReporte4->denominador = $ejecutados[$llave4];
        if($vigente[$llave4] == 0 || $vigente[$llave4] == null){
          $editReporte4->resultado = 0;
        }else{
          $editReporte4->resultado = round(($ejecutados[$llave4]/$vigente[$llave4])*100, 2);
        }     
        

        if($observacionReportador){
          $editReporte4->observacion_rep = $observacionReportador;
          }
        $editReporte4->save();
      }
    
      alert()->success('!','Reporte Corregido exitosamente');
      return redirect()->route('programas_reportar_usuario'); 
    
    }


    public function enviarReporte($idPrograma){

      $idReporteEstado = ProgramasEstados::where('nombre' , 'Reportada')->first()->id;
      $idPrograma = $idPrograma;
      $eventoHoy = ProgramasCalendarioEventos::where('ano' , date("Y")) ->whereRaw('now() BETWEEN fecha_inicio AND fecha_fin')->first();
      $eventoMes = $eventoHoy->mes;

      $datosComponentes = ProgramasReportes::select('id_componente')->where('id_programa' , $idPrograma)->where('id_mes_reporte' , $eventoMes)->whereNotNull('id_componente')->get();
      $componente = collect($datosComponentes);
      $componentesArray = $componente->toArray();

      foreach ($componentesArray as $llave => $valor) {
        $editReporte = ProgramasReportes::whereIn('id_componente' , [$valor])->where('id_mes_reporte' , $eventoMes)->first();
        $editReporte->id_estado = $idReporteEstado;
        $editReporte->save();
      }

      $datosIndicadores = ProgramasReportes::select('id_indicador')->where('id_programa' , $idPrograma)->where('id_mes_reporte' , $eventoMes)->whereNotNull('id_indicador')->get();
      $indicadores = collect($datosIndicadores);
      $indicadoresArray = $indicadores->toArray();

      foreach ($indicadoresArray as $llave2 => $valor2) {
        $editReporte2 = ProgramasReportes::whereIn('id_indicador' , [$valor2])->where('id_mes_reporte' , $eventoMes)->first();
        $editReporte2->id_estado = $idReporteEstado;
        $editReporte2->save();
      }

      $datosCobertura = ProgramasReportes::select('id_cobertura')->where('id_programa' , $idPrograma)->where('id_mes_reporte' , $eventoMes)->whereNotNull('id_cobertura')->get();
      $cobertura = collect($datosCobertura);
      $coberturaArray = $cobertura->toArray();

      foreach ($coberturaArray as $llave3 => $valor3) {
        $editReporte3 = ProgramasReportes::whereIn('id_cobertura' , [$valor3])->where('id_mes_reporte' , $eventoMes)->first();
        $editReporte3->id_estado = $idReporteEstado;
        $editReporte3->save();
      }

      $datosPresupuesto = ProgramasReportes::select('id_presupuesto')->where('id_programa' , $idPrograma)->where('id_mes_reporte' , $eventoMes)->whereNotNull('id_presupuesto')->get();
      $presupuesto = collect($datosPresupuesto);
      $presupuestoArray = $presupuesto->toArray();
//dd($presupuestoArray);
      foreach ($presupuestoArray as $llave4 => $valor4) {
        $editReporte4 = ProgramasReportes::where('id_presupuesto' , $valor4)->where('id_programa' , $idPrograma)->where('id_mes_reporte' , $eventoMes)->first();
        $editReporte4->id_estado = $idReporteEstado;
        $editReporte4->save();
      }

      if ($editReporte4){
        $this->notificacionReporte($editReporte4);
      }


      alert()->success('!','Reporte Enviado exitosamente exitosamente');
      return redirect()->route('programas_reportar_usuario');

    }

    public function notificacionReporte($editReporte4){

      
      $emails = ['belen.pavez@ind.cl','jorge.vergara@ind.cl','nicolas.prenafeta@mindep.cl','javier.saez@mindep.cl'];
      $datos = ['programa'=>$editReporte4];

      foreach ($emails as $key => $correo) {
        Mail::to($correo)->send(new NotificacionNuevoReporteProgramaSocialesEmail($datos));
        $notificacionesId = 9;
        $nuevoCorreo = new CorreoNotificaciones();
        $nuevoCorreo->id_notificaciones = $notificacionesId;
        $nuevoCorreo->destintario = $correo;
        $nuevoCorreo->save();
      }
    }


    public function revisionReporte($idEvento , $idPrograma){

      $dataDefinitivaA = Programas::where('ano' , (date("Y")))->where('id' , $idPrograma)->orderby('id' , 'asc')->get();
      $idProgramaReportar  = $dataDefinitivaA->first()->id;
      $eventoReporte = ProgramasCalendarioEventos::where('id' , $idEvento)->first();
      $eventoReporteMes = ProgramasCalendarioEventos::Select('mes')->where('id' , $idEvento)->get(); 
      $dataDefinitivaF = ProgramasMesesReportes::where('ano' , (date("Y")))->orderby('id' , 'asc')->get();
      $dataDefinitivaF2 = ProgramasMesesReportes::whereIn('id' , $eventoReporteMes)->where('ano' , (date("Y")))->orderby('id' , 'asc')->get();

      if($eventoReporte){
        $mesReporte = $eventoReporte->meses->nombre;
        $mesReporteId = $eventoReporte->meses->id;
      }
      $dataDefinitivaB = ProgramasComponentes::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF as $keymesB => $valuemesB) {
          foreach ($dataDefinitivaB as $keyB => $valorB) {
            $resultPormes = ProgramasReportes::where('id_programa' , $idPrograma)->where('id_mes_reporte', $valuemesB->id)->where('id_componente', $valorB->id)->first();
              $valorB[str_replace(' ' ,'',$valuemesB->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
          }
      }

      $dataDefinitivaC = ProgramasIndicadores::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->orderby('id_proposito' , 'desc')->get();
        foreach ($dataDefinitivaF as $keymesC => $valuemesC) {
          foreach ($dataDefinitivaC as $keyC => $valorC) {
            $resultPormes = ProgramasReportes::where('id_programa' , $idPrograma)->where('id_mes_reporte', $valuemesC->id)->where('id_indicador', $valorC->id)->first();
            $valorC[str_replace(' ' ,'',$valuemesC->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
            $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_numerador'] =  $resultPormes ? $resultPormes->numerador : null;
            $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_denominador'] =  $resultPormes ? $resultPormes->denominador : null;
          }
        }

     


        $dataDefinitivaD = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->orderby('id' , 'asc')->get();
        $obj = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('cod_interno' , 'obj')->first();
        $bnf = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('cod_interno' , 'bnf')->first();
      foreach ($dataDefinitivaD as $keyco => $valueco) {
        if ($valueco->cod_interno =='t'){
          for ($i=1; $i < 4; $i++) { 
          $resultado = 0;
          $formula = $valueco->f_t;
          $d_obj = $obj['efectivo_'.$i];
          $d_bnf = $bnf['efectivo_'.$i];
          eval('$resultado =' .$formula.';');
          $valueco['efectivo_'.$i] = round($resultado, 2);
          

          }
        }
      }
        foreach ($dataDefinitivaF2 as $keymesF => $valuemesF) {
          foreach ($dataDefinitivaD as $keyD => $valorD) {
            $resultPormes = ProgramasReportes::where('id_programa' , $idPrograma)->where('id_mes_reporte', $valuemesF->id)->where('id_cobertura', $valorD->id)->first();
            $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_numeradorp'] =  $resultPormes ? $resultPormes->numerador : null;
            $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_resultadop'] =  $resultPormes ? $resultPormes->resultado : null;

            /* if ($valorD->cod_interno =='t'){
              $obj = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('cod_interno' , 'obj')->first();
              $bnf = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('cod_interno' , 'bnf')->first();
              $d_obj = $obj->efectivo_3;
              $bnfReporte = ProgramasReportes::where('id_programa' , $idPrograma)->where('id_mes_reporte', $valuemesF->id)->where('id_cobertura', $bnf->id)->first();
              $d_bnf = $bnfReporte->resultado;
              $resultado = 0;
              $formula = $valorD->f_t;
              eval('$resultado =' .$formula.';');
              $valorD[str_replace(' ' ,'',$valuemesF->nombre)] = $resultado;
            } */
          } 
        }
        
        $dataDefinitivaE2 = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->first();

      $dataDefinitivaE = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF2 as $keymesG => $valuemesG) {
        foreach ($dataDefinitivaE as $keyE => $valorE) {
          $resultPormes = ProgramasReportes::where('id_programa' , $idPrograma)->where('id_mes_reporte', $valuemesG->id)->where('id_presupuesto', $valorE->id)->first();
          $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_vigente'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecutados'] =  $resultPormes ? $resultPormes->denominador : null;
          $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecucion'] =  $resultPormes ? $resultPormes->resultado : null;
          /* $d_eje = $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecutados'];
          $d_vi = $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_vigente'];
          $resultado = 0;
          $formula = $valorE->f_ejecucion;
          eval('$resultado =' .$formula.';');
          $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_resultado'] = $resultado; */

        }
      }



      $nuevaRevision = ProgramasReportes::where('id_programa' , $idPrograma)->where('id_calendario' , $idEvento)->get();
      $reporte = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('id_mes_reporte', $mesReporteId)->first();
      $observacionReporte = $reporte->observacion_rep;
      $selectEstado[null]  = 'SELECCIONE ESTADO'; foreach (ProgramasEstados::orderby('id', 'desc')->whereIn('id', [1, 2, 6])->get()as $key1 => $value1) { $selectEstado[$value1->id]=$value1->nombre;}


      return view('sigc.programas_revision_reporte')
      ->with('dataDefinitivaA' , $dataDefinitivaA)
      ->with('dataDefinitivaB' , $dataDefinitivaB)
      ->with('dataDefinitivaC' , $dataDefinitivaC)
      ->with('dataDefinitivaD' , $dataDefinitivaD)
      ->with('dataDefinitivaE' , $dataDefinitivaE)
      ->with('dataDefinitivaE2' , $dataDefinitivaE2)
      ->with('dataDefinitivaF' , $dataDefinitivaF)
      ->with('dataDefinitivaF2' , $dataDefinitivaF2)
      ->with('idProgramaReportar' , $idProgramaReportar)
      ->with('eventoReporte' , $eventoReporte)
      ->with('nuevaRevision' , $nuevaRevision)
      ->with('selectEstado' , $selectEstado)
      ->with('observacionReporte' , $observacionReporte)
      ->with('mesReporte' , $mesReporte);
    
    }


    public function reporteRevisado(Request $request, $idPrograma){

      $idPrograma = $idPrograma;
      $idCalendario = $request->id_calendario;
      $idMes = $request->id_mes;
      $observacion = $request->observacion;
      $idReporteEstado = $request->id_estado;
      $consultaReportado = ProgramasReportes::where('id_programa' , $idPrograma)->where('id_mes_reporte' , $idMes)->get();
      foreach ($consultaReportado as $keyReport => $valueReport) {
          $valueReport->observacion =  $observacion;
          $valueReport->id_estado = $idReporteEstado;
          $valueReport->save();
      }

      if ($valueReport){
        $this->notificacionRevision($valueReport);
      }

      alert()->success('!','Reporte Revisado exitosamente');
      return redirect()->route('programas_reportar_usuario');
    }

    public function notificacionRevision($valueReport){

      $emails = ['belen.pavez@ind.cl','jorge.vergara@ind.cl','nicolas.prenafeta@mindep.cl','javier.saez@mindep.cl'];
      $datos = ['programa'=>$valueReport];

      foreach ($emails as $key => $correo) {
        Mail::to($correo)->send(new NotificacionNuevaRevisionReporteProgramaSocialesEmail($datos));
        $notificacionesId = 10;
        $nuevoCorreo = new CorreoNotificaciones();
        $nuevoCorreo->id_notificaciones = $notificacionesId;
        $nuevoCorreo->destintario = $correo;
        $nuevoCorreo->save();
      }
    }


    public function datosProCorreccion($idEvento , $idPrograma){

      $dataDefinitivaA = Programas::where('ano' , (date("Y")))->where('id' , $idPrograma)->orderby('id' , 'asc')->get();
      $idProgramaReportar  = $dataDefinitivaA->first()->id;
      $eventoReporte = ProgramasCalendarioEventos::where('id' , $idEvento)->first();
      $eventoReporteMes = ProgramasCalendarioEventos::Select('mes')->where('id' , $idEvento)->get();
      $dataDefinitivaF = ProgramasMesesReportes::where('ano' , (date("Y")))->orderby('id' , 'asc')->get();
      $dataDefinitivaF2 = ProgramasMesesReportes::whereIn('id' , $eventoReporteMes)->where('ano' , (date("Y")))->orderby('id' , 'asc')->get();
      if($eventoReporte){
        $mesReporte = $eventoReporte->meses->nombre;
        $mesReporteId = $eventoReporte->meses->id;
      }

      $dataDefinitivaB = ProgramasComponentes::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->orderby('id' , 'asc')->get();
        foreach ($dataDefinitivaF2 as $keymesB => $valuemesB) {
            foreach ($dataDefinitivaB as $keyB => $valorB) {
              $resultPormes = ProgramasReportes::where('id_programa' , $idPrograma)->where('id_mes_reporte', $valuemesB->id)->where('id_componente', $valorB->id)->first();
                $valorB[str_replace(' ' ,'',$valuemesB->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
            }
        }

        $dataDefinitivaC = ProgramasIndicadores::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->orderby('id_proposito' , 'desc')->get();
        foreach ($dataDefinitivaF2 as $keymesC => $valuemesC) {
          foreach ($dataDefinitivaC as $keyC => $valorC) {
            $resultPormes = ProgramasReportes::where('id_programa' , $idPrograma)->where('id_mes_reporte', $valuemesC->id)->where('id_indicador', $valorC->id)->first();
            $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_resultado'] =  $resultPormes ? $resultPormes->resultado : null;
            $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_numerador'] =  $resultPormes ? $resultPormes->numerador : null;
            $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_denominador'] =  $resultPormes ? $resultPormes->denominador : null;

          }
        }

     

        $dataDefinitivaD = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->orderby('id' , 'asc')->get();
        $obj = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('cod_interno' , 'obj')->first();
        $bnf = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('cod_interno' , 'bnf')->first();
      foreach ($dataDefinitivaD as $keyco => $valueco) {
        if ($valueco->cod_interno =='t'){
          for ($i=1; $i < 4; $i++) { 
          $resultado = 0;
          $formula = $valueco->f_t;
          $d_obj = $obj['efectivo_'.$i];
          $d_bnf = $bnf['efectivo_'.$i];
          eval('$resultado =' .$formula.';');
          $valueco['efectivo_'.$i] = round($resultado, 2);
          

          }
        }
      }
        foreach ($dataDefinitivaF2 as $keymesF => $valuemesF) {
          foreach ($dataDefinitivaD as $keyD => $valorD) {
            $resultPormes = ProgramasReportes::where('id_programa' , $idPrograma)->where('id_mes_reporte', $valuemesF->id)->where('id_cobertura', $valorD->id)->first();
            $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_numeradorp'] =  $resultPormes ? $resultPormes->numerador : null;
            $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_resultadop'] =  $resultPormes ? $resultPormes->resultado : null;

            /* if ($valorD->cod_interno =='t'){
              $obj = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('cod_interno' , 'obj')->first();
              $bnf = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('cod_interno' , 'bnf')->first();
              $d_obj = $obj->efectivo_3;
              $bnfReporte = ProgramasReportes::where('id_programa' , $idPrograma)->where('id_mes_reporte', $valuemesF->id)->where('id_cobertura', $bnf->id)->first();
              $d_bnf = $bnfReporte->resultado;
              $resultado = 0;
              $formula = $valorD->f_t;
              eval('$resultado =' .$formula.';');
              $valorD[str_replace(' ' ,'',$valuemesF->nombre)] = $resultado;
            } */
          } 
        }
        
        $dataDefinitivaE2 = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->first();

      $dataDefinitivaE = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF2 as $keymesG => $valuemesG) {
        foreach ($dataDefinitivaE as $keyE => $valorE) {
          $resultPormes = ProgramasReportes::where('id_programa' , $idPrograma)->where('id_mes_reporte', $valuemesG->id)->where('id_presupuesto', $valorE->id)->first();
          $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_vigente'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecutados'] =  $resultPormes ? $resultPormes->denominador : null;
          $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecucion'] =  $resultPormes ? $resultPormes->resultado : null;
          /* $d_eje = $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecutados'];
          $d_vi = $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_vigente'];
          $resultado = 0;
          $formula = $valorE->f_ejecucion;
          eval('$resultado =' .$formula.';');
          $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_resultado'] = $resultado; */

        }
      }




      $reporte = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('id_mes_reporte', $mesReporteId)->first();
      $observacionReporte = $reporte->observacion_rep;
      $observacion = $reporte->observacion;
      

      return view('sigc.programas_correccion_reporte')
      ->with('dataDefinitivaA' , $dataDefinitivaA)
      ->with('dataDefinitivaB' , $dataDefinitivaB)
      ->with('dataDefinitivaC' , $dataDefinitivaC)
      ->with('dataDefinitivaD' , $dataDefinitivaD)
      ->with('dataDefinitivaE' , $dataDefinitivaE)
      ->with('dataDefinitivaE2' , $dataDefinitivaE2)
      ->with('dataDefinitivaF' , $dataDefinitivaF)
      ->with('dataDefinitivaF2' , $dataDefinitivaF2)
      ->with('idProgramaReportar' , $idProgramaReportar)
      ->with('eventoReporte' , $eventoReporte)
      ->with('observacion' , $observacion)
      ->with('observacionReporte' , $observacionReporte)
      ->with('mesReporte' , $mesReporte)
      ->with('resultPormes' , $resultPormes);

    }


    public function correccionReportePrograma(Request $request , $idPrograma){

      $idReporteEstado = ProgramasEstados::where('nombre' , 'Corregido')->first()->id;
      $observacionReportador = $request->observacion_rep;
      $idPrograma = $idPrograma;

      $componente = $request->id_componente;
      $resultado = $request->reporte_componente;
      $mes = $request->id_mesc;
      foreach ($componente as $llave => $valor) {
        $editReporte = ProgramasReportes::whereIn('id_componente' , [$valor])->where('id_programa' , $idPrograma)->where('id_mes_reporte' , $mes)->first();
        $editReporte->resultado = $resultado[$llave];
        $editReporte->id_estado = $idReporteEstado;

        if($observacionReportador){
          $editReporte->observacion_rep = $observacionReportador;
          }
        $editReporte->save();
      }


      $indicador = $request->id_indicador;
      $numerador = $request->numerador;
      $denominador = $request->denominador;
      $mes2 = $request->id_mesi;
      foreach ($indicador as $llave2 => $valor2) {
        $editReporte2 = ProgramasReportes::whereIn('id_indicador' , [$valor2])->where('id_programa' , $idPrograma)->where('id_mes_reporte' , $mes2)->first();
        $editReporte2->numerador = $numerador[$llave2];
        $editReporte2->denominador = $denominador[$llave2];
        if($denominador[$llave2] == 0 || $denominador[$llave2] == null){
          $editReporte2->resultado = 0;
        }else{
          $editReporte2->resultado = round(($numerador[$llave2]/$denominador[$llave2])*100, 2);
        }          
        $editReporte2->id_estado = $idReporteEstado;

        if($observacionReportador){
          $editReporte2->observacion_rep = $observacionReportador;
          }
        $editReporte2->save();
      }


      $cobertura = $request->id_cobertura;
      $objs = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('cod_interno' , 'obj')->first();
      $obj = $objs->efectivo_3;
      $numerador3 = $request->reporte_cobertura;
      $resultado3 = $request->reporte_resultadot;
      $mes3 = $request->id_mesp;
      foreach ($cobertura as $llave3 => $valor3) {
      $editReporte3 = ProgramasReportes::whereIn('id_cobertura' , [$valor3])->where('id_programa' , $idPrograma)->where('id_mes_reporte' , $mes3)->first();
      $editReporte3->numerador = $numerador3[$llave3];
      if($obj == 0 || $obj == null){
        $editReporte3->resultado = 0;
      }else{
        $editReporte3->resultado = round(($numerador3[$llave3]/$obj)*100, 2);
      }   
      $editReporte3->id_estado = $idReporteEstado;

      if($observacionReportador){
        $editReporte3->observacion_rep = $observacionReportador;
        }
      $editReporte3->save();
    }

      $bnf = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('cod_interno' , 'bnf')->first();
      $repBnf = ProgramasReportes::where('id_cobertura', $bnf->id)->where('id_programa' , $idPrograma)->where('id_mes_reporte', $mes3)->first();
      $num = $repBnf->numerador;
      $resul = $repBnf->resultado;
      
      $t = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('cod_interno' , 't')->first();
      $repT = ProgramasReportes::where('id_cobertura', $t->id)->where('id_programa' , $idPrograma)->where('id_mes_reporte', $mes3)->first();
      $coberturaT = $repT->id_cobertura;
      $editReporte5 = ProgramasReportes::where('id_cobertura' , $coberturaT)->where('id_programa' , $idPrograma)->where('id_mes_reporte' , $mes3)->first();
      $editReporte5->numerador = $num;
      $editReporte5->resultado = $resul;
      $editReporte5->id_estado = $idReporteEstado;

      if($observacionReportador){
        $editReporte5->observacion_rep = $observacionReportador;
        }
      $editReporte5->save();
    

      $gastos = $request->id_gastos;
      $mes4 = $request->id_mesg;
      $ejecutados = $request->reporte_ejecutados;
      $vigente = $request->reporte_vigente;
      $ejecucion = $request->reporte_resultados;
      foreach ($gastos as $llave4 => $valor4) {
        $editReporte4 = ProgramasReportes::whereIn('id_presupuesto' , [$valor4])->where('id_programa' , $idPrograma)->where('id_mes_reporte' , $mes4)->first();
        $editReporte4->numerador = $vigente[$llave4];
        $editReporte4->denominador = $ejecutados[$llave4];
        if($vigente[$llave4] == 0 || $vigente[$llave4] == null){
          $editReporte4->resultado = 0;
        }else{
          $editReporte4->resultado = round(($ejecutados[$llave4]/$vigente[$llave4])*100, 2);
        }  
        $editReporte4->id_estado = $idReporteEstado;


        if($observacionReportador){
          $editReporte4->observacion_rep = $observacionReportador;
          }
        $editReporte4->save();
      }

      if ($editReporte4){
        $this->notificacionCorreccion($editReporte4);
      }
    
      alert()->success('!','Reporte Corregido exitosamente');
      return redirect()->route('programas_reportar_usuario'); 
    
    }

    public function notificacionCorreccion($editReporte4){

      $emails = ['belen.pavez@ind.cl','jorge.vergara@ind.cl','nicolas.prenafeta@mindep.cl','javier.saez@mindep.cl'];
      $datos = ['programa'=>$editReporte4];

      foreach ($emails as $key => $correo) {
        Mail::to($correo)->send(new NotificacionNuevaCorreccionReporteProgramaSocialesEmail($datos));
        $notificacionesId = 11;
        $nuevoCorreo = new CorreoNotificaciones();
        $nuevoCorreo->id_notificaciones = $notificacionesId;
        $nuevoCorreo->destintario = $correo;
        $nuevoCorreo->save();
      }
    }


    public function revisionCorreccion($idEvento , $idPrograma){

      $dataDefinitivaA = Programas::where('ano' , (date("Y")))->where('id' , $idPrograma)->orderby('id' , 'asc')->get();
        $idProgramaReportar  = $dataDefinitivaA->first()->id;
        $eventoReporte = ProgramasCalendarioEventos::where('id' , $idEvento)->first();
      $eventoReporteMes = ProgramasCalendarioEventos::Select('mes')->where('id' , $idEvento)->get();

        $dataDefinitivaF = ProgramasMesesReportes::where('ano' , (date("Y")))->orderby('id' , 'asc')->get();
      $dataDefinitivaF2 = ProgramasMesesReportes::whereIn('id' , $eventoReporteMes)->where('ano' , (date("Y")))->orderby('id' , 'asc')->get();

        if($eventoReporte){
          $mesReporte = $eventoReporte->meses->nombre;
          $mesReporteId = $eventoReporte->meses->id;
        }
        $dataDefinitivaB = ProgramasComponentes::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->orderby('id' , 'asc')->get();
        foreach ($dataDefinitivaF as $keymesB => $valuemesB) {
            foreach ($dataDefinitivaB as $keyB => $valorB) {
              $resultPormes = ProgramasReportes::where('id_programa' , $idPrograma)->where('id_mes_reporte', $valuemesB->id)->where('id_componente', $valorB->id)->first();
                $valorB[str_replace(' ' ,'',$valuemesB->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
            }
        }
  
        $dataDefinitivaC = ProgramasIndicadores::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->orderby('id_proposito' , 'desc')->get();
          foreach ($dataDefinitivaF as $keymesC => $valuemesC) {
            foreach ($dataDefinitivaC as $keyC => $valorC) {
              $resultPormes = ProgramasReportes::where('id_programa' , $idPrograma)->where('id_mes_reporte', $valuemesC->id)->where('id_indicador', $valorC->id)->first();
              $valorC[str_replace(' ' ,'',$valuemesC->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
              $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_numerador'] =  $resultPormes ? $resultPormes->numerador : null;
              $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_denominador'] =  $resultPormes ? $resultPormes->denominador : null;
            }
          }
  
          $dataDefinitivaD = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->orderby('id' , 'asc')->get();
        $obj = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('cod_interno' , 'obj')->first();
        $bnf = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('cod_interno' , 'bnf')->first();
      foreach ($dataDefinitivaD as $keyco => $valueco) {
        if ($valueco->cod_interno =='t'){
          for ($i=1; $i < 4; $i++) { 
          $resultado = 0;
          $formula = $valueco->f_t;
          $d_obj = $obj['efectivo_'.$i];
          $d_bnf = $bnf['efectivo_'.$i];
          eval('$resultado =' .$formula.';');
          $valueco['efectivo_'.$i] = round($resultado, 2);
          

          }
        }
      }
        foreach ($dataDefinitivaF2 as $keymesF => $valuemesF) {
          foreach ($dataDefinitivaD as $keyD => $valorD) {
            $resultPormes = ProgramasReportes::where('id_programa' , $idPrograma)->where('id_mes_reporte', $valuemesF->id)->where('id_cobertura', $valorD->id)->first();
            $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_numeradorp'] =  $resultPormes ? $resultPormes->numerador : null;
            $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_resultadop'] =  $resultPormes ? $resultPormes->resultado : null;

            /* if ($valorD->cod_interno =='t'){
              $obj = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('cod_interno' , 'obj')->first();
              $bnf = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('cod_interno' , 'bnf')->first();
              $d_obj = $obj->efectivo_3;
              $bnfReporte = ProgramasReportes::where('id_programa' , $idPrograma)->where('id_mes_reporte', $valuemesF->id)->where('id_cobertura', $bnf->id)->first();
              $d_bnf = $bnfReporte->resultado;
              $resultado = 0;
              $formula = $valorD->f_t;
              eval('$resultado =' .$formula.';');
              $valorD[str_replace(' ' ,'',$valuemesF->nombre)] = $resultado;
            } */
          } 
        }
        
        $dataDefinitivaE2 = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->first();

      $dataDefinitivaE = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF2 as $keymesG => $valuemesG) {
        foreach ($dataDefinitivaE as $keyE => $valorE) {
          $resultPormes = ProgramasReportes::where('id_programa' , $idPrograma)->where('id_mes_reporte', $valuemesG->id)->where('id_presupuesto', $valorE->id)->first();
          $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_vigente'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecutados'] =  $resultPormes ? $resultPormes->denominador : null;
          $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecucion'] =  $resultPormes ? $resultPormes->resultado : null;
          /* $d_eje = $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecutados'];
          $d_vi = $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_vigente'];
          $resultado = 0;
          $formula = $valorE->f_ejecucion;
          eval('$resultado =' .$formula.';');
          $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_resultado'] = $resultado; */

        }
      }


        $nuevaRevision = ProgramasReportes::where('id_programa' , $idPrograma)->where('id_calendario' , $idEvento)->get();
        $reporte = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , $idPrograma)->where('id_mes_reporte', $mesReporteId)->first();
        $observacion = $reporte->observacion;
        $observacionReporte = $reporte->observacion_rep;
        $selectEstado[null]  = 'SELECCIONE ESTADO'; foreach (ProgramasEstados::orderby('id', 'desc')->whereIn('id', [1, 2, 6])->get()as $key1 => $value1) { $selectEstado[$value1->id]=$value1->nombre;}


        return view('sigc.programas_revisarcorreccion_reporte')
        ->with('dataDefinitivaA' , $dataDefinitivaA)
        ->with('dataDefinitivaB' , $dataDefinitivaB)
        ->with('dataDefinitivaC' , $dataDefinitivaC)
        ->with('dataDefinitivaD' , $dataDefinitivaD)
        ->with('dataDefinitivaE' , $dataDefinitivaE)
        ->with('dataDefinitivaE2' , $dataDefinitivaE2)
        ->with('dataDefinitivaF' , $dataDefinitivaF)
        ->with('dataDefinitivaF2' , $dataDefinitivaF2)
        ->with('idProgramaReportar' , $idProgramaReportar)
        ->with('eventoReporte' , $eventoReporte)
        ->with('nuevaRevision' , $nuevaRevision)
        ->with('selectEstado' , $selectEstado)
        ->with('observacion' , $observacion)
        ->with('observacionReporte' , $observacionReporte)
        ->with('mesReporte' , $mesReporte)
        ->with('resultPormes' , $resultPormes);

    }

    
    public function correccionRevisada(Request $request, $idPrograma){

      $idPrograma = $idPrograma;
      $idCalendario = $request->id_calendario;
      $idMes = $request->id_mes;
      $observacion = $request->observacion;
      $idReporteEstado = $request->id_estado;
      $consultaReportado = ProgramasReportes::where('id_programa' , $idPrograma)->where('id_mes_reporte' , $idMes)->get();
      foreach ($consultaReportado as $keyReport => $valueReport) {
          $valueReport->observacion =  $observacion;
          $valueReport->id_estado = $idReporteEstado;
          $valueReport->save();
      }

      if ($valueReport){
        $this->notificacionRevisionCorreccion($valueReport);
      }


      alert()->success('!','Reporte Revisado exitosamente');
      return redirect()->route('programas_reportar_usuario');

    }

    public function notificacionRevisionCorreccion($valueReport){

      $emails = ['belen.pavez@ind.cl','jorge.vergara@ind.cl','nicolas.prenafeta@mindep.cl','javier.saez@mindep.cl'];
      $datos = ['programa'=>$valueReport];

      foreach ($emails as $key => $correo) {
        Mail::to($correo)->send(new NotificacionNuevaRevisionCorreccionReporteProgramaSocialesEmail($datos));
        $notificacionesId = 12;
        $nuevoCorreo = new CorreoNotificaciones();
        $nuevoCorreo->id_notificaciones = $notificacionesId;
        $nuevoCorreo->destintario = $correo;
        $nuevoCorreo->save();
      }
    }
}
