<?php

namespace App\Http\Controllers\Sigc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Sigc\Programas;
use App\Models\Sigc\ProgramasComponentes;
use App\Models\Sigc\ProgramasIndicadores;
use App\Models\Sigc\ProgramasCobertura;
use App\Models\Sigc\ProgramasPresupuesto;
use App\Models\Sigc\ProgramasReportes;
use App\Models\Sigc\ProgramasMesesReportes;
use Maatwebsite\Excel\Facades\Excel;
use Dompdf\Dompdf;
use PDF;


class ProgramasSocialesController extends Controller
{

    public function indexCEM(Request $request)
    {

      $dataDefinitivaA = Programas::where('ano' , (date("Y")))->where('id' , 1)->orderby('id' , 'asc')->get();
      $dataDefinitivaF = ProgramasMesesReportes::where('ano' , (date("Y")))->orderby('id' , 'asc')->get();

      $dataDefinitivaB = ProgramasComponentes::where('ano' , (date("Y")))->where('id_programa' , 1)->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF as $keymesB => $valuemesB) {
          foreach ($dataDefinitivaB as $keyB => $valorB) {
            $resultPormes = ProgramasReportes::where('id_programa' , 1)->where('id_mes_reporte', $valuemesB->id)->where('id_componente', $valorB->id)->first();
              $valorB[str_replace(' ' ,'',$valuemesB->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
          }
      }

      $dataDefinitivaC = ProgramasIndicadores::where('ano' , (date("Y")))->where('id_programa' , 1)->orderby('id_proposito' , 'desc')->get();
      foreach ($dataDefinitivaF as $keymesC => $valuemesC) {
        foreach ($dataDefinitivaC as $keyC => $valorC) {
          $resultPormes = ProgramasReportes::where('id_programa' , 1)->where('id_mes_reporte', $valuemesC->id)->where('id_indicador', $valorC->id)->first();
          $valorC[str_replace(' ' ,'',$valuemesC->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
          $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_numerador'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_denominador'] =  $resultPormes ? $resultPormes->denominador : null;
        }
      }

      $dataDefinitivaD = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 1)->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF as $keymesF => $valuemesF) {
        foreach ($dataDefinitivaD as $keyD => $valorD) {
          $resultPormes = ProgramasReportes::where('id_programa' , 1)->where('id_mes_reporte', $valuemesF->id)->where('id_cobertura', $valorD->id)->first();
          $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_numeradorp'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_resultadop'] =  $resultPormes ? $resultPormes->resultado : null;
            
        }
      }
          $obj = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 1)->where('cod_interno' , 'obj')->first();
          $bnf = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 1)->where('cod_interno' , 'bnf')->first();
        foreach ($dataDefinitivaD as $keyco => $valueco) {
          if ($valueco->cod_interno =='t'){
            for ($i=1; $i < 4; $i++) { 
            $resultado = 0;
            $formula = $valueco->f_t;
            $d_obj = $obj['efectivo_'.$i];
            $d_bnf = $bnf['efectivo_'.$i];
            eval('$resultado =' .$formula.';');
            $valueco['efectivo_'.$i] = round($resultado, 2);
            }
          }
        }

      $dataDefinitivaE3 = ProgramasPresupuesto::whereIn('fecha',[(date("Y")-1),(date("Y")-2)])->where('ano' , (date("Y")))->where('id_programa' , 1)->orderby('id' , 'asc')->get();
      $dataDefinitivaE2 = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , 1)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->first();
      $dataDefinitivaE = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , 1)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->get();
          foreach ($dataDefinitivaF as $keymesG => $valuemesG) {
            foreach ($dataDefinitivaE as $keyE => $valorE) {
              $resultPormes = ProgramasReportes::where('id_programa' , 1)->where('id_mes_reporte', $valuemesG->id)->where('id_presupuesto', $valorE->id)->first();
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_vigente'] =  $resultPormes ? $resultPormes->numerador : null;
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecutados'] =  $resultPormes ? $resultPormes->denominador : null;
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecucion'] =  $resultPormes ? $resultPormes->resultado : null; 
            }
          }
          
          $observacionReporte = '';
          $observacion = '';
          $reportMesActual = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , 1)->max('id_mes_reporte');
          if ($reportMesActual) {
            $reporte = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , 1)->where('id_mes_reporte', $reportMesActual)->first();
            $observacionReporte = $reporte->observacion_rep;
            $observacion = $reporte->observacion;
          }
          
        

      return view('sigc.programas_cem')
      ->with('dataDefinitivaA' , $dataDefinitivaA)
      ->with('dataDefinitivaB' , $dataDefinitivaB)
      ->with('dataDefinitivaC' , $dataDefinitivaC)
      ->with('dataDefinitivaD' , $dataDefinitivaD)
      ->with('dataDefinitivaE' , $dataDefinitivaE)
      ->with('dataDefinitivaE2' , $dataDefinitivaE2)
      ->with('dataDefinitivaE3' , $dataDefinitivaE3)
      ->with('dataDefinitivaF' , $dataDefinitivaF)
      ->with('resultPormes' , $resultPormes)
      ->with('observacion' , $observacion)
      ->with('observacionReporte' , $observacionReporte);
      
    }



  public function reporteCEM(Request $request) {

    $tipoReporte = $request->imprimir;

    $dataDefinitivaA = Programas::where('ano' , (date("Y")))->where('id' , 1)->orderby('id' , 'asc')->get();
    $dataDefinitivaF = ProgramasMesesReportes::where('ano' , (date("Y")))->orderby('id' , 'asc')->get();

    $dataDefinitivaB = ProgramasComponentes::where('ano' , (date("Y")))->where('id_programa' , 1)->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF as $keymesB => $valuemesB) {
          foreach ($dataDefinitivaB as $keyB => $valorB) {
            $resultPormes = ProgramasReportes::where('id_programa' , 1)->where('id_mes_reporte', $valuemesB->id)->where('id_componente', $valorB->id)->first();
              $valorB[str_replace(' ' ,'',$valuemesB->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
          }
      }

      $dataDefinitivaC = ProgramasIndicadores::where('ano' , (date("Y")))->where('id_programa' , 1)->orderby('id_proposito' , 'desc')->get();
      foreach ($dataDefinitivaF as $keymesC => $valuemesC) {
        foreach ($dataDefinitivaC as $keyC => $valorC) {
          $resultPormes = ProgramasReportes::where('id_programa' , 1)->where('id_mes_reporte', $valuemesC->id)->where('id_indicador', $valorC->id)->first();
          $valorC[str_replace(' ' ,'',$valuemesC->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
          $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_numerador'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_denominador'] =  $resultPormes ? $resultPormes->denominador : null;
        }
      }

      $dataDefinitivaD = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 1)->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF as $keymesF => $valuemesF) {
        foreach ($dataDefinitivaD as $keyD => $valorD) {
          $resultPormes = ProgramasReportes::where('id_programa' , 1)->where('id_mes_reporte', $valuemesF->id)->where('id_cobertura', $valorD->id)->first();
          $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_numeradorp'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_resultadop'] =  $resultPormes ? $resultPormes->resultado : null;
            
        }
      }
          $obj = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 1)->where('cod_interno' , 'obj')->first();
          $bnf = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 1)->where('cod_interno' , 'bnf')->first();
        foreach ($dataDefinitivaD as $keyco => $valueco) {
          if ($valueco->cod_interno =='t'){
            for ($i=1; $i < 4; $i++) { 
            $resultado = 0;
            $formula = $valueco->f_t;
            $d_obj = $obj['efectivo_'.$i];
            $d_bnf = $bnf['efectivo_'.$i];
            eval('$resultado =' .$formula.';');
            $valueco['efectivo_'.$i] = round($resultado, 2);
            }
          }
        }

      $dataDefinitivaE3 = ProgramasPresupuesto::whereIn('fecha',[(date("Y")-1),(date("Y")-2)])->where('ano' , (date("Y")))->where('id_programa' , 1)->orderby('id' , 'asc')->get();
      $dataDefinitivaE2 = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , 1)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->first();
      $dataDefinitivaE = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , 1)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->get();
          foreach ($dataDefinitivaF as $keymesG => $valuemesG) {
            foreach ($dataDefinitivaE as $keyE => $valorE) {
              $resultPormes = ProgramasReportes::where('id_programa' , 1)->where('id_mes_reporte', $valuemesG->id)->where('id_presupuesto', $valorE->id)->first();
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_vigente'] =  $resultPormes ? $resultPormes->numerador : null;
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecutados'] =  $resultPormes ? $resultPormes->denominador : null;
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecucion'] =  $resultPormes ? $resultPormes->resultado : null; 
            }
          }

      $observacionReporte = '';
      $observacion = '';
      $reportMesActual = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , 1)->max('id_mes_reporte');
      if ($reportMesActual) {
        $reporte = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , 1)->where('id_mes_reporte', $reportMesActual)->first();
        $observacionReporte = $reporte->observacion_rep;
        $observacion = $reporte->observacion;
      }

      switch ($tipoReporte) {
        case 'PDF':

          $data = [
            'dataDefinitivaA' => $dataDefinitivaA,
            'dataDefinitivaB' => $dataDefinitivaB,
            'dataDefinitivaC' => $dataDefinitivaC,
            'dataDefinitivaD' => $dataDefinitivaD,
            'dataDefinitivaE' => $dataDefinitivaE,
            'dataDefinitivaE2' => $dataDefinitivaE2,
            'dataDefinitivaE3' => $dataDefinitivaE3,
            'dataDefinitivaF' => $dataDefinitivaF,
            'resultPormes' => $resultPormes,
            'observacion' => $observacion,
            'observacionReporte' => $observacionReporte,
          ];
          
          $pdf = PDF::loadView('sigc.programas_reporte_cem', $data)
          ->setPaper('legal', 'portrait');
          return $pdf->download('PROGRAMA_CEM.pdf'); //construcción PDF

        break;
        case 'XLS':
          Excel::create("reporteCEM", function ($excel) use ($dataDefinitivaA , $dataDefinitivaB, $dataDefinitivaC, $dataDefinitivaD, 
                                                            $dataDefinitivaE , $dataDefinitivaE2, $dataDefinitivaE3, $dataDefinitivaF,
                                                            $resultPormes, $observacion, $observacionReporte) {
          $excel->setTitle("reporteCEM");
          $excel->sheet("reporteCEM", function ($sheet) use ($dataDefinitivaA , $dataDefinitivaB, $dataDefinitivaC, $dataDefinitivaD, 
                                                            $dataDefinitivaE , $dataDefinitivaE2, $dataDefinitivaE3, $dataDefinitivaF,
                                                            $resultPormes, $observacion, $observacionReporte) {
          $sheet->loadView('sigc.programas_reporte_cem_ex_excel')->with('dataDefinitivaA', $dataDefinitivaA)->with('dataDefinitivaB', $dataDefinitivaB)
                                                                  ->with('dataDefinitivaC', $dataDefinitivaC)->with('dataDefinitivaD', $dataDefinitivaD)
                                                                  ->with('dataDefinitivaE', $dataDefinitivaE)->with('dataDefinitivaE2', $dataDefinitivaE2)
                                                                  ->with('dataDefinitivaE3', $dataDefinitivaE3)->with('dataDefinitivaF', $dataDefinitivaF)
                                                                  ->with('resultPormes', $resultPormes)->with('observacion', $observacion)->with('observacionReporte', $observacionReporte);
          });
        })->download('xlsx');

        return back();
        break;
        default:
        break;
        }
  }

  public function indexDPS(Request $request)
  {

    $dataDefinitivaA = Programas::where('ano' , (date("Y")))->where('id' , 2)->orderby('id' , 'asc')->get();
    $dataDefinitivaF = ProgramasMesesReportes::where('ano' , (date("Y")))->orderby('id' , 'asc')->get();

    $dataDefinitivaB = ProgramasComponentes::where('ano' , (date("Y")))->where('id_programa' , 2)->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF as $keymesB => $valuemesB) {
          foreach ($dataDefinitivaB as $keyB => $valorB) {
            $resultPormes = ProgramasReportes::where('id_programa' , 2)->where('id_mes_reporte', $valuemesB->id)->where('id_componente', $valorB->id)->first();
              $valorB[str_replace(' ' ,'',$valuemesB->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
          }
      }

      $dataDefinitivaC = ProgramasIndicadores::where('ano' , (date("Y")))->where('id_programa' , 2)->orderby('id_proposito' , 'desc')->get();
      foreach ($dataDefinitivaF as $keymesC => $valuemesC) {
        foreach ($dataDefinitivaC as $keyC => $valorC) {
          $resultPormes = ProgramasReportes::where('id_programa' , 2)->where('id_mes_reporte', $valuemesC->id)->where('id_indicador', $valorC->id)->first();
          $valorC[str_replace(' ' ,'',$valuemesC->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
          $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_numerador'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_denominador'] =  $resultPormes ? $resultPormes->denominador : null;
        }
      }

      $dataDefinitivaD = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 2)->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF as $keymesF => $valuemesF) {
        foreach ($dataDefinitivaD as $keyD => $valorD) {
          $resultPormes = ProgramasReportes::where('id_programa' , 2)->where('id_mes_reporte', $valuemesF->id)->where('id_cobertura', $valorD->id)->first();
          $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_numeradorp'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_resultadop'] =  $resultPormes ? $resultPormes->resultado : null;
            
        }
      }
          $obj = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 2)->where('cod_interno' , 'obj')->first();
          $bnf = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 2)->where('cod_interno' , 'bnf')->first();
        foreach ($dataDefinitivaD as $keyco => $valueco) {
          if ($valueco->cod_interno =='t'){
            for ($i=1; $i < 4; $i++) { 
            $resultado = 0;
            $formula = $valueco->f_t;
            $d_obj = $obj['efectivo_'.$i];
            $d_bnf = $bnf['efectivo_'.$i];
            eval('$resultado =' .$formula.';');
            $valueco['efectivo_'.$i] = round($resultado, 2);
            }
          }
        }

      $dataDefinitivaE3 = ProgramasPresupuesto::whereIn('fecha',[(date("Y")-1),(date("Y")-2)])->where('ano' , (date("Y")))->where('id_programa' , 2)->orderby('id' , 'asc')->get();
      $dataDefinitivaE2 = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , 2)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->first();
      $dataDefinitivaE = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , 2)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->get();
          foreach ($dataDefinitivaF as $keymesG => $valuemesG) {
            foreach ($dataDefinitivaE as $keyE => $valorE) {
              $resultPormes = ProgramasReportes::where('id_programa' , 2)->where('id_mes_reporte', $valuemesG->id)->where('id_presupuesto', $valorE->id)->first();
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_vigente'] =  $resultPormes ? $resultPormes->numerador : null;
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecutados'] =  $resultPormes ? $resultPormes->denominador : null;
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecucion'] =  $resultPormes ? $resultPormes->resultado : null; 
            }
          }
    
          $observacionReporte = '';
          $observacion = '';
          $reportMesActual = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , 2)->max('id_mes_reporte');
          if ($reportMesActual) {
            $reporte = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , 2)->where('id_mes_reporte', $reportMesActual)->first();
            $observacionReporte = $reporte->observacion_rep;
            $observacion = $reporte->observacion;
          }
        

      return view('sigc.programas_dps')
      ->with('dataDefinitivaA' , $dataDefinitivaA)
      ->with('dataDefinitivaB' , $dataDefinitivaB)
      ->with('dataDefinitivaC' , $dataDefinitivaC)
      ->with('dataDefinitivaD' , $dataDefinitivaD)
      ->with('dataDefinitivaE' , $dataDefinitivaE)
      ->with('dataDefinitivaE2' , $dataDefinitivaE2)
      ->with('dataDefinitivaE3' , $dataDefinitivaE3)
      ->with('dataDefinitivaF' , $dataDefinitivaF)
      ->with('resultPormes' , $resultPormes)
      ->with('observacion' , $observacion)
      ->with('observacionReporte' , $observacionReporte);
    
  }

  public function reporteDPS(Request $request) {

    $tipoReporte = $request->imprimir;

    $dataDefinitivaA = Programas::where('ano' , (date("Y")))->where('id' , 2)->orderby('id' , 'asc')->get();
    $dataDefinitivaF = ProgramasMesesReportes::where('ano' , (date("Y")))->orderby('id' , 'asc')->get();

    $dataDefinitivaB = ProgramasComponentes::where('ano' , (date("Y")))->where('id_programa' , 2)->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF as $keymesB => $valuemesB) {
          foreach ($dataDefinitivaB as $keyB => $valorB) {
            $resultPormes = ProgramasReportes::where('id_programa' , 2)->where('id_mes_reporte', $valuemesB->id)->where('id_componente', $valorB->id)->first();
              $valorB[str_replace(' ' ,'',$valuemesB->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
          }
      }

      $dataDefinitivaC = ProgramasIndicadores::where('ano' , (date("Y")))->where('id_programa' , 2)->orderby('id_proposito' , 'desc')->get();
      foreach ($dataDefinitivaF as $keymesC => $valuemesC) {
        foreach ($dataDefinitivaC as $keyC => $valorC) {
          $resultPormes = ProgramasReportes::where('id_programa' , 2)->where('id_mes_reporte', $valuemesC->id)->where('id_indicador', $valorC->id)->first();
          $valorC[str_replace(' ' ,'',$valuemesC->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
          $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_numerador'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_denominador'] =  $resultPormes ? $resultPormes->denominador : null;
        }
      }

      $dataDefinitivaD = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 2)->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF as $keymesF => $valuemesF) {
        foreach ($dataDefinitivaD as $keyD => $valorD) {
          $resultPormes = ProgramasReportes::where('id_programa' , 2)->where('id_mes_reporte', $valuemesF->id)->where('id_cobertura', $valorD->id)->first();
          $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_numeradorp'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_resultadop'] =  $resultPormes ? $resultPormes->resultado : null;
            
        }
      }
          $obj = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 2)->where('cod_interno' , 'obj')->first();
          $bnf = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 2)->where('cod_interno' , 'bnf')->first();
        foreach ($dataDefinitivaD as $keyco => $valueco) {
          if ($valueco->cod_interno =='t'){
            for ($i=1; $i < 4; $i++) { 
            $resultado = 0;
            $formula = $valueco->f_t;
            $d_obj = $obj['efectivo_'.$i];
            $d_bnf = $bnf['efectivo_'.$i];
            eval('$resultado =' .$formula.';');
            $valueco['efectivo_'.$i] = round($resultado, 2);
            }
          }
        }

      $dataDefinitivaE3 = ProgramasPresupuesto::whereIn('fecha',[(date("Y")-1),(date("Y")-2)])->where('ano' , (date("Y")))->where('id_programa' , 2)->orderby('id' , 'asc')->get();
      $dataDefinitivaE2 = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , 2)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->first();
      $dataDefinitivaE = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , 2)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->get();
          foreach ($dataDefinitivaF as $keymesG => $valuemesG) {
            foreach ($dataDefinitivaE as $keyE => $valorE) {
              $resultPormes = ProgramasReportes::where('id_programa' , 2)->where('id_mes_reporte', $valuemesG->id)->where('id_presupuesto', $valorE->id)->first();
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_vigente'] =  $resultPormes ? $resultPormes->numerador : null;
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecutados'] =  $resultPormes ? $resultPormes->denominador : null;
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecucion'] =  $resultPormes ? $resultPormes->resultado : null; 
            }
          }
    

          $observacionReporte = '';
          $observacion = '';
          $reportMesActual = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , 2)->max('id_mes_reporte');
          if ($reportMesActual) {
            $reporte = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , 2)->where('id_mes_reporte', $reportMesActual)->first();
            $observacionReporte = $reporte->observacion_rep;
            $observacion = $reporte->observacion;
          }
    
          switch ($tipoReporte) {
            case 'PDF':
    
              $data = [
                'dataDefinitivaA' => $dataDefinitivaA,
                'dataDefinitivaB' => $dataDefinitivaB,
                'dataDefinitivaC' => $dataDefinitivaC,
                'dataDefinitivaD' => $dataDefinitivaD,
                'dataDefinitivaE' => $dataDefinitivaE,
                'dataDefinitivaE2' => $dataDefinitivaE2,
                'dataDefinitivaE3' => $dataDefinitivaE3,
                'dataDefinitivaF' => $dataDefinitivaF,
                'resultPormes' => $resultPormes,
                'observacion' => $observacion,
                'observacionReporte' => $observacionReporte,
          ];

          $pdf = PDF::loadView('sigc.programas_reporte_dps', $data)
          ->setPaper('legal', 'portrait');
          return $pdf->download('PROGRAMA_DPS.pdf'); //construcción PDF

        break;
        case 'XLS':
          Excel::create("reporteDPS", function ($excel) use ($dataDefinitivaA , $dataDefinitivaB, $dataDefinitivaC, $dataDefinitivaD, 
                                                            $dataDefinitivaE , $dataDefinitivaE2, $dataDefinitivaE3, $dataDefinitivaF,
                                                            $resultPormes, $observacion, $observacionReporte) {
          $excel->setTitle("reporteDPS");
          $excel->sheet("reporteDPS", function ($sheet) use ($dataDefinitivaA , $dataDefinitivaB, $dataDefinitivaC, $dataDefinitivaD, 
                                                            $dataDefinitivaE , $dataDefinitivaE2, $dataDefinitivaE3, $dataDefinitivaF,
                                                            $resultPormes, $observacion, $observacionReporte) {
          $sheet->loadView('sigc.programas_reporte_dps_ex_excel')->with('dataDefinitivaA', $dataDefinitivaA)->with('dataDefinitivaB', $dataDefinitivaB)
                                                                  ->with('dataDefinitivaC', $dataDefinitivaC)->with('dataDefinitivaD', $dataDefinitivaD)
                                                                  ->with('dataDefinitivaE', $dataDefinitivaE)->with('dataDefinitivaE2', $dataDefinitivaE2)
                                                                  ->with('dataDefinitivaE3', $dataDefinitivaE3)->with('dataDefinitivaF', $dataDefinitivaF)
                                                                  ->with('resultPormes', $resultPormes)->with('observacion', $observacion)->with('observacionReporte', $observacionReporte);
          });
        })->download('xlsx');

        return back();
        break;
        default:
        break;
        }

  }

  public function indexFONDEPORTE(Request $request)
  {

    $dataDefinitivaA = Programas::where('ano' , (date("Y")))->where('id' , 3)->orderby('id' , 'asc')->get();
    $dataDefinitivaF = ProgramasMesesReportes::where('ano' , (date("Y")))->orderby('id' , 'asc')->get();

    $dataDefinitivaB = ProgramasComponentes::where('ano' , (date("Y")))->where('id_programa' , 3)->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF as $keymesB => $valuemesB) {
          foreach ($dataDefinitivaB as $keyB => $valorB) {
            $resultPormes = ProgramasReportes::where('id_programa' , 3)->where('id_mes_reporte', $valuemesB->id)->where('id_componente', $valorB->id)->first();
              $valorB[str_replace(' ' ,'',$valuemesB->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
          }
      }

      $dataDefinitivaC = ProgramasIndicadores::where('ano' , (date("Y")))->where('id_programa' , 3)->orderby('id_proposito' , 'desc')->get();
      foreach ($dataDefinitivaF as $keymesC => $valuemesC) {
        foreach ($dataDefinitivaC as $keyC => $valorC) {
          $resultPormes = ProgramasReportes::where('id_programa' , 3)->where('id_mes_reporte', $valuemesC->id)->where('id_indicador', $valorC->id)->first();
          $valorC[str_replace(' ' ,'',$valuemesC->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
          $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_numerador'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_denominador'] =  $resultPormes ? $resultPormes->denominador : null;
        }
      }

      $dataDefinitivaD = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 3)->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF as $keymesF => $valuemesF) {
        foreach ($dataDefinitivaD as $keyD => $valorD) {
          $resultPormes = ProgramasReportes::where('id_programa' , 3)->where('id_mes_reporte', $valuemesF->id)->where('id_cobertura', $valorD->id)->first();
          $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_numeradorp'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_resultadop'] =  $resultPormes ? $resultPormes->resultado : null;
            
        }
      }
          $obj = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 3)->where('cod_interno' , 'obj')->first();
          $bnf = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 3)->where('cod_interno' , 'bnf')->first();
        foreach ($dataDefinitivaD as $keyco => $valueco) {
          if ($valueco->cod_interno =='t'){
            for ($i=1; $i < 4; $i++) { 
            $resultado = 0;
            $formula = $valueco->f_t;
            $d_obj = $obj['efectivo_'.$i];
            $d_bnf = $bnf['efectivo_'.$i];
            eval('$resultado =' .$formula.';');
            $valueco['efectivo_'.$i] = round($resultado, 2);
            }
          }
        }

      $dataDefinitivaE3 = ProgramasPresupuesto::whereIn('fecha',[(date("Y")-1),(date("Y")-2)])->where('ano' , (date("Y")))->where('id_programa' , 3)->orderby('id' , 'asc')->get();
      $dataDefinitivaE2 = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , 3)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->first();
      $dataDefinitivaE = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , 3)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->get();
          foreach ($dataDefinitivaF as $keymesG => $valuemesG) {
            foreach ($dataDefinitivaE as $keyE => $valorE) {
              $resultPormes = ProgramasReportes::where('id_programa' , 3)->where('id_mes_reporte', $valuemesG->id)->where('id_presupuesto', $valorE->id)->first();
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_vigente'] =  $resultPormes ? $resultPormes->numerador : null;
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecutados'] =  $resultPormes ? $resultPormes->denominador : null;
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecucion'] =  $resultPormes ? $resultPormes->resultado : null; 
            }
          }
    
          $observacionReporte = '';
          $observacion = '';
          $reportMesActual = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , 3)->max('id_mes_reporte');
          if ($reportMesActual) {
            $reporte = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , 3)->where('id_mes_reporte', $reportMesActual)->first();
            $observacionReporte = $reporte->observacion_rep;
            $observacion = $reporte->observacion;
          }
        

      return view('sigc.programas_fondeporte')
      ->with('dataDefinitivaA' , $dataDefinitivaA)
      ->with('dataDefinitivaB' , $dataDefinitivaB)
      ->with('dataDefinitivaC' , $dataDefinitivaC)
      ->with('dataDefinitivaD' , $dataDefinitivaD)
      ->with('dataDefinitivaE' , $dataDefinitivaE)
      ->with('dataDefinitivaE2' , $dataDefinitivaE2)
      ->with('dataDefinitivaE3' , $dataDefinitivaE3)
      ->with('dataDefinitivaF' , $dataDefinitivaF)
      ->with('resultPormes' , $resultPormes)
      ->with('observacion' , $observacion)
      ->with('observacionReporte' , $observacionReporte);
    
  }

  public function reporteFONDEPORTE(Request $request) {

    $tipoReporte = $request->imprimir;

    $dataDefinitivaA = Programas::where('ano' , (date("Y")))->where('id' , 3)->orderby('id' , 'asc')->get();
    $dataDefinitivaF = ProgramasMesesReportes::where('ano' , (date("Y")))->orderby('id' , 'asc')->get();

    $dataDefinitivaB = ProgramasComponentes::where('ano' , (date("Y")))->where('id_programa' , 3)->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF as $keymesB => $valuemesB) {
          foreach ($dataDefinitivaB as $keyB => $valorB) {
            $resultPormes = ProgramasReportes::where('id_programa' , 3)->where('id_mes_reporte', $valuemesB->id)->where('id_componente', $valorB->id)->first();
              $valorB[str_replace(' ' ,'',$valuemesB->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
          }
      }

      $dataDefinitivaC = ProgramasIndicadores::where('ano' , (date("Y")))->where('id_programa' , 3)->orderby('id_proposito' , 'desc')->get();
      foreach ($dataDefinitivaF as $keymesC => $valuemesC) {
        foreach ($dataDefinitivaC as $keyC => $valorC) {
          $resultPormes = ProgramasReportes::where('id_programa' , 3)->where('id_mes_reporte', $valuemesC->id)->where('id_indicador', $valorC->id)->first();
          $valorC[str_replace(' ' ,'',$valuemesC->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
          $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_numerador'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_denominador'] =  $resultPormes ? $resultPormes->denominador : null;
        }
      }

      $dataDefinitivaD = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 3)->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF as $keymesF => $valuemesF) {
        foreach ($dataDefinitivaD as $keyD => $valorD) {
          $resultPormes = ProgramasReportes::where('id_programa' , 3)->where('id_mes_reporte', $valuemesF->id)->where('id_cobertura', $valorD->id)->first();
          $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_numeradorp'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_resultadop'] =  $resultPormes ? $resultPormes->resultado : null;
            
        }
      }
          $obj = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 3)->where('cod_interno' , 'obj')->first();
          $bnf = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 3)->where('cod_interno' , 'bnf')->first();
        foreach ($dataDefinitivaD as $keyco => $valueco) {
          if ($valueco->cod_interno =='t'){
            for ($i=1; $i < 4; $i++) { 
            $resultado = 0;
            $formula = $valueco->f_t;
            $d_obj = $obj['efectivo_'.$i];
            $d_bnf = $bnf['efectivo_'.$i];
            eval('$resultado =' .$formula.';');
            $valueco['efectivo_'.$i] = round($resultado, 2);
            }
          }
        }

      $dataDefinitivaE3 = ProgramasPresupuesto::whereIn('fecha',[(date("Y")-1),(date("Y")-2)])->where('ano' , (date("Y")))->where('id_programa' , 3)->orderby('id' , 'asc')->get();
      $dataDefinitivaE2 = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , 3)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->first();
      $dataDefinitivaE = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , 3)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->get();
          foreach ($dataDefinitivaF as $keymesG => $valuemesG) {
            foreach ($dataDefinitivaE as $keyE => $valorE) {
              $resultPormes = ProgramasReportes::where('id_programa' , 3)->where('id_mes_reporte', $valuemesG->id)->where('id_presupuesto', $valorE->id)->first();
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_vigente'] =  $resultPormes ? $resultPormes->numerador : null;
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecutados'] =  $resultPormes ? $resultPormes->denominador : null;
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecucion'] =  $resultPormes ? $resultPormes->resultado : null; 
            }
          }

          $observacionReporte = '';
      $observacion = '';
      $reportMesActual = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , 3)->max('id_mes_reporte');
      if ($reportMesActual) {
        $reporte = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , 3)->where('id_mes_reporte', $reportMesActual)->first();
        $observacionReporte = $reporte->observacion_rep;
        $observacion = $reporte->observacion;
      }
    
          switch ($tipoReporte) {
            case 'PDF':
    
              $data = [
                'dataDefinitivaA' => $dataDefinitivaA,
                'dataDefinitivaB' => $dataDefinitivaB,
                'dataDefinitivaC' => $dataDefinitivaC,
                'dataDefinitivaD' => $dataDefinitivaD,
                'dataDefinitivaE' => $dataDefinitivaE,
                'dataDefinitivaE2' => $dataDefinitivaE2,
                'dataDefinitivaE3' => $dataDefinitivaE3,
                'dataDefinitivaF' => $dataDefinitivaF,
                'resultPormes' => $resultPormes,
                'observacion' => $observacion,
                'observacionReporte' => $observacionReporte,
          ];

          $pdf = PDF::loadView('sigc.programas_reporte_fondeporte', $data)
          ->setPaper('legal', 'portrait');
          return $pdf->download('PROGRAMA_FONDEPORTE.pdf'); //construcción PDF

        break;
        case 'XLS':
          Excel::create("reporteFONDEPORTE", function ($excel) use ($dataDefinitivaA , $dataDefinitivaB, $dataDefinitivaC, $dataDefinitivaD, 
                                                            $dataDefinitivaE , $dataDefinitivaE2, $dataDefinitivaE3, $dataDefinitivaF,
                                                            $resultPormes, $observacion, $observacionReporte) {
          $excel->setTitle("reporteFONDEPORTE");
          $excel->sheet("reporteFONDEPORTE", function ($sheet) use ($dataDefinitivaA , $dataDefinitivaB, $dataDefinitivaC, $dataDefinitivaD, 
                                                            $dataDefinitivaE , $dataDefinitivaE2, $dataDefinitivaE3, $dataDefinitivaF,
                                                            $resultPormes, $observacion, $observacionReporte) {
          $sheet->loadView('sigc.programas_reporte_fondeporte_ex_excel')->with('dataDefinitivaA', $dataDefinitivaA)->with('dataDefinitivaB', $dataDefinitivaB)
                                                                  ->with('dataDefinitivaC', $dataDefinitivaC)->with('dataDefinitivaD', $dataDefinitivaD)
                                                                  ->with('dataDefinitivaE', $dataDefinitivaE)->with('dataDefinitivaE2', $dataDefinitivaE2)
                                                                  ->with('dataDefinitivaE3', $dataDefinitivaE3)->with('dataDefinitivaF', $dataDefinitivaF)
                                                                  ->with('resultPormes', $resultPormes)->with('observacion', $observacion)->with('observacionReporte', $observacionReporte);
          });
        })->download('xlsx');

        return back();
        break;
        default:
        break;
        }

  }


  public function indexACD(Request $request)
  {

    $dataDefinitivaA = Programas::where('ano' , (date("Y")))->where('id' , 4)->orderby('id' , 'asc')->get();
    $dataDefinitivaF = ProgramasMesesReportes::where('ano' , (date("Y")))->orderby('id' , 'asc')->get();

    $dataDefinitivaB = ProgramasComponentes::where('ano' , (date("Y")))->where('id_programa' , 4)->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF as $keymesB => $valuemesB) {
          foreach ($dataDefinitivaB as $keyB => $valorB) {
            $resultPormes = ProgramasReportes::where('id_programa' , 4)->where('id_mes_reporte', $valuemesB->id)->where('id_componente', $valorB->id)->first();
              $valorB[str_replace(' ' ,'',$valuemesB->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
          }
      }

      $dataDefinitivaC = ProgramasIndicadores::where('ano' , (date("Y")))->where('id_programa' , 4)->orderby('id_proposito' , 'desc')->get();
      foreach ($dataDefinitivaF as $keymesC => $valuemesC) {
        foreach ($dataDefinitivaC as $keyC => $valorC) {
          $resultPormes = ProgramasReportes::where('id_programa' , 4)->where('id_mes_reporte', $valuemesC->id)->where('id_indicador', $valorC->id)->first();
          $valorC[str_replace(' ' ,'',$valuemesC->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
          $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_numerador'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_denominador'] =  $resultPormes ? $resultPormes->denominador : null;
        }
      }

      $dataDefinitivaD = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 4)->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF as $keymesF => $valuemesF) {
        foreach ($dataDefinitivaD as $keyD => $valorD) {
          $resultPormes = ProgramasReportes::where('id_programa' , 4)->where('id_mes_reporte', $valuemesF->id)->where('id_cobertura', $valorD->id)->first();
          $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_numeradorp'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_resultadop'] =  $resultPormes ? $resultPormes->resultado : null;
            
        }
      }
          $obj = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 4)->where('cod_interno' , 'obj')->first();
          $bnf = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 4)->where('cod_interno' , 'bnf')->first();
        foreach ($dataDefinitivaD as $keyco => $valueco) {
          if ($valueco->cod_interno =='t'){
            for ($i=1; $i < 4; $i++) { 
            $resultado = 0;
            $formula = $valueco->f_t;
            $d_obj = $obj['efectivo_'.$i];
            $d_bnf = $bnf['efectivo_'.$i];
            eval('$resultado =' .$formula.';');
            $valueco['efectivo_'.$i] = round($resultado, 2);
            }
          }
        }

      $dataDefinitivaE3 = ProgramasPresupuesto::whereIn('fecha',[(date("Y")-1),(date("Y")-2)])->where('ano' , (date("Y")))->where('id_programa' , 4)->orderby('id' , 'asc')->get();
      $dataDefinitivaE2 = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , 4)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->first();
      $dataDefinitivaE = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , 4)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->get();
          foreach ($dataDefinitivaF as $keymesG => $valuemesG) {
            foreach ($dataDefinitivaE as $keyE => $valorE) {
              $resultPormes = ProgramasReportes::where('id_programa' , 4)->where('id_mes_reporte', $valuemesG->id)->where('id_presupuesto', $valorE->id)->first();
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_vigente'] =  $resultPormes ? $resultPormes->numerador : null;
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecutados'] =  $resultPormes ? $resultPormes->denominador : null;
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecucion'] =  $resultPormes ? $resultPormes->resultado : null; 
            }
          }
    
          $observacionReporte = '';
          $observacion = '';
          $reportMesActual = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , 4)->max('id_mes_reporte');
          if ($reportMesActual) {
            $reporte = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , 4)->where('id_mes_reporte', $reportMesActual)->first();
            $observacionReporte = $reporte->observacion_rep;
            $observacion = $reporte->observacion;
          }
        

      return view('sigc.programas_acd')
      ->with('dataDefinitivaA' , $dataDefinitivaA)
      ->with('dataDefinitivaB' , $dataDefinitivaB)
      ->with('dataDefinitivaC' , $dataDefinitivaC)
      ->with('dataDefinitivaD' , $dataDefinitivaD)
      ->with('dataDefinitivaE' , $dataDefinitivaE)
      ->with('dataDefinitivaE2' , $dataDefinitivaE2)
      ->with('dataDefinitivaE3' , $dataDefinitivaE3)
      ->with('dataDefinitivaF' , $dataDefinitivaF)
      ->with('resultPormes' , $resultPormes)
      ->with('observacion' , $observacion)
      ->with('observacionReporte' , $observacionReporte);
    
  }

  public function reporteACD(Request $request) {

    $tipoReporte = $request->imprimir;

    $dataDefinitivaA = Programas::where('ano' , (date("Y")))->where('id' , 4)->orderby('id' , 'asc')->get();
    $dataDefinitivaF = ProgramasMesesReportes::where('ano' , (date("Y")))->orderby('id' , 'asc')->get();

    $dataDefinitivaB = ProgramasComponentes::where('ano' , (date("Y")))->where('id_programa' , 4)->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF as $keymesB => $valuemesB) {
          foreach ($dataDefinitivaB as $keyB => $valorB) {
            $resultPormes = ProgramasReportes::where('id_programa' , 4)->where('id_mes_reporte', $valuemesB->id)->where('id_componente', $valorB->id)->first();
              $valorB[str_replace(' ' ,'',$valuemesB->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
          }
      }

      $dataDefinitivaC = ProgramasIndicadores::where('ano' , (date("Y")))->where('id_programa' , 4)->orderby('id_proposito' , 'desc')->get();
      foreach ($dataDefinitivaF as $keymesC => $valuemesC) {
        foreach ($dataDefinitivaC as $keyC => $valorC) {
          $resultPormes = ProgramasReportes::where('id_programa' , 4)->where('id_mes_reporte', $valuemesC->id)->where('id_indicador', $valorC->id)->first();
          $valorC[str_replace(' ' ,'',$valuemesC->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
          $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_numerador'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_denominador'] =  $resultPormes ? $resultPormes->denominador : null;
        }
      }

      $dataDefinitivaD = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 4)->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF as $keymesF => $valuemesF) {
        foreach ($dataDefinitivaD as $keyD => $valorD) {
          $resultPormes = ProgramasReportes::where('id_programa' , 4)->where('id_mes_reporte', $valuemesF->id)->where('id_cobertura', $valorD->id)->first();
          $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_numeradorp'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_resultadop'] =  $resultPormes ? $resultPormes->resultado : null;
            
        }
      }
          $obj = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 4)->where('cod_interno' , 'obj')->first();
          $bnf = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 4)->where('cod_interno' , 'bnf')->first();
        foreach ($dataDefinitivaD as $keyco => $valueco) {
          if ($valueco->cod_interno =='t'){
            for ($i=1; $i < 4; $i++) { 
            $resultado = 0;
            $formula = $valueco->f_t;
            $d_obj = $obj['efectivo_'.$i];
            $d_bnf = $bnf['efectivo_'.$i];
            eval('$resultado =' .$formula.';');
            $valueco['efectivo_'.$i] = round($resultado, 2);
            }
          }
        }

      $dataDefinitivaE3 = ProgramasPresupuesto::whereIn('fecha',[(date("Y")-1),(date("Y")-2)])->where('ano' , (date("Y")))->where('id_programa' , 4)->orderby('id' , 'asc')->get();
      $dataDefinitivaE2 = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , 4)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->first();
      $dataDefinitivaE = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , 4)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->get();
          foreach ($dataDefinitivaF as $keymesG => $valuemesG) {
            foreach ($dataDefinitivaE as $keyE => $valorE) {
              $resultPormes = ProgramasReportes::where('id_programa' , 4)->where('id_mes_reporte', $valuemesG->id)->where('id_presupuesto', $valorE->id)->first();
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_vigente'] =  $resultPormes ? $resultPormes->numerador : null;
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecutados'] =  $resultPormes ? $resultPormes->denominador : null;
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecucion'] =  $resultPormes ? $resultPormes->resultado : null; 
            }
          }
    
          $observacionReporte = '';
      $observacion = '';
      $reportMesActual = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , 4)->max('id_mes_reporte');
      if ($reportMesActual) {
        $reporte = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , 4)->where('id_mes_reporte', $reportMesActual)->first();
        $observacionReporte = $reporte->observacion_rep;
        $observacion = $reporte->observacion;
      }
    
          switch ($tipoReporte) {
            case 'PDF':
    
              $data = [
                'dataDefinitivaA' => $dataDefinitivaA,
                'dataDefinitivaB' => $dataDefinitivaB,
                'dataDefinitivaC' => $dataDefinitivaC,
                'dataDefinitivaD' => $dataDefinitivaD,
                'dataDefinitivaE' => $dataDefinitivaE,
                'dataDefinitivaE2' => $dataDefinitivaE2,
                'dataDefinitivaE3' => $dataDefinitivaE3,
                'dataDefinitivaF' => $dataDefinitivaF,
                'resultPormes' => $resultPormes,
                'observacion' => $observacion,
                'observacionReporte' => $observacionReporte,
          ];

          $pdf = PDF::loadView('sigc.programas_reporte_acd', $data)
          ->setPaper('legal', 'portrait');
          return $pdf->download('PROGRAMA_ACD.pdf'); //construcción PDF

        break;
        case 'XLS':
          Excel::create("reporteACD", function ($excel) use ($dataDefinitivaA , $dataDefinitivaB, $dataDefinitivaC, $dataDefinitivaD, 
                                                            $dataDefinitivaE , $dataDefinitivaE2, $dataDefinitivaE3, $dataDefinitivaF,
                                                            $resultPormes, $observacion, $observacionReporte) {
          $excel->setTitle("reporteACD");
          $excel->sheet("reporteACD", function ($sheet) use ($dataDefinitivaA , $dataDefinitivaB, $dataDefinitivaC, $dataDefinitivaD, 
                                                            $dataDefinitivaE , $dataDefinitivaE2, $dataDefinitivaE3, $dataDefinitivaF,
                                                            $resultPormes, $observacion, $observacionReporte) {
          $sheet->loadView('sigc.programas_reporte_acd_ex_excel')->with('dataDefinitivaA', $dataDefinitivaA)->with('dataDefinitivaB', $dataDefinitivaB)
                                                                  ->with('dataDefinitivaC', $dataDefinitivaC)->with('dataDefinitivaD', $dataDefinitivaD)
                                                                  ->with('dataDefinitivaE', $dataDefinitivaE)->with('dataDefinitivaE2', $dataDefinitivaE2)
                                                                  ->with('dataDefinitivaE3', $dataDefinitivaE3)->with('dataDefinitivaF', $dataDefinitivaF)
                                                                  ->with('resultPormes', $resultPormes)->with('observacion', $observacion)->with('observacionReporte', $observacionReporte);
          });
        })->download('xlsx');

        return back();
        break;
        default:
        break;
        }

  }


  public function indexFDRCP(Request $request)
  {

    $dataDefinitivaA = Programas::where('ano' , (date("Y")))->where('id' , 5)->orderby('id' , 'asc')->get();
    $dataDefinitivaF = ProgramasMesesReportes::where('ano' , (date("Y")))->orderby('id' , 'asc')->get();

    $dataDefinitivaB = ProgramasComponentes::where('ano' , (date("Y")))->where('id_programa' , 5)->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF as $keymesB => $valuemesB) {
          foreach ($dataDefinitivaB as $keyB => $valorB) {
            $resultPormes = ProgramasReportes::where('id_programa' , 5)->where('id_mes_reporte', $valuemesB->id)->where('id_componente', $valorB->id)->first();
              $valorB[str_replace(' ' ,'',$valuemesB->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
          }
      }

      $dataDefinitivaC = ProgramasIndicadores::where('ano' , (date("Y")))->where('id_programa' , 5)->orderby('id_proposito' , 'desc')->get();
      foreach ($dataDefinitivaF as $keymesC => $valuemesC) {
        foreach ($dataDefinitivaC as $keyC => $valorC) {
          $resultPormes = ProgramasReportes::where('id_programa' , 5)->where('id_mes_reporte', $valuemesC->id)->where('id_indicador', $valorC->id)->first();
          $valorC[str_replace(' ' ,'',$valuemesC->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
          $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_numerador'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_denominador'] =  $resultPormes ? $resultPormes->denominador : null;
        }
      }

      $dataDefinitivaD = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 5)->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF as $keymesF => $valuemesF) {
        foreach ($dataDefinitivaD as $keyD => $valorD) {
          $resultPormes = ProgramasReportes::where('id_programa' , 5)->where('id_mes_reporte', $valuemesF->id)->where('id_cobertura', $valorD->id)->first();
          $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_numeradorp'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_resultadop'] =  $resultPormes ? $resultPormes->resultado : null;
            
        }
      }
          $obj = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 5)->where('cod_interno' , 'obj')->first();
          $bnf = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 5)->where('cod_interno' , 'bnf')->first();
        foreach ($dataDefinitivaD as $keyco => $valueco) {
          if ($valueco->cod_interno =='t'){
            for ($i=1; $i < 4; $i++) { 
            $resultado = 0;
            $formula = $valueco->f_t;
            $d_obj = $obj['efectivo_'.$i];
            $d_bnf = $bnf['efectivo_'.$i];
            eval('$resultado =' .$formula.';');
            $valueco['efectivo_'.$i] = round($resultado, 2);
            }
          }
        }

      $dataDefinitivaE3 = ProgramasPresupuesto::whereIn('fecha',[(date("Y")-1),(date("Y")-2)])->where('ano' , (date("Y")))->where('id_programa' , 5)->orderby('id' , 'asc')->get();
      $dataDefinitivaE2 = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , 5)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->first();
      $dataDefinitivaE = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , 5)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->get();
          foreach ($dataDefinitivaF as $keymesG => $valuemesG) {
            foreach ($dataDefinitivaE as $keyE => $valorE) {
              $resultPormes = ProgramasReportes::where('id_programa' , 5)->where('id_mes_reporte', $valuemesG->id)->where('id_presupuesto', $valorE->id)->first();
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_vigente'] =  $resultPormes ? $resultPormes->numerador : null;
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecutados'] =  $resultPormes ? $resultPormes->denominador : null;
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecucion'] =  $resultPormes ? $resultPormes->resultado : null; 
            }
          }
    
          $observacionReporte = '';
          $observacion = '';
          $reportMesActual = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , 5)->max('id_mes_reporte');
          if ($reportMesActual) {
            $reporte = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , 5)->where('id_mes_reporte', $reportMesActual)->first();
            $observacionReporte = $reporte->observacion_rep;
            $observacion = $reporte->observacion;
          }
        

      return view('sigc.programas_fdrcp')
      ->with('dataDefinitivaA' , $dataDefinitivaA)
      ->with('dataDefinitivaB' , $dataDefinitivaB)
      ->with('dataDefinitivaC' , $dataDefinitivaC)
      ->with('dataDefinitivaD' , $dataDefinitivaD)
      ->with('dataDefinitivaE' , $dataDefinitivaE)
      ->with('dataDefinitivaE2' , $dataDefinitivaE2)
      ->with('dataDefinitivaE3' , $dataDefinitivaE3)
      ->with('dataDefinitivaF' , $dataDefinitivaF)
      ->with('resultPormes' , $resultPormes)
      ->with('observacion' , $observacion)
      ->with('observacionReporte' , $observacionReporte);
    
  }

  public function reporteFDRCP(Request $request) {

    $tipoReporte = $request->imprimir;

    $dataDefinitivaA = Programas::where('ano' , (date("Y")))->where('id' , 5)->orderby('id' , 'asc')->get();
    $dataDefinitivaF = ProgramasMesesReportes::where('ano' , (date("Y")))->orderby('id' , 'asc')->get();

    $dataDefinitivaB = ProgramasComponentes::where('ano' , (date("Y")))->where('id_programa' , 5)->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF as $keymesB => $valuemesB) {
          foreach ($dataDefinitivaB as $keyB => $valorB) {
            $resultPormes = ProgramasReportes::where('id_programa' , 5)->where('id_mes_reporte', $valuemesB->id)->where('id_componente', $valorB->id)->first();
              $valorB[str_replace(' ' ,'',$valuemesB->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
          }
      }

      $dataDefinitivaC = ProgramasIndicadores::where('ano' , (date("Y")))->where('id_programa' , 5)->orderby('id_proposito' , 'desc')->get();
      foreach ($dataDefinitivaF as $keymesC => $valuemesC) {
        foreach ($dataDefinitivaC as $keyC => $valorC) {
          $resultPormes = ProgramasReportes::where('id_programa' , 5)->where('id_mes_reporte', $valuemesC->id)->where('id_indicador', $valorC->id)->first();
          $valorC[str_replace(' ' ,'',$valuemesC->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
          $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_numerador'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_denominador'] =  $resultPormes ? $resultPormes->denominador : null;
        }
      }

      $dataDefinitivaD = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 5)->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF as $keymesF => $valuemesF) {
        foreach ($dataDefinitivaD as $keyD => $valorD) {
          $resultPormes = ProgramasReportes::where('id_programa' , 5)->where('id_mes_reporte', $valuemesF->id)->where('id_cobertura', $valorD->id)->first();
          $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_numeradorp'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_resultadop'] =  $resultPormes ? $resultPormes->resultado : null;
            
        }
      }
          $obj = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 5)->where('cod_interno' , 'obj')->first();
          $bnf = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 5)->where('cod_interno' , 'bnf')->first();
        foreach ($dataDefinitivaD as $keyco => $valueco) {
          if ($valueco->cod_interno =='t'){
            for ($i=1; $i < 4; $i++) { 
            $resultado = 0;
            $formula = $valueco->f_t;
            $d_obj = $obj['efectivo_'.$i];
            $d_bnf = $bnf['efectivo_'.$i];
            eval('$resultado =' .$formula.';');
            $valueco['efectivo_'.$i] = round($resultado, 2);
            }
          }
        }

      $dataDefinitivaE3 = ProgramasPresupuesto::whereIn('fecha',[(date("Y")-1),(date("Y")-2)])->where('ano' , (date("Y")))->where('id_programa' , 5)->orderby('id' , 'asc')->get();
      $dataDefinitivaE2 = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , 5)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->first();
      $dataDefinitivaE = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , 5)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->get();
          foreach ($dataDefinitivaF as $keymesG => $valuemesG) {
            foreach ($dataDefinitivaE as $keyE => $valorE) {
              $resultPormes = ProgramasReportes::where('id_programa' , 5)->where('id_mes_reporte', $valuemesG->id)->where('id_presupuesto', $valorE->id)->first();
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_vigente'] =  $resultPormes ? $resultPormes->numerador : null;
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecutados'] =  $resultPormes ? $resultPormes->denominador : null;
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecucion'] =  $resultPormes ? $resultPormes->resultado : null; 
            }
          }
    
      $observacionReporte = '';
      $observacion = '';
      $reportMesActual = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , 5)->max('id_mes_reporte');
      if ($reportMesActual) {
        $reporte = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , 5)->where('id_mes_reporte', $reportMesActual)->first();
        $observacionReporte = $reporte->observacion_rep;
        $observacion = $reporte->observacion;
      }
    
          switch ($tipoReporte) {
            case 'PDF':
    
              $data = [
                'dataDefinitivaA' => $dataDefinitivaA,
                'dataDefinitivaB' => $dataDefinitivaB,
                'dataDefinitivaC' => $dataDefinitivaC,
                'dataDefinitivaD' => $dataDefinitivaD,
                'dataDefinitivaE' => $dataDefinitivaE,
                'dataDefinitivaE2' => $dataDefinitivaE2,
                'dataDefinitivaE3' => $dataDefinitivaE3,
                'dataDefinitivaF' => $dataDefinitivaF,
                'resultPormes' => $resultPormes,
                'observacion' => $observacion,
                'observacionReporte' => $observacionReporte,
          ];

          $pdf = PDF::loadView('sigc.programas_reporte_fdrcp', $data)
          ->setPaper('legal', 'portrait');
          return $pdf->download('PROGRAMA_FDRCP.pdf'); //construcción PDF

        break;
        case 'XLS':
          Excel::create("reporteFDRCP", function ($excel) use ($dataDefinitivaA , $dataDefinitivaB, $dataDefinitivaC, $dataDefinitivaD, 
                                                            $dataDefinitivaE , $dataDefinitivaE2, $dataDefinitivaE3, $dataDefinitivaF,
                                                            $resultPormes, $observacion, $observacionReporte) {
          $excel->setTitle("reporteFDRCP");
          $excel->sheet("reporteFDRCP", function ($sheet) use ($dataDefinitivaA , $dataDefinitivaB, $dataDefinitivaC, $dataDefinitivaD, 
                                                            $dataDefinitivaE , $dataDefinitivaE2, $dataDefinitivaE3, $dataDefinitivaF,
                                                            $resultPormes, $observacion, $observacionReporte) {
          $sheet->loadView('sigc.programas_reporte_fdrcp_ex_excel')->with('dataDefinitivaA', $dataDefinitivaA)->with('dataDefinitivaB', $dataDefinitivaB)
                                                                  ->with('dataDefinitivaC', $dataDefinitivaC)->with('dataDefinitivaD', $dataDefinitivaD)
                                                                  ->with('dataDefinitivaE', $dataDefinitivaE)->with('dataDefinitivaE2', $dataDefinitivaE2)
                                                                  ->with('dataDefinitivaE3', $dataDefinitivaE3)->with('dataDefinitivaF', $dataDefinitivaF)
                                                                  ->with('resultPormes', $resultPormes)->with('observacion', $observacion)->with('observacionReporte', $observacionReporte);
          });
        })->download('xlsx');

        return back();
        break;
        default:
        break;
        }

  }


  public function indexSNCD(Request $request)
  {

    $dataDefinitivaA = Programas::where('ano' , (date("Y")))->where('id' , 6)->orderby('id' , 'asc')->get();
    $dataDefinitivaF = ProgramasMesesReportes::where('ano' , (date("Y")))->orderby('id' , 'asc')->get();

    $dataDefinitivaB = ProgramasComponentes::where('ano' , (date("Y")))->where('id_programa' , 6)->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF as $keymesB => $valuemesB) {
          foreach ($dataDefinitivaB as $keyB => $valorB) {
            $resultPormes = ProgramasReportes::where('id_programa' , 6)->where('id_mes_reporte', $valuemesB->id)->where('id_componente', $valorB->id)->first();
              $valorB[str_replace(' ' ,'',$valuemesB->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
          }
      }

      $dataDefinitivaC = ProgramasIndicadores::where('ano' , (date("Y")))->where('id_programa' , 6)->orderby('id_proposito' , 'desc')->get();
      foreach ($dataDefinitivaF as $keymesC => $valuemesC) {
        foreach ($dataDefinitivaC as $keyC => $valorC) {
          $resultPormes = ProgramasReportes::where('id_programa' , 6)->where('id_mes_reporte', $valuemesC->id)->where('id_indicador', $valorC->id)->first();
          $valorC[str_replace(' ' ,'',$valuemesC->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
          $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_numerador'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_denominador'] =  $resultPormes ? $resultPormes->denominador : null;
        }
      }

      $dataDefinitivaD = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 6)->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF as $keymesF => $valuemesF) {
        foreach ($dataDefinitivaD as $keyD => $valorD) {
          $resultPormes = ProgramasReportes::where('id_programa' , 6)->where('id_mes_reporte', $valuemesF->id)->where('id_cobertura', $valorD->id)->first();
          $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_numeradorp'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_resultadop'] =  $resultPormes ? $resultPormes->resultado : null;
            
        }
      }
          $obj = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 6)->where('cod_interno' , 'obj')->first();
          $bnf = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 6)->where('cod_interno' , 'bnf')->first();
        foreach ($dataDefinitivaD as $keyco => $valueco) {
          if ($valueco->cod_interno =='t'){
            for ($i=1; $i < 4; $i++) { 
            $resultado = 0;
            $formula = $valueco->f_t;
            $d_obj = $obj['efectivo_'.$i];
            $d_bnf = $bnf['efectivo_'.$i];
            eval('$resultado =' .$formula.';');
            $valueco['efectivo_'.$i] = round($resultado, 2);
            }
          }
        }

      $dataDefinitivaE3 = ProgramasPresupuesto::whereIn('fecha',[(date("Y")-1),(date("Y")-2)])->where('ano' , (date("Y")))->where('id_programa' , 6)->orderby('id' , 'asc')->get();
      $dataDefinitivaE2 = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , 6)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->first();
      $dataDefinitivaE = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , 6)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->get();
          foreach ($dataDefinitivaF as $keymesG => $valuemesG) {
            foreach ($dataDefinitivaE as $keyE => $valorE) {
              $resultPormes = ProgramasReportes::where('id_programa' , 6)->where('id_mes_reporte', $valuemesG->id)->where('id_presupuesto', $valorE->id)->first();
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_vigente'] =  $resultPormes ? $resultPormes->numerador : null;
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecutados'] =  $resultPormes ? $resultPormes->denominador : null;
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecucion'] =  $resultPormes ? $resultPormes->resultado : null; 
            }
          }
    
          $observacionReporte = '';
          $observacion = '';
          $reportMesActual = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , 6)->max('id_mes_reporte');
          if ($reportMesActual) {
            $reporte = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , 6)->where('id_mes_reporte', $reportMesActual)->first();
            $observacionReporte = $reporte->observacion_rep;
            $observacion = $reporte->observacion;
          }
        

      return view('sigc.programas_sncd')
      ->with('dataDefinitivaA' , $dataDefinitivaA)
      ->with('dataDefinitivaB' , $dataDefinitivaB)
      ->with('dataDefinitivaC' , $dataDefinitivaC)
      ->with('dataDefinitivaD' , $dataDefinitivaD)
      ->with('dataDefinitivaE' , $dataDefinitivaE)
      ->with('dataDefinitivaE2' , $dataDefinitivaE2)
      ->with('dataDefinitivaE3' , $dataDefinitivaE3)
      ->with('dataDefinitivaF' , $dataDefinitivaF)
      ->with('resultPormes' , $resultPormes)
      ->with('observacion' , $observacion)
      ->with('observacionReporte' , $observacionReporte);
    
  }

  public function reporteSNCD(Request $request) {

    $tipoReporte = $request->imprimir;

    $dataDefinitivaA = Programas::where('ano' , (date("Y")))->where('id' , 6)->orderby('id' , 'asc')->get();
    $dataDefinitivaF = ProgramasMesesReportes::where('ano' , (date("Y")))->orderby('id' , 'asc')->get();

    $dataDefinitivaB = ProgramasComponentes::where('ano' , (date("Y")))->where('id_programa' , 6)->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF as $keymesB => $valuemesB) {
          foreach ($dataDefinitivaB as $keyB => $valorB) {
            $resultPormes = ProgramasReportes::where('id_programa' , 6)->where('id_mes_reporte', $valuemesB->id)->where('id_componente', $valorB->id)->first();
              $valorB[str_replace(' ' ,'',$valuemesB->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
          }
      }

      $dataDefinitivaC = ProgramasIndicadores::where('ano' , (date("Y")))->where('id_programa' , 6)->orderby('id_proposito' , 'desc')->get();
      foreach ($dataDefinitivaF as $keymesC => $valuemesC) {
        foreach ($dataDefinitivaC as $keyC => $valorC) {
          $resultPormes = ProgramasReportes::where('id_programa' , 6)->where('id_mes_reporte', $valuemesC->id)->where('id_indicador', $valorC->id)->first();
          $valorC[str_replace(' ' ,'',$valuemesC->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
          $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_numerador'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_denominador'] =  $resultPormes ? $resultPormes->denominador : null;
        }
      }

      $dataDefinitivaD = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 6)->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF as $keymesF => $valuemesF) {
        foreach ($dataDefinitivaD as $keyD => $valorD) {
          $resultPormes = ProgramasReportes::where('id_programa' , 6)->where('id_mes_reporte', $valuemesF->id)->where('id_cobertura', $valorD->id)->first();
          $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_numeradorp'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_resultadop'] =  $resultPormes ? $resultPormes->resultado : null;
            
        }
      }
          $obj = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 6)->where('cod_interno' , 'obj')->first();
          $bnf = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 6)->where('cod_interno' , 'bnf')->first();
        foreach ($dataDefinitivaD as $keyco => $valueco) {
          if ($valueco->cod_interno =='t'){
            for ($i=1; $i < 4; $i++) { 
            $resultado = 0;
            $formula = $valueco->f_t;
            $d_obj = $obj['efectivo_'.$i];
            $d_bnf = $bnf['efectivo_'.$i];
            eval('$resultado =' .$formula.';');
            $valueco['efectivo_'.$i] = round($resultado, 2);
            }
          }
        }

      $dataDefinitivaE3 = ProgramasPresupuesto::whereIn('fecha',[(date("Y")-1),(date("Y")-2)])->where('ano' , (date("Y")))->where('id_programa' , 6)->orderby('id' , 'asc')->get();
      $dataDefinitivaE2 = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , 6)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->first();
      $dataDefinitivaE = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , 6)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->get();
          foreach ($dataDefinitivaF as $keymesG => $valuemesG) {
            foreach ($dataDefinitivaE as $keyE => $valorE) {
              $resultPormes = ProgramasReportes::where('id_programa' , 6)->where('id_mes_reporte', $valuemesG->id)->where('id_presupuesto', $valorE->id)->first();
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_vigente'] =  $resultPormes ? $resultPormes->numerador : null;
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecutados'] =  $resultPormes ? $resultPormes->denominador : null;
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecucion'] =  $resultPormes ? $resultPormes->resultado : null; 
            }
          }
    
      $observacionReporte = '';
      $observacion = '';
      $reportMesActual = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , 6)->max('id_mes_reporte');
      if ($reportMesActual) {
        $reporte = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , 6)->where('id_mes_reporte', $reportMesActual)->first();
        $observacionReporte = $reporte->observacion_rep;
        $observacion = $reporte->observacion;
      }
    
          switch ($tipoReporte) {
            case 'PDF':
    
              $data = [
                'dataDefinitivaA' => $dataDefinitivaA,
                'dataDefinitivaB' => $dataDefinitivaB,
                'dataDefinitivaC' => $dataDefinitivaC,
                'dataDefinitivaD' => $dataDefinitivaD,
                'dataDefinitivaE' => $dataDefinitivaE,
                'dataDefinitivaE2' => $dataDefinitivaE2,
                'dataDefinitivaE3' => $dataDefinitivaE3,
                'dataDefinitivaF' => $dataDefinitivaF,
                'resultPormes' => $resultPormes,
                'observacion' => $observacion,
                'observacionReporte' => $observacionReporte,
          ];

          $pdf = PDF::loadView('sigc.programas_reporte_sncd', $data)
          ->setPaper('legal', 'portrait');
          return $pdf->download('PROGRAMA_SNCD.pdf'); //construcción PDF

        break;
        case 'XLS':
          Excel::create("reporteSNCD", function ($excel) use ($dataDefinitivaA , $dataDefinitivaB, $dataDefinitivaC, $dataDefinitivaD, 
                                                            $dataDefinitivaE , $dataDefinitivaE2, $dataDefinitivaE3, $dataDefinitivaF,
                                                            $resultPormes, $observacion, $observacionReporte) {
          $excel->setTitle("reporteSNCD");
          $excel->sheet("reporteSNCD", function ($sheet) use ($dataDefinitivaA , $dataDefinitivaB, $dataDefinitivaC, $dataDefinitivaD, 
                                                            $dataDefinitivaE , $dataDefinitivaE2, $dataDefinitivaE3, $dataDefinitivaF,
                                                            $resultPormes, $observacion, $observacionReporte) {
          $sheet->loadView('sigc.programas_reporte_sncd_ex_excel')->with('dataDefinitivaA', $dataDefinitivaA)->with('dataDefinitivaB', $dataDefinitivaB)
                                                                  ->with('dataDefinitivaC', $dataDefinitivaC)->with('dataDefinitivaD', $dataDefinitivaD)
                                                                  ->with('dataDefinitivaE', $dataDefinitivaE)->with('dataDefinitivaE2', $dataDefinitivaE2)
                                                                  ->with('dataDefinitivaE3', $dataDefinitivaE3)->with('dataDefinitivaF', $dataDefinitivaF)
                                                                  ->with('resultPormes', $resultPormes)->with('observacion', $observacion)->with('observacionReporte', $observacionReporte);
          });
        })->download('xlsx');

        return back();
        break;
        default:
        break;
        }

  }


  public function indexSCCD(Request $request)
  {

    $dataDefinitivaA = Programas::where('ano' , (date("Y")))->where('id' , 7)->orderby('id' , 'asc')->get();
    $dataDefinitivaF = ProgramasMesesReportes::where('ano' , (date("Y")))->orderby('id' , 'asc')->get();

    $dataDefinitivaB = ProgramasComponentes::where('ano' , (date("Y")))->where('id_programa' , 7)->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF as $keymesB => $valuemesB) {
          foreach ($dataDefinitivaB as $keyB => $valorB) {
            $resultPormes = ProgramasReportes::where('id_programa' , 7)->where('id_mes_reporte', $valuemesB->id)->where('id_componente', $valorB->id)->first();
              $valorB[str_replace(' ' ,'',$valuemesB->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
          }
      }

      $dataDefinitivaC = ProgramasIndicadores::where('ano' , (date("Y")))->where('id_programa' , 7)->orderby('id_proposito' , 'desc')->get();
      foreach ($dataDefinitivaF as $keymesC => $valuemesC) {
        foreach ($dataDefinitivaC as $keyC => $valorC) {
          $resultPormes = ProgramasReportes::where('id_programa' , 7)->where('id_mes_reporte', $valuemesC->id)->where('id_indicador', $valorC->id)->first();
          $valorC[str_replace(' ' ,'',$valuemesC->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
          $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_numerador'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_denominador'] =  $resultPormes ? $resultPormes->denominador : null;
        }
      }

      $dataDefinitivaD = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 7)->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF as $keymesF => $valuemesF) {
        foreach ($dataDefinitivaD as $keyD => $valorD) {
          $resultPormes = ProgramasReportes::where('id_programa' , 7)->where('id_mes_reporte', $valuemesF->id)->where('id_cobertura', $valorD->id)->first();
          $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_numeradorp'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_resultadop'] =  $resultPormes ? $resultPormes->resultado : null;
            
        }
      }
          $obj = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 7)->where('cod_interno' , 'obj')->first();
          $bnf = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 7)->where('cod_interno' , 'bnf')->first();
        foreach ($dataDefinitivaD as $keyco => $valueco) {
          if ($valueco->cod_interno =='t'){
            for ($i=1; $i < 4; $i++) { 
            $resultado = 0;
            $formula = $valueco->f_t;
            $d_obj = $obj['efectivo_'.$i];
            $d_bnf = $bnf['efectivo_'.$i];
            eval('$resultado =' .$formula.';');
            $valueco['efectivo_'.$i] = round($resultado, 2);
            }
          }
        }

      $dataDefinitivaE3 = ProgramasPresupuesto::whereIn('fecha',[(date("Y")-1),(date("Y")-2)])->where('ano' , (date("Y")))->where('id_programa' , 7)->orderby('id' , 'asc')->get();
      $dataDefinitivaE2 = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , 7)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->first();
      $dataDefinitivaE = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , 7)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->get();
          foreach ($dataDefinitivaF as $keymesG => $valuemesG) {
            foreach ($dataDefinitivaE as $keyE => $valorE) {
              $resultPormes = ProgramasReportes::where('id_programa' , 7)->where('id_mes_reporte', $valuemesG->id)->where('id_presupuesto', $valorE->id)->first();
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_vigente'] =  $resultPormes ? $resultPormes->numerador : null;
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecutados'] =  $resultPormes ? $resultPormes->denominador : null;
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecucion'] =  $resultPormes ? $resultPormes->resultado : null; 
            }
          }
    
          $observacionReporte = '';
          $observacion = '';
          $reportMesActual = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , 7)->max('id_mes_reporte');
          if ($reportMesActual) {
            $reporte = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , 7)->where('id_mes_reporte', $reportMesActual)->first();
            $observacionReporte = $reporte->observacion_rep;
            $observacion = $reporte->observacion;
          }
        

      return view('sigc.programas_sccd')
      ->with('dataDefinitivaA' , $dataDefinitivaA)
      ->with('dataDefinitivaB' , $dataDefinitivaB)
      ->with('dataDefinitivaC' , $dataDefinitivaC)
      ->with('dataDefinitivaD' , $dataDefinitivaD)
      ->with('dataDefinitivaE' , $dataDefinitivaE)
      ->with('dataDefinitivaE2' , $dataDefinitivaE2)
      ->with('dataDefinitivaE3' , $dataDefinitivaE3)
      ->with('dataDefinitivaF' , $dataDefinitivaF)
      ->with('resultPormes' , $resultPormes)
      ->with('observacion' , $observacion)
      ->with('observacionReporte' , $observacionReporte);
    
  }

  public function reporteSCCD(Request $request) {

    $tipoReporte = $request->imprimir;

    $dataDefinitivaA = Programas::where('ano' , (date("Y")))->where('id' , 7)->orderby('id' , 'asc')->get();
    $dataDefinitivaF = ProgramasMesesReportes::where('ano' , (date("Y")))->orderby('id' , 'asc')->get();

    $dataDefinitivaB = ProgramasComponentes::where('ano' , (date("Y")))->where('id_programa' , 7)->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF as $keymesB => $valuemesB) {
          foreach ($dataDefinitivaB as $keyB => $valorB) {
            $resultPormes = ProgramasReportes::where('id_programa' , 7)->where('id_mes_reporte', $valuemesB->id)->where('id_componente', $valorB->id)->first();
              $valorB[str_replace(' ' ,'',$valuemesB->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
          }
      }

      $dataDefinitivaC = ProgramasIndicadores::where('ano' , (date("Y")))->where('id_programa' , 7)->orderby('id_proposito' , 'desc')->get();
      foreach ($dataDefinitivaF as $keymesC => $valuemesC) {
        foreach ($dataDefinitivaC as $keyC => $valorC) {
          $resultPormes = ProgramasReportes::where('id_programa' , 7)->where('id_mes_reporte', $valuemesC->id)->where('id_indicador', $valorC->id)->first();
          $valorC[str_replace(' ' ,'',$valuemesC->nombre)] =  $resultPormes ? $resultPormes->resultado : null;
          $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_numerador'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorC[str_replace(' ' ,'',$valuemesC->nombre).'_denominador'] =  $resultPormes ? $resultPormes->denominador : null;
        }
      }

      $dataDefinitivaD = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 7)->orderby('id' , 'asc')->get();
      foreach ($dataDefinitivaF as $keymesF => $valuemesF) {
        foreach ($dataDefinitivaD as $keyD => $valorD) {
          $resultPormes = ProgramasReportes::where('id_programa' , 7)->where('id_mes_reporte', $valuemesF->id)->where('id_cobertura', $valorD->id)->first();
          $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_numeradorp'] =  $resultPormes ? $resultPormes->numerador : null;
          $valorD[str_replace(' ' ,'',$valuemesF->nombre).'_resultadop'] =  $resultPormes ? $resultPormes->resultado : null;
            
        }
      }
          $obj = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 7)->where('cod_interno' , 'obj')->first();
          $bnf = ProgramasCobertura::where('ano' , (date("Y")))->where('id_programa' , 7)->where('cod_interno' , 'bnf')->first();
        foreach ($dataDefinitivaD as $keyco => $valueco) {
          if ($valueco->cod_interno =='t'){
            for ($i=1; $i < 4; $i++) { 
            $resultado = 0;
            $formula = $valueco->f_t;
            $d_obj = $obj['efectivo_'.$i];
            $d_bnf = $bnf['efectivo_'.$i];
            eval('$resultado =' .$formula.';');
            $valueco['efectivo_'.$i] = round($resultado, 2);
            }
          }
        }

      $dataDefinitivaE3 = ProgramasPresupuesto::whereIn('fecha',[(date("Y")-1),(date("Y")-2)])->where('ano' , (date("Y")))->where('id_programa' , 7)->orderby('id' , 'asc')->get();
      $dataDefinitivaE2 = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , 7)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->first();
      $dataDefinitivaE = ProgramasPresupuesto::where('ano' , (date("Y")))->where('id_programa' , 7)->where('fecha' , (date("Y")))->orderby('id' , 'asc')->get();
          foreach ($dataDefinitivaF as $keymesG => $valuemesG) {
            foreach ($dataDefinitivaE as $keyE => $valorE) {
              $resultPormes = ProgramasReportes::where('id_programa' , 7)->where('id_mes_reporte', $valuemesG->id)->where('id_presupuesto', $valorE->id)->first();
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_vigente'] =  $resultPormes ? $resultPormes->numerador : null;
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecutados'] =  $resultPormes ? $resultPormes->denominador : null;
              $valorE[str_replace(' ' ,'',$valuemesG->nombre).'_ejecucion'] =  $resultPormes ? $resultPormes->resultado : null; 
            }
          }
    
      $observacionReporte = '';
      $observacion = '';
      $reportMesActual = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , 7)->max('id_mes_reporte');
      if ($reportMesActual) {
        $reporte = ProgramasReportes::where('ano' , (date("Y")))->where('id_programa' , 7)->where('id_mes_reporte', $reportMesActual)->first();
        $observacionReporte = $reporte->observacion_rep;
        $observacion = $reporte->observacion;
      }
    
          switch ($tipoReporte) {
            case 'PDF':
    
              $data = [
                'dataDefinitivaA' => $dataDefinitivaA,
                'dataDefinitivaB' => $dataDefinitivaB,
                'dataDefinitivaC' => $dataDefinitivaC,
                'dataDefinitivaD' => $dataDefinitivaD,
                'dataDefinitivaE' => $dataDefinitivaE,
                'dataDefinitivaE2' => $dataDefinitivaE2,
                'dataDefinitivaE3' => $dataDefinitivaE3,
                'dataDefinitivaF' => $dataDefinitivaF,
                'resultPormes' => $resultPormes,
                'observacion' => $observacion,
                'observacionReporte' => $observacionReporte,
          ];

          $pdf = PDF::loadView('sigc.programas_reporte_sccd', $data)
          ->setPaper('legal', 'portrait');
          return $pdf->download('PROGRAMA_SCCD.pdf'); //construcción PDF

        break;
        case 'XLS':
          Excel::create("reporteSCCD", function ($excel) use ($dataDefinitivaA , $dataDefinitivaB, $dataDefinitivaC, $dataDefinitivaD, 
                                                            $dataDefinitivaE , $dataDefinitivaE2, $dataDefinitivaE3, $dataDefinitivaF,
                                                            $resultPormes, $observacion, $observacionReporte) {
          $excel->setTitle("reporteSCCD");
          $excel->sheet("reporteSCCD", function ($sheet) use ($dataDefinitivaA , $dataDefinitivaB, $dataDefinitivaC, $dataDefinitivaD, 
                                                            $dataDefinitivaE , $dataDefinitivaE2, $dataDefinitivaE3, $dataDefinitivaF,
                                                            $resultPormes, $observacion, $observacionReporte) {
          $sheet->loadView('sigc.programas_reporte_sccd_ex_excel')->with('dataDefinitivaA', $dataDefinitivaA)->with('dataDefinitivaB', $dataDefinitivaB)
                                                                  ->with('dataDefinitivaC', $dataDefinitivaC)->with('dataDefinitivaD', $dataDefinitivaD)
                                                                  ->with('dataDefinitivaE', $dataDefinitivaE)->with('dataDefinitivaE2', $dataDefinitivaE2)
                                                                  ->with('dataDefinitivaE3', $dataDefinitivaE3)->with('dataDefinitivaF', $dataDefinitivaF)
                                                                  ->with('resultPormes', $resultPormes)->with('observacion', $observacion)->with('observacionReporte', $observacionReporte);
          });
        })->download('xlsx');

        return back();
        break;
        default:
        break;
        }

  }

}
