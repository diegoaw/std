<?php

namespace App\Http\Controllers\Sigc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Sigc\Indicadores;
use App\Models\Sigc\Mediciones;
use App\Models\Sigc\Archivos;
use App\Http\Requests\SIGC\MedicionesRequest;
use Illuminate\Support\Facades\Auth;
use App\Models\Sigc\Users as UsersSigc;
use App\Models\Sigc\EquipoMiembros;
use App\Models\Sigc\MedicionArchivos;
use App\Models\Sigc\Calendario;
use App\Models\Sigc\Notificaciones;
use App\Models\Sigc\CorreoNotificaciones;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Models\Sigc\MedicionEstados;
use App\Models\Sigc\MedicionEventos;
use App\Models\Sigc\MedicionHistoricos;
use App\Mail\NotificacionNuevoReporteEmail;
use App\Mail\NotificacionCorreccionReporteEmail;
use App\Mail\NotificacionRevisionEmail;
use App\Mail\NotificacionRevisionCorreccionEmail;
use App\Mail\TestEmail;
use Mail;
use App\Models\Sigc\Users;
use App\Models\Sigc\Condiciones_formulas_indicadores;




class ReportesController extends Controller
{
    public function indicators(Request $request)
          {
            $usuarioEmailStd = Auth::user();
            $MedicionEstadoidObservada = MedicionEstados::where('nombre' , 'Observada')->first()->id;
            $MedicionEstadoidBorrador = MedicionEstados::where('nombre' , 'Borrador')->first()->id;
            $idMedicionEstadoEnProceso = MedicionEstados::where('nombre' , 'En Proceso')->first()->id;
            $idMedicionEstadoEnProceso = MedicionEstados::where('nombre' , 'En Revision')->first()->id;


            $usuarioSigc = UsersSigc::where('email' , $usuarioEmailStd->email)->first();
            $habititarReportar = false;
            $habititarCorreccion = false;
            if($usuarioSigc) {
              $equipos = EquipoMiembros::select('equipo_id')->where('user_id' , $usuarioSigc->id)->get();
              $equiposArray = $equipos->toArray();
              $indicadores = Indicadores::where('reportador_id' , $usuarioSigc->id)->orWhere('reportadores_id',  $usuarioSigc->id)->where('habilitar_suplencia' , 1)->get();
            }else{
              $indicadores = [];
            }
            if(count($indicadores)>0){
              $mesActual  =  date('n');
              $anoActual = date("Y");
              if($mesActual >= 2 ){
                $mesConsulta = $mesActual -1 ;
              }else{
                $mesConsulta = 12;
              }

              if ($mesConsulta == 12 ){
                 $anoConsulta = $anoActual - 1;
              }else{
                $anoConsulta = $anoActual;
              }
              /*provicional provicional solo mes 1 hasta ero enero comentar linea 72 y 73*/
              //$mesConsulta =12;
              //$anoActual = 2022;
                  
              $indicadores = Indicadores::where('reportador_id' , $usuarioSigc->id)->where('ano' , $anoConsulta)->orWhere('reportadores_id',  $usuarioSigc->id)->where('habilitar_suplencia' , 1)->get();
              $idCalendario =  Calendario::where('ano' , $anoConsulta)->where('mes' , $mesConsulta)->first()->id;
              //dd($indicadores);
                
         
              $calendario =  Calendario::where('ano' , $anoConsulta)->where('mes' , $mesConsulta)->first();
              if($calendario){
                $habititarReportar = false;
                $habititarCorreccion = false;

                $ahora = Carbon::now()->format('Y-m-d');

                if(Date('Y-m-d',strtotime($ahora))>= Date('Y-m-d',strtotime($calendario->rep_desde)) &&  Date('Y-m-d',strtotime($ahora)) <= Date('Y-m-d',strtotime($calendario->rep_hasta))){
                  $habititarReportar = true;
                }
                if(Date('Y-m-d',strtotime($ahora))>= Date('Y-m-d',strtotime($calendario->corrige_desde)) &&  Date('Y-m-d',strtotime($ahora)) <= Date('Y-m-d',strtotime($calendario->corrige_hasta))){
                  $habititarCorreccion = true;
                  $habititarReportar = true;
                }


                foreach ($indicadores as $key => $value) {
                  $existMEdition = Mediciones::where('calendario_id' ,$idCalendario)->where('indicador_id' , $value->id)->first();
                  $existMEdition ? $value->reportar = false : $value->reportar = true;
                  $existMEditionCorrecion = Mediciones::where('calendario_id' ,$idCalendario)->where('indicador_id' , $value->id)->where('medicion_estado_id',$MedicionEstadoidObservada)->first();
                  $existMEditionCorrecion ? $value->corregir = true : $value->corregir = false;
                  $existMEditionCorrecion ? $value->id_medicion_correccion =  $existMEditionCorrecion->id : $value->id_medicion_correccion = null;
                  $existMedicionBorrador = Mediciones::where('calendario_id' ,$idCalendario)->where('indicador_id' , $value->id)->where('medicion_estado_id',$MedicionEstadoidBorrador)->first();
                  $existMedicionBorrador ? $value->editar = true : $value->editar = false;
                  $existMedicionBorrador ? $value->id_medicion_editar =  $existMedicionBorrador->id : $value->id_medicion_editar = null;
                  $existMedicionEstado = Mediciones::where('calendario_id' ,$idCalendario)->where('indicador_id' , $value->id)->whereIn('medicion_estado_id',[1,3,5,6])->first();
                  $existMedicionEstado ? $value->habilitar_estado = true : $value->habilitar_estado = false;
                  $existMedicionEstado ? $value->id_medicion_estado = $existMedicionEstado->id : $value->id_medicion_estado = null;
                }
              }
            }
//dd($habititarReportar, $indicadores);
            return view('sigc.sigc_indicadores_usuario')
            ->with('habititarReportar' , $habititarReportar)
            ->with('habititarCorreccion' , $habititarCorreccion)
            ->with('dataDefinitiva' , $indicadores);
          }


    public function mediciones(Request $request)
            {
              $mesActual  =  date('n');
              $anoActual = date("Y");
              $habititarRevisar = false;
              $habititarRevisarCorrecion = false;

              $usuarioEmailStd = Auth::user();
              $usuarioSigc = UsersSigc::where('email' , $usuarioEmailStd->email)->first();
              if($usuarioSigc) {
                $equipos = EquipoMiembros::select('equipo_id')->where('user_id' , $usuarioSigc->id)->get();
                $equiposArray = $equipos->toArray();
                $indicadores = Indicadores::select('id')->where('revisor_id' , $usuarioSigc->id)->orWhere('responsable_id',  $usuarioSigc->id)->get();
                $indicadoresArray = $indicadores->toArray();
              }else{
                $dataDefinitiva = [];
              }


              if($mesActual >= 2 ){
                $mesConsulta = $mesActual -1 ;
              }else{
                $mesConsulta = 12;
              }

              if ($mesConsulta == 12){
                 $anoConsulta = $anoActual - 1;
              }else{
                $anoConsulta = $anoActual;
              }
              $idCalendario =  Calendario::where('ano' , $anoConsulta)->where('mes' , $mesConsulta)->first()->id;
              $calendario =  Calendario::where('ano' , $anoConsulta)->where('mes' , $mesConsulta)->first();

              if($calendario){
                $habititarReportar = false;
                $habititarCorreccion = false;

                $ahora = Carbon::now()->format('Y-m-d');

                if(Date('Y-m-d',strtotime($ahora))>= Date('Y-m-d',strtotime($calendario->rev_desde)) &&  Date('Y-m-d',strtotime($ahora)) <= Date('Y-m-d',strtotime($calendario->rev_hasta))){
                  $habititarRevisar = true;
                }
                if(Date('Y-m-d',strtotime($ahora))>= Date('Y-m-d',strtotime($calendario->rev_correccion_desde)) &&  Date('Y-m-d',strtotime($ahora)) <= Date('Y-m-d',strtotime($calendario->rev_correccion_hasta))){
                  $habititarRevisarCorrecion = true;
                  $habititarRevisar = true;
                }
              }
              if($usuarioSigc){
                $indicadores = Indicadores::select('id')->where('revisor_id' , $usuarioSigc->id)->orWhere('responsable_id',  $usuarioSigc->id)->where('ano' , $anoConsulta)->get();
              }else{
                $indicadores = collect([]);
              }
              $indicadoresArray = $indicadores->toArray();
              $MedicionEstadoid = MedicionEstados::select('id')->whereIn('nombre' , ['En Proceso','En Revisión'] )->get();
              $MedicionEstadoid = $MedicionEstadoid->toArray();

              $dataDefinitiva = [];
              $tipoView = $request->tipo;
              $equipoView = $request->equipo;
              $mesView = $request->mes;
              $indicadorView = $request->indicador;
              $anoView = $request->ano;

              $dataDefinitiva = Mediciones::whereIn('indicador_id' , $indicadoresArray)
              ->whereIn('medicion_estado_id' ,  $MedicionEstadoid)
              ->whereHas('Indicador', function ($q) use ($anoConsulta)  { $q->where('ano' , $anoConsulta);})
              ->get();



              return view('sigc.sigc_mediciones_revision')
              ->with('dataDefinitiva' , $dataDefinitiva)
              ->with('habititarRevisar' , $habititarRevisar)
              ->with('habititarRevisarCorrecion' , $habititarRevisarCorrecion);
            }


    public function report($id)
          {
          $selectIndicador[null] = "SELECCIONE INDICADOR"; foreach (Indicadores::orderby('id', 'desc')->get() as $llave => $valor) {  $selectIndicador[$valor->id] = $valor->Nombrecompleto;}
          $indicadorView = $id;
          $indicador  = Indicadores::where('id' , $id)->first();
            return view('sigc.sigc_reportar')
            ->with('selectIndicador' , $selectIndicador)
            ->with('indicador' ,$indicador)
            ->with('indicadorView' , $indicadorView);
          }


    public function reported(MedicionesRequest $request , $idIndicador)
         {

           $valorNumerador = $request->valorNumerador;
           $analisis = $request->analisis;
           $acciones = $request->acciones;
           $denominador = $request->denominador;
           $resultado = $request->resultado;

           $mesActual  =  date('n');
           $anoActual = date("Y");
           if($mesActual >= 2 ){
             $mesConsulta = $mesActual -1 ;
           }else{
             $mesConsulta = 12;
           }

           if ($mesConsulta == 12 ){
              $anoConsulta = $anoActual - 1;
           }else{
             $anoConsulta = $anoActual;
           }
           /* provicional provicional solo mes 1 hasta ero enero*/
           //$mesConsulta =12;
           //$anoActual = 2022;
               
           $idCalendario =  Calendario::where('ano' , $anoConsulta)->where('mes' , $mesConsulta)->first()->id;
           $idMedicionEstado = MedicionEstados::where('nombre' , 'Borrador')->first()->id;
           $indicador= Indicadores::where('id' , $idIndicador)->first();
           $nuevaMedicion = new Mediciones();
           $nuevaMedicion->calendario_id =  $idCalendario;
           $nuevaMedicion->indicador_id =  $idIndicador;
           $nuevaMedicion->resultado =  $resultado;
           $nuevaMedicion->fecha = Carbon::now();
           $nuevaMedicion->valor_numerador = $valorNumerador;
           $nuevaMedicion->analisis = $analisis;
           $nuevaMedicion->acciones = $acciones;

           $nuevaMedicion->medicion_estado_id =  $idMedicionEstado;
           if($indicador->categoria_id == 2){
            $nuevaMedicion->denominador = $denominador;
           }
           $nuevaMedicion->save();

           $documentos = $request->filename;
           if($documentos){
              foreach ($documentos as $key => $value) {
                    $nuevoArchivo = new MedicionArchivos();
                    $nuevoArchivo->medicion_id = $nuevaMedicion->id;
                    $nuevoArchivo->filename = '"'.$value->getClientOriginalName().'"';
                    $nuevoArchivo->visible = 1;
                    $nuevoArchivo->save();

                    $extencion =  $value->getClientOriginalExtension();
                    $docNombre = sha1(Auth::user()->id).'.'.$extencion;
                    $upload = Storage::disk('s3')->put('medicion_archivo/'.$nuevoArchivo->id, $value, 'public');
                    $nuevoArchivo->filename_aws = 'https://sigc.s3.amazonaws.com/'.$upload;
                    $nuevoArchivo->save();
              }
          }

          $idMedicion = $nuevaMedicion->id;
          $idMedicionEvento = MedicionEventos::where('nombre' , 'Creada')->first()->id;
          $email = Auth::user()->email;

          /* CALCULO DE FORMULAS */

          /* datos */
          /*
          $numerador = $valorNumerador;
          $denominador = $denominador;
          $casoEspecial01 = 22.32;
          $casoEspecial02  = 12;
          $meta = $indicador->meta;
          $ponderadorOriginal = $indicador->ponderador;
          $ponderador = ($indicador->ponderador/100);
          $ponderador_d = $ponderador;
          $porcentaje = 100;
          $multiplicador = $indicador->multiplicador;
          $pasarCalculo = true;
          $pasarCalculoMeta = true;
          $pasarCalculoCumplimiento = true;
          $c1 = 51;
          $sinformula = 0;
          $medicionAprobada = true;
          if($indicador->categoria_id == 1){
            $denominador = $indicador->denominador;
            }else{
                $denominador = $denominador;
            }

            $validacionesIndicador3 = Condiciones_formulas_indicadores::where('tipo_formula' , 'formula_cumplimiento')->where('id_indicador' , $indicador->id)->where('estatus', 1)->get();
            if(count($validacionesIndicador3)>0){
                foreach($validacionesIndicador3 as $indiceValidacion3 => $valorValidacion3 ){
                    eval('$resultadoTempVal3 =' .$valorValidacion3->validacion.';');
                    if(!$resultadoTempVal3){
                        $pasarCalculoCumplimiento = false;
                        break;
                    }
                }
            }



          /*calculo de la formula de resultado*/
         /* if($value0->formula_cumplimiento && $pasarCalculoCumplimiento){
            $formulaCumplimiento = $indicador->formula_cumplimiento;
          }else{
            $formulaCumplimiento = 0;
          }
          eval('$resultado =' .$formulaCumplimiento.';');
          $nuevaMedicion->resultado =  $resultado;
          /*calculo de la formula de meta*/
      /*    $validacionesIndicador2 = Condiciones_formulas_indicadores::where('tipo_formula' , 'formula_meta')->where('id_indicador' , $indicador->id)->where('estatus', 1)->get();
          if(count($validacionesIndicador2)>0){
              foreach($validacionesIndicador2 as $indiceValidacion2 => $valorValidacion2 ){
                  eval('$resultadoTempVal2 =' .$valorValidacion2->validacion.';');
                  if(!$resultadoTempVal2){
                      $pasarCalculoMeta = false;
                      break;
                  }
              }
          }
          if($indicador->formula_calculo_meta && $pasarCalculoMeta){
            $formulaMeta = $indicador->formula_calculo_meta;
          }else{
            $formulaMeta = 0;
          }
           eval('$resultadoM =' .$formulaMeta.';');
           $nuevaMedicion->resultado_formula_meta =  $resultadoM;

          /*calculo de la formula de ponderacion*/
      /*    $validacionesIndicador = Condiciones_formulas_indicadores::where('tipo_formula' , 'formula_ponderador')->where('id_indicador' , $indicador->id)->where('estatus', 1)->get();
          if(count($validacionesIndicador)>0){
              foreach($validacionesIndicador as $indiceValidacion => $valorValidacion ){
                  eval('$resultadoTempVal =' .$valorValidacion->validacion.';');
                  if(!$resultadoTempVal){
                      $pasarCalculo = false;
                      break;
                  }
              }
          }
          if($indicador->formula_ponderador  && $pasarCalculo){
            $formulaPonderador = $indicador->formula_ponderador;
          }else{
            $formulaPonderador = 0;
          }


           eval('$resultadoP =' .$formulaPonderador.';');
           $nuevaMedicion->resultado_formula_ponderacion =  $resultadoP;
           $nuevaMedicion->save();

          /* fin CALCULO DE FORMULAS */




          $nuevaMedicionHisto = new MedicionHistoricos();
          $nuevaMedicionHisto->medicion_id =  $idMedicion;
          $nuevaMedicionHisto->calendario_id =  $idCalendario;
          $nuevaMedicionHisto->indicador_id =  $idIndicador;
          $nuevaMedicionHisto->fecha = Carbon::now();
          $nuevaMedicionHisto->valor_numerador = $valorNumerador;
          $nuevaMedicionHisto->analisis = $analisis;
          $nuevaMedicionHisto->acciones = $acciones;
          $nuevaMedicionHisto->usuario = $email;

          $nuevaMedicionHisto->medicion_evento_id =  $idMedicionEvento;
          if($indicador->categoria_id == 2){
           $nuevaMedicionHisto->denominador = $denominador;
          }
          $nuevaMedicionHisto->save();


           alert()->success('!','Reporte creado exitosamente');
           return redirect()->route('sigc_indicadores_usuario');
         }
         public function enviarReporte($idMedicion){
            $idMedicionEstado = MedicionEstados::where('nombre' , 'En Proceso')->first()->id;
            $nuevaMedicion = Mediciones::where('id' , $idMedicion)->first();
            $nuevaMedicion->medicion_estado_id =  $idMedicionEstado;
            $nuevaMedicion->save();
            if ($nuevaMedicion){
              $this->notificacionReporte($nuevaMedicion);
            }

            $email = Auth::user()->email;

            $idMedicionEvento = MedicionEventos::where('nombre' , 'Ingresada')->first()->id;
            $nuevaMedicionHisto = new MedicionHistoricos();
            $nuevaMedicionHisto->medicion_id =  $nuevaMedicion->id;
            $nuevaMedicionHisto->calendario_id =  $nuevaMedicion->calendario_id;
            $nuevaMedicionHisto->indicador_id =  $nuevaMedicion->indicador_id;
            $nuevaMedicionHisto->fecha = Carbon::now();
            $nuevaMedicionHisto->valor_numerador = $nuevaMedicion->valor_numerador;
            $nuevaMedicionHisto->analisis = $nuevaMedicion->analisis;
            $nuevaMedicionHisto->acciones = $nuevaMedicion->acciones;
            $nuevaMedicionHisto->usuario = $email;
            $nuevaMedicionHisto->medicion_evento_id =  $idMedicionEvento;
            $nuevaMedicionHisto->denominador = $nuevaMedicion->denominador;
            $nuevaMedicionHisto->save();

            $idMedicionEvento = MedicionEventos::where('nombre' , 'En Revisión')->first()->id;
            $nuevaMedicionHisto2 = new MedicionHistoricos();
            $nuevaMedicionHisto2->medicion_id =  $nuevaMedicion->id;
            $nuevaMedicionHisto2->calendario_id =  $nuevaMedicion->calendario_id;
            $nuevaMedicionHisto2->indicador_id =  $nuevaMedicion->indicador_id;
            $nuevaMedicionHisto2->fecha = Carbon::now();
            $nuevaMedicionHisto2->valor_numerador = $nuevaMedicion->valor_numerador;
            $nuevaMedicionHisto2->analisis = $nuevaMedicion->analisis;
            $nuevaMedicionHisto2->acciones = $nuevaMedicion->acciones;
            $nuevaMedicionHisto2->usuario = $email;
            $nuevaMedicionHisto2->medicion_evento_id =  $idMedicionEvento;
            $nuevaMedicionHisto2->denominador = $nuevaMedicion->denominador;
            $nuevaMedicionHisto2->save();


            alert()->success('!','Reporte Enviado exitosamente exitosamente');
            return redirect()->route('sigc_indicadores_usuario');

         }



         public function notificacionReporte($nuevaMedicion){

           $medicion = Mediciones::where('id' , $nuevaMedicion->id)->first();
           $indicador = Indicadores::where('id', $medicion->indicador_id)->first();
           $usuario = Users::where('id',$indicador->revisor_id)->first();
           $usuarioRevisorSuplente = Users::where('id',$indicador->revisor_id)->first();
           if($usuarioRevisorSuplente){
              $emails = [$usuario->email,Auth::user()->email , $usuarioRevisorSuplente->email];
            }else{
              $emails = [$usuario->email,Auth::user()->email];
            }
           $datos = ['medicion'=>$nuevaMedicion , 'indicador'=>$indicador];

           foreach ($emails as $key => $correo) {
             Mail::to($correo)->send(new NotificacionNuevoReporteEmail($datos));
             $notificacionesId = 4;
             $nuevoCorreo = new CorreoNotificaciones();
             $nuevoCorreo->id_notificaciones = $notificacionesId;
             $nuevoCorreo->destintario = $correo;
             $nuevoCorreo->save();
           }

         }


    public function edit($id)
         {
          $selectEstado[null] = 'SELECCIONE ESTADO'; foreach (MedicionEstados::orderby('id', 'asc')->get() as $key1 => $value1) { $selectEstado[$value1->id]=$value1->nombre;}
          $selectIndicador[null] = "SELECCIONE INDICADOR"; foreach (Indicadores::orderby('id', 'desc')->get() as $llave => $valor) {  $selectIndicador[$valor->id] = $valor->Nombrecompleto;}
          $fechaActual = Carbon::now();
          $editRevision = Mediciones::where('id' , $id)->first();
          $editRevisionHisto = MedicionHistoricos::where('medicion_id' , $editRevision->$id)->first();
          $indicador = Indicadores::where('id', $editRevision->indicador_id)->first();
           return view('sigc.sigc_editar_reporte')
                ->with('editRevision' , $editRevision)
                ->with('editRevisionHisto' , $editRevisionHisto)
                ->with('selectEstado' , $selectEstado)
                ->with('fechaActual' , $fechaActual)
                ->with('selectIndicador' , $selectIndicador)
                ->with('indicador' , $indicador);
         }

         public function editBorrador($id)
         {
          $selectEstado[null] = 'SELECCIONE ESTADO'; foreach (MedicionEstados::orderby('id', 'asc')->get() as $key1 => $value1) { $selectEstado[$value1->id]=$value1->nombre;}
          $selectIndicador[null] = "SELECCIONE INDICADOR"; foreach (Indicadores::orderby('id', 'desc')->get() as $llave => $valor) {  $selectIndicador[$valor->id] = $valor->Nombrecompleto;}
          $fechaActual = Carbon::now();
          $editRevision = Mediciones::where('id' , $id)->first();
          $editRevisionHisto = MedicionHistoricos::where('medicion_id' , $editRevision->$id)->first();
          $indicador = Indicadores::where('id', $editRevision->indicador_id)->first();
           return view('sigc.sigc_editar_borrador_reporte')
                ->with('editRevision' , $editRevision)
                ->with('editRevisionHisto' , $editRevisionHisto)
                ->with('selectEstado' , $selectEstado)
                ->with('fechaActual' , $fechaActual)
                ->with('selectIndicador' , $selectIndicador)
                ->with('indicador' , $indicador);
         }

    public function update(MedicionesRequest $request, $id)
         {
          $valorNumerador = $request->valorNumerador;
          $analisis = $request->analisis;
          $acciones = $request->acciones;
          $denominador = $request->denominador;
          $resultado = $request->resultado;



          $idMedicionEstado = MedicionEstados::where('nombre' , 'En Revisión')->first()->id;
          $nuevaMedicion =Mediciones::where('id' , $id)->first();
          $indicador= Indicadores::where('id' , $nuevaMedicion->indicador_id)->first();
          $nuevaMedicion->fecha = Carbon::now();
          $nuevaMedicion->valor_numerador = $valorNumerador;
          $nuevaMedicion->resultado =  $resultado;
          $nuevaMedicion->analisis = $analisis;
          $nuevaMedicion->acciones = $acciones;
          $nuevaMedicion->medicion_estado_id =  $idMedicionEstado;
          if($indicador->categoria_id == 2){
           $nuevaMedicion->denominador = $denominador;
          }
          $nuevaMedicion->save();

          $documentos = $request->filename;
          if($documentos){
             foreach ($documentos as $key => $value) {
                   $nuevoArchivo = new MedicionArchivos();
                   $nuevoArchivo->medicion_id = $nuevaMedicion->id;
                   $nuevoArchivo->filename = '"'.$value->getClientOriginalName().'"';
                   $nuevoArchivo->visible = 1;
                   $nuevoArchivo->save();

                   $extencion =  $value->getClientOriginalExtension();
                   $docNombre = sha1(Auth::user()->id).'.'.$extencion;
                   $upload = Storage::disk('s3')->put('medicion_archivo/'.$nuevoArchivo->id, $value, 'public');
                   $nuevoArchivo->filename_aws = 'https://sigc.s3.amazonaws.com/'.$upload;
                   $nuevoArchivo->save();
             }
         }


         if ($nuevaMedicion){
           $this->notificacionCorrecionReporte($nuevaMedicion);
         }

         /* CALCULO DE FORMULAS */

         /* datos */
       /*  $numerador = $valorNumerador;
         $denominador = $denominador;
         $casoEspecial01 = 22.32;
         $casoEspecial02  = 12;
         $meta = $indicador->meta;
         $ponderadorOriginal = $indicador->ponderador;
         $ponderador = ($indicador->ponderador/100);
         $ponderador_d = $ponderador;
         $porcentaje = 100;
         $multiplicador = $indicador->multiplicador;
         $pasarCalculo = true;
         $pasarCalculoMeta = true;
         $pasarCalculoCumplimiento = true;
         $c1 = 51;
         $sinformula = 0;
         $medicionAprobada = true;
         if($indicador->categoria_id == 1){
           $denominador = $indicador->denominador;
           }else{
               $denominador = $denominador;
           }

           $validacionesIndicador3 = Condiciones_formulas_indicadores::where('tipo_formula' , 'formula_cumplimiento')->where('id_indicador' , $indicador->id)->where('estatus', 1)->get();
           if(count($validacionesIndicador3)>0){
               foreach($validacionesIndicador3 as $indiceValidacion3 => $valorValidacion3 ){
                   eval('$resultadoTempVal3 =' .$valorValidacion3->validacion.';');
                   if(!$resultadoTempVal3){
                       $pasarCalculoCumplimiento = false;
                       break;
                   }
               }
           }



         /*calculo de la formula de resultado*/
   /*      if($value0->formula_cumplimiento && $pasarCalculoCumplimiento){
           $formulaCumplimiento = $indicador->formula_cumplimiento;
         }else{
           $formulaCumplimiento = 0;
         }
         eval('$resultado =' .$formulaCumplimiento.';');
         $nuevaMedicion->resultado =  $resultado;
         /*calculo de la formula de meta*/
   /*      $validacionesIndicador2 = Condiciones_formulas_indicadores::where('tipo_formula' , 'formula_meta')->where('id_indicador' , $indicador->id)->where('estatus', 1)->get();
         if(count($validacionesIndicador2)>0){
             foreach($validacionesIndicador2 as $indiceValidacion2 => $valorValidacion2 ){
                 eval('$resultadoTempVal2 =' .$valorValidacion2->validacion.';');
                 if(!$resultadoTempVal2){
                     $pasarCalculoMeta = false;
                     break;
                 }
             }
         }
         if($indicador->formula_calculo_meta && $pasarCalculoMeta){
           $formulaMeta = $indicador->formula_calculo_meta;
         }else{
           $formulaMeta = 0;
         }
          eval('$resultadoM =' .$formulaMeta.';');
          $nuevaMedicion->resultado_formula_meta =  $resultadoM;

         /*calculo de la formula de ponderacion*/
    /*     $validacionesIndicador = Condiciones_formulas_indicadores::where('tipo_formula' , 'formula_ponderador')->where('id_indicador' , $indicador->id)->where('estatus', 1)->get();
         if(count($validacionesIndicador)>0){
             foreach($validacionesIndicador as $indiceValidacion => $valorValidacion ){
                 eval('$resultadoTempVal =' .$valorValidacion->validacion.';');
                 if(!$resultadoTempVal){
                     $pasarCalculo = false;
                     break;
                 }
             }
         }
         if($indicador->formula_ponderador  && $pasarCalculo){
           $formulaPonderador = $indicador->formula_ponderador;
         }else{
           $formulaPonderador = 0;
         }


          eval('$resultadoP =' .$formulaPonderador.';');
          $nuevaMedicion->resultado_formula_ponderacion =  $resultadoP;
          $nuevaMedicion->save();

         /* fin CALCULO DE FORMULAS */


         $email = Auth::user()->email;
         $idMedicionEvento = MedicionEventos::where('nombre' , 'En Revisión')->first()->id;
         $nuevaMedicionHisto2 = new MedicionHistoricos();
         $nuevaMedicionHisto2->medicion_id =  $nuevaMedicion->id;
         $nuevaMedicionHisto2->calendario_id =  $nuevaMedicion->calendario_id;
         $nuevaMedicionHisto2->indicador_id =  $nuevaMedicion->indicador_id;
         $nuevaMedicionHisto2->fecha = Carbon::now();
         $nuevaMedicionHisto2->valor_numerador = $nuevaMedicion->valor_numerador;
         $nuevaMedicionHisto2->analisis = $nuevaMedicion->analisis;
         $nuevaMedicionHisto2->acciones = $nuevaMedicion->acciones;
         $nuevaMedicionHisto2->usuario = $email;
         $nuevaMedicionHisto2->medicion_evento_id =  $idMedicionEvento;
         if($indicador->categoria_id == 2){
          $nuevaMedicionHisto2->denominador = $denominador;
         }
         $nuevaMedicionHisto2->save();

          alert()->success('!','Reporte Corregido exitosamente');
          return redirect()->route('sigc_indicadores_usuario');
         }

         public function updateBorrador(MedicionesRequest $request, $id)
         {
          $valorNumerador = $request->valorNumerador;
          $analisis = $request->analisis;
          $acciones = $request->acciones;
          $denominador = $request->denominador;
          $resultado = $request->resultado;




          $idMedicionEstado = MedicionEstados::where('nombre' , 'Borrador')->first()->id;
          $nuevaMedicion =Mediciones::where('id' , $id)->first();
          $indicador= Indicadores::where('id' , $nuevaMedicion->indicador_id)->first();
          $nuevaMedicion->fecha = Carbon::now();
          $nuevaMedicion->valor_numerador = $valorNumerador;
          $nuevaMedicion->resultado =  $resultado;
          $nuevaMedicion->analisis = $analisis;
          $nuevaMedicion->acciones = $acciones;
          $nuevaMedicion->medicion_estado_id =  $idMedicionEstado;
          if($indicador->categoria_id == 2){
           $nuevaMedicion->denominador = $denominador;
          }
          $nuevaMedicion->save();

          $documentos = $request->filename;
          if($documentos){
             foreach ($documentos as $key => $value) {
                   $nuevoArchivo = new MedicionArchivos();
                   $nuevoArchivo->medicion_id = $nuevaMedicion->id;
                   $nuevoArchivo->filename = '"'.$value->getClientOriginalName().'"';
                   $nuevoArchivo->visible = 1;
                   $nuevoArchivo->save();

                   $extencion =  $value->getClientOriginalExtension();
                   $docNombre = sha1(Auth::user()->id).'.'.$extencion;
                   $upload = Storage::disk('s3')->put('medicion_archivo/'.$nuevoArchivo->id, $value, 'public');
                   $nuevoArchivo->filename_aws = 'https://sigc.s3.amazonaws.com/'.$upload;
                   $nuevoArchivo->save();
             }
         }

         if ($nuevaMedicion){
           $this->notificacionCorrecionReporte($nuevaMedicion);
         }

         /* CALCULO DE FORMULAS */

         /* datos */
    /*     $numerador = $valorNumerador;
         $denominador = $denominador;
         $casoEspecial01 = 22.32;
         $casoEspecial02  = 12;
         $meta = $indicador->meta;
         $ponderadorOriginal = $indicador->ponderador;
         $ponderador = ($indicador->ponderador/100);
         $ponderador_d = $ponderador;
         $porcentaje = 100;
         $multiplicador = $indicador->multiplicador;
         $pasarCalculo = true;
         $pasarCalculoMeta = true;
         $pasarCalculoCumplimiento = true;
         $c1 = 51;
         $sinformula = 0;
         $medicionAprobada = true;
         if($indicador->categoria_id == 1){
           $denominador = $indicador->denominador;
           }else{
               $denominador = $denominador;
           }

           $validacionesIndicador3 = Condiciones_formulas_indicadores::where('tipo_formula' , 'formula_cumplimiento')->where('id_indicador' , $indicador->id)->where('estatus', 1)->get();
           if(count($validacionesIndicador3)>0){
               foreach($validacionesIndicador3 as $indiceValidacion3 => $valorValidacion3 ){
                   eval('$resultadoTempVal3 =' .$valorValidacion3->validacion.';');
                   if(!$resultadoTempVal3){
                       $pasarCalculoCumplimiento = false;
                       break;
                   }
               }
           }



           /*calculo de la formula de resultado*/
    /*       if($value0->formula_cumplimiento && $pasarCalculoCumplimiento){
           $formulaCumplimiento = $indicador->formula_cumplimiento;
         }else{
           $formulaCumplimiento = 0;
         }
         eval('$resultado =' .$formulaCumplimiento.';');
         $nuevaMedicion->resultado =  $resultado;
         /*calculo de la formula de meta*/
    /*     $validacionesIndicador2 = Condiciones_formulas_indicadores::where('tipo_formula' , 'formula_meta')->where('id_indicador' , $indicador->id)->where('estatus', 1)->get();
         if(count($validacionesIndicador2)>0){
             foreach($validacionesIndicador2 as $indiceValidacion2 => $valorValidacion2 ){
                 eval('$resultadoTempVal2 =' .$valorValidacion2->validacion.';');
                 if(!$resultadoTempVal2){
                     $pasarCalculoMeta = false;
                     break;
                 }
             }
         }
         if($indicador->formula_calculo_meta && $pasarCalculoMeta){
           $formulaMeta = $indicador->formula_calculo_meta;
         }else{
           $formulaMeta = 0;
         }
          eval('$resultadoM =' .$formulaMeta.';');
          $nuevaMedicion->resultado_formula_meta =  $resultadoM;

         /*calculo de la formula de ponderacion*/
     /*    $validacionesIndicador = Condiciones_formulas_indicadores::where('tipo_formula' , 'formula_ponderador')->where('id_indicador' , $indicador->id)->where('estatus', 1)->get();
         if(count($validacionesIndicador)>0){
             foreach($validacionesIndicador as $indiceValidacion => $valorValidacion ){
                 eval('$resultadoTempVal =' .$valorValidacion->validacion.';');
                 if(!$resultadoTempVal){
                     $pasarCalculo = false;
                     break;
                 }
             }
         }
         if($indicador->formula_ponderador  && $pasarCalculo){
           $formulaPonderador = $indicador->formula_ponderador;
         }else{
           $formulaPonderador = 0;
         }


          eval('$resultadoP =' .$formulaPonderador.';');
          $nuevaMedicion->resultado_formula_ponderacion =  $resultadoP;
          $nuevaMedicion->save();

         /* fin CALCULO DE FORMULAS */


         $email = Auth::user()->email;
         $idMedicionEvento = MedicionEventos::where('nombre' , 'Creada')->first()->id;
         $nuevaMedicionHisto = new MedicionHistoricos();
         $nuevaMedicionHisto->fecha = Carbon::now();
         $nuevaMedicionHisto->valor_numerador = $valorNumerador;
         $nuevaMedicionHisto->analisis = $analisis;
         $nuevaMedicionHisto->acciones = $acciones;
         $nuevaMedicionHisto->usuario = $email;
         $nuevaMedicionHisto->medicion_evento_id =  $idMedicionEvento;
         if($indicador->categoria_id == 2){
          $nuevaMedicionHisto->denominador = $denominador;
         }
         $nuevaMedicionHisto->save();

          alert()->success('!','Reporte Corregido exitosamente');
          return redirect()->route('sigc_indicadores_usuario');
         }




         public function notificacionCorrecionReporte($nuevaMedicion){

           $medicion = Mediciones::where('id' , $nuevaMedicion->id)->first();
           $indicador = Indicadores::where('id', $medicion->indicador_id)->first();
           $usuario = Users::where('id',$indicador->revisor_id)->first();
           $usuarioRevisorSuplente = Users::where('id',$indicador->revisor_id)->first();
           if($usuarioRevisorSuplente){
              $emails = [$usuario->email,Auth::user()->email , $usuarioRevisorSuplente->email];
            }else{
              $emails = [$usuario->email,Auth::user()->email];
            }
           $datos = ['medicion'=>$nuevaMedicion , 'indicador'=>$indicador];

           foreach ($emails as $key => $correo) {
             Mail::to($correo)->send(new NotificacionCorreccionReporteEmail($datos));
             $notificacionesId = 6;
             $nuevoCorreo = new CorreoNotificaciones();
             $nuevoCorreo->id_notificaciones = $notificacionesId;
             $nuevoCorreo->destintario = $correo;
             $nuevoCorreo->save();
           }

         }

         public function revision($id)
              {
                //$selectEstado[null] = 'SELECCIONE ESTADO'; foreach (MedicionEstados::orderby('id', 'asc')->get() as $key1 => $value1) { $selectEstado[$value1->id]=$value1->nombre;}
                $selectEstado[null] = 'SELECCIONE ESTADO'; foreach (MedicionEstados::whereIn('id' , [1 ,6 ,2])->orderby('id', 'asc')->get() as $key1 => $value1) { $selectEstado[$value1->id]=$value1->nombre;}
                $selectIndicador[null] = "SELECCIONE INDICADOR"; foreach (Indicadores::orderby('id', 'desc')->get() as $llave => $valor) {  $selectIndicador[$valor->id] = $valor->Nombrecompleto;}
                $fechaActual = Carbon::now();
                $editRevision = Mediciones::where('id' , $id)->first();
                $editRevisionHisto = MedicionHistoricos::where('medicion_id' , $editRevision->$id)->first();
                $indicador = Indicadores::where('id', $editRevision->indicador_id)->first();
                return view('sigc.sigc_revision')
                ->with('editRevision' , $editRevision)
                ->with('editRevisionHisto' , $editRevisionHisto)
                ->with('selectEstado' , $selectEstado)
                ->with('fechaActual' , $fechaActual)
                ->with('selectIndicador' , $selectIndicador)
                ->with('indicador' , $indicador);
              }

         public function revisada(MedicionesRequest $request, $id)
              {
                $revisionFecha = $request->revisionFecha;
                $revisionObs = $request->revisionObs;
                $estado = $request->estado;
                $nuevaRevision = Mediciones::where('id' , $id)->first();
                $nuevaRevision->revision_fecha = $revisionFecha;
                $nuevaRevision->revision_obs = $revisionObs;
                $nuevaRevision->medicion_estado_id = $estado;
                $nuevaRevision->save();
                if ($nuevaRevision){
                  $this->notificacionRevision($nuevaRevision);
                }

                switch ($estado) {
                      case 1:
                          $estado2 = 4;
                          break;
                      case 2:
                          $estado2 = 3;
                          break;
                      case 6:
                          $estado2 = 5;
                          break;
                  }

                  $email = Auth::user()->email;
                  $nuevaMedicionHisto2 = new MedicionHistoricos();
                  $nuevaMedicionHisto2->medicion_id =  $nuevaRevision->id;
                  $nuevaMedicionHisto2->calendario_id =  $nuevaRevision->calendario_id;
                  $nuevaMedicionHisto2->indicador_id =  $nuevaRevision->indicador_id;
                  $nuevaMedicionHisto2->fecha = Carbon::now();
                  $nuevaMedicionHisto2->valor_numerador = $nuevaRevision->valor_numerador;
                  $nuevaMedicionHisto2->analisis = $nuevaRevision->analisis;
                  $nuevaMedicionHisto2->acciones = $nuevaRevision->acciones;
                  $nuevaMedicionHisto2->usuario = $email;
                  $nuevaMedicionHisto2->revision_fecha = $revisionFecha;
                  $nuevaMedicionHisto2->revision_obs = $revisionObs;
                  $nuevaMedicionHisto2->medicion_evento_id =  $estado2;
                  $nuevaMedicionHisto2->denominador = $nuevaRevision->denominador;
                  $nuevaMedicionHisto2->save();

                alert()->success('!','Revisión realizada exitosamente');
                return redirect()->route('sigc_mediciones_revision');
              }


          public function notificacionRevision($nuevaRevision){

                $medicion = Mediciones::where('id' , $nuevaRevision->id)->first();
                $indicador = Indicadores::where('id', $medicion->indicador_id)->first();
                $usuario = Users::where('id',$indicador->reportador_id)->first();
                $emails = [$usuario->email,Auth::user()->email];
                $datos = ['medicion'=>$nuevaRevision , 'indicador'=>$indicador];


                foreach ($emails as $key => $correo) {
                  Mail::to($correo)->send(new NotificacionRevisionEmail($datos));
                  $notificacionesId = 5;
                  $nuevoCorreo = new CorreoNotificaciones();
                  $nuevoCorreo->id_notificaciones = $notificacionesId;
                  $nuevoCorreo->destintario = $correo;
                  $nuevoCorreo->save();
          }
        }

          public function revisioncorreccion($id)
               {
                 $selectEstado[null] = 'SELECCIONE ESTADO'; foreach (MedicionEstados::whereIn('id' , [1 ,6])->orderby('id', 'asc')->get() as $key1 => $value1) { $selectEstado[$value1->id]=$value1->nombre;}
                 $selectIndicador[null] = "SELECCIONE INDICADOR"; foreach (Indicadores::orderby('id', 'desc')->get() as $llave => $valor) {  $selectIndicador[$valor->id] = $valor->Nombrecompleto;}
                 $fechaActual = Carbon::now();
                 $editRevision = Mediciones::where('id' , $id)->first();
                 $editRevisionHisto = MedicionHistoricos::where('medicion_id' , $editRevision->$id)->first();
                 $indicador = Indicadores::where('id', $editRevision->indicador_id)->first();
                 return view('sigc.sigc_editar_revision')
                 ->with('editRevision' , $editRevision)
                 ->with('editRevisionHisto' , $editRevisionHisto)
                 ->with('selectEstado' , $selectEstado)
                 ->with('fechaActual' , $fechaActual)
                 ->with('selectIndicador' , $selectIndicador)
                 ->with('indicador' , $indicador);
               }

          public function correccionrevisada(MedicionesRequest $request, $id)
               {
                 $revisionFecha = $request->revisionFecha;
                 $revisionObs = $request->revisionObs;
                 $estado = $request->estado;
                 if(!$estado){
                  return redirect()->back()->withErrors(['es necesario colocar un estado a la medicion']);
                 }
                 $nuevaRevision = Mediciones::where('id' , $id)->first();
                 $nuevaRevision->revision_fecha = $revisionFecha;
                 $nuevaRevision->revision_obs = $revisionObs;
                 $nuevaRevision->medicion_estado_id = $estado;
                 $nuevaRevision->save();

                 if ($nuevaRevision){
                   $this->notificacionRevisionCorreccion($nuevaRevision);
                 }

                 switch ($estado) {
                       case 1:
                           $estado2 = 4;
                           break;
                       case 2:
                           $estado2 = 3;
                           break;
                       case 6:
                           $estado2 = 5;
                           break;
                   }

                   $email = Auth::user()->email;
                   $nuevaMedicionHisto2 = new MedicionHistoricos();
                   $nuevaMedicionHisto2->medicion_id =  $nuevaRevision->id;
                   $nuevaMedicionHisto2->calendario_id =  $nuevaRevision->calendario_id;
                   $nuevaMedicionHisto2->indicador_id =  $nuevaRevision->indicador_id;
                   $nuevaMedicionHisto2->fecha = Carbon::now();
                   $nuevaMedicionHisto2->valor_numerador = $nuevaRevision->valor_numerador;
                   $nuevaMedicionHisto2->analisis = $nuevaRevision->analisis;
                   $nuevaMedicionHisto2->acciones = $nuevaRevision->acciones;
                   $nuevaMedicionHisto2->usuario = $email;
                   $nuevaMedicionHisto2->revision_fecha = $revisionFecha;
                   $nuevaMedicionHisto2->revision_obs = $revisionObs;
                   $nuevaMedicionHisto2->medicion_evento_id =  $estado2;
                   $nuevaMedicionHisto2->denominador = $nuevaRevision->denominador;
                   $nuevaMedicionHisto2->save();

                 alert()->success('!','Revisión de corrección realizada exitosamente');
                 return redirect()->route('sigc_mediciones_revision');
               }


           public function notificacionRevisionCorreccion($nuevaRevision){

                 $medicion = Mediciones::where('id' , $nuevaRevision->id)->first();
                 $indicador = Indicadores::where('id', $medicion->indicador_id)->first();
                 $usuario = Users::where('id',$indicador->reportador_id)->first();
                 $emails = [$usuario->email,Auth::user()->email];
                 $datos = ['medicion'=>$nuevaRevision , 'indicador'=>$indicador];

                 foreach ($emails as $key => $correo) {
                   Mail::to($correo)->send(new NotificacionRevisionCorreccionEmail($datos));
                   $notificacionesId = 7;
                   $nuevoCorreo = new CorreoNotificaciones();
                   $nuevoCorreo->id_notificaciones = $notificacionesId;
                   $nuevoCorreo->destintario = $correo;
                   $nuevoCorreo->save();
                 }
           }


           public function editmedicion($id)
                {
                  $selectEstado[null] = 'SELECCIONE ESTADO'; foreach (MedicionEstados::orderby('id', 'asc')->get() as $key1 => $value1) { $selectEstado[$value1->id]=$value1->nombre;}
                  $selectIndicador[null] = "SELECCIONE INDICADOR"; foreach (Indicadores::orderby('id', 'desc')->get() as $llave => $valor) {  $selectIndicador[$valor->id] = $valor->Nombrecompleto;}
                  $fechaActual = Carbon::now();
                  $editRevision = Mediciones::where('id' , $id)->first();
                  $indicador = Indicadores::where('id', $editRevision->indicador_id)->first();
                   return view('sigc.sigc_editar_medicion')
                        ->with('editRevision' , $editRevision)
                        ->with('selectEstado' , $selectEstado)
                        ->with('fechaActual' , $fechaActual)
                        ->with('selectIndicador' , $selectIndicador)
                        ->with('indicador' , $indicador);

                }


           public function updatetmedicion(MedicionesRequest $request, $id)
                {
                  $valorNumerador = $request->valorNumerador;
                  $analisis = $request->analisis;
                  $acciones = $request->acciones;
                  $denominador = $request->denominador;
                  $resultado = $request->resultado;
                  $revisionFecha = $request->revisionFecha;
                  $revisionObs = $request->revisionObs;
                  $estado = $request->estado;
                  $idMedicionEstado = MedicionEstados::where('nombre' , 'En Revisión')->first()->id;

                  $nuevaMedicion =Mediciones::where('id' , $id)->first();
                  $indicador= Indicadores::where('id' , $nuevaMedicion->indicador_id)->first();
                  $nuevaMedicion->fecha = Carbon::now();
                  $nuevaMedicion->valor_numerador = $valorNumerador;
                  $nuevaMedicion->resultado =  $resultado;
                  $nuevaMedicion->analisis = $analisis;
                  $nuevaMedicion->acciones = $acciones;
                  $nuevaMedicion->medicion_estado_id =  $idMedicionEstado;
                  $nuevaMedicion->revision_fecha = $revisionFecha;
                  $nuevaMedicion->revision_obs = $revisionObs;
                  $nuevaMedicion->medicion_estado_id = $estado;

                  if($indicador->categoria_id == 2){
                   $nuevaMedicion->denominador = $denominador;
                  }
                  $nuevaMedicion->save();

                  $documentos = $request->filename;
                  if($documentos){
                     foreach ($documentos as $key => $value) {
                           $nuevoArchivo = new MedicionArchivos();
                           $nuevoArchivo->medicion_id = $nuevaMedicion->id;
                           $nuevoArchivo->filename = '"'.$value->getClientOriginalName().'"';
                           $nuevoArchivo->visible = 1;
                           $nuevoArchivo->save();

                           $extencion =  $value->getClientOriginalExtension();
                           $docNombre = sha1(Auth::user()->id).'.'.$extencion;
                           $upload = Storage::disk('s3')->put('medicion_archivo/'.$nuevoArchivo->id, $value, 'public');
                           $nuevoArchivo->filename_aws = 'https://sigc.s3.amazonaws.com/'.$upload;
                           $nuevoArchivo->save();
                     }
                 }

                 /* CALCULO DE FORMULAS */

                 /* datos */
                 $numerador = $valorNumerador;
                 $denominador = $denominador;
                 $casoEspecial01 = 22.32;
                 $casoEspecial02  = 12;
                 $meta = $indicador->meta;
                 $ponderadorOriginal = $indicador->ponderador;
                 $ponderador = ($indicador->ponderador/100);
                 $ponderador_d = $ponderador;
                 $porcentaje = 100;
                 $multiplicador = $indicador->multiplicador;
                 $pasarCalculo = true;
                 $pasarCalculoMeta = true;
                 $pasarCalculoCumplimiento = true;
                 $c1 = 51;
                 $sinformula = 0;
                 $medicionAprobada = true;
                 if($indicador->categoria_id == 1){
                   $denominador = $indicador->denominador;
                   }else{
                       $denominador = $denominador;
                   }

                   $validacionesIndicador3 = Condiciones_formulas_indicadores::where('tipo_formula' , 'formula_cumplimiento')->where('id_indicador' , $indicador->id)->where('estatus', 1)->get();
                   if(count($validacionesIndicador3)>0){
                       foreach($validacionesIndicador3 as $indiceValidacion3 => $valorValidacion3 ){
                           eval('$resultadoTempVal3 =' .$valorValidacion3->validacion.';');
                           if(!$resultadoTempVal3){
                               $pasarCalculoCumplimiento = false;
                               break;
                           }
                       }
                   }



                   /*calculo de la formula de resultado*/
                   if($value0->formula_cumplimiento && $pasarCalculoCumplimiento){
                   $formulaCumplimiento = $indicador->formula_cumplimiento;
                 }else{
                   $formulaCumplimiento = 0;
                 }
                 eval('$resultado =' .$formulaCumplimiento.';');
                 $nuevaMedicion->resultado =  $resultado;
                 /*calculo de la formula de meta*/
                 $validacionesIndicador2 = Condiciones_formulas_indicadores::where('tipo_formula' , 'formula_meta')->where('id_indicador' , $indicador->id)->where('estatus', 1)->get();
                 if(count($validacionesIndicador2)>0){
                     foreach($validacionesIndicador2 as $indiceValidacion2 => $valorValidacion2 ){
                         eval('$resultadoTempVal2 =' .$valorValidacion2->validacion.';');
                         if(!$resultadoTempVal2){
                             $pasarCalculoMeta = false;
                             break;
                         }
                     }
                 }
                 if($indicador->formula_calculo_meta && $pasarCalculoMeta){
                   $formulaMeta = $indicador->formula_calculo_meta;
                 }else{
                   $formulaMeta = 0;
                 }
                  eval('$resultadoM =' .$formulaMeta.';');
                  $nuevaMedicion->resultado_formula_meta =  $resultadoM;

                 /*calculo de la formula de ponderacion*/
                 $validacionesIndicador = Condiciones_formulas_indicadores::where('tipo_formula' , 'formula_ponderador')->where('id_indicador' , $indicador->id)->where('estatus', 1)->get();
                 if(count($validacionesIndicador)>0){
                     foreach($validacionesIndicador as $indiceValidacion => $valorValidacion ){
                         eval('$resultadoTempVal =' .$valorValidacion->validacion.';');
                         if(!$resultadoTempVal){
                             $pasarCalculo = false;
                             break;
                         }
                     }
                 }
                 if($indicador->formula_ponderador  && $pasarCalculo){
                   $formulaPonderador = $indicador->formula_ponderador;
                 }else{
                   $formulaPonderador = 0;
                 }


                  eval('$resultadoP =' .$formulaPonderador.';');
                  $nuevaMedicion->resultado_formula_ponderacion =  $resultadoP;
                  $nuevaMedicion->save();

                 /* fin CALCULO DE FORMULAS */

                  alert()->success('!','Corrección realizada exitosamente');
                  return redirect()->route('sigc_mediciones');
                }

            public function editMedicionArchivos($id)
            {

                  $editRevision = Mediciones::where('id' , $id)->first();
                   return view('sigc.sigc_mediciones_archivos_asociados')
                        ->with('editRevision' , $editRevision);

            }

            public function chageStatusFile($idArchivo){
              $archivo = MedicionArchivos::where('id' , $idArchivo)->first();
              $archivo->visible = 0;
              $archivo->save();
              return response()->json(['success'=>'true']);
            }

            public function calcular($num,$den,$ind){
              $indicador  = Indicadores::where('id' , $ind)->first();
              $numerador = $num;
              $denominador = $den;
              $multiplicador =  $indicador->multiplicador;
              $ponderador = $indicador->poderador;
              $meta =  $indicador->meta;
              $resultado = 0;
              $formula = $indicador->formula;
              eval('$resultado =' .$formula.';');
              return response()->json($resultado);
            }


}
