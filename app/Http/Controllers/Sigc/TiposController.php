<?php

namespace App\Http\Controllers\Sigc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Sigc\Tipos;
use App\Models\Sigc\TiposTabla;
use App\Http\Requests\SIGC\TiposRequest;


class TiposController extends Controller
{

    public function index(Request $request)
    {
      //dd($request->nombre);
      /*   set_time_limit(14200);
        $dataDefinitiva = [];
        $dataFiltrada = [];


        $client = new Client();

            try {
              $request = $client->get("http://10.100.23.13/webserviceautenticateactived/public/api/Get/Tipos" , ['verify' => false]);
              $response = $request->getBody()->getContents();
              $data =  json_decode($response);
            }catch (BadResponseException   $e) {
              $response = $e->getResponse();
          }



//dd($data2);
       foreach ($data as $key => $value) {

        $objeto = new Tipos();
        $objeto->id = $value->id;
        $objeto->nombre = $value->nombre;
        $objeto->ano = $value->ano;
        $objeto->alias = $value->alias;
        $dataDefinitiva[$key] = $objeto;
       }    */

       $dataDefinitiva = [];

       $requestnombreTipo = $request->nombre;
       $requestanoTipo = $request->ano;
       $requestaliasTipo = $request->alias;
       $dataDefinitiva = Tipos::orderby('id' , 'desc')->get();

      return view('sigc.sigc_tipos')
      ->with('dataDefinitiva' , $dataDefinitiva)
      ->with('requestnombreTipo' , $requestnombreTipo)
      ->with('requestanoTipo' , $requestanoTipo)
      ->with('requestaliasTipo' , $requestaliasTipo);
    }

    public function create()
    {
      $selectAno = [''=>'Seleccione Año','2021'=>'2021','2022'=>'2022'];

      return view('sigc.sigc_crear_tipo')
      ->with('selectAno' , $selectAno);
    }

    public function store(TiposRequest $request)
    {
      $nombre = $request->nombre;
      $ano = $request->ano;
      $alias = $request->alias;
      $nuevoTipo = new TiposTabla();
      $nuevoTipo->nombre = $nombre;
      $nuevoTipo->ano = $ano;
      $nuevoTipo->alias = $alias;
      $nuevoTipo->save();
      alert()->success('!','Tipo creado exitosamente');
      return redirect()->route('sigc_tipos');
    }

    public function edit($id)
    {
      $selectAno = [''=>'Seleccione Año','2021'=>'2021','2022'=>'2022'];
      $editTipo = TiposTabla::where('id' , $id)->first();
      return view('sigc.sigc_editar_tipo')
      ->with('selectAno' , $selectAno)
      ->with('editTipo' , $editTipo);
    }

    public function update(TiposRequest $request, $id)
    {
      $nombre = $request->nombre;
      $ano = $request->ano;
      $alias = $request->alias;
      $nuevoTipo = TiposTabla::where('id' , $id)->first();
      $nuevoTipo->nombre = $nombre;
      $nuevoTipo->ano = $ano;
      $nuevoTipo->alias = $alias;
      $nuevoTipo->save();
      alert()->success('!','Tipo editado exitosamente');
      return redirect()->route('sigc_tipos');
    }

}
