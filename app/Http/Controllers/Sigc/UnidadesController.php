<?php

namespace App\Http\Controllers\Sigc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Sigc\Unidades;
use App\Models\Sigc\UnidadesTabla;
use App\Http\Requests\SIGC\UnidadesRequest;

class UnidadesController extends Controller
{

    public function index(Request $request)
    {
      //dd($request->nombre);
      /*   set_time_limit(14200);
        $dataDefinitiva = [];
        $dataFiltrada = [];


        $client = new Client();

            try {
              $request = $client->get("http://10.100.23.13/webserviceautenticateactived/public/api/Get/Unidades" , ['verify' => false]);
              $response = $request->getBody()->getContents();
              $data =  json_decode($response);
            }catch (BadResponseException   $e) {
              $response = $e->getResponse();
          }



//dd($data2);
       foreach ($data as $key => $value) {

        $objeto = new Unidades();
        $objeto->id = $value->id;
        $objeto->nombre = $value->nombre;
        $dataDefinitiva[$key] = $objeto;
       }    */

       $dataDefinitiva = [];

       $requestnombreUnidad = $request->nombre;

       $dataDefinitiva = Unidades::get();

      return view('sigc.sigc_unidades')
      ->with('dataDefinitiva' , $dataDefinitiva)
      ->with('requestnombreUnidad' , $requestnombreUnidad);
    }

    public function create()
    {
      return view('sigc.sigc_crear_unidad');
    }

    public function store(UnidadesRequest $request)
    {
      $nombre = $request->nombre;
      $nuevaUnidad = new UnidadesTabla();
      $nuevaUnidad->nombre = $nombre;
      $nuevaUnidad->save();
      alert()->success('!','Unidad creada exitosamente');
      return redirect()->route('sigc_unidades');
    }

    public function edit($id)
    {
      $editUnidad = UnidadesTabla::where('id' , $id)->first();
      return view('sigc.sigc_editar_unidad')
      ->with('editUnidad' , $editUnidad);
    }

    public function update(UnidadesRequest $request, $id)
    {
      $nombre = $request->nombre;
      $nuevaUnidad = UnidadesTabla::where('id' , $id)->first();
      $nuevaUnidad->nombre = $nombre;
      $nuevaUnidad->save();
      alert()->success('!','Unidad editada exitosamente');
      return redirect()->route('sigc_unidades');
    }

}
