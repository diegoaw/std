<?php

namespace App\Http\Controllers\Sigc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Sigc\Users;
use App\Models\Sigc\Roles;
use App\Models\Sigc\HasRoles;
use App\User;
use App\Models\Seguridad\Role;
use App\Models\Seguridad\AssignedRoles;
use App\Models\UsuariosMindep\UsuariosMindep;
use App\Models\PyR\PyR;
use App\Http\Requests\SIGC\UsuariosRequest;
use Illuminate\Support\Facades\Hash;

class UsuariosController extends Controller
{

    public function index(Request $request)
    {

       $dataDefinitiva = [];
       $dataDefinitiva = Users::orderby('nombres', 'asc')->get();

      return view('sigc.sigc_usuarios')
      ->with('dataDefinitiva' , $dataDefinitiva);
    }

    public function create()
    {
      $roles=Role::whereIn('id',[23,24,25,29,36,53])->get();
      $selectUser[null] = 'SELECCIONE USUARIO'; foreach (PyR::orderby('apellido_paterno', 'asc')->get() as $key1 => $value1) { $selectUser[$value1->id]=$value1->nombresc;}
      return view('sigc.sigc_crear_usuario')
      ->with('selectUser', $selectUser)
      ->with('roles', $roles);
    }


     public function store(UsuariosRequest $request)
    {
      $nombres = $request->nombres;
      $apellidoP = $request->apellidoP;
      $apellidoM = $request->apellidoM;
      $name = $nombres.' '.$apellidoP.' '.$apellidoM;
      $email = $request->email;
      $estado = 1;

      $exist = Users::where('email' , $request->email)->first();

        if(!$exist){
          $nuevoUsuario = new Users();
          $nuevoUsuario->nombres = $nombres;
          $nuevoUsuario->ap_paterno = $apellidoP;
          $nuevoUsuario->ap_materno = $apellidoM;
          $nuevoUsuario->email = $email;
          $nuevoUsuario->estado = $estado;
          $nuevoUsuario->save();
        }else{
          $nuevoUsuario = Users::where('email' , $request->email)->first();
        }

      $exist2 = User::where('email' , $request->email)->first();

        if(!$exist2){
          $nuevoUsuario2 = new User();
          $nuevoUsuario2->name = $name;
          $nuevoUsuario2->email = $email;
          $nuevoUsuario2->password = Hash::make(1234);
          $nuevoUsuario2->save();
        }else{
          $nuevoUsuario2 = User::where('email' , $request->email)->first();
        }

      foreach ($request->get('assignees_roles') as $rol) {
          $assignees_roles= new AssignedRoles ;
          $assignees_roles->user_id = $nuevoUsuario2->id;
          $assignees_roles->role_id = $rol;
          $assignees_roles->save();
      }

      foreach ($request->get('assignees_roles') as $roles) {
          $has_roles= new HasRoles ;
          $has_roles->user_id = $nuevoUsuario->id;
          if($roles == 23){
          $has_roles->role_id = 2;}
          if($roles == 24){
          $has_roles->role_id = 3;}
          if($roles == 25){
          $has_roles->role_id = 1;}
          if($roles == 29){
          $has_roles->role_id = 5;}
          if($roles == 36){
          $has_roles->role_id = 6;}
          if($roles == 53){
          $has_roles->role_id = 7;}
          $has_roles->save();
      }

      alert()->success('!','Usuario creado exitosamente');
      return redirect()->route('sigc_usuarios');
    }

    public function edit($id)
    {
      $roles=Role::whereIn('id',[23,24,25,29,36,53])->get();
      $user_roles [] = [];

      
      $selectUser[null] = 'SELECCIONE USUARIO'; foreach (PyR::orderby('apellido_paterno', 'asc')->get() as $key1 => $value1) { $selectUser[$value1->id]=$value1->nombresc;}
      
      $editUsuario = Users::where('id' , $id)->first();
      $editUsuario2 = User::where('email' , $editUsuario->email)->first();

      if(!$editUsuario2){
        $nuevoUsuarioStd = new User();
        $nuevoUsuarioStd->email = $editUsuario->email;
        $nuevoUsuarioStd->name = '';
        $nuevoUsuarioStd->password = '';
        $nuevoUsuarioStd->save();
      }
      if($editUsuario2){
      foreach ($editUsuario2->roles as $rol) {
        $user_roles [] = $rol->id;
    }
  }else{
    foreach ($nuevoUsuarioStd->roles as $rol) {
      $user_roles [] = $rol->id;
  }
  }
      return view('sigc.sigc_editar_usuario')
      ->with('selectUser', $selectUser)
      ->with('user_roles', $user_roles)
      ->with('roles', $roles)
      ->with('editUsuario' , $editUsuario);
    }

    public function update(UsuariosRequest $request, $id)
    {
      $nombres = $request->nombres;
      $apellidoP = $request->apellidoP;
      $apellidoM = $request->apellidoM;
      $email = $request->email;
      $nuevoUsuario = Users::where('id' , $id)->first();
      $nuevoUsuario->nombres = $nombres;
      $nuevoUsuario->ap_paterno = $apellidoP;
      $nuevoUsuario->ap_materno = $apellidoM;
      $nuevoUsuario->email = $email;
      if ($request->get('estado')==null){
          $nuevoUsuario->estado = 0;
      } else {
          $nuevoUsuario->estado = 1;
      }
      $nuevoUsuario->save();


      $editUsuario = Users::where('id' , $id)->first();
      $editUsuario2 = User::where('email' , $editUsuario->email)->first();

      $assigned_roles= AssignedRoles::where('user_id',$editUsuario2->id )->whereIn('role_id' , [23,24,25,29,36,53] )->get();
      if(count($assigned_roles)>0){
        foreach ($assigned_roles as $assigned_role) {
            $assigned_roles_d=AssignedRoles::FindOrFail($assigned_role->id);
            $assigned_roles_d->delete();
        }
      }

      $hastRolePlanificacion = HasRoles::where('user_id',$editUsuario->id )->whereIn('role_id' , [23,24,25,29,36,53] )->get();
      if(count($hastRolePlanificacion)>0){
        foreach ($hastRolePlanificacion as $assigned_role_pla) {
            $assigned_roles_d_pla=HasRoles::FindOrFail($assigned_role_pla->id);
            $assigned_roles_d_pla->delete();
        }
      }

      foreach ($request->get('assignees_roles') as $rol) {
        $assignees_roles= new AssignedRoles ;
        $assignees_roles->user_id = $editUsuario2->id;
        $assignees_roles->role_id = $rol;
        $assignees_roles->save();
    }

      foreach ($request->get('assignees_roles') as $roles) {
        $has_roles= new HasRoles ;
        $has_roles->user_id = $editUsuario->id;
        if($roles == 23){
        $has_roles->role_id = 2;}
        if($roles == 24){
        $has_roles->role_id = 3;}
        if($roles == 25){
        $has_roles->role_id = 1;}
        if($roles == 29){
        $has_roles->role_id = 5;}
        if($roles == 36){
        $has_roles->role_id = 6;}
        if($roles == 53){
        $has_roles->role_id = 7;}
        $has_roles->save();
    }

      alert()->success('!','Usuario editado exitosamente');
      return redirect()->route('sigc_usuarios');
    }

    public function getUsuarioMindep($id){
      $usuario = PyR::where('id' , $id)->first();
      return $usuario;
    }

}
