<?php

namespace App\Http\Controllers\TextoDocElectronicos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Models\IP;

class TextoDocElectronicosController extends Controller
{

    public function index()
    {
      $sineditar = 1;
      $textoeditado = '';

        return view('texto_doc_electronicos.editar_texto')
        ->with('sineditar' ,$sineditar)
        ->with('textoeditado' ,$textoeditado);
    }
 
    public function convertir(Request $request)
    {
      $sineditar = 0;
      $textoeditado = $request->texto;
      
        return view('texto_doc_electronicos.editar_texto')
        ->with('sineditar' ,$sineditar)
        ->with('textoeditado' ,$textoeditado);
    }

    public function testGetIp(){
        
        $newInt = new IP();
        $newInt->ipaddress =request()->ip();
        $newInt->save();
        return response()->json(';) --- ;)----' , 200, array('Content-Type' => 'application/json;charset=utf8'), JSON_UNESCAPED_UNICODE);
    }
    public function testSqlServer(){
         $query = "SELECT * FROM indicadores";
         $indicadores = DB::connection('sqlsrv')->select($query);
         dd($indicadores);
    }
    
}
