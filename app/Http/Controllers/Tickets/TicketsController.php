<?php

namespace App\Http\Controllers\Tickets;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use App\Models\TramitesSimple\TramitesSimple;
use App\Models\File\File;
use App\Models\Simple\HasGrupo;
use App\Models\Simple\Grupos;
use App\Models\UsuariosSimple\UsuariosSimple;
use App\Models\Divisiones\Divisiones;
use App\Models\Simple\TmpTramiteCompras;
use App\Models\Simple\Etapa;
use App\Models\Simple\DatoSeguimiento;
use App\Models\Tickets\Tickets;

class TicketsController extends Controller
{
    public function index(Request $request){

    $tramites = [];
    $tramiteFiltrado = [];
    $idTramiteFiltro = $request->nRegistro;
    $tituloFiltro = $request->titulo;

    $queryFilter = "where r.numero_tramite = $idTramiteFiltro ";
    
        $query = "SELECT  *
          FROM
            (select
            tramite.id as numero_tramite ,
            tramite.proceso_id as numero_proeceso ,
            proceso.nombre as nombre_proceso ,
            tramite.created_at as fecha_inicio ,
            tramite.updated_at as fecha_ultima_modificacion ,
            tramite.pendiente as pendiente,
            tramite.ended_at as fecha_culmino,
            tarea.nombre as etapa,
            et.id as id_etapa,
            dts.conteo as cantidad_campos
            from tramite
            INNER JOIN proceso ON tramite.proceso_id = proceso.id
            INNER JOIN (SELECT * FROM etapa WHERE id IN (SELECT MAX(id) FROM etapa GROUP BY tramite_id) ORDER BY id DESC) et on et.tramite_id = tramite.id
            INNER JOIN tarea on et.tarea_id = tarea.id
            LEFT JOIN (SELECT  COUNT(id) as conteo, etapa_id FROM dato_seguimiento  group by etapa_id ) dts on dts.etapa_id = et.id
            where tramite.proceso_id = 29  and tarea.nombre = 'Ingreso Ticket' and tramite.deleted_at is null
            and dts.conteo >4             
          UNION ALL
            select
            tramite.id as numero_tramite ,
            tramite.proceso_id as numero_proeceso ,
            proceso.nombre as nombre_proceso ,
            tramite.created_at as fecha_inicio ,
            tramite.updated_at as fecha_ultima_modificacion ,
            tramite.pendiente as pendiente,
            tramite.ended_at as fecha_culmino,
            tarea.nombre as etapa,
            et.id as id_etapa,
            dts.conteo as cantidad_campos
            from tramite
            INNER JOIN proceso ON tramite.proceso_id = proceso.id
            INNER JOIN (SELECT * FROM etapa WHERE id IN (SELECT MAX(id) FROM etapa GROUP BY tramite_id) ORDER BY id DESC) et on et.tramite_id = tramite.id
            INNER JOIN tarea on et.tarea_id = tarea.id
            LEFT JOIN (SELECT  COUNT(id) as conteo, etapa_id FROM dato_seguimiento  group by etapa_id ) dts on dts.etapa_id = et.id
            where tramite.proceso_id = 29
            and tramite.deleted_at is null
            and tarea.nombre <> 'Ingreso Ticket') r ";
    
        ($idTramiteFiltro != null )? $query = $query.$queryFilter : $query = $query;
        $query = $query."order by r.numero_tramite DESC";
    
        $tramites = DB::connection('mysqlSimple')->select($query);

        foreach ($tramites as $llave2 => $valor2) {
          $etapas = Etapa::Select('id')->where('tramite_id' , $valor2->numero_tramite)->get()->pluck('id');
          $etapas2 = Etapa::Select('id')->where('tramite_id' , $valor2->numero_tramite)->get()->pluck('id');
          

          
          $datoSeguimientoACargo = DatoSeguimiento::whereIn('etapa_id', $etapas)->where('nombre', 'a_cargo')->orderBy('etapa_id', 'desc')->first();
          if ($datoSeguimientoACargo) {
            $datoSeguimientoACargo = $datoSeguimientoACargo->valor;
            $datoSeguimientoACargo = str_replace( '"', '' , $datoSeguimientoACargo);
            }
        
          $valor2->ACargo = $datoSeguimientoACargo;

          $datoSeguimientoTipos = DatoSeguimiento::whereIn('etapa_id', $etapas)->where('nombre', 'tipo_solicitud_s')->first();
          if ($datoSeguimientoTipos) {
            $datoSeguimientoTipos = $datoSeguimientoTipos->valor;
            $datoSeguimientoTipos = str_replace( '"', '' , $datoSeguimientoTipos);
            }
        
          $valor2->Tipos = $datoSeguimientoTipos;

          $datoSeguimientoTipod = DatoSeguimiento::whereIn('etapa_id', $etapas)->where('nombre', 'tipo_solicitud_d')->first();
          if ($datoSeguimientoTipod) {
            $datoSeguimientoTipod = $datoSeguimientoTipod->valor;
            $datoSeguimientoTipod = str_replace( '"', '' , $datoSeguimientoTipod);
            }
        
          $valor2->Tipod  = $datoSeguimientoTipod;

          $datoSeguimientoEstrellas = DatoSeguimiento::whereIn('etapa_id', $etapas)->where('nombre', 'estrellas')->first();
          if ($datoSeguimientoEstrellas) {
            $datoSeguimientoEstrellas = $datoSeguimientoEstrellas->valor;
            $datoSeguimientoEstrellas = str_replace( '"', '' , $datoSeguimientoEstrellas);
            }
        
          $valor2->Estrellas = $datoSeguimientoEstrellas;

          $datoSeguimientoEstado = DatoSeguimiento::whereIn('etapa_id', $etapas2)->where('nombre', 'estado')->orderBy('id', 'desc')->first();
          if ($datoSeguimientoEstado) {
            $datoSeguimientoEstado = $datoSeguimientoEstado->valor;
            $datoSeguimientoEstado = str_replace( '"', '' , $datoSeguimientoEstado);
            }
        
          $valor2->Estado = $datoSeguimientoEstado;
          

        }
        return view('tickets.tickets')
        ->with('tramitesD', $tramites)
        ->with('idTramiteFiltro', $idTramiteFiltro);
    }       

    public function show($id){
                  
        $client = new Client();
              try {
              $request = $client->get("https://simple.mindep.cl/backend/api/tramites/$id?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu " , ['verify' => false]);
              $response = $request->getBody()->getContents();
              $data =  json_decode($response);
                }catch (BadResponseException   $e) {
                    $response = $e->getResponse();
                }
        $data = $data->tramite;
 
        $etapasCont = count($data->etapas);

        $nuevoTicket = new Tickets();
        $nuevoTicket->id =  $data->id;
        $nuevoTicket->fecha_inicio =  $data->fecha_inicio;
        $nuevoTicket->fecha_termino =  $data->fecha_termino;
        $nuevoTicket->fecha_modificacion =  $data->fecha_modificacion;
        $nuevoTicket->estado =  $data->estado;
        
        $datos=$data->datos;

        foreach ($datos as $key => $value2) {
             if(property_exists($value2, 'fecha_actual')){
                $nuevoTicket->fecha_actual =  $value2->fecha_actual;
              }
              if(property_exists($value2, 'cargo')){
                $nuevoTicket->cargo =  $value2->cargo;
              }
              if(property_exists($value2, 'division')){
                $nuevoTicket->division =  $value2->division;
              }
              if(property_exists($value2, 'email_a')){
                $nuevoTicket->email_a =  $value2->email_a;
              }
              if(property_exists($value2, 'area')){
                $nuevoTicket->area =  $value2->area;
              }
              if(property_exists($value2, 'tipo_solicitud_d')){
                $nuevoTicket->tipo_solicitud_d =  $value2->tipo_solicitud_d;
              }
              if(property_exists($value2, 'tipo_solicitud_s')){
                $nuevoTicket->tipo_solicitud_s =  $value2->tipo_solicitud_s;
              }
              if(property_exists($value2, 'observaciones')){
                $nuevoTicket->observaciones =  $value2->observaciones;
              }
              if(property_exists($value2, 'adjunto')){
                $nuevoTicket->adjunto =  $value2->adjunto->URL;
              }
              if(property_exists($value2, 'a_cargo')){
                $nuevoTicket->a_cargo =  $value2->a_cargo;
              }
              if(property_exists($value2, 'estado')){
                $nuevoTicket->estado =  $value2->estado;
              }
              if(property_exists($value2, 'observacion_soporte')){
                $nuevoTicket->observacion_soporte =  $value2->observacion_soporte;
              }
              if(property_exists($value2, 'satisfaccion')){
                $nuevoTicket->satisfaccion =  $value2->satisfaccion;
              }
              if(property_exists($value2, 'estrellas')){
                $nuevoTicket->estrellas =  $value2->estrellas;
              }
              if(property_exists($value2, 'comentarios')){
                $nuevoTicket->comentarios =  $value2->comentarios;
              }
              if(property_exists($value2, 'comprobante')){
                $nuevoTicket->comprobante_descarga = 'https://simple.mindep.cl/uploads/documentos/'.$value2->comprobante;
              }
          }

      return view('tickets.tickets_detalle')
      ->with('data' , $nuevoTicket);
    }

    public function index2(Request $request){

      $tramites = [];
      $tramiteFiltrado = [];
      $idTramiteFiltro = $request->nRegistro;
      $tituloFiltro = $request->titulo;
  
      $queryFilter = "where r.numero_tramite = $idTramiteFiltro ";
      
          $query = "SELECT  *
            FROM
              (select
              tramite.id as numero_tramite ,
              tramite.proceso_id as numero_proeceso ,
              proceso.nombre as nombre_proceso ,
              tramite.created_at as fecha_inicio ,
              tramite.updated_at as fecha_ultima_modificacion ,
              tramite.pendiente as pendiente,
              tramite.ended_at as fecha_culmino,
              tarea.nombre as etapa,
              et.id as id_etapa,
              dts.conteo as cantidad_campos
              from tramite
              INNER JOIN proceso ON tramite.proceso_id = proceso.id
              INNER JOIN (SELECT * FROM etapa WHERE id IN (SELECT MAX(id) FROM etapa GROUP BY tramite_id) ORDER BY id DESC) et on et.tramite_id = tramite.id
              INNER JOIN tarea on et.tarea_id = tarea.id
              LEFT JOIN (SELECT  COUNT(id) as conteo, etapa_id FROM dato_seguimiento  group by etapa_id ) dts on dts.etapa_id = et.id
              where tramite.proceso_id = 29  and tarea.nombre = 'Ingreso Ticket' and tramite.deleted_at is null
              and dts.conteo >4             
            UNION ALL
              select
              tramite.id as numero_tramite ,
              tramite.proceso_id as numero_proeceso ,
              proceso.nombre as nombre_proceso ,
              tramite.created_at as fecha_inicio ,
              tramite.updated_at as fecha_ultima_modificacion ,
              tramite.pendiente as pendiente,
              tramite.ended_at as fecha_culmino,
              tarea.nombre as etapa,
              et.id as id_etapa,
              dts.conteo as cantidad_campos
              from tramite
              INNER JOIN proceso ON tramite.proceso_id = proceso.id
              INNER JOIN (SELECT * FROM etapa WHERE id IN (SELECT MAX(id) FROM etapa GROUP BY tramite_id) ORDER BY id DESC) et on et.tramite_id = tramite.id
              INNER JOIN tarea on et.tarea_id = tarea.id
              LEFT JOIN (SELECT  COUNT(id) as conteo, etapa_id FROM dato_seguimiento  group by etapa_id ) dts on dts.etapa_id = et.id
              where tramite.proceso_id = 29
              and tramite.deleted_at is null
              and tarea.nombre <> 'Ingreso Ticket') r ";
      
          ($idTramiteFiltro != null )? $query = $query.$queryFilter : $query = $query;
          $query = $query."order by r.numero_tramite DESC";
      
          $tramites = DB::connection('mysqlSimple')->select($query);
  
          foreach ($tramites as $llave2 => $valor2) {
            $etapas = Etapa::Select('id')->where('tramite_id' , $valor2->numero_tramite)->get()->pluck('id');
            $etapas2 = Etapa::Select('id')->where('tramite_id' , $valor2->numero_tramite)->get()->pluck('id');
            
  
            
            $datoSeguimientoACargo = DatoSeguimiento::whereIn('etapa_id', $etapas)->where('nombre', 'a_cargo')->orderBy('etapa_id', 'desc')->first();
            if ($datoSeguimientoACargo) {
              $datoSeguimientoACargo = $datoSeguimientoACargo->valor;
              $datoSeguimientoACargo = str_replace( '"', '' , $datoSeguimientoACargo);
              }
          
            $valor2->ACargo = $datoSeguimientoACargo;
  
            $datoSeguimientoTipos = DatoSeguimiento::whereIn('etapa_id', $etapas)->where('nombre', 'tipo_solicitud_s')->first();
            if ($datoSeguimientoTipos) {
              $datoSeguimientoTipos = $datoSeguimientoTipos->valor;
              $datoSeguimientoTipos = str_replace( '"', '' , $datoSeguimientoTipos);
              }
          
            $valor2->Tipos = $datoSeguimientoTipos;
  
            $datoSeguimientoTipod = DatoSeguimiento::whereIn('etapa_id', $etapas)->where('nombre', 'tipo_solicitud_d')->first();
            if ($datoSeguimientoTipod) {
              $datoSeguimientoTipod = $datoSeguimientoTipod->valor;
              $datoSeguimientoTipod = str_replace( '"', '' , $datoSeguimientoTipod);
              }
          
            $valor2->Tipod  = $datoSeguimientoTipod;
  
            $datoSeguimientoarea = DatoSeguimiento::whereIn('etapa_id', $etapas)->where('nombre', 'area')->first();
            if ($datoSeguimientoarea) {
              $datoSeguimientoarea = $datoSeguimientoarea->valor;
              $datoSeguimientoarea = str_replace( '"', '' , $datoSeguimientoarea);
              }
          
            $valor2->area = $datoSeguimientoarea;
  
            $datoSeguimientoEstado = DatoSeguimiento::whereIn('etapa_id', $etapas2)->where('nombre', 'estado')->orderBy('id', 'desc')->first();
            if ($datoSeguimientoEstado) {
              $datoSeguimientoEstado = $datoSeguimientoEstado->valor;
              $datoSeguimientoEstado = str_replace( '"', '' , $datoSeguimientoEstado);
              }
          
            $valor2->Estado = $datoSeguimientoEstado;
            
  
          }
          return view('tickets.tickets_funcionarios')
          ->with('tramitesD', $tramites)
          ->with('idTramiteFiltro', $idTramiteFiltro);
      }       
  
      public function show2($id){
                    
          $client = new Client();
                try {
                $request = $client->get("https://simple.mindep.cl/backend/api/tramites/$id?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu " , ['verify' => false]);
                $response = $request->getBody()->getContents();
                $data =  json_decode($response);
                  }catch (BadResponseException   $e) {
                      $response = $e->getResponse();
                  }
          $data = $data->tramite;
   
          $etapasCont = count($data->etapas);
  
          $nuevoTicket = new Tickets();
          $nuevoTicket->id =  $data->id;
          $nuevoTicket->fecha_inicio =  $data->fecha_inicio;
          $nuevoTicket->fecha_termino =  $data->fecha_termino;
          $nuevoTicket->fecha_modificacion =  $data->fecha_modificacion;
          $nuevoTicket->estado =  $data->estado;
          
          $datos=$data->datos;
  
          foreach ($datos as $key => $value2) {
               if(property_exists($value2, 'fecha_actual')){
                  $nuevoTicket->fecha_actual =  $value2->fecha_actual;
                }
                if(property_exists($value2, 'cargo')){
                  $nuevoTicket->cargo =  $value2->cargo;
                }
                if(property_exists($value2, 'division')){
                  $nuevoTicket->division =  $value2->division;
                }
                if(property_exists($value2, 'email_a')){
                  $nuevoTicket->email_a =  $value2->email_a;
                }
                if(property_exists($value2, 'area')){
                  $nuevoTicket->area =  $value2->area;
                }
                if(property_exists($value2, 'tipo_solicitud_d')){
                  $nuevoTicket->tipo_solicitud_d =  $value2->tipo_solicitud_d;
                }
                if(property_exists($value2, 'tipo_solicitud_s')){
                  $nuevoTicket->tipo_solicitud_s =  $value2->tipo_solicitud_s;
                }
                if(property_exists($value2, 'observaciones')){
                  $nuevoTicket->observaciones =  $value2->observaciones;
                }
                if(property_exists($value2, 'adjunto')){
                  $nuevoTicket->adjunto =  $value2->adjunto->URL;
                }
                if(property_exists($value2, 'a_cargo')){
                  $nuevoTicket->a_cargo =  $value2->a_cargo;
                }
                if(property_exists($value2, 'estado')){
                  $nuevoTicket->estado =  $value2->estado;
                }
                if(property_exists($value2, 'observacion_soporte')){
                  $nuevoTicket->observacion_soporte =  $value2->observacion_soporte;
                }
                if(property_exists($value2, 'satisfaccion')){
                  $nuevoTicket->satisfaccion =  $value2->satisfaccion;
                }
                if(property_exists($value2, 'estrellas')){
                  $nuevoTicket->estrellas =  $value2->estrellas;
                }
                if(property_exists($value2, 'comentarios')){
                  $nuevoTicket->comentarios =  $value2->comentarios;
                }
                if(property_exists($value2, 'comprobante')){
                  $nuevoTicket->comprobante_descarga = 'https://simple.mindep.cl/uploads/documentos/'.$value2->comprobante;
                }
            }
  
        return view('tickets.tickets_funcionarios_detalle')
        ->with('data' , $nuevoTicket);
      }

}
