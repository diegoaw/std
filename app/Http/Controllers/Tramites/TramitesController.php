<?php

namespace App\Http\Controllers\Tramites;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\DerivacionesDocumento\DerivacionesDocumento;
use App\Models\IngresoTramite\IngresoTramite;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ComboBoxCompra;
use App\Models\TiposDocumentos\TiposDocumentos;
use DB;
use App\Models\File\File;
use App\Models\Simple\TmpTramiteDerivacion;
use App\Models\Simple\Etapa;
use App\Models\Simple\DatoSeguimiento;
use App\Models\Divisiones\Divisiones;



class TramitesController extends Controller
{
   public function index(Request $request){


  //  dd($request);
      set_time_limit(14200);
      $selectA = $this->cargarExcelDeparatamentos();
      $tiposDocumentos = $this->tiposDocuemntosJsonOpa();
      $anno = Carbon::now()->format('Y');
      $fechaStringInicio = ($anno).'-01-01 00:00:00';
      $feDesde = Carbon::parse($fechaStringInicio);
      $feHasta =  Carbon::parse(Carbon::now()->format('Y-m-d').' 23:59:59');
      $fechaInDesde = Carbon::parse($fechaStringInicio);
      $fechaInHasta = Carbon::now();
      $fechas[1] = Date('Y-m-d', strtotime( '1970-01-01' ));
      $fechas[0] = Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
      $tramites = [];
      $requestNRegistro = null;
      $origenView ='';
      $docdigitalView = '';
      $areRemitenteView = '';
      $areDestitenteView = '';
      $tiposDocumentosView = '';
      $contenidoView='';
      $docto_nView='';
      $origenController='';
      $procedenciaView ='';
      $mostrar = false;

          if( $request->FechaDesde != null ) {
            $feDesde=(Carbon::parse($request->get('FechaDesde'))->format('dmY'));
            $feDesde= Carbon::parse(substr($feDesde, 0, 2).'-'.substr($feDesde, 2, 2).'-'.substr($feDesde, 4, 4).' 00:00:00');
          }
          if( $request->FechaHasta != null ) {
             $feHasta=(Carbon::parse($request->get('FechaHasta'))->format('dmY'));
             $feHasta= Carbon::parse(substr($feHasta, 0, 2).'-'.substr($feHasta, 2, 2).'-'.substr($feHasta, 4, 4).' 23:59:59');
          }
          if($request->origen == '' ) {
                $origenController =  null;
          }else{
            $origenController = $request->origen;
          }
          if($request->docdigital == '' ) {
                $docdigitalController =  null;
          }else{
            $docdigitalController = $request->docdigital;
          }


      $query = IngresoTramite::query ();
      $query->when(request('nRegistro', false), function ($q, $numRegistro) {
            return $q->where('tramite_id', $numRegistro);
        });
      $query->when($feDesde, function ($q, $feDesde ) {
            return $q->where('created_at','>=', $feDesde);
        });

        $query->when($feHasta, function ($q, $feHasta ) {
            return $q->where('created_at','<=', $feHasta);
        });
       $query->when($origenController, function ($q, $origen) {
            return $q->where('origen', $origen);
        });
       $query->when($docdigitalController, function ($q, $docdigital) {
            return $q->where('docdigital', $docdigital);
        });
       $query->when(request('tiposDocumentos', false), function ($q, $tiposDocumentos) {
            return $q->where('tipo_documento', $tiposDocumentos);
        });
       $query->when(request('areaRemtiente', false), function ($q, $areaRemtiente) {
            return $q->where('area_remitente', $areaRemtiente);
        });
       $query->when(request('procedencia'), function ($q, $procedencia) {
            return $q->where('area_remitente', 'LIKE', "%$procedencia%");
        });
       $query->when(request('contenido'), function ($q, $contenido) {
            return $q->where('contenido', 'LIKE', "%$contenido%");
        });
       $query->when(request('docto_n'), function ($q, $docto_n) {
            return $q->where('docto_n', 'LIKE', "%$docto_n%");
        });

       $listado = $query->get()->sortByDesc(function($q){
     return $q->created_at;
     });

      //$listado = $query->get();

        foreach ($listado as $cu => $tramite) {
           $tramiteBDsimple =  File::where('tramite_id' , $tramite->tramite_id)->where('tipo' , 's3')->get()->max('id');
           $infoTramite =  File::where('id' , $tramiteBDsimple)->get()->first();
           $urlDoc = 'https:simple.mindep.cl/uploader/datos_get_s3/'.$infoTramite->id.'/'.$infoTramite->campo_id.'/'.$infoTramite->llave.'/'.$infoTramite->filename;
           $tramite->urlDoc=$urlDoc;
        }

      $listado=$listado->groupby('tramite_id');
      $recorrer = 0;
      foreach ($listado as $indice => $registro) {
        $tramites[$recorrer] = IngresoTramite::where('tramite_id' , $indice)->first();
        $recorrer = $recorrer + 1;
      }

      if( $request->nRegistro != null ) {
        $requestNRegistro = $request->nRegistro;
      }

      if( $request->origen != null ) {
        $origenView =  $request->origen;
      }
      if( $request->docdigital != null ) {
        $docdigitalView =  $request->docdigital;
      }
      if($request->areaRemtiente != null ) {
        $areRemitenteView =  $request->areaRemtiente;
      }
      if($request->areaDesti != null ) {
        $areDestitenteView =  $request->areaDesti;
      }
      if($request->tiposDocumentos != null ) {
        $tiposDocumentosView =  $request->tiposDocumentos;
      }
      if($request->contenido != null ) {
        $contenidoView =  $request->contenido;
      }
      if($request->docto_n != null ) {
        $docto_nView =  $request->docto_n;
      }
       if($request->procedencia != null ) {
        $procedenciaView =  $request->procedencia;
      }

      if (count($tramites)==0){$mostrar = false;} else {$mostrar = true;}


      $mostrar = true;
      return view('tramites.tramites')
      ->with('selectA' ,$selectA)
      ->with('origenView' , $origenView)
      ->with('docdigitalView' , $docdigitalView)
      ->with('procedenciaView' , $procedenciaView)
      ->with('contenidoView' , $contenidoView)
      ->with('docto_nView' , $docto_nView)
      ->with('areRemitenteView' , $areRemitenteView)
      ->with('areDestitenteView' , $areDestitenteView)
      ->with('tiposDocumentosView' , $tiposDocumentosView)
      ->with('tiposDocumentos' ,$tiposDocumentos)
      ->with('requestNRegistro' ,$requestNRegistro)
      ->with('feDesde' ,$feDesde)
      ->with('feHasta' ,$feHasta)
      ->with('fechaInDesde' ,$fechaInDesde)
      ->with('fechaInHasta' ,$fechaInHasta)
      ->with('fechas' ,$fechas)
      ->with('tramites' ,$tramites)
      ->with('listado' ,$listado)
      ->with('mostrar' ,$mostrar);

}

public function cargarExcelDeparatamentos (){


      $array = collect(Excel::load(public_path('departamentos2.xlsx'), function($reader) {})->get());
      $arrayTransformado = [];
      $cantidad = count($array);

      for ($i=0; $i < $cantidad; $i++) {
        $arrayTransformado[$array[$i]['area']] = $array[$i]['area'];
       }

        return (collect($arrayTransformado)->prepend('Todos', null));

    }

public function tiposDocuemntosJsonOpa(){

    $tiposDocumentos = TiposDocumentos::get();
    $tiposDocumentosDefinitivo  = [];
    foreach ($tiposDocumentos as $indice => $regisro) {
      $tiposDocumentosDefinitivo [$regisro->nombre] = $regisro->nombre;
    }

    return (collect($tiposDocumentosDefinitivo)->prepend('Todos', null));
}


public function reporteTramite(Request $request) {

          $tramites = [];
          if( $request->FechaDesde != null ) {
            $feDesde=(Carbon::parse($request->get('FechaDesde'))->format('dmY'));
            $feDesde= Carbon::parse(substr($feDesde, 0, 2).'-'.substr($feDesde, 2, 2).'-'.substr($feDesde, 4, 4).' 00:00:00');
          }
          if( $request->FechaHasta != null ) {
             $feHasta=(Carbon::parse($request->get('FechaHasta'))->format('dmY'));
             $feHasta= Carbon::parse(substr($feHasta, 0, 2).'-'.substr($feHasta, 2, 2).'-'.substr($feHasta, 4, 4).' 23:59:59');
          }
          if($request->origen1 == '' ) {
                $origenController =  null;
          }else{
            $origenController = $request->origen1;
          }
          if($request->docdigital == '' ) {
                $docdigitalController =  null;
          }else{
            $docdigitalController = $request->docdigital;
          }



        $query = IngresoTramite::query ();
        $query->when(request('nRegistro', false), function ($q, $numRegistro) {
            return $q->where('tramite_id', $numRegistro);
        });
        $query->when($feDesde, function ($q, $feDesde ) {
            return $q->where('created_at','>=', $feDesde);
        });

        $query->when($feHasta, function ($q, $feHasta ) {
            return $q->where('created_at','<=', $feHasta);
        });
        $query->when($origenController, function ($q, $origen) {
            return $q->where('origen', $origen);
        });
        $query->when($docdigitalController, function ($q, $docdigital) {
            return $q->where('docdigital', $docdigital);
        });
       $query->when(request('tiposDocumentos1', false), function ($q, $tiposDocumentos) {
            return $q->where('tipo_documento', $tiposDocumentos);
        });
       $query->when(request('areaRemtiente1', false), function ($q, $areaRemtiente) {
            return $q->where('area_remitente', $areaRemtiente);
        });
       $query->when(request('procedencia'), function ($q, $procedencia) {
            return $q->where('area_remitente', 'LIKE', "%$procedencia%");
        });
       $query->when(request('contenido'), function ($q, $contenido) {
            return $q->where('contenido', 'LIKE', "%$contenido%");
        });
       $query->when(request('docto_n'), function ($q, $docto_n) {
            return $q->where('docto_n', 'LIKE', "%$docto_n%");
        });

        $listado = $query->get()->sortByDesc(function($q){
     return $q->created_at;
     });
        // $listado = $query->get();
        $listado=$listado->groupby('tramite_id');


      $recorrer = 0;
      foreach ($listado as $indice => $registro) {
        $tramites[$recorrer] = IngresoTramite::where('tramite_id' , $indice)->first();
        $recorrer = $recorrer + 1;
      }

        //dd($tramites);

        //toma en cuenta el tipo de reporte seleccionado
        Excel::create("TramiteReportes", function ($excel)  use($tramites ) {
        $excel->setTitle("TramiteReportes");
        $excel->sheet("tramites", function ($sheet) use($tramites ) {
        $sheet->loadView('tramites.ex_excel')->with('tramites', $tramites);
        });
        })->download('xlsx');

            return back();

    }


    public function index2(Request $request){


    //  dd($request);
       set_time_limit(14200);
       $selectA = $this->cargarExcelDeparatamentos();
       $tiposDocumentos = $this->tiposDocuemntosJsonOpa();
       $anno = Carbon::now()->format('Y');
       $fechaStringInicio = ($anno).'-01-01 00:00:00';
       $feDesde = Carbon::parse($fechaStringInicio);
       $feHasta =  Carbon::parse(Carbon::now()->format('Y-m-d').' 23:59:59');
       $fechaInDesde = Carbon::parse($fechaStringInicio);
       $fechaInHasta = Carbon::now();
       $fechas[1] = Date('Y-m-d', strtotime( '1970-01-01' ));
       $fechas[0] = Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
       $tramites = [];
       $requestNRegistro = null;
       $origenView ='';
       $docdigitalView = '';
       $areRemitenteView = '';
       $areDestitenteView = '';
       $tiposDocumentosView = '';
       $contenidoView='';
       $docto_nView='';
       $origenController='';
       $procedenciaView ='';
       $mostrar = false;
       $arrayEtapasDatos = [];

           if( $request->FechaDesde != null ) {
             $feDesde=(Carbon::parse($request->get('FechaDesde'))->format('dmY'));
             $feDesde= Carbon::parse(substr($feDesde, 0, 2).'-'.substr($feDesde, 2, 2).'-'.substr($feDesde, 4, 4).' 00:00:00');
           }
           if( $request->FechaHasta != null ) {
              $feHasta=(Carbon::parse($request->get('FechaHasta'))->format('dmY'));
              $feHasta= Carbon::parse(substr($feHasta, 0, 2).'-'.substr($feHasta, 2, 2).'-'.substr($feHasta, 4, 4).' 23:59:59');
           }
           if($request->origen == '' ) {
                 $origenController =  null;
           }else{
             $origenController = $request->origen;
           }
           if($request->docdigital == '' ) {
                 $docdigitalController =  null;
           }else{
             $docdigitalController = $request->docdigital;
           }

           $storeProcedium = 'call derivacion_documental()';
           $tramites = DB::connection('mysqlSimple')->select($storeProcedium);


       $query = TmpTramiteDerivacion::query ();
       $query->where('origen', '!=' , null);

       $query->when(request('nRegistro', false), function ($q, $numRegistro) {
             return $q->where('tramite_id', $numRegistro);
         });
       $query->when($feDesde, function ($q, $feDesde ) {
             return $q->where('created_at','>=', $feDesde);
         });

         $query->when($feHasta, function ($q, $feHasta ) {
             return $q->where('created_at','<=', $feHasta);
         });
        $query->when($origenController, function ($q, $origen) {
             return $q->where('origen', $origen);
         });
        $query->when($docdigitalController, function ($q, $docdigital) {
             return $q->where('docdigital', $docdigital);
         });
        $query->when(request('tiposDocumentos', false), function ($q, $tiposDocumentos) {
             return $q->where('documento', $tiposDocumentos)->orWhere('documento2', $tiposDocumentos);
         });
        $query->when(request('areaRemtiente', false), function ($q, $areaRemtiente) {
             return $q->where('subarea_origen', $areaRemtiente);
         });
        $query->when(request('procedencia'), function ($q, $procedencia) {
             return $q->where('subarea_origen_ext', 'LIKE', "%$procedencia%");
         });
        $query->when(request('contenido'), function ($q, $contenido) {
             return $q->where('contenido_origen', 'LIKE', "%$contenido%");
         });
        $query->when(request('docto_n'), function ($q, $docto_n) {
             return $q->where('n_docto', 'LIKE', "%$docto_n%");
         });

       $listado = $query->get()->sortByDesc(function($q){
      return $q->created_at;
    });

          foreach ($listado as $clave => $reg) {

            $reg->nombres_solicitante =  str_replace( '&Ntilde;', 'Ñ' , $reg->nombres_solicitante);
            $reg->nombres_solicitante =  str_replace( '&ntilde;', 'ñ' , $reg->nombres_solicitante);
            $reg->nombres_solicitante =  str_replace( '&Aacute;', 'Á' , $reg->nombres_solicitante);
            $reg->nombres_solicitante =  str_replace( '&Eacute;', 'É' , $reg->nombres_solicitante);
            $reg->nombres_solicitante =  str_replace( '&Iacute;', 'Í' , $reg->nombres_solicitante);
            $reg->nombres_solicitante =  str_replace( '&Oacute;', 'Ó' , $reg->nombres_solicitante);
            $reg->nombres_solicitante =  str_replace( '&Uacute;', 'Ú' , $reg->nombres_solicitante);
            $reg->nombres_solicitante =  str_replace( '&eacute;', 'é' , $reg->nombres_solicitante);
            $reg->nombres_solicitante =  str_replace( '&aacute;', 'á' , $reg->nombres_solicitante);
            $reg->nombres_solicitante =  str_replace( '&iacute;', 'í' , $reg->nombres_solicitante);
            $reg->nombres_solicitante =  str_replace( '&oacute;', 'ó' , $reg->nombres_solicitante);
            $reg->nombres_solicitante =  str_replace( '&uacute;', 'ú' , $reg->nombres_solicitante);

            $reg->n_registro =  str_replace( '&Ntilde;', 'Ñ' , $reg->n_registro);
            $reg->n_registro =  str_replace( '&ntilde;', 'ñ' , $reg->n_registro);
            $reg->n_registro =  str_replace( '&Aacute;', 'Á' , $reg->n_registro);
            $reg->n_registro =  str_replace( '&Eacute;', 'É' , $reg->n_registro);
            $reg->n_registro =  str_replace( '&Iacute;', 'Í' , $reg->n_registro);
            $reg->n_registro =  str_replace( '&Oacute;', 'Ó' , $reg->n_registro);
            $reg->n_registro =  str_replace( '&Uacute;', 'Ú' , $reg->n_registro);
            $reg->n_registro =  str_replace( '&eacute;', 'é' , $reg->n_registro);
            $reg->n_registro =  str_replace( '&aacute;', 'á' , $reg->n_registro);
            $reg->n_registro =  str_replace( '&iacute;', 'í' , $reg->n_registro);
            $reg->n_registro =  str_replace( '&oacute;', 'ó' , $reg->n_registro);
            $reg->n_registro =  str_replace( '&uacute;', 'ú' , $reg->n_registro);
            $reg->n_registro =  str_replace( '\u00E9', 'é' , $reg->n_registro);
            $reg->n_registro =  str_replace( '\u00E1', 'á' , $reg->n_registro);
            $reg->n_registro =  str_replace( '\u00ED', 'í' , $reg->n_registro);
            $reg->n_registro =  str_replace( '\u00F3', 'ó' , $reg->n_registro);
            $reg->n_registro =  str_replace( '\u00FA', 'ú' , $reg->n_registro);
			      $reg->n_registro =  str_replace( '\u00C9', 'É' , $reg->n_registro);
            $reg->n_registro =  str_replace( '\u00C1', 'Á' , $reg->n_registro);
            $reg->n_registro =  str_replace( '\u00CD', 'Í' , $reg->n_registro);
            $reg->n_registro =  str_replace( '\u00D3', 'Ó' , $reg->n_registro);
            $reg->n_registro =  str_replace( '\u00DA', 'Ú' , $reg->n_registro);
			      $reg->n_registro =  str_replace( '\u00D1', 'Ñ' , $reg->n_registro);
            $reg->n_registro =  str_replace( '\u00F1', 'ñ' , $reg->n_registro);
            $reg->n_registro =  str_replace( '\u00b0', '°' , $reg->n_registro);

            $reg->n_memo_e =  str_replace( '&Ntilde;', 'Ñ' , $reg->n_memo_e);
            $reg->n_memo_e =  str_replace( '&ntilde;', 'ñ' , $reg->n_memo_e);
            $reg->n_memo_e =  str_replace( '&Aacute;', 'Á' , $reg->n_memo_e);
            $reg->n_memo_e =  str_replace( '&Eacute;', 'É' , $reg->n_memo_e);
            $reg->n_memo_e =  str_replace( '&Iacute;', 'Í' , $reg->n_memo_e);
            $reg->n_memo_e =  str_replace( '&Oacute;', 'Ó' , $reg->n_memo_e);
            $reg->n_memo_e =  str_replace( '&Uacute;', 'Ú' , $reg->n_memo_e);
            $reg->n_memo_e =  str_replace( '&eacute;', 'é' , $reg->n_memo_e);
            $reg->n_memo_e =  str_replace( '&aacute;', 'á' , $reg->n_memo_e);
            $reg->n_memo_e =  str_replace( '&iacute;', 'í' , $reg->n_memo_e);
            $reg->n_memo_e =  str_replace( '&oacute;', 'ó' , $reg->n_memo_e);
            $reg->n_memo_e =  str_replace( '&uacute;', 'ú' , $reg->n_memo_e);
            $reg->n_memo_e =  str_replace( '\\u00E1', 'á' , $reg->n_memo_e);
            $reg->n_memo_e =  str_replace( '\u00e1', 'á' , $reg->n_memo_e);

            $reg->n_oficio_e =  str_replace( '&Ntilde;', 'Ñ' , $reg->n_oficio_e);
            $reg->n_oficio_e =  str_replace( '&ntilde;', 'ñ' , $reg->n_oficio_e);
            $reg->n_oficio_e =  str_replace( '&Aacute;', 'Á' , $reg->n_oficio_e);
            $reg->n_oficio_e =  str_replace( '&Eacute;', 'É' , $reg->n_oficio_e);
            $reg->n_oficio_e =  str_replace( '&Iacute;', 'Í' , $reg->n_oficio_e);
            $reg->n_oficio_e =  str_replace( '&Oacute;', 'Ó' , $reg->n_oficio_e);
            $reg->n_oficio_e =  str_replace( '&Uacute;', 'Ú' , $reg->n_oficio_e);
            $reg->n_oficio_e =  str_replace( '&eacute;', 'é' , $reg->n_oficio_e);
            $reg->n_oficio_e =  str_replace( '&aacute;', 'á' , $reg->n_oficio_e);
            $reg->n_oficio_e =  str_replace( '&iacute;', 'í' , $reg->n_oficio_e);
            $reg->n_oficio_e =  str_replace( '&oacute;', 'ó' , $reg->n_oficio_e);
            $reg->n_oficio_e =  str_replace( '&uacute;', 'ú' , $reg->n_oficio_e);
            $reg->n_oficio_e =  str_replace( '\\u00E1', 'á' , $reg->n_oficio_e);
            $reg->n_oficio_e =  str_replace( '\u00e1', 'á' , $reg->n_oficio_e);

            $reg->n_derivacion_d =  str_replace( '&Ntilde;', 'Ñ' , $reg->n_derivacion_d);
            $reg->n_derivacion_d =  str_replace( '&ntilde;', 'ñ' , $reg->n_derivacion_d);
            $reg->n_derivacion_d =  str_replace( '&Aacute;', 'Á' , $reg->n_derivacion_d);
            $reg->n_derivacion_d =  str_replace( '&Eacute;', 'É' , $reg->n_derivacion_d);
            $reg->n_derivacion_d =  str_replace( '&Iacute;', 'Í' , $reg->n_derivacion_d);
            $reg->n_derivacion_d =  str_replace( '&Oacute;', 'Ó' , $reg->n_derivacion_d);
            $reg->n_derivacion_d =  str_replace( '&Uacute;', 'Ú' , $reg->n_derivacion_d);
            $reg->n_derivacion_d =  str_replace( '&eacute;', 'é' , $reg->n_derivacion_d);
            $reg->n_derivacion_d =  str_replace( '&aacute;', 'á' , $reg->n_derivacion_d);
            $reg->n_derivacion_d =  str_replace( '&iacute;', 'í' , $reg->n_derivacion_d);
            $reg->n_derivacion_d =  str_replace( '&oacute;', 'ó' , $reg->n_derivacion_d);
            $reg->n_derivacion_d =  str_replace( '&uacute;', 'ú' , $reg->n_derivacion_d);
            $reg->n_derivacion_d =  str_replace( '\\u00E1', 'á' , $reg->n_derivacion_d);
            $reg->n_derivacion_d =  str_replace( '\u00e1', 'á' , $reg->n_derivacion_d);

            $reg->remitente =  str_replace( '&Ntilde;', 'Ñ' , $reg->remitente);
            $reg->remitente =  str_replace( '&ntilde;', 'ñ' , $reg->remitente);
            $reg->remitente =  str_replace( '&Aacute;', 'Á' , $reg->remitente);
            $reg->remitente =  str_replace( '&Eacute;', 'É' , $reg->remitente);
            $reg->remitente =  str_replace( '&Iacute;', 'Í' , $reg->remitente);
            $reg->remitente =  str_replace( '&Oacute;', 'Ó' , $reg->remitente);
            $reg->remitente =  str_replace( '&Uacute;', 'Ú' , $reg->remitente);
            $reg->remitente =  str_replace( '&eacute;', 'é' , $reg->remitente);
            $reg->remitente =  str_replace( '&aacute;', 'á' , $reg->remitente);
            $reg->remitente =  str_replace( '&iacute;', 'í' , $reg->remitente);
            $reg->remitente =  str_replace( '&oacute;', 'ó' , $reg->remitente);
            $reg->remitente =  str_replace( '&uacute;', 'ú' , $reg->remitente);

            $reg->remitente_ext =  str_replace( '&Ntilde;', 'Ñ' , $reg->remitente_ext);
            $reg->remitente_ext =  str_replace( '&ntilde;', 'ñ' , $reg->remitente_ext);
            $reg->remitente_ext =  str_replace( '&Aacute;', 'Á' , $reg->remitente_ext);
            $reg->remitente_ext =  str_replace( '&Eacute;', 'É' , $reg->remitente_ext);
            $reg->remitente_ext =  str_replace( '&Iacute;', 'Í' , $reg->remitente_ext);
            $reg->remitente_ext =  str_replace( '&Oacute;', 'Ó' , $reg->remitente_ext);
            $reg->remitente_ext =  str_replace( '&Uacute;', 'Ú' , $reg->remitente_ext);
            $reg->remitente_ext =  str_replace( '&eacute;', 'é' , $reg->remitente_ext);
            $reg->remitente_ext =  str_replace( '&aacute;', 'á' , $reg->remitente_ext);
            $reg->remitente_ext =  str_replace( '&iacute;', 'í' , $reg->remitente_ext);
            $reg->remitente_ext =  str_replace( '&oacute;', 'ó' , $reg->remitente_ext);
            $reg->remitente_ext =  str_replace( '&uacute;', 'ú' , $reg->remitente_ext);

            $reg->cargo =  str_replace( '&Ntilde;', 'Ñ' , $reg->cargo);
            $reg->cargo =  str_replace( '&ntilde;', 'ñ' , $reg->cargo);
            $reg->cargo =  str_replace( '&Aacute;', 'Á' , $reg->cargo);
            $reg->cargo =  str_replace( '&Eacute;', 'É' , $reg->cargo);
            $reg->cargo =  str_replace( '&Iacute;', 'Í' , $reg->cargo);
            $reg->cargo =  str_replace( '&Oacute;', 'Ó' , $reg->cargo);
            $reg->cargo =  str_replace( '&Uacute;', 'Ú' , $reg->cargo);
            $reg->cargo =  str_replace( '&eacute;', 'é' , $reg->cargo);
            $reg->cargo =  str_replace( '&aacute;', 'á' , $reg->cargo);
            $reg->cargo =  str_replace( '&iacute;', 'í' , $reg->cargo);
            $reg->cargo =  str_replace( '&oacute;', 'ó' , $reg->cargo);
            $reg->cargo =  str_replace( '&uacute;', 'ú' , $reg->cargo);

            $reg->cargo_ext =  str_replace( '&Ntilde;', 'Ñ' , $reg->cargo_ext);
            $reg->cargo_ext =  str_replace( '&ntilde;', 'ñ' , $reg->cargo_ext);
            $reg->cargo_ext =  str_replace( '&Aacute;', 'Á' , $reg->cargo_ext);
            $reg->cargo_ext =  str_replace( '&Eacute;', 'É' , $reg->cargo_ext);
            $reg->cargo_ext =  str_replace( '&Iacute;', 'Í' , $reg->cargo_ext);
            $reg->cargo_ext =  str_replace( '&Oacute;', 'Ó' , $reg->cargo_ext);
            $reg->cargo_ext =  str_replace( '&Uacute;', 'Ú' , $reg->cargo_ext);
            $reg->cargo_ext =  str_replace( '&eacute;', 'é' , $reg->cargo_ext);
            $reg->cargo_ext =  str_replace( '&aacute;', 'á' , $reg->cargo_ext);
            $reg->cargo_ext =  str_replace( '&iacute;', 'í' , $reg->cargo_ext);
            $reg->cargo_ext =  str_replace( '&oacute;', 'ó' , $reg->cargo_ext);
            $reg->cargo_ext =  str_replace( '&uacute;', 'ú' , $reg->cargo_ext);

            $reg->subarea_origen_ext =  str_replace( '&Ntilde;', 'Ñ' , $reg->subarea_origen_ext);
            $reg->subarea_origen_ext =  str_replace( '&ntilde;', 'ñ' , $reg->subarea_origen_ext);
            $reg->subarea_origen_ext =  str_replace( '&Aacute;', 'Á' , $reg->subarea_origen_ext);
            $reg->subarea_origen_ext =  str_replace( '&Eacute;', 'É' , $reg->subarea_origen_ext);
            $reg->subarea_origen_ext =  str_replace( '&Iacute;', 'Í' , $reg->subarea_origen_ext);
            $reg->subarea_origen_ext =  str_replace( '&Oacute;', 'Ó' , $reg->subarea_origen_ext);
            $reg->subarea_origen_ext =  str_replace( '&Uacute;', 'Ú' , $reg->subarea_origen_ext);
            $reg->subarea_origen_ext =  str_replace( '&eacute;', 'é' , $reg->subarea_origen_ext);
            $reg->subarea_origen_ext =  str_replace( '&aacute;', 'á' , $reg->subarea_origen_ext);
            $reg->subarea_origen_ext =  str_replace( '&iacute;', 'í' , $reg->subarea_origen_ext);
            $reg->subarea_origen_ext =  str_replace( '&oacute;', 'ó' , $reg->subarea_origen_ext);
            $reg->subarea_origen_ext =  str_replace( '&uacute;', 'ú' , $reg->subarea_origen_ext);

            $reg->documento =  str_replace( '&Ntilde;', 'Ñ' , $reg->documento);
            $reg->documento =  str_replace( '&ntilde;', 'ñ' , $reg->documento);
            $reg->documento =  str_replace( '&Aacute;', 'Á' , $reg->documento);
            $reg->documento =  str_replace( '&Eacute;', 'É' , $reg->documento);
            $reg->documento =  str_replace( '&Iacute;', 'Í' , $reg->documento);
            $reg->documento =  str_replace( '&Oacute;', 'Ó' , $reg->documento);
            $reg->documento =  str_replace( '&Uacute;', 'Ú' , $reg->documento);
            $reg->documento =  str_replace( '&eacute;', 'é' , $reg->documento);
            $reg->documento =  str_replace( '&aacute;', 'á' , $reg->documento);
            $reg->documento =  str_replace( '&iacute;', 'í' , $reg->documento);
            $reg->documento =  str_replace( '&oacute;', 'ó' , $reg->documento);
            $reg->documento =  str_replace( '&uacute;', 'ú' , $reg->documento);
            $reg->documento =  str_replace( '\u00E9', 'é' , $reg->documento);
            $reg->documento =  str_replace( '\u00E1', 'á' , $reg->documento);
            $reg->documento =  str_replace( '\u00ED', 'í' , $reg->documento);
            $reg->documento =  str_replace( '\u00f3', 'ó' , $reg->documento);
            $reg->documento =  str_replace( '\u00FA', 'ú' , $reg->documento);

            $reg->documento2 =  str_replace( '&Ntilde;', 'Ñ' , $reg->documento2);
            $reg->documento2 =  str_replace( '&ntilde;', 'ñ' , $reg->documento2);
            $reg->documento2 =  str_replace( '&Aacute;', 'Á' , $reg->documento2);
            $reg->documento2 =  str_replace( '&Eacute;', 'É' , $reg->documento2);
            $reg->documento2 =  str_replace( '&Iacute;', 'Í' , $reg->documento2);
            $reg->documento2 =  str_replace( '&Oacute;', 'Ó' , $reg->documento2);
            $reg->documento2 =  str_replace( '&Uacute;', 'Ú' , $reg->documento2);
            $reg->documento2 =  str_replace( '&eacute;', 'é' , $reg->documento2);
            $reg->documento2 =  str_replace( '&aacute;', 'á' , $reg->documento2);
            $reg->documento2 =  str_replace( '&iacute;', 'í' , $reg->documento2);
            $reg->documento2 =  str_replace( '&oacute;', 'ó' , $reg->documento2);
            $reg->documento2 =  str_replace( '&uacute;', 'ú' , $reg->documento2);
            $reg->documento2 =  str_replace( '\u00E9', 'é' , $reg->documento2);
            $reg->documento2 =  str_replace( '\u00E1', 'á' , $reg->documento2);
            $reg->documento2 =  str_replace( '\u00ED', 'í' , $reg->documento2);
            $reg->documento2 =  str_replace( '\u00f3', 'ó' , $reg->documento2);
            $reg->documento2 =  str_replace( '\u00FA', 'ú' , $reg->documento2);

            $reg->contenido_origen =  str_replace( '&Ntilde;', 'Ñ' , $reg->contenido_origen);
            $reg->contenido_origen =  str_replace( '&ntilde;', 'ñ' , $reg->contenido_origen);
            $reg->contenido_origen =  str_replace( '&Aacute;', 'Á' , $reg->contenido_origen);
            $reg->contenido_origen =  str_replace( '&Eacute;', 'É' , $reg->contenido_origen);
            $reg->contenido_origen =  str_replace( '&Iacute;', 'Í' , $reg->contenido_origen);
            $reg->contenido_origen =  str_replace( '&Oacute;', 'Ó' , $reg->contenido_origen);
            $reg->contenido_origen =  str_replace( '&Uacute;', 'Ú' , $reg->contenido_origen);
            $reg->contenido_origen =  str_replace( '&eacute;', 'é' , $reg->contenido_origen);
            $reg->contenido_origen =  str_replace( '&aacute;', 'á' , $reg->contenido_origen);
            $reg->contenido_origen =  str_replace( '&iacute;', 'í' , $reg->contenido_origen);
            $reg->contenido_origen =  str_replace( '&oacute;', 'ó' , $reg->contenido_origen);
            $reg->contenido_origen =  str_replace( '&uacute;', 'ú' , $reg->contenido_origen);
            $reg->contenido_origen =  str_replace( '\u00E9', 'é' , $reg->contenido_origen);
            $reg->contenido_origen =  str_replace( '\u00E1', 'á' , $reg->contenido_origen);
            $reg->contenido_origen =  str_replace( '\u00ED', 'í' , $reg->contenido_origen);
            $reg->contenido_origen =  str_replace( '\u00F3', 'ó' , $reg->contenido_origen);
            $reg->contenido_origen =  str_replace( '\u00FA', 'ú' , $reg->contenido_origen);
			      $reg->contenido_origen =  str_replace( '\u00C9', 'É' , $reg->contenido_origen);
            $reg->contenido_origen =  str_replace( '\u00C1', 'Á' , $reg->contenido_origen);
            $reg->contenido_origen =  str_replace( '\u00CD', 'Í' , $reg->contenido_origen);
            $reg->contenido_origen =  str_replace( '\u00D3', 'Ó' , $reg->contenido_origen);
            $reg->contenido_origen =  str_replace( '\u00DA', 'Ú' , $reg->contenido_origen);
			      $reg->contenido_origen =  str_replace( '\u00D1', 'Ñ' , $reg->contenido_origen);
            $reg->contenido_origen =  str_replace( '\u00F1', 'ñ' , $reg->contenido_origen);
            $reg->contenido_origen =  str_replace( '\u00b0', '°' , $reg->contenido_origen);

            $reg->n_docto =  str_replace( '&Ntilde;', 'Ñ' , $reg->n_docto);
            $reg->n_docto =  str_replace( '&ntilde;', 'ñ' , $reg->n_docto);
            $reg->n_docto =  str_replace( '&Aacute;', 'Á' , $reg->n_docto);
            $reg->n_docto =  str_replace( '&Eacute;', 'É' , $reg->n_docto);
            $reg->n_docto =  str_replace( '&Iacute;', 'Í' , $reg->n_docto);
            $reg->n_docto =  str_replace( '&Oacute;', 'Ó' , $reg->n_docto);
            $reg->n_docto =  str_replace( '&Uacute;', 'Ú' , $reg->n_docto);
            $reg->n_docto =  str_replace( '&eacute;', 'é' , $reg->n_docto);
            $reg->n_docto =  str_replace( '&aacute;', 'á' , $reg->n_docto);
            $reg->n_docto =  str_replace( '&iacute;', 'í' , $reg->n_docto);
            $reg->n_docto =  str_replace( '&oacute;', 'ó' , $reg->n_docto);
            $reg->n_docto =  str_replace( '&uacute;', 'ú' , $reg->n_docto);
            $reg->n_docto =  str_replace( '\u00E9', 'é' , $reg->n_docto);
            $reg->n_docto =  str_replace( '\u00E1', 'á' , $reg->n_docto);
            $reg->n_docto =  str_replace( '\u00ED', 'í' , $reg->n_docto);
            $reg->n_docto =  str_replace( '\u00F3', 'ó' , $reg->n_docto);
            $reg->n_docto =  str_replace( '\u00FA', 'ú' , $reg->n_docto);
			      $reg->n_docto =  str_replace( '\u00C9', 'É' , $reg->n_docto);
            $reg->n_docto =  str_replace( '\u00C1', 'Á' , $reg->n_docto);
            $reg->n_docto =  str_replace( '\u00CD', 'Í' , $reg->n_docto);
            $reg->n_docto =  str_replace( '\u00D3', 'Ó' , $reg->n_docto);
            $reg->n_docto =  str_replace( '\u00DA', 'Ú' , $reg->n_docto);
			      $reg->n_docto =  str_replace( '\u00D1', 'Ñ' , $reg->n_docto);
            $reg->n_docto =  str_replace( '\u00F1', 'ñ' , $reg->n_docto);
            $reg->n_docto =  str_replace( '\u00b0', '°' , $reg->n_docto);
          }

       //$listado = $query->get();

         foreach ($listado as $cu => $tramite) {
            $tramiteBDsimple =  File::where('tramite_id' , $tramite->tramite_id)->where('tipo' , 's3')->get()->max('id');
            if($tramiteBDsimple){
              $infoTramite =  File::where('id' , $tramiteBDsimple)->get()->first();
              $urlDoc = 'https://simple.mindep.cl/uploader/datos_get_s3/'.$infoTramite->id.'/'.$infoTramite->campo_id.'/'.$infoTramite->llave.'/'.$infoTramite->filename;
              $tramite->urlDoc=$urlDoc;
            }else{
                $urlDoc = null;
                $tramite->urlDoc=$urlDoc;
            }

            $tramite->etapas_array = explode(",", $tramite->etapas);
            foreach ($tramite->etapas_array as $llave5 => $valor5) {
              $temporal = DatoSeguimiento::where('etapa_id' , $valor5)->get();
              $numDerivacion = $temporal->where('nombre', 'num_derivacion')->first();
              $adjuntarDoc2 = $temporal->where('nombre', 'adjuntar_doc2')->first();
              $observacion = $temporal->where('nombre', 'observacion')->first();
              $observacionFinal = $temporal->where('nombre', 'observacion_final')->first();
              $prioridad = $temporal->where('nombre', 'prioridad')->first();
              $apiArea = $temporal->where('nombre', 'api_area')->first();
              $derivar22remitente = $temporal->where('nombre', 'derivar22remitente')->first();
              $derivar2remitente = $temporal->where('nombre', 'derivar2remitente')->first();
              $derivar2 = $temporal->where('nombre', 'derivar2')->first();
              $destinatario = $temporal->where('nombre', 'destinatario')->first();
              $remitente2 = $temporal->where('nombre', 'remitente2')->first();
              $subareaRemitente2 = $temporal->where('nombre', 'subarea_remitente2')->first();
              $subareaRemitente = $temporal->where('nombre', 'subarea_remitente')->first();
              $fechaDerivacion = $temporal->where('nombre', 'fecha_derivacion')->first();
              $nDocto = $temporal->where('nombre', 'n_docto')->first();
              $fechaCierreA = $temporal->where('nombre', 'fecha_cierre_a')->first();
              $nombresSolicitante = $temporal->where('nombre', 'nombres_solicitante')->first();
              $apellidosSolicitante = $temporal->where('nombre', 'apellidos_solicitante')->first();
              $subareaOrigen = $temporal->where('nombre', 'subarea_origen')->first();
              $subarea2 = $temporal->where('nombre', 'subarea2')->first();

              if($numDerivacion){
              $objeto['num_derivacion'] = $numDerivacion->valor;
            }else{
                $objeto['num_derivacion'] = $numDerivacion;
            }

              if($adjuntarDoc2){
              $objeto['adjuntar_doc2'] = json_decode($adjuntarDoc2->valor)->URL;
            }else{
                $objeto['adjuntar_doc2'] = $adjuntarDoc2;
            }

              if($observacion){
              $objeto['observacion'] = $observacion->valor;
            }else{
                $objeto['observacion'] = $observacion;
            }

              if($observacionFinal){
              $objeto['observacion_final'] = $observacionFinal->valor;
            }else{
                $objeto['observacion_final'] = $observacionFinal;
            }

              if($prioridad){
              $objeto['prioridad'] = $prioridad->valor;
            }else{
                $objeto['prioridad'] = $prioridad;
            }

              if($apiArea){
              $objeto['api_area'] = $apiArea->valor;
            }else{
                $objeto['api_area'] = $apiArea;
            }

              if($derivar22remitente){
              $objeto ['derivar22remitente'] = $derivar22remitente->valor;
            }else{
                $objeto['derivar22remitente'] = $derivar22remitente;
            }

              if($derivar2remitente){
              $objeto ['derivar2remitente'] = $derivar2remitente->valor;
            }else{
                $objeto['derivar2remitente'] = $derivar2remitente;
            }

              if($derivar2){
              $objeto ['derivar2'] = $derivar2->valor;
            }else{
                $objeto['derivar2'] = $derivar2;
            }

              if($destinatario){
              $objeto['destinatario'] = $destinatario->valor;
            }else{
                $objeto['destinatario'] = $destinatario;
            }

              if($remitente2){
              $objeto['remitente2'] = $remitente2->valor;
            }else{
                $objeto['remitente2'] = $remitente2;
            }

              if($subareaRemitente2){
              $objeto['subarea_remitente2'] = $subareaRemitente2->valor;
            }else{
                $objeto['subarea_remitente2'] = $subareaRemitente2;
            }

              if($subareaRemitente){
              $objeto['subarea_remitente'] = $subareaRemitente->valor;
            }else{
                $objeto['subarea_remitente'] = $subareaRemitente;
            }

              if($fechaDerivacion){
              $objeto['fecha_derivacion'] = $fechaDerivacion->valor;
            }else{
                $objeto['fecha_derivacion'] = $fechaDerivacion;
            }

              if($nDocto){
              $objeto['n_docto'] = $nDocto->valor;
            }else{
                $objeto['n_docto'] = $nDocto;
            }

              if($fechaCierreA){
              $objeto['fecha_cierre_a'] = $fechaCierreA->valor;
            }else{
                $objeto['fecha_cierre_a'] = $fechaCierreA;
            }

              if($nombresSolicitante){
              $objeto['nombres_solicitante'] = $nombresSolicitante->valor;
            }else{
                $objeto['nombres_solicitante'] = $nombresSolicitante;
            }

              if($apellidosSolicitante){
              $objeto['apellidos_solicitante'] = $apellidosSolicitante->valor;
            }else{
                $objeto['apellidos_solicitante'] = $apellidosSolicitante;
            }

              if($subareaOrigen){
              $objeto['subarea_origen'] = $subareaOrigen->valor;
            }else{
                $objeto['subarea_origen'] = $subareaOrigen;
            }

              if($subarea2){
              $objeto['subarea2'] = $subarea2->valor;
            }else{
                $objeto['subarea2'] = $subarea2;
            }


              $arrayEtapasDatos[$valor5] = (object) $objeto;
              $objeto = [];

            }
            $tramite->etapas_array_datos = $arrayEtapasDatos;
            $arrayEtapasDatos = [];
         }


       if( $request->nRegistro != null ) {
         $requestNRegistro = $request->nRegistro;
       }

       if( $request->origen != null ) {
         $origenView =  $request->origen;
       }
       if( $request->docdigital != null ) {
         $docdigitalView =  $request->docdigital;
       }
       if($request->areaRemtiente != null ) {
         $areRemitenteView =  $request->areaRemtiente;
       }
       // if($request->areaDesti != null ) {
       //   $areDestitenteView =  $request->areaDesti;
       // }
       if($request->tiposDocumentos != null ) {
         $tiposDocumentosView =  $request->tiposDocumentos;
       }
       if($request->contenido != null ) {
         $contenidoView =  $request->contenido;
       }
       if($request->docto_n != null ) {
         $docto_nView =  $request->docto_n;
       }
        if($request->procedencia != null ) {
         $procedenciaView =  $request->procedencia;
       }



       return view('tramites.tramitess')
       ->with('selectA' ,$selectA)
       ->with('origenView' , $origenView)
       ->with('docdigitalView' , $docdigitalView)
       ->with('procedenciaView' , $procedenciaView)
       ->with('contenidoView' , $contenidoView)
       ->with('docto_nView' , $docto_nView)
       ->with('areRemitenteView' , $areRemitenteView)
       //->with('areDestitenteView' , $areDestitenteView)
       ->with('tiposDocumentosView' , $tiposDocumentosView)
       ->with('tiposDocumentos' ,$tiposDocumentos)
       ->with('requestNRegistro' ,$requestNRegistro)
       ->with('feDesde' ,$feDesde)
       ->with('feHasta' ,$feHasta)
       ->with('fechaInDesde' ,$fechaInDesde)
       ->with('fechaInHasta' ,$fechaInHasta)
       ->with('fechas' ,$fechas)
       ->with('tramites' ,$tramites)
       ->with('listado' ,$listado)
       ->with('mostrar' ,$mostrar)
       ->with('arrayEtapasDatos' ,$arrayEtapasDatos);


    }

    public function reporteTramite2(Request $request) {

              $tramites = [];
              if( $request->FechaDesde != null ) {
                $feDesde=(Carbon::parse($request->get('FechaDesde'))->format('dmY'));
                $feDesde= Carbon::parse(substr($feDesde, 0, 2).'-'.substr($feDesde, 2, 2).'-'.substr($feDesde, 4, 4).' 00:00:00');
              }
              if( $request->FechaHasta != null ) {
                 $feHasta=(Carbon::parse($request->get('FechaHasta'))->format('dmY'));
                 $feHasta= Carbon::parse(substr($feHasta, 0, 2).'-'.substr($feHasta, 2, 2).'-'.substr($feHasta, 4, 4).' 23:59:59');
              }
              if($request->origen1 == '' ) {
                    $origenController =  null;
              }else{
                $origenController = $request->origen1;
              }
              if($request->docdigital == '' ) {
                    $docdigitalController =  null;
              }else{
                $docdigitalController = $request->docdigital;
              }


              $query = TmpTramiteDerivacion::query ();
              $query->where('origen', '!=' , null);

              $query->when(request('nRegistro', false), function ($q, $numRegistro) {
                    return $q->where('tramite_id', $numRegistro);
                });
              $query->when($feDesde, function ($q, $feDesde ) {
                    return $q->where('created_at','>=', $feDesde);
                });

                $query->when($feHasta, function ($q, $feHasta ) {
                    return $q->where('created_at','<=', $feHasta);
                });
               $query->when($origenController, function ($q, $origen) {
                    return $q->where('origen', $origen);
                });
               $query->when($docdigitalController, function ($q, $docdigital) {
                    return $q->where('docdigital', $docdigital);
                });
               $query->when(request('tiposDocumentos', false), function ($q, $tiposDocumentos) {
                    return $q->where('documento', $tiposDocumentos)->orWhere('documento2', $tiposDocumentos);
                });
               $query->when(request('areaRemtiente', false), function ($q, $areaRemtiente) {
                    return $q->where('subarea_origen', $areaRemtiente);
                });
            $query->when(request('procedencia'), function ($q, $procedencia) {
                    return $q->where('subarea_origen_ext', 'LIKE', "%$procedencia%");
                });
               $query->when(request('contenido'), function ($q, $contenido) {
                    return $q->where('contenido_origen', 'LIKE', "%$contenido%");
                });
               $query->when(request('docto_n'), function ($q, $docto_n) {
                    return $q->where('n_docto', 'LIKE', "%$docto_n%");
                });

              $listado = $query->get()->sortByDesc(function($q){
             return $q->created_at;
           });

           foreach ($listado as $clave => $reg) {

             $reg->nombres_solicitante =  str_replace( '&Ntilde;', 'Ñ' , $reg->nombres_solicitante);
             $reg->nombres_solicitante =  str_replace( '&ntilde;', 'ñ' , $reg->nombres_solicitante);
             $reg->nombres_solicitante =  str_replace( '&Aacute;', 'Á' , $reg->nombres_solicitante);
             $reg->nombres_solicitante =  str_replace( '&Eacute;', 'É' , $reg->nombres_solicitante);
             $reg->nombres_solicitante =  str_replace( '&Iacute;', 'Í' , $reg->nombres_solicitante);
             $reg->nombres_solicitante =  str_replace( '&Oacute;', 'Ó' , $reg->nombres_solicitante);
             $reg->nombres_solicitante =  str_replace( '&Uacute;', 'Ú' , $reg->nombres_solicitante);
             $reg->nombres_solicitante =  str_replace( '&eacute;', 'é' , $reg->nombres_solicitante);
             $reg->nombres_solicitante =  str_replace( '&aacute;', 'á' , $reg->nombres_solicitante);
             $reg->nombres_solicitante =  str_replace( '&iacute;', 'í' , $reg->nombres_solicitante);
             $reg->nombres_solicitante =  str_replace( '&oacute;', 'ó' , $reg->nombres_solicitante);
             $reg->nombres_solicitante =  str_replace( '&uacute;', 'ú' , $reg->nombres_solicitante);

             $reg->n_registro =  str_replace( '&Ntilde;', 'Ñ' , $reg->n_registro);
             $reg->n_registro =  str_replace( '&ntilde;', 'ñ' , $reg->n_registro);
             $reg->n_registro =  str_replace( '&Aacute;', 'Á' , $reg->n_registro);
             $reg->n_registro =  str_replace( '&Eacute;', 'É' , $reg->n_registro);
             $reg->n_registro =  str_replace( '&Iacute;', 'Í' , $reg->n_registro);
             $reg->n_registro =  str_replace( '&Oacute;', 'Ó' , $reg->n_registro);
             $reg->n_registro =  str_replace( '&Uacute;', 'Ú' , $reg->n_registro);
             $reg->n_registro =  str_replace( '&eacute;', 'é' , $reg->n_registro);
             $reg->n_registro =  str_replace( '&aacute;', 'á' , $reg->n_registro);
             $reg->n_registro =  str_replace( '&iacute;', 'í' , $reg->n_registro);
             $reg->n_registro =  str_replace( '&oacute;', 'ó' , $reg->n_registro);
             $reg->n_registro =  str_replace( '&uacute;', 'ú' , $reg->n_registro);
             $reg->n_registro =  str_replace( '\u00E9', 'é' , $reg->n_registro);
             $reg->n_registro =  str_replace( '\u00E1', 'á' , $reg->n_registro);
             $reg->n_registro =  str_replace( '\u00ED', 'í' , $reg->n_registro);
             $reg->n_registro =  str_replace( '\u00F3', 'ó' , $reg->n_registro);
             $reg->n_registro =  str_replace( '\u00FA', 'ú' , $reg->n_registro);
 			      $reg->n_registro =  str_replace( '\u00C9', 'É' , $reg->n_registro);
             $reg->n_registro =  str_replace( '\u00C1', 'Á' , $reg->n_registro);
             $reg->n_registro =  str_replace( '\u00CD', 'Í' , $reg->n_registro);
             $reg->n_registro =  str_replace( '\u00D3', 'Ó' , $reg->n_registro);
             $reg->n_registro =  str_replace( '\u00DA', 'Ú' , $reg->n_registro);
 			      $reg->n_registro =  str_replace( '\u00D1', 'Ñ' , $reg->n_registro);
             $reg->n_registro =  str_replace( '\u00F1', 'ñ' , $reg->n_registro);
             $reg->n_registro =  str_replace( '\u00b0', '°' , $reg->n_registro);

             $reg->n_memo_e =  str_replace( '&Ntilde;', 'Ñ' , $reg->n_memo_e);
             $reg->n_memo_e =  str_replace( '&ntilde;', 'ñ' , $reg->n_memo_e);
             $reg->n_memo_e =  str_replace( '&Aacute;', 'Á' , $reg->n_memo_e);
             $reg->n_memo_e =  str_replace( '&Eacute;', 'É' , $reg->n_memo_e);
             $reg->n_memo_e =  str_replace( '&Iacute;', 'Í' , $reg->n_memo_e);
             $reg->n_memo_e =  str_replace( '&Oacute;', 'Ó' , $reg->n_memo_e);
             $reg->n_memo_e =  str_replace( '&Uacute;', 'Ú' , $reg->n_memo_e);
             $reg->n_memo_e =  str_replace( '&eacute;', 'é' , $reg->n_memo_e);
             $reg->n_memo_e =  str_replace( '&aacute;', 'á' , $reg->n_memo_e);
             $reg->n_memo_e =  str_replace( '&iacute;', 'í' , $reg->n_memo_e);
             $reg->n_memo_e =  str_replace( '&oacute;', 'ó' , $reg->n_memo_e);
             $reg->n_memo_e =  str_replace( '&uacute;', 'ú' , $reg->n_memo_e);
             $reg->n_memo_e =  str_replace( '\\u00E1', 'á' , $reg->n_memo_e);
             $reg->n_memo_e =  str_replace( '\u00E1', 'á' , $reg->n_memo_e);
             $reg->n_memo_e =  str_replace( '\\u00e1', 'á' , $reg->n_memo_e);
             $reg->n_memo_e =  str_replace( '\u00e1', 'á' , $reg->n_memo_e);

             $reg->n_oficio_e =  str_replace( '&Ntilde;', 'Ñ' , $reg->n_oficio_e);
             $reg->n_oficio_e =  str_replace( '&ntilde;', 'ñ' , $reg->n_oficio_e);
             $reg->n_oficio_e =  str_replace( '&Aacute;', 'Á' , $reg->n_oficio_e);
             $reg->n_oficio_e =  str_replace( '&Eacute;', 'É' , $reg->n_oficio_e);
             $reg->n_oficio_e =  str_replace( '&Iacute;', 'Í' , $reg->n_oficio_e);
             $reg->n_oficio_e =  str_replace( '&Oacute;', 'Ó' , $reg->n_oficio_e);
             $reg->n_oficio_e =  str_replace( '&Uacute;', 'Ú' , $reg->n_oficio_e);
             $reg->n_oficio_e =  str_replace( '&eacute;', 'é' , $reg->n_oficio_e);
             $reg->n_oficio_e =  str_replace( '&aacute;', 'á' , $reg->n_oficio_e);
             $reg->n_oficio_e =  str_replace( '&iacute;', 'í' , $reg->n_oficio_e);
             $reg->n_oficio_e =  str_replace( '&oacute;', 'ó' , $reg->n_oficio_e);
             $reg->n_oficio_e =  str_replace( '&uacute;', 'ú' , $reg->n_oficio_e);
             $reg->n_oficio_e =  str_replace( '\\u00E1', 'á' , $reg->n_oficio_e);
             $reg->n_oficio_e =  str_replace( '\u00E1', 'á' , $reg->n_oficio_e);
             $reg->n_oficio_e =  str_replace( '\\u00e1', 'á' , $reg->n_oficio_e);
             $reg->n_oficio_e =  str_replace( '\u00e1', 'á' , $reg->n_oficio_e);

             $reg->n_derivacion_d =  str_replace( '&Ntilde;', 'Ñ' , $reg->n_derivacion_d);
             $reg->n_derivacion_d =  str_replace( '&ntilde;', 'ñ' , $reg->n_derivacion_d);
             $reg->n_derivacion_d =  str_replace( '&Aacute;', 'Á' , $reg->n_derivacion_d);
             $reg->n_derivacion_d =  str_replace( '&Eacute;', 'É' , $reg->n_derivacion_d);
             $reg->n_derivacion_d =  str_replace( '&Iacute;', 'Í' , $reg->n_derivacion_d);
             $reg->n_derivacion_d =  str_replace( '&Oacute;', 'Ó' , $reg->n_derivacion_d);
             $reg->n_derivacion_d =  str_replace( '&Uacute;', 'Ú' , $reg->n_derivacion_d);
             $reg->n_derivacion_d =  str_replace( '&eacute;', 'é' , $reg->n_derivacion_d);
             $reg->n_derivacion_d =  str_replace( '&aacute;', 'á' , $reg->n_derivacion_d);
             $reg->n_derivacion_d =  str_replace( '&iacute;', 'í' , $reg->n_derivacion_d);
             $reg->n_derivacion_d =  str_replace( '&oacute;', 'ó' , $reg->n_derivacion_d);
             $reg->n_derivacion_d =  str_replace( '&uacute;', 'ú' , $reg->n_derivacion_d);
             $reg->n_derivacion_d =  str_replace( '\\u00E1', 'á' , $reg->n_derivacion_d);
             $reg->n_derivacion_d =  str_replace( '\u00E1', 'á' , $reg->n_derivacion_d);
             $reg->n_derivacion_d =  str_replace( '\\u00e1', 'á' , $reg->n_derivacion_d);
             $reg->n_derivacion_d =  str_replace( '\u00e1', 'á' , $reg->n_derivacion_d);

             $reg->remitente =  str_replace( '&Ntilde;', 'Ñ' , $reg->remitente);
             $reg->remitente =  str_replace( '&ntilde;', 'ñ' , $reg->remitente);
             $reg->remitente =  str_replace( '&Aacute;', 'Á' , $reg->remitente);
             $reg->remitente =  str_replace( '&Eacute;', 'É' , $reg->remitente);
             $reg->remitente =  str_replace( '&Iacute;', 'Í' , $reg->remitente);
             $reg->remitente =  str_replace( '&Oacute;', 'Ó' , $reg->remitente);
             $reg->remitente =  str_replace( '&Uacute;', 'Ú' , $reg->remitente);
             $reg->remitente =  str_replace( '&eacute;', 'é' , $reg->remitente);
             $reg->remitente =  str_replace( '&aacute;', 'á' , $reg->remitente);
             $reg->remitente =  str_replace( '&iacute;', 'í' , $reg->remitente);
             $reg->remitente =  str_replace( '&oacute;', 'ó' , $reg->remitente);
             $reg->remitente =  str_replace( '&uacute;', 'ú' , $reg->remitente);

             $reg->remitente_ext =  str_replace( '&Ntilde;', 'Ñ' , $reg->remitente_ext);
             $reg->remitente_ext =  str_replace( '&ntilde;', 'ñ' , $reg->remitente_ext);
             $reg->remitente_ext =  str_replace( '&Aacute;', 'Á' , $reg->remitente_ext);
             $reg->remitente_ext =  str_replace( '&Eacute;', 'É' , $reg->remitente_ext);
             $reg->remitente_ext =  str_replace( '&Iacute;', 'Í' , $reg->remitente_ext);
             $reg->remitente_ext =  str_replace( '&Oacute;', 'Ó' , $reg->remitente_ext);
             $reg->remitente_ext =  str_replace( '&Uacute;', 'Ú' , $reg->remitente_ext);
             $reg->remitente_ext =  str_replace( '&eacute;', 'é' , $reg->remitente_ext);
             $reg->remitente_ext =  str_replace( '&aacute;', 'á' , $reg->remitente_ext);
             $reg->remitente_ext =  str_replace( '&iacute;', 'í' , $reg->remitente_ext);
             $reg->remitente_ext =  str_replace( '&oacute;', 'ó' , $reg->remitente_ext);
             $reg->remitente_ext =  str_replace( '&uacute;', 'ú' , $reg->remitente_ext);

             $reg->cargo =  str_replace( '&Ntilde;', 'Ñ' , $reg->cargo);
             $reg->cargo =  str_replace( '&ntilde;', 'ñ' , $reg->cargo);
             $reg->cargo =  str_replace( '&Aacute;', 'Á' , $reg->cargo);
             $reg->cargo =  str_replace( '&Eacute;', 'É' , $reg->cargo);
             $reg->cargo =  str_replace( '&Iacute;', 'Í' , $reg->cargo);
             $reg->cargo =  str_replace( '&Oacute;', 'Ó' , $reg->cargo);
             $reg->cargo =  str_replace( '&Uacute;', 'Ú' , $reg->cargo);
             $reg->cargo =  str_replace( '&eacute;', 'é' , $reg->cargo);
             $reg->cargo =  str_replace( '&aacute;', 'á' , $reg->cargo);
             $reg->cargo =  str_replace( '&iacute;', 'í' , $reg->cargo);
             $reg->cargo =  str_replace( '&oacute;', 'ó' , $reg->cargo);
             $reg->cargo =  str_replace( '&uacute;', 'ú' , $reg->cargo);

             $reg->cargo_ext =  str_replace( '&Ntilde;', 'Ñ' , $reg->cargo_ext);
             $reg->cargo_ext =  str_replace( '&ntilde;', 'ñ' , $reg->cargo_ext);
             $reg->cargo_ext =  str_replace( '&Aacute;', 'Á' , $reg->cargo_ext);
             $reg->cargo_ext =  str_replace( '&Eacute;', 'É' , $reg->cargo_ext);
             $reg->cargo_ext =  str_replace( '&Iacute;', 'Í' , $reg->cargo_ext);
             $reg->cargo_ext =  str_replace( '&Oacute;', 'Ó' , $reg->cargo_ext);
             $reg->cargo_ext =  str_replace( '&Uacute;', 'Ú' , $reg->cargo_ext);
             $reg->cargo_ext =  str_replace( '&eacute;', 'é' , $reg->cargo_ext);
             $reg->cargo_ext =  str_replace( '&aacute;', 'á' , $reg->cargo_ext);
             $reg->cargo_ext =  str_replace( '&iacute;', 'í' , $reg->cargo_ext);
             $reg->cargo_ext =  str_replace( '&oacute;', 'ó' , $reg->cargo_ext);
             $reg->cargo_ext =  str_replace( '&uacute;', 'ú' , $reg->cargo_ext);

             $reg->subarea_origen_ext =  str_replace( '&Ntilde;', 'Ñ' , $reg->subarea_origen_ext);
             $reg->subarea_origen_ext =  str_replace( '&ntilde;', 'ñ' , $reg->subarea_origen_ext);
             $reg->subarea_origen_ext =  str_replace( '&Aacute;', 'Á' , $reg->subarea_origen_ext);
             $reg->subarea_origen_ext =  str_replace( '&Eacute;', 'É' , $reg->subarea_origen_ext);
             $reg->subarea_origen_ext =  str_replace( '&Iacute;', 'Í' , $reg->subarea_origen_ext);
             $reg->subarea_origen_ext =  str_replace( '&Oacute;', 'Ó' , $reg->subarea_origen_ext);
             $reg->subarea_origen_ext =  str_replace( '&Uacute;', 'Ú' , $reg->subarea_origen_ext);
             $reg->subarea_origen_ext =  str_replace( '&eacute;', 'é' , $reg->subarea_origen_ext);
             $reg->subarea_origen_ext =  str_replace( '&aacute;', 'á' , $reg->subarea_origen_ext);
             $reg->subarea_origen_ext =  str_replace( '&iacute;', 'í' , $reg->subarea_origen_ext);
             $reg->subarea_origen_ext =  str_replace( '&oacute;', 'ó' , $reg->subarea_origen_ext);
             $reg->subarea_origen_ext =  str_replace( '&uacute;', 'ú' , $reg->subarea_origen_ext);

             $reg->documento =  str_replace( '&Ntilde;', 'Ñ' , $reg->documento);
             $reg->documento =  str_replace( '&ntilde;', 'ñ' , $reg->documento);
             $reg->documento =  str_replace( '&Aacute;', 'Á' , $reg->documento);
             $reg->documento =  str_replace( '&Eacute;', 'É' , $reg->documento);
             $reg->documento =  str_replace( '&Iacute;', 'Í' , $reg->documento);
             $reg->documento =  str_replace( '&Oacute;', 'Ó' , $reg->documento);
             $reg->documento =  str_replace( '&Uacute;', 'Ú' , $reg->documento);
             $reg->documento =  str_replace( '&eacute;', 'é' , $reg->documento);
             $reg->documento =  str_replace( '&aacute;', 'á' , $reg->documento);
             $reg->documento =  str_replace( '&iacute;', 'í' , $reg->documento);
             $reg->documento =  str_replace( '&oacute;', 'ó' , $reg->documento);
             $reg->documento =  str_replace( '&uacute;', 'ú' , $reg->documento);
             $reg->documento =  str_replace( '\u00E9', 'é' , $reg->documento);
             $reg->documento =  str_replace( '\u00E1', 'á' , $reg->documento);
             $reg->documento =  str_replace( '\u00ED', 'í' , $reg->documento);
             $reg->documento =  str_replace( '\u00f3', 'ó' , $reg->documento);
             $reg->documento =  str_replace( '\u00FA', 'ú' , $reg->documento);

             $reg->documento2 =  str_replace( '&Ntilde;', 'Ñ' , $reg->documento2);
             $reg->documento2 =  str_replace( '&ntilde;', 'ñ' , $reg->documento2);
             $reg->documento2 =  str_replace( '&Aacute;', 'Á' , $reg->documento2);
             $reg->documento2 =  str_replace( '&Eacute;', 'É' , $reg->documento2);
             $reg->documento2 =  str_replace( '&Iacute;', 'Í' , $reg->documento2);
             $reg->documento2 =  str_replace( '&Oacute;', 'Ó' , $reg->documento2);
             $reg->documento2 =  str_replace( '&Uacute;', 'Ú' , $reg->documento2);
             $reg->documento2 =  str_replace( '&eacute;', 'é' , $reg->documento2);
             $reg->documento2 =  str_replace( '&aacute;', 'á' , $reg->documento2);
             $reg->documento2 =  str_replace( '&iacute;', 'í' , $reg->documento2);
             $reg->documento2 =  str_replace( '&oacute;', 'ó' , $reg->documento2);
             $reg->documento2 =  str_replace( '&uacute;', 'ú' , $reg->documento2);
             $reg->documento2 =  str_replace( '\u00E9', 'é' , $reg->documento2);
             $reg->documento2 =  str_replace( '\u00E1', 'á' , $reg->documento2);
             $reg->documento2 =  str_replace( '\u00ED', 'í' , $reg->documento2);
             $reg->documento2 =  str_replace( '\u00f3', 'ó' , $reg->documento2);
             $reg->documento2 =  str_replace( '\u00FA', 'ú' , $reg->documento2);

             $reg->contenido_origen =  str_replace( '&Ntilde;', 'Ñ' , $reg->contenido_origen);
             $reg->contenido_origen =  str_replace( '&ntilde;', 'ñ' , $reg->contenido_origen);
             $reg->contenido_origen =  str_replace( '&Aacute;', 'Á' , $reg->contenido_origen);
             $reg->contenido_origen =  str_replace( '&Eacute;', 'É' , $reg->contenido_origen);
             $reg->contenido_origen =  str_replace( '&Iacute;', 'Í' , $reg->contenido_origen);
             $reg->contenido_origen =  str_replace( '&Oacute;', 'Ó' , $reg->contenido_origen);
             $reg->contenido_origen =  str_replace( '&Uacute;', 'Ú' , $reg->contenido_origen);
             $reg->contenido_origen =  str_replace( '&eacute;', 'é' , $reg->contenido_origen);
             $reg->contenido_origen =  str_replace( '&aacute;', 'á' , $reg->contenido_origen);
             $reg->contenido_origen =  str_replace( '&iacute;', 'í' , $reg->contenido_origen);
             $reg->contenido_origen =  str_replace( '&oacute;', 'ó' , $reg->contenido_origen);
             $reg->contenido_origen =  str_replace( '&uacute;', 'ú' , $reg->contenido_origen);
             $reg->contenido_origen =  str_replace( '\u00E9', 'é' , $reg->contenido_origen);
             $reg->contenido_origen =  str_replace( '\u00E1', 'á' , $reg->contenido_origen);
             $reg->contenido_origen =  str_replace( '\u00ED', 'í' , $reg->contenido_origen);
             $reg->contenido_origen =  str_replace( '\u00F3', 'ó' , $reg->contenido_origen);
             $reg->contenido_origen =  str_replace( '\u00FA', 'ú' , $reg->contenido_origen);
 			      $reg->contenido_origen =  str_replace( '\u00C9', 'É' , $reg->contenido_origen);
             $reg->contenido_origen =  str_replace( '\u00C1', 'Á' , $reg->contenido_origen);
             $reg->contenido_origen =  str_replace( '\u00CD', 'Í' , $reg->contenido_origen);
             $reg->contenido_origen =  str_replace( '\u00D3', 'Ó' , $reg->contenido_origen);
             $reg->contenido_origen =  str_replace( '\u00DA', 'Ú' , $reg->contenido_origen);
 			      $reg->contenido_origen =  str_replace( '\u00D1', 'Ñ' , $reg->contenido_origen);
             $reg->contenido_origen =  str_replace( '\u00F1', 'ñ' , $reg->contenido_origen);
             $reg->contenido_origen =  str_replace( '\u00b0', '°' , $reg->contenido_origen);

             $reg->n_docto =  str_replace( '&Ntilde;', 'Ñ' , $reg->n_docto);
             $reg->n_docto =  str_replace( '&ntilde;', 'ñ' , $reg->n_docto);
             $reg->n_docto =  str_replace( '&Aacute;', 'Á' , $reg->n_docto);
             $reg->n_docto =  str_replace( '&Eacute;', 'É' , $reg->n_docto);
             $reg->n_docto =  str_replace( '&Iacute;', 'Í' , $reg->n_docto);
             $reg->n_docto =  str_replace( '&Oacute;', 'Ó' , $reg->n_docto);
             $reg->n_docto =  str_replace( '&Uacute;', 'Ú' , $reg->n_docto);
             $reg->n_docto =  str_replace( '&eacute;', 'é' , $reg->n_docto);
             $reg->n_docto =  str_replace( '&aacute;', 'á' , $reg->n_docto);
             $reg->n_docto =  str_replace( '&iacute;', 'í' , $reg->n_docto);
             $reg->n_docto =  str_replace( '&oacute;', 'ó' , $reg->n_docto);
             $reg->n_docto =  str_replace( '&uacute;', 'ú' , $reg->n_docto);
             $reg->n_docto =  str_replace( '\u00E9', 'é' , $reg->n_docto);
             $reg->n_docto =  str_replace( '\u00E1', 'á' , $reg->n_docto);
             $reg->n_docto =  str_replace( '\u00ED', 'í' , $reg->n_docto);
             $reg->n_docto =  str_replace( '\u00F3', 'ó' , $reg->n_docto);
             $reg->n_docto =  str_replace( '\u00FA', 'ú' , $reg->n_docto);
 			      $reg->n_docto =  str_replace( '\u00C9', 'É' , $reg->n_docto);
             $reg->n_docto =  str_replace( '\u00C1', 'Á' , $reg->n_docto);
             $reg->n_docto =  str_replace( '\u00CD', 'Í' , $reg->n_docto);
             $reg->n_docto =  str_replace( '\u00D3', 'Ó' , $reg->n_docto);
             $reg->n_docto =  str_replace( '\u00DA', 'Ú' , $reg->n_docto);
 			      $reg->n_docto =  str_replace( '\u00D1', 'Ñ' , $reg->n_docto);
             $reg->n_docto =  str_replace( '\u00F1', 'ñ' , $reg->n_docto);
             $reg->n_docto =  str_replace( '\u00b0', '°' , $reg->n_docto);
           }


            // $listado = $query->get();
            $listado=$listado->groupby('tramite_id');


          $recorrer = 0;
          foreach ($listado as $indice => $registro) {
            $tramites[$recorrer] = TmpTramiteDerivacion::where('tramite_id' , $indice)->first();
            $recorrer = $recorrer + 1;
          }

            //dd($tramites);

            //toma en cuenta el tipo de reporte seleccionado
            Excel::create("TramiteReportes2", function ($excel)  use($tramites ) {
            $excel->setTitle("TramiteReportes2");
            $excel->sheet("tramites", function ($sheet) use($tramites ) {
            $sheet->loadView('tramites.ex_excel2')->with('tramites', $tramites);
            });
            })->download('xlsx');

                return back();

        }


}
