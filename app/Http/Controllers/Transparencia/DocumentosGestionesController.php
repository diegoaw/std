<?php

namespace App\Http\Controllers\Transparencia;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Transparencia\DocumentosGestiones;
use App\Models\Transparencia\Docs\ActosPublicadosEnDiarioOficial;
use App\Models\Transparencia\Docs\ActosResolucionesEfectosSobreTerceros;
use App\Models\Transparencia\Docs\AntecedentesAfectenEmpresasMenorTamano;
use App\Models\Transparencia\Docs\AuditoriasEjercicioPresupuestario;
use App\Models\Transparencia\Docs\ConsejoConsultivo;
use App\Models\Transparencia\Docs\EntidadesParticipacionOrganismo;
use App\Models\Transparencia\Docs\EscalaRemuneraciones;
use App\Models\Transparencia\Docs\FacultadesFuncionesAtribuciones;
use App\Models\Transparencia\Docs\MarcoNormativo;
use App\Models\Transparencia\Docs\MecanismosParticipacionCiudadana;
use App\Models\Transparencia\Docs\NominaBeneficiarios;
use App\Models\Transparencia\Docs\OtrasComprasAdquisiciones;
use App\Models\Transparencia\Docs\OtrasTransferencias;
use App\Models\Transparencia\Docs\PersonalCodigoDelTrabajo;
use App\Models\Transparencia\Docs\PersonalContrata;
use App\Models\Transparencia\Docs\PersonalPlanta;
use App\Models\Transparencia\Docs\PersonasContratosHonorarios;
use App\Models\Transparencia\Docs\PotestadesCompetenciasFacultades;
use App\Models\Transparencia\Docs\SubsidiosBeneficiosIntermediario;
use App\Models\Transparencia\Docs\SubsidiosBeneficiosPropios;
use App\Models\Transparencia\Docs\TramitesAnteOrganismo;
use App\Models\Transparencia\DetalleTiposGestiones;
use App\Models\Transparencia\TiposGestiones;
use App\Models\Transparencia\FechasPeriodos;
use App\Models\Transparencia\UsuariosTiposGestiones;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;


class DocumentosGestionesController extends Controller
{

    public function index(Request $request)
    {
        $habititarcrear = false;
        $detalleTipoGestionAsignados = UsuariosTiposGestiones::select('detalle_tipos_gestiones_id')->where('user_id' , Auth::user()->id)->get();
        $detalleTipoGestionAsignados= array_column($detalleTipoGestionAsignados->toArray(), 'detalle_tipos_gestiones_id');
        $detalleTipoGestionAsignados= DetalleTiposGestiones::select('tipos_gestiones_id')->whereIn('id' , $detalleTipoGestionAsignados)->get();
        $detalleTipoGestionAsignados= array_column($detalleTipoGestionAsignados->toArray(), 'tipos_gestiones_id');

        if(count($detalleTipoGestionAsignados)>0){
            $habititarcrear = true;
        }
       $dataDefinitiva = DocumentosGestiones::orderby('id' , 'desc')->get();
       
       return view('transparencia.documentos_gestiones')
       ->with('habititarcrear' , $habititarcrear)
      ->with('dataDefinitiva' , $dataDefinitiva);
    }
    public function SeleccionGestion (){
      
        //$detalleTipoGestionAsignados = UsuariosTiposGestiones::select('detalle_tipos_gestiones_id')->where('user_id' , Auth::user()->id)->get();
        //$detalleTipoGestionAsignados= array_column($detalleTipoGestionAsignados->toArray(), 'detalle_tipos_gestiones_id');
        //$detalleTipoGestionAsignados= DetalleTiposGestiones::select('tipos_gestiones_id')->whereIn('id' , $detalleTipoGestionAsignados)->get();
        //$detalleTipoGestionAsignados= array_column($detalleTipoGestionAsignados->toArray(), 'tipos_gestiones_id');
        //$selectTipoGestion[null] = 'Seleccione Tipo Gestión'; foreach (TiposGestiones::whereIn('id' , $detalleTipoGestionAsignados)->orderby('id', 'desc')->get() as $key0 => $value0) { $selectTipoGestion[$value0->id]=$value0->nombre;}
        $selectTipoGestion[null] = 'Seleccione Tipo Gestión'; foreach (TiposGestiones::orderby('id')->get() as $key0 => $value0) { $selectTipoGestion[$value0->id]=$value0->nombre;}
        //$selectAno = [null=>'Seleccione Año' , '2020'=> 2020, '2021'=> 2021, '2022'=> 2022];
        //$selectMes = [null=>'Seleccione Mes' , '1'=> 'ENERO', '2'=> 'FEBRERO', '3'=> 'MARZO', '4'=> 'ABRIL', '5'=> 'MAYO', '6'=> 'JUNIO', '7'=> 'JULIO', '8'=> 'AGOSTO', '9'=> 'SEPTIEMBRE', '10'=> 'OCTUBRE', '11'=> 'NOVIEMBRE', '12'=> 'DICIEMBRE'];
        
        return view('transparencia.documentos_gestiones_crear')
        ->with('selectTipoGestion' , $selectTipoGestion);
       // ->with('selectAno' , $selectAno)
        //->with('selectMes' , $selectMes);

    }

     public function create(Request $request){
        $gestionSeleccionada = $request->tipos_gestiones_id;
        $selectTipoGestion[null] = 'Seleccione Tipo Gestión'; foreach (TiposGestiones::orderby('id', 'desc')->get() as $key0 => $value0) { $selectTipoGestion[$value0->id]=$value0->nombre;}
        $selectAno = [null=>'Seleccione Año' , '2020'=> 2020, '2021'=> 2021, '2022'=> 2022];
        $selectMes = [null=>'Seleccione Mes' , '1'=> 'ENERO', '2'=> 'FEBRERO', '3'=> 'MARZO', '4'=> 'ABRIL', '5'=> 'MAYO', '6'=> 'JUNIO', '7'=> 'JULIO', '8'=> 'AGOSTO', '9'=> 'SEPTIEMBBRE', '10'=> 'OCTUBRE', '11'=> 'NOVIEMBRE', '12'=> 'DICIEMBRE'];
        $selectRegion = [null=>'Seleccione Región', 'Región de Antofagasta'=> 'Región de Antofagasta', 'Región de Arica y Parinacota'=> 'Región de Arica y Parinacota', 'Región de Atacama'=> 'Región de Atacama', 'Región de Aysén'=> 'Región de Aysén', 'Región de Coquimbo'=> 'Región de Coquimbo', 'Región de La Araucanía'=> 'Región de La Araucanía', 'Región de Los Lagos'=> 'Región de Los Lagos', 'Región de Los Ríos'=> 'Región de Los Ríos', 'Región de Magallanes'=> 'Región de Magallanes', 'Región de Tarapacá'=> 'Región de Tarapacá', 'Región de Valparaíso'=> 'Región de Valparaíso', 'Región del Biobío'=> 'Región del Biobío', 'Región del Libertador General Bernardo O’Higgins'=> 'Región del Libertador General Bernardo O’Higgins', 'Región del Maule'=> 'Región del Maule', 'Región del Ñuble'=> 'Región del Ñuble', 'Región Metropolitana'=> 'Región Metropolitana'];
        $fechas[1] = Date('Y-m-d', strtotime( '2020-01-01' ));
        $fechas[0] = Date('Y-m-d', strtotime('2022-12-31'));
        $fe_desde =Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
        $fe_hasta =Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
        return view('transparencia.documentos_gestiones_include_formulario')
        ->with('selectTipoGestion' , $selectTipoGestion)
        ->with('gestionSeleccionada' , $gestionSeleccionada)
        ->with('selectAno' , $selectAno)
        ->with('selectMes' , $selectMes)
        ->with('selectRegion' , $selectRegion)
        ->with('fechas' , $fechas)
        ->with('fe_hasta' , $fe_hasta)
        ->with('fe_desde' , $fe_desde);
     }
     public function store (Request $request)
    {
        $tipoGestionGuardar = $request->tipos_gestiones_id;
        if($tipoGestionGuardar == 1){
            $this->GuardadoGestionId1($request);
        }
        if($tipoGestionGuardar == 2){
            $this->GuardadoGestionId2($request);
        }
        if($tipoGestionGuardar == 3){
            $this->GuardadoGestionId3($request);
        }
        if($tipoGestionGuardar == 4){
            $this->GuardadoGestionId4($request);
        }
        if($tipoGestionGuardar == 5){
            $this->GuardadoGestionId5($request);
        }
        if($tipoGestionGuardar == 6){
            $this->GuardadoGestionId6($request);
        }
        if($tipoGestionGuardar == 7){
            $this->GuardadoGestionId7($request);
        }
        if($tipoGestionGuardar == 8){
            $this->GuardadoGestionId8($request);
        }
        if($tipoGestionGuardar == 9){
            $this->GuardadoGestionId9($request);
        }
        if($tipoGestionGuardar == 10){
            $this->GuardadoGestionId10($request);
        }
        if($tipoGestionGuardar == 11){
            $this->GuardadoGestionId11($request);
        }
        if($tipoGestionGuardar == 12){
            $this->GuardadoGestionId12($request);
        }
        if($tipoGestionGuardar == 13){
            $this->GuardadoGestionId13($request);
        }
        if($tipoGestionGuardar == 14){
            $this->GuardadoGestionId14($request);
        }
        if($tipoGestionGuardar == 15){
            $this->GuardadoGestionId15($request);
        }
        if($tipoGestionGuardar == 16){
            $this->GuardadoGestionId16($request);
        }
        if($tipoGestionGuardar == 17){
            $this->GuardadoGestionId17($request);
        }
        if($tipoGestionGuardar == 18){
            $this->GuardadoGestionId18($request);
        }
        if($tipoGestionGuardar == 19){
            $this->GuardadoGestionId19($request);
        }
        if($tipoGestionGuardar == 20){
            $this->GuardadoGestionId20($request);
        }
        if($tipoGestionGuardar == 21){
            $this->GuardadoGestionId21($request);
        }
        
        //dd($request->tipos_gestiones_id);
        /*$nombreDocumento = $request->nombre_documento_original;
        $tipoGestion = $request->tipos_gestiones_id;
        $ano = $request->ano;
        $mes = $request->mes;
        $notaGestor = $request->nota_generador;
        $nuevoDocGest = new DocumentosGestiones();
        $nuevoDocGest->tipos_gestiones_id = $tipoGestion;
        $nuevoDocGest->ano = $ano;
        $nuevoDocGest->mes = $mes;
        $nuevoDocGest->nombre_documento_original = $nombreDocumento;
        $nuevoDocGest->nota_generador = $notaGestor;
        $nuevoDocGest->save();*/
        alert()->success('!','Documento guardado exitosamente');
        return redirect()->route('transparencia_documentos_gestiones');
    }
    public function GuardadoGestionId1($request){
        
        $ano = $request->ano;
        $mes = $request->mes;
        $tipoNorma = $request->tipo_norma;
        $numerNorma = $request->numero_norma;
        $denominacion = $request->denominacion;
        $fechaPublicacion = $request->fecha_publicacion;
        $enlace1 = $request->enlace1;
        $enlace2 = $request->enlace2;
        $FechaModificacion = $request->fecha_modificacion;
        $notaGenerador = $request->nota_generador;
        $mesActual  = date('n');
        $anoActual = date("Y");
        $idFechaPeriodo =  FechasPeriodos::where('ano' , $anoActual)->where('mes' , $mesActual)->where('periodos_id' , '1')->first()->id;
        $idDetalleTiposGestiones = DetalleTiposGestiones::where('nombre' , 'Actos Publicados en Diario Oficial Generar  Documento')->first()->id;
        $idTiposGestiones = TiposGestiones::where('nombre' , 'Actos Publicados en Diario Oficial')->first()->id;     
        $userId = Auth::user()->id;   
        $nuevoDocGest = new ActosPublicadosEnDiarioOficial();
        $nuevoDocGest->ano = $ano;
        $nuevoDocGest->mes = $mes;
        $nuevoDocGest->tipo_norma = $tipoNorma;
        $nuevoDocGest->numero_norma = $numerNorma;
        $nuevoDocGest->denominacion = $denominacion;
        $nuevoDocGest->fecha_publicacion =Date('Ymd', strtotime( $request->fechaPublicacion ));
        $nuevoDocGest->enlace1 = $enlace1;
        $nuevoDocGest->enlace2 = $enlace2;       
        $nuevoDocGest->fecha_modificacion =Date('Ymd', strtotime( $request->FechaModificacion ));
        $nuevoDocGest->nota_generador = $notaGenerador;
        $nuevoDocGest->fechas_periodos_id = $idFechaPeriodo;
        $nuevoDocGest->detalle_tipos_gestiones_id =  $idDetalleTiposGestiones;
        $nuevoDocGest->tipos_gestiones_id =  $idTiposGestiones;
        $nuevoDocGest->user_id =  $userId;
        $nuevoDocGest->save();
        alert()->success('!','Documento guardado exitosamente');
        return redirect()->route('transparencia_documentos_gestiones');
    }

    public function GuardadoGestionId2($request){
        
        $ano = $request->ano;
        $mes = $request->mes;
        $tipoMarcoNormativo = $request->tipo_marco_normativo;
        $tipoNorma = $request->tipo_norma;
        $numerNorma = $request->numero_norma;
        $denominacion = $request->denominacion;
        $fechaPublicacion = $request->fecha_publicacion;
        $enlace1 = $request->enlace1;
        $enlace2 = $request->enlace2;
        $FechaModificacion = $request->fecha_modificacion;
        $notaGenerador = $request->nota_generador;
        $mesActual  = date('n');
        $anoActual = date("Y");
        $idFechaPeriodo =  FechasPeriodos::where('ano' , $anoActual)->where('mes' , $mesActual)->where('periodos_id' , '1')->first()->id;
        $idDetalleTiposGestiones = DetalleTiposGestiones::where('nombre' , 'Marco Normativo Generar  Documento')->first()->id;
        $idTiposGestiones = TiposGestiones::where('nombre' , 'Marco Normativo')->first()->id;     
        $userId = Auth::user()->id;   
        $nuevoDocGest = new MarcoNormativo();
        $nuevoDocGest->ano = $ano;
        $nuevoDocGest->mes = $mes;
        $nuevoDocGest->tipo_marco_normativo = $tipoMarcoNormativo;
        $nuevoDocGest->tipo_norma = $tipoNorma;
        $nuevoDocGest->numero_norma = $numerNorma;
        $nuevoDocGest->denominacion = $denominacion;
        $nuevoDocGest->fecha_publicacion =Date('Ymd', strtotime( $request->fechaPublicacion ));
        $nuevoDocGest->enlace1 = $enlace1;
        $nuevoDocGest->enlace2 = $enlace2;       
        $nuevoDocGest->fecha_modificacion =Date('Ymd', strtotime( $request->FechaModificacion ));
        $nuevoDocGest->nota_generador = $notaGenerador;
        $nuevoDocGest->fechas_periodos_id = $idFechaPeriodo;
        $nuevoDocGest->detalle_tipos_gestiones_id =  $idDetalleTiposGestiones;
        $nuevoDocGest->tipos_gestiones_id =  $idTiposGestiones;
        $nuevoDocGest->user_id =  $userId;
        $nuevoDocGest->save();
        alert()->success('!','Documento guardado exitosamente');
        return redirect()->route('transparencia_documentos_gestiones');
    }

    public function GuardadoGestionId3($request){
        
        $ano = $request->ano;
        $mes = $request->mes;
        $denominacion = $request->denominacion;
        $potestades = $request->potestades;
        $fuenteLegal = $request->fuente_legal;
        $enlace = $request->enlace;
        $notaGenerador = $request->nota_generador;
        $mesActual  = date('n');
        $anoActual = date("Y");
        $idFechaPeriodo =  FechasPeriodos::where('ano' , $anoActual)->where('mes' , $mesActual)->where('periodos_id' , '1')->first()->id;
        $idDetalleTiposGestiones = DetalleTiposGestiones::where('nombre' , 'Potestades Competencias Facultades Generar  Documento')->first()->id;
        $idTiposGestiones = TiposGestiones::where('nombre' , 'Potestades Competencias Facultades')->first()->id;     
        $userId = Auth::user()->id;   
        $nuevoDocGest = new PotestadesCompetenciasFacultades();
        $nuevoDocGest->ano = $ano;
        $nuevoDocGest->mes = $mes;
        $nuevoDocGest->denominacion = $denominacion;
        $nuevoDocGest->potestades = $potestades;
        $nuevoDocGest->fuente_legal = $fuenteLegal;        
        $nuevoDocGest->enlace = $enlace;
        $nuevoDocGest->nota_generador = $notaGenerador;
        $nuevoDocGest->fechas_periodos_id = $idFechaPeriodo;
        $nuevoDocGest->detalle_tipos_gestiones_id =  $idDetalleTiposGestiones;
        $nuevoDocGest->tipos_gestiones_id =  $idTiposGestiones;
        $nuevoDocGest->user_id =  $userId;
        $nuevoDocGest->save();
        alert()->success('!','Documento guardado exitosamente');
        return redirect()->route('transparencia_documentos_gestiones');
    }

    public function GuardadoGestionId4($request){
        
        $ano = $request->ano;
        $mes = $request->mes;
        $unidadOrgano = $request->unidad_organo;
        $funcionesFalcutades = $request->funciones_falcutades;
        $fuenteLegal = $request->fuente_legal;
        $fechaPublicacion = $request->fecha_publicacion;
        $enlace1 = $request->enlace1;
        $enlace2 = $request->enlace2;
        $FechaModificacion = $request->fecha_modificacion;
        $notaGenerador = $request->nota_generador;
        $mesActual  = date('n');
        $anoActual = date("Y");
        $idFechaPeriodo =  FechasPeriodos::where('ano' , $anoActual)->where('mes' , $mesActual)->where('periodos_id' , '1')->first()->id;
        $idDetalleTiposGestiones = DetalleTiposGestiones::where('nombre' , 'Facultades Funciones Atribuciones Generar  Documento')->first()->id;
        $idTiposGestiones = TiposGestiones::where('nombre' , 'Facultades Funciones Atribuciones')->first()->id;     
        $userId = Auth::user()->id;   
        $nuevoDocGest = new FacultadesFuncionesAtribuciones();
        $nuevoDocGest->ano = $ano;
        $nuevoDocGest->mes = $mes;
        $nuevoDocGest->unidad_organo = $unidadOrgano;
        $nuevoDocGest->funciones_falcutades = $funcionesFalcutades;
        $nuevoDocGest->fuente_legal = $fuenteLegal;
        $nuevoDocGest->fecha_publicacion =Date('Ymd', strtotime( $request->fechaPublicacion ));
        $nuevoDocGest->enlace1 = $enlace1;
        $nuevoDocGest->enlace2 = $enlace2;       
        $nuevoDocGest->fecha_modificacion =Date('Ymd', strtotime( $request->FechaModificacion ));
        $nuevoDocGest->nota_generador = $notaGenerador;
        $nuevoDocGest->fechas_periodos_id = $idFechaPeriodo;
        $nuevoDocGest->detalle_tipos_gestiones_id =  $idDetalleTiposGestiones;
        $nuevoDocGest->tipos_gestiones_id =  $idTiposGestiones;
        $nuevoDocGest->user_id =  $userId;
        $nuevoDocGest->save();
        alert()->success('!','Documento guardado exitosamente');
        return redirect()->route('transparencia_documentos_gestiones');
    }

    public function GuardadoGestionId5($request){
        
        $ano = $request->ano;
        $mes = $request->mes;
        $estamento = $request->estamento;
        $grado = $request->grado;
        $unidadMonetaria = $request->unidad_monetaria;
        $sueldoBase = $request->sueldo_base;
        $asignacion = $request->asignacion;
        $remuneracion = $request->remuneracion;
        $notaGenerador = $request->nota_generador;
        $mesActual  = date('n');
        $anoActual = date("Y");
        $idFechaPeriodo =  FechasPeriodos::where('ano' , $anoActual)->where('mes' , $mesActual)->where('periodos_id' , '1')->first()->id;
        $idDetalleTiposGestiones = DetalleTiposGestiones::where('nombre' , 'Escala Remuneraciones Generar  Documento')->first()->id;
        $idTiposGestiones = TiposGestiones::where('nombre' , 'Escala Remuneraciones')->first()->id;     
        $userId = Auth::user()->id;   
        $nuevoDocGest = new EscalaRemuneraciones();
        $nuevoDocGest->ano = $ano;
        $nuevoDocGest->mes = $mes;
        $nuevoDocGest->estamento = $estamento;
        $nuevoDocGest->grado = $grado;
        $nuevoDocGest->unidad_monetaria = $unidadMonetaria;
        $nuevoDocGest->sueldo_base = $sueldoBase;
        $nuevoDocGest->asignacion = $asignacion;
        $nuevoDocGest->remuneracion = $remuneracion;
        $nuevoDocGest->nota_generador = $notaGenerador;
        $nuevoDocGest->fechas_periodos_id = $idFechaPeriodo;
        $nuevoDocGest->detalle_tipos_gestiones_id =  $idDetalleTiposGestiones;
        $nuevoDocGest->tipos_gestiones_id =  $idTiposGestiones;
        $nuevoDocGest->user_id =  $userId;
        $nuevoDocGest->save();
        alert()->success('!','Documento guardado exitosamente');
        return redirect()->route('transparencia_documentos_gestiones');
    }

    public function GuardadoGestionId6($request){
        
        $ano = $request->ano;
        $mes = $request->mes;
        $tipoPersonal = $request->tipo_personal;
        $apellidoPaterno = $request->apellido_paterno;
        $apellidoMaterno = $request->apellido_materno;
        $nombres = $request->nombres;
        $grado = $request->grado;   
        $calificacionProfesional = $request->calificacion_profesional;
        $cargo = $request->cargo;
        $region = $request->region;
        $asignacionesEspeciales = $request->asignaciones_especiales;
        $unidadMonetaria = $request->unidad_monetaria;
        $remuneracionBruta = $request->remuneracion_bruta;
        $remuneracionLiquida = $request->remuneracion_liquida;
        $horasExtraordinarias = $request->horas_extraordinarias;
        $nHorasDiurnas = $request->n_horas_diurnas;
        $remuneracionHorasDiurnas = $request->remuneracion_horas_diurnas;
        $nHorasNocturnas = $request->n_horas_nocturnas;
        $remuneracionHorasNocturnas = $request->remuneracion_horas_nocturnas;
        $nHorasFestivas = $request->n_horas_festivas;
        $remuneracionHorasFestiva = $request->remuneracion_horas_festivas;
        $FechaInicio = $request->fecha_inicio;
        $FechaTermino = $request->fecha_termino;
        $observaciones = $request->observaciones;
        $declaracionPatrimonio = $request->declaracion_patrimonio;
        $declaracionIntereses = $request->declaracion_intereses;
        $viaticos = $request->viaticos;
        $notaGenerador = $request->nota_generador;
        $mesActual  = date('n');
        $anoActual = date("Y");
        $idFechaPeriodo =  FechasPeriodos::where('ano' , $anoActual)->where('mes' , $mesActual)->where('periodos_id' , '1')->first()->id;
        $idDetalleTiposGestiones = DetalleTiposGestiones::where('nombre' , 'Personal Código del Trabajo Generar  Documento')->first()->id;
        $idTiposGestiones = TiposGestiones::where('nombre' , 'Personal Código del Trabajo')->first()->id;     
        $userId = Auth::user()->id;   
        $nuevoDocGest = new PersonalCodigoDelTrabajo();
        $nuevoDocGest->ano = $ano;
        $nuevoDocGest->mes = $mes;
        $nuevoDocGest->tipo_personal = $tipoPersonal;
        $nuevoDocGest->apellido_paterno = $apellidoPaterno;
        $nuevoDocGest->apellido_materno = $apellidoMaterno;
        $nuevoDocGest->nombres = $nombres;
        $nuevoDocGest->grado = $grado; 
        $nuevoDocGest->calificacion_profesional = $calificacionProfesional;
        $nuevoDocGest->cargo = $cargo; 
        $nuevoDocGest->region = $region;
        $nuevoDocGest->asignaciones_especiales = $asignacionesEspeciales;
        $nuevoDocGest->unidad_monetaria = $unidadMonetaria;
        $nuevoDocGest->remuneracion_bruta = $remuneracionBruta;
        $nuevoDocGest->remuneracion_liquida = $remuneracionLiquida;
        $nuevoDocGest->horas_extraordinarias = $horasExtraordinarias;
        $nuevoDocGest->n_horas_diurnas = $nHorasDiurnas;
        $nuevoDocGest->remuneracion_horas_diurnas = $remuneracionHorasDiurnas;
        $nuevoDocGest->n_horas_nocturnas = $nHorasNocturnas;
        $nuevoDocGest->remuneracion_horas_nocturnas = $remuneracionHorasNocturnas;
        $nuevoDocGest->n_horas_festivas = $nHorasFestivas;
        $nuevoDocGest->remuneracion_horas_festivas = $remuneracionHorasFestiva;
        $nuevoDocGest->fecha_inicio =Date('Ymd', strtotime( $request->FechaInicio ));
        $nuevoDocGest->fecha_termino =Date('Ymd', strtotime( $request->FechaTermino ));
        $nuevoDocGest->observaciones = $observaciones;
        $nuevoDocGest->declaracion_patrimonio = $declaracionPatrimonio;
        $nuevoDocGest->declaracion_intereses = $declaracionIntereses;
        $nuevoDocGest->viaticos = $viaticos;
        $nuevoDocGest->nota_generador = $notaGenerador;
        $nuevoDocGest->fechas_periodos_id = $idFechaPeriodo;
        $nuevoDocGest->detalle_tipos_gestiones_id =  $idDetalleTiposGestiones;
        $nuevoDocGest->tipos_gestiones_id =  $idTiposGestiones;
        $nuevoDocGest->user_id =  $userId;
        $nuevoDocGest->save();
        alert()->success('!','Documento guardado exitosamente');
        return redirect()->route('transparencia_documentos_gestiones');
    }

    public function GuardadoGestionId7($request){
        
        $ano = $request->ano;
        $mes = $request->mes;
        $estamento = $request->estamento;
        $tipoPersonal = $request->tipo_personal;
        $apellidoPaterno = $request->apellido_paterno;
        $apellidoMaterno = $request->apellido_materno;
        $nombres = $request->nombres;
        $grado = $request->grado;   
        $calificacionProfesional = $request->calificacion_profesional;
        $cargo = $request->cargo;
        $region = $request->region;
        $asignacionesEspeciales = $request->asignaciones_especiales;
        $unidadMonetaria = $request->unidad_monetaria;
        $remuneracionBruta = $request->remuneracion_bruta;
        $remuneracionLiquida = $request->remuneracion_liquida;
        $horasExtraordinarias = $request->horas_extraordinarias;
        $nHorasDiurnas = $request->n_horas_diurnas;
        $remuneracionHorasDiurnas = $request->remuneracion_horas_diurnas;
        $nHorasNocturnas = $request->n_horas_nocturnas;
        $remuneracionHorasNocturnas = $request->remuneracion_horas_nocturnas;
        $nHorasFestivas = $request->n_horas_festivas;
        $remuneracionHorasFestiva = $request->remuneracion_horas_festivas;
        $FechaInicio = $request->fecha_inicio;
        $FechaTermino = $request->fecha_termino;
        $observaciones = $request->observaciones;
        $declaracionPatrimonio = $request->declaracion_patrimonio;
        $declaracionIntereses = $request->declaracion_intereses;
        $viaticos = $request->viaticos;
        $notaGenerador = $request->nota_generador;
        $mesActual  = date('n');
        $anoActual = date("Y");
        $idFechaPeriodo =  FechasPeriodos::where('ano' , $anoActual)->where('mes' , $mesActual)->where('periodos_id' , '1')->first()->id;
        $idDetalleTiposGestiones = DetalleTiposGestiones::where('nombre' , 'Personal Contrata Generar  Documento')->first()->id;
        $idTiposGestiones = TiposGestiones::where('nombre' , 'Personal Contrata')->first()->id;     
        $userId = Auth::user()->id;   
        $nuevoDocGest = new PersonalContrata();
        $nuevoDocGest->ano = $ano;
        $nuevoDocGest->mes = $mes;
        $nuevoDocGest->estamento = $estamento;
        $nuevoDocGest->tipo_personal = $tipoPersonal;
        $nuevoDocGest->apellido_paterno = $apellidoPaterno;
        $nuevoDocGest->apellido_materno = $apellidoMaterno;
        $nuevoDocGest->nombres = $nombres;
        $nuevoDocGest->grado = $grado; 
        $nuevoDocGest->calificacion_profesional = $calificacionProfesional;
        $nuevoDocGest->cargo = $cargo; 
        $nuevoDocGest->region = $region;
        $nuevoDocGest->asignaciones_especiales = $asignacionesEspeciales;
        $nuevoDocGest->unidad_monetaria = $unidadMonetaria;
        $nuevoDocGest->remuneracion_bruta = $remuneracionBruta;
        $nuevoDocGest->remuneracion_liquida = $remuneracionLiquida;
        $nuevoDocGest->horas_extraordinarias = $horasExtraordinarias;
        $nuevoDocGest->n_horas_diurnas = $nHorasDiurnas;
        $nuevoDocGest->remuneracion_horas_diurnas = $remuneracionHorasDiurnas;
        $nuevoDocGest->n_horas_nocturnas = $nHorasNocturnas;
        $nuevoDocGest->remuneracion_horas_nocturnas = $remuneracionHorasNocturnas;
        $nuevoDocGest->n_horas_festivas = $nHorasFestivas;
        $nuevoDocGest->remuneracion_horas_festivas = $remuneracionHorasFestiva;
        $nuevoDocGest->fecha_inicio =Date('Ymd', strtotime( $request->FechaInicio ));
        $nuevoDocGest->fecha_termino =Date('Ymd', strtotime( $request->FechaTermino ));
        $nuevoDocGest->observaciones = $observaciones;
        $nuevoDocGest->declaracion_patrimonio = $declaracionPatrimonio;
        $nuevoDocGest->declaracion_intereses = $declaracionIntereses;
        $nuevoDocGest->viaticos = $viaticos;
        $nuevoDocGest->nota_generador = $notaGenerador;
        $nuevoDocGest->fechas_periodos_id = $idFechaPeriodo;
        $nuevoDocGest->detalle_tipos_gestiones_id =  $idDetalleTiposGestiones;
        $nuevoDocGest->tipos_gestiones_id =  $idTiposGestiones;
        $nuevoDocGest->user_id =  $userId;
        $nuevoDocGest->save();
        alert()->success('!','Documento guardado exitosamente');
        return redirect()->route('transparencia_documentos_gestiones');
    }

    public function GuardadoGestionId8($request){
        
        $ano = $request->ano;
        $mes = $request->mes;
        $estamento = $request->estamento;
        $tipoPersonal = $request->tipo_personal;
        $apellidoPaterno = $request->apellido_paterno;
        $apellidoMaterno = $request->apellido_materno;
        $nombres = $request->nombres;
        $grado = $request->grado;   
        $calificacionProfesional = $request->calificacion_profesional;
        $cargo = $request->cargo;
        $region = $request->region;
        $asignacionesEspeciales = $request->asignaciones_especiales;
        $unidadMonetaria = $request->unidad_monetaria;
        $remuneracionBruta = $request->remuneracion_bruta;
        $remuneracionLiquida = $request->remuneracion_liquida;
        $horasExtraordinarias = $request->horas_extraordinarias;
        $nHorasDiurnas = $request->n_horas_diurnas;
        $remuneracionHorasDiurnas = $request->remuneracion_horas_diurnas;
        $nHorasNocturnas = $request->n_horas_nocturnas;
        $remuneracionHorasNocturnas = $request->remuneracion_horas_nocturnas;
        $nHorasFestivas = $request->n_horas_festivas;
        $remuneracionHorasFestiva = $request->remuneracion_horas_festivas;
        $FechaInicio = $request->fecha_inicio;
        $FechaTermino = $request->fecha_termino;
        $observaciones = $request->observaciones;
        $declaracionPatrimonio = $request->declaracion_patrimonio;
        $declaracionIntereses = $request->declaracion_intereses;
        $viaticos = $request->viaticos;
        $notaGenerador = $request->nota_generador;
        $mesActual  = date('n');
        $anoActual = date("Y");
        $idFechaPeriodo =  FechasPeriodos::where('ano' , $anoActual)->where('mes' , $mesActual)->where('periodos_id' , '1')->first()->id;
        $idDetalleTiposGestiones = DetalleTiposGestiones::where('nombre' , 'Personal Planta Generar  Documento')->first()->id;
        $idTiposGestiones = TiposGestiones::where('nombre' , 'Personal Planta')->first()->id;     
        $userId = Auth::user()->id;   
        $nuevoDocGest = new PersonalPlanta();
        $nuevoDocGest->ano = $ano;
        $nuevoDocGest->mes = $mes;
        $nuevoDocGest->estamento = $estamento;
        $nuevoDocGest->tipo_personal = $tipoPersonal;
        $nuevoDocGest->apellido_paterno = $apellidoPaterno;
        $nuevoDocGest->apellido_materno = $apellidoMaterno;
        $nuevoDocGest->nombres = $nombres;
        $nuevoDocGest->grado = $grado; 
        $nuevoDocGest->calificacion_profesional = $calificacionProfesional;
        $nuevoDocGest->cargo = $cargo; 
        $nuevoDocGest->region = $region;
        $nuevoDocGest->asignaciones_especiales = $asignacionesEspeciales;
        $nuevoDocGest->unidad_monetaria = $unidadMonetaria;
        $nuevoDocGest->remuneracion_bruta = $remuneracionBruta;
        $nuevoDocGest->remuneracion_liquida = $remuneracionLiquida;
        $nuevoDocGest->horas_extraordinarias = $horasExtraordinarias;
        $nuevoDocGest->n_horas_diurnas = $nHorasDiurnas;
        $nuevoDocGest->remuneracion_horas_diurnas = $remuneracionHorasDiurnas;
        $nuevoDocGest->n_horas_nocturnas = $nHorasNocturnas;
        $nuevoDocGest->remuneracion_horas_nocturnas = $remuneracionHorasNocturnas;
        $nuevoDocGest->n_horas_festivas = $nHorasFestivas;
        $nuevoDocGest->remuneracion_horas_festivas = $remuneracionHorasFestiva;
        $nuevoDocGest->fecha_inicio =Date('Ymd', strtotime( $request->FechaInicio ));
        $nuevoDocGest->fecha_termino =Date('Ymd', strtotime( $request->FechaTermino ));
        $nuevoDocGest->observaciones = $observaciones;
        $nuevoDocGest->declaracion_patrimonio = $declaracionPatrimonio;
        $nuevoDocGest->declaracion_intereses = $declaracionIntereses;
        $nuevoDocGest->viaticos = $viaticos;
        $nuevoDocGest->nota_generador = $notaGenerador;
        $nuevoDocGest->fechas_periodos_id = $idFechaPeriodo;
        $nuevoDocGest->detalle_tipos_gestiones_id =  $idDetalleTiposGestiones;
        $nuevoDocGest->tipos_gestiones_id =  $idTiposGestiones;
        $nuevoDocGest->user_id =  $userId;
        $nuevoDocGest->save();
        alert()->success('!','Documento guardado exitosamente');
        return redirect()->route('transparencia_documentos_gestiones');
    }

    public function GuardadoGestionId9($request){
        
        $ano = $request->ano;
        $mes = $request->mes;
        $apellidoPaterno = $request->apellido_paterno;
        $apellidoMaterno = $request->apellido_materno;
        $nombres = $request->nombres;
        $grado = $request->grado;   
        $descripcionFuncion = $request->descripcion_funcion;
        $calificacionProfesional = $request->calificacion_profesional;
        $region = $request->region;
        $unidadMonetaria = $request->unidad_monetaria;
        $honorarioTotalBruto = $request->honorario_total_bruto;
        $remuneracionLiquida = $request->remuneracion_liquida;
        $pagoMensual = $request->pago_mensual;
        $FechaInicio = $request->fecha_inicio;
        $FechaTermino = $request->fecha_termino;
        $observaciones = $request->observaciones;
        $declaracionPatrimonio = $request->declaracion_patrimonio;
        $declaracionIntereses = $request->declaracion_intereses;
        $viaticos = $request->viaticos;
        $notaGenerador = $request->nota_generador;
        $mesActual  = date('n');
        $anoActual = date("Y");
        $idFechaPeriodo =  FechasPeriodos::where('ano' , $anoActual)->where('mes' , $mesActual)->where('periodos_id' , '1')->first()->id;
        $idDetalleTiposGestiones = DetalleTiposGestiones::where('nombre' , 'Personas Contratos Honorarios Generar  Documento')->first()->id;
        $idTiposGestiones = TiposGestiones::where('nombre' , 'Personas Contratos Honorarios')->first()->id;     
        $userId = Auth::user()->id;   
        $nuevoDocGest = new PersonasContratosHonorarios();
        $nuevoDocGest->ano = $ano;
        $nuevoDocGest->mes = $mes;
        $nuevoDocGest->apellido_paterno = $apellidoPaterno;
        $nuevoDocGest->apellido_materno = $apellidoMaterno;
        $nuevoDocGest->nombres = $nombres;
        $nuevoDocGest->grado = $grado; 
        $nuevoDocGest->descripcion_funcion = $descripcionFuncion; 
        $nuevoDocGest->calificacion_profesional = $calificacionProfesional;
        $nuevoDocGest->region = $region;
        $nuevoDocGest->unidad_monetaria = $unidadMonetaria;
        $nuevoDocGest->honorario_total_bruto = $honorarioTotalBruto;
        $nuevoDocGest->remuneracion_liquida = $remuneracionLiquida;
        $nuevoDocGest->pago_mensual = $pagoMensual;
        $nuevoDocGest->fecha_inicio =Date('Ymd', strtotime( $request->FechaInicio ));
        $nuevoDocGest->fecha_termino =Date('Ymd', strtotime( $request->FechaTermino ));
        $nuevoDocGest->observaciones = $observaciones;
        $nuevoDocGest->declaracion_patrimonio = $declaracionPatrimonio;
        $nuevoDocGest->declaracion_intereses = $declaracionIntereses;
        $nuevoDocGest->viaticos = $viaticos;
        $nuevoDocGest->nota_generador = $notaGenerador;
        $nuevoDocGest->fechas_periodos_id = $idFechaPeriodo;
        $nuevoDocGest->detalle_tipos_gestiones_id =  $idDetalleTiposGestiones;
        $nuevoDocGest->tipos_gestiones_id =  $idTiposGestiones;
        $nuevoDocGest->user_id =  $userId;
        $nuevoDocGest->save();
        alert()->success('!','Documento guardado exitosamente');
        return redirect()->route('transparencia_documentos_gestiones');
    }

    public function GuardadoGestionId10($request){
        
        $ano = $request->ano;
        $mes = $request->mes;
        $tipoCompra = $request->tipo_compra;
        $tipoActo = $request->tipo_acto;
        $denominacionActo = $request->denominacion_acto;
        $fechaContrato = $request->fecha_contrato;
        $nActo = $request->n_acto;
        $razonSocial = $request->razon_social;
        $nombre = $request->nombre;
        $apellidoPaterno = $request->apellido_paterno;
        $apellidoMaterno = $request->apellido_materno;
        $rut = $request->rut;
        $socioAccionista = $request->socio_accionista;
        $objetoContratacion = $request->objeto_contratacion;
        $FechaInicio = $request->fecha_inicio;
        $FechaTermino = $request->fecha_termino;
        $montoOperacion = $request->monto_operacion;
        $tipoUnidad = $request->tipo_unidad;
        $observaciones = $request->observaciones;
        $enlaceTextoContrato = $request->enlace_texto_contrato;
        $enlaceTextoActo = $request->enlace_texto_acto;
        $enlaceTextActoModificacion = $request->enlace_texto_acto_modificacion;
        $notaGenerador = $request->nota_generador;
        $mesActual  = date('n');
        $anoActual = date("Y");
        $idFechaPeriodo =  FechasPeriodos::where('ano' , $anoActual)->where('mes' , $mesActual)->where('periodos_id' , '1')->first()->id;
        $idDetalleTiposGestiones = DetalleTiposGestiones::where('nombre' , 'Otras Compras Adquisiciones Generar  Documento')->first()->id;
        $idTiposGestiones = TiposGestiones::where('nombre' , 'Otras Compras Adquisiciones')->first()->id;     
        $userId = Auth::user()->id;   
        $nuevoDocGest = new OtrasComprasAdquisiciones();
        $nuevoDocGest->ano = $ano;
        $nuevoDocGest->mes = $mes;
        $nuevoDocGest->tipo_compra = $tipoCompra;
        $nuevoDocGest->tipo_acto = $tipoActo;
        $nuevoDocGest->denominacion_acto = $denominacionActo;
        $nuevoDocGest->fecha_contrato =Date('Ymd', strtotime( $request->fechaContrato ));
        $nuevoDocGest->n_acto = $nActo;
        $nuevoDocGest->razon_social = $razonSocial;
        $nuevoDocGest->nombre = $nombre;
        $nuevoDocGest->apellido_paterno = $apellidoPaterno;
        $nuevoDocGest->apellido_materno = $apellidoMaterno;
        $nuevoDocGest->rut = $rut;
        $nuevoDocGest->socio_accionista = $socioAccionista;
        $nuevoDocGest->objeto_contratacion = $objetoContratacion;
        $nuevoDocGest->fecha_inicio =Date('Ymd', strtotime( $request->FechaInicio ));
        $nuevoDocGest->fecha_termino =Date('Ymd', strtotime( $request->FechaTermino ));
        $nuevoDocGest->monto_operacion = $montoOperacion;
        $nuevoDocGest->tipo_unidad = $tipoUnidad;
        $nuevoDocGest->observaciones = $observaciones;
        $nuevoDocGest->enlace_texto_contrato = $enlaceTextoContrato;
        $nuevoDocGest->enlace_texto_acto = $enlaceTextoActo;
        $nuevoDocGest->enlace_texto_acto_modificacion = $enlaceTextActoModificacion;
        $nuevoDocGest->nota_generador = $notaGenerador;
        $nuevoDocGest->fechas_periodos_id = $idFechaPeriodo;
        $nuevoDocGest->detalle_tipos_gestiones_id =  $idDetalleTiposGestiones;
        $nuevoDocGest->tipos_gestiones_id =  $idTiposGestiones;
        $nuevoDocGest->user_id =  $userId;
        $nuevoDocGest->save();
        alert()->success('!','Documento guardado exitosamente');
        return redirect()->route('transparencia_documentos_gestiones');
    }
    public function GuardadoGestionId11($request){
        
        $ano = $request->ano;
        $mes = $request->mes;
        $fechaTransferencia = $request->fecha_transferencia;
        $denominacionTransferencia = $request->denominacion_transferencia;
        $monto = $request->monto;
        $tipoUnidad = $request->tipo_unidad;
        $imputacion = $request->imputacion;
        $objetoTransferencia = $request->objeto_transferencia;
        $razonSocial = $request->razon_social;
        $nombre = $request->nombre;
        $apellidoPaterno = $request->apellido_paterno;
        $apellidoMaterno = $request->apellido_materno;
        $notaGenerador = $request->nota_generador;
        $mesActual  = date('n');
        $anoActual = date("Y");
        $idFechaPeriodo =  FechasPeriodos::where('ano' , $anoActual)->where('mes' , $mesActual)->where('periodos_id' , '1')->first()->id;
        $idDetalleTiposGestiones = DetalleTiposGestiones::where('nombre' , 'Otras Transferencias Generar  Documento')->first()->id;
        $idTiposGestiones = TiposGestiones::where('nombre' , 'Otras Transferencias')->first()->id;     
        $userId = Auth::user()->id;   
        $nuevoDocGest = new OtrasTransferencias();
        $nuevoDocGest->ano = $ano;
        $nuevoDocGest->mes = $mes;
        $nuevoDocGest->fecha_transferencia =Date('Ymd', strtotime( $request->fechaTransferencia ));
        $nuevoDocGest->denominacion_transferencia = $denominacionTransferencia;
        $nuevoDocGest->monto = $monto;
        $nuevoDocGest->tipo_unidad = $tipoUnidad;
        $nuevoDocGest->imputacion = $imputacion;    
        $nuevoDocGest->objeto_transferencia = $objetoTransferencia;
        $nuevoDocGest->razon_social = $razonSocial;
        $nuevoDocGest->nombre = $nombre;
        $nuevoDocGest->apellido_paterno = $apellidoPaterno;
        $nuevoDocGest->apellido_materno = $apellidoMaterno;
        $nuevoDocGest->nota_generador = $notaGenerador;
        $nuevoDocGest->fechas_periodos_id = $idFechaPeriodo;
        $nuevoDocGest->detalle_tipos_gestiones_id =  $idDetalleTiposGestiones;
        $nuevoDocGest->tipos_gestiones_id =  $idTiposGestiones;
        $nuevoDocGest->user_id =  $userId;
        $nuevoDocGest->save();
        alert()->success('!','Documento guardado exitosamente');
        return redirect()->route('transparencia_documentos_gestiones');
    }
    public function GuardadoGestionId12($request){
        
        $ano = $request->ano;
        $mes = $request->mes;
        $tipologiaActo = $request->tipologia_acto;
        $tipoActo = $request->tipo_acto;
        $denominacionActo = $request->denominacion_acto;
        $nActo = $request->n_acto;
        $fechaActo = $request->fecha_acto;
        $fechaPublicacion = $request->fecha_publicacion;
        $indicacionForma = $request->indicacion_forma;
        $efectosGenerales = $request->efectos_generales;
        $fechaActualizacion = $request->fecha_actualizacion;
        $descripcion = $request->descripcion;
        $enlace1 = $request->enlace1;
        $enlace2 = $request->enlace2;       
        $notaGenerador = $request->nota_generador;
        $mesActual  = date('n');
        $anoActual = date("Y");
        $idFechaPeriodo =  FechasPeriodos::where('ano' , $anoActual)->where('mes' , $mesActual)->where('periodos_id' , '1')->first()->id;
        $idDetalleTiposGestiones = DetalleTiposGestiones::where('nombre' , 'Actos Resoluciones Efectos Sobre Terceros Generar  Documento')->first()->id;
        $idTiposGestiones = TiposGestiones::where('nombre' , 'Actos Resoluciones Efectos Sobre Terceros')->first()->id;     
        $userId = Auth::user()->id;   
        $nuevoDocGest = new ActosResolucionesEfectosSobreTerceros();
        $nuevoDocGest->ano = $ano;
        $nuevoDocGest->mes = $mes;
        $nuevoDocGest->tipologia_acto = $tipologiaActo;
        $nuevoDocGest->tipo_acto = $tipoActo;
        $nuevoDocGest->denominacion_acto = $denominacionActo;
        $nuevoDocGest->n_acto = $nActo;
        $nuevoDocGest->fecha_acto =Date('Ymd', strtotime( $request->fechaActo ));
        $nuevoDocGest->fecha_publicacion =Date('Ymd', strtotime( $request->fechaPublicacion ));
        $nuevoDocGest->indicacion_forma = $indicacionForma;
        $nuevoDocGest->efectos_generales = $efectosGenerales;
        $nuevoDocGest->fecha_actualizacion =Date('Ymd', strtotime( $request->fechaActualizacion ));
        $nuevoDocGest->descripcion = $descripcion;
        $nuevoDocGest->enlace1 = $enlace1;
        $nuevoDocGest->enlace2 = $enlace2;             
        $nuevoDocGest->nota_generador = $notaGenerador;
        $nuevoDocGest->fechas_periodos_id = $idFechaPeriodo;
        $nuevoDocGest->detalle_tipos_gestiones_id =  $idDetalleTiposGestiones;
        $nuevoDocGest->tipos_gestiones_id =  $idTiposGestiones;
        $nuevoDocGest->user_id =  $userId;
        $nuevoDocGest->save();
        alert()->success('!','Documento guardado exitosamente');
        return redirect()->route('transparencia_documentos_gestiones');
    }
    public function GuardadoGestionId13($request){
        
        $ano = $request->ano;
        $mes = $request->mes;
        $descripcion = $request->descripcion;
        $requisitosAntecedentes = $request->requisitos_antecedentes;
        $tramitesEnlinea = $request->tramites_enlinea;
        $tramitesArealizar = $request->tramites_arealizar;
        $valor = $request->valor;
        $dondeRealizan = $request->donde_realizan;
        $informacionComplementaria = $request->informacion_complementaria;
        $notaGenerador = $request->nota_generador;
        $mesActual  = date('n');
        $anoActual = date("Y");
        $idFechaPeriodo =  FechasPeriodos::where('ano' , $anoActual)->where('mes' , $mesActual)->where('periodos_id' , '1')->first()->id;
        $idDetalleTiposGestiones = DetalleTiposGestiones::where('nombre' , 'Trámites Ante Organismo Generar  Documento')->first()->id;
        $idTiposGestiones = TiposGestiones::where('nombre' , 'Trámites Ante Organismo')->first()->id;     
        $userId = Auth::user()->id;   
        $nuevoDocGest = new TramitesAnteOrganismo();
        $nuevoDocGest->ano = $ano;
        $nuevoDocGest->mes = $mes;
        $nuevoDocGest->descripcion = $descripcion;
        $nuevoDocGest->requisitos_antecedentes = $requisitosAntecedentes;
        $nuevoDocGest->tramites_enlinea = $tramitesEnlinea;
        $nuevoDocGest->tramites_arealizar = $tramitesArealizar;
        $nuevoDocGest->valor = $valor;
        $nuevoDocGest->donde_realizan = $dondeRealizan;
        $nuevoDocGest->informacion_complementaria = $informacionComplementaria;
        $nuevoDocGest->nota_generador = $notaGenerador;
        $nuevoDocGest->fechas_periodos_id = $idFechaPeriodo;
        $nuevoDocGest->detalle_tipos_gestiones_id =  $idDetalleTiposGestiones;
        $nuevoDocGest->tipos_gestiones_id =  $idTiposGestiones;
        $nuevoDocGest->user_id =  $userId;
        $nuevoDocGest->save();
        alert()->success('!','Documento guardado exitosamente');
        return redirect()->route('transparencia_documentos_gestiones');
    }
    public function GuardadoGestionId14($request){
        
        $ano = $request->ano;
        $mes = $request->mes;
        $nombrePrograma = $request->nombre_programa;
        $fechaOtrogamiento = $request->fecha_otrogamiento;
        $tipoActo = $request->tipo_acto;
        $denominacionActo = $request->denominacion_acto;
        $fechaActo = $request->fecha_acto;
        $nActo = $request->n_acto;
        $razonSocial = $request->razon_social;
        $nombre = $request->nombre;
        $apellidoPaterno = $request->apellido_paterno;
        $apellidoMaterno = $request->apellido_materno;
        $notaGenerador = $request->nota_generador;
        $mesActual  = date('n');
        $anoActual = date("Y");
        $idFechaPeriodo =  FechasPeriodos::where('ano' , $anoActual)->where('mes' , $mesActual)->where('periodos_id' , '1')->first()->id;
        $idDetalleTiposGestiones = DetalleTiposGestiones::where('nombre' , 'Nómina Beneficiarios Generar  Documento')->first()->id;
        $idTiposGestiones = TiposGestiones::where('nombre' , 'Nómina Beneficiarios')->first()->id;     
        $userId = Auth::user()->id;   
        $nuevoDocGest = new NominaBeneficiarios();
        $nuevoDocGest->ano = $ano;
        $nuevoDocGest->mes = $mes;
        $nuevoDocGest->nombre_programa = $nombrePrograma;
        $nuevoDocGest->fecha_otrogamiento =Date('Ymd', strtotime( $request->fechaOtrogamiento ));
        $nuevoDocGest->tipo_acto = $tipoActo;
        $nuevoDocGest->denominacion_acto = $denominacionActo;
        $nuevoDocGest->fecha_acto =Date('Ymd', strtotime( $request->fechaActo ));
        $nuevoDocGest->n_acto = $nActo;
        $nuevoDocGest->razon_social = $razonSocial;
        $nuevoDocGest->nombre = $nombre;
        $nuevoDocGest->apellido_paterno = $apellidoPaterno;
        $nuevoDocGest->apellido_materno = $apellidoMaterno;
        $nuevoDocGest->nota_generador = $notaGenerador;
        $nuevoDocGest->fechas_periodos_id = $idFechaPeriodo;
        $nuevoDocGest->detalle_tipos_gestiones_id =  $idDetalleTiposGestiones;
        $nuevoDocGest->tipos_gestiones_id =  $idTiposGestiones;
        $nuevoDocGest->user_id =  $userId;
        $nuevoDocGest->save();
        alert()->success('!','Documento guardado exitosamente');
        return redirect()->route('transparencia_documentos_gestiones');
    }
    public function GuardadoGestionId15($request){
        
        $ano = $request->ano;
        $mes = $request->mes;
        $nombreSubsidio = $request->nombre_subsidio;
        $nBeneficiarios = $request->n_beneficiarios;
        $razon = $request->razon;
        $nombrePrograma = $request->nombre_programa;
        $enlace = $request->enlace;
        $notaGenerador = $request->nota_generador;
        $mesActual  = date('n');
        $anoActual = date("Y");
        $idFechaPeriodo =  FechasPeriodos::where('ano' , $anoActual)->where('mes' , $mesActual)->where('periodos_id' , '1')->first()->id;
        $idDetalleTiposGestiones = DetalleTiposGestiones::where('nombre' , 'Subsidios Beneficios Intermediario Generar  Documento')->first()->id;
        $idTiposGestiones = TiposGestiones::where('nombre' , 'Subsidios Beneficios Intermediario')->first()->id;     
        $userId = Auth::user()->id;   
        $nuevoDocGest = new SubsidiosBeneficiosIntermediario();
        $nuevoDocGest->ano = $ano;
        $nuevoDocGest->mes = $mes;
        $nuevoDocGest->nombre_subsidio = $nombreSubsidio;
        $nuevoDocGest->n_beneficiarios = $nBeneficiarios;
        $nuevoDocGest->razon = $razon;
        $nuevoDocGest->nombre_programa = $nombrePrograma;
        $nuevoDocGest->enlace = $enlace;
        $nuevoDocGest->nota_generador = $notaGenerador;
        $nuevoDocGest->fechas_periodos_id = $idFechaPeriodo;
        $nuevoDocGest->detalle_tipos_gestiones_id =  $idDetalleTiposGestiones;
        $nuevoDocGest->tipos_gestiones_id =  $idTiposGestiones;
        $nuevoDocGest->user_id =  $userId;
        $nuevoDocGest->save();
        alert()->success('!','Documento guardado exitosamente');
        return redirect()->route('transparencia_documentos_gestiones');
    }
    public function GuardadoGestionId16($request){
        
        $ano = $request->ano;
        $mes = $request->mes;
        $tipoSubsidio = $request->tipo_subsidio;
        $unidadOrganoDependencia = $request->unidad_organo_dependencia;
        $requisitosAntecedentes = $request->requisitos_antecedentes;
        $montoGlobal = $request->monto_global;
        $tipoUnidadMonetaria = $request->tipo_unidad_monetaria;
        $inicioPostulacion = $request->inicio_postulacion;
        $finPostulacion = $request->fin_postulacion;
        $criterioEvaluacion = $request->criterio_evaluacion;
        $plazosAsociados = $request->plazos_asociados;
        $objetivoSubsidio = $request->objetivo_subsidio;
        $tipo = $request->tipo;
        $denominacion = $request->denominacion;
        $numero = $request->numero;
        $fecha = $request->fecha;
        $link = $request->link_texto_integro;
        $numeroBeneficiarios = $request->numero_beneficiarios;
        $razon = $request->razon;
        $nombrePrograma = $request->nombre_programa;
        $enlace = $request->enlace;
        $notaGenerador = $request->nota_generador;
        $mesActual  = date('n');
        $anoActual = date("Y");
        $idFechaPeriodo =  FechasPeriodos::where('ano' , $anoActual)->where('mes' , $mesActual)->where('periodos_id' , '1')->first()->id;
        $idDetalleTiposGestiones = DetalleTiposGestiones::where('nombre' , 'Subsidios Beneficios Propios Generar  Documento')->first()->id;
        $idTiposGestiones = TiposGestiones::where('nombre' , 'Subsidios Beneficios Propios')->first()->id;     
        $userId = Auth::user()->id;   
        $nuevoDocGest = new SubsidiosBeneficiosPropios();
        $nuevoDocGest->ano = $ano;
        $nuevoDocGest->mes = $mes;
        $nuevoDocGest->tipo_subsidio = $tipoSubsidio;
        $nuevoDocGest->unidad_organo_dependencia = $unidadOrganoDependencia;
        $nuevoDocGest->requisitos_antecedentes = $requisitosAntecedentes;
        $nuevoDocGest->monto_global = $montoGlobal;
        $nuevoDocGest->tipo_unidad_monetaria = $tipoUnidadMonetaria;
        $nuevoDocGest->inicio_postulacion =Date('Ymd', strtotime( $request->inicioPostulacion ));
        $nuevoDocGest->fin_postulacion =Date('Ymd', strtotime( $request->finPostulacion ));
        $nuevoDocGest->criterio_evaluacion = $criterioEvaluacion;
        $nuevoDocGest->plazos_asociados = $plazosAsociados;
        $nuevoDocGest->objetivo_subsidio = $objetivoSubsidio;
        $nuevoDocGest->tipo = $tipo;
        $nuevoDocGest->denominacion = $denominacion;
        $nuevoDocGest->numero = $numero;
        $nuevoDocGest->fecha =Date('Ymd', strtotime( $request->fecha ));
        $nuevoDocGest->link_texto_integro = $link;
        $nuevoDocGest->numero_beneficiarios = $numeroBeneficiarios;
        $nuevoDocGest->razon = $razon;
        $nuevoDocGest->nombre_programa = $nombrePrograma;
        $nuevoDocGest->enlace = $enlace;
        $nuevoDocGest->nota_generador = $notaGenerador;
        $nuevoDocGest->fechas_periodos_id = $idFechaPeriodo;
        $nuevoDocGest->detalle_tipos_gestiones_id =  $idDetalleTiposGestiones;
        $nuevoDocGest->tipos_gestiones_id =  $idTiposGestiones;
        $nuevoDocGest->user_id =  $userId;
        $nuevoDocGest->save();
        alert()->success('!','Documento guardado exitosamente');
        return redirect()->route('transparencia_documentos_gestiones');
    }

    public function GuardadoGestionId17($request){

        $ano = $request->ano;
        $mes = $request->mes;
        $nombreConsejo = $request->nombre_consejo;
        $formaIntegracion = $request->forma_integracion;
        $nombreConsejero = $request->nombre_consejero;
        $apellidoPaterno = $request->apellido_paterno;
        $apellidoMaterno = $request->apellido_materno;
        $representacionCalidades = $request->representacion_calidades;
        $notaGenerador = $request->nota_generador;
        $mesActual  = date('n');
        $anoActual = date("Y");
        $idFechaPeriodo =  FechasPeriodos::where('ano' , $anoActual)->where('mes' , $mesActual)->where('periodos_id' , '1')->first()->id;
        $idDetalleTiposGestiones = DetalleTiposGestiones::where('nombre' , 'Consejo Consultivo Generar  Documento')->first()->id;
        $idTiposGestiones = TiposGestiones::where('nombre' , 'Consejo Consultivo')->first()->id;     
        $userId = Auth::user()->id;   
        $nuevoDocGest = new ConsejoConsultivo();
        $nuevoDocGest->ano = $ano;
        $nuevoDocGest->mes = $mes;
        $nuevoDocGest->nombre_consejo = $nombreConsejo;
        $nuevoDocGest->forma_integracion = $formaIntegracion;
        $nuevoDocGest->nombre_consejero = $nombreConsejero;
        $nuevoDocGest->apellido_paterno = $apellidoPaterno;
        $nuevoDocGest->apellido_materno = $apellidoMaterno;
        $nuevoDocGest->representacion_calidades = $representacionCalidades;
        $nuevoDocGest->nota_generador = $notaGenerador;
        $nuevoDocGest->fechas_periodos_id = $idFechaPeriodo;
        $nuevoDocGest->detalle_tipos_gestiones_id =  $idDetalleTiposGestiones;
        $nuevoDocGest->tipos_gestiones_id =  $idTiposGestiones;
        $nuevoDocGest->user_id =  $userId;
        $nuevoDocGest->save();
        alert()->success('!','Documento guardado exitosamente');
        return redirect()->route('transparencia_documentos_gestiones');
    }

    public function GuardadoGestionId18($request){
        
        $ano = $request->ano;
        $mes = $request->mes;
        $nombreMecanismo = $request->nombre_mecanismo;
        $descripcion = $request->descripcion;
        $requisitos = $request->requisitos;
        $enlace = $request->enlace;
        $notaGenerador = $request->nota_generador;
        $mesActual  = date('n');
        $anoActual = date("Y");
        $idFechaPeriodo =  FechasPeriodos::where('ano' , $anoActual)->where('mes' , $mesActual)->where('periodos_id' , '1')->first()->id;
        $idDetalleTiposGestiones = DetalleTiposGestiones::where('nombre' , 'Mecanismos Participación Ciudadana Generar  Documento')->first()->id;
        $idTiposGestiones = TiposGestiones::where('nombre' , 'Mecanismos Participación Ciudadana')->first()->id;     
        $userId = Auth::user()->id;   
        $nuevoDocGest = new MecanismosParticipacionCiudadana();
        $nuevoDocGest->ano = $ano;
        $nuevoDocGest->mes = $mes;
        $nuevoDocGest->nombre_mecanismo = $nombreMecanismo;
        $nuevoDocGest->descripcion = $descripcion;
        $nuevoDocGest->requisitos = $requisitos;
        $nuevoDocGest->enlace = $enlace;
        $nuevoDocGest->nota_generador = $notaGenerador;
        $nuevoDocGest->fechas_periodos_id = $idFechaPeriodo;
        $nuevoDocGest->detalle_tipos_gestiones_id =  $idDetalleTiposGestiones;
        $nuevoDocGest->tipos_gestiones_id =  $idTiposGestiones;
        $nuevoDocGest->user_id =  $userId;
        $nuevoDocGest->save();
        alert()->success('!','Documento guardado exitosamente');
        return redirect()->route('transparencia_documentos_gestiones');
    }

    public function GuardadoGestionId19($request){
 
        $ano = $request->ano;
        $mes = $request->mes;
        $TituloAuditoria = $request->titulo_auditoria;
        $entidad = $request->entidad;
        $materia = $request->materia;
        $FechaInicio = $request->fecha_inicio;
        $FechaTermino = $request->fecha_termino;
        $InicioPeriodo = $request->inicio_periodo;
        $FinPeriodo = $request->fin_periodo;
        $respuestaServicio = $request->respuesta_servicio;
        $FechaPublicacion = $request->fecha_publicacion;
        $enlace = $request->enlace;
        $notaGenerador = $request->nota_generador;
        $mesActual  = date('n');
        $anoActual = date("Y");
        $idFechaPeriodo =  FechasPeriodos::where('ano' , $anoActual)->where('mes' , $mesActual)->where('periodos_id' , '1')->first()->id;
        $idDetalleTiposGestiones = DetalleTiposGestiones::where('nombre' , 'Auditorías Ejercicio Presupuestario Generar  Documento')->first()->id;
        $idTiposGestiones = TiposGestiones::where('nombre' , 'Auditorías Ejercicio Presupuestario')->first()->id;     
        $userId = Auth::user()->id;   
        $nuevoDocGest = new AuditoriasEjercicioPresupuestario();
        $nuevoDocGest->ano = $ano;
        $nuevoDocGest->mes = $mes;
        $nuevoDocGest->titulo_auditoria = $TituloAuditoria;
        $nuevoDocGest->entidad = $entidad;
        $nuevoDocGest->materia = $materia;
        $nuevoDocGest->fecha_inicio =Date('Ymd', strtotime( $request->FechaInicio ));
        $nuevoDocGest->fecha_termino =Date('Ymd', strtotime( $request->FechaTermino ));
        $nuevoDocGest->inicio_periodo =Date('Ymd', strtotime( $request->InicioPeriodo ));
        $nuevoDocGest->fin_periodo =Date('Ymd', strtotime( $request->FinPeriodo ));
        $nuevoDocGest->respuesta_servicio = $respuestaServicio;
        $nuevoDocGest->fecha_publicacion =Date('Ymd', strtotime( $request->FechaPublicacion ));
        $nuevoDocGest->enlace = $enlace;
        $nuevoDocGest->nota_generador = $notaGenerador;
        $nuevoDocGest->fechas_periodos_id = $idFechaPeriodo;
        $nuevoDocGest->detalle_tipos_gestiones_id =  $idDetalleTiposGestiones;
        $nuevoDocGest->tipos_gestiones_id =  $idTiposGestiones;
        $nuevoDocGest->user_id =  $userId;
        $nuevoDocGest->save();
        alert()->success('!','Documento guardado exitosamente');
        return redirect()->route('transparencia_documentos_gestiones');
        
    }
    public function GuardadoGestionId20($request){
        
        $ano = $request->ano;
        $mes = $request->mes;
        $entidad = $request->entidad;
        $tipoVinculo = $request->tipo_vinculo;
        $descripcion = $request->descripcion;
        $FechaInicio = $request->fecha_inicio;
        $FechaTermino = $request->fecha_termino;
        $vinculoIndefinido = $request->vinculo_indefinido;
        $enlace = $request->enlace;
        $notaGenerador = $request->nota_generador;
        $mesActual  = date('n');
        $anoActual = date("Y");
        $idFechaPeriodo =  FechasPeriodos::where('ano' , $anoActual)->where('mes' , $mesActual)->where('periodos_id' , '1')->first()->id;
        $idDetalleTiposGestiones = DetalleTiposGestiones::where('nombre' , 'Entidades Participación Organismo Generar  Documento')->first()->id;
        $idTiposGestiones = TiposGestiones::where('nombre' , 'Entidades Participación Organismo')->first()->id;     
        $userId = Auth::user()->id;   
        $nuevoDocGest = new EntidadesParticipacionOrganismo();
        $nuevoDocGest->ano = $ano;
        $nuevoDocGest->mes = $mes;
        $nuevoDocGest->entidad = $entidad;
        $nuevoDocGest->tipo_vinculo = $tipoVinculo;
        $nuevoDocGest->descripcion = $descripcion;
        $nuevoDocGest->fecha_inicio =Date('Ymd', strtotime( $request->FechaInicio ));
        $nuevoDocGest->fecha_termino =Date('Ymd', strtotime( $request->FechaTermino ));
        $nuevoDocGest->vinculo_indefinido = $vinculoIndefinido;
        $nuevoDocGest->enlace = $enlace;
        $nuevoDocGest->nota_generador = $notaGenerador;
        $nuevoDocGest->fechas_periodos_id = $idFechaPeriodo;
        $nuevoDocGest->detalle_tipos_gestiones_id =  $idDetalleTiposGestiones;
        $nuevoDocGest->tipos_gestiones_id =  $idTiposGestiones;
        $nuevoDocGest->user_id =  $userId;
        $nuevoDocGest->save();
        alert()->success('!','Documento guardado exitosamente');
        return redirect()->route('transparencia_documentos_gestiones');
    }
    public function GuardadoGestionId21($request){
        
        $ano = $request->ano;
        $mes = $request->mes;
        $fechaPublicacion = $request->fecha_publicacion;
        $organismo = $request->organismo;
        $tituloDenominacion = $request->titulo_denominacion;
        $tipoNorma = $request->tipo_norma;
        $efectosNorma = $request->efectos_norma;
        $enlaceFormulario = $request->enlace_formulario;
        $enlace = $request->enlace;
        $notaGenerador = $request->nota_generador;
        $mesActual  = date('n');
        $anoActual = date("Y");
        $idFechaPeriodo =  FechasPeriodos::where('ano' , $anoActual)->where('mes' , $mesActual)->where('periodos_id' , '1')->first()->id;
        $idDetalleTiposGestiones = DetalleTiposGestiones::where('nombre' , 'Antecedentes Afecten Empresas Menor Tamaño Generar  Documento')->first()->id;
        $idTiposGestiones = TiposGestiones::where('nombre' , 'Antecedentes Afecten Empresas Menor Tamaño')->first()->id;     
        $userId = Auth::user()->id;   
        $nuevoDocGest = new AntecedentesAfectenEmpresasMenorTamano();
        $nuevoDocGest->ano = $ano;
        $nuevoDocGest->mes = $mes;
        $nuevoDocGest->fecha_publicacion =Date('Ymd', strtotime( $request->fechaPublicacion ));
        $nuevoDocGest->organismo = $organismo;
        $nuevoDocGest->titulo_denominacion = $tituloDenominacion;
        $nuevoDocGest->tipo_norma = $tipoNorma;
        $nuevoDocGest->efectos_norma = $efectosNorma;
        $nuevoDocGest->enlace_formulario = $enlaceFormulario;
        $nuevoDocGest->enlace = $enlace;
        $nuevoDocGest->nota_generador = $notaGenerador;
        $nuevoDocGest->fechas_periodos_id = $idFechaPeriodo;
        $nuevoDocGest->detalle_tipos_gestiones_id =  $idDetalleTiposGestiones;
        $nuevoDocGest->tipos_gestiones_id =  $idTiposGestiones;
        $nuevoDocGest->user_id =  $userId;
        $nuevoDocGest->save();
        alert()->success('!','Documento guardado exitosamente');
        return redirect()->route('transparencia_documentos_gestiones');
    }



    public function edit (Request $request , $id){
        $editDocGest = DocumentosGestiones::where('id' , $id)->first();

        $selectTipoGestion[null] = 'Seleccione Tipo Gestión'; foreach (TiposGestiones::orderby('id', 'desc')->get() as $key0 => $value0) { $selectTipoGestion[$value0->id]=$value0->nombre;}
        $selectAno = [null=>'Seleccione Año' , '2020'=> 2020, '2021'=> 2021, '2022'=> 2022];
        $selectMes = [null=>'Seleccione Mes' , '1'=> 'ENERO', '2'=> 'FEBRERO', '3'=> 'MARZO', '4'=> 'ABRIL', '5'=> 'MAYO', '6'=> 'JUNIO', '7'=> 'JULIO', '8'=> 'AGOSTO', '9'=> 'SEPTIEMBRE', '10'=> 'OCTUBRE', '11'=> 'NOVIEMBRE', '12'=> 'DICIEMBRE'];
        
        return view('transparencia.documentos_gestiones_editar')
        ->with('selectTipoGestion' , $selectTipoGestion)
        ->with('selectAno' , $selectAno)
        ->with('selectMes' , $selectMes)        
        ->with('editDocGest' , $editDocGest);
    }
    public function update(Request $request, $id)
    {
        $nombreDocumento = $request->nombre_documento_original;
        $tipoGestion = $request->tipos_gestiones_id;
        $ano = $request->ano;
        $mes = $request->mes;
        $notaGestor = $request->nota_generador;
        $editDocGest = DocumentosGestiones::where('id' , $id)->first();
        $editDocGest->tipos_gestiones_id = $tipoGestion;
        $editDocGest->ano = $ano;
        $editDocGest->mes = $mes;
        $editDocGest->nombre_documento_original = $nombreDocumento;
        $editDocGest->nota_generador = $notaGestor;
        $editDocGest->save();
    
        alert()->success('!','Documento actualizado exitosamente');
        return redirect()->route('transparencia_documentos_gestiones');
    }
    
    public function destroy(Request $request, $id){
      
        $tramite = DocumentosGestiones::where('id', $id)->first();
        $result = $tramite->delete();
            if ($result) {
                return response()->json(['success'=>'true']);
            }else{
                return response()->json(['success'=> 'false']);
            }
    }
}