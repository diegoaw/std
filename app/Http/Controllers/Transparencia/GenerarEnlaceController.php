<?php

namespace App\Http\Controllers\Transparencia;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Transparencia\Items;
use App\Models\Transparencia\Materias;
use App\Models\Transparencia\TaArchivos;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class GenerarEnlaceController extends Controller
{

    public function index(Request $request)
    {
        $dataDefinitiva = [];
        $dataDefinitiva = TaArchivos::get();
 
       return view('transparencia.generar_enlace')
       ->with('dataDefinitiva' , $dataDefinitiva);
    }

    public function create (){
        $selectItems[null] = 'Seleccione Item'; foreach (Items::where('id_user_generador', Auth::user()->id)->orwhere('id_user_generador_suplente', Auth::user()->id)->orderby('id', 'desc')->get() as $key0 => $value0) { $selectItems[$value0->id]=$value0->nombre;}
        $selectAno = [null=>'Seleccione Año' , '2020'=> 2020, '2021'=> 2021, '2022'=> 2022];
        $selectMes = [null=>'Seleccione Mes' , '1'=> 'ENERO', '2'=> 'FEBRERO', '3'=> 'MARZO', '4'=> 'ABRIL', '5'=> 'MAYO', '6'=> 'JUNIO', '7'=> 'JULIO', '8'=> 'AGOSTO', '9'=> 'SEPTIEMBBRE', '10'=> 'OCTUBRE', '11'=> 'NOVIEMBRE', '12'=> 'DICIEMBRE'];
        
        return view('transparencia.generar_enlace_crear')
        ->with('selectItems' , $selectItems)
        ->with('selectAno' , $selectAno)
        ->with('selectMes' , $selectMes);
    }

    public function store (Request $request)
    {
        set_time_limit(14200);
        $item = $request->id_items;
        $descripcion = $request->descripcion;
        $ano = $request->ano;
        $mes = $request->mes;      
        $documento = $request->filename;
        $nuevoEnlace = new TaArchivos();
        $nuevoEnlace->id_items = $item;
        $nuevoEnlace->descripcion = $descripcion;
        $nuevoEnlace->ano = $ano;
        $nuevoEnlace->mes = $mes;
        
        $nuevoEnlace->filename = $request->filename->getClientOriginalName();
        $upload = Storage::disk('s3_2')->put('archivos/'.$ano.'/'.$item.'/'.$mes, $documento, 'public');
        $nuevoEnlace->url_aws = 'https://transparenciaactiva.s3.amazonaws.com/'.$upload;
                   
        $nuevoEnlace->save();
        alert()->success('!','Enlace Generado exitosamente');
        return redirect()->route('mostrar_enlace' , [ $nuevoEnlace->id]);
    }

    public function mostrarEnalace (Request $request , $idArchivo)
    {
        $datosEnlace = TaArchivos::where('id', $idArchivo)->first();

        return view('transparencia.enlace_generado')
       ->with('datosEnlace' , $datosEnlace);
    }
       
    public function edit($id){
        
        $editEnlace = TaArchivos::where('id' , $id)->first();
        $selectItems[null] = 'Seleccione Item'; foreach (Items::where('id_user_generador', Auth::user()->id)->orwhere('id_user_generador_suplente', Auth::user()->id)->orderby('id', 'desc')->get() as $key0 => $value0) { $selectItems[$value0->id]=$value0->nombre;}
        $selectAno = [null=>'Seleccione Año' , '2020'=> 2020, '2021'=> 2021, '2022'=> 2022];
        $selectMes = [null=>'Seleccione Mes' , '1'=> 'ENERO', '2'=> 'FEBRERO', '3'=> 'MARZO', '4'=> 'ABRIL', '5'=> 'MAYO', '6'=> 'JUNIO', '7'=> 'JULIO', '8'=> 'AGOSTO', '9'=> 'SEPTIEMBBRE', '10'=> 'OCTUBRE', '11'=> 'NOVIEMBRE', '12'=> 'DICIEMBRE'];
        
        return view('transparencia.generar_enlace_editar')
        ->with('editEnlace' , $editEnlace)
        ->with('selectItems' , $selectItems)
        ->with('selectAno' , $selectAno)
        ->with('selectMes' , $selectMes);
    }

    public function update (Request $request, $id)
    {
        set_time_limit(14200);
        $item = $request->id_items;
        $descripcion = $request->descripcion;
        $ano = $request->ano;
        $mes = $request->mes;      
        $documento = $request->filename;
        $nuevoEnlace = TaArchivos::where('id' , $id)->first();
        $nuevoEnlace->id_items = $item;
        $nuevoEnlace->descripcion = $descripcion;
        $nuevoEnlace->ano = $ano;
        $nuevoEnlace->mes = $mes;
        if ($documento) {
        $nuevoEnlace->filename = $request->filename->getClientOriginalName();
        $upload = Storage::disk('s3_2')->put('archivos/'.$ano.'/'.$item.'/'.$mes, $documento, 'public');
        $nuevoEnlace->url_aws = 'https://transparenciaactiva.s3.amazonaws.com/'.$upload;
        }         
        $nuevoEnlace->save();
        alert()->success('!','Enlace Editado exitosamente');
        return redirect()->route('mostrar_enlace' , [ $nuevoEnlace->id]);
    }

    public function destroy(Request $request, $id){
      
        $tramite = TaArchivos::where('id', $id)->first();
        
        $result = $tramite->delete();
            if ($result) {
                return response()->json(['success'=>'true']);
            }else{
                return response()->json(['success'=> 'false']);
            }
    }
    

   
}