<?php

namespace App\Http\Controllers\Transparencia;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Transparencia\Items;
use App\Models\Transparencia\Materias;
use App\Models\Transparencia\Divisiones;
use App\Models\Transparencia\TaArchivos;
use App\User;




class ItemsController extends Controller
{

    public function index(Request $request)
    {
       $dataDefinitiva = [];
       $dataDefinitiva = Items::get();
       
     
      return view('transparencia.items')
      ->with('dataDefinitiva' , $dataDefinitiva);
    }

    public function create()
    {
      $selectMateria[null] = 'Seleccione Materia'; foreach (Materias::orderby('nombre', 'asc')->get() as $key0 => $value0) { $selectMateria[$value0->id]=$value0->nombre;} 
      $selectDivision[null] = 'Seleccione División'; foreach (Divisiones::orderby('nombre', 'asc')->get() as $key0 => $value0) { $selectDivision[$value0->nombre]=$value0->nombre;} 
      $selectUsuariosid[null] = 'Seleccione Usuario'; foreach (User::orderby('name', 'asc')->get() as $key2 => $value2) { $selectUsuariosid[$value2->id]=$value2->name;}
      return view('transparencia.items_crear')
      ->with('selectMateria' , $selectMateria)
      ->with('selectDivision' , $selectDivision)
      ->with('selectUsuariosid' , $selectUsuariosid);
    }

    public function store(Request $request)
    {
      $materia = $request->id_materia;
      $nombre = $request->nombre;
      $division = $request->division;
      $usuario = $request->id_user_generador;
      $usuarioSuplente = $request->id_user_generador_suplente;

      $nuevoItem = new Items();
      $nuevoItem->id_materia = $materia;
      $nuevoItem->nombre = $nombre;
      $nuevoItem->division = $division;
      $nuevoItem->id_user_generador = $usuario;
      $nuevoItem->id_user_generador_suplente = $usuarioSuplente;
      $nuevoItem->save();

      alert()->success('!','Item creado exitosamente');
      return redirect()->route('transparencia_items');
    }

    public function edit($id)
    {
      $editItems = Items::where('id' , $id)->first();
      $selectMateria[null] = 'Seleccione Materia'; foreach (Materias::orderby('nombre', 'asc')->get() as $key0 => $value0) { $selectMateria[$value0->id]=$value0->nombre;} 
      $selectDivision[null] = 'Seleccione División'; foreach (Divisiones::orderby('nombre', 'asc')->get() as $key0 => $value0) { $selectDivision[$value0->nombre]=$value0->nombre;} 
      $selectUsuariosid[null] = 'Seleccione Usuario'; foreach (User::orderby('name', 'asc')->get() as $key2 => $value2) { $selectUsuariosid[$value2->id]=$value2->name;}
      return view('transparencia.items_editar')
      ->with('editItems' , $editItems)
      ->with('selectMateria' , $selectMateria)
      ->with('selectDivision' , $selectDivision)
      ->with('selectUsuariosid' , $selectUsuariosid);
    }

    public function update(Request $request, $id)
    {
      $materia = $request->id_materia;
      $nombre = $request->nombre;
      $division = $request->division;
      $usuario = $request->id_user_generador;
      $usuarioSuplente = $request->id_user_generador_suplente;

      $editItem = Items::where('id' , $id)->first();
      $editItem->id_materia = $materia;
      $editItem->nombre = $nombre;
      $editItem->division = $division;
      $editItem->id_user_generador = $usuario;
      $editItem->id_user_generador_suplente = $usuarioSuplente;
      $editItem->save();

      alert()->success('!','Item editado exitosamente');
      return redirect()->route('transparencia_items');
    }

    public function destroy(Request $request, $id){
      
        $tramite = Items::where('id', $id)->first();
        
        $result = $tramite->delete();
            if ($result) {
                return response()->json(['success'=>'true']);
            }else{
                return response()->json(['success'=> 'false']);
            }
    }

    public function asignacion($id)
    {
      $asignacionItem = Items::where('id' , $id)->first();
      $selectUsuariosid[null] = 'Seleccione Usuario'; foreach (User::orderby('name', 'asc')->get() as $key2 => $value2) { $selectUsuariosid[$value2->id]=$value2->name;}

      return view('transparencia.items_usuarios')
      ->with('asignacionItem' , $asignacionItem)
      ->with('selectUsuariosid' , $selectUsuariosid);;
    }
 
    public function asignacionstore(Request $request, $id)
    {
        $usuario = $request->id_user_generador;
        $usuarioSuplente = $request->id_user_generador_suplente;
  
        $asignacionItem = Items::where('id' , $id)->first();
        $asignacionItem->id_user_generador = $usuario;
        $asignacionItem->id_user_generador_suplente = $usuarioSuplente;
        $asignacionItem->save();

        alert()->success('!','Asignación de Usuarios a Item exitosamente');
        return redirect()->route('transparencia_items');
    } 

    

   
}