<?php

namespace App\Http\Controllers\Transparencia;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Transparencia\Materias;




class MateriasController extends Controller
{

    public function index(Request $request)
    {
       $dataDefinitiva = [];
       $dataDefinitiva = Materias::get();
       
     
      return view('transparencia.materias')
      ->with('dataDefinitiva' , $dataDefinitiva);
    }

    public function create()
    {
      return view('transparencia.materias_crear');
    }

    public function store(Request $request)
    {
      $nombre = $request->nombre;

      $nuevaMateria = new Materias();
      $nuevaMateria->nombre = $nombre;
      $nuevaMateria->save();

      alert()->success('!','Materia creada exitosamente');
      return redirect()->route('transparencia_materias');
    }

    public function edit($id)
    {
      $editMaterias = Materias::where('id' , $id)->first();
      return view('transparencia.materias_editar')
      ->with('editMaterias' , $editMaterias);
    }

    public function update(Request $request, $id)
    {
      $nombre = $request->nombre;

      $editMateria = Materias::where('id' , $id)->first();
      $editMateria->nombre = $nombre;
      $editMateria->save();

      alert()->success('!','Materia editada exitosamente');
      return redirect()->route('transparencia_materias');
    }

    public function destroy(Request $request, $id){
      
        $tramite = Materias::where('id', $id)->first();
        
        $result = $tramite->delete();
            if ($result) {
                return response()->json(['success'=>'true']);
            }else{
                return response()->json(['success'=> 'false']);
            }
    }

   
}