<?php

namespace App\Http\Controllers\Transparencia;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Transparencia\Periodos;
use App\Models\Transparencia\DetalleTiposGestiones;
use App\Models\Transparencia\FechasPeriodos;
use Carbon\Carbon;


class PeriodosController extends Controller
{

    public function index(Request $request)
    {
       $dataDefinitiva = Periodos::orderby('id' , 'desc')->get();
       return view('transparencia.periodos')
      ->with('dataDefinitiva' , $dataDefinitiva);
    }
    public function create (){
        return view('transparencia.periodos_crear');
    }
    public function store (Request $request)
    {
        $nombre = $request->nombre;
        $cantDias = $request->cant_dias;

        $nuevoTipo = new Periodos();
        $nuevoTipo->nombre = $nombre;
        $nuevoTipo->cant_dias = $cantDias;
        $nuevoTipo->save();
        alert()->success('!','Periodo guardado exitosamente');
        return redirect()->route('Periodos.index');
    }
    public function edit (Request $request , $id){
        $editPeriodo = Periodos::where('id' , $id)->first();
        return view('transparencia.periodos_editar')
        ->with('editPeriodo' , $editPeriodo);
    }
    public function update(Request $request, $id)
    {
      $nombre = $request->nombre;
      $cantDias = $request->cant_dias;

      $editTipo = Periodos::where('id' , $id)->first();
      $editTipo->nombre = $nombre;
      $editTipo->cant_dias = $cantDias;
      $editTipo->save();
    
      alert()->success('!','Periodo actualizado exitosamente');
        return redirect()->route('Periodos.index');
    }
    
    public function destroy(Request $request, $id){
      
        $tramite = Periodos::where('id', $id)->first();
        $result = $tramite->delete();
            if ($result) {
                return response()->json(['success'=>'true']);
            }else{
                return response()->json(['success'=> 'false']);
            }
    }
    public function vistaGenerarFechas (){

        $arrayAnoDis = ['2021'=>2021];
        $dataDefinitiva = FechasPeriodos::get();
        return view('transparencia.generar_periodos_fechas')
        ->with('arrayAnoDis' , $arrayAnoDis)
        ->with('dataDefinitiva' , $dataDefinitiva);
    }
    public function generarFechas(Request $request)
    {
      $arrayMesesInt = ['01','02','03','04','05','06','07','08','09','10','11','12'];
      $periodos = Periodos::orderby('id')->get();
      $arrayFechasIF = [];
      $arrayPeriodosCrear = [];
      $ano = $request->ano;
      $sumatoriaDias = 0;
      $arrayDef = [];
      $cantDias2 = 0;
      foreach($arrayMesesInt as $indice => $mes){
        $arrayFechasIF[$mes]=['fecha_inicial' =>  Carbon::parse($ano.'-'.$mes.'-'.'01') , 'fecha_final'=>Carbon::parse($ano.'-'.$mes.'-'.'01')->endOfMonth()];
      }
      
       foreach($arrayFechasIF as $indice1 => $mes){
          
            foreach($periodos as $indice2 => $periodo){
                if($indice2 == 0){
                    $cantDias = 0;
                }else{
                    $cantDias = $periodos[$indice2-1]->cant_dias;
                }
                $consulta = FechasPeriodos::where('ano' , intval($ano))->where('mes' ,  intval($indice1) )->where('periodos_id' , $periodo->id)->first();
              //  dd( $consulta);
                if(!$consulta){
                    $nuevoFechaPeriodo = new FechasPeriodos();
                }else{
                    $nuevoFechaPeriodo = $consulta;
                }
                $nuevoFechaPeriodo->mes = intval($indice1);
                $nuevoFechaPeriodo->ano = intval($ano);
                $fechaIA = $mes['fecha_inicial'];
                $nuevoFechaPeriodo->fecha_desde = $mes['fecha_inicial']->addDays($cantDias);
                $nuevoFechaPeriodo->periodos_id = $periodo->id;
                $nuevoFechaPeriodo->save();
                $cantDias = 0;
               
            }
      } 

      $arrayFechasIF = [];
      foreach($arrayMesesInt as $indice => $mes){
        $arrayFechasIF[$mes]=['fecha_inicial' =>  Carbon::parse($ano.'-'.$mes.'-'.'01') , 'fecha_final'=>Carbon::parse($ano.'-'.$mes.'-'.'01')->endOfMonth()];
      }

      foreach($arrayFechasIF as $indice1 => $mes){
          
        foreach($periodos as $indice2 => $periodo){
                $cantDias = $periodo->cant_dias;
                $consulta = FechasPeriodos::where('ano' , $ano)->where('mes' ,  intval($indice1) )->where('periodos_id' , $periodo->id)->first();
                $nuevoFechaPeriodo = $consulta;
                $nuevoFechaPeriodo->mes = intval($indice1);
                $nuevoFechaPeriodo->ano = intval($ano);
                $fechaIA = $mes['fecha_inicial'];
                $nuevoFechaPeriodo->fecha_hasta = $mes['fecha_inicial']->addDays($cantDias);
                $nuevoFechaPeriodo->periodos_id = $periodo->id;
                $nuevoFechaPeriodo->save();
                $cantDias = 0;
        }
    } 


        return redirect()->route('transparencia_generar_fecha');
    } 
}