<?php

namespace App\Http\Controllers\Transparencia;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Transparencia\FechasPeriodos;
use App\Http\Requests\Transparencia\PeriodosFechasRequest;
use App\Models\Transparencia\DetalleTiposGestiones;
use App\Models\Transparencia\Periodos;


class PeriodosFechasController extends Controller
{

    public function index(Request $request)
    {
       $dataDefinitiva = [];
       $dataDefinitiva = FechasPeriodos::orderby('mes')->get();
     
      return view('transparencia.periodos_fechas')
      ->with('dataDefinitiva' , $dataDefinitiva);
    }


    public function edit($id)
    {
      $editFechasPeriodos = FechasPeriodos::where('id' , $id)->first();
      return view('transparencia.periodos_fechas_edit')
      ->with('editFechasPeriodos' , $editFechasPeriodos);
    }

    public function update(PeriodosFechasRequest $request, $id)
    {
      
      $fechaDesde = $request->fecha_desde;
      $fechaHasta = $request->fecha_hasta;
      $editFechasPeriodos = FechasPeriodos::where('id' , $id)->first();
      $editFechasPeriodos->fecha_desde = $fechaDesde;
      $editFechasPeriodos->fecha_hasta = $fechaHasta;
      $editFechasPeriodos->save();

      alert()->success('!','Fechas del Periodo editado exitosamente');
      return redirect()->route ('periodosfechas');

    } 

}