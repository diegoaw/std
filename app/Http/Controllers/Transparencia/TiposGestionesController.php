<?php

namespace App\Http\Controllers\Transparencia;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Transparencia\TiposGestiones;
use App\Models\Transparencia\DetalleTiposGestiones;
use App\Models\Transparencia\UsuariosTiposGestiones;
use App\Http\Requests\Transparencia\TiposRequest;
use App\Models\Transparencia\Periodos;
use App\User;




class TiposGestionesController extends Controller
{

    public function index(Request $request)
    {
       $dataDefinitiva = [];
       $requestnombreTipo = $request->nombre;
       $dataDefinitiva = TiposGestiones::orderby('id' , 'desc')->get();
       //dd($dataDefinitiva[1]->DetallesTipos);
       
     
      return view('transparencia.tipos_gestiones')
      ->with('dataDefinitiva' , $dataDefinitiva)
      ->with('requestnombreTipo' , $requestnombreTipo);
    }

    public function create()
    {
      return view('transparencia.tipos_gestiones_crear');
    }

    public function store(TiposRequest $request)
    {
      $nombre = $request->nombre;
      $nuevoTipo = new TiposGestiones();
      $nuevoTipo->nombre = $nombre;
      $nuevoTipo->save();

     // $arrayDetalle = ['Generador', 'Revisor', 'Encargado TA'];
     $arrayDetalle = Periodos::get();
      foreach($arrayDetalle as $indice => $registro){
        $nombre2 = $request->nombre.' '.$registro->nombre;
        $nuevoDetalleTipo = new DetalleTiposGestiones();
        $nuevoDetalleTipo->nombre = $nombre2;
        $nuevoDetalleTipo->periodos_id =$registro->id;
        $nuevoDetalleTipo->tipos_gestiones_id = $nuevoTipo->id;
        $nuevoDetalleTipo->save();
      }

      alert()->success('!','Tipo creado exitosamente');
      return redirect()->route('transparencia_tipos');
    }

    public function edit($id)
    {
      $editTipo = TiposGestiones::where('id' , $id)->first();
      $editDetalleTipo = DetalleTiposGestiones::where('tipos_gestiones_id' , $editTipo->$id)->first();
      return view('transparencia.tipos_gestiones_editar')
      ->with('editDetalleTipo' , $editDetalleTipo)
      ->with('editTipo' , $editTipo);
    }

    public function update(TiposRequest $request, $id)
    {
      $nombre = $request->nombre;
      $editTipo = TiposGestiones::where('id' , $id)->first();
      $editTipo->nombre = $nombre;
      $editTipo->save();

      $detallesTiposReg = DetalleTiposGestiones::where('tipos_gestiones_id' , $id)->get();
     
      foreach($detallesTiposReg as $indice2 =>  $registro2){
        $registro2->delete();
      }

      //$arrayDetalle = ['Generador', 'Revisor', 'Encargado TA'];
      $arrayDetalle = Periodos::get();
      foreach($arrayDetalle as $indice => $registro){
        $nombre2 = $request->nombre.' '.$registro->nombre;
        $nuevoDetalleTipo = new DetalleTiposGestiones();
        $nuevoDetalleTipo->nombre = $nombre2;
        $nuevoDetalleTipo->periodos_id =$registro->id;
        $nuevoDetalleTipo->tipos_gestiones_id = $editTipo->id;
        $nuevoDetalleTipo->save();
      }

      alert()->success('!','Tipo editado exitosamente');
      return redirect()->route ('transparencia_tipos');

    } 

    public function destroy(Request $request, $id){
      
      $tramite = TiposGestiones::where('id', $id)->first();
      $tramite2 = DetalleTiposGestiones::where('tipos_gestiones_id' , $id)->get();
      
      foreach($tramite2 as $indice2 =>  $registro2){ 
        $registro2->delete();
      }
      $result = $tramite->delete();
          if ($result) {
              return response()->json(['success'=>'true']);
          }else{
              return response()->json(['success'=> 'false']);
          }
  }

  public function asignacion($id)
    {
      $asignacionTipo = TiposGestiones::where('id' , $id)->first();
      $DetalleTipo = DetalleTiposGestiones::where('tipos_gestiones_id' , $id)->get();
      $selectUsuariosid[null] = 'SELECCIONE USUARIO'; foreach (User::get() as $key2 => $value2) { $selectUsuariosid[$value2->id]=$value2->name;}

      return view('transparencia.tipos_gestiones_usuarios')
      ->with('DetalleTipo' , $DetalleTipo)
      ->with('asignacionTipo' , $asignacionTipo)
      ->with('selectUsuariosid', $selectUsuariosid);
    }
 
    public function asignacionstore(Request $request)
    {
      $usuario_id = $request->usuario_id;
      $id_detalle_tipo = $request->detalle_tipo_id;
      $id_gestion =  $request->gestion_id;
      //dd($id_gestion);
      $nuevoUsuarioTipoGestion = new UsuariosTiposGestiones();
      $nuevoUsuarioTipoGestion->detalle_tipos_gestiones_id =  $id_detalle_tipo;
      $nuevoUsuarioTipoGestion->user_id = $usuario_id;
      $nuevoUsuarioTipoGestion->save();

      return redirect()->route('transparencia_tipo_usuario' , [$id_gestion]);
    } 

    public function asignaciondestroy(Request $request, $id, $detalleTipoGestion){
     
      $usuario = UsuariosTiposGestiones::where('user_id', $id)->where('detalle_tipos_gestiones_id', $detalleTipoGestion)->first();
      
      $result = $usuario->delete();
          if ($result) {
              return response()->json(['success'=>'true']);
          }else{
              return response()->json(['success'=> 'false']);
          }
  }
}