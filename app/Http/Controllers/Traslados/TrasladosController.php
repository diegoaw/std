<?php

namespace App\Http\Controllers\Traslados;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use App\Models\TramitesSimple\TramitesSimple;
use App\Models\File\File;
use App\Models\Simple\HasGrupo;
use App\Models\Simple\Grupos;
use App\Models\UsuariosSimple\UsuariosSimple;
use App\Models\Divisiones\Divisiones;
use App\Models\Simple\TmpTramiteCompras;
use App\Models\Simple\Etapa;
use App\Models\Simple\DatoSeguimiento;
use App\Models\Traslado\Traslado;

class TrasladosController extends Controller
{
    public function index(Request $request){

    $tramites = [];
    $tramiteFiltrado = [];
    $idTramiteFiltro = $request->nRegistro;
    $tituloFiltro = $request->titulo;

    $queryFilter = "where r.numero_tramite = $idTramiteFiltro ";
    
        $query = "SELECT  *
          FROM
            (select
            tramite.id as numero_tramite ,
            tramite.proceso_id as numero_proeceso ,
            proceso.nombre as nombre_proceso ,
            tramite.created_at as fecha_inicio ,
            tramite.updated_at as fecha_ultima_modificacion ,
            tramite.pendiente as pendiente,
            tramite.ended_at as fecha_culmino,
            tarea.nombre as etapa,
            et.id as id_etapa,
            dts.conteo as cantidad_campos
            from tramite
            INNER JOIN proceso ON tramite.proceso_id = proceso.id
            INNER JOIN (SELECT * FROM etapa WHERE id IN (SELECT MAX(id) FROM etapa GROUP BY tramite_id) ORDER BY id DESC) et on et.tramite_id = tramite.id
            INNER JOIN tarea on et.tarea_id = tarea.id
            LEFT JOIN (SELECT  COUNT(id) as conteo, etapa_id FROM dato_seguimiento  group by etapa_id ) dts on dts.etapa_id = et.id
            where tramite.proceso_id = 27  and tarea.nombre = 'Ingreso Solicitud de Traslados' and tramite.deleted_at is null
            and dts.conteo >3             
          UNION ALL
            select
            tramite.id as numero_tramite ,
            tramite.proceso_id as numero_proeceso ,
            proceso.nombre as nombre_proceso ,
            tramite.created_at as fecha_inicio ,
            tramite.updated_at as fecha_ultima_modificacion ,
            tramite.pendiente as pendiente,
            tramite.ended_at as fecha_culmino,
            tarea.nombre as etapa,
            et.id as id_etapa,
            dts.conteo as cantidad_campos
            from tramite
            INNER JOIN proceso ON tramite.proceso_id = proceso.id
            INNER JOIN (SELECT * FROM etapa WHERE id IN (SELECT MAX(id) FROM etapa GROUP BY tramite_id) ORDER BY id DESC) et on et.tramite_id = tramite.id
            INNER JOIN tarea on et.tarea_id = tarea.id
            LEFT JOIN (SELECT  COUNT(id) as conteo, etapa_id FROM dato_seguimiento  group by etapa_id ) dts on dts.etapa_id = et.id
            where tramite.proceso_id = 27
            and tramite.deleted_at is null
            and tarea.nombre <> 'Ingreso Solicitud de Traslados') r ";
    
        ($idTramiteFiltro != null )? $query = $query.$queryFilter : $query = $query;
        $query = $query."order by r.numero_tramite DESC";
    
        $tramites = DB::connection('mysqlSimple')->select($query);
        

        return view('traslados.traslados')
        ->with('tramitesD', $tramites)
        ->with('idTramiteFiltro', $idTramiteFiltro);
    }       

    public function show($id){
                  
        $client = new Client();
              try {
              $request = $client->get("https://simple.mindep.cl/backend/api/tramites/$id?token=bvhbiynk4nq5hpjfkrypf3m07youdcdu " , ['verify' => false]);
              $response = $request->getBody()->getContents();
              $data =  json_decode($response);
                }catch (BadResponseException   $e) {
                    $response = $e->getResponse();
                }
        $data = $data->tramite;
 
        $etapasCont = count($data->etapas);

        $nuevoTraslado = new Traslado();
        $nuevoTraslado->id =  $data->id;
        $nuevoTraslado->fecha_inicio =  $data->fecha_inicio;
        $nuevoTraslado->fecha_termino =  $data->fecha_termino;
        $nuevoTraslado->fecha_modificacion =  $data->fecha_modificacion;
        $nuevoTraslado->estado =  $data->estado;
        
        $datos=$data->datos;

        foreach ($datos as $key => $value2) {
             if(property_exists($value2, 'declaro')){
                $nuevoTraslado->declaro =  $value2->declaro;
              }
              if(property_exists($value2, 'motivo_traslado')){
                $nuevoTraslado->motivo_traslado =  $value2->motivo_traslado;
              }
              if(property_exists($value2, 'adjunta_respaldo')){
                $nuevoTraslado->adjunta_respaldo =  $value2->adjunta_respaldo->URL;
              }
              if(property_exists($value2, 'centro_costos')){
                $nuevoTraslado->centro_costos =  $value2->centro_costos;
              }
              if(property_exists($value2, 'destinatario')){
                $nuevoTraslado->destinatario =  $value2->destinatario;
              }
              if(property_exists($value2, 'api_area')){
                $nuevoTraslado->api_area =  $value2->api_area;
              }
              if(property_exists($value2, 'cantidad_pasajeros')){
                $nuevoTraslado->cantidad_pasajeros =  $value2->cantidad_pasajeros;
              }
              if(property_exists($value2, 'fecha_traslado')){
                $nuevoTraslado->fecha_traslado =  $value2->fecha_traslado;
              }
              if(property_exists($value2, 'nombre_pasajero')){
                $nuevoTraslado->nombre_pasajero =  $value2->nombre_pasajero;
              }
              if(property_exists($value2, 'numero_telefono')){
                $nuevoTraslado->numero_telefono =  $value2->numero_telefono;
              }
              if(property_exists($value2, 'lugar_salida')){
                $nuevoTraslado->lugar_salida =  $value2->lugar_salida;
              }
              if(property_exists($value2, 'hora_salida')){
                $nuevoTraslado->hora_salida =  $value2->hora_salida;
              }
              if(property_exists($value2, 'lugar_destino')){
                $nuevoTraslado->lugar_destino =  $value2->lugar_destino;
              }
              if(property_exists($value2, 'servicio_regreso')){
                $nuevoTraslado->servicio_regreso =  $value2->servicio_regreso;
              }
              if(property_exists($value2, 'indique_hora_regreso')){
                $nuevoTraslado->indique_hora_regreso =  $value2->indique_hora_regreso;
              }
              if(property_exists($value2, 'itinerario_vuelo')){
                $nuevoTraslado->itinerario_vuelo =  $value2->itinerario_vuelo->URL;
              }
              if(property_exists($value2, 'observaciones')){
                $nuevoTraslado->observaciones =  $value2->observaciones;
              }  
              if(property_exists($value2, 'comprobante')){
                $nuevoTraslado->comprobante_descarga = 'https://simple.mindep.cl/uploads/documentos/'.$value2->comprobante;
               } 
              if(property_exists($value2, 'prueba_solicitud')){
                $nuevoTraslado->prueba_solicitud =  $value2->prueba_solicitud;
              }
              if(property_exists($value2, 'observaciones_j')){
                $nuevoTraslado->observaciones_j =  $value2->observaciones_j;
              }
              if(property_exists($value2, 'aprueba_solicitud_servicios')){
                $nuevoTraslado->aprueba_solicitud_servicios =  $value2->aprueba_solicitud_servicios;
              }
              if(property_exists($value2, 'observaciones_s')){
                $nuevoTraslado->observaciones_s =  $value2->observaciones_s;
              }
          }

      return view('traslados.traslados_detalle')
      ->with('data' , $nuevoTraslado);
    }

}
