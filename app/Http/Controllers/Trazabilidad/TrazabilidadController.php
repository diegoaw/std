<?php

namespace App\Http\Controllers\Trazabilidad;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\DerivacionesDocumento\DerivacionesDocumento;
use App\Models\IngresoTramite\IngresoTramite;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ComboBoxCompra;
use App\Models\TiposDocumentos\TiposDocumentos;
use DB;

use App\Models\CorrelativoTipoDocumento\CorrelativoTipoDocumento;



class TrazabilidadController extends Controller
{



   	public function index(Request $request){


      set_time_limit(14200);
      $selectA = $this->cargarExcelDeparatamentos();
      $tiposDocumentos = $this->tiposDocuemntosJsonOpa();
      $anno = Carbon::now()->format('Y');
      $fechaStringInicio = ($anno).'-01-01 00:00:00';
      $feDesde = Carbon::parse($fechaStringInicio);
      $feHasta =  Carbon::parse(Carbon::now()->format('Y-m-d').' 23:59:59');
      $fechaInDesde = Carbon::parse($fechaStringInicio);
      $fechaInHasta = Carbon::now();
      $fechas[1] = Date('Y-m-d', strtotime( '1970-01-01' ));
      $fechas[0] = Date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
      $tramites = [];
      $requestNRegistro = null;
      $origenView ='';
      $areRemitenteView = '';
      $areDestitenteView = '';
      $tiposDocumentosView = '';
      $contenidoView='';
      $origenController='';
      $procedenciaView ='';
      $mostrar = false;
      $observacionView='';

          if( $request->FechaDesde != null ) {
            $feDesde=(Carbon::parse($request->get('FechaDesde'))->format('dmY'));
            $feDesde= Carbon::parse(substr($feDesde, 0, 2).'-'.substr($feDesde, 2, 2).'-'.substr($feDesde, 4, 4).' 00:00:00');
          }
          if( $request->FechaHasta != null ) {
             $feHasta=(Carbon::parse($request->get('FechaHasta'))->format('dmY'));
             $feHasta= Carbon::parse(substr($feHasta, 0, 2).'-'.substr($feHasta, 2, 2).'-'.substr($feHasta, 4, 4).' 23:59:59');
          }
          if($request->origen == '' ) {
                $origenController =  null;
          }else{
            $origenController = $request->origen;
          }




      $query = DerivacionesDocumento::query ();
        $query->when(request('nRegistro', false), function ($q, $numRegistro) {
            return $q->where('tramite_id', $numRegistro);
        });

        $query->when($feDesde, function ($q, $feDesde ) {
            return $q->where('fecha','>=', $feDesde);
        });

        $query->when($feHasta, function ($q, $feHasta ) {
            return $q->where('fecha','<=', $feHasta);
        });
       $query->when($origenController, function ($q, $origen) {
            return $q->where('origen', $origen);
        });
       $query->when(request('areaRemtiente', false), function ($q, $areaRemtiente) {
            return $q->where('area_remitente', $areaRemtiente);
        });

       $query->when(request('areaDesti', false), function ($q, $areaDesti) {
            return $q->where('area_destinatario', $areaDesti);
        });
       $query->when(request('tiposDocumentos', false), function ($q, $tiposDocumentos) {
            return $q->where('tipo_documento', $tiposDocumentos);
        });
      $query->when(request('contenido'), function ($q, $contenido) {
            return $q->where('contenido', 'LIKE', "%$contenido%");
        });

      $query->when(request('procedencia'), function ($q, $procedencia) {
            return $q->where('area_remitente', 'LIKE', "%$procedencia%");
        });
      $query->when(request('observacion'), function ($q, $observacion) {
            return $q->where('observacion', 'LIKE', "%$observacion%");
        });

      $listado = $query->get()->sortByDesc(function($q){
     return $q->created_at;
     });

     // $listado = $query->get();

      foreach ($listado as $indiceListado => $valueRegistro) {
        $numeroCorrelativo = CorrelativoTipoDocumento::where('tramite_id' , $valueRegistro->tramite_id)->where('derivacion_n' , $valueRegistro->derivacion_n)->first();
        if($numeroCorrelativo){
          $valueRegistro->correlativo = $numeroCorrelativo->correlativo;
        }
      }




      $listado=$listado->groupby('tramite_id');
      $recorrer = 0;
      foreach ($listado as $indice => $registro) {
        $tramites[$recorrer] = IngresoTramite::where('tramite_id' , $indice)->first();
        $recorrer = $recorrer + 1;
      }
      if( $request->nRegistro != null ) {
        $requestNRegistro = $request->nRegistro;
      }
      if( $request->origen != null ) {
        $origenView =  $request->origen;
      }
      if($request->areaRemtiente != null ) {
        $areRemitenteView =  $request->areaRemtiente;
      }
      if($request->areaDesti != null ) {
        $areDestitenteView =  $request->areaDesti;
      }
      if($request->tiposDocumentos != null ) {
        $tiposDocumentosView =  $request->tiposDocumentos;
      }
      if($request->contenido != null ) {
        $contenidoView =  $request->contenido;
        //dd($contenidoView);
      }
       if($request->procedencia != null ) {
        $procedenciaView =  $request->procedencia;
      }
      if($request->observacion != null ) {
        $observacionView =  $request->observacion;
      }

      if (count($tramites)==0){$mostrar = false;} else {$mostrar = true;}


   		$mostrar = true;

      //dd($listado);

      return view('trazabilidad.trazabilidad')
      ->with('selectA' ,$selectA)
      ->with('origenView' , $origenView)
      ->with('procedenciaView' , $procedenciaView)
      ->with('contenidoView' , $contenidoView)
      ->with('areRemitenteView' , $areRemitenteView)
      ->with('areDestitenteView' , $areDestitenteView)
      ->with('tiposDocumentosView' , $tiposDocumentosView)
      ->with('tiposDocumentos' ,$tiposDocumentos)
      ->with('requestNRegistro' ,$requestNRegistro)
      ->with('feDesde' ,$feDesde)
      ->with('feHasta' ,$feHasta)
      ->with('fechaInDesde' ,$fechaInDesde)
      ->with('fechaInHasta' ,$fechaInHasta)
      ->with('fechas' ,$fechas)
      ->with('tramites' ,$tramites)
      ->with('listado' ,$listado)
      ->with('observacionView' , $observacionView)
      ->with('mostrar' ,$mostrar);

}

public function cargarExcelDeparatamentos (){


      $array = collect(Excel::load(public_path('departamentos2.xlsx'), function($reader) {})->get());
      $arrayTransformado = [];
      $cantidad = count($array[0]);

      for ($i=0; $i < $cantidad; $i++) {
        $arrayTransformado[$array[0][$i]['area']] = $array[0][$i]['area'];
       }


        return (collect($arrayTransformado)->prepend('Todos', null));

    }

public function tiposDocuemntosJsonOpa(){
    $tiposDocumentos = TiposDocumentos::get();
    $tiposDocumentosDefinitivo  = [];
    foreach ($tiposDocumentos as $indice => $regisro) {
      $tiposDocumentosDefinitivo [$regisro->nombre] = $regisro->nombre;
    }
    return (collect($tiposDocumentosDefinitivo)->prepend('Todos', null));
  }

public function reporteTrazabilidad(Request $request) {

  $tramites = [];
   if( $request->FechaDesde != null ) {
            $feDesde=(Carbon::parse($request->get('FechaDesde'))->format('dmY'));
            $feDesde= Carbon::parse(substr($feDesde, 0, 2).'-'.substr($feDesde, 2, 2).'-'.substr($feDesde, 4, 4).' 00:00:00');
          }
          if( $request->FechaHasta != null ) {
             $feHasta=(Carbon::parse($request->get('FechaHasta'))->format('dmY'));
             $feHasta= Carbon::parse(substr($feHasta, 0, 2).'-'.substr($feHasta, 2, 2).'-'.substr($feHasta, 4, 4).' 23:59:59');
          }
          if($request->origen1 == '' ) {
                $origenController =  null;
          }else{
            $origenController = $request->origen1;
          }
          if($request->docdigital == '' ) {
                $docdigitalController =  null;
          }else{
            $docdigitalController = $request->docdigital;
          }

        $query = DerivacionesDocumento::query ();
        $query->when(request('nRegistro', false), function ($q, $numRegistro) {
            return $q->where('tramite_id', $numRegistro);
        });
        $query->when($feDesde, function ($q, $feDesde ) {
            return $q->where('fecha','>=', $feDesde);
        });

        $query->when($feHasta, function ($q, $feHasta ) {
            return $q->where('fecha','<=', $feHasta);
        });
         $query->when($origenController, function ($q, $origen) {
            return $q->where('origen', $origen);
        });
         $query->when($docdigitalController, function ($q, $docdigital) {
            return $q->where('docdigital', $docdigital);
        });
        $query->when(request('areaRemtiente1', false), function ($q, $areaRemtiente) {
            return $q->where('area_remitente', $areaRemtiente);
        });

       $query->when(request('areaDesti1', false), function ($q, $areaDesti) {
            return $q->where('area_destinatario', $areaDesti);
        });
       $query->when(request('tiposDocumentos1', false), function ($q, $tiposDocumentos) {
            return $q->where('tipo_documento', $tiposDocumentos);
        });
       $query->when(request('contenido'), function ($q, $contenido) {
            return $q->where('contenido', 'LIKE', "%$contenido%");
        });
       $query->when(request('procedencia'), function ($q, $procedencia) {
            return $q->where('area_remitente', 'LIKE', "%$procedencia%");
        });

       $listado = $query->get()->sortByDesc(function($q){
     return $q->created_at;
     });

        //$listado = $query->get();
        $listado=$listado->groupby('tramite_id');


      $recorrer = 0;
      foreach ($listado as $indice => $registro) {
        $tramites[$recorrer] = IngresoTramite::where('tramite_id' , $indice)->first();
        $recorrer = $recorrer + 1;
      }

        //dd($tramites);

        //toma en cuenta el tipo de reporte seleccionado
        Excel::create("TrazabilidadReportes", function ($excel)  use($tramites ) {
        $excel->setTitle("TrazabilidadReportes");
        $excel->sheet("tramites", function ($sheet) use($tramites ) {
        $sheet->loadView('trazabilidad.ex_excel')->with('tramites', $tramites);
        });
        })->download('xlsx');
        die ;


    }     

}
