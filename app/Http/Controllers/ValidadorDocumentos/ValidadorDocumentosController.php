<?php

namespace App\Http\Controllers\ValidadorDocumentos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use App\Http\Requests\ValidarDocumentos\ValidarDocumentosRequest;
use App\Models\DocumentosFirmados\DocumentosFirmados;
class ValidadorDocumentosController extends Controller
{

    public function index()
    {
        return view('validar_documentos.validar_documento');
    }
    public function Validar(ValidarDocumentosRequest $request)
    {
       $id = $request->id;
       $codigo = $request->hash;
       $documento = DocumentosFirmados::where('code' , $id)->where('hash_autenticador' , $codigo)->first();
       $firmantes = $documento->firmantes;
       if(!$documento){
            return redirect()->back()->withErrors(['No sentimos no existe documento con estos datos']); 
       }else{
         return view('validar_documentos.documento_validado')
         ->with('firmantes' , $firmantes)
         ->with('documento' , $documento);
       }

    }
}
