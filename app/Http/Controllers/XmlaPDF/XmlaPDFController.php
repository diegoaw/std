<?php

namespace App\Http\Controllers\XmlaPDF;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Storage;


class XmlaPDFController extends Controller
{

    public function index()
    {   
      return view('xmlapdf.xmlapdf');
    }
    public function converHtml(Request $request){

        $docu = $request->documento;
        $docNombre = sha1('Fachtml').'.'.$request->file('documento')->getClientOriginalExtension();
        $rutaguardar = public_path().'/xml';
        $docu->move($rutaguardar,$docNombre);

        $ruta = public_path('xml/'.  $docNombre);
        $cargaXml = simplexml_load_file($ruta);
        $json = json_encode($cargaXml);
        $json = json_decode($json,TRUE);

        $dataEmisor = ($json['SetDTE']['DTE']['Documento']['Encabezado']['Emisor']);
        $dataReceptor = ($json['SetDTE']['DTE']['Documento']['Encabezado']['Receptor']);
        $dataDocumento = ($json['SetDTE']['DTE']['Documento']['Encabezado']['IdDoc']);
        $detalleProducto =($json['SetDTE']['DTE']['Documento']['Detalle']);
        $totales = ($json['SetDTE']['DTE']['Documento']['Encabezado']['Totales']);

        $referencia = array_key_exists('Referencia' , $json['SetDTE']['DTE']['Documento']) ? $json['SetDTE']['DTE']['Documento']['Referencia'] : null;
        //dd($detalleProducto);
       
      return view('xmlapdf.xmlapdfview')
            ->with('dataDocumento' , $dataDocumento)
            ->with('dataEmisor' , $dataEmisor)
            ->with('dataReceptor' , $dataReceptor)
            ->with('detalleProducto' , $detalleProducto)
            ->with('referencia' , $referencia)
            ->with('totales' , $totales)
            ->with('json' , $json);
    }
}
