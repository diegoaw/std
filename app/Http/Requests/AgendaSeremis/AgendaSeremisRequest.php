<?php

namespace App\Http\Requests\AgendaSeremis;

use App\Http\Requests\Request;

class AgendaSeremisRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            
            // 'nombre_actividad' => 'required',
            // 'pais' => 'required',
            // 'fecha_realizacion' => 'required',
            // 'parlamentario_invitado' => 'required',
            // 'institucion_convoca' => 'required',
            // 'fecha_inicio' => 'required',
            // 'fecha_fin' => 'required',
            // 'observacion' => 'required',
        ];
    }

    public function messages()
    {
        return [
           
            // 'nombre_actividad.required' => 'El nombre de la actividad es requerida.',
            // 'pais.required' => 'El país es requerido.',
            // 'fecha_realizacion.required' => 'La fecha de realización es requerida.',
            // 'parlamentario_invitado.required' => 'El parlamentario/a invitado/a es requerido/a.',
            // 'institucion_convoca.required' => 'La institución que convoca es requerida.',
            // 'fecha_inicio.required' => 'La fecha de inicio es requerida.',
            // 'fecha_fin.required' => 'La fecha fin es requerida.',
            // 'observacion.required' => 'La observación requerida.',
        ];
    }
}
