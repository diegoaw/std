<?php

namespace App\Http\Requests\DocumentosDigitales;

use App\Http\Requests\Request;
use  App\Models\Seguridad\PasswordRequirements;

class ColaboracionDocumentoFirmanteRequest extends Request
{

    public function authorize()
    {
     return true;
    }

    public function rules()
    {
        return [
            'firmante'=>'required',
            'id_tipos_documentos_digitales'=>'required',
            'lugar'=>'required',
            'id_categoria'=>'required',
            'titulo'=>'required',
            'derivar'=>'required',
        ];
    }

    public function messages()
    {
        return [
            'firmante.required' => 'La pregunta ¿Desea Cambiar Firmante Actual? es requerida.',
            'id_tipos_documentos_digitales.required' => 'El Tipo de documento es requerido.',
            'lugar.required' => 'El Lugar del documento es requerido.',
            'id_categoria.required' => 'La Categoría del documento es requerida.',
            'titulo.required' => 'La Materia del documento requerida.',
            'derivar.required' => 'La pregunta ¿Enviar a Colaboración para Edición del Documento? es requerida.',
        ];
    }
}