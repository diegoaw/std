<?php

namespace App\Http\Requests\DocumentosDigitales;

use App\Http\Requests\Request;
use  App\Models\Seguridad\PasswordRequirements;

class DistribuirFolioRequest extends Request
{

    public function authorize()
    {
     return true;
    }

    public function rules()
    {
        return [
           // 'tipo_distribucion'=>'required',
        ];
    }

    public function messages()
    {
        return [
           // 'tipo_distribucion.required' => 'El Tipo de Distribución del documento digital es requerido.',
        ];
    }
}