<?php

namespace App\Http\Requests\DocumentosDigitales;

use App\Http\Requests\Request;
use  App\Models\Seguridad\PasswordRequirements;

class EditarReservarFolioRequest extends Request
{

    public function authorize()
    {
     return true;
    }

    public function rules()
    {
        return [
            'lugar'=>'required',
            'motivo_cambio'=>'required',
        ];
    }

    public function messages()
    {
        return [
            'lugar.required' => 'El Lugar del apartado del folio(s) es requerido.',
            'motivo_cambio.required' => 'El Motivo del Cambio es requerido.',
        ];
    }
}