<?php

namespace App\Http\Requests\DocumentosDigitales;

use App\Http\Requests\Request;
use  App\Models\Seguridad\PasswordRequirements;

class GenerarEnlaceRequest extends Request
{

    public function authorize()
    {
     return true;
    }

    public function rules()
    {
        return [
            'descripcion'=>'required',
            'filename'=>'required',
        ];
    }

    public function messages()
    {
        return [
            'descripcion.required' => 'La Materia o Justificación del folio(s) es requerida.',
            'filename.required' => 'El Archivo PDF es requerido.',
        ];
    }
}