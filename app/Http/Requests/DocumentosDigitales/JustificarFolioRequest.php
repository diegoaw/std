<?php

namespace App\Http\Requests\DocumentosDigitales;

use App\Http\Requests\Request;
use  App\Models\Seguridad\PasswordRequirements;

class JustificarFolioRequest extends Request
{

    public function authorize()
    {
     return true;
    }

    public function rules()
    {
        return [
            'materia'=>'required',
            'filename'=>'required|mimes:pdf',
        ];
    }

    public function messages()
    {
        return [
            'materia.required' => 'La Materia o Justificación del folio(s) es requerida.',
            'filename.required' => 'El Archivo PDF es requerido.',
            'filename.mimes' => 'El Archivo debe ser PDF.',
        ];
    }
}