<?php

namespace App\Http\Requests\DocumentosDigitales;

use App\Http\Requests\Request;
use  App\Models\Seguridad\PasswordRequirements;

class RegistroDocumentoRequest extends Request
{

    public function authorize()
    {
     return true;
    }

    public function rules()
    {
        return [
            'id_tipos_documentos_digitales'=>'required',
            'lugar'=>'required',
            'id_categoria'=>'required',
            'memo'=>'required',
            'titulo'=>'required',
            'filename'=>'required|mimes:pdf',
            'id_user_firmante'=>'required',
        ];
    }

    public function messages()
    {
        return [
            'id_tipos_documentos_digitales.required' => 'El Tipo de documento es requerido.',
            'lugar.required' => 'El Lugar del documento es requerido.',
            'id_categoria.required' => 'La Categoría del documento es requerida.',
            'memo.required' => 'La Pregunta de si tiene relación con un Memo es requerida.',
            'titulo.required' => 'La Materia del documento requerida.',
            'filename.required' => 'El Archivo PDF es requerido.',
            'filename.mimes' => 'El Archivo debe ser PDF.',
            'id_user_firmante.required' => 'El Campo del Firmante es requerido.',
        ];
    }
}