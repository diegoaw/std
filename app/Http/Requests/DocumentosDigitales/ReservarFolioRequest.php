<?php

namespace App\Http\Requests\DocumentosDigitales;

use App\Http\Requests\Request;
use  App\Models\Seguridad\PasswordRequirements;

class ReservarFolioRequest extends Request
{

    public function authorize()
    {
     return true;
    }

    public function rules()
    {
        return [
            'tipo_documento'=>'required',
            'lugar'=>'required',
            'n_folios'=>'required',
            'descripcion'=>'required',
        ];
    }

    public function messages()
    {
        return [
            'tipo_documento.required' => 'El Tipo de documento es requerido.',
            'lugar.required' => 'El Lugar del apartado del folio(s) es requerido.',
            'n_folios.required' => 'La Cantidad de folios a apartar es requerida.',
            'descripcion.required' => 'El Motivo del Apartado del folio(s) es requerido.',
        ];
    }
}