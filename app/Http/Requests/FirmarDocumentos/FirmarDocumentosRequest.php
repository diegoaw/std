<?php

namespace App\Http\Requests\FirmarDocumentos;

use App\Http\Requests\Request;
use  App\Models\Seguridad\PasswordRequirements;

class FirmarDocumentosRequest extends Request
{

    public function authorize()
    {
     return true;
    }



    public function rules()
    {
        return [
            'rut'=>'required',
            'folio'=>'required',
            'nombre'=>'required',
            'descripcion'=>'required',


        ];
    }

    public function messages()
    {
        return [
            'rut.required' => 'El rut es requerido.',
            'folio.required' => 'El folio del documento es requerido.',
            'nombre.required' => 'El Nombre del documento requerido.',
            'descripcion.required' => 'la descripcion del documento es requerida.',
        ];
    }
}
