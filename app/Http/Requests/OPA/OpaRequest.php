<?php

namespace App\Http\Requests\OPA;

use App\Http\Requests\Request;

class OpaRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id_opa_tipo' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'id_opa_tipo.required' => 'El tipo es requerido.',
        ];
    }
}
