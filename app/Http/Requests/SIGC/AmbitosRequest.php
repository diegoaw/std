<?php

namespace App\Http\Requests\SIGC;

use App\Http\Requests\Request;

class AmbitosRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nombre' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El nombre del Ámbito es requerido.',
        ];
    }
}
