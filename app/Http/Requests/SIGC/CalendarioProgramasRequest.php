<?php

namespace App\Http\Requests\SIGC;

use App\Http\Requests\Request;

class CalendarioProgramasRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            
            'ano' => 'required',
            'mes' => 'required',
            'id_programas_eventos' => 'required',
            'fecha_inicio' => 'required',
            'fecha_fin' => 'required',
            
        ];
    }

    public function messages()
    {
        return [
            
            'ano.required' => 'El Año del Calendario Evento es Requerido.',
            'mes.required' => 'El Mes del Calendario Evento es Requerido.',
            'id_programas_eventos.required' => 'El Evento del Calendario Evento es Requerido.',
            'fecha_inicio.required' => 'La Fecha Inicio del Calendario Evento es Requerido.',
            'fecha_fin.required' => 'La Fecha Fin del Calendario Evento es Requerido.',
            
        ];
    }
}
