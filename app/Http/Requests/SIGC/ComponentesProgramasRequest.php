<?php

namespace App\Http\Requests\SIGC;

use App\Http\Requests\Request;

class ComponentesProgramasRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'ano' => 'required',
            'nombre' => 'required',
            'id_programa' => 'required', 
            'unidad' => 'required',
            'efectivo_1' => 'required',
            'efectivo_2' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'ano.required' => 'El Año del Componente es requerido.',
            'nombre.required' => 'El Nombre del Componente es requerido.',
            'id_programa.required' => 'El Programa del Componente es requerido.',
            'unidad.required' => 'La Unidad del Componente es requerida.',
            'efectivo_1.required' => 'El Efectivo 1 del Componente es requerido.',
            'efectivo_2.required' => 'El Efectivo 2 del Componente es requerido.',
        ];
    }
}
