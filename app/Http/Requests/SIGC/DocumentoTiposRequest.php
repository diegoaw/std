<?php

namespace App\Http\Requests\SIGC;

use App\Http\Requests\Request;

class DocumentoTiposRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nombre' => 'required',
            'ano' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El Nombre del Tipo Documento es Requerido.',
            'ano.required' => 'El Año del Tipo Documento es Requerido.',
        ];
    }
}
