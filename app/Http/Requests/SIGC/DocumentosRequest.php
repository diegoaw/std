<?php

namespace App\Http\Requests\SIGC;

use App\Http\Requests\Request;

class DocumentosRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nombre' => 'required',
            'filename' => 'required',
            'tipoDocumento' => 'required',
            'ano' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El Nombre es Requerido.',
            'filename.required' => 'El Archivo es Requerido.',
            'tipoDocumento.required' => 'El Tipo es Requerido.',
            'ano.required' => 'El Año es Requerido.',
        ];
    }
}
