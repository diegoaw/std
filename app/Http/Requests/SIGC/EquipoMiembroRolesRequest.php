<?php

namespace App\Http\Requests\SIGC;

use App\Http\Requests\Request;

class EquipoMiembroRolesRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nombre' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El nombre del Equipo Miembro Rol es requerido.',
        ];
    }
}
