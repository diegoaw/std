<?php

namespace App\Http\Requests\SIGC;

use App\Http\Requests\Request;

class EventosProgramasRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nombre' => 'required',
            'descripcion' => 'required',
            'class' => 'required',
            'encargado' => 'required',
            'suplente' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El Nombre del Evento es requerido.',
            'descripcion.required' => 'La Descripcion del Evento es requerida.',
            'class.required' => 'El color del Evento es requerido.',
            'encargado.required' => 'El Encargado del Evento es requerido.',
            'suplente.required' => 'El Suplente del Evento es requerido.',
        ];
    }
}
