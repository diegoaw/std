<?php

namespace App\Http\Requests\SIGC;

use App\Http\Requests\Request;

class GastosProgramasRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'ano' => 'required',
            'id_programa' => 'required', 
            'fecha' => 'required',
            'ley' => 'required',
            'vigente' => 'required',
            'ejecutados' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'ano.required' => 'El Año del Gasto es requerido.',
            'id_programa.required' => 'El Programa del Gasto es requerido.',
            'fecha.required' => 'La Fecha del Gasto es requerida.',
            'ley.required' => 'La Ley (M$) del Gasto es requerida.',
            'vigente.required' => 'El Vigente (M$) del Gasto es requerido.',
            'ejecutados.required' => 'El Ejecutado (M$) del Gasto es requerido.',
        ];
    }
}
