<?php

namespace App\Http\Requests\SIGC;

use App\Http\Requests\Request;

class IndicadoresProgramasRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nombre' => 'required',
            'ano' => 'required',
            'id_programa' => 'required', 
            'efectivo_1' => 'required',
            'efectivo_2' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El Nombre del Indicador es requerido.',
            'ano.required' => 'El Año del Indicador es requerido.',
            'id_programa.required' => 'El Programa del Indicador es requerido.',
            'efectivo_1.required' => 'El Efectivo 1 del Indicador es requerido.',
            'efectivo_2.required' => 'El Efectivo 2 del Indicador es requerido.',
        ];
    }
}
