<?php

namespace App\Http\Requests\SIGC;

use App\Http\Requests\Request;

class IndicadoresRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'ano' => 'required',
            'nombre' => 'required',
            'tipo' => 'required',
            'numerador' => 'required',
            'categoria' => 'required',
            'multiplicador' => 'required',
            'ponderador' => 'required',
            'meta' => 'required',
            'unidad' => 'required',
            'medio_verificacion' => 'required',
            'notas' => 'required',
            'producto' => 'required',
            'ambito' => 'required',
            'dimension' => 'required',
            'equipo' => 'required',
            'divisionId' => 'required',
            'revisorId' => 'required',
            'reportertId' => 'required',
            'formula' => 'required',




        ];
    }

    public function messages()
    {
        return [
            'ano.required' => 'El año del indicador es requerido.',
            'nombre.required' => 'El nombre del indicador es requerido.',
            'tipo.required' => 'El tipo del indicador es requerido.',
            'numerador.required' => 'El numerador del indicador es requerido.',
            'categoria.required' => 'El categoria del indicador es requerida.',
            'multiplicador.required' => 'El multiplicador del indicador es requerido.',
            'ponderador.required' => 'El ponderador del indicador es requerido.',
            'meta.required' => 'El meta del indicador es requerido.',
            'unidad.required' => 'El unidad del indicador es requerido.',
            'medio_verificacion.required' => 'El medio verificacion del indicador es requerido.',
            'notas.required' => 'El notas del indicador es requerido.',
            'producto.required' => 'El producto del indicador es requerido.',
            'ambito.required' => 'El ambito del indicador es requerido.',
            'dimension.required' => 'El dimension del indicador es requerido.',
            'equipo.required' => 'El equipo del indicador es requerido.',
            'divisionId.required' => 'La División del indicador es requerida.',
            'revisorId.required' => 'El reportador del indicador es requerido.',
            'reportertId.required' => 'El revisor del indicador es requerido.',
            'formula.required' => 'La formula del indicador es requerida.',
        ];
    }
}
