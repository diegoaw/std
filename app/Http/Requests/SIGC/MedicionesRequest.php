<?php

namespace App\Http\Requests\SIGC;

use App\Http\Requests\Request;

class MedicionesRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'valorNumerador' => 'required',
            'analisis' => 'required',
            'acciones' => 'required',
            'denominador' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'valorNumerador.required' => 'El Numerador es Requerido.',
            'analisis.required' => 'El Análisis es Requerido.',
            'acciones.required' => 'Las Acciones son Requeridas.',
            'denominador.required' => 'El Denominador es Requerido.',
        ];
    }
}
