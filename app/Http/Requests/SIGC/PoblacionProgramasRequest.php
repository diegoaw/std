<?php

namespace App\Http\Requests\SIGC;

use App\Http\Requests\Request;

class PoblacionProgramasRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'ano' => 'required',
            'nombre' => 'required',
            'id_programa' => 'required', 
            'unidad' => 'required',
            'efectivo_1' => 'required',
            'efectivo_2' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'ano.required' => 'El Año de la Población es requerido.',
            'nombre.required' => 'El Nombre de la Población es requerido.',
            'id_programa.required' => 'El Programa de la Población es requerido.',
            'unidad.required' => 'La Unidad de la Población es requerida.',
            'efectivo_1.required' => 'El Efectivo 1 de la Población es requerido.',
            'efectivo_2.required' => 'El Efectivo 2 de la Población es requerido.',
        ];
    }
}
