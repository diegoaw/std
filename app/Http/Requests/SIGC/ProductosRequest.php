<?php

namespace App\Http\Requests\SIGC;

use App\Http\Requests\Request;

class ProductosRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nombre' => 'required',
            'ano' => 'required',
            'orden' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El Nombre del Producto es Requerido.',
            'ano.required' => 'El Año del Producto es Requerido.',
            'orden.required' => 'El Orden del Producto es Requerido.',
        ];
    }
}
