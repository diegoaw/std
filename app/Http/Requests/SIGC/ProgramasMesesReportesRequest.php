<?php

namespace App\Http\Requests\SIGC;

use App\Http\Requests\Request;

class ProgramasMesesReportesRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'ano' => 'required',
            'nombre' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'ano.required' => 'El Mes de Reporte es requerido.',
            'nombre.required' => 'El Mes de Reporte es requerido.',
        ];
    }
}
