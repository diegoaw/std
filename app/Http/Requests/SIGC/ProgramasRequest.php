<?php

namespace App\Http\Requests\SIGC;

use App\Http\Requests\Request;

class ProgramasRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'ano' => 'required',
            'nombre' => 'required',
            'proposito' => 'required', 
            'ano_inicio' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'ano.required' => 'El Año del Programa es requerido.',
            'nombre.required' => 'El Nombre del Programa es requerido.',
            'proposito.required' => 'El Propósito del Programa es requerido.',
            'ano_inicio.required' => 'El Año de Inicio del Programa es requerido.',
        ];
    }
}
