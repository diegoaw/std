<?php

namespace App\Http\Requests\SIGC;

use App\Http\Requests\Request;

class TiposRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nombre' => 'required',
            'ano' => 'required',
            'alias' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El Nombre del Tipo es Requerido.',
            'ano.required' => 'El Año del Tipo es Requerido.',
            'alias.required' => 'El Alias del Tipo es Requerido.',
        ];
    }
}
