<?php

namespace App\Http\Requests\Transparencia;

use App\Http\Requests\Request;

class PeriodosFechasRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'fecha_desde' => 'required',
            'fecha_hasta' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'fecha_desde.required' => 'La Fecha Desde es Requerida.',
            'fecha_hasta.required' => 'La Fecha Hasta es Requerida.',
        ];
    }
}
