<?php

namespace App\Http\Requests\Transparencia;

use App\Http\Requests\Request;

class TiposRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nombre' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El Nombre del Tipo es Requerido.',
        ];
    }
}
