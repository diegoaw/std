<?php

namespace App\Http\Requests\ValidarDocumentos;

use App\Http\Requests\Request;
use  App\Models\Seguridad\PasswordRequirements;

class ValidarDocumentosRequest extends Request
{

    public function authorize()
    {
     return true;
    }



    public function rules()
    {
        return [
            'id'=>'required',
            'hash'=>'required'
        ];
    }

    public function messages()
    {
        return [
            'id.required' => 'El id del documento es necesario.',
            'hash.required' => 'El codigo del documento es necesario.',
        ];
    }
}
