<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;


class DistribucionMemoEmail extends Mailable
{
      use Queueable, SerializesModels;

      //public $arregloDatos;

    /** 
     * @return void
     */
    public function __construct($arregloDatos)
    {
         $this->arregloDatos = $arregloDatos;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.distribucion_memoss_email')->with('dat' ,$this->arregloDatos);
    }
}
