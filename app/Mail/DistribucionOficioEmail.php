<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DistribucionOficioEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @return void
     */
    public function __construct($arregloDatos)
    {
         $this->arregloDatos = $arregloDatos;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.distribucion_oficios_email')->with('dat' ,$this->arregloDatos);
    }
}
