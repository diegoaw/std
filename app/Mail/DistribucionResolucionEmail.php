<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DistribucionResolucionEmail extends Mailable
{
      use Queueable, SerializesModels;

    /** use Queueable, SerializesModels;

    public $arregloDatos;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($arregloDatos)
    {
         $this->arregloDatos = $arregloDatos;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.distribucion_resolucionexcenta_email')->with('detalle' ,$this->arregloDatos);
    }
}
