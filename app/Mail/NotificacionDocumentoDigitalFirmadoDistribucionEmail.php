<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificacionDocumentoDigitalFirmadoDistribucionEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($arregloDatos)
    {
        $this->arregloDatos = $arregloDatos;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.documentosdigitales.notificacion_documento_digital_firmado_distribucion')->with('datos' ,$this->arregloDatos);
    }
}
