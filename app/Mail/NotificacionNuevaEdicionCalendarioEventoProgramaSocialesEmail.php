<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificacionNuevaEdicionCalendarioEventoProgramaSocialesEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($arregloDatos) 
    {
        $this->arregloDatos = $arregloDatos;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.sigc.notificacion_nueva_edicion_calendario_evento_programas_sociales')->with('datos' ,$this->arregloDatos);
    }
}
