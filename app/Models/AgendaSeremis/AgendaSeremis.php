<?php

namespace App\Models\AgendaSeremis;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\AgendaSeremis\Region;
use App\Models\AgendaSeremis\Comuna;
use App\User;

class AgendaSeremis extends Model
{
  use SoftDeletes;

  protected $connection = 'mysql';
  protected $table =  'agenda_seremis';
  protected $fillable = ['id', 'id_wsad', 'id_region', 'dia', 'hora', 'lugar', 'id_comuna', 'nombre_actividad', 'organizador', 'asistentes', 'viatico', 'observaciones', 'created_at', 'updated_at', 'deleted_at'];
  protected $primarykey = 'id';

  public function Region()
	{
		$relacion = $this->belongsTo(Region::class, 'id_region', 'id');
		if($relacion){
			$relacion = $this->belongsTo(Region::class, 'id_region', 'id');
		}else{
			$relacion = null;

		}

		return $relacion;
	}

 public function Comuna()
	{
		$relacion = $this->belongsTo(Comuna::class, 'id_comuna', 'id');
		if($relacion){
			$relacion = $this->belongsTo(Comuna::class, 'id_comuna', 'id');
		}else{
			$relacion = null;

		}

		return $relacion;
	}

	public function Seremi()
	{
		$relacion = $this->belongsTo(User::class, 'id_wsad', 'id');
		if($relacion){
			$relacion = $this->belongsTo(User::class, 'id_wsad', 'id');
		}else{
			$relacion = null;

		}

		return $relacion;
	}

}
