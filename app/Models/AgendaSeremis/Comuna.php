<?php

namespace App\Models\AgendaSeremis;

use Illuminate\Database\Eloquent\Model;
use App\Models\AgendaSeremis\Region;

class Comuna extends Model
{
  protected $connection = 'mysql';
  protected $table =  'comuna';
  protected $fillable = ['id', 'nombre', 'id_region', 'cod_area'];
  protected $primarykey = 'id';

  public function Region()
	{
		$relacion = $this->belongsTo(Region::class, 'id_region', 'id');
		if($relacion){
			$relacion = $this->belongsTo(Region::class, 'id_region', 'id');
		}else{
			$relacion = null;

		}

		return $relacion;
	}
    
}