<?php

namespace App\Models\AgendaSeremis;

use Illuminate\Database\Eloquent\Model;
use App\Models\AgendaSeremis\Region;
use App\Models\PyR\PyR;


class PyrHasRegion extends Model
{
    protected $connection = 'mysql';
    protected $table =  'pyr_has_region';
    protected $fillable = ['id', 'id_wsad', 'id_region'];
    protected $primarykey = 'id';

    public function regionesRela()
	{
		$relacion = $this->belongsTo(Region::class, 'id_region', 'id');
		if($relacion){
			$relacion = $this->belongsTo(Region::class, 'id_region', 'id');
		}else{
			$relacion = null;

		}

		return $relacion;
	}

    public function seremisRela()
	{
		$relacion = $this->belongsTo(PyR::class, 'id_wsad', 'id_wsad');
		if($relacion){
			$relacion = $this->belongsTo(PyR::class, 'id_wsad', 'id_wsad');
		}else{
			$relacion = null;

		}

		return $relacion;
	}
    
}