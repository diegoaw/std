<?php

namespace App\Models\AgendaSeremis;

use Illuminate\Database\Eloquent\Model;
use App\Models\AgendaSeremis\Comuna;

class Region extends Model
{
  protected $connection = 'mysql';
  protected $table =  'region';
  protected $fillable = ['id', 'nombre'];
  protected $primarykey = 'id';

  public function Comunas()
	{
	    return $this->hasMany(Comuna::class,'id_region', 'id');
	}

}