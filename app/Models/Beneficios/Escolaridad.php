<?php

namespace App\Models\Beneficios;

use Illuminate\Database\Eloquent\Model;


class Escolaridad extends Model
{
  protected $fillable = ['id', 'fecha_solicitud', 'seleccione', 'cargo', 'division', 'correo', 'rut', 'calidad_juridica', 'grado', 'fecha_ingreso', 'nombre_hijo', 'run_hijo', 'fecha_n_hijo', 'edad_hijo', 'nivel_enseanza', 'certificado_nacimiento', 'alumno_regular', 'declaracion_jurada', 'vigente_servicio', 'comprobante', 'fecha_revision', 'cumple_requisitos', 'observaciones_correccion', 'fecha_correccion']; 
 
}