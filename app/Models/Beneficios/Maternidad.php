<?php

namespace App\Models\Beneficios;

use Illuminate\Database\Eloquent\Model;


class Maternidad extends Model
{
  protected $fillable = ['id', 'nombre_solicitante', 'cargo_solicitante', 'correo_solicitante', 'dependencia_solicitante', 'rut_solicitante', 'calidad_juridica', 'grado_solicitante', 'nombre_menor', 'rut_menor', 'fecha_n_menor', 'certificado_nacimiento', 'declaracion_jurada', 'que_beneficio_optara', 'goza_beneficio', 'cual', 'modalidad', 'modalidad_j', 'nombre_sala', 'nombre_jardin', 'rol_junji', 'rol_junji_j', 'nombre_del_directora', 'nombre_del_directora_j', 'rut_directora', 'rut_directora_j', 'nombre_rl', 'nombre_rl_j', 'rut_rl', 'rut_rl_j', 'direccion_establecimiento', 'direccion_establecimiento_j', 'region_establecimiento', 'region_establecimiento_j', 'telefono_establecimiento', 'telefono_establecimiento_j', 'correo_establecimiento', 'correo_establecimiento_j', 'certificado_junji', 'certificado_junji_j', 'costos_asociados', 'costos_asociados_j', 'antecedentes_legales', 'antecedentes_legales_j', 'lactancia', 'entre1', 'y1', 'entre2', 'y2', 'entrada', 'salida', 'desde', 'hasta', 'pago_excepcional', 'condicion_medica', 'resolucion_teletrabajo', 'declaracion_jurada_simple', 'comprobante_lactancia', 'comprobante_sala', 'comprobante_jardin', 'comprobante_excepcional', 'comprobante_sala_m1', 'comprobante_jardin_m1', 'cumple', 'observaciones', 'observacion_adicional', 'observacion_rechazo', 'adjuntar_resolucion'];
}