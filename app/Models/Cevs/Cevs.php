<?php

namespace App\Models\Cevs;

use Illuminate\Database\Eloquent\Model;


class Cevs extends Model
{
  protected $fillable = ['id', 'fecha', 'correo_electronico' , 'encargado_proyecto_na' , 'region_comuna' , 'prototipo_cevs' , 'direccion_cevs', 'select', 'certificado_dominio_vigente_tpm', 
                         'certificado_hipotecas_tpm', 'autorizacion_consejo_municipal', 'otros_tpm', 'certificado_dominio_vigente_tpt', 'certificado_hipotecas_tpt', 'otros_tpt', 
                         'copia_comodato_tpt', 'observaciones_area_juridica', 'documento_area_juridica', 'certificado_informes_dom', 'certificado_afectacion', 'copia_permisosrecepciones', 
                         'cabida_preliminar', 'terreno_zonas_riesgo', 'situacion_plan_regulador', 'anexo_e', 'otros_archivos_diseno','observaciones_area_diseno', 'documento_area_diseno', 'informe_mecanica_suelos', 
                         'preinforme_mecanica_suelos', 'levantamiento_topografico', 'factibilidad_electrica', 'factibilidad_agua_alcanta', 'factibilidad_gas', 'factibilidad_wifi', 
                         'otros_archivos_especialidades', 'observaciones_area_especialidade', 'documento_area_especialidades', 'politica_comunal_deportiva', 'plan_desarrollo_comunal', 
                         'listado_proyectos_deportivos', 'fotografia_recintos', 'mapas_redes_transporte', 'mapas_comunales', 'zona_no_infraestructura', 'grupo_no_atendido', 
                         'identificar_demandas', 'localizacion_colegios', 'catastro_infraestructura', 'listado_de_organizaciones_deport', 'proximidad_del_terreno', 'documentacion_estudios', 
                         'carta_compromiso_uso', 'segunda_alternativa_terreno', 'funcionamiento_instalaciones_dep', 'otros_archivos_preinversion', 'observaciones_area_preinversion', 
                         'documento_area_preinversion', 'confirmacion_de_documentos', 'adjuntar_archivo_conductor','comprobante'];
}
