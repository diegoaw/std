<?php

namespace App\Models\Colaboracion;

use Illuminate\Database\Eloquent\Model;

class MindepResponsibles extends Model
{
	protected $connection = 'mysqlSigi';
	
    protected $table =  'mindep_responsibles';

    protected $fillable = ['id_usuario', 'rut' , 'nombres' , 'apellido_paterno' , 'apellido_materno', 'genero' , 'n_actividades_impartidas'];

    public $timestamps = false;
}
