<?php

namespace App\Models\CorrelativoDocumentoUnidad;

use Illuminate\Database\Eloquent\Model;

class CorrelativoDocumentoUnidad extends Model
{
	
    protected $table =  'correlativo_documento_division';

    protected $fillable = ['id', 'tipo_documento_id' , 'division_id' , 'tramite_documento_id' , 'correlativo' , 'ano'];

    protected $primarykey = 'id';

    public $timestamps = true;
}
