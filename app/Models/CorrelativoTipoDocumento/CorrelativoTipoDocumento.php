<?php

namespace App\Models\CorrelativoTipoDocumento;

use Illuminate\Database\Eloquent\Model;


class CorrelativoTipoDocumento extends Model
{

	//protected $connection = 'mysqlSimpleApi';
	
    protected $table =  'correlativo_tipo_documento';

    protected $fillable = ['id', 'codigo_tipo_documento' , 'correlativo' , 'ano' , 'tramite_id'];

    protected $primarykey = 'id';

    public $timestamps = true;
}
