<?php 
namespace App\Models\CorreoDistribucionProcesos;

use Illuminate\Database\Eloquent\Model;

class CorreoDistribucionProcesos extends Model
{
	
    protected $table =  'correo_distrubucion_procesos';

    protected $fillable = ['id', 'proceso_simple_id' , 'usuario_id' , 'tramite_id' , 'correo_destino'];

    protected $primarykey = 'id';

    public $timestamps = true;
}