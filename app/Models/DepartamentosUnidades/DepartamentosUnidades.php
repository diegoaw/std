<?php

namespace App\Models\DepartamentosUnidades;

use Illuminate\Database\Eloquent\Model;

class DepartamentosUnidades extends Model
{
	
    protected $table =  'departamentos_unidades';

    protected $fillable = ['id', 'nombre' , 'descripcion'];

    protected $primarykey = 'id';

    public $timestamps = false;
}
