<?php

namespace App\Models\DerivacionDocumental;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;


class Derivaciones extends Model
{
	use SoftDeletes;
	
	protected $connection = 'mysqlDocDigitales';
	protected $table =  'gd_derivaciones';
    protected $fillable = ['id', 'registro_id', 'id_user', 'nombre_desti', 'funcion_desti', 'procedencia_desti', 'created_at', 'updated_at', 'n_folio_opa', 'cierra', 'docdigital', 'ventanilla_unica', 'subir_doc', 'filename_deri', 'url_s3_deri', 'observaciones', 'requiere_respuesta', 'id_prioridad', 'deleted_at'];
    protected $primarykey = 'id';

	public function Iduser()
		{
			return $this->belongsTo(User::class, 'id_user', 'id');
		}
		
	public function Prioridad()
		{
			return $this->belongsTo(Prioridad::class, 'id_prioridad', 'id');
		}

}