<?php

namespace App\Models\DerivacionDocumental;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;


class Distribuciones extends Model
{
	use SoftDeletes;
	
	protected $connection = 'mysqlDocDigitales';
	protected $table =  'gd_distribuciones';
    protected $fillable = ['id', 'registro_id', 'email', 'created_at', 'updated_at', 'recepcionado', 'docdigital', 'ventanilla_unica', 'deleted_at'];
    protected $primarykey = 'id';

	public function Iduser()
		{
			return $this->belongsTo(User::class, 'id_user', 'id');
		}

}