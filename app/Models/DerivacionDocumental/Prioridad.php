<?php

namespace App\Models\DerivacionDocumental;

use Illuminate\Database\Eloquent\Model;

class Prioridad extends Model
{
	
	protected $connection = 'mysqlDocDigitales';
	protected $table =  'gd_prioridad';
    protected $fillable = ['id', 'nombre', 'dias'];
    protected $primarykey = 'id';

}