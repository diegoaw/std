<?php

namespace App\Models\DerivacionDocumental;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\DocumentosDigitales\TiposDocumentosDigitales;
use App\User;


class Registros extends Model
{
	use SoftDeletes;

	protected $connection = 'mysqlDocDigitales';
	protected $table =  'gd_registros';
    protected $fillable = ['id', 'origen', 'id_user', 'nombre_remi', 'funcion_remi', 'procedencia_remi', 'relacion_docs', 'id_tipo_documento', 'n_folio_rela_docs', 'tiene_folio', 'n_folio_deriva', 'asignacion_folio', 'materia', 'filename', 'url_s3', 'tipo_distribucion', 'docdigital', 'ventanilla_unica', 'observaciones', 'requiere_respuesta', 'id_prioridad', 'created_at', 'updated_at', 'deleted_at'];
    protected $primarykey = 'id';


	public function TiposDocumentosDigitales()
		{
			$relacion = $this->belongsTo(TiposDocumentosDigitales::class, 'id_tipo_documento', 'id');
			if($relacion){
				$relacion = $this->belongsTo(TiposDocumentosDigitales::class, 'id_tipo_documento', 'id');
			}else{
				$relacion = null;

			}

			return $relacion;
	}

    public function Iduser()
		{
			return $this->belongsTo(User::class, 'id_user', 'id');
		}

	public function Prioridad()
		{
			return $this->belongsTo(Prioridad::class, 'id_prioridad', 'id');
		}
	
	public function Distribuciones()
		{
			return $this->hasMany(Distribuciones::class, 'registro_id', 'id');
		}

	public function Derivaciones()
		{
			return $this->belongTo(Derivaciones::class, 'registro_id', 'id');
		}

}