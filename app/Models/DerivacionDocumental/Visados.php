<?php

namespace App\Models\DerivacionDocumental;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;


class Visados extends Model
{
	use SoftDeletes;
	
	protected $connection = 'mysqlDocDigitales';
	protected $table =  'gd_visados';
    protected $fillable = ['id', 'registro_id', 'id_user', 'nombre_visa', 'funcion_visa', 'procedencia_visa', 'created_at', 'updated_at', 'n_folio_opa', 'cierra', 'subir_doc', 'filename_visa', 'url_s3_visa', 'observaciones', 'requiere_respuesta', 'id_prioridad', 'deleted_at'];
    protected $primarykey = 'id';

	public function Iduser()
		{
			return $this->belongsTo(User::class, 'id_user', 'id');
		}
		
	public function Prioridad()
		{
			return $this->belongsTo(Prioridad::class, 'id_prioridad', 'id');
		}
}