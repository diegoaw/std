<?php

namespace App\Models\DerivacionesDocumento;

use Illuminate\Database\Eloquent\Model;
use App\Models\IngresoTramite\IngresoTramite;
use App\Models\CorrelativoTipoDocumento\CorrelativoTipoDocumento;

class DerivacionesDocumento extends Model
{
    protected $table =  'derivaciones_documento';

    protected $fillable = ['id', 'tramite_id' , 'remitente' , 'destinatario' , 'fecha' , 'derivacion_n' , 'area_remitente' , 'area_destinatario' , 'observacion', 'documento', 'prioridad'];

    //protected $primarykey = 'id';

    public $timestamps = false;


    public function tramite(){
      return $this->belongsTo(IngresoTramite::class, 'tramite_id', 'tramite_id');

    }

	public function getCorrelativoAttribute()
	{
		$numeroCorrelativo = CorrelativoTipoDocumento::where('tramite_id' , $this->tramite_id)->where('derivacion_n' , $this->derivacion_n)->first();
		if($numeroCorrelativo){
	    	return $numeroCorrelativo->correlativo;
		}else{
			return '';
		}
	}
}
