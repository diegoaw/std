<?php

namespace App\Models\Divisiones;

use Illuminate\Database\Eloquent\Model;

class Divisiones extends Model
{
	
    protected $table =  'divisiones';

    protected $fillable = ['id', 'nombre' , 'descripcion'];

    protected $primarykey = 'id';

    public $timestamps = false;
}
