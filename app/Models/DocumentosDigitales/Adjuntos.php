<?php

namespace App\Models\DocumentosDigitales;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Adjuntos extends Model
{
	use SoftDeletes;

        protected $connection = 'mysqlDocDigitales';
		protected $table =  'adjuntos';
		protected $fillable = ['id', 'descripcion', 'filename', 'url_aws', 'created_at', 'updated_at', 'deleted_at'];
		protected $primarykey = 'id';
	
}