<?php

namespace App\Models\DocumentosDigitales;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AnexosDocumentosDigitales extends Model
{
		use SoftDeletes;

        protected $connection = 'mysqlDocDigitales';
		protected $table =  'anexos_documentos_digitales';
		protected $fillable = ['id', 'id_registro_documentos_digitales', 'id_derivacion', 'estatus', 'filename2', 'url_s3', 'created_at', 'updated_at', 'deleted_at'];
		protected $primarykey = 'id';

}