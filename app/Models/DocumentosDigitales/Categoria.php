<?php

namespace App\Models\DocumentosDigitales;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
	
        protected $connection = 'mysqlDocDigitales';
		protected $table =  'categoria';
		protected $fillable = ['id', 'nombre', 'descripcion', 'estatus'];
		protected $primarykey = 'id';

}