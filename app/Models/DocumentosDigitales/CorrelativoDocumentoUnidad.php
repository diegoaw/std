<?php

namespace App\Models\DocumentosDigitales;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable; 

class CorrelativoDocumentoUnidad extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

	protected $connection = 'mysqlgeneral';
	
    protected $table =  'correlativo_documento_division';

    protected $fillable = ['id', 'tipo_documento_id', 'id_registro' , 'correlativo' , 'ano'];

    protected $primarykey = 'id';

    public $timestamps = true;
}
