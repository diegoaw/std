<?php

namespace App\Models\DocumentosDigitales;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable; 
use App\Models\DocumentosDigitales;
use App\User;
use Carbon\Carbon;



class CorrelativoDocumentosDigitales extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

        protected $connection = 'mysqlDocDigitales';
        protected $table =  'correlativo_documentos_digitales';
        protected $fillable = ['id', 'tipo_documento_id', 'id_user', 'id_registro' , 'reservado_distribucion', 'correlativo' , 'ano' , 'lugar','motivo_reserva','motivo_cambio', 'reservado', 'estatus', 'materia', 'documento', 'tipo_distribucion', 'docdigital', 'distribucion_observacion', 'distribuido', 'created_at', 'updated_at'];
        protected $primarykey = 'id';
        public $timestamps = true;

        public function tipoDocumento()
		{
			return $this->belongsTo(TiposDocumentosDigitales::class, 'tipo_documento_id', 'id');
		}
        public function Iduser()
		{
			return $this->belongsTo(User::class, 'id_user', 'id');
		}

        public function getfechasAttribute()
        {
    
            $fechas=  $this->created_at->format('Y-m-d');
            return $fechas;
    
        }
        

        public function getNombrecAttribute()
        {
    
            return $this->correlativo.' - '.$this->lugar.' - '.$this->fechas;
    
        }

        public function getNombreccAttribute()
        {
    
            return $this->correlativo.' - '.$this->Iduser->name.' - '.$this->lugar.' - '.$this->fechas;
    
        }

        
        
}
