<?php

namespace App\Models\DocumentosDigitales;

use App\User;
use App\Models\DocumentosDigitales\EstatusDerivacionRegistroDocumentosDigitales;
use App\Models\DocumentosDigitales\RegistroDocumentosDigitales;
use OwenIt\Auditing\Contracts\Auditable; 


use Illuminate\Database\Eloquent\Model;

class DerivacionRegistroDocumentosDigitales extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;
	
        protected $connection = 'mysqlDocDigitales';
		protected $table =  'derivacion_registro_documentos_digitales';
		protected $fillable = ['id', 'id_registro_documentos_digitales', 'id_estatus_derivacion_registro_documentos_digitales', 'id_user_emisor', 'id_user_destino', 'id_user_generador', 'observacion', 'created_at', 'updated_at'];
		protected $primarykey = 'id';

		public function userdestino()
		{
			return $this->hasOne(User::class,'id', 'id_user_destino');
		}

		public function useremisor()
		{
			return $this->hasOne(User::class,'id', 'id_user_emisor');
		}

		public function usergenerador()
		{
			return $this->hasOne(User::class,'id', 'id_user_generador');
		}

		public function estatus()
		{
			return $this->hasOne(EstatusDerivacionRegistroDocumentosDigitales::class,'id', 'id_estatus_derivacion_registro_documentos_digitales');
		}

		public function tipo()
		{
			return $this->hasOne(RegistroDocumentosDigitales::class,'id', 'id_registro_documentos_digitales');
		}

		public function anexo()
		{
			return $this->hasOne(AnexosDocumentosDigitales::class,'id_derivacion', 'id');
		}
}