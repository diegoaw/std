<?php

namespace App\Models\DocumentosDigitales;

use Illuminate\Database\Eloquent\Model;

class EstatusDerivacionRegistroDocumentosDigitales extends Model
{
	
        protected $connection = 'mysqlDocDigitales';
		protected $table =  'estatus_derivacion_registro_documentos_digitales';
		protected $fillable = ['id', 'nombre', 'descripcion'];
		protected $primarykey = 'id';

}