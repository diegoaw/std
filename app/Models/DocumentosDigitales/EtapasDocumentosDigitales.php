<?php

namespace App\Models\DocumentosDigitales;

use Illuminate\Database\Eloquent\Model;

class EtapasDocumentosDigitales extends Model
{
	
        protected $connection = 'mysqlDocDigitales';
		protected $table =  'etapas_documentos_digitales';
		protected $fillable = ['id', 'nombre', 'descripcion'];
		protected $primarykey = 'id';

}