<?php

namespace App\Models\DocumentosDigitales;
use App\User;

use Illuminate\Database\Eloquent\Model;

class FirmantesSub extends Model
{
	
        protected $connection = 'mysqlDocDigitales';
		protected $table =  'firmantes_sub';
		protected $fillable = ['id', 'id_user', 'email', 'nombre', 'rut', 'digito_v', 'division','id_lugar','lugar'];
		protected $primarykey = 'id';

		public function user()
		{
			$relacion = $this->belongsTo(User::class, 'id_user', 'id');
			if($relacion){
				$relacion = $this->belongsTo(User::class, 'id_user', 'id');
			}else{
				$relacion = null;

			}

			return $relacion;
		}

		public function getNombressAttribute()
		{

			return $this->nombre.' - '.$this->division;

		}

}