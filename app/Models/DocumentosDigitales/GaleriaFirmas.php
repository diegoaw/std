<?php

namespace App\Models\DocumentosDigitales;
use App\User;

use Illuminate\Database\Eloquent\Model;

class GaleriaFirmas extends Model
{
	
        protected $connection = 'mysqlDocDigitales';
		protected $table =  'galeria_firmas';
		protected $fillable = ['id', 'nombre', 'ruta', 'id_secretaria'];
		protected $primarykey = 'id';

}