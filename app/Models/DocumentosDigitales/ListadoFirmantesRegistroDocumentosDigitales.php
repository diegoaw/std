<?php

namespace App\Models\DocumentosDigitales;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\DocumentosDigitales\RegistroDocumentosDigitales;
use App\Models\DocumentosDigitales\VistosBuenos;
use App\Models\DocumentosDigitales\Firmantes;
use App\Models\DocumentosDigitales\FirmantesSub;
use OwenIt\Auditing\Contracts\Auditable; 


class ListadoFirmantesRegistroDocumentosDigitales extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;	
	use SoftDeletes;
	
        protected $connection = 'mysqlDocDigitales';
		protected $table =  'listado_firmantes_registro_documentos_digitales';
		protected $fillable = ['id', 'id_registro_documento_digitales', 'id_user_firmante', 'id_user_crea_tabla', 'estatus', 'sub', 'created_at', 'updated_at', 'deleted_at'];
		protected $primarykey = 'id';

		public function RegistroDocumentos()
		{
			$relacion = $this->belongsTo(RegistroDocumentosDigitales::class, 'id_registro_documento_digitales', 'id');
			if($relacion){
				$relacion = $this->belongsTo(RegistroDocumentosDigitales::class, 'id_registro_documento_digitales', 'id');
			}else{
				$relacion = null;

			}

			return $relacion;
		}


		public function username()
		{
			if($this->sub == 0){
				$relacion = $this->belongsTo(Firmantes::class, 'id_user_firmante', 'id_user');
				if($relacion){
					$relacion = $this->belongsTo(Firmantes::class, 'id_user_firmante', 'id_user');
				}else{
					$relacion = null;

				}
		    }else{
				$relacion = $this->belongsTo(FirmantesSub::class, 'id_user_firmante', 'id_user');
				if($relacion){
					$relacion = $this->belongsTo(FirmantesSub::class, 'id_user_firmante', 'id_user');
				}else{
					$relacion = null;
	
				}
			}

			return $relacion;
		}

		

		public function getVistosAttribute()
		{
			$vistos = VistosBuenos::where('id_registro_documento_digitales', $this->id_registro_documento_digitales)->where('estatus', 0)->get(); 
			if(count($vistos)>0){
				return false;
			}else{
				return true;
			}

		}
}