<?php

namespace App\Models\DocumentosDigitales;

use Illuminate\Database\Eloquent\Model;

class Lugar extends Model 
{
	protected $connection = 'mysqlDocDigitales';
	
    protected $table =  'lugar';

    protected $fillable = ['id', 'nombre'];

    protected $primarykey = 'id';
}
