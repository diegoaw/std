<?php

namespace App\Models\DocumentosDigitales;

use Illuminate\Database\Eloquent\Model;

class Plantillas extends Model
{

        protected $connection = 'mysqlDocDigitales';
		protected $table =  'plantillas';
		protected $fillable = ['id', 'nombre', 'tipo_doc', 'link', 'link_blocqueada'];
		protected $primarykey = 'id';
	
}