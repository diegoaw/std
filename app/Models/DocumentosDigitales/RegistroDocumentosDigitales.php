<?php

namespace App\Models\DocumentosDigitales;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\DocumentosDigitales\TiposDocumentosDigitales;
use App\Models\DocumentosDigitales\EtapasDocumentosDigitales;
use App\Models\DocumentosDigitales\CorrelativoDocumentosDigitales;
use OwenIt\Auditing\Contracts\Auditable; 
use App\User;


class RegistroDocumentosDigitales extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;
	use SoftDeletes;

	protected $connection = 'mysqlDocDigitales';
	protected $table =  'registro_documentos_digitales';
	protected $fillable = ['id', 'n_memo', 'id_tipos_documentos_digitales', 'id_categoria', 'reservado_distribucion', 'titulo', 'descripcion', 'derivacion', 'destinatario_memo', 'distribuye',  'correos_distri', 'id_user', 'id_etapas_documentos_digitales', 'estatus', 'lugar', 'code', 'hash_autenticador', 'filename', 'url_s3', 'url_vista', 'url_correlativo', 'url_firmado', 'distribuido', 'created_at', 'updated_at', 'deleted_at'];
	protected $primarykey = 'id';

	public function TiposDocumentosDigitales()
		{
			$relacion = $this->belongsTo(TiposDocumentosDigitales::class, 'id_tipos_documentos_digitales', 'id');
			if($relacion){
				$relacion = $this->belongsTo(TiposDocumentosDigitales::class, 'id_tipos_documentos_digitales', 'id');
			}else{
				$relacion = null;

			}

			return $relacion;
	}

	public function Correlativo()
		{
			$relacion = $this->belongsTo(CorrelativoDocumentosDigitales::class, 'id', 'id_registro');
			if($relacion){
				$relacion = $this->belongsTo(CorrelativoDocumentosDigitales::class, 'id', 'id_registro');
			}else{
				$relacion = null;

			}

			return $relacion;
	}

	public function Iduser()
		{
			return $this->belongsTo(User::class, 'id_user', 'id');
		}

	public function CategoriaDocumentosDigitales()
		{
			$relacion = $this->belongsTo(Categoria::class, 'id_categoria', 'id');
			if($relacion){
				$relacion = $this->belongsTo(Categoria::class, 'id_categoria', 'id');
			}else{
				$relacion = null;

			}

			return $relacion;
	}

	public function EtapaDocumentosDigitales()
		{
			$relacion = $this->belongsTo(EtapasDocumentosDigitales::class, 'id_etapas_documentos_digitales', 'id');
			if($relacion){
				$relacion = $this->belongsTo(EtapasDocumentosDigitales::class, 'id_etapas_documentos_digitales', 'id');
			}else{
				$relacion = null;

			}

			return $relacion;
	}

}