<?php

namespace App\Models\DocumentosDigitales;

use Illuminate\Database\Eloquent\Model;

class TiposDocumentosDigitales extends Model
{
	
        protected $connection = 'mysqlDocDigitales';
		protected $table =  'tipos_documentos_digitales';
		protected $fillable = ['id', 'nombre', 'descripcion', 'estatus'];
		protected $primarykey = 'id';

}