<?php

namespace App\Models\DocumentosDigitales;

use Illuminate\Database\Eloquent\Model;

class VistosBuenos extends Model
{
	
        protected $connection = 'mysqlDocDigitales';
		protected $table =  'vistos_buenos';
		protected $fillable = ['id', 'id_user', 'email', 'nombre', 'id_registro_documento_digitales', 'estatus'];
		protected $primarykey = 'id';

}