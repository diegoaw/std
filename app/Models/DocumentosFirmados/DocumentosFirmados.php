<?php

namespace App\Models\DocumentosFirmados;

use Illuminate\Database\Eloquent\Model;
use App\Models\TiposDocumentos\TiposDocumentos;
use App\Models\TipoFirma\TipoFirma;
use GuzzleHttp\Client;
use App\Models\DocumentosFirmadosFirmantes\DocumentosFirmadosFirmantes;
class DocumentosFirmados extends Model
{
		
    protected $table =  'documentos_firmados';

    protected $fillable = ['id', 'nombre' , 'descripcion' , 'id_tipo_firma' , 'rut_firmante' , 'folio_documento' , 'id_tipo_documento' , 'reservado' , 'url_documento_no_firmado' , 'url_documento_firmado' , 'code' , 'hash_autenticador' , 'autorizacion' ];

    protected $primarykey = 'id';

    public function tipoDocumento()
	{
	    return $this->belongsTo(TiposDocumentos::class, 'id_tipo_documento', 'id');
	}
	public function tipoFirma()
	{
	    return $this->belongsTo(TipoFirma::class, 'id_tipo_firma', 'id');
	}

	public function getReservadoCAttribute()
	{
		if($this->reservado==0){
			return 'No';
		}else{
			return 'Si';
		}
	}

	public function firmantes()
	{
      return $this->hasMany(DocumentosFirmadosFirmantes::class, 'documento_firmado_id'); 
    }


}
