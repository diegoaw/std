<?php

namespace App\Models\DocumentosFirmadosFirmantes;

use Illuminate\Database\Eloquent\Model;
use App\Models\TiposDocumentos\TiposDocumentos;
use App\Models\TipoFirma\TipoFirma;
use GuzzleHttp\Client;
class DocumentosFirmadosFirmantes extends Model
{
		
    protected $table =  'documento_firmado_firmantes';

    protected $fillable = ['id', 'documento_firmado_id' , 'documento_firmado_code' , 'rut_firmante' , 'nombre_firmante', 'usuario_id'];

    protected $primarykey = 'id';

   
}
