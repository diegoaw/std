<?php

namespace App\Models\File;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
	protected $connection = 'mysqlSimple';
	
    protected $table =  'file';

    protected $fillable = ['id', 'filename' , 'tipo' , 'llave' , 'llave_copia', 'llave_firma' , 'validez' , 'tramite_id' , 'created_at' , 'update_at' , 'validez_habiles' , 'extra' , 'campo_id'];

    //protected $primarykey = 'id';

    public $timestamps = false;
}
