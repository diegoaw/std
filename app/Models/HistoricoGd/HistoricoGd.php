<?php

namespace App\Models\HistoricoGd;

use Illuminate\Database\Eloquent\Model;


class HistoricoGd extends Model
{
	protected $table =  'historico_gd';

    protected $fillable = ['TablaDB', 'Expediente', 'Cod_Departamento', 'Departamento', 'Division', 'Cod_Usuario', 'Rut', 'Dv', 'Nombre', 'Usuario', 'Cargo', 'Correo', 'FechaCarpeta', 'FechaCarpetaAgno', 'FechaCarpetaMes', 'EsIngresoExterno', 'OrigendelDocto', 'Remitente', 'CargoRemitente', 'Procedencia', 'Cod_Tipo', 'Tipo', 'Numero', 'Contenido', 'TipoContenido', 'Archivo', 'ArchivoEstado', 'Cod_UsuarioArchivo', 'UsuarioArchivoRut', 'UsuarioArchivoDv', 'UsuarioArchivoDvNombre', 'UsuarioArchivo'];

    protected $primarykey = 'id';

    public $timestamps = false;


    public function getUrlArchivoAttribute(){
    	
		   return str_replace("\\\\natacion\GESTION_DOCUMENTAL\Documentos\\", 'https://mindep.s3.amazonaws.com/HistoricoGD/', $this->Archivo);
    }

	
}

