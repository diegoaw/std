<?php

namespace App\Models\HistoricoGdArchivos;

use Illuminate\Database\Eloquent\Model;


class HistoricoGdArchivos extends Model
{
	protected $table =  'historico_gd_archivos';

    protected $fillable = ['ano', 'confidencialidad', 'clasificacion', 'materia', 'fecha_modificacion', 'archivo_nombre', 'ruta'];

    protected $primarykey = 'id';

    public $timestamps = false;


}
