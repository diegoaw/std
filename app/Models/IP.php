<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IP extends Model
{

	
    protected $table =  'ip';

    protected $fillable = ['id', 'ipaddress'];

    protected $primarykey = 'id';

    public $timestamps = false;
}
