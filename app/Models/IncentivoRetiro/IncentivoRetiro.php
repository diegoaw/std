<?php

namespace App\Models\IncentivoRetiro;

use Illuminate\Database\Eloquent\Model;


class IncentivoRetiro extends Model
{
  protected $fillable = ['id', 'fecha_solicitud', 'seleccione', 'cargo', 'division', 'correo', 'rut', 'calidad_juridica', 'grado', 'fecha_nacimiento', 'rango', 'selec_previ', 'nombre_afp_18', 'nombre_afp', 'afiliacion_afp_18', 'afiliacion_afp', 'cotixaciones_afp', 'certificado_antiguedad', 'manifiesto', 'declaracion_jurada', 'carta_renuncia', 'genero', 'certificado_nacimiento', 'edad_f', 'edad_m', 'edad_o', 'formulario_firmado', 'comprobante', 'fecha_revision', 'cumple_requisitos', 'observaciones_correccion', 'fecha_correccion']; 
 
}