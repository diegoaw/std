<?php

namespace App\Models\IngresoTramite;
use App\Models\DerivacionesDocumento\DerivacionesDocumento;
use App\Models\CorrelativoTipoDocumento\CorrelativoTipoDocumento;

use Illuminate\Database\Eloquent\Model;

class IngresoTramite extends Model
{
    protected $table =  'ingreso_tramite';

    protected $fillable = ['id', 'tramite_id' , 'origen', 'docdigital', 'remitente' , 'cargo' , 'area_remitente' , 'tramite_relacion_id' , 'tipo_documento' , 'docto_n'  , 'contenido' , 'documento' , 'url'];

   // protected $primarykey = 'id';

    public function derivaciones(){
      return $this->hasMany(DerivacionesDocumento::class, 'tramite_id', 'tramite_id'); 
    }

	public function correlativo()
	{
	    return $this->belongsTo(CorrelativoTipoDocumento::class, 'tramite_id', 'tramite_id');
	}


}
