<?php

namespace App\Models\Integridad;

use Illuminate\Database\Eloquent\Model;


class Consulta extends Model
{
  protected $fillable = ['id', 'nombres_solicitante', 'apellidos_solicitante', 'correo_solicitante', 'rut', 'unidad_o_seremia', 'opcion_sexual', 'otra_cual', 'tipo_de_consulta', 'consulta', 'antecedente_1', 'antecedente_2', 'descargar'];
}