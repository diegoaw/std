<?php

namespace App\Models\Integridad;

use Illuminate\Database\Eloquent\Model;


class Denuncia extends Model
{
  protected $fillable = ['id', 'nombres_solicitante', 'apellidos_solicitante', 'correo_solicitante', 'rut', 'unidad_o_seremia', 'opcion_sexual', 'indique', 'telefono_movil', 'direccion', 'tipo_de_denuncia', 'especifique', 'fecha_inicio', 'fecha_fin', 'archivo_de_respaldo_1',  'archivo_de_respaldo_2', 'archivo_de_respaldo_3', 'descargar', 'fecha_de_recepcion', 'observacion', 'fecha_de_respuesta', 'resultado', 'respuesta', 'adjuntar_doc_final', 'comprobante_aprobacion', 'comprobante_rechazo'];
}