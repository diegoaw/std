<?php

namespace App\Models\Memo;

use Illuminate\Database\Eloquent\Model;
use  App\Models\MemoColaboraciones\MemoColaboraciones;
use  App\Models\CorrelativoDocumentoUnidad\CorrelativoDocumentoUnidad;
use App\Models\UsuariosSimple\UsuariosSimple;

class Memo extends Model
{
	
    protected $table =  'memo';

    protected $fillable = ['id', 'tramite_id' , 'division' , 'unidad', 'lugar' , 'fecha' , 'destinatario' , 'cargo_destinatario' , 'remitente' , 'cargo_remitente' , 'materia', 'texto' , 'vistos' , 'distribucion' , 'anexos' , 'doc_anexo_a' , 'doc_anexo_b' , 'doc_anexo_c' , 'doc_anexo_d' , 'doc_memo', 'reservado'];

    protected $primarykey = 'id';

    public $timestamps = true;


    public function getEstatusAttribute()
	{
		if($this->doc_memo){
	    	return 'Completado';
		}else{
			return 'Incompleto';
		}
	}

	public function colaboraciones()
	{
      return $this->hasMany(MemoColaboraciones::class, 'tramite_id', 'tramite_id'); 
    }
    public function correlativo()
	{
	    return $this->belongsTo(CorrelativoDocumentoUnidad::class, 'tramite_id', 'tramite_documento_id');
	}

	public function getEmailRemitenteAttribute()
	{
		
		$email = UsuariosSimple::where('nombre_cargo',$this->remitente)->first();
		return $email;
	   	
	}


}
