<?php

namespace App\Models\MemoColaboraciones;

use Illuminate\Database\Eloquent\Model;
use  App\Models\CorrelativoDocumentoUnidad\CorrelativoDocumentoUnidad;
use App\Models\UsuariosSimple\UsuariosSimple;

class MemoColaboraciones extends Model
{
	
    protected $table =  'memo_colaboradores';

    protected $fillable = ['id', 'id_referencia', 'tramite_id' , 'division' , 'lugar' , 'fecha' , 'destinatario' , 'remitente' , 'materia', 'texto' , 'vistos' , 'distribucion' , 'anexos' , 'colaborador', 'lista_colaboradores', 'observacion_general', 'colaborador_siguiente', 'doc_anexo_a' , 'doc_anexo_b' , 'doc_anexo_c' , 'doc_memo'];

    protected $primarykey = 'id';

    public $timestamps = true;

    public function correlativo()
	{
	    return $this->belongsTo(CorrelativoDocumentoUnidad::class, 'tramite_id', 'tramite_documento_id');
	}

	public function getEmailRemitenteAttribute()
	{
		$email = UsuariosSimple::where('nombre_cargo',$this->remitente)->first()->email;
		return $email;
	   	
	}

}
