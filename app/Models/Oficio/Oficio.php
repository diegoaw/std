<?php

namespace App\Models\Oficio;

use Illuminate\Database\Eloquent\Model;
use  App\Models\OficioColaboracion\OficioColaboracion;
use  App\Models\CorrelativoDocumentoUnidad\CorrelativoDocumentoUnidad;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\UsuariosSimple\UsuariosSimple;

class Oficio extends Model
{
	use SoftDeletes;

    protected $table =  'oficio';

    protected $fillable = ['id', 'tramite_id' , 'division' , 'lugar' , 'correlativo', 'categoria', 'fecha' , 'fecha_folio', 'remitente' , 'destinatario', 'desde' , 'antecedentes', 'materia', 'contenido' , 'vistos_responsabilidad' , 'distribucion' , 'doc_anexo_a' , 'doc_anexo_b' , 'doc_anexo_c' , 'doc_oficio', 'deleted_at', 'documento_distribuido'];

    protected $primarykey = 'id';

    public $timestamps = true;


    public function getEstatusAttribute()
	{
		if($this->doc_oficio!=null && $this->doc_oficio!=" "){
	    	return 'Completado';
		}else{
			return 'Incompleto';
		}
	}

	public function colaboraciones()
	{
      return $this->hasMany(OficioColaboracion::class, 'tramite_id', 'tramite_id');
    }
    public function correlativo()
	{
		$relacion = $this->belongsTo(CorrelativoDocumentoUnidad::class, 'tramite_id', 'tramite_documento_id');
		if($relacion){
			return $relacion;
			}
			else{
				return '';
			}

	    //return $this->belongsTo(CorrelativoDocumentoUnidad::class, 'tramite_id', 'tramite_documento_id');
	}

	public function getEmailRemitenteAttribute()
	{
		$email = UsuariosSimple::where('nombre_cargo',$this->remitente)->first();
		return $email;

	}

}
