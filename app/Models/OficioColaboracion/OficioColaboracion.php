<?php

namespace App\Models\OficioColaboracion;

use Illuminate\Database\Eloquent\Model;
use  App\Models\CorrelativoDocumentoUnidad\CorrelativoDocumentoUnidad;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\UsuariosSimple\UsuariosSimple;

class OficioColaboracion extends Model
{
	use SoftDeletes;
	
    protected $table =  'oficio_colaboradores';

    protected $fillable = ['id', 'tramite_id' , 'division' , 'lugar' , 'fecha' , 'fecha_folio', 'remitente' , 'destinatario', 'desde' , 'antecedentes', 'materia', 'contenido' , 'vistos_responsabilidad' , 'distribucion' , 'colaborador', 'lista_colaboradores', 'observacion_general', 'colaborador_siguiente', 'doc_anexo_a' , 'doc_anexo_b' , 'doc_anexo_c' , 'doc_oficio', 'deleted_at'];


    protected $primarykey = 'id';

    public $timestamps = true;

public function correlativo()
	{
		$relacion = $this->belongsTo(CorrelativoDocumentoUnidad::class, 'tramite_id', 'tramite_documento_id');
		if($relacion){
			return $relacion;
			}
			else{
				return '';
			}

	    //return $this->belongsTo(CorrelativoDocumentoUnidad::class, 'tramite_id', 'tramite_documento_id');
	}

	public function getEmailRemitenteAttribute()
	{
		$email = UsuariosSimple::where('nombre_cargo',$this->remitente)->first()->email;
		return $email;
	   	
	}
	
}
