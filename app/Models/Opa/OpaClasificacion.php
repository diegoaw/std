<?php

namespace App\Models\Opa;

use Illuminate\Database\Eloquent\Model;

class OpaClasificacion extends Model
{
  protected $connection = 'mysql';
  protected $table =  'opa_clasificacion';
  protected $fillable = ['id','nombre'];
  protected $primarykey = 'id';

}
