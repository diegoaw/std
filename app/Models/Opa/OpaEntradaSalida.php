<?php

namespace App\Models\Opa;
use Illuminate\Database\Eloquent\Model;
use App\Models\Opa\OpaTipo;
use App\Models\Opa\OpaClasificacion;
use App\Models\Opa\OpaTipoDocumentacion;
use App\Models\Opa\OpaModalidadDocumento;
use App\Models\Opa\OpaPlataformaDocumento;

class OpaEntradaSalida extends Model
{
    protected $connection = 'mysql';
    protected $table =  'opa_entrada_salida';
    protected $fillable = ['id','id_opa_tipo','id_opa_clasificacion','de','de_correo','para','para_correo','descripcion','estado','id_opa_tipo_documentacion','id_opa_modalidad_documento','id_opa_plataforma_documento','n_doc','alta_prioridad','retirado','enviado','fecha_cierre','en_ruta','empaquetado'];
    protected $primarykey = 'id';

    public function OpaTipo()
    {
      $relacion = $this->belongsTo(OpaTipo::class, 'id_opa_tipo', 'id');
      if($relacion){
        $relacion = $this->belongsTo(OpaTipo::class, 'id_opa_tipo', 'id');
      }else{
        $relacion = null;
      }
      return $relacion;
    }

    public function OpaClasificacion()
    {
      $relacion = $this->belongsTo(OpaClasificacion::class, 'id_opa_clasificacion', 'id');
      if($relacion){
        $relacion = $this->belongsTo(OpaClasificacion::class, 'id_opa_clasificacion', 'id');
      }else{
        $relacion = null;
      }
      return $relacion;
    }

    public function OpaTipoDocumentacion()
    {
      $relacion = $this->belongsTo(OpaTipoDocumentacion::class, 'id_opa_tipo_documentacion', 'id');
      if($relacion){
        $relacion = $this->belongsTo(OpaTipoDocumentacion::class, 'id_opa_tipo_documentacion', 'id');
      }else{
        $relacion = null;
      }
      return $relacion;
    }

    public function OpaModalidadDocumento()
    {
      $relacion = $this->belongsTo(OpaModalidadDocumento::class, 'id_opa_modalidad_documento', 'id');
      if($relacion){
        $relacion = $this->belongsTo(OpaModalidadDocumento::class, 'id_opa_modalidad_documento', 'id');
      }else{
        $relacion = null;
      }
      return $relacion;
    }

    public function OpaPlataformaDocumento()
    {
      $relacion = $this->belongsTo(OpaPlataformaDocumento::class, 'id_opa_plataforma_documento', 'id');
      if($relacion){
        $relacion = $this->belongsTo(OpaPlataformaDocumento::class, 'id_opa_plataforma_documento', 'id');
      }else{
        $relacion = null;
      }
      return $relacion;
    }

}
