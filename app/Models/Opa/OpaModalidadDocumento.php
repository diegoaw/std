<?php

namespace App\Models\Opa;

use Illuminate\Database\Eloquent\Model;

class OpaModalidadDocumento extends Model
{
  protected $connection = 'mysql';
  protected $table =  'opa_modalidad_documento';
  protected $fillable = ['id','nombre'];
  protected $primarykey = 'id';
}
