<?php

namespace App\Models\Opa;

use Illuminate\Database\Eloquent\Model;

class OpaPlataformaDocumento extends Model
{
  protected $connection = 'mysql';
  protected $table =  'opa_plataforma_documento';
  protected $fillable = ['id','nombre'];
  protected $primarykey = 'id';
}
