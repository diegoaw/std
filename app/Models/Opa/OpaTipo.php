<?php

namespace App\Models\Opa;

use Illuminate\Database\Eloquent\Model;

class OpaTipo extends Model
{
  protected $connection = 'mysql';
  protected $table =  'opa_tipo';
  protected $fillable = ['id','nombre'];
  protected $primarykey = 'id';

}
