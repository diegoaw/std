<?php

namespace App\Models\Opa;

use Illuminate\Database\Eloquent\Model;

class OpaTipoDocumentacion extends Model
{
  protected $connection = 'mysql';
  protected $table =  'opa_tipo_documentacion';
  protected $fillable = ['id','nombre'];
  protected $primarykey = 'id';
}
