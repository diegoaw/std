<?php

namespace App\Models\ParametrosSistemas;

use Illuminate\Database\Eloquent\Model;

class ParametrosSistema extends Model
{
	
        protected $connection = 'mysqlDocDigitales';
		protected $table =  'parametros_sistema';
		protected $fillable = ['id', 'nombre', 'descripcion', 'tabla_relacionada' , 'valor'];
		protected $primarykey = 'id';

}