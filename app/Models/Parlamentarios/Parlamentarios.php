<?php

namespace App\Models\Parlamentarios;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Parlamentarios extends Model
{
  use SoftDeletes;

  protected $connection = 'mysql';
  protected $table =  'viaje_parlamentarios';
  protected $fillable = ['id', 'ministerio', 'nombre_actividad', 'pais', 'ciudad', 'fecha_realizacion', 'parlamentario_invitado', 'institucion_convoca', 'fecha_inicio', 'fecha_fin', 'observacion', 'created_at', 'updated_at', 'deleted_at'];
  protected $primarykey = 'id';

}
