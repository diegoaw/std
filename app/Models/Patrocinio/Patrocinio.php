<?php

namespace App\Models\Patrocinio;

use Illuminate\Database\Eloquent\Model;


class Patrocinio extends Model
{
  protected $fillable = ['id', 'nombre' , 'telefono' , 'direccion' , 'comuna' , 'region', 'email', 'descripcion', 'objetivos', 'fines_de_lucro', 'libre_acceso',
  'si_requiere_pago', 'lugar_de_desarrollo', 'fecha_de_desarrollo', 'postulando_a_otros_beneficios', 'capacidad_de_atraer_demanda', 'posicionamiento_del_pais', 'tipo_de_persona',
  'tipo_de_organizacion', 'evento', 'si_requiere_pago', 'carta_ministra', 'fotocopia_cedula', 'ficha_de_solicitud', '1_a', '2_a', '3_a', '4_a', '1_b', '2_b', '3_b', '4_b', '5_b', '6_b', '7_b', '1_c', '2_c', '3_c',
  '4_c', '5_c', '6_c', '7_c', '1_d', '2_d', '3_d', '4_d', '1_e', '2_e', '1_f', '2_f', '3_f', '4_f', '5_f', '1_g', '2_g', '3_g' ];
}
