<?php

namespace App\Models\Protocolo;

use Illuminate\Database\Eloquent\Model;


class Protocolo extends Model
{
  protected $fillable = ['id', 'nombrecompleto' , 'email', 'telefono', 'mensaje', 'archivo_adjuntar', 'mensaje_respuesta', 'adjuntar_archivo'];
}
