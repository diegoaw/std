<?php

namespace App\Models\PyR;

use Illuminate\Database\Eloquent\Model;

class PyR extends Model
{

        protected $connection = 'mysqlpyr';
		protected $table =  'personas';
        protected $fillable = ['id','periodo','mes','codigo_persona','nombres','apellido_paterno','apellido_materno','sexo', 'fecha_nacimiento', 'edad', 'estado_civil', 'email', 'profesion', 'nacionalidad', 'activo', 'funcion', 'cargo', 'planta', 'tipo_funcionario', 'calidad_juridica', 'grado', 'unidad_desempeno', 'id_grupo'];
        public $timestamps = false;


		public function getNombrescAttribute()
    {

        return $this->apellido_paterno.' '.$this->nombres.' '.' - '.$this->email;

    }

    public function getNameAttribute()
    {

        return $this->nombres.' '.$this->apellido_paterno.' '.$this->apellido_materno.' '.'  -  '.$this->unidad_desempeno;

    }
}