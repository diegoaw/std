<?php

namespace App\Models\Reclamos;

use Illuminate\Database\Eloquent\Model;


class Reclamos extends Model
{
  protected $fillable = ['id', 'servicio_bien' , 'nombre', 'region', 'comuna', 'numero', 'correo_electronico', 'mensaje'];
}
