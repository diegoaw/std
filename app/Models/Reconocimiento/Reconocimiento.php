<?php

namespace App\Models\Reconocimiento;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reconocimiento extends Model
{
  use SoftDeletes;
  protected $fillable = ['id', 'tipo_de_persona', 'apellidos_solicitante' , 'email_solicitante' , 'fecha_solicitud' , 'nombres_solicitante' , 'rut_solicitante', 'personalidad_juridica', 'domicilio', 'region', 'mision', 'vision', 'objetivos', 'rut_del_representante_legal', 'nombre_del_representante_legal', 'carta', 'documentos', 'condiciones'];
}
