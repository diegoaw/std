<?php

namespace App\Models\RequerimientoCompras;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActaRecepcionConforme extends Model
{
  use SoftDeletes;
  protected $fillable = ['id', 'acta_recepcion', 'titulocompra', 'adjuntar_certificado', 'orden_de_compra', 'adjuntar_factura', 'grupo', 'api_area', 'correo_acta', 'agregar_observaciones', 'nombre_de_proveedor', 'numero_de_orcom', 'fecha_orden_compra', 'n_factura', 'fecha_factura', 'monto_factura', 'listado_requerimiento', 'declaracion', 'dependencia', 'subrogante', 'firma', 'archivo_a', 'archivo_b', 'archivo_c', 'archivo_d', 'archivo_e', 'listado_requerimiento', 'declaracion', 'fecha_recepcion', 'subrogante', 'archivo_a', 'archivo_b', 'archivo_c', 'archivo_d', 'archivo_e', 'act_rc' ];
}