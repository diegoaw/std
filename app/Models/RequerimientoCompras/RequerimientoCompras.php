<?php

namespace App\Models\RequerimientoCompras;

use Illuminate\Database\Eloquent\Model;
use  App\Models\MemoColaboraciones\MemoColaboraciones;
use  App\Models\CorrelativoDocumentoUnidad\CorrelativoDocumentoUnidad;
use App\Models\UsuariosSimple\UsuariosSimple;
use Illuminate\Database\Eloquent\SoftDeletes;


class RequerimientoCompras extends Model
{
  	use SoftDeletes;
    protected $fillable = ['id', 'origen_requerimiento', 'estado' , 'fecha_requerimiento', 'usuario_asignado_etapa', 'con_cargo_a', 'titulo_compra', 'unidad_requirente', 'pasaje_aereo', 'etapa_actual', 'estado_etapa_actual', 'fecha_modificacion', 'monto', 'listado_requerimiento'  , 'estado_aprobacion', 'fecha_inicio', 'fecha_termino' , 'fecha_modificacion', 'tiempo_transcurrido' , 'fecha_modificacion' , 'etapas' ];

    
}
