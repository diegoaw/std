<?php

namespace App\Models\ResolucionExcentaColaboracion;

use Illuminate\Database\Eloquent\Model;
use  App\Models\CorrelativoDocumentoUnidad\CorrelativoDocumentoUnidad;
use App\Models\UsuariosSimple\UsuariosSimple;

class ResolucionExcentaColaboracion extends Model
{
	
      protected $table =  'resolucion_exenta_colaboradores';

    protected $fillable = ['id', 'tramite_id' , 'division' , 'lugar' , 'fecha' , 'fecha_folio', 'remitente' , 'referencia', 'considerando' , 'resuelvo', 'vistos' , 'vistos_responsabilidad' , 'distribucion' , 'colaborador', 'lista_colaboradores', 'observacion_general', 'colaborador_siguiente', 'doc_anexo_a' , 'doc_anexo_b' , 'doc_anexo_c' , 'doc_resolucion_exenta'];


    protected $primarykey = 'id';

    public $timestamps = true;

    public function correlativo()
	{
		$relacion = $this->belongsTo(CorrelativoDocumentoUnidad::class, 'tramite_id', 'tramite_documento_id');
		if($relacion){
			return $relacion;
			}
			else{
				return '';
			}

	    //return $this->belongsTo(CorrelativoDocumentoUnidad::class, 'tramite_id', 'tramite_documento_id');
	}

	public function getEmailRemitenteAttribute()
	{
		$email = UsuariosSimple::where('nombre_cargo',$this->remitente)->first()->email;
		return $email;
	   	
	}
	
}
