<?php

namespace App\Models\ResolucionExenta;

use Illuminate\Database\Eloquent\Model;
use  App\Models\ResolucionExcentaColaboracion\ResolucionExcentaColaboracion;
use  App\Models\CorrelativoDocumentoUnidad\CorrelativoDocumentoUnidad;
use App\Models\UsuariosSimple\UsuariosSimple;

class ResolucionExenta extends Model
{
	
    protected $table =  'resolucion_exenta';

    protected $fillable = ['id', 'tramite_id' , 'division' , 'lugar' , 'fecha' , 'fecha_folio', 'remitente' , 'referencia', 'considerando' , 'resuelvo', 'vistos' , 'vistos_responsabilidad' , 'distribucion' , 'doc_anexo_a' , 'doc_anexo_b' , 'doc_anexo_c' , 'doc_resolucion_exenta', 'documento_distribuido'];

    protected $primarykey = 'id';

    public $timestamps = true;


    public function getEstatusAttribute()
	{
		if($this->doc_resolucion_exenta!=null && $this->doc_resolucion_exenta!=" "){
	    	return 'Completado';
		}else{
			return 'Incompleto';
		}
	}

	public function colaboraciones()
	{
      return $this->hasMany(ResolucionExcentaColaboracion::class, 'tramite_id', 'tramite_id'); 
    }
    public function correlativo()
	{
		$relacion = $this->belongsTo(CorrelativoDocumentoUnidad::class, 'tramite_id', 'tramite_documento_id');
		if($relacion){
			return $relacion;
			}
			else{
				return '';
			}

	    //return $this->belongsTo(CorrelativoDocumentoUnidad::class, 'tramite_id', 'tramite_documento_id');
	}

	public function getEmailRemitenteAttribute()
	{
		$email = UsuariosSimple::where('nombre_cargo',$this->remitente)->first()->email;
		return $email;
	   	
	}

}
