<?php

namespace App\Models\Seguridad;
use App\User;

use Illuminate\Database\Eloquent\Model;


class Audits extends Model
{
     
    protected $connection = 'mysqlgeneral';
    protected $table= 'audits';
    protected $fillable = ['user_type', 'user_id', 'event', 'auditable_type', 'auditable_id', 'old_values', 'new_values', 'url', 'ip_address', 'user_agent', 'tags']; 
    protected $primarykey = 'id';
    public $timestamps = true;

    public function user()
		{
			$relacion = $this->belongsTo(User::class, 'user_id', 'id');
			if($relacion){
				$relacion = $this->belongsTo(User::class, 'user_id', 'id');
			}else{
				$relacion = null;

			}

			return $relacion;
		}
   
}