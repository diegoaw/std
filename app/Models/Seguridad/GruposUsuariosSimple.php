<?php

namespace App\Models\Seguridad;

use Illuminate\Database\Eloquent\Model;

class GruposUsuariosSimple extends Model
{
     
    protected $connection = 'mysqlSimple';
    protected $table= 'grupo_usuarios';
    protected $fillable = ['nombre', 'cuenta_id']; 
    protected $primarykey = 'id';
    public $timestamps = false;
   
}
