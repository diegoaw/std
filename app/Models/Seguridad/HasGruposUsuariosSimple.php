<?php

namespace App\Models\Seguridad;

use Illuminate\Database\Eloquent\Model;

class HasGruposUsuariosSimple extends Model
{
     
    protected $connection = 'mysqlSimple';
    protected $table= 'grupo_usuarios_has_usuario';
    protected $fillable = ['grupo_usuarios_id', 'usuario_id']; 
    public $timestamps = false;
   
}
