<?php

namespace App\Models\Siac;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Siac extends Model
{
  use SoftDeletes;
  protected $fillable = ['id', 'origen_requerimiento', 'cual_origen', 'tipificacion', 'detalle_solicitud', 'nombre_completo', 'nacionalidad', 'indique_nacionalidad', 'pasaporte', 'rut', 'pais_residencia', 'region', 'calle_casa', 'direccion_completa', 'telefono', 'correo2', 'zona', 'nivel_educacional', 'ocupacion', 'participa_organizacion', 'cual_organizacion', 'respuesta_mediante', 'correo', 'direccion_carta'];
}
