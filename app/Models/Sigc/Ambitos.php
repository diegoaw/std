<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;

class Ambitos extends Model
{
		protected $connection = 'mysqlSIGC';
		protected $table =  'ambitos';
    protected $fillable = ['id','nombre'];
    protected $primarykey = 'id';

}
