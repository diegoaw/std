<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;

class AmbitosTabla extends Model
{
		protected $connection = 'mysqlSIGC';
		protected $table =  'ambitos';
    protected $fillable = ['id','nombre'];
		protected $primarykey = 'id';

}
