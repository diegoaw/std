<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;

class Archivos extends Model
{
		protected $connection = 'mysqlSIGC';
		protected $table =  'medicion_archivos';
    protected $fillable = ['id','medicion_id','filename','visible'];
    protected $primarykey = 'id';
}
