<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;

class BeneficiariosActual extends Model
{
    protected $connection = 'mysqlprogramas';
    protected $table =  'beneficiarios_2022';
    protected $fillable = ['id', 'id_usuario', 'id_activity_registrations', 'fecha_inicio', 'fecha_termino', 'id_programa', 'programa', 'id_componente', 'componente', 'id_subcomponente', 'subcomponente', 'id_producto', 'producto', 'cod_region', 'latitud', 'longitud', 'altitud', 'geo_orden_region', 'region', 'cod_provincia', 'provincia', 'cod_comuna', 'comuna', 'deporte_disciplina', 'tipo_documento', 'rut_identificador', 'pais', 'genero', 'edad', 'cod_comuna_recinto', 'comuna_recinto', 'cod_region_recinto', 'region_recinto', 'asistencia_promedio_fecha'];
    protected $primarykey = 'id';

}