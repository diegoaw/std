<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;
use App\Models\Sigc\Meses;

class Calendario extends Model
{
    protected $connection = 'mysqlSIGC';
	  protected $table =  'calendarios';
    protected $fillable = ['id','ano','mes','rep_desde','rep_hasta','rev_desde','rev_hasta','corrige_desde','corrige_hasta','rev_correccion_desde','rev_correccion_hasta'];
    protected $primarykey = 'id';

    public function Meses()
  {
    $relacion = $this->belongsTo(Meses::class, 'mes', 'id');
    if($relacion){
      $relacion = $this->belongsTo(Meses::class, 'mes', 'id');
    }else{
      $relacion = null;

    }

    return $relacion;
  }

}
