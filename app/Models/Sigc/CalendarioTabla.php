<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;

class CalendarioTabla extends Model
{
		protected $connection = 'mysqlSIGC';
    protected $table =  'calendarios';
    protected $fillable = ['id','ano','mes','rep_desde','rep_hasta','rev_desde','rev_hasta','corrige_desde','corrige_hasta','rev_correccion_desde','rev_correccion_hasta'];
		protected $primarykey = 'id';

}
