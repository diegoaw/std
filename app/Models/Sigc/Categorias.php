<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;

class Categorias extends Model
{

		protected $connection = 'mysqlSIGC';
		protected $table =  'categorias';
    	protected $fillable = ['id','nombre'];
		protected $primarykey = 'id';

}
