<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;

class CategoriasTabla extends Model
{
		protected $connection = 'mysqlSIGC';
		protected $table =  'categorias';
		protected $fillable = ['id','nombre'];
		protected $primarykey = 'id';

}
