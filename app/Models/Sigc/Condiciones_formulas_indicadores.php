<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;

class Condiciones_formulas_indicadores extends Model
{

	protected $connection = 'mysqlSIGC';
	protected $table =  'condiciones_formulas_indicadores';
	protected $fillable = ['id','id_indicador','tipo_formula','validacion','estatus'];
	protected $primarykey = 'id';

}
