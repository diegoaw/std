<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;
use App\Models\Sigc\Notificaciones;

class CorreoNotificaciones extends Model
{
		protected $connection = 'mysqlSIGC';
		protected $table =  'correo_notificaciones';
    protected $fillable = ['id','id_notificaciones','destintario'];
    protected $primarykey = 'id';

    public function Notificaciones()
  {
    $relacion = $this->belongsTo(Notificaciones::class, 'id_notificaciones', 'id');
    if($relacion){
      $relacion = $this->belongsTo(Notificaciones::class, 'id_notificaciones', 'id');
    }else{
      $relacion = null;

    }

    return $relacion;
  }

}
