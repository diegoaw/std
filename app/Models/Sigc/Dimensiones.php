<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;

class Dimensiones extends Model
{

	protected $connection = 'mysqlSIGC';
	protected $table =  'dimensiones';
	protected $fillable = ['id','nombre'];
	protected $primarykey = 'id';

}
