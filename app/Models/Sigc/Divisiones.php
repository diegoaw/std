<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;

class Divisiones extends Model
{
		protected $connection = 'mysqlSIGC';
		protected $table =  'divisiones';
    protected $fillable = ['id','nombre'];
    protected $primarykey = 'id';



}
