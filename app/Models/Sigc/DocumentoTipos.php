<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;

class DocumentoTipos extends Model
{

		protected $connection = 'mysqlSIGC';
		protected $table =  'documento_tipos';
		protected $fillable = ['id', 'nombre', 'ano'];
		protected $primarykey = 'id';

		public function getNombreaAttribute()
	{

	    	return $this->ano.' - '.$this->nombre;

	}

}
