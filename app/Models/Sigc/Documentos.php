<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;

class Documentos extends Model
{
		protected $connection = 'mysqlSIGC';
		protected $table =  'documentos';
		protected $fillable = ['id', 'nombre', 'filename', 'documento_tipo_id', 'ano'];
		protected $primarykey = 'id';

    public function DocumentoTipos()
  {
    $relacion = $this->belongsTo(DocumentoTipos::class, 'documento_tipo_id', 'id');
      if($relacion){
        $relacion = $this->belongsTo(DocumentoTipos::class, 'documento_tipo_id', 'id');
      }else{
        $relacion = null;
      }
    return $relacion;
  }

}
