<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;

class EquipoMiembroRoles extends Model
{

    protected $connection = 'mysqlSIGC';
		protected $table =  'equipo_miembro_roles';
		protected $fillable = ['id', 'nombre'];
		protected $primarykey = 'id';


}
