<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;

class EquipoMiembros extends Model
{

    protected $connection = 'mysqlSIGC';
		protected $table =  'equipo_miembros';
		protected $fillable = ['id', 'equipo_id', 'equipo_miembro_rol_id', 'user_id' , 'estado'];
		protected $primarykey = 'id';


}
