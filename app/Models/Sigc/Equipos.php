<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;
use App\Models\Sigc\Users as UsersSigc;


class Equipos extends Model
{
	protected $connection = 'mysqlSIGC';
	protected $table =  'equipos';
	protected $fillable = ['id','nombre','ano'];
	protected $primarykey = 'id';

	public function getNombreCompletoAttribute()
	{

	    	return $this->ano.' - '.$this->id.' - '.$this->nombre;

	}

	public function users()
		 {
				 return $this->belongsToMany(UsersSigc::class, 'equipo_miembros','equipo_id','user_id')
					->withPivot('user_id','equipo_id');
		 }

}
