<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;

class EquiposTabla extends Model
{
	protected $connection = 'mysqlSIGC';
	protected $table =  'equipos';
	protected $fillable = ['id','nombre','ano'];
	protected $primarykey = 'id';


}
