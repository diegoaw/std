<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;

class Eventos extends Model
{
	    //protected $fillable = ['titulo', 'descripcion', 'fecha' , 'color'];
			protected $connection = 'mysqlSIGC';
			protected $table =  'events';
			protected $fillable = ['id','title','start_date','end_date','calendario_id','etapa_id','class'];
			protected $primarykey = 'id';


			public function Calendario()
			{
				$relacion = $this->belongsTo(Calendario::class, 'calendario_id', 'id');
				if($relacion){
					$relacion = $this->belongsTo(Calendario::class, 'calendario_id', 'id');
				}else{
					$relacion = null;

				}

				return $relacion;
			}


}
