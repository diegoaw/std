<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;

class HasRoles extends Model
{

    protected $connection = 'mysqlSIGC';
		protected $table =  'has_roles';
		protected $fillable = ['id', 'user_id', 'role_id'];
		protected $primarykey = 'id';


    public function UserID()
  {
    $relacion = $this->belongsToMany(Users::class, 'user_id', 'id');
    if($relacion){
      $relacion = $this->belongsToMany(Users::class, 'user_id', 'id');
    }else{
      $relacion = null;

    }

    return $relacion;
  }

  public function RoleID()
{
  $relacion = $this->belongsToMany(Roles::class, 'role_id', 'id');
  if($relacion){
    $relacion = $this->belongsToMany(Roles::class, 'role_id', 'id');
  }else{
    $relacion = null;

  }

  return $relacion;
}

}
