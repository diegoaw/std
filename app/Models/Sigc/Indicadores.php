<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;
use App\Models\Sigc\Tipos;
use App\Models\Sigc\Equipos;
use App\Models\Sigc\Categorias;
use App\Models\Sigc\Unidades;
use App\Models\Sigc\Productos;
use App\Models\Sigc\Ambitos;
use App\Models\Sigc\Dimensiones;
use App\Models\Sigc\Divisiones;
use App\Models\Sigc\Users;


class Indicadores extends Model
{
	
	protected $connection = 'mysqlSIGC';
	protected $table =  'indicadores';
    protected $fillable = ['id', 'ano', 'nombre', 'numerador', 'denominador', 'multiplicador', 'ponderador', 'meta', 'unidad_id', 'supuestos', 'medio_verif', 'notas', 'producto_id', 'ambito_id', 'dimension_id', 'tipo_id', 'equipo_id', 'reportert_id', 'reporters_id', 'responsablet_id', 'revisor_id', 'responsable_id','reportador_id','reportadores_id', 'division_id','categoria_id','formula','unidad_id_meta'];


		public function getNombreCompletoAttribute()
		{

					return $this->ano.' - '.$this->id.' - '.$this->nombre;

		}


    public function Tipo()
	{
		$relacion = $this->belongsTo(Tipos::class, 'tipo_id', 'id');
		if($relacion){
			$relacion = $this->belongsTo(Tipos::class, 'tipo_id', 'id');
		}else{
			$relacion = null;

		}

		return $relacion;
	}

	 public function Equipo()
	{
		$relacion = $this->belongsTo(Equipos::class, 'equipo_id', 'id');
		if($relacion){
			$relacion = $this->belongsTo(Equipos::class, 'equipo_id', 'id');
		}else{
			$relacion = null;

		}

		return $relacion;
	}

	 public function Categoria()
	{
		$relacion = $this->belongsTo(Categorias::class, 'categoria_id', 'id');
		if($relacion){
			$relacion = $this->belongsTo(Categorias::class, 'categoria_id', 'id');
		}else{
			$relacion = null;

		}

		return $relacion;
	}

	 public function Unidad()
	{
		$relacion = $this->belongsTo(Unidades::class, 'unidad_id', 'id');
		if($relacion){
			$relacion = $this->belongsTo(Unidades::class, 'unidad_id', 'id');
		}else{
			$relacion = null;

		}

		return $relacion;
	}


	public function UnidadMeta()
 {
	 $relacion = $this->belongsTo(Unidades::class, 'unidad_id_meta', 'id');
	 if($relacion){
		 $relacion = $this->belongsTo(Unidades::class, 'unidad_id_meta', 'id');
	 }else{
		 $relacion = null;

	 }

	 return $relacion;
 }

	 public function Producto()
	{
		$relacion = $this->belongsTo(Productos::class, 'producto_id', 'id');
		if($relacion){
			$relacion = $this->belongsTo(Productos::class, 'producto_id', 'id');
		}else{
			$relacion = null;

		}

		return $relacion;
	}

	public function Ambito()
	{
		$relacion = $this->belongsTo(Ambitos::class, 'ambito_id', 'id');
		if($relacion){
			$relacion = $this->belongsTo(Ambitos::class, 'ambito_id', 'id');
		}else{
			$relacion = null;

		}

		return $relacion;
	}

	public function Dimension()
	{
		$relacion = $this->belongsTo(Dimensiones::class, 'dimension_id', 'id');
		if($relacion){
			$relacion = $this->belongsTo(Dimensiones::class, 'dimension_id', 'id');
		}else{
			$relacion = null;

		}

		return $relacion;
	}

	public function Division()
{
	$relacion = $this->belongsTo(Divisiones::class, 'division_id', 'id');
	if($relacion){
		$relacion = $this->belongsTo(Divisiones::class, 'division_id', 'id');
	}else{
		$relacion = null;

	}

	return $relacion;
}

	public function Reportador()
	{
		$relacion = $this->belongsTo(Users::class, 'reportert_id', 'persona_id');
		if($relacion){
			$relacion = $this->belongsTo(Users::class, 'reportert_id', 'persona_id');
		}else{
			$relacion = null;

		}

		return $relacion;
	}

	public function Encargado()
	{
		$relacion = $this->belongsTo(Users::class, 'reporters_id', 'persona_id');
		if($relacion){
			$relacion = $this->belongsTo(Users::class, 'reporters_id', 'persona_id');
		}else{
			$relacion = null;

		}

		return $relacion;
	}

	public function Responsable()
	{
		$relacion = $this->belongsTo(Users::class, 'responsablet_id', 'persona_id');
		if($relacion){
			$relacion = $this->belongsTo(Users::class, 'responsablet_id', 'persona_id');
		}else{
			$relacion = null;

		}

		return $relacion;
	}

	public function Revisor()
	{
		$relacion = $this->belongsTo(Users::class, 'revisor_id', 'id');
		if($relacion){
			$relacion = $this->belongsTo(Users::class, 'revisor_id', 'id');
		}else{
			$relacion = null;

		}

		return $relacion;
	}

	public function Responsable1()
	{
		$relacion = $this->belongsTo(Users::class, 'responsable_id', 'id');
		if($relacion){
			$relacion = $this->belongsTo(Users::class, 'responsable_id', 'id');
		}else{
			$relacion = null;

		}

		return $relacion;
	}

	public function Reportador1()
	{
		$relacion = $this->belongsTo(Users::class, 'reportador_id', 'id');
		if($relacion){
			$relacion = $this->belongsTo(Users::class, 'reportador_id', 'id');
		}else{
			$relacion = null;

		}

		return $relacion;
	}

	public function Reportadores1()
	{
		$relacion = $this->belongsTo(Users::class, 'reportadores_id', 'id');
		if($relacion){
			$relacion = $this->belongsTo(Users::class, 'reportadores_id', 'id');
		}else{
			$relacion = null;

		}

		return $relacion;
	}

}
