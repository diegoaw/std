<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;

class MedicionArchivos extends Model
{
	protected $connection = 'mysqlSIGC';
	protected $table =  'medicion_archivos';
    protected $fillable = ['medicion_id','filename','visible', 'filename_aws'];
    protected $primarykey = 'id';

}
