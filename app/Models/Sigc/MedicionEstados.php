<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;

class MedicionEstados extends Model
{
		protected $connection = 'mysqlSIGC';
		protected $table =  'medicion_estados';
    protected $fillable = ['id','nombre'];
    protected $primarykey = 'id';

}
