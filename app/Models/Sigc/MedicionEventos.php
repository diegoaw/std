<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;

class MedicionEventos extends Model
{
		protected $connection = 'mysqlSIGC';
		protected $table =  'medicion_eventos';
    protected $fillable = ['id','nombre'];
    protected $primarykey = 'id';

}
