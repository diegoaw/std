<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;
use App\Models\Sigc\Indicadores;
use App\Models\Sigc\Calendario;
use App\Models\Sigc\Mediciones;

class MedicionHistoricos extends Model
{
			protected $connection = 'mysqlSIGC';
			protected $table =  'medicion_historicos';
			protected $fillable = ['id', 'medicion_id', 'calendario_id', 'indicador_id', 'fecha', 'valor_numerador', 'analisis', 'acciones', 'medicion_evento_id', 'revision_fecha', 'revision_obs', 'denominador' , 'usuario'];
			protected $primarykey = 'id';

      public function Medicion()
    {
      $relacion = $this->belongsTo(Mediciones::class, 'medicion_id', 'id');
      if($relacion){
        $relacion = $this->belongsTo(Mediciones::class, 'medicion_id', 'id');
      }else{
        $relacion = null;

      }

      return $relacion;
    }

      public function Calendario()
    {
      $relacion = $this->belongsTo(Calendario::class, 'calendario_id', 'id');
      if($relacion){
        $relacion = $this->belongsTo(Calendario::class, 'calendario_id', 'id');
      }else{
        $relacion = null;

      }

      return $relacion;
    }

      public function Indicador()
    {
      $relacion = $this->belongsTo(Indicadores::class, 'indicador_id', 'id');
      if($relacion){
        $relacion = $this->belongsTo(Indicadores::class, 'indicador_id', 'id');
      }else{
        $relacion = null;

      }

      return $relacion;
    }

    public function MedicionEvento()
  {
    $relacion = $this->belongsTo(MedicionEventos::class, 'medicion_evento_id', 'id');
    if($relacion){
      $relacion = $this->belongsTo(MedicionEventos::class, 'medicion_evento_id', 'id');
    }else{
      $relacion = null;

    }

    return $relacion;
  }

    public function getMesCalendarioAttribute()
    {
        return $this->Calendario->mes;
    }


}
