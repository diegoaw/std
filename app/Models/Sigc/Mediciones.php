<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;
use App\Models\Sigc\Indicadores;
use App\Models\Sigc\Calendario;
use App\Models\Sigc\MedicionEstados;

class Mediciones extends Model
{
			protected $connection = 'mysqlSIGC';
			protected $table =  'mediciones';
			//protected $fillable = ['id', 'calendario_id', 'indicador_id', 'fecha', 'valor_numerador', 'analisis', 'acciones', 'medicion_estado_id', 'revision_fecha', 'revision_obs', 'denominador', 'mes', 'nombre_indicador', 'nombre_medicion_estado', 'indicadores_tipo_id', 'indicadores_equipo_id'];
			protected $fillable = ['id', 'calendario_id', 'indicador_id', 'fecha', 'valor_numerador', 'analisis', 'acciones', 'medicion_estado_id', 'revision_fecha', 'revision_obs', 'denominador' , 'resualtado', 'resultado_formula_ponderacion', 'resultado_formula_meta'];
			protected $primarykey = 'id';

			public function Calendario()
		{
			$relacion = $this->belongsTo(Calendario::class, 'calendario_id', 'id');
			if($relacion){
				$relacion = $this->belongsTo(Calendario::class, 'calendario_id', 'id');
			}else{
				$relacion = null;

			}

			return $relacion;
		}

			public function Indicador()
		{
			$relacion = $this->belongsTo(Indicadores::class, 'indicador_id', 'id');
			if($relacion){
				$relacion = $this->belongsTo(Indicadores::class, 'indicador_id', 'id');
			}else{
				$relacion = null;

			}

			return $relacion;
		}

		public function MedicionEstado()
	{
		$relacion = $this->belongsTo(MedicionEstados::class, 'medicion_estado_id', 'id');
		if($relacion){
			$relacion = $this->belongsTo(MedicionEstados::class, 'medicion_estado_id', 'id');
		}else{
			$relacion = null;

		}

		return $relacion;
	}

		public function getMesCalendarioAttribute()
		{
				return $this->Calendario->mes;
		}

		public function Archivos()
	{
	    return $this->hasMany(Archivos::class,'medicion_id', 'id');
	}

	public function getNombreCompletosAttribute()
	{

				return $this->Indicador->ano.' - '.$this->indicador_id.' - '.$this->Indicador->nombre;

	}

}
