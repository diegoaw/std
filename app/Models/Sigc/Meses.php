<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;

class Meses extends Model
{
	protected $connection = 'mysqlSIGC';
	protected $table =  'meses';
	protected $fillable = ['id', 'nombre', 'abreviacion'];
	protected $primarykey = 'id';


	public function getMesNombreAttribute()
	{

	    	return $this->nombre;

	}

}
