<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;

class Notificaciones extends Model
{
		protected $connection = 'mysqlSIGC';
		protected $table =  'notificaciones';
    protected $fillable = ['id','notificacion'];
    protected $primarykey = 'id';

}
