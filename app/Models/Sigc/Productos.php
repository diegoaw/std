<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
	   
		protected $connection = 'mysqlSIGC';
		protected $table =  'productos';
		protected $fillable = ['id', 'ano', 'nombre', 'orden'];
		protected $primarykey = 'id';

}
