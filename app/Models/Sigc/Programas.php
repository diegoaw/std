<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Programas extends Model
{
    use SoftDeletes;
		protected $connection = 'mysqlSIGC';
		protected $table =  'programas';
    protected $fillable = ['id', 'ano', 'nombre', 'proposito', 'ano_inicio', 'created_at' ,'updated_at', 'deleted_at'];
    protected $primarykey = 'id';

}
