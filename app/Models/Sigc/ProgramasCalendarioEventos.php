<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;
use App\Models\Sigc\ProgramasMesesReportes;
use App\Models\Sigc\ProgramasEventos;
use Illuminate\Database\Eloquent\SoftDeletes;


class ProgramasCalendarioEventos extends Model
{
    use SoftDeletes;
		protected $connection = 'mysqlSIGC';
		protected $table =  'programas_calendario_eventos';
    protected $fillable = ['id','ano', 'mes', 'id_programas_eventos', 'fecha_inicio', 'fecha_fin',  'editar' , 'created_at' ,'updated_at','deleted_at'];
    protected $primarykey = 'id';

    public function Meses()
    {
      $relacion = $this->belongsTo(ProgramasMesesReportes::class, 'mes', 'id');
      if($relacion){
        $relacion = $this->belongsTo(ProgramasMesesReportes::class, 'mes', 'id');
      }else{
        $relacion = null;

      }

      return $relacion;
    }

    public function Evento()
    {
      $relacion = $this->belongsTo(ProgramasEventos::class, 'id_programas_eventos', 'id');
      if($relacion){
        $relacion = $this->belongsTo(ProgramasEventos::class, 'id_programas_eventos', 'id');
      }else{
        $relacion = null;

      }

      return $relacion;
    }

}
