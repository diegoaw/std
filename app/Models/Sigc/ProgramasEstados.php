<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;

class ProgramasEstados extends Model
{
    protected $connection = 'mysqlSIGC';
    protected $table =  'programas_estados';
    protected $fillable = ['id','nombre'];
    protected $primarykey = 'id';

}
