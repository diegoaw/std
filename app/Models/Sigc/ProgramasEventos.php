<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;
use App\Models\Sigc\Users;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProgramasEventos extends Model
{
    use SoftDeletes;
	protected $connection = 'mysqlSIGC';
	protected $table =  'programas_eventos';
    protected $fillable = ['id','nombre', 'descripcion', 'class', 'editar', 'encargado', 'suplente', 'habilitar_suplencia', 'created_at' ,'updated_at', 'deleted_at'];
    protected $primarykey = 'id';

    public function Encargado()
	{
		$relacion = $this->belongsTo(Users::class, 'encargado', 'id');
		if($relacion){
			$relacion = $this->belongsTo(Users::class, 'encargado', 'id');
		}else{
			$relacion = null;

		}

		return $relacion;

  }
	

  public function Suplente()
	{
		$relacion = $this->belongsTo(Users::class, 'suplente', 'id');
		if($relacion){
			$relacion = $this->belongsTo(Users::class, 'suplente', 'id');
		}else{
			$relacion = null;

		}

		return $relacion;
	}

}
