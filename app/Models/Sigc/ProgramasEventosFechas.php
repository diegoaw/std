<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ProgramasEventosFechas extends Model
{
    use SoftDeletes;

		protected $connection = 'mysqlSIGC';
		protected $table =  'programas_eventos_fechas';
    protected $fillable = ['id','title', 'description', 'start_date', 'end_date' ,'programas_calendario_eventos_id', 'etapa_id', 'created_at' ,'updated_at', 'class','deleted_at'];
    protected $primarykey = 'id';

    public function Calendario()
			{
				$relacion = $this->belongsTo(ProgramasCalendarioEventos::class, 'programas_calendario_eventos_id', 'id');
				if($relacion){
					$relacion = $this->belongsTo(ProgramasCalendarioEventos::class, 'programas_calendario_eventos_id', 'id');
				}else{
					$relacion = null;

				}

				return $relacion;
			}


}
