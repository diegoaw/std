<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;
use App\Models\Sigc\Programas;
use App\Models\Sigc\ProgramasComponentes;
use Illuminate\Database\Eloquent\SoftDeletes;


class ProgramasIndicadores extends Model
{
    use SoftDeletes;
		protected $connection = 'mysqlSIGC';
		protected $table =  'programas_indicadores';
    protected $fillable = ['id', 'ano', 'nombre', 'id_programa', 'id_componente', 'id_proposito', 'efectivo_1','efectivo_2', 'created_at' ,'updated_at', 'deleted_at'];
    protected $primarykey = 'id';

    public function Programas()
    {
      $relacion = $this->belongsTo(Programas::class, 'id_programa', 'id');
      if($relacion){
        $relacion = $this->belongsTo(Programas::class, 'id_programa', 'id');
      }else{
        $relacion = null;

      }

      return $relacion;
    }

    public function Componentes()
    {
      $relacion = $this->belongsTo(ProgramasComponentes::class, 'id_componente', 'id');
      if($relacion){
        $relacion = $this->belongsTo(ProgramasComponentes::class, 'id_componente', 'id');
      }else{
        $relacion = null;

      }

      return $relacion;
    }

}
