<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ProgramasMesesReportes extends Model
{
    use SoftDeletes;
		protected $connection = 'mysqlSIGC';
		protected $table =  'programas_meses_reportes';
    protected $fillable = ['id', 'ano', 'nombre', 'created_at' ,'updated_at', 'deleted_at'];
    protected $primarykey = 'id';

}
