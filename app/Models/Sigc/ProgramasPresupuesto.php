<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;
use App\Models\Sigc\Programas;
use Illuminate\Database\Eloquent\SoftDeletes;


class ProgramasPresupuesto extends Model
{
    use SoftDeletes;
		protected $connection = 'mysqlSIGC';
		protected $table =  'programas_presupuesto';
    protected $fillable = ['id', 'ano', 'fecha', 'id_programa', 'ley', 'vigente', 'ejecutados', 'ejecucion', 'f_ejecucion', 'created_at' ,'updated_at', 'deleted_at'];
    protected $primarykey = 'id';

    public function Programas()
    {
      $relacion = $this->belongsTo(Programas::class, 'id_programa', 'id');
      if($relacion){
        $relacion = $this->belongsTo(Programas::class, 'id_programa', 'id');
      }else{
        $relacion = null;

      }

      return $relacion;
    }

}
