<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;
use App\Models\Sigc\ProgramasMesesReportes;
use App\Models\Sigc\ProgramasCalendarioEventos;
use App\Models\Sigc\ProgramasEstados;
use App\Models\Sigc\Programas;
use Illuminate\Database\Eloquent\SoftDeletes;


class ProgramasReportes extends Model
{
    use SoftDeletes;
		protected $connection = 'mysqlSIGC';
		protected $table =  'programas_reportes';
    protected $fillable = ['id', 'ano', 'id_calendario', 'id_mes_reporte', 'id_programa', 'id_reportador', 'id_componente', 'id_indicador', 'id_cobertura', 'id_presupuesto', 'numerador', 'denominador', 'resultado', 'observacion_rep', 'id_estado', 'observacion', 'created_at', 'updated_at', 'deleted_at'];
    protected $primarykey = 'id';


    public function Meses()
    {
      $relacion = $this->belongsTo(ProgramasMesesReportes::class, 'id_mes_reporte', 'id');
      if($relacion){
        $relacion = $this->belongsTo(ProgramasMesesReportes::class, 'id_mes_reporte', 'id');
      }else{
        $relacion = null;

      }

      return $relacion;
    }


    public function Calendario()
    {
      $relacion = $this->belongsTo(ProgramasCalendarioEventos::class, 'id_calendario', 'id');
      if($relacion){
        $relacion = $this->belongsTo(ProgramasCalendarioEventos::class, 'id_calendario', 'id');
      }else{
        $relacion = null;

      }

      return $relacion;

    }  

      public function Estados()
	{
		$relacion = $this->belongsTo(ProgramasEstados::class, 'id_estado', 'id');
		if($relacion){
			$relacion = $this->belongsTo(ProgramasEstados::class, 'id_estado', 'id');
		}else{
			$relacion = null;

		}

		return $relacion;
	}

  public function Programas()
	{
		$relacion = $this->belongsTo(Programas::class, 'id_programa', 'id');
		if($relacion){
			$relacion = $this->belongsTo(Programas::class, 'id_programa', 'id');
		}else{
			$relacion = null;

		}

		return $relacion;
	}

}
