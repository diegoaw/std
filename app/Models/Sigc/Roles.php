<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{

    protected $connection = 'mysqlSIGC';
		protected $table =  'roles';
		protected $fillable = ['id', 'name'];
		protected $primarykey = 'id';


}
