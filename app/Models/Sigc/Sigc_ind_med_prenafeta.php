<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;

class Sigc_ind_med_prenafeta extends Model
{

    protected $connection = 'mysqlgeneral';
		protected $table =  'VT_SIGC_IND_MED_PRENAFETA';
		protected $fillable = ['Indicador_Id','Indicador_Agno', 'Indicador_Nombre', 'Medicion_Mes_Reportado','CALCULO_AVANCE_PONDERADO'];



}
