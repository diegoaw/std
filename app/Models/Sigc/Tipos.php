<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;

class Tipos extends Model
{

    protected $connection = 'mysqlSIGC';
		protected $table =  'tipos';
		protected $fillable = ['id', 'ano', 'nombre', 'alias'];
		protected $primarykey = 'id';


	  public function getAliascAttribute()
	{

	    	return $this->ano.' - '.$this->id.' - '.$this->alias;

	}

}
