<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;

class TiposTabla extends Model
{
	protected $connection = 'mysqlSIGC';
	protected $table =  'tipos';
	protected $fillable = ['id', 'ano', 'nombre', 'alias'];
	protected $primarykey = 'id';

}
