<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;

class Unidades extends Model
{

		protected $connection = 'mysqlSIGC';
		protected $table =  'unidades';
		protected $fillable = ['id','nombre'];
		protected $primarykey = 'id';

}
