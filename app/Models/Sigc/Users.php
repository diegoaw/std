<?php

namespace App\Models\Sigc;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{

    	protected $connection = 'mysqlSIGC';
		protected $table =  'users';
		protected $fillable = ['id', 'email', 'nombres', 'ap_paterno' , 'ap_materno' , 'encrypted_password' , 'rebember_created_at' , 'sign_in_count' , 'current_sign_in_at' , 'last_sign_in_at' , 'current_sign_in_ip' , 'last_sign_in_ip' , 'failed_attempts' , 'unlock_token' , 'locked_at' , 'persona_id' , 'estado'];
		protected $primarykey = 'id';

    public function getNombrecAttribute()
    {

        return $this->nombres.' '.$this->ap_paterno.' '.$this->ap_materno;

    }

    public function getRutAttribute()
    {

        return $this->nombres.' '.$this->ap_paterno.' '.$this->ap_materno.' - '.$this->persona_id;

    }

    public function getUsuariosidAttribute()
    {

        return $this->nombres.' '.$this->ap_paterno.' '.$this->ap_materno.' - '.$this->id;

    }
}
