<?php

namespace App\Models\Simple;

use Illuminate\Database\Eloquent\Model;



class DatoSeguimiento extends Model
{

    
	protected $connection = 'mysqlSimple';
	
    protected $table =  'dato_seguimiento';

    protected $fillable = ['id', 'nombre' , 'valor' , 'etapa_id'];

    protected $primarykey = 'id';

    public $timestamps = true;


}
