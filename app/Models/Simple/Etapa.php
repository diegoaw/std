<?php

namespace App\Models\Simple;

use Illuminate\Database\Eloquent\Model;
use App\Models\Simple\DatoSeguimiento;



class Etapa extends Model
{

    
	protected $connection = 'mysqlSimple';
	
    protected $table =  'etapa';

    protected $fillable = ['id', 'tarea_id' , 'usuario_id' , 'pendiente' , 'etapa_ancestro_split_id', 'vencimiento_at' , 'ended_at' , 'tramite_id' , 'extra' ];

    protected $primarykey = 'id';

    public $timestamps = true;

    public function DatosSeguimiento()
	{
      return $this->hasMany(DatoSeguimiento::class, 'etapa_id', 'id'); 
    }

}
