<?php

namespace App\Models\Simple;

use Illuminate\Database\Eloquent\Model;

class Grupos extends Model
{
	protected $connection = 'mysqlSimple';
	
    protected $table =  'grupo_usuarios';

    protected $fillable = ['id', 'nombre', 'cuenta_id'];

    //protected $primarykey = 'id';

    public $timestamps = false;
}