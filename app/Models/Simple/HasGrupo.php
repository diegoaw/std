<?php

namespace App\Models\Simple;

use Illuminate\Database\Eloquent\Model;

class HasGrupo extends Model
{
	protected $connection = 'mysqlSimple';
	
    protected $table =  'grupo_usuarios_has_usuario';

    protected $fillable = ['grupo_usuarios_id', 'usuario_id'];

    //protected $primarykey = 'id';

    public $timestamps = false;
}