<?php

namespace App\Models\Simple;

use Illuminate\Database\Eloquent\Model;

class TmpTramiteCompras extends Model
{
     
        protected $connection = 'mysqlSimple';
		protected $table =  'tmp_tramite_compras';
        protected $fillable = ['id', 'tramite_id', 'pendiente', 'proceso_id', 'ended_at' , 'deleted_at' , 'etapas' , 'unidad_requirente', 'estatus_requerimiento'];
        public $timestamps = true;
}