<?php

namespace App\Models\Simple;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Simple\Etapa;
use App\Models\UsuariosSimple\UsuariosSimple;

class TmpTramiteDerivacion extends Model
{
    use SoftDeletes;

	protected $connection = 'mysqlSimple';

    protected $table =  'tmp_tramite_derivacion';

    protected $fillable = ['id', 'tramite_id' , 'proceso_id' , 'pendiente' , 'created_at', 'updated_at', 'deleted_at', 'ended_at',  'etapas', 'estatus' , 'fecha_creacion', 'n_registro', 'n_memo_e', 'n_oficio_e', 'n_derivacion_d', 'nombres_solicitante' ,'origen' ,'docdigital' ,'remitente', 'desde' , 'remitente', 'remitente_ext', 'cargo' , 'cargo_ext' ,  'subarea_origen' , 'subarea_origen_ext' , 'documento' , 'documento2' , 'n_docto' , 'contenido_origen'];

    protected $primarykey = 'id';

    public $timestamps = true;

    public function getEmailRemitenteAttribute()
	{
		$email = UsuariosSimple::where('nombre_cargo',$this->remitente)->first()->email;
		return $email;

	}

}
