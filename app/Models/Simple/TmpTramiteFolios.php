<?php

namespace App\Models\Simple;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Simple\Etapa;
use App\Models\UsuariosSimple\UsuariosSimple;

class TmpTramiteFolios extends Model
{
    use SoftDeletes;

	protected $connection = 'mysqlSimple';
    protected $table =  'tmp_tramite_folios';
    protected $fillable = ['id', 'tramite_id' , 'proceso_id' , 'pendiente' , 'created_at', 'updated_at', 'ended_at',  'fecha', 'etapas' , 'correlativo' , 'categoria','reservado' ,'http_code_correlativo' ,'estatus' , 'lugar'];
    protected $primarykey = 'id';
    public $timestamps = true;

}
