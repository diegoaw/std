<?php

namespace App\Models\Simple;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Simple\Etapa;
use App\Models\UsuariosSimple\UsuariosSimple;

class TmpTramiteMemos extends Model
{
    use SoftDeletes;

	protected $connection = 'mysqlSimple';

    protected $table =  'tmp_tramite_memos';

    protected $fillable = ['id', 'tramite_id' , 'proceso_id' , 'pendiente' , 'ended_at',  'fecha', 'etapas' , 'correlativo' , 'reservado' ,'http_code_correlativo' ,'materia' , 'remitente', 'de_subrogante', 'division_remitente' , 'destinatario' , 'estatus' , 'lugar' , 'contenido' , 'vistos' , 'distribucion' , 'anexos' , 'anexo_a' , 'anexo_b' , 'anexo_c'];

    protected $primarykey = 'id';

    public $timestamps = true;

    public function getEmailRemitenteAttribute()
	{
		$email = UsuariosSimple::where('nombre_cargo',$this->remitente)->first()->email;
		return $email;

	}

}
