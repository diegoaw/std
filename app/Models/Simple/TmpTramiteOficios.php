<?php

namespace App\Models\Simple;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Simple\Etapa;
use App\Models\UsuariosSimple\UsuariosSimple;

class TmpTramiteOficios extends Model
{
    use SoftDeletes;

	protected $connection = 'mysqlSimple';

    protected $table =  'tmp_tramite_oficios';

    protected $fillable = ['id', 'tramite_id' , 'proceso_id' , 'pendiente' , 'ended_at',  'fecha',  'fecha_inicio', 'etapas' , 'correlativo' , 'reservado' ,'http_code_correlativo' ,'mat' ,'ant', 'desde' , 'remitente', 'de_subrogante', 'division_remitente' , 'destinatario' , 'estatus' , 'lugar' , 'contenido' , 'vistos' , 'distribucion' , 'anexos' , 'anexo_a' , 'anexo_b' , 'anexo_c'];

    protected $primarykey = 'id';

    public $timestamps = true;

    public function getEmailRemitenteAttribute()
	{
		$email = UsuariosSimple::where('nombre_cargo',$this->remitente)->first()->email;
		return $email;

	}

}
