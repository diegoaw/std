<?php

namespace App\Models\Simple;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Simple\Etapa;


class Tramite extends Model
{
    use SoftDeletes;
    
	protected $connection = 'mysqlSimple';
	
    protected $table =  'tramite';

    protected $fillable = ['id', 'proceso_id' , 'created_at' , 'updated_at' , 'ended_at', 'tramite_proc_cont' , 'deleted_at' , 'pendiente' ];

    protected $primarykey = 'id';

    public $timestamps = true;


    public function etapas()
	{
      return $this->hasMany(Etapa::class, 'tramite_id', 'id'); 
    }
}
