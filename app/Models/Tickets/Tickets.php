<?php

namespace App\Models\Tickets;

use Illuminate\Database\Eloquent\Model;

class Tickets extends Model
{
protected $fillable = ['id', 'fecha_actual', 'cargo', 'division', 'email_a', 'area', 'tipo_solicitud_d', 'tipo_solicitud_s', 'observaciones', 'adjunto', 'a_cargo', 'estado', 'observacion_soporte', 'satisfaccion', 'estrellas', 'comentarios', 'comprobante'];
}