<?php

namespace App\Models\TipoFirma;

use Illuminate\Database\Eloquent\Model;

class TipoFirma extends Model
{
		
    protected $table =  'tipo_firma';

    protected $fillable = ['id', 'nombre' , 'valor' , 'descripcion'];

    protected $primarykey = 'id';

    public $timestamps = false;
}
