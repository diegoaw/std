<?php

namespace App\Models\TiposDocumentos;

use Illuminate\Database\Eloquent\Model;

class TiposDocumentos extends Model
{
		
    protected $table =  'tipos_documentos';

    protected $fillable = ['id', 'nombre' , 'interno' , 'externo' , 'asignacion_automatica' , 'asignacion_opa'];

    protected $primarykey = 'id';

    public $timestamps = false;
}
