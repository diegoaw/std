<?php

namespace App\Models\TramitesSimple;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TramitesSimple extends Model
{
	use SoftDeletes;

	protected $connection = 'mysqlSimple';
	
    protected $table =  'tramite';

    protected $fillable = ['id', 'proceso_id' , 'pendiente' , 'created_at' , 'updated_at' , 'ended_at' , 'tramite_proc_cont' , 'deleted_at'];

    protected $primarykey = 'id';

    public $timestamps = true;
}
