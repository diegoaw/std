<?php

namespace App\Models\TramitesSimpleExterno;

use Illuminate\Database\Eloquent\Model;



class DatoSeguimiento extends Model
{

    
	protected $connection = 'mysqlSimpleExterno';
	
    protected $table =  'dato_seguimiento';

    protected $fillable = ['id', 'nombre' , 'valor' , 'etapa_id'];

    protected $primarykey = 'id';

    public $timestamps = true;


}
