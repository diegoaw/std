<?php

namespace App\Models\TramitesSimpleExterno;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TramitesSimpleExterno extends Model
{
	use SoftDeletes;

	protected $connection = 'mysqlSimpleExterno';
	
    protected $table =  'tramite';

    protected $fillable = ['id', 'proceso_id' , 'pendiente' , 'created_at' , 'updated_at' , 'ended_at' , 'tramite_proc_cont' , 'deleted_at'];

    protected $primarykey = 'id';

    public $timestamps = true;
}
