<?php

namespace App\Models\Transparencia;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Archivos extends Model
{
	use SoftDeletes;

        protected $connection = 'mysqlTransparencia';
		protected $table =  'archivos';
		protected $fillable = ['id', 'id_doc', 'tipo_archivo', 'tabla_asociada', 'nombre_documento_original', 'nombre_documento_aws', 'extension_documento', 'url_publica_aws'];
		protected $primarykey = 'id';

}