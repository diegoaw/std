<?php

namespace App\Models\Transparencia;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use App\Models\Transparencia\UsuariosTiposGestiones;

class DetalleTiposGestiones extends Model
{
	use SoftDeletes;

        protected $connection = 'mysqlTransparencia';
		protected $table =  'detalle_tipos_gestiones';
		protected $fillable = ['id', 'nombre', 'tipos_gestiones_id', 'periodos_id'];
		protected $primarykey = 'id';



		public function usuarios()
        {	
			$arrayUsuarios = collect([]);
            $usuariosRelacion = UsuariosTiposGestiones::where('detalle_tipos_gestiones_id' , $this->id)->get();
			if($usuariosRelacion->count()>0){
				foreach ($usuariosRelacion as $key => $value) {
					$usuario = User::where('id' , $value->user_id)->first();
					$arrayUsuarios->push($usuario);
				}	
			}

			return $arrayUsuarios;
        }
}