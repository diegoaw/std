<?php

namespace App\Models\Transparencia;

use Illuminate\Database\Eloquent\Model;

class Divisiones extends Model
{
	    protected $connection = 'mysqlTransparencia';
		protected $table =  'ta_divisiones';
		protected $fillable = ['id', 'nombre'];
		protected $primarykey = 'id';
		public $timestamps = false;
		

}