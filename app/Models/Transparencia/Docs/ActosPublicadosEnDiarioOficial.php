<?php

namespace App\Models\Transparencia\Docs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActosPublicadosEnDiarioOficial extends Model
{
	use SoftDeletes;

        protected $connection = 'mysqlTransparencia';
		protected $table =  'doc_actos_publicados_en_diario_oficial';
		protected $fillable = ['id', 'ano', 'mes', 'tipo_norma', 'numero_norma', 'denominacion', 'fecha_publicacion', 'enlace1', 'fecha_modificacion', 'enlace2', 'nota_generador', 'nota_revisor', 'nota_encargado_ta', 'fechas_periodos_id', 'tipos_gestiones_id', 'detalle_tipos_gestiones_id', 'user_id', 'created_at', 'updated_at', 'delete_at'];
		protected $primarykey = 'id';

}
