<?php

namespace App\Models\Transparencia\Docs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActosResolucionesEfectosSobreTerceros extends Model
{
	use SoftDeletes;

        protected $connection = 'mysqlTransparencia';
		protected $table =  'doc_actos_resoluciones_efectos_sobre_terceros';
		protected $fillable = ['id', 'ano', 'mes', 'tipologia_acto', 'tipo_acto', 'denominacion_acto', 'n_acto', 'fecha_acto', 'fecha_publicacion', 'indicacion_forma', 'efectos_generales', 'fecha_actualizacion', 'descripcion', 'enlace1', 'enlace2', 'nota_generador', 'nota_revisor', 'nota_encargado_ta', 'fechas_periodos_id', 'tipos_gestiones_id', 'detalle_tipos_gestiones_id', 'user_id', 'created_at', 'updated_at', 'deleted_at'];
		protected $primarykey = 'id';

}
