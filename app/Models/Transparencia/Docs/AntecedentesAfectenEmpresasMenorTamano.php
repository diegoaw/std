<?php

namespace App\Models\Transparencia\Docs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AntecedentesAfectenEmpresasMenorTamano extends Model
{
	use SoftDeletes;

        protected $connection = 'mysqlTransparencia';
		protected $table =  'doc_antecedentes_afecten_empresas_menor_tamano';
		protected $fillable = ['id', 'ano', 'mes', 'fecha_publicacion', 'organismo', 'titulo_denominacion', 'tipo_norma', 'efectos_norma', 'enlace_formulario', 'enlace', 'nota_generador', 'nota_revisor', 'nota_encargado_ta', 'fechas_periodos_id', 'tipos_gestiones_id', 'detalle_tipos_gestiones_id', 'user_id', 'created_at', 'updated_at', 'deleted_at'];
		protected $primarykey = 'id';

}