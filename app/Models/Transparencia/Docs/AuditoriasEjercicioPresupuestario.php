<?php

namespace App\Models\Transparencia\Docs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AuditoriasEjercicioPresupuestario extends Model
{
	use SoftDeletes;

        protected $connection = 'mysqlTransparencia';
		protected $table =  'doc_auditorias_ejercicio_presupuestario';
		protected $fillable = ['id', 'ano', 'mes', 'titulo_auditoria', 'entidad', 'materia', 'fecha_inicio', 'fecha_termino', 'inicio_periodo', 'fin_periodo', 'respuesta_servicio', 'fecha_publicacion', 'enlace', 'nota_generador', 'nota_revisor', 'nota_encargado_ta', 'fechas_periodos_id', 'tipos_gestiones_id', 'detalle_tipos_gestiones_id', 'user_id', 'created_at', 'updated_at', 'deleted_at'];
		protected $primarykey = 'id';
		

}