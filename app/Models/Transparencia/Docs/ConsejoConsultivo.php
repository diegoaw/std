<?php

namespace App\Models\Transparencia\Docs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConsejoConsultivo extends Model
{
	use SoftDeletes;

        protected $connection = 'mysqlTransparencia';
		protected $table =  'doc_consejo_consultivo';
		protected $fillable = ['id', 'ano', 'mes', 'nombre_consejo', 'forma_integracion', 'nombre_consejero', 'apellido_paterno', 'apellido_materno', 'representacion_calidades', 'nota_generador', 'nota_revisor', 'nota_encargado_ta', 'fechas_periodos_id', 'tipos_gestiones_id', 'detalle_tipos_gestiones_id', 'user_id', 'created_at', 'updated_at', 'deleted_at'];
		protected $primarykey = 'id';

}