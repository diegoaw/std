<?php

namespace App\Models\Transparencia\Docs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EntidadesParticipacionOrganismo extends Model
{
	use SoftDeletes;

        protected $connection = 'mysqlTransparencia';
		protected $table =  'doc_entidades_participacion_organismo';
		protected $fillable = ['id', 'ano', 'mes', 'entidad', 'tipo_vinculo', 'descripcion', 'fecha_inicio', 'fecha_termino', 'vinculo_indefinido', 'enlace', 'nota_generador', 'nota_revisor', 'nota_encargado_ta', 'fechas_periodos_id', 'tipos_gestiones_id', 'detalle_tipos_gestiones_id', 'user_id', 'created_at', 'updated_at', 'delete_at'];
		protected $primarykey = 'id';

}