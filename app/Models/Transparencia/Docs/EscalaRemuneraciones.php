<?php

namespace App\Models\Transparencia\Docs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EscalaRemuneraciones extends Model
{
	use SoftDeletes;

        protected $connection = 'mysqlTransparencia';
		protected $table =  'doc_escala_remuneraciones';
		protected $fillable = ['id', 'ano', 'mes', 'estamento', 'grado', 'unidad_monetaria', 'sueldo_base', 'asignacion', 'remuneracion', 'nota_generador', 'nota_revisor', 'nota_encargado_ta', 'fechas_periodos_id', 'tipos_gestiones_id', 'detalle_tipos_gestiones_id', 'user_id', 'created_at', 'updated_at', 'deleted_at'];
		protected $primarykey = 'id';

}