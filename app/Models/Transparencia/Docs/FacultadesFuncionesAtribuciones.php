<?php

namespace App\Models\Transparencia\Docs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FacultadesFuncionesAtribuciones extends Model
{
	use SoftDeletes;

        protected $connection = 'mysqlTransparencia';
		protected $table =  'doc_facultades_funciones_atribuciones';
		protected $fillable = ['id', 'ano', 'mes', 'unidad_organo', 'funciones_falcutades', 'fuente_legal', 'fecha_publicacion', 'enlace1', 'fecha_modificacion', 'enlace2', 'nota_generador', 'nota_revisor', 'nota_encargado_ta', 'fechas_periodos_id', 'tipos_gestiones_id', 'detalle_tipos_gestiones_id', 'user_id', 'created_at', 'updated_at', 'deleted_at'];
		protected $primarykey = 'id';

}