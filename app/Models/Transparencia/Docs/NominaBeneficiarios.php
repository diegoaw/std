<?php

namespace App\Models\Transparencia\Docs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NominaBeneficiarios extends Model
{
	use SoftDeletes;

        protected $connection = 'mysqlTransparencia';
		protected $table =  'doc_nomina_beneficiarios';
		protected $fillable = ['id', 'ano', 'mes', 'nombre_programa', 'fecha_otrogamiento', 'tipo_acto', 'denominacion_acto', 'fecha_acto', 'n_acto', 'razon_social', 'nombre', 'apellido_paterno', 'apellido_materno', 'nota_generador', 'nota_revisor', 'nota_encargado_ta', 'fechas_periodos_id', 'tipos_gestiones_id', 'detalle_tipos_gestiones_id', 'user_id', 'created_at', 'updated_at', 'deleted_at'];
		protected $primarykey = 'id';

}
		