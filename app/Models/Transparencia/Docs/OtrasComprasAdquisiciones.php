<?php

namespace App\Models\Transparencia\Docs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OtrasComprasAdquisiciones extends Model
{
	use SoftDeletes;

        protected $connection = 'mysqlTransparencia';
		protected $table =  'doc_otras_compras_adquisiciones';
		protected $fillable = ['id', 'ano', 'mes', 'tipo_compra', 'tipo_acto', 'denominacion_acto', 'fecha_contrato', 'n_acto', 'razon_social', 'nombre', 'apellido_paterno', 'apellido_materno', 'rut', 'socio_accionista', 'objeto_contratacion', 'fecha_inicio', 'fecha_termino', 'monto_operacion', 'tipo_unidad', 'observaciones', 'enlace_texto_contrato', 'enlace_texto_acto', 'enlace_texto_acto_modificacion', 'nota_generador', 'nota_revisor', 'nota_encargado_ta', 'fechas_periodos_id', 'tipos_gestiones_id', 'detalle_tipos_gestiones_id', 'user_id', 'created_at', 'updated_at', 'deleted_at'];
		protected $primarykey = 'id';

}