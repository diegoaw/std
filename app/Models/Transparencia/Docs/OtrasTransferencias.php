<?php

namespace App\Models\Transparencia\Docs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OtrasTransferencias extends Model
{
	use SoftDeletes;

        protected $connection = 'mysqlTransparencia';
		protected $table =  'doc_otras_transferencias';
		protected $fillable = ['id', 'ano', 'mes', 'fecha_transferencia', 'denominacion_transferencia', 'monto', 'tipo_unidad', 'imputacion', 'objeto_transferencia', 'razon_social', 'nombre', 'apellido_paterno', 'apellido_materno', 'nota_generador', 'nota_revisor', 'nota_encargado_ta', 'fechas_periodos_id', 'tipos_gestiones_id', 'detalle_tipos_gestiones_id', 'user_id', 'created_at', 'updated_at', 'deleted_at'];
		protected $primarykey = 'id';

}