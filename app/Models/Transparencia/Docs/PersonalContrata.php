<?php

namespace App\Models\Transparencia\Docs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PersonalContrata extends Model
{
	use SoftDeletes;

        protected $connection = 'mysqlTransparencia';
		protected $table =  'doc_personal_contrata';
		protected $fillable = ['id', 'ano', 'mes', 'estamento', 'tipo_personal', 'apellido_paterno', 'apellido_materno', 'nombres', 'grado', 'calificacion_profesional', 'cargo', 'region', 'asignaciones_especiales', 'unidad_monetaria', 'remuneracion_bruta', 'remuneracion_liquida', 'horas_extraordinarias', 'n_horas_diurnas', 'remuneracion_horas_diurnas', 'n_horas_nocturnas', 'remuneracion_horas_nocturnas', 'n_horas_festivas', 'remuneracion_horas_festivas', 'fecha_inicio', 'fecha_termino', 'observaciones', 'declaracion_patrimonio', 'declaracion_intereses', 'viaticos', 'nota_generador', 'nota_revisor', 'nota_encargado_ta', 'fechas_periodos_id', 'tipos_gestiones_id', 'detalle_tipos_gestiones_id', 'user_id', 'created_at', 'updated_at', 'deleted_at'];
		protected $primarykey = 'id';

}