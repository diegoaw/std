<?php

namespace App\Models\Transparencia\Docs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PersonasContratosHonorarios extends Model
{
	use SoftDeletes;

        protected $connection = 'mysqlTransparencia';
		protected $table =  'doc_personas_contratos_honorarios';
		protected $fillable = ['id', 'ano', 'mes', 'apellido_paterno', 'apellido_materno', 'nombres', 'grado', 'descripcion_funcion', 'calificacion_profesional', 'region', 'unidad_monetaria', 'honorario_total_bruto', 'remuneracion_liquida', 'pago_mensual', 'fecha_inicio', 'fecha_termino', 'observaciones', 'declaracion_patrimonio', 'declaracion_intereses', 'viaticos', 'nota_generador', 'nota_revisor', 'nota_encargado_ta', 'fechas_periodos_id', 'tipos_gestiones_id', 'detalle_tipos_gestiones_id', 'user_id', 'created_at', 'updated_at', 'deleted_at'];
		protected $primarykey = 'id';

}