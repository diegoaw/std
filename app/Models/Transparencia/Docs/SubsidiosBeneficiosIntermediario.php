<?php

namespace App\Models\Transparencia\Docs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubsidiosBeneficiosIntermediario extends Model
{
	use SoftDeletes;

        protected $connection = 'mysqlTransparencia';
		protected $table =  'doc_subsidios_beneficios_intermediario';
		protected $fillable = ['id', 'ano', 'mes', 'nombre_subsidio', 'n_beneficiarios', 'razon', 'nombre_programa', 'enlace', 'nota_generador', 'nota_revisor', 'nota_encargado_ta', 'fechas_periodos_id', 'tipos_gestiones_id', 'detalle_tipos_gestiones_id', 'user_id', 'created_at', 'updated_at', 'deleted_at'];
		protected $primarykey = 'id';

}