<?php

namespace App\Models\Transparencia\Docs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubsidiosBeneficiosPropios extends Model
{
	use SoftDeletes;

        protected $connection = 'mysqlTransparencia';
		protected $table =  'doc_subsidios_beneficios_propios';
		protected $fillable = ['id', 'ano', 'mes', 'tipo_subsidio', 'unidad_organo_dependencia', 'requisitos_antecedentes', 'monto_global', 'tipo_unidad_monetaria', 'inicio_postulacion', 'fin_postulacion', 'n_beneficiarios', 'criterio_evaluacion', 'plazos_asociados', 'objetivo_subsidio', 'tipo', 'denominacion', 'numero', 'fecha', 'link_texto_integro', 'numero_beneficiarios', 'razon', 'nombre_programa', 'enlace', 'nota_generador', 'nota_revisor', 'nota_encargado_ta', 'fechas_periodos_id', 'tipos_gestiones_id', 'detalle_tipos_gestiones_id', 'user_id', 'created_at', 'updated_at', 'deleted_at'];
		protected $primarykey = 'id';

}