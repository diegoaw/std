<?php

namespace App\Models\Transparencia\Docs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TramitesAnteOrganismo extends Model
{
	use SoftDeletes;

        protected $connection = 'mysqlTransparencia';
		protected $table =  'doc_tramites_ante_organismo';
		protected $fillable = ['id', 'ano', 'mes', 'descripcion', 'requisitos_antecedentes', 'tramites_enlinea', 'tramites_arealizar', 'valor', 'donde_realizan', 'informacion_complementaria', 'nota_generador', 'nota_revisor', 'nota_encargado_ta', 'fechas_periodos_id', 'tipos_gestiones_id', 'detalle_tipos_gestiones_id', 'user_id', 'created_at', 'updated_at', 'deleted_at'];
		protected $primarykey = 'id';

}