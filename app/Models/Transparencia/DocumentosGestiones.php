<?php

namespace App\Models\Transparencia;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Transparencia\DetalleTiposGestiones;
use App\Models\Transparencia\TiposGestiones;
use App\Models\Transparencia\FechasPeriodos;



class DocumentosGestiones extends Model
{
	use SoftDeletes;

        protected $connection = 'mysqlTransparencia';
		protected $table =  'documentos_gestiones';
		protected $fillable = ['id', 'fechas_periodos_id', 'tipos_gestiones_id', 'detalle_tipos_gestiones_id', 'user_id', 'nombre_documento_original', 'nombre_documento_aws', 'extension_documento', 'url_publica_aws', 'ano', 'mes', 'nota_generador', 'nota_revisor', 'nota_encargado_ta', 'deleted_at'];
		protected $primarykey = 'id';


        public function FechasPeriodo()
		{
			$relacion = $this->belongsTo(FechasPeriodos::class, 'fechas_periodos_id', 'id');
			if($relacion){
				$relacion = $this->belongsTo(FechasPeriodos::class, 'fechas_periodos_id', 'id');
			}else{
				$relacion = null;

			}

			return $relacion;
		}

        public function TipoGestiones()
		{
			$relacion = $this->belongsTo(TiposGestiones::class, 'tipos_gestiones_id', 'id');
			if($relacion){
				$relacion = $this->belongsTo(TiposGestiones::class, 'tipos_gestiones_id', 'id');
			}else{
				$relacion = null;

			}

			return $relacion;
		}

        public function DetalleTipo()
		{
			$relacion = $this->belongsTo(DetalleTiposGestiones::class, 'detalle_tipos_gestiones_id', 'id');
			if($relacion){
				$relacion = $this->belongsTo(DetalleTiposGestiones::class, 'detalle_tipos_gestiones_id', 'id');
			}else{
				$relacion = null;

			}

			return $relacion;
		}


}