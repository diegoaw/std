<?php

namespace App\Models\Transparencia;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Transparencia\DetalleTiposGestiones;
use App\Models\Transparencia\Periodos;

class FechasPeriodos extends Model
{
	//use SoftDeletes;

        protected $connection = 'mysqlTransparencia';
		protected $table =  'fechas_periodos';
		protected $fillable = ['id', 'mes' , 'fecha_desde' , 'fecha_hasta' , 'periodos_id' , 'detalle_tipos_gestiones_id'];
		protected $primarykey = 'id';

		public function DetalleTipo()
	{
		$relacion = $this->belongsTo(DetalleTiposGestiones::class, 'detalle_tipos_gestiones_id', 'id');
		if($relacion){
			$relacion = $this->belongsTo(DetalleTiposGestiones::class, 'detalle_tipos_gestiones_id', 'id');
		}else{
			$relacion = null;

		}

		return $relacion;
	}

	public function Periodo()
	{
		$relacion = $this->belongsTo(Periodos::class, 'periodos_id', 'id');
		if($relacion){
			$relacion = $this->belongsTo(Periodos::class, 'periodos_id', 'id');
		}else{
			$relacion = null;

		}

		return $relacion;
	}


}
