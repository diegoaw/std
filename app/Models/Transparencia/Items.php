<?php

namespace App\Models\Transparencia;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Transparencia\Materias;
use App\User;

class Items extends Model
{
	use SoftDeletes;

        protected $connection = 'mysqlTransparencia';
		protected $table =  'ta_items';
		protected $fillable = ['id', 'id_materia', 'nombre', 'division', 'id_user_generador', 'id_user_generador_suplente', 'created_at', 'updated_at', 'deleted_at'];
		protected $primarykey = 'id';

		public function Materia()
	    {
		$relacion = $this->belongsTo(Materias::class, 'id_materia', 'id');
		if($relacion){
			$relacion = $this->belongsTo(Materias::class, 'id_materia', 'id');
		}else{
			$relacion = null;

		}

		return $relacion;
	    }
		
		

}