<?php

namespace App\Models\Transparencia;

use Illuminate\Database\Eloquent\Model;

class Materias extends Model
{
	    protected $connection = 'mysqlTransparencia';
		protected $table =  'ta_materia';
		protected $fillable = ['id', 'nombre'];
		protected $primarykey = 'id';
		public $timestamps = false;
		

}