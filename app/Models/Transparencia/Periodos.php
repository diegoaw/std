<?php

namespace App\Models\Transparencia;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Transparencia\DetalleTiposGestiones;

class Periodos extends Model
{
	//use SoftDeletes;

        protected $connection = 'mysqlTransparencia';
		protected $table =  'periodos';
		protected $fillable = ['id', 'nombre' , 'cant_dias'];
		protected $primarykey = 'id';

}
