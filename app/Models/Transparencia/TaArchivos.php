<?php

namespace App\Models\Transparencia;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Transparencia\Items;

class TaArchivos extends Model
{
	use SoftDeletes;

        protected $connection = 'mysqlTransparencia';
		protected $table =  'ta_archivos';
		protected $fillable = ['id', 'id_items', 'ano', 'mes', 'descripcion', 'filename', 'url_aws', 'created_at', 'updated_at', 'deleted_at'];
		protected $primarykey = 'id';

		public function Item()
	    {
		return $this->belongsTo(Items::class, 'id_items', 'id');	

		}
	
}