<?php

namespace App\Models\Transparencia;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Transparencia\DetalleTiposGestiones;

class TiposGestiones extends Model
{
	use SoftDeletes;

        protected $connection = 'mysqlTransparencia';
		protected $table =  'tipos_gestiones';
		protected $fillable = ['id', 'nombre', 'tabla_asociada'];
		protected $primarykey = 'id';


	public function DetallesTipos()
		{
			return $this->hasMany(DetalleTiposGestiones::class,'tipos_gestiones_id', 'id');
		}

}
