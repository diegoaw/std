<?php

namespace App\Models\Transparencia;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UsuariosTiposGestiones extends Model
{
	use SoftDeletes;

        protected $connection = 'mysqlTransparencia';
		protected $table =  'users_detalles_tipos_gestiones';
		protected $fillable = ['id', 'detalle_tipos_gestiones_id', 'user_id', 'deleted_at'];
		protected $primarykey = 'id';


}