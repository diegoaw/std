<?php

namespace App\Models\Traslado;

use Illuminate\Database\Eloquent\Model;

class Traslado extends Model
{
  protected $fillable = ['id', 'declaro', 'motivo_traslado', 'adjunta_respaldo', 'centro_costos', 'destinatario', 'api_area', 'cantidad_pasajeros', 'fecha_traslado', 'nombre_pasajero', 'numero_telefono', 'lugar_salida', 'hora_salida', 'lugar_destino', 'servicio_regreso', 'indique_hora_regreso','itinerario_vuelo', 'observaciones'];
}

