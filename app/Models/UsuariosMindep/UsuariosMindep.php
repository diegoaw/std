<?php

namespace App\Models\UsuariosMindep;

use Illuminate\Database\Eloquent\Model;

class UsuariosMindep extends Model
{

    protected $table =  'usuarios_mindep';

    protected $fillable = ['id', 'nombres' , 'apellido_p' , 'apellido_m' , 'cargo' , 'email' , 'departamento' , 'has_token' , 'grupo'];

    protected $primarykey = 'id';

    public $timestamps = true;


		public function getNombrescAttribute()
    {

        return $this->apellido_p.' '.$this->nombres.' '.' - '.$this->email;

    }
}
