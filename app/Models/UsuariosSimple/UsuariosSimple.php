<?php

namespace App\Models\UsuariosSimple;

use Illuminate\Database\Eloquent\Model;

class UsuariosSimple extends Model
{
	protected $connection = 'mysqlSimple';
	
    protected $table =  'usuario';

    protected $fillable = ['id', 'usuario' , 'password' , 'rut' , 'nombres' , 'apellido_paterno' , 'apellido_materno' , 'email' , 'registrado' , 'vacaciones' , 'cuenta_id' , 'salt' , 'open_id' , 'reset_token' , 'remember_token', 'nombre_cargo', 'correo_ad'];

    protected $primarykey = 'id';

    public $timestamps = true;
}
