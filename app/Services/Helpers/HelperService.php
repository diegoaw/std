<?php

namespace App\Services\Helpers;

use Carbon\Carbon;
use DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Config;

use App\Models\Configuracion\SesionCompensacion\Periodo;

class HelperService
{
  //retorna la fecha actual de base de datos en formato Carbon
  public function actual_date()
  {
    $query = 'SELECT SYSDATE as fecha FROM DUAL';
    $fecha_actual = DB::select($query)[0]->fecha;
    $fecha_actual = Carbon::parse("{$fecha_actual}");
    return $fecha_actual;
  }

  //retorna un arreglo con las horas para un periodo completo de compensacion -> ["19:00","19:30","20:00",...]
  //$diferencia = cantidad de minutos de diferencia entre una hora y la otra
  public function generateHoras($diferenciaMinutos){

    // se obtiene la menor hora de inicio para un periodo, de modo que sea la base para todos los demas
    $menorInicio = Periodo::select(DB::raw('MIN(hora_inicio) as hora_inicio'))->where('dia_inicio','D-1')->first();
    $menorInicio = isset($menorInicio) ? $menorInicio->hora_inicio : '19:00';

    $i = 0; //contador para las posiciones del vector de horas
    $horaInicial = Carbon::parse("{$menorInicio}"); //se transforma la hora en un objeto carbon para añadir los minutos
    $horaInicialBase = $horaInicial->copy(); //para mantener la hora base con quien comparar la hora final del ciclo
    do { //se van a sumar los minutos indicados hasta que la diferencia en dias sea de 1
      $rangosHora[$i++] = $horaInicial->format('H:i');
    } while($horaInicialBase->diffInDays($horaInicial->addMinutes($diferenciaMinutos)) < 1);

    return $rangosHora;
  }

  //Paginador
  //$array, $request, $selPage = pagina seleccionada para mostrar
  public function arrayPaginator($array, $request, $selPage)
  {
    $page = $selPage;
    //se toma el valor constante del archivo para la cantidad de registros para la tabla, por cada pagina
    $perPage = Config::get('constants.tablas.caract_cantidad_registros');
    $offset = ($page * $perPage) - $perPage;
    return new LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page,
    ['path' => $request->url(), 'query' => $request->query()]);
  }

  //ordenamiento de un array
  //$array, $columna = nombre de la propiedad por la cual se va a ordenar, $orden = ASC o DESC
  public function ordenarArregloPorColumna($array, $columna, $orden)
  {
    if(count($array) > 1){
      array_multisort(array_column($array, $columna), strtoupper($orden) == 'ASC' || strtoupper($orden) == null ? SORT_ASC : SORT_DESC, $array);
    }
    return $array;
  }

  public function identificarOrdenamiento($request, $defaultCol, $defaultOrd)
  {
    if ($request->has('order_by')) {
      $array = explode('-', $request->get('order_by'));
    }

    $columna = $array[0] ?? $defaultCol;
    $orden = $array[1] ?? strtoupper($defaultOrd);

    return (object) [
      'columna' => $columna,
      'orden' => $orden,
    ];
  }

  public function cantidadRegistrosPaginacion()
  {
    return Config::get('constants.tablas.caract_cantidad_registros');
  }
}
