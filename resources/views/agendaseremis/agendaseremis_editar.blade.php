@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
    <h1><i class="fa fa-calendar"></i> Editar Cita de Agenda Seremis</h1><br>
    </div>    
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')

{!! Form::open(['route' => ['agendaseremis_update' , $editAgendaSeremis->id], 'files' => true, 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','id'=>'form-create']) !!}

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-danger card-outline">
        <h3 class="card-title">Editar Cita de Agenda Seremis</h3>
      </div>
      <div class="card-body">

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>

        <div class="callout col-sm-8 offset-sm-2" style="color: #737373;">
          <h4 style="font-size: 14px;"><i class="fa fa-info-circle" style="color: #ff0000;"></i> Nota:</h4>
          <span>Los campos marcados con un asterisco (*) son obligatorios.</span>
        </div><br>

        <div class="form-group row">
           {!! Form::label('Seleccione Seremi', 'Seleccione Seremi(*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
          {!! Form::select('id_wsad',$selectSeremis, $editAgendaSeremis->id_wsad, ['id'=>'id_wsad','name'=>'id_wsad', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
           {!! Form::label('Seleccione Región', 'Seleccione Región(*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
          {!! Form::select('id_region',$selectRegiones, $editAgendaSeremis->id_region, ['id'=>'id_region','name'=>'id_region', 'class'=>'form-control' , 'readonly'=>'readonly']) !!}
          </div>
        </div>

        <div class="form-group row">
           {!! Form::label('Seleccione Comuna', 'Seleccione Comuna(*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
          {!! Form::select('id_comuna',$selectComunas, $editAgendaSeremis->id_comuna, ['id'=>'id_comuna','name'=>'id_comuna', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
          {!!Form::label('Dia', 'Día(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
          <div class="input-group date">
                <input type="text" class="form-control group-date" id="dia" name="dia"  value="{!! date('Y-m-d', strtotime($editAgendaSeremis->dia))  !!}">
                <div class="input-group-append">
                  <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
              </div>
          </div>
        </div> 

        <div class="form-group row">
          {!! Form::label('Hora', 'Hora(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('hora', $editAgendaSeremis->hora, ['class' => 'form-control', 'placeholder' => 'Indique Hora']) !!}
          </div>
        </div>   

        <div class="form-group row">
          {!! Form::label('Lugar', 'Lugar(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('lugar', $editAgendaSeremis->lugar, ['class' => 'form-control', 'placeholder' => 'Indique Lugar']) !!}
          </div>
        </div> 
      
        <div class="form-group row">
          {!! Form::label('Nombre Actividad', 'Nombre Actividad(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('nombre_actividad', $editAgendaSeremis->nombre_actividad, ['class' => 'form-control', 'placeholder' => 'Indique Nombre Actividad']) !!}
          </div>
        </div> 

        <div class="form-group row">
          {!! Form::label('Organizador', 'Organizador(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('organizador', $editAgendaSeremis->organizador, ['class' => 'form-control', 'placeholder' => 'Indique Organizador']) !!}
          </div>
        </div> 
        
        <p style="text-align:center; color:DarkRed;"><b>Si son varios asistentes separelos por coma.</b></p>
        <div class="form-group row">
          {!! Form::label('Asistentes', 'Asistente(s)(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('asistentes', $editAgendaSeremis->asistentes, ['class' => 'form-control', 'placeholder' =>'Indique el/los Asistente(s)']) !!}
          </div>
        </div> 

        <div class="form-group row">
          {!! Form::label('viatico','Viático(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6" style="text-align:center">
            <label class="radio-inline">
                {{ Form::radio('viatico', '1', $editAgendaSeremis->viatico == 1)}} Si
            </label>
            <label class="radio-inline">
                {{ Form::radio('viatico', '0', $editAgendaSeremis->viatico == 0)}} No
            </label>
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Observaciones', 'Observaciones', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('observaciones', $editAgendaSeremis->observaciones, ['class' => 'form-control', 'placeholder' => 'Observaciones']) !!}
          </div>
        </div> 
  
        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! Form::submit('Guardar', ['class' => 'btn btn-success btn-sm','title'=>'Guardar']) !!}
              {!! link_to_route('agendaseremis', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) !!}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{!! Form::close() !!}
@stop
@section('after-scripts-end')
<script type="text/javascript">
$(document).ready(function(){
  $('#id_wsad').on('change', function(){
       var seremi = $(this).val();
       console.log(seremi);
        if(seremi !== ''){
          $.get('https://webservices.mindep.cl/api/seremisMindepPyR/DatosRegiones/STD/'+seremi, function(datos){
             $('#id_region').val(datos.region)
         })
        }
  });
});
</script>
<script type="text/javascript">
  function inload(){
      setTimeout(function(){ document.getElementById("load").style.display="none"; }, 5000);

      }
     

      $(document).ready(function(){
      $('#id_wsad').on('change', function(){
            var seremi = $(this).val();
            
            var region = $(this).val();
            console.log(region);
      
            //console.log(seremi);
            if(seremi !== ''){ 
              $.get('/AgendaSeremis/SelectComunas/'+region, function(comunas){
                $('#id_comuna').empty();
                $('#id_comuna').append("<option value>Seleccione Comuna</option>");
                $.each(comunas, function(index , value){
                $('#id_comuna').append("<option value='"+index+"'>"+value+" </option>");
                })
              })
            }else{
              $.get('/AgendaSeremis/SelectComunas', function(comunas){
                $('#id_comuna').empty();
                $('#id_comuna').append("<option value>Seleccione Comuna</option>");
                $.each(comunas, function(index , value){
                $('#id_comuna').append("<option value='"+index+"'>"+value+" </option>");
                })
              })
            }
      });     

    });
</script>
@include('includes.scripts.script_fecha.scripts_filtro_fecha')
@endsection