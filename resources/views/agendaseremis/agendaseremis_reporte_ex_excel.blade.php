<table>
  <tr>
   <th colspan="52" style="text-align: center;"></th>
  </tr>
  <tr>
   <th colspan="52" style="text-align: center;">AGENDA SEMANAL</th>
  </tr>
</table >
<table>
      <tr>
        <th colspan="6" style="color:#ffff; text-align: center; background-color: #013273;">SEREMI</th>
        <th colspan="6" style="color:#ffff; text-align: center; background-color: #013273;">REGIÓN</th>
        <th colspan="2" style="color:#ffff; text-align: center; background-color: #013273;">DÍA</th>
        <th colspan="2" style="color:#ffff; text-align: center; background-color: #013273;">HORA</th>
        <th colspan="4" style="color:#ffff; text-align: center; background-color: #013273;">LUGAR</th>
        <th colspan="6" style="color:#ffff; text-align: center; background-color: #013273;">COMUNA</th>
        <th colspan="7" style="color:#ffff; text-align: center; background-color: #013273;">NOMBRE ACTIVIDAD</th>
        <th colspan="7" style="color:#ffff; text-align: center; background-color: #013273;">ORGANIZADOR</th>
        <th colspan="7" style="color:#ffff; text-align: center; background-color: #013273;">ASISTENTES</th>
        <th colspan="1" style="color:#ffff; text-align: center; background-color: #013273;">VIÁTICO</th>
        <th colspan="10" style="color:#ffff; text-align: center; background-color: #013273;">OBSERVACIONES</th>
      </tr>
    @if(count($dataFinal)>0)
      @foreach ($dataFinal as $indice => $tramite)
      <tr>
        <td colspan="6" valign="middle" style="text-align: center;" ><center>{{$tramite->Seremi->name}}</center></td>
        <td colspan="6" valign="middle" style="text-align: center;" ><center>{{$tramite->Region->nombre}}</center></td>
        <td colspan="2" valign="middle" style="text-align: center;" ><center>{{$tramite->dia}}</center></td>
        <td colspan="2" valign="middle" style="text-align: center;" ><center>{{$tramite->hora}}</center></td>
        <td colspan="4" valign="middle" style="text-align: center;" ><center>{{$tramite->lugar}}</center></td>
        <td colspan="6" valign="middle" style="text-align: center;" ><center>{{$tramite->Comuna->nombre}}</center></td>
        <td colspan="7" valign="middle" style="text-align: center;" ><center>{{$tramite->nombre_actividad}}</center></td>
        <td colspan="7" valign="middle" style="text-align: center;" ><center>{{$tramite->organizador}}</center></td>
        <td colspan="7" valign="middle" style="text-align: center;" ><center>{{$tramite->asistentes}}</center></td>
        <td colspan="1" valign="middle" style="text-align: center;" ><center>@if($tramite->viatico == 1) Si @endif @if($tramite->viatico == 0) No @endif</center></td>
        <td colspan="10" valign="middle" style="text-align: center;" ><center>{{$tramite->observaciones}}</center></td>
      </tr>
      @endforeach
    @endif
</table>