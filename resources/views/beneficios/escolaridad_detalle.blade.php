@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-5">
      <h1><b><i class="fa fa-folder-open"></i> Consultas sobre Bono Escolaridad</b></h1>
    </div>
    <div class="col-sm-7">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-folder-open"></i> Consultas sobre el Bono Escolaridad</li>
        <li class="breadcrumb-item active">Detalles</li>
      </ol>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <!-- Line chart -->
    <div class="card card-danger card-outline">
      <div class="card-header">
        <h3 class="card-title"><b>Consulta sobre el Bono Escolaridad </b><b style="color:DarkRed;">N° {{$data->id}}</b></h3>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <br>
        <h4><b style="color:DarkRed;">Login Correo Institucional</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b style="color:DarkBlue;">Fecha Solicitud:</b></td>
              <td nowrap>{{$data->fecha_solicitud}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Seleccione a el(la) Funcionario(a):</b></td>
              <td nowrap>{{$data->seleccione}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Nombre y Cargo Funcionario(a):</b></td>
              <td >{{$data->cargo}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>División/Área:</b></td>
              <td nowrap>{{$data->division}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Correo Funcionario(a):</b></td>
              <td nowrap>{{$data->correo}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>RUT Funcionario:</b></td>
              <td nowrap>{{$data->rut}}</td>
          </tr>          
          <tr>
              <td style="width:450px"><b>Calidad Jurídica:</b></td>
              <td nowrap>{{$data->calidad_juridica}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Grado:</b></td>
              <td nowrap>{{$data->grado}}°</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Fecha Ingreso al Servicio:</b></td>
              <td nowrap>{{$data->fecha_ingreso}}</td>
          </tr>
          </table>
          
        <br>
        <br>
        <h4><b style="color:DarkRed;">Datos de Hijo(a)</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Nombre Completo:</b></td>
              <td nowrap>{{$data->nombre_hijo}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>RUN:</b></td>
              <td nowrap>{{$data->run_hijo}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Fecha de Nacimiento:</b></td>
              <td nowrap>{{$data->fecha_n_hijo}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Edad del Hijo(a):</b></td>
              <td nowrap>{{$data->edad_hijo}}</td>
          </tr>       
        </table>

        <br>
        <br>
        <h4><b style="color:DarkRed;">Nivel de Enseñanza</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Indique Nivel de Enseñanza:</b></td>
              <td nowrap>{{$data->nivel_enseanza}}</td>
          </tr>           
        </table>

        <br>
        <br>
        <h4><b style="color:DarkRed;">Documentos Adjuntos</b></h4>
        <br>
        <table id="example1" class="table table-striped">
            <tr>
              <td style="width:450px"><b>Certificado de Nacimiento del o la Menor:</b></td>
              <td nowrap>@if($data->certificado_nacimiento)
              <a href="{{$data->certificado_nacimiento}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Certificado de Alumno Regular, Extendido por el Establecimiento Educacional y/o Declaración Jurada Simple Firmada, Donde se Indique que el Hijo(a) se Encuentra Estudiando :</b></td>
              <td nowrap>@if($data->alumno_regular)
              <a href="{{$data->alumno_regular}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif</td>
          </tr>  
          <tr>
              <td style="width:450px"><b>Declaración Jurada:</b></td>
              <td nowrap>@if($data->declaracion_jurada)
              <a href="{{$data->declaracion_jurada}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif</td>
          </tr> 
        </table>

        <br>
        <br>
        <h4><b style="color:DarkRed;">Beneficio al que Optará</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>El Hijo(a) se Encuentra como Carga Legal Vigente en El Servicio:</b></td>
              <td nowrap>{{$data->vigente_servicio}}</td>
          </tr>           
        </table>

        <br>
        <br>
        <h4><b style="color:DarkRed;">Comprobante de Solicitud</b></h4>
        <br>
        <table id="example1" class="table table-striped">
            <tr>
              <td style="width:450px"><b>Descargar Comprobante:</b></td>
              <td nowrap><a href="{{$data->comprobante}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a></td>
            </tr>               
        </table>

        
        <br>
        <br>
        <h4><b style="color:DarkBlue;">Recepción y Revisión Solicitud Bono Escolaridad</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Fecha Revisión:</b</td>
              <td nowrap>{{$data->fecha_revision}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>¿Solicitud Bono de Escolaridad Cumple con los Requisitos?</b></td>
              <td nowrap>{{$data->cumple_requisitos}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Observaciones:</b></td>
              <td nowrap>{{$data->observaciones_correccion}}</td>
          </tr>            
        </table>

      </div>

        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! link_to_route('escolaridad', 'Volver', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
            </div>
          </div>
        </div>
    </div>
  </div>
</div>


@endsection
@section('before-scripts-end')
@stop