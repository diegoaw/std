@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-5">
      <h1><b><i class="fa fa-female"></i> Consultas sobre Beneficio de Maternidad</b></h1>
    </div>
    <div class="col-sm-7">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-female"></i> Consultas sobre el Beneficio de Maternidad</li>
        <li class="breadcrumb-item active">Detalles</li>
      </ol>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <!-- Line chart -->
    <div class="card card-danger card-outline">
      <div class="card-header">
        <h3 class="card-title"><b>Consulta sobre el Beneficio de Maternidad N° {{$data->id}}</b></h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <br>
        <h4><b>Datos Solicitante</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Fecha del Trámite:</b></td>
              <td nowrap>{{$data->fecha_tramite}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Nombre Solicitante:</b></td>
              <td nowrap>{{$data->nombre_solicitante}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Cargo Solicitante:</b></td>
              <td nowrap>{{$data->cargo_solicitante}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Correo Solicitante:</b></td>
              <td nowrap>{{$data->correo_solicitante}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Dependencia Solicitante:</b></td>
              <td nowrap>{{$data->dependencia_solicitante}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Rut Solicitante:</b></td>
              <td nowrap>{{$data->rut_solicitante}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Calidad Jurídica:</b></td>
              <td nowrap>{{$data->calidad_juridica}}</td>
          </tr>          
          <tr>
              <td style="width:450px"><b>Grado Solicitante:</b></td>
              <td nowrap>{{$data->grado_solicitante}}°</td>
          </tr>
          </table>
          
        <br>
        <br>
        <h4><b>Datos del o la Menor</b></h4>
        <br>
        <table id="example1" class="table table-striped">
        <tr>
              <td style="width:450px"><b>Fecha de Inicio del Beneficio:</b></td>
              <td nowrap>{{$data->fecha_inicio_beneficio}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Nombre Completo del Menor:</b></td>
              <td nowrap>{{$data->nombre_menor}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Rut del Menor:</b></td>
              <td nowrap>{{$data->rut_menor}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Fecha Nacimiento del Menor:</b></td>
              <td nowrap>{{$data->fecha_n_menor}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Certificado de Nacimiento del o la Menor:</b></td>
              <td nowrap>@if($data->certificado_nacimiento)
              <a href="{{$data->certificado_nacimiento}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Declaración Jurada Simple de que el Cónyuge No Está Recibiendo Otro Beneficio:</b></td>
              <td nowrap>@if($data->declaracion_jurada)
              <a href="{{$data->declaracion_jurada}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif</td>
          </tr>         
        </table>

        <br>
        <br>
        <h4><b>Datos del Beneficio a Optar</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Que Beneficio Optará:</b></td>
              <td nowrap>{{$data->que_beneficio_optara}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>He Gozado o Estoy Gozando Actualmente de Algún Beneficio de los Anteriormente Nombrados:</b></td>
              <td nowrap>{{$data->goza_beneficio}} @if($data->cual) {{$data->cual}}@endif</td>
          </tr>   
          @if($data->declaracion_cuidado_menor_s)<tr>
          <td style="width:450px"><b>Declaración del Cuidado del Menor:</b></td>
          <td nowrap><a href="{{$data->declaracion_cuidado_menor_s}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a></td>
          </tr>@endif
          @if($data->declaracion_cuidado_menor)<tr>
          <td style="width:450px"><b>Declaración del Cuidado del Menor:</b></td>
          <td nowrap><a href="{{$data->declaracion_cuidado_menor}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a></td>
          </tr>@endif         
        </table>

        <br>
        <br>
        <h4><b>Datos para Optar a {{$data->que_beneficio_optara}}</b></h4>
        <br>
        <table id="example1" class="table table-striped">
            @if($data->modalidad)<tr>
              <td style="width:450px"><b>Asimismo, manifiesto estar en conocimiento del Derecho a Sala Cuna y Alimentación de mi hijo(a) menor de dos años y teniendo presente mi situación particular, solicito a la Subsecretaria del Ministerio del Deporte, se me permita hacer uso de:</b></td>
              <td nowrap>{{$data->modalidad}} (para casos de sala cuna y apoyo a la Ed. Parvularia): Elección del establecimiento al que la funcionaria desee llevar a su hijo (a).</td>
            </tr>@endif
            @if($data->modalidad_j)<tr>
              <td style="width:450px"><b>Asimismo, manifiesto estar en conocimiento del Derecho a Sala Cuna y Alimentación de mi hijo(a) menor de dos años y teniendo presente mi situación particular, solicito a la Subsecretaria del Ministerio del Deporte, se me permita hacer uso de:</b></td>
              <td nowrap>{{$data->modalidad_j}} (para casos de sala cuna y apoyo a la Ed. Parvularia): Elección del establecimiento al que la funcionaria desee llevar a su hijo (a).</td>
            </tr>@endif
            @if($data->nombre_sala)<tr>
              <td style="width:450px"><b>Nombre Sala Cuna:</b></td>
              <td nowrap>{{$data->nombre_sala}}</td>
            </tr>@endif  
            @if($data->nombre_jardin)<tr>
              <td style="width:450px"><b>Nombre Jardín Infantil:</b></td>
              <td nowrap>{{$data->nombre_jardin}}</td>
            </tr>@endif   
            @if($data->rol_junji)<tr>
              <td style="width:450px"><b>N° de Resolución de Autorización de Funcionamiento:</b></td>
              <td nowrap>{{$data->rol_junji}}</td>
            </tr>@endif  
            @if($data->rol_junji_j)<tr>
              <td style="width:450px"><b>N° de Resolución de Autorización de Funcionamiento:</b></td>
              <td nowrap>{{$data->rol_junji_j}}</td>
            </tr>@endif 
            @if($data->nombre_del_directora)<tr>
              <td style="width:450px"><b>Nombre del Director(a):</b></td>
              <td nowrap>{{$data->nombre_del_directora}}</td>
            </tr>@endif
            @if($data->nombre_del_directora_j)<tr>
              <td style="width:450px"><b>Nombre del Director(a):</b></td>
              <td nowrap>{{$data->nombre_del_directora_j}}</td>
            </tr>@endif
            @if($data->rut_directora)<tr>
              <td style="width:450px"><b>Rut Director(a):</b></td>
              <td nowrap>{{$data->rut_directora}}</td>
            </tr>@endif  
            @if($data->rut_directora_j)<tr>
              <td style="width:450px"><b>Rut Director(a):</b></td>
              <td nowrap>{{$data->rut_directora_j}}</td>
            </tr>@endif   
            @if($data->nombre_rl)<tr>
              <td style="width:450px"><b>Nombre Representante Legal:</b></td>
              <td nowrap>{{$data->nombre_rl}}</td>
            </tr>@endif  
            @if($data->nombre_rl_j)<tr>
              <td style="width:450px"><b>Nombre Representante Legal:</b></td>
              <td nowrap>{{$data->nombre_rl_j}}</td>
            </tr>@endif          
            @if($data->rut_rl)<tr>
              <td style="width:450px"><b>Rut Representante Legal:</b></td>
              <td nowrap>{{$data->rut_rl}}</td>
            </tr>@endif  
            @if($data->rut_rl_j)<tr>
              <td style="width:450px"><b>Rut Representante Legal:</b></td>
              <td nowrap>{{$data->rut_rl_j}}</td>
            </tr>@endif 
            @if($data->direccion_establecimiento)<tr>
              <td style="width:450px"><b>Dirección Establecimiento:</b></td>
              <td nowrap>{{$data->direccion_establecimiento}}</td>
            </tr>@endif
            @if($data->direccion_establecimiento_j)<tr>
              <td style="width:450px"><b>Dirección Establecimiento:</b></td>
              <td nowrap>{{$data->direccion_establecimiento_j}}</td>
            </tr>@endif
            @if($data->region_establecimiento)<tr>
              <td style="width:450px"><b>Región:</b></td>
              <td nowrap>{{$data->region_establecimiento}}</td>
            </tr>@endif              
            @if($data->region_establecimiento_j)<tr>
              <td style="width:450px"><b>Región:</b></td>
              <td nowrap>{{$data->region_establecimiento_j}}</td>
            </tr>@endif   
            @if($data->region_establecimiento)<tr>
              <td style="width:450px"><b>Comuna:</b></td>
              <td nowrap>{{$data->comuna_establecimiento}}</td>
            </tr>@endif 
            @if($data->region_establecimiento_j)<tr>
              <td style="width:450px"><b>Comuna:</b></td>
              <td nowrap>{{$data->comuna_establecimientoo_j}}</td>
            </tr>@endif   
            @if($data->telefono_establecimiento)<tr>
              <td style="width:450px"><b>Teléfono:</b></td>
              <td nowrap>{{$data->telefono_establecimiento}}</td>
            </tr>@endif  
            @if($data->telefono_establecimiento_j)<tr>
              <td style="width:450px"><b>Teléfono:</b></td>
              <td nowrap>{{$data->telefono_establecimiento_j}}</td>
            </tr>@endif          
            @if($data->correo_establecimiento)<tr>
              <td style="width:450px"><b>Correo Electrónico:</b></td>
              <td nowrap>{{$data->correo_establecimiento}}</td>
            </tr>@endif              
            @if($data->correo_establecimiento_j)<tr>
              <td style="width:450px"><b>Correo Electrónico:</b></td>
              <td nowrap>{{$data->correo_establecimiento_j}}</td>
            </tr>@endif   
            @if($data->certificado_junji)<tr>
              <td style="width:450px"><b>Certificado de reconocimiento oficial o autorización de funcionamiento del Ministerio de Educación (sala cuna y/o jardín infantil):</b></td>
              <td nowrap><a href="{{$data->certificado_junji}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a></td>
            </tr>@endif 
            @if($data->certificado_junji_j)<tr>
            <td style="width:450px"><b>Certificado de reconocimiento oficial o autorización de funcionamiento del Ministerio de Educación (sala cuna y/o jardín infantil):</b></td>
              <td nowrap><a href="{{$data->certificado_junji_j}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a></td>
            </tr>@endif   
            @if($data->costos_asociados)<tr>
              <td style="width:450px"><b>Costos Asociados a Matricula y Mensualidad del Jardín Infantil/Sala Cuna:</b></td>
              <td nowrap><a href="{{$data->costos_asociados}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a></td>
            </tr>@endif  
            @if($data->asistencia_menor)<tr>
              <td style="width:450px"><b>Asistencia del menor, emitido por el establecimiento educacional:</b></td>
              <td nowrap><a href="{{$data->asistencia_menor}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a></td>
            </tr>@endif
            @if($data->costos_asociados_j)<tr>
              <td style="width:450px"><b>Costos Asociados a Matricula y Mensualidad del Jardín Infantil/Sala Cuna:</b></td>
              <td nowrap><a href="{{$data->costos_asociados_j}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a></td>
            </tr>@endif
            @if($data->asistencia_menor_j)<tr>
              <td style="width:450px"><b>Asistencia del menor, emitido por el establecimiento educacional:</b></td>
              <td nowrap><a href="{{$data->asistencia_menor_j}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a></td>
            </tr>@endif
            @if($data->antecedentes_legales)<tr>
              <td style="width:450px"><b>Antecedentes Legales de establecimiento educacional:</b></td>
              <td nowrap><a href="{{$data->antecedentes_legales}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a></td>
            </tr>@endif  
            @if($data->antecedentes_legales_j)<tr>
              <td style="width:450px"><b>Antecedentes Legales de establecimiento educacional:</b></td>
              <td nowrap><a href="{{$data->antecedentes_legales_j}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a></td>
            </tr>@endif
            @if($data->lactancia)<tr>
              <td style="width:450px"><b>En virtud del presente acuerdo, se ha convenido que la funcionaria hará uso de su permiso establecido en la Ley de la siguiente manera:</b></td>
              <td nowrap>{{$data->lactancia}}</td>
            </tr>@endif              
            @if($data->entre1)<tr>
              <td style="width:450px"><b>Porcion A - Entre:</b></td>
              <td nowrap>{{$data->entre1}}</td>
            </tr>@endif
            @if($data->y1)<tr>
              <td style="width:450px"><b>Porcion A - Y:</b></td>
              <td nowrap>{{$data->y1}}</td>
            </tr>@endif              
            @if($data->entre2)<tr>
              <td style="width:450px"><b>Porcion B - Entre:</b></td>
              <td nowrap>{{$data->entre2}}</td>
            </tr>@endif
            @if($data->y2)<tr>
              <td style="width:450px"><b>Porcion B - Y:</b></td>
              <td nowrap>{{$data->y2}}</td>
            </tr>@endif              
            @if($data->entrada)<tr>
              <td style="width:450px"><b>Entrada:</b></td>
              <td nowrap>{{$data->entrada}}</td>
            </tr>@endif
            @if($data->salida)<tr>
              <td style="width:450px"><b>Salida:</b></td>
              <td nowrap>{{$data->salida}}</td>
            </tr>@endif              
            @if($data->desde)<tr>
              <td style="width:450px"><b>Desde:</b></td>
              <td nowrap>{{$data->desde}}</td>
            </tr>@endif
            @if($data->hasta)<tr>
              <td style="width:450px"><b>Hasta:</b></td>
              <td nowrap>{{$data->hasta}}</td>
            </tr>@endif     
            @if($data->adjunta_autorizacion_jef)<tr>
            <td style="width:450px"><b>Autorización de Jefatura:</b></td>
              <td nowrap><a href="{{$data->adjunta_autorizacion_jef}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a></td>
            </tr>@endif  
            @if($data->pago_excepcional)<tr>
              <td style="width:450px"><b>Pago Excepcional:</b></td>
              <td nowrap>{{$data->pago_excepcional}}</td>
            </tr>@endif
            @if($data->condicion_medica)<tr>
            <td style="width:450px"><b>Certificado Médico emitido por médico pediatra tratante, donde se exprese el diagnóstico médico y/o cuadro de salud en que se encuentra el/la menor, señalando además la imposibilidad de asistir a un establecimiento de sala cuna por a lo menos sus primeros dos años de vida:</b></td>
              <td nowrap><a href="{{$data->condicion_medica}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a></td>
            </tr>@endif   
            @if($data->resolucion_teletrabajo)<tr>
              <td style="width:450px"><b>Resolución de Teletrabajo:</b></td>
              <td nowrap><a href="{{$data->resolucion_teletrabajo}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a></td>
            </tr>@endif  
            @if($data->declaracion_jurada_simple)<tr>
              <td style="width:450px"><b>Declaración Jurada Simple:</b></td>
              <td nowrap><a href="{{$data->declaracion_jurada_simple}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a></td>
            </tr>@endif
        </table>

        <br>
        <br>
        <h4><b>Comprobante de Solicitud</b></h4>
        <br>
        <table id="example1" class="table table-striped">
            @if($data->que_beneficio_optara == 'Derecho a Lactancia')<tr>
              <td style="width:450px"><b>Descargar Comprobante:</b></td>
              <td nowrap><a href="{{$data->comprobante_lactancia}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a></td>
            </tr>@endif
            @if($data->que_beneficio_optara == 'Derecho a Sala Cuna')<tr>
              <td style="width:450px"><b>Descargar Comprobante:</b></td>
              <td nowrap><a href="{{$data->comprobante_sala}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a></td>
            </tr>@endif
            @if($data->que_beneficio_optara == 'Beneficio Jardin Infantil')<tr>
              <td style="width:450px"><b>Descargar Comprobante:</b></td>
              <td nowrap><a href="{{$data->comprobante_jardin}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a></td>
            </tr>@endif
            @if($data->que_beneficio_optara == 'Beneficio Pago Excepcional')<tr>
              <td style="width:450px"><b>Descargar Comprobante:</b></td>
              <td nowrap><a href="{{$data->comprobante_excepcional}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a></td>
            </tr>@endif                
        </table>

        
        <br>
        <br>
        <h4><b>Revisión del Beneficio a Optar</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>¿La Solicitud Cumple y Corresponde a lo Indicado?:</b></td>
              <td nowrap>{{$data->cumple}}</td>
          </tr>

           @if($data->observaciones)<tr>
              <td style="width:450px"><b>Observaciones para la Corrección:</b></td>
              <td nowrap>{{$data->observaciones}}</td>
            </tr>@endif
            @if($data->observacion_adicional)<tr>
              <td style="width:450px"><b>Observaciones:</b></td>
              <td nowrap>{{$data->observacion_adicional}}</td>
            </tr>@endif
            @if($data->observacion_rechazo)<tr>
              <td style="width:450px"><b>DObservaciones de Rechazo:</b></td>
              <td nowrap>{{$data->observacion_rechazo}}</td>
            </tr>@endif    
            @if($data->adjuntar_resolucion)<tr>
              <td style="width:450px"><b>Descargar Resolución de Aceptación:</b></td>
              <td nowrap><a href="{{$data->adjuntar_resolucion}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a></td>
            </tr>@endif             
        </table>

      </div>

        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! link_to_route('maternidad', 'Volver', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
            </div>
          </div>
        </div>
    </div>
  </div>
</div>


@endsection
@section('before-scripts-end')
@stop