@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-search"></i> Buscador Global de Documentos y Trámites MINDEP</h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="card card-danger card-outline">
      <div class="card-header ">
        <h3 class="card-title"><b>Filtros </b></h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">
             {!! Form::open(['route' => 'buscador_global_buscar', 'method' => 'post' , 'class'=>"form-horizontal"]) !!}
            <div class="form-row">

                    <div class="col-sm-0">
                      <label for="nRegistro" class=" col-form-label">N° Trámite: </label>
                      <input type="text" class="form-control" id="nRegistro" name="nRegistro" value="{{ $NRegistrovIew }}" placeholder="N° Trámite">
                    </div>

                    <div class="col-sm-2">
                      <label for="materia" class=" col-form-label">Materia:</label>
                      <textarea class="form-control rounded-0" id="materia" name="materia" placeholder="Materia" rows="3">{{ $materiaView }} </textarea>
                    </div>

                    <div class="form-group   {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                    <div class="input-group date">
                      <div class="col">
                          <label for="FechaDesde" class=" col-form-label">Fecha Desde: </label>
                          <input type="text" class="form-control group-date" id="FechaDesde" name="FechaDesde" readonly="readonly" value="{!! date('Y-m-d', strtotime($feDesde))  !!}">
                      </div>
                    </div>
                    </div>

                    <div class="form-group   {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                    <div class="input-group date">
                      <div class="col">
                          <label for="FechaHasta" class=" col-form-label">Fecha Hasta: </label>
                          <input type="text" class="form-control group-date" id="FechaHasta" name="FechaHasta" readonly="readonly" value="{!! date('Y-m-d', strtotime($feHasta ))  !!}">
                      </div>
                    </div>
                    </div>








            </div>

            <div class="card-body">
        <br>
          <h4><b>STD Interno</b></h4>
        <br>
        <br>

        <div class="form-group row">
          {!! Form::label('Derivación de Documentos','Derivación de Documentos', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::checkbox('derivacion', '1', false) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Memos Electrónicos','Memos Electrónicos', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::checkbox('memo', '1', false) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Oficios Electrónicos','Oficios Electrónicos', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::checkbox('oficio', '1', false) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Resolución Electrónicas','Resolución Electrónicas', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::checkbox('resolucion', '1', false) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Requerimiento de Compras Electrónico','Requerimiento de Compras Electrónico', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::checkbox('compra', '1', false) !!}
          </div>
        </div>

        <br>
          <h4><b>STD Externo</b></h4>
        <br>
        <br>

        <div class="form-group row">
          {!! Form::label('Solicitud de Reconocimiento','Solicitud de Reconocimiento', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::checkbox('reconocimiento', '1', false) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Solicitud de Patrocinio','Solicitud de Patrocinio', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::checkbox('patrocinio', '1', false) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Reclamos de Bienes y Servicios','Reclamos de Bienes y Servicios', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::checkbox('reclamo', '1', false) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Procotolo Cuidemos Nuestro Deporte','Procotolo Cuidemos Nuestro Deporte', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::checkbox('procotolo', '1', false) !!}
          </div>
        </div>

        <br>
          <h4><b>Historico Gestión Documental Antiguo</b></h4>
        <br>
        <br>

        <div class="form-group row">
          {!! Form::label('Historico Gestión Documental','Historico Gestión Documental', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::checkbox('historico', '1', false) !!}
          </div>
        </div>



       </div>


              <button type="submit" class="btn btn-primary pull-right">Buscar</button>
                    {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>




@endsection
@section('before-scripts-end')
@include('includes.scripts.script_fecha.scripts_filtro_fecha')
@stop
