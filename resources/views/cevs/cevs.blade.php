@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-search"></i> Tramites Ingreso Documentos | Centros Deportivo Comunitario (CDC)</h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title">Filtros </h5>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">
            {!! Form::open(['route' => 'cevs', 'method' => 'get' , 'class'=>"form-horizontal"]) !!}

              <div class="form-row">

                <div class="col-sm-0">
                  <label for="nRegistro" class=" col-form-label">N° Trámite: </label>
                  <input type="text" class="form-control" id="nRegistro" name="nRegistro" value="{{ $idTramiteFiltro }}" placeholder="N° Trámite">
                </div>

              </div>
              <!--<button type="submit" class="btn btn-primary pull-right">Buscar</button>-->
              <input type="submit" class="btn btn-primary pull-right" name="Buscar" value="Buscar">

            {!! Form::close() !!}


          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header card-danger card-outline">

          <h5 class="card-title">Consultas sobre Ingreso Documentos | Centros Deportivo Comunitario (CDC)</h5>

        </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
            <table class="table table-striped table-sm"  >
              <thead >
                  <tr style="background-color: #cd0b27;">
                      <th nowrap style="color:white">Tramite ID</th>
                      <th nowrap style="color:white">Estado</th>
                      <th nowrap style="color:white">Fecha de Inicio</th>
                      <th nowrap style="color:white">Correo Electrónico</th>
                      <th nowrap style="color:white">Nombre y Apellido Encargado</th>
                      <th nowrap style="color:white">Región</th>
                      <th nowrap style="color:white">Prototipo CDC</th>
                      <th nowrap style="color:white">Área(s)</th>
                      <th nowrap style="color:white"><center>Detalles Trámite</enter></th>
                  </tr>
              </thead>
              <tbody class="panel">
              @if(count($tramitesD)>0)
                @foreach ($tramitesD as $indice => $tramites)
                    <tr>
                         <td nowrap>{{$tramites->numero_tramite}}</td>   
                         @if($tramites->pendiente == 1)
                         <td nowrap style="background-color: #cd0b27; color: white;">Pendiente</td>
                         @else
                         <td nowrap>Completado</td>
                         @endif
                         <td nowrap>{{$tramites->fecha_inicio}}</td>
                         @foreach ($tramites->detalles as $indi => $seguimiento)
                          @if($seguimiento->nombre ==  'correo_electronico' )
                          <td nowrap>{{str_replace( '"', '' , $seguimiento->valor)}}</td> 
                          @endif
                          @if($seguimiento->nombre ==  'encargado_proyecto_na' )
                          <td nowrap>{{str_replace( '"', '' , $seguimiento->valor)}}</td>
                          @endif
                          @if($seguimiento->nombre ==  'region_comuna' )
                          <td nowrap>{{str_replace( '"', '' , json_decode($seguimiento->valor)->region)}}</td>
                          @endif
                          @if($seguimiento->nombre ==  'prototipo_cevs' )
                          <td nowrap>{{str_replace( '"', '' , $seguimiento->valor)}}</td>
                          @endif
                          @if($seguimiento->nombre ==  'select' )
                          <td nowrap>{{str_replace( '"', '' , $seguimiento->valor)}}</td>
                          @endif
                         @endforeach
                         <td><center>
                          <a class="btn accion btn-info accion" href="{{route('cevsDetalle',$tramites->numero_tramite)}}"><i class="fa fa-eye"></i></a>
                         </center></td>
                    </tr>
                @endforeach
              @endif
              </tbody>
            </table>
          </div>

    </div>
  </div>
</div>

@endsection
@section('before-scripts-end')
@stop
