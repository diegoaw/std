@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-5">
      <h1><i class="fa fa-comments"></i> Ingreso Documento | Centros Deportivo Comunitario (CDC)</h1>
    </div>
    <div class="col-sm-7">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-user"></i> Consultas sobre Ingreso Documentos | Centros Deportivo Comunitario (CDC)</li>
        <li class="breadcrumb-item active">Detalles</li>
      </ol>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <!-- Line chart -->
    <div class="card card-danger card-outline">
      <div class="card-header">
        <h4 class="card-title"><b>Detalles Consultas sobre Ingreso Documentos | Centros Deportivo Comunitario (CDC)</b></h4>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <br>
        <h4><b style="color:DarkBlue;"><i>Trámite N° {{$data->id}}</i></b></h4>
        <br>
        <br>
        <h4><b>Datos Paso 2</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
            <td style="width:450px"><b>Fecha de Ingreso</b></td>
            <td>{{$data->fecha}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Nombre y Apellido Encargado Proyecto</b></td>
            <td nowrap>{{$data->encargado_proyecto_na}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Correo Electrónico</b></td>
            <td nowrap>{{$data->correo_electronico}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Prototipo Elegido para CDC</b></td>
            <td>{{$data->prototipo_cevs}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Región</b></td>
            <td>{{$data->region}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Comuna</b></td>
            <td>{{$data->comuna}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Dirección CDC</b></td>
            <td>{{$data->direccion_cevs}}</td>
          </tr>
        </table>
        <br>
        <h4><b>Datos Paso 3</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
            <td style="width:450px"><b>Área(s) a la(s) cual(es) quiere ingresar archivos</b></td>
            <td>
                {{implode(", ", $data->select)}}
            </td>
          </tr> 
          <tr>
            <td></td>
            <td></td>
          </tr> 
          @if(in_array('Juridica', $data->select)) 
          <tr>
            <td><h4><b style="color:DarkBlue;">Área Jurídica</b></h4></td>   
            <td><h4><b style="color:DarkBlue;"> </b></h4></td> 
          </tr>
          <tr>
            <td><h4><b><i>Terreno Propiedad Municipal</i></b></h4></td>   
            <td><b style="color:DarkBlue;"> </b></td> 
          </tr>
          <tr>
            <td style="width:450px"><b>Certificado de Dominio Vigente</b></td>
            <td>{{$data->certificado_dominio_vigente_tpm}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Certificado de Hipotecas Gravámenes y Prohibiciones, con litigios, en caso de existir hipotecas y gravámenes, adjuntarlos</b></td>
            <td>{{$data->certificado_hipotecas_tpm}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Autorización Consejo Municipal (inmuebles propiedad del municipio)</b></td>
            <td>{{$data->autorizacion_consejo_municipal}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Otros Archivos Jurídica TPM</b></td>
            <td>{{$data->otros_tpm}}</td>
          </tr>
          <tr>
            <td><h4><b><i>Terreno Propiedad de un Tercero</i></b></h4></td>   
            <td><b style="color:DarkBlue;"> </b></td> 
          </tr>
          <tr>
            <td style="width:450px"><b>Certificado de Dominio Vigente</b></td>
            <td>{{$data->certificado_dominio_vigente_tpt}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Certificado de Hipotecas Gravámenes y Prohibiciones, con litigios, en caso de existir hipotecas y gravámenes</b></td>
            <td>{{$data->certificado_hipotecas_tpt}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Otros Archivos Jurídica TPT</b></td>
            <td>{{$data->otros_tpt}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Copia de comodato o resolución de destinación u otro que habilita al municipio para la tenencia del inmueble</b></td>
            <td>{{$data->copia_comodato_tpt}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Observaciones Área Jurídica</b></td>
            <td>{{$data->observaciones_area_juridica}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Documento Área Jurídica</b></td>
            <td>
            @if($data->documento_area_juridica)
              <a href="{{$data->documento_area_juridica}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
            @endif
            </td>
          </tr>
          <tr>
            <td></td>
            <td></td>
          </tr> 
          @endif
          
          @if(in_array('Diseno', $data->select)) 
          <tr>
            <td><h4><b style="color:DarkBlue;">Área Diseño</b></h4></td>   
            <td><h4><b style="color:DarkBlue;"> </b></h4></td> 
          </tr>
          <tr>
            <td><h4><b><i>Condiciones Urbanísticas</i></b></h4></td>   
            <td><b style="color:DarkBlue;"> </b></td> 
          </tr>
          <tr>
            <td style="width:450px"><b>Certificado de Informes Previos DOM</b></td>
            <td>{{$data->certificado_informes_dom}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Certificado de Afectación a Utilidad Pública</b></td>
            <td>{{$data->certificado_afectacion}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Copia Permisos/Recepciones anteriores del predio completo</b></td>
            <td>{{$data->copia_permisosrecepciones}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Cabida Preliminar</b></td>
            <td>{{$data->cabida_preliminar}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Indicar si terreno está cercano a lechos de ríos o zonas de riesgo</b></td>
            <td>{{$data->terreno_zonas_riesgo}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Situación Plan Regulador** (no hay, aprobado, en proceso de modificación, etc.)</b></td>
            <td>{{$data->situacion_plan_regulador}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Anexo E</b></td>
            <td>{{$data->anexo_e}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Otros Archivos Diseño</b></td>
            <td>{{$data->otros_archivos_diseno}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Observaciones Área Diseño</b></td>
            <td>{{$data->observaciones_area_diseno}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Documento Área Diseño</b></td>
            <td>
            @if($data->documento_area_diseno)
              <a href="{{$data->documento_area_diseno}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
            @endif
            </td>
          </tr>
          <tr>
            <td></td>
            <td></td>
          </tr> 
          @endif
          
          @if(in_array('Especialidades', $data->select)) 
          <tr>
            <td><h4><b style="color:DarkBlue;">Área Especialidades</b></h4></td>   
            <td><h4><b style="color:DarkBlue;"> </b></h4></td> 
          </tr>
          <tr>
            <td><h4><b><i>Especialidades</i></b></h4></td>   
            <td><b style="color:DarkBlue;"> </b></td> 
          </tr>
          <tr>
            <td style="width:450px"><b>Informe de Mecánica de Suelos</b></td>
            <td>{{$data->informe_mecanica_suelos}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Pre Informe de Mecanica de Suelos</b></td>
            <td>{{$data->preinforme_mecanica_suelos}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Levantamiento Topográfico</b></td>
            <td>{{$data->levantamiento_topografico}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Factibilidad Eléctrica</b></td>
            <td>{{$data->factibilidad_electrica}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Factibilidad Agua Potable y Alcantarillado</b></td>
            <td>{{$data->factibilidad_agua_alcanta}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Factibilidad de Suministro de Gas por Red Pública</b></td>
            <td>{{$data->factibilidad_gas}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Factibilidad Red Internet o WiFi en el Sector</b></td>
            <td>{{$data->factibilidad_wifi}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Otros Archivos Especialidades</b></td>
            <td>{{$data->otros_archivos_especialidades}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Observaciones Área Especialidades</b></td>
            <td>{{$data->observaciones_area_especialidade}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Documento Área Especialidades</b></td>
            <td>
            @if($data->documento_area_especialidades)
              <a href="{{$data->documento_area_especialidades}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
            @endif
            </td>
          </tr>
          <tr>
            <td></td>
            <td></td>
          </tr>
          @endif

          @if(in_array('Preinversion', $data->select)) 
          <tr>
            <td><h4><b style="color:DarkBlue;">Área Preinversion</b></h4></td>   
            <td><h4><b style="color:DarkBlue;"> </b></h4></td> 
          </tr>
          <tr>
            <td><h4><b><i>Desarrollo del Problema (Anexo A)</i></b></h4></td>   
            <td><b style="color:DarkBlue;"> </b></td> 
          </tr>
          <tr>
            <td style="width:450px"><b>Política Comunal Deportiva</b></td>
            <td>{{$data->politica_comunal_deportiva}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Plan de desarrollo comunal (PLADECO)</b></td>
            <td>{{$data->plan_desarrollo_comunal}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Listado de proyectos deportivos comunales en carpeta con código BIP</b></td>
            <td>{{$data->listado_proyectos_deportivos}}</td>
          </tr>
          <tr>
            <td><h4><b><i>Diagnóstico de la situación actual (Anexo B)</i></b></h4></td>   
            <td><b style="color:DarkBlue;"> </b></td> 
          </tr>
          <tr>
            <td style="width:450px"><b>Fotografía de los principales recintos deportivos de la comuna</b></td>
            <td>{{$data->fotografia_recintos}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Mapas con redes de transporte o estructuras de las vías del área donde se emplazará el recinto</b></td>
            <td>{{$data->mapas_redes_transporte}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Mapas comunales con divisiones administrativas (unidades vecinales, juntas de vecinos, etc.)</b></td>
            <td>{{$data->mapas_comunales}}</td>
          </tr>
          <tr>
            <td><h4><b><i>Desarrollo de la demanda (Anexo C)</i></b></h4></td>   
            <td><b style="color:DarkBlue;"> </b></td> 
          </tr>
          <tr>
            <td style="width:450px"><b>Identificar zonas donde no existe infraestructura deportiva</b></td>
            <td>{{$data->zona_no_infraestructura}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Identificar grupo etario que no es atendido por los programas o planes deportivos comunales</b></td>
            <td>{{$data->grupo_no_atendido}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Identificar demandas deportivas que existan en la comuna y que no son atendidas</b></td>
            <td>{{$data->identificar_demandas}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Localización de colegios públicos o privados de la comuna próximos al terreno (Distancia en metros y tiempo de traslado a pie y en medio de transporte)</b></td>
            <td>{{$data->localizacion_colegios}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Catastro infraestructura deportiva comunal (nombre recinto, dirección, propietario, tipo de instalación, metros construidos, estado, material del piso, usuarios semanales, horario) ANEXO</b></td>
            <td>{{$data->catastro_infraestructura}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Listado de organizaciones deportivas, de organizaciones sociales y colegios, indicando el sector al que pertenece, para definir oferta deportiva</b></td>
            <td>{{$data->listado_de_organizaciones_deport}}</td>
          </tr>
          <tr>
            <td><h4><b><i>Otros</i></b></h4></td>   
            <td><b style="color:DarkBlue;"> </b></td> 
          </tr>
          <tr>
            <td style="width:450px"><b>proximidad del terreno al centro de la ciudad. Documentación de estudios, consulta de expertos o aplicación de encuestas relacionadas con el ámbito deportivo Carta de compromiso de uso de colegios y carta de compromiso de uso de organizaciones deportivas (segunda Etapa) Otros archivos Preinversión Indicar distancia o proximidad del terreno al centro de la ciudad</b></td>
            <td>{{$data->proximidad_del_terreno}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Documentación de estudios, consulta de expertos o aplicación de encuestas relacionadas con el ámbito deportivo</b></td>
            <td>{{$data->documentacion_estudios}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Carta de compromiso de uso de colegios y carta de compromiso de uso de organizaciones deportivas (segunda Etapa)</b></td>
            <td>{{$data->carta_compromiso_uso}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Segunda Alternativa de Terreno</b></td>
            <td>{{$data->segunda_alternativa_terreno}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Horario de Funcionamiento de las Instalaciones Deportivas</b></td>
            <td>{{$data->funcionamiento_instalaciones_dep}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Otros Archivos Preinversión</b></td>
            <td>{{$data->otros_archivos_preinversion}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Observaciones Área Preinversion</b></td>
            <td>{{$data->observaciones_area_preinversion}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Documento Área Preinversion</b></td>
            <td>
            @if($data->documento_area_preinversion)
              <a href="{{$data->documento_area_preinversion}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
            @endif
            </td>
          </tr>
          <tr>
            <td></td>
            <td></td>
          </tr>
          @endif
        </table>
        <br>
        <h4><b>Datos Paso 4</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
            <td style="width:450px"><b>Confirmación de Documentos</b></td>
            <td>{{implode(", ", $data->confirmacion_de_documentos)}}</td>
          </tr>
          <tr>
            <td style="width:450px"><b>Archivo Conductor</b></td>
            <td>
            @if($data->adjuntar_archivo_conductor)
              <a href="{{$data->adjuntar_archivo_conductor}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
            @endif
            </td>
          </tr>
        </table>
        <br>
        <h4><b>Datos Paso 5</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Comprobante</b></td>
              <td >
              @if($data->comprobante)
              <a href="{{$data->comprobante}}" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
              @endif
            </td>
          </tr>
        </table>
      </div>
        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! link_to_route('cevs', 'Volver', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
            </div>
          </div>
        </div>
    </div>
  </div>
</div>


@endsection
@section('before-scripts-end')
@stop
