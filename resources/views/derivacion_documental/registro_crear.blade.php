@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
    <h1><i class="fa fa-file-text-o"></i> <i class="nav-icon fa fa-exchange"></i><b> Derivación Documental - </b><b style="color:DarkRed;">Nuevo Registro</b></h1><br> 
    </div>    
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<link rel="stylesheet" href="{{asset('css/chosen/chosen.css')}}">

{!! Form::open(['route' => 'registrogd_nuevo', 'files' => true, 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','id'=>'form-create']) !!}

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-danger card-outline">
        <h3 class="card-title">Gestión Documental - Crear Registro</h3>
      </div>
      <div class="card-body">

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>

        <div class="callout col-sm-8 offset-sm-2" style="color: #737373;">
          <h4 style="font-size: 14px;"><i class="fa fa-info-circle" style="color: #ff0000;"></i> Nota:</h4>
          <span>Los campos marcados con un asterisco (*) son obligatorios.</span>
        </div>

        <div class="form-group row">
          {!! Form::label('origen','Origen(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            <label class="radio-inline">
                {{ Form::radio('origen', '1', 'active')}} Interno
            </label>
            <label class="radio-inline">
                {{ Form::radio('origen', '0')}} Externo
            </label>
          </div>
        </div>

        <div class="form-group row">
           {!! Form::label('Seleccione Remitente Interno del Documento', 'Seleccione Remitente(*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
           {!! Form::select('id_user',$selectFuncionarios, $idUsuarioSesion,  ['id'=>'id_user','name'=>'id_user', 'class'=>'form-control', 'onchange'=>'function()']) !!}
          </div>
        </div>

        <div class="form-group row">
          <label for="name" class="col-sm-3 col-form-label">Nombre Remitente Interno(*): </label>
            <div class="col-sm-6">
              <input type="text" name="name" class="form-control" readonly="readonly" id="name" placeholder="{{ Auth::user()->name }}">
            </div>
        </div>

        <div class="form-group row">
          <label for="department" class="col-sm-3 col-form-label">División/Área Remitente Interno(*): </label>
            <div class="col-sm-6">
              <input type="text" name="department" class="form-control" readonly="readonly" id="department" placeholder="{{ Auth::user()->department }}">
            </div>
        </div>

        <div class="form-group row">
          <label for="email" class="col-sm-3 col-form-label">Email Remitente Interno(*): </label>
            <div class="col-sm-6">
              <input type="text" name="email" class="form-control" readonly="readonly" id="email" placeholder="{{ Auth::user()->email }}">
            </div>
        </div>

        <div class="form-group row">
          {!! Form::label('nombre_remi', 'Nombre Remitente Externo(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('nombre_remi', null, ['class' => 'form-control', 'placeholder' => 'Nombre Remitente Externo']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('funcion_remi', 'Función Remitente Externo(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('funcion_remi', null, ['class' => 'form-control', 'placeholder' => 'Función Remitente Externo']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('procedencia_remi', 'Procedencia Remitente Externo(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('procedencia_remi', null, ['class' => 'form-control', 'placeholder' => 'Procedencia Remitente Externo']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('relacion_docs','¿Tiene Relación con Otro Documento?(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            <label class="radio-inline">
                {{ Form::radio('relacion_docs', '1')}} Si
            </label>
            <label class="radio-inline">
                {{ Form::radio('relacion_docs', '0', 'active')}} No
            </label>
          </div>
        </div>

        <div class="form-group row" id="folio_rela">
          {!! Form::label('n_folio_rela_docs', 'N° Folio de Relación(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('n_folio_rela_docs', null, ['class' => 'form-control', 'placeholder' => 'N° Folio de Relación']) !!}
          </div>
        </div>

        <div class="form-group row">
           {!! Form::label('Seleccione Tipo de Documento a Derivar', 'Seleccione Tipo de Documento a Derivar(*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
          {!! Form::select('id_tipo_documento',$selectTipoDocumento, $varNull, ['id'=>'id_tipo_documento','name'=>'id_tipo_documento', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('tiene_folio','¿Tiene N° de Folio?(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            <label class="radio-inline">
                {{ Form::radio('tiene_folio', '1')}} Si
            </label>
            <label class="radio-inline">
                {{ Form::radio('tiene_folio', '0')}} No
            </label>
          </div>
        </div>

        <div class="form-group row" id="folio_deriva">
          {!! Form::label('n_folio_deriva', 'N° Folio de Documento a Derivar(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('n_folio_deriva', null, ['class' => 'form-control', 'placeholder' => 'N° Folio de Documento a Derivar']) !!}
          </div>
        </div>

        <div class="form-group row" id="asigna_folio">
          {!! Form::label('asignacion_folio','¿Requiere Asignación de Folio por Área Gestión Documental?(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            <label class="radio-inline">
                {{ Form::radio('asignacion_folio', '1')}} Si
            </label>
            <label class="radio-inline">
                {{ Form::radio('asignacion_folio', '0')}} No
            </label>
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('materia', 'Materia(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('materia', null, ['class' => 'form-control', 'placeholder' => 'Ingrese la Materia del Documento']) !!}
          </div>
        </div> 
        
        <div class="field_wrapper">
          <div class="form-group row col-md-12">
            {!! Form::label('filename', 'Cargar Archivo a Derivar(*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-md-6">
            {!! Form::file('filename', ['class' => 'form-control']) !!}
            </div>
          </div>
        </div>   

        <div class="form-group row">
          {!! Form::label('tipo_distribucion','Indique el Tipo de Salida del Documento(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            <label class="radio-inline">
                {{ Form::radio('tipo_distribucion', 'derivacion')}} Derivación
            </label>
            <label class="radio-inline">
                {{ Form::radio('tipo_distribucion', 'distribucion')}} Distribución
            </label>
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('docdigital','¿Derivó o Distribuyó por DocDigital?(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            <label class="radio-inline">
                {{ Form::radio('docdigital', '1')}} Si
            </label>
            <label class="radio-inline">
                {{ Form::radio('docdigital', '0')}} No
            </label>
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('ventanilla_unica','¿Derivó o Distribuyó por Ventanilla Única?(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            <label class="radio-inline">
                {{ Form::radio('ventanilla_unica', '1')}} Si
            </label>
            <label class="radio-inline">
                {{ Form::radio('ventanilla_unica', '0')}} No
            </label>
          </div>
        </div>

        <!--Deribación-->
        <div class="form-group row" id="deri">
           {!! Form::label('Seleccione Funcionario', 'Seleccione Funcionario(*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
           {!! Form::select('id_destinatario',$selectFuncionariosDeri, $varNull,  ['id'=>'id_destinatario','name'=>'id_destinatario', 'class'=>'form-control']) !!}
          </div>
        </div>

        <!--Distribución-->
        <div class="form-group row" style="text-align:center;" id="distri">
          <label for="correos_internos" class="col-sm-3 col-form-label">Seleccione Funcionario(s) o Grupos(*)</label>
            <div class="col-sm-6" style="text-align:left;" >
              {{ Form::select('correos_internos[]', $selectUsuarios, null, ['class' => 'chosen-select', 'multiple','col-sm-10', 'data-placeholder' => 'Seleccione uno o más destinatarios internos','style'=>'width:nowrap; !important']) }}
              <p style="text-align:center; color:DarkRed;"><b>Enviar solo hasta 15 Funcionarios o Grupos.</b></p>
            </div>            
        </div>
        

        <div class="form-group row">
          {!! Form::label('observaciones', 'Observaciones de Derivación', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('observaciones', null, ['class' => 'form-control', 'placeholder' =>'Ingrese Observaciones de la Derivación']) !!}
          </div>
        </div>
        
        <div class="form-group row">
          {!! Form::label('requiere_respuesta','¿Requere Repuesta?(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            <label class="radio-inline">
                {{ Form::radio('requiere_respuesta', '1')}} Si
            </label>
            <label class="radio-inline">
                {{ Form::radio('requiere_respuesta', '0')}} No
            </label>
          </div>
        </div>

        <div class="form-group row">
           {!! Form::label('Seleccione Prioridad', 'Seleccione Prioridad(*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
          {!! Form::select('id_prioridad',$selectPrioridad, $varNull,  ['id'=>'id_prioridad','name'=>'id_prioridad', 'class'=>'form-control']) !!}
          </div>
        </div>
        
        
        
        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! Form::submit('Guardar', ['class' => 'btn btn-success btn-sm','title'=>'Guardar']) !!}
              {!! link_to_route('registros', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) !!}
            </div>
          </div>
        </div>



      </div>
    </div>
  </div>
</div>
{!! Form::close() !!}

@stop
@section('after-scripts-end')
<script type="text/javascript">
$(document).ready(function(){
  $('#id_user').on('change', function(){
       var grupo = $(this).val();
       console.log(grupo);
        if(grupo !== ''){
          $.get('https://webservices.mindep.cl/api/usuariosMindepPyR/DatosUsuario/STD/'+grupo, function(datos){
             $('#name').val(datos.destinatario)
             $('#department').val(datos.departamento)
             $('#email').val(datos.email)
         })
        }
  });
});
</script>
<script type="text/javascript" src="{{asset('js/chosen/chosen.jquery.js')}}"></script>
<script type="text/javascript">
    $(".chosen-select").chosen({
      width: "100%",
      max_selected_options: 16,
      no_results_text: "No se encontraron resultados para "
    });
  </script>
@endsection
