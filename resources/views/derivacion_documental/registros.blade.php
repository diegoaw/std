
@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-7">
      <div class="col-sm-11">
        <h1><b><i class="fa fa-file-text-o"></i> <i class="nav-icon fa fa-exchange"></i> Derivación Documental - </b><b style="color:DarkRed;">Registros</b></h1><br>
      </div>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title">Filtros - Registros</h5>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">

            {!! Form::open(['route' => 'registros', 'method' => 'get' , 'class'=>"form-horizontal"]) !!}
                    
                      
                    
                    
                    
            {!! Form::close() !!}

            <div class="form-group">
              <div class="col-sm-12">
                <div class="text-center">
                {!! link_to_route('registrogd_crear', 'Nuevo Registro', [], ['class' => 'btn btn-primary btn-sm','title'=>'Nuevo Registro']) !!}
                </div>
              </div>
            </div>
                  

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title"><b style="color:DarkRed;">Historial Registros {{date("Y")}}</b></h5>

          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm">
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th nowrap style="color:white"><center>ID</center></th>
                    </tr>
                </thead>
                <tbody class="panel">
                  @if(count($registros)>0)
                    @foreach ($registros as $indice => $registro)
                      <tr>
                        <td nowrap><center>{{$registro->id}}</center></td>
                      </tr>
                    @endforeach
                  @endif
                </tbody>
              </table>
          </div>
      </div>
    </div>
</div>

@endsection
@section('before-scripts-end')
@stop
