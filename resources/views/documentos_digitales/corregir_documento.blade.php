@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
            <h1><i class="fa fa-file"></i> Documentos Digitales - Corrección de Documento PDF</h1><br>

    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')


<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title"> Corrección de Documento PDF</h5>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
            <div class="col-sm-12">
                
                <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>
        <div class="text-center">
                    <br>  
                    <br>  
                        <h5 style="color:DarkBlue;"><b>Previsualización PDF</b></h5>
                        <br>      
                      
                        <embed src="{{asset($corregirDocumentoRegistro->url_vista)}}" type="application/pdf" width="100%" height="500px" />

                    <br>                   
                </div>  
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

{!! Form::open(['route' => ['corregido_documento' , $corregirDocumentoRegistro->id,] , 'files' => true, 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','id'=>'form-create']) !!}

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-danger card-outline">
        <h3 class="card-title">Cargar Documento a Corregir</h3>
      </div>
      <div class="card-body">

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>

        <div class="callout col-sm-8 offset-sm-2" style="color: #737373;">
          <h4 style="font-size: 14px;"><i class="fa fa-info-circle" style="color: #ff0000;"></i> Nota:</h4>
          <span>Los campos marcados con un asterisco (*) son obligatorios.</span>
        </div><br>

        <div class="field_wrapper">
        <div class="form-group row col-md-12">
          {!! Form::label('filename', 'Cargar PDF de Documento Digital Listo(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-md-6">
          {!! Form::file('filename', ['class' => 'form-control']) !!}

          <p style="color:Red;">* Cargue arhivo para reemplazar el documento cargado anteriormente. <i style="color:Blue;">({{$corregirDocumentoRegistro->filename}})</i> </p>
          </div>
        </div>
        </div><br><br>
        {{  Form::hidden('url',URL::previous())  }}
        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
            {!! Form::submit('Enviar', ['class' => 'btn btn-success btn-sm','title'=>'Enviar']) !!}
              <a href="{{ url()->previous() }}" class="btn btn-danger">Volver</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{!! Form::close() !!}

@endsection
@section('before-scripts-end')
@stop