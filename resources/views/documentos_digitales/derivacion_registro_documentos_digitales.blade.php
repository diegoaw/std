
@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-exchange"></i> Documentos Digitales - Modificaciones Pendientes de Documentos Digitales</h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title">Filtrar Modificaciones de Documentos Digitales por </h5>

      </div>
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">
          {!! Form::open(['route' => 'registros_derivados', 'method' => 'get' , 'class'=>"form-horizontal"]) !!}
                  <div class="form-row">
                    <div class="col-sm-0">
                      <label for="tramite" class=" col-form-label">ID Trámite: </label>
                      <input type="number" class="form-control" id="tramite" name="tramite" value="{{$tramite}}" placeholder="ID Trámite">
                    </div>

                    <div class="form-group   {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                    <div class="input-group date">
                      <div class="col">
                          <label for="FechaDesde" class=" col-form-label">Fecha Desde: </label>
                          <input type="text" class="form-control group-date" id="FechaDesde" name="FechaDesde" readonly="readonly" value="{!! date('Y-m-d', strtotime($feDesde))  !!}">
                      </div>
                    </div>
                    </div>

                    <div class="form-group   {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                    <div class="input-group date">
                      <div class="col">
                          <label for="FechaHasta" class=" col-form-label">Fecha Hasta: </label>
                          <input type="text" class="form-control group-date" id="FechaHasta" name="FechaHasta" readonly="readonly" value="{!! date('Y-m-d', strtotime($feHasta ))  !!}">
                      </div>
                    </div>
                    </div>

                    <div class="col-sm-3">
                      <label for="tipo_documento" class=" col-form-label">Tipo Documento: </label>
                      {!! Form::select('tipo_documento',$tiposDocumentos, $tipoDocumento, ['id'=>'tipo_documento','name'=>'tipo_documento', 'class'=>'form-control']) !!}
                    </div>

                    <div class="col-sm-3">
                      <label for="categoria" class=" col-form-label">Categoría Registro: </label>
                      {!! Form::select('categoria',$categorias, $categoria,['id'=>'categoria','name'=>'categoria', 'class'=>'form-control']) !!}
                    </div>

                    <div class="col-sm-2">
                      <label for="materia" class=" col-form-label">Materia Registro:</label>
                      <textarea class="form-control" id="materia" name="materia" placeholder="Materia" >{{$materia}}</textarea>
                    </div>

                    <div class="col-sm-6">
                    <button type="submit" class="btn btn-primary pull-right">Buscar</button>
                    </div>
                  </div>
                  
                  
              {!! Form::close() !!}

               
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">Listado de Modificaciones Pendientes de Documentos Digitales</h5>

          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th nowrap style="color:white"><center>ID</th>
                        <th nowrap style="color:white"><center>Fecha Modificación</center></th>
                        <th nowrap style="color:white"><center>Tipo Documento</center></th>
                        <th nowrap style="color:white"><center>Categoría Documento</center></th>
                        <th nowrap style="color:white">Materia Registro</th>
                        <th nowrap style="color:white"><center>Revisión</center></th>
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($dataDefinitiva)>0)
                      @foreach ($dataDefinitiva as $indice => $tramite)
                          <tr>
                           <td nowrap><center>{{$tramite->id}}</center></td>
                           <td nowrap><center>{{$tramite->created_at}}</center></td>
                           <td nowrap><center>{{$tramite->tiposdocumentosdigitales->nombre}}</center></td>
                           <td nowrap><center>{{$tramite->categoriadocumentosdigitales->nombre}}</center></td>
                           <td >{{$tramite->titulo}}</td>
                           <td nowrap><center><a class="btn accion btn-success accion" href="{{route('registro_derivado_editar',$tramite->id)}}"><i class="fa fa-pencil"></i></a></center></td>
                           </tr>

                  @endforeach
                @endif
                </tbody>
              </table>
          </div>
      </div>
    </div>
</div>

@endsection
@section('before-scripts-end')
@include('includes.scripts.script_fecha.scripts_filtro_fecha')
@stop
