@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
            <h1><i class="fa fa-exchange"></i> Documentos Digitales - Modificaciones Pendientes Detalles</h1><br>

    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')


<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title"> Modificación de Documento Digital</h5>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
            <div class="col-sm-12">
                
                <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>
        <div class="text-center">
                    <br>  
                    <br>  
                        <h5 style="color:DarkBlue;"><b>Previsualización PDF</b></h5>
                        <br>      
                      
                        <embed src="{{asset($editRegistro->url_vista)}}" type="application/pdf" width="100%" height="500px" />

                    <br><br><br><br>
                    <div class="callout col-sm-8 offset-sm-2">
                        <h5 style="color:DarkBlue;"><b>Observación de Modificación</b></h5>
                        <br>          
                        <p>{{$datosDerivacion->observacion}}</p>
                    </div>
                    <br>
                   
                </div>  
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

{!! Form::open(['route' => ['registro_derivado_update' , $editRegistro->id,] , 'files' => true, 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','id'=>'form-create']) !!}

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-danger card-outline">
        <h3 class="card-title">Editar Propiedades del Documento</h3>
      </div>
      <div class="card-body">

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>

        <div class="callout col-sm-8 offset-sm-2" style="color: #737373;">
          <h4 style="font-size: 14px;"><i class="fa fa-info-circle" style="color: #ff0000;"></i> Nota:</h4>
          <span>Los campos marcados con un asterisco (*) son obligatorios.</span>
        </div><br>

        <center>

        <div class="form-group row">
          <div class="col-sm-3 col-form-label">
            <b>Firmante Actual</b>
          </div>
          <div class="col-sm-6">
          <table class='table table-striped'>
          
          <div class="form-group row">
          {!! Form::label('firmante','¿Desea Cambiar Firmante Actual?', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            <label class="radio-inline">
                {{ Form::radio('firmante', '1')}} Si
            </label>
            <label class="radio-inline">
                {{ Form::radio('firmante', '0', 'active')}} No
            </label>
          </div>
        </div> 
              
          @if (count($datosFirmante)>0)
            @foreach($datosFirmante as $key => $value)                 
            <tr>
              
              <td>{{$value->username->nombre}}</td>
              <td id="eliminarF2" style="display:none;">
                  @if ($value->estatus == 0)
                  <a title="Eliminar" class="btn btn-sm btn-danger"  OnClick="Eliminar('{{$value->id}}','{{$editRegistro->id}}')"><i class="fa fa-trash"></i></a>
                  @endif
              </td>
            </tr>                
          @endforeach
          @endif
          
          </table>   
          </div>
        </div>
        
        </center>

       <!--  <div class="form-group row" id="subr" style="display:none;" style="text-align:center">
          {!! Form::label('sub','¿Va a Firmar Como Subrogante?', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            <label class="radio-inline">
                {{ Form::radio('sub', '1')}} Si
            </label>
            <label class="radio-inline">
                {{ Form::radio('sub', '0')}} No
            </label>
          </div>
        </div> -->
        <div class="form-group row" id="nota" style="display:none;" style="text-align:center">
        <div class="col-sm-3 col-form-label">
        </div>
        <div class="col-sm-6">
        <p style="color:Red;"><b>NOTA:</b> Primero Elimine al Firmante Anterior y luego agregue al Nuevo Firmante.</p>
        <br>
        </div>
        </div>

        <div class="form-group row" id="firma" style="display:none;">
           {!! Form::label('firmantes', 'Enviar para Firma de(*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
          {!! Form::select('id_user_firmante',$selectFirmantes, $varNull, ['id'=>'id_user_firmante','name'=>'id_user_firmante', 'class'=>'form-control']) !!}
          </div>
        </div>  
        
        <!-- <div class="form-group row" id="subrogante" style="display:none;">
           {!! Form::label('firmantesS', 'Enviar para Firma de Subrogante de(*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
          {!! Form::select('id_user_firmante',$selectFirmantesS, $varNull, ['id'=>'id_user_firmante','name'=>'id_user_firmante', 'class'=>'form-control']) !!}
          </div>
        </div>  -->
 
<br><br>

        <div>
        </div>

        <div class="form-group row">
           {!! Form::label('Tipo Documento', 'Tipo de Documento(*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
          {!! Form::text('id_tipos_documentos_digitales', $editRegistro->tiposdocumentosdigitales->nombre, ['id'=>'id_tipos_documentos_digitales','name'=>'id_tipos_documentos_digitales', 'class'=>'form-control',  'readonly'=>'readonly']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('lugar','Lugar (*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
            {!! Form::select('lugar',$selectLugar, $lugarEdit, ['id'=>'lugar','name'=>'lugar', 'class'=>'form-control' ]) !!}
            </div>
        </div>

        <div class="form-group row">
           {!! Form::label('Categoría', 'Categoría del Documento(*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
          {!! Form::select('id_categoria',$categoriaDoc, $editRegistro->id_categoria, ['id'=>'id_categoria','name'=>'id_categoria', 'class'=>'form-control']) !!}
          </div>
        </div>
        
     
        <div class="form-group row" >
          {!! Form::label('reservado_distribucion','¿Desea que el Documento sea Distribuido Después de Ser Firmado? (*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6" style="text-align:center">
            <label class="radio-inline">
                {{ Form::radio('reservado_distribucion', '1', 'active')}} Si
            </label>
            <label class="radio-inline">
                {{ Form::radio('reservado_distribucion', '0')}} No
            </label>
          </div>
        </div>
        


        @if($editRegistro->n_memo) 
        <div class="form-group row" style="text-align:center">
          {!! Form::label('memo','¿Este documento tiene un número de Memo asociado? (*)', ['class' => 'col-sm-3 col-form-label']) !!}
        <div class="col-sm-6">
          <label class="radio-inline">
                {{ Form::radio('memo', '1', 'active')}} Si
          </label>
          <label class="radio-inline">
                {{ Form::radio('memo', '0')}} No
          </label>
        </div>
        </div>     

        <div class="form-group row" id="n_memo" style="display:flex;">
          {!! Form::label('n_memo', 'N° de Memo', ['class' => 'col-sm-3 col-form-label']) !!}
        <div class="col-sm-6">
          {!! Form::text('n_memo', $editRegistro->n_memo, ['class' => 'form-control', 'placeholder' => 'Ingrese N° de Memo...']) !!}
        </div>
        </div>

        @else
        <div class="form-group row" style="text-align:center">
          {!! Form::label('memo','¿Este documento tiene un número de Memo asociado? (*)', ['class' => 'col-sm-3 col-form-label']) !!}
        <div class="col-sm-6">
          <label class="radio-inline">
                {{ Form::radio('memo', '1')}} Si
          </label>
          <label class="radio-inline">
                {{ Form::radio('memo', '0', 'active')}} No
          </label>            
        </div>
        </div>     

        <div class="form-group row" id="n_memo" style="display:none;">
          {!! Form::label('n_memo', 'N° de Memo', ['class' => 'col-sm-3 col-form-label']) !!}
        <div class="col-sm-6">
          {!! Form::text('n_memo', $editRegistro->n_memo, ['class' => 'form-control', 'placeholder' => 'Ingrese N° de Memo...']) !!}
        </div>
        </div>
        @endif

    
        <div class="form-group row">
          {!! Form::label('titulo', 'Materia(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('titulo', $editRegistro->titulo, ['class' => 'form-control', 'placeholder' => 'Ingrese la Materia del Documento, Copie y Pegue, servirá para poder buscarla más tarde.']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('descripcion', 'Observaciones Internas', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('descripcion', $editRegistro->descripcion, ['class' => 'form-control', 'placeholder' =>'Estas no forman parte del Documento, es solo para comunicaciones internas con el firmante...']) !!}
          </div>
        </div>   
 
        <div class="field_wrapper">
        <div class="form-group row col-md-12">
          {!! Form::label('filename', 'Cargar PDF de Documento Digital Listo(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-md-6">
          {!! Form::file('filename', ['class' => 'form-control']) !!}

          <p style="color:Red;">* Cargue arhivo para reemplazar el documento cargado anteriormente.</p>
          </div>
        </div>
        </div><br><br>

        

        <!-- <center>
        <div class="form-group row col-sm-6">       
            <table class='table table-striped'>
                <thead>
                <tr>
                    <th>Orden de Derivaciones</th>
                    <th>Destinatarios de Derivaciones</th>
                    <th>Eliminar</th>
                <tr>
                </thead>
                <tbody>
                @if (count($datosVB)>0)
                  @foreach($datosVB as $key2 => $value2)                 
                  <tr>
                  <td>{{$key2+1}}</td>
                    <td>{{$value2->nombre}}</td>
                    <td>
                      @if ($value2->estatus == 0)
                        <a title="Eliminar" class="btn btn-sm btn-danger"  OnClick="Eliminar2('{{$value2->id}}','{{$editRegistro->id}}')"><i class="fa fa-trash"></i></a>
                      @endif
                    </td>
                  </tr>                
                @endforeach
                @endif
                </tbody>
            </table>         
        </div>
        </center><br><br>

        <div class="form-group row">
          {!! Form::label('desti','¿Desea Agregar Destiatarios a la Lista?', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            <label class="radio-inline">
                {{ Form::radio('desti', '1')}} Si
            </label>
            <label class="radio-inline">
                {{ Form::radio('desti', '0')}} No
            </label>
          </div>
        </div>

        <div class="user_wrapper">
        <div class="form-group row" id="destinatario" style="display:none;">
           {!! Form::label('usuario destinatario', 'Seleccione más Destinatario(s)(*)', ['class' => 'col-sm-3 col-form-label']) !!} 
            <div class="col-sm-6">
          {!! Form::select('id_user_destino[]',$selectUsuariosid, $varNull, ['id'=>'id_user_destino[]','name'=>'id_user_destino[]', 'class'=>'form-control']) !!}
          <a href="javascript:void(0);" class="add_button" title="Add signers"><i class="fa fa fa-plus"></i></a>
          </div>
        </div>
        </div><br><br>

        <div class="form-group row">
          {!! Form::label('derivar','¿Continuan las Derivaciones?', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            <label class="radio-inline">
                {{ Form::radio('derivar', '1')}} Si
            </label>
            <label class="radio-inline">
                {{ Form::radio('derivar', '0')}} No
            </label>
          </div>
        </div>

        <div class="field_wrapper">
        <div class="form-group row col-md-12" id="documentoW" style="display:none;">
          {!! Form::label('filename2', 'Cargar Word editable(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-md-6">
          {!! Form::file('filename2', ['class' => 'form-control']) !!}
          </div>
        </div>
        </div>

        <div class="form-group row" id="observacion" style="display:none;">
          {!! Form::label('observacion', 'Observación', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('observacion', null, ['class' => 'form-control', 'placeholder' =>'Escriba una breve observacion...']) !!}
          </div>
        </div>         -->

        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! Form::submit('Siguiente', ['class' => 'btn btn-success btn-sm','title'=>'Siguiente']) !!}
              {!! link_to_route('registros_derivados', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) !!}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{!! Form::close() !!}

@endsection
@section('before-scripts-end')
@include('includes.scripts.firmantes_docdigitales')
@include('includes.scripts.destinatarios_docdigitales')

<script type="text/javascript">
$(document).ready(function(){
    var maxUser = 10; //Input users increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.user_wrapper'); //Input user wrapper
    var userHTML = '<div class="form-group row col-md-12"><div class="col-md-3"></div><div class="col-md-6">{!! Form::select("id_user_destino[]",$selectUsuariosid, $varNull, ["id"=>"id_user_destino[]","name"=>"id_user_destino[]", "class"=>"form-control"]) !!}<a href="javascript:void(0);" class="remove_button" title="Remove signers"><i class="fa fa fa-minus"></i></a></div></div>'; //New input user html
    var x = 1; //Initial user counter is 1
    $(addButton).click(function(){ //Once add button is clicked
        if(x < maxUser){ //Check maximum number of input users
            x++; //Increment user counter
            $(wrapper).append(userHTML); // Add user html
        }
    });
    $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove user html
        x--; //Decrement user counter
    });
});
</script>

<script>
$(document).ready(function(){
  $('input[type=radio][name=firmante]').on('change', function(){
    let firmante = this.value;
    if(firmante == 1){
      $("#firmanteA").css("display", "flex");
      $("#eliminarF").css("display", "flex");
      $("#eliminarF2").css("display", "flex"); 
      $("#firma").css("display", "flex");  
      $("#nota").css("display", "flex");    
    }
    if(firmante == 0){
      $("#firmanteA").css("display", "none");
      $("#eliminarF").css("display", "none");
      $("#eliminarF2").css("display", "none");
      $("#firma").css("display", "none");
      $("#nota").css("display", "none"); 
      //$("#subrogante").css("display", "none");
    }
  });
});
</script>

<script>
$(document).ready(function(){
  $('input[type=radio][name=desti]').on('change', function(){
    let desti = this.value;
    if(desti == 1){
      $("#destinatario").css("display", "flex");
    }
    if(desti == 0){
      $("#destinatario").css("display", "none");
    }
  });
});
</script>
<script>
$(document).ready(function(){
  $('input[type=radio][name=derivar]').on('change', function(){
    let derivar = this.value;
    if(derivar == 1){
      $("#documentoW").css("display", "flex");
      $("#observacion").css("display", "flex");
    }
    if(derivar == 0){
      $("#documentoW").css("display", "none");
      $("#observacion").css("display", "none");
    }
  });
});
</script>
<script>
$(document).ready(function(){
  $('#id_categoria').on('change', function(){
    let id_categoria = this.value;
    console.log(id_categoria)
    if(id_categoria != 1){
      $("#reservadoDistribucion").css("display", "flex");     
    }
    if(id_categoria == 1){
      $("#reservadoDistribucion").css("display", "none");
    }
    console.log('si')
  });
});
</script>
<script>
$(document).ready(function(){
  $('input[type=radio][name=memo]').on('change', function(){
    let memo = this.value;
    if(memo == 1){
      $("#n_memo").css("display", "flex");     
    }
    if(memo == 0){
      $("#n_memo").css("display", "none");
    }
  });
});
</script>
<!-- <script>
$(document).ready(function(){
  $('input[type=radio][name=sub]').on('change', function(){
    let sub = this.value;
    if(sub == 1){
      $("#firma").css("display", "none");
      $("#subrogante").css("display", "flex");
    }
    if(sub == 0){
      $("#firma").css("display", "flex");
      $("#subrogante").css("display", "none");
    }
  });
});
</script> -->
@stop