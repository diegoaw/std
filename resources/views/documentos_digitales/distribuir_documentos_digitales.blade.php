
@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-5">
      <h1><i class="fa fa-envelope"></i> Distibución Documentos Digitales Firmados</h1>
    </div>
    <div class="col-sm-7">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-envelope"></i> Distribución Documentos Digitales Firmados</li>
        <li class="breadcrumb-item active">Detalles</li>
      </ol>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
<link rel="stylesheet" href="{{asset('css/chosen/chosen.css')}}">
@endsection
@section('content')
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title"><b>PREVISUALIZACIÓN DE DOCUMENTO DIGITAL FIRMADO</b></h5><br>
          <div class="card-body table-responsive p-0">
          @if($Registro->registro_id == null )
          <embed src="{{$Registro->documento}}" type="application/pdf" width="100%" height="500px" />
          @else
          <embed src="{{asset($Registro->documento)}}" type="application/pdf" width="100%" height="500px" />
          @endif
          </div>
    </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title">  Distribución de {{$Registro->tipodocumento->nombre }} N° {{$Registro->correlativo }} </h5>

      </div>
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">

            {!! Form::open(['route' => ['enviodistribucion.folios' , $Registro->id], 'method' => 'post' , 'class'=>"form-horizontal"]) !!}

            <br>
            <div class="form-group row">
              <div class="col-sm-6 offset-sm-3">
                @include('includes.messages')
              </div>
            </div>
            <div class="callout col-sm-8 offset-sm-2" style="color: #737373;">
              <h4 style="font-size: 14px;"><i class="fa fa-info-circle" style="color: #ff0000;"></i> Nota:</h4>
              
              <h4 style="text-align:center; color:Red;">Enviar solo hasta 5 destinatarios.</h4>
              
            </div>
            <br>
            
              
              <h4><center><b>Distribución Interna</b></center></h4>
                
            <br>
            <br>
            <br>
           <!-- INICIO CORREOS INTERNOS -->
            <div class="form-group row" style="text-align:center;" >
              <label for="correos_internos" class="col-sm-3 col-form-label">Destinatarios Internos </label>
              <div class="col-sm-6" style="text-align:left;" >
              {{ Form::select('correos_internos[]', $selectUsuarios, null, ['class' => 'chosen-select', 'multiple','col-sm-10', 'data-placeholder' => 'Seleccione uno o más destinatarios internos','style'=>'width:nowrap; !important']) }}
              </div>
              
            </div>
            <br>
            <!-- <div class="form-group row" style="text-align:center;" >
              {!! Form::label('distribucion_observacion', 'Observaciones Distribución', ['class' => 'col-sm-3 col-form-label']) !!}
              <div class="col-sm-6">
                {!! Form::textarea('distribucion_observacion', null, ['class' => 'form-control', 'placeholder' =>'Observaciones Distribución...']) !!}
              </div>
            </div>  -->  
            <!-- FIN CORREOS INTERNOS -->
            <h4><center><b>Distribución Externa</b></center></h4>
            
            <br>
            <br>
            <br>

            <div class="form-group row">
              {!! Form::label('docdigital','¿Distribución por Docdigital o Ventanilla?', ['class' => 'col-sm-3 col-form-label']) !!}
              <div class="col-sm-6" style="text-align:center;">
                <label class="radio-inline">
                    {{ Form::radio('docdigital', '1')}} Si
                </label>
                <label class="radio-inline">
                    {{ Form::radio('docdigital', '0', 'active')}} No
                </label>
              </div>
            </div>
            <br>
            <center>
              <div class="col-sm-10">
                <label for="correos_externos" class=" col-form-label">Destinatarios Externos </label>
              </div>
              <div class="col-sm-10">
                <input class="form-control rounded-0" id="correos_externos[]" name="correos_externos[]" placeholder="Ingrese Destinatario Externo 1" rows="3" >
              </div><br>
              <div class="col-sm-10">
                <input class="form-control rounded-0" id="correos_externos[]" name="correos_externos[]" placeholder="Ingrese Destinatario Externo 2" rows="3" >
              </div><br>
              <div class="col-sm-10">
                <input class="form-control rounded-0" id="correos_externos[]" name="correos_externos[]" placeholder="Ingrese Destinatario Externo 3" rows="3" >
              </div><br>
              <div class="col-sm-10">
                <input class="form-control rounded-0" id="correos_externos[]" name="correos_externos[]" placeholder="Ingrese Destinatario Externo 4" rows="3" >
              </div><br>
              <div class="col-sm-10">
                <input class="form-control rounded-0" id="correos_externos[]" name="correos_externos[]" placeholder="Ingrese Destinatario Externo 5" rows="3" >
              </div><br>
              <div id='inputsce'></div>
            </center>
            <br>
            <br>
            <center><button type="submit" class="btn btn-primary">Distribuir y/o Completar</button></center>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
@section('before-scripts-end')
<script type="text/javascript" src="{{asset('js/chosen/chosen.jquery.js')}}"></script>
<script type="text/javascript">
    $(".chosen-select").chosen({
      width: "100%",
      max_selected_options: 5,
      no_results_text: "No se encontraron resultados para "
    });
  </script>
  
@stop
