@extends('layouts.out2')
@section('after-styles-end')
<style type="text/css">
  body{
    background-color: #071221 !important;
  }
  .letrasTitulo {
    color: #FFF !important;
  }
</style>
@endsection
@section('content')
<html>
<body>
<div>


<br><br><center><h2 class="letrasTitulo"> Validación de Información de Firma y Documento Digital</h2></center><br><br>



@if($documento->id_categoria ==1)
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                 <!--  <img class="profile-user-img img-fluid img-circle"
                       src="https://lh3.googleusercontent.com/proxy/CwephsXIxpl1DjTtoWYDvJcUyKE317EzeZTj_0Z22jI2t28I8dNme2hovx_UxLCEdPA-crJXP1RoeW7zc18_6ZLHp8ALWMp86reBP98FJypctRieJjMTBIKDirP9NnsB7A"
                       alt="User profile picture"> -->
                </div>
                <h3 class="profile-username text-center"></h3>
                <p class="text-muted text-center">Información del Documento Digital</p>
                <ul class="list-group list-group-unbordered mb-3">
                <li class="list-group-item">
                    <b>Materia Documento</b> <a class="float-right">{{$documento->titulo}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Tipo Documento</b> <a class="float-right">{{$documento->tiposdocumentosdigitales->nombre}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Correlativo / Folio</b> <a class="float-right">{{$documento->correlativo}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Categoría Documento</b> <a class="float-right">{{$documento->categoriadocumentosdigitales->nombre}}</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Documento</a></li>
                  <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Información del Documento</a></li>
                  <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Firma</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                    <embed src="{{asset($documento->url_firmado)}}" type="application/pdf" width="100%" height="800px" />
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="timeline">
                    <!-- The timeline -->
                    <div class="timeline timeline-inverse">
                    

                       <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Materia Documento</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" placeholder="" value="{{$documento->titulo}}" disabled="">
                        </div>
                      </div> 

                        <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Descripción  Documento</label>
                        <div class="col-sm-10">
                         <textarea class="form-control" rows="5" placeholder="{{$documento->descripcion}}" value="" disabled=""></textarea>
                        </div>
                      </div> 

                       <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Correlativo / Folio</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" placeholder="" value="{{$documento->correlativo}}" disabled="">
                        </div>
                      </div> 

                       <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Tipo Documento</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" placeholder="" value="{{$documento->tiposdocumentosdigitales->nombre}}" disabled="">
                        </div>
                      </div> 
                     
                       <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Categoría Documento</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" placeholder="" value="{{$documento->categoriadocumentosdigitales->nombre}}" disabled="">
                        </div>
                      </div>
                    
                  </div>
                  </div>
                  <div class="tab-pane" id="settings">
                  <div class="timeline timeline-inverse">
                  

                       <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Nombre Firmante</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" placeholder="" value="{{$listadoFirmantes->username->nombre}} " disabled="">
                        </div>
                      </div> 

                       <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Departamento Firmante</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" placeholder="" value="{{$listadoFirmantes->username->division}} " disabled="">
                        </div>
                      </div> 
                     
                       <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Correo Firmante</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" placeholder="" value="{{$listadoFirmantes->username->email}}" disabled="">
                        </div>
                      </div>
                    
</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
@else
<section class="content">
      <div class="container-fluid">
        <div class="row">
        <div class="card-body" style="background-color: #FFF">

                <center><h4><b>
                  Documento Reservado y Firmado en la Subsecretaría del Deporte por: </b><br>
                  <br><b style="color:DarkBlue;">{{$listadoFirmantes->username->user->name}} 
                  <br>{{$listadoFirmantes->username->user->email}}
                  </b></h4></center> 
                
              </div>
        </div>
     
</div>
</section>
@endif

<div class="form-group">
    <div class="col-sm-12">
        <div class="text-center"><br><br>
        <a href="{{ url()->previous() }}" class="btn btn-danger">Salir</a>
        </div>
    </div>
</div>

</div>
</body>
</html>

@endsection
@section('before-scripts-end')
@stop