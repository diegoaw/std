@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-link"></i><b> Documentos Digitales - Enlace Generado</b></h1><br> 
    </div>    
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-danger card-outline">
        <h3 class="card-title"><b>Enlace Generado</b></h3>
      </div>
      <div class="card-body">

        <div class="form-group row">
          {!! Form::label('descripcion', 'Descripción', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('descripcion', $datosEnlace->descripcion, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
          </div>
        </div> 

        <div class="form-group row">
          {!! Form::label('url_aws', 'Enlace', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('url_aws', $datosEnlace->url_aws, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              <button class="btn btn-primary" onclick="copiarAlPortapapeles('textoeditado')">Copiar</button>
              {!! link_to_route('generar_enlace', 'Finalizar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Finalizar']) !!}
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </div>
</div>

@stop
@section('after-scripts-end')
<script type="text/javascript">


function copiarAlPortapapeles(id_elemento) {
  var aux = document.getElementById("url_aws");
  aux.select();
  document.execCommand("copy");
  alert("Enlace Copiado");
}
    
 
  </script>
@endsection