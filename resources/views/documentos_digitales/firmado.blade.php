@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-file-text-o"></i> Documento Digital Firmado</h1><br>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline"> 
      <div class="card-body">
        <div class="box-body">
            <div class="col-sm-12">
                <div class="text-center">
                  <br>  
                  <h5 style="color:DarkBlue;"><b>PDF de Documento Digital Firmado Correctamente</b></h5>
                  <br><br>         
                  <embed src="{{asset($editRegistro->url_firmado)}}" type="application/pdf" width="100%" height="500px" />
                  <br>  
                </div>  
            </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
              <div class="text-center">
                  {!! link_to_route('inicio', 'Finalizar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


@endsection
@section('after-styles-end')
@endsection
@section('content')


@endsection
@section('before-scripts-end')
@stop
