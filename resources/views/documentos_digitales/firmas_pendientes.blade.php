
@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-pencil"></i> Documentos Digitales - Firmas Pendientes</h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')






<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">Listado de Firmas Pendientes</h5>
            
          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;"> 
                        <th nowrap style="color:white"><center>Fecha</center></th>
                        <th nowrap style="color:white"><center>Tipo</center></th>
                        <th  style="color:white">Materia</th>
                        <th nowrap style="color:white"><center>Detalle</center></th>
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($dataDefinitiva)>0)
                    @foreach ($dataDefinitiva as $indice => $tramite)
                      @if($tramite->vistos)
                          <tr>
                           <td nowrap><center>{{$tramite->registrodocumentos->created_at}}</center></td>
                           <td nowrap><center>{{$tramite->registrodocumentos->tiposdocumentosdigitales->nombre}}</center></td>
                           <td >{{$tramite->registrodocumentos->titulo}}</td>
                           <td nowrap><center><a class="btn accion btn-success accion" target="_blank" href="{{route('firmas_pendientes_detalles',$tramite->id_registro_documento_digitales)}}"><i class="fa fa-eye"></i></a></center></td>
                           </tr>
                      @endif
                  @endforeach
                @endif
                </tbody>
              </table>
          </div>
          
          
      </div>
    </div>
</div>

@endsection
@section('before-scripts-end')
@stop
