
@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
            <h1><i class="fa fa-pencil"></i> Documentos Digitales - Firmas Pendientes Detalles</h1><br>

    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
{!! Form::open(['route' => ['firmando' , $statusfirmante->id],  'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','id'=>'form-create' ,  'name'=>'firmando']) !!}

<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">PREVISUALIZACIÓN DEL DOCUMENTO</h5>

          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
         
                    
            
          <embed src="{{asset($detallesFirmar->url_vista)}}" type="application/pdf" width="100%" height="500px" />
          
          
          </div>
          <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center" id="Firmar">
              <!--{!! Form::submit('Firmar', ['class' => 'btn btn-primary','title'=>'Firmar', 'onclick'=>'confirmation()']) !!} -->
              <a title="Firmar" class="btn btn-primary"  OnClick="confirmation()">Firmar</a>
              <a class="btn accion btn-warning accion" href="{{route('registro_derivado_editar_firmante',$detallesFirmar->id)}}">Devolver para Modificación</a>
              {!! link_to_route('inicio', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) !!}
              </div>
          </div>
        </div>
      </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('before-scripts-end')
@include('includes.scripts.confirmacion_firma_docdigitales')
@endsection
