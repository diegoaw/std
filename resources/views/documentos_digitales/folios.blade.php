@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-sort-numeric-desc"></i> Consulta Folios</h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title">Filtrar Folios por</h5>

      </div>
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">
                  <div class="form-row">
                  {!! Form::open(['route' => 'folios', 'method' => 'get' , 'class'=>"form-horizontal"]) !!}
                  <div class="form-row">
                    <div class="col-sm-2">
                      <label for="correlativo" class=" col-form-label">Correlativo:</label>
                      <input type="number" class="form-control" id="correlativo" name="correlativo"  value="{{$correlativo}}" placeholder="Correlativo">
                    </div>
                    <div class="col-sm-0">
                      <label for="tramite" class=" col-form-label">N° Trámite: </label>
                      <input type="number" class="form-control" id="tramite" name="tramite" value="{{$tramite}}" placeholder="N° Trámite">
                    </div>

                    <div class="col-sm-3">
                      <label for="tipo_documento" class=" col-form-label">Tipo Documento: </label>
                      {!! Form::select('tipo_documento',$tiposDocumentos, $tipoDocumento, ['id'=>'tipo_documento','name'=>'tipo_documento', 'class'=>'form-control']) !!}
                    </div>

                    <div class="col-sm-3">
                      <label for="ano" class=" col-form-label">Año: </label>
                      {!! Form::select('ano',$anos,$ano, ['id'=>'ano','name'=>'ano', 'class'=>'form-control']) !!}
</div>

                    <div class="col-sm-3">
                      <label for="estado" class=" col-form-label">Estado: </label>
                      {!! Form::select('estado',$estados, $estado,['id'=>'estado','name'=>'estado', 'class'=>'form-control']) !!}
                    </div>

                    
                    
                  </div>
                  <button type="submit" class="btn btn-primary pull-right">Buscar</button>
              {!! Form::close() !!}

                  </div>
                 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



@if(count($listado)>0)
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">Listado de Folios</h5>

          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th nowrap style="color:white"><center>Correlativo</center></th>
                        <th nowrap style="color:white"><center>Tipo Documento</center></th>
                        <th nowrap style="color:white"><center>Año</center></th>
                        <th nowrap style="color:white"><center>N° Trámite</center></th>                        
                        <th nowrap style="color:white"><center>Estado</center></th>
                        <th nowrap style="color:white"><center>Documento</center></th>
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($listado)>0)
                  @foreach ($listado as $indice => $registro)
                    <tr>
                      <td nowrap><center>{{$registro->correlativo}}</center></td>
                      <td><center>@if ($registro->proceso_id == '2')  Memorándum Electrónico @endif @if ($registro->proceso_id == '4')  Oficio Electrónico @endif</center></td>
                      <td nowrap><center>{{$registro->fecha}}</center></td>
                      <td nowrap><center>{{$registro->tramite_id}}</center></td>
                      @if ($registro->estatus != 'Incompleto') <td><center> {{$registro->estatus}} @else <td style="background-color: #cd0b27; color: white;"><center>{{$registro->estatus}} @endif</center></td>
                      <td nowrap><center>
                      @if($registro->anexos)
                      <a href="{{$registro->anexos}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
                      @endif  
                      </center></td>
                    </tr>
                  @endforeach
                @endif
                </tbody>
            </table>
          <br>
        </div>
      </div>
     
    </div>
</div>

<br>
@else
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">Listado de Folios</h5>
          </div>
          <div class="card-body table-responsive p-0">
              <center>  No se encontraron resultados.</center>
          <br>
        </div>
      </div>
    </div>
</div>
@endif

@endsection
@section('before-scripts-end')
@stop
