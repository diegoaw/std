@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <!-- <h1><i class="fa fa-sort-numeric-desc"></i> Galeria de Firmas</h1><br> -->
      <h1><img src="{{ asset('img/signature.png') }}" alt="firma" style=" color:#ffffff " width="20" height="20">  Galería de Firmas</h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')

<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">Descarga Foto de Firma</h5>

          </div>
          <!-- /.card-header -->
          <!-- Start Content images firmas-->
          <div class="card-body table-responsive p-0">
              <div class="row" style="padding:30px">
                @if(count($firmas)>0)
                  @foreach ($firmas as $indice => $tramite)
                  <div class="col">
                    <a href="{{ asset($tramite->ruta) }}" download>
                      <div class="panel panel-default card">
                        <div class="panel-body">
                            <img src="{{ asset($tramite->ruta) }}" class="img-responsive rounded mx-auto d-block" alt="firma" style="width: 300px;height: 200px;object-fit: cover;">
                            <br><h4 class="text-center">{{$tramite->nombre}}</h4>
                         </div>
                      </div>
                    </a>
                  </div>
                  @endforeach
                @endif
              </div>
              <div class="container">    
                <div class="card-body">
                  <div class="box-body">
                    <br>
                      <div class="form-group">
                        <div class="col-sm-12">
                          <div class="text-center">
                            <br><br>
                            {!! link_to_route('recursos_apoyo', 'Volver', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>  
           </div>
           <!-- End Content images firmas-->
       </div>
     </div>
  
  </div>

@endsection
@section('before-scripts-end')
@stop
