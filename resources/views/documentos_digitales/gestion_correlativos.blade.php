@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-list-ol"></i> Gestión Folios</h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title">Filtrar Folios por</h5>
      </div>
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">
              {!! Form::open(['route' => 'folios2', 'method' => 'get' , 'class'=>"form-horizontal"]) !!}
                  <div class="form-row">
                    <div class="col-sm-2">
                      <label for="correlativo" class=" col-form-label">Correlativo:</label>
                      <input type="number" class="form-control" id="correlativo" name="correlativo"  value="{{$correlativo}}" placeholder="Correlativo">
                    </div>
                    <div class="col-sm-0">
                      <label for="tramite" class=" col-form-label">N° Trámite: </label>
                      <input type="number" class="form-control" id="tramite" name="tramite" value="{{$tramite}}" placeholder="N° Trámite">
                    </div>

                    <div class="col-sm-3">
                      <label for="tipo_documento" class=" col-form-label">Tipo Documento: </label>
                      {!! Form::select('tipo_documento',$tiposDocumentos, $tipoDocumento, ['id'=>'tipo_documento','name'=>'tipo_documento', 'class'=>'form-control']) !!}
                    </div>

                    <div class="col-sm-3">
                      <label for="ano" class=" col-form-label">Año: </label>
                      {!! Form::select('ano',$anos,$ano, ['id'=>'ano','name'=>'ano', 'class'=>'form-control']) !!}
                    </div>

                    <div class="col-sm-3">
                      <label for="lugar" class=" col-form-label">Lugar: </label>
                      {!! Form::select('lugar',$lugares, $lugar, ['id'=>'lugar','name'=>'lugar', 'class'=>'form-control']) !!}
                    </div>

                    <div class="col-sm-3">
                      <label for="estado" class=" col-form-label">Estado: </label>
                      {!! Form::select('estado',$estados, $estado,['id'=>'estado','name'=>'estado', 'class'=>'form-control']) !!}
                    </div>

                    <div class="col-sm-2">
                      <label for="materia" class=" col-form-label">Materia:</label>
                      <textarea class="form-control" id="materia" name="materia" placeholder="Materia" >{{$materia}}</textarea>
                    </div>
                    
                  </div>
                  <button type="submit" class="btn btn-primary pull-right">Buscar</button>
              {!! Form::close() !!}
                  
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@if(count($listado)>0)
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">Listado de Folios</h5>

            @if (auth()->user()->hasPermissionNotAdmin(['15-18']) || auth()->user()->hasPermissionNotAdmin(['15-17']) || auth()->user()->hasPermissionNotAdmin(['15-23']))
            <b>
            {!! link_to_route('reserva_folios', 'Apartar Folio(s)', [], ['class' => 'btn btn-warning btn-sm pull-right','title'=>'Volver']) !!}
            </b>

            <div class="col-md-6">
            {!! Form::open(['route' => 'reporte_folios', 'method' => 'get' , 'class'=>"form-horizontal"]) !!}
              <input type="submit" onclick="inload()" class= "btn btn-primary pull-right" name="imprimir" value='XLS' id='imprimir'>
            {!! Form::close() !!}
            </div>
            @endif

          </div>
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th nowrap style="color:white"><center>Correlativo</center></th>
                        <th nowrap style="color:white"><center>Fecha Creación</center></th>
                        <!-- <th nowrap style="color:white"><center>Motivo Reserva</center></th> -->
                        <th nowrap style="color:white"><center>N° Trámite</center></th>                        
                        <th nowrap style="color:white"><center>Tipo Documento</center></th>
                        <th nowrap style="color:white"><center>Año</center></th>
                        <th nowrap style="color:white"><center>Lugar</center></th>
                        <th nowrap style="color:white"><center>Estado</center></th>
                        <th nowrap style="color:white"><center>Materia</center></th>
                        <th nowrap style="color:white"><center>Documento</center></th>
                        <th nowrap style="color:white"><center>Distribución</center></th>
                        <!-- <th nowrap style="color:white"><center>Acciones</center></th> -->
                    </tr>
                </thead>
                <tbody class="panel">
                
                @if(count($listado)>0)
                  @foreach ($listado as $indice => $registro)
                 
                   
                    <tr>
                      <td nowrap><center>{{$registro->correlativo}}</center></td>
                      <td nowrap><center>{{$registro->created_at}}</center></td>
                      <!-- <td ><center>{{$registro->motivo_reserva}}</center></td> -->
                      <td nowrap><center>{{$registro->id_registro}}</center></td>
                      <td nowrap><center>{{$registro->tipodocumento->nombre}}</center></td>
                      <td nowrap><center>{{$registro->ano}}</center></td>
                      <td nowrap><center>
                      @if($registro->id_registro == null)
                        {{$registro->lugar}}
                      @endif
                      @if($registro->id_registro != null)
                      {{$registro->lugar}}
                      @endif
                      </center></td>
                      <td nowrap><center>{{$registro->estatus}}</center></td>
                      <td><center>
                      @if($registro->id_registro == null)
                        {{$registro->materia}}
                      @endif
                      @if($registro->id_registro != null)
                      {{$registro->materia}}
                      @endif
                      </center></td>


                      <td><center> 
                        @if($registro->documento && $registro->reservado_distribucion == 1 || $registro->documento && $registro->distribuido == 1)
                          
                          
                        
                              @if($registro->id_registro == null && $registro->estatus == 'OCUPADO')
                                <a type="button" class="btn accion btn-success accion" href="{{$registro->documento}}" target="_blank" download><i class="fa fa-download"></i></a>
                              @endif
                              
                              @if($registro->id_registro != null && $registro->estatus == 'OCUPADO')
                                <a type="button" class="btn accion btn-success accion" href="{{asset($registro->documento)}}" target="_blank" download><i class="fa fa-download"></i></a>
                              @endif
                              
                              
                        @endif
                      </center></td>


                      <td><center> 
                      @if($registro->distribuido == 1)
                        <b style="color:Red;">DISTRIBUIDO</b>
                      @elseif($registro->tipodocumento->nombre == 'Memorandum Electrónico')
                        <b style="color:DarKBlue;">SIN INFORMACIÓN</b>
                      @elseif($registro->documento && $registro->reservado_distribucion == 1 && $registro->tipodocumento->nombre != 'Memorandum Electrónico')
                      <a class="btn accion btn-info accion" href="{{route('distribucion.folios',$registro->id)}}">Distribuir</a>
                      @else
                      
                      <b style="color:Gold;">NO DISTRIBUIR</b>
                      @endif
                      </center></td>

                  

                      
                      <!-- <td nowrap>@if($registro->estatus == 'APARTADO') 
                        @if (auth()->user()->hasPermissionNotAdmin(['15-20']))
                        <a class="btn accion btn-danger accion" href="{{route('reserva_folio_editar',$registro->id)}}">Editar Lugar</a> 
                        @endif
                        <a></a>
                        @if (auth()->user()->hasPermissionNotAdmin(['15-19']))
                        <a class="btn accion btn-primary accion" href="{{route('folio_ocupar',$registro->id)}}">Justificar Folio</a>
                        @endif
                      @endif</td> -->
                      
                    </tr>
                   
                    
                  @endforeach  
                @endif
                </tbody>
            </table>
          <br>
        </div>
      </div>
    </div>
</div>
<br>
@else
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">Listado de Folios</h5>
            @if (auth()->user()->hasPermissionNotAdmin(['15-18']) || auth()->user()->hasPermissionNotAdmin(['15-17']) || auth()->user()->hasPermissionNotAdmin(['15-23']))
              {!! link_to_route('reserva_folios', 'Apartar Folio(s)', [], ['class' => 'btn btn-warning btn-sm pull-right','title'=>'Volver']) !!}
            @endif
          </div>
          <div class="card-body table-responsive p-0">
              <center>  No se encontraron resultados.</center>
          <br>
        </div>
      </div>
    </div>
</div>
@endif

@endsection
@section('before-scripts-end')
<script type="text/javascript">
function inload(){
  setTimeout(function(){ document.getElementById("load").style.display="none"; }, 5000);
}
</script>
<script>
  setInterval("location.reload()",60000);
</script>
@stop
