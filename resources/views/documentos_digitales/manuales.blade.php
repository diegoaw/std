@extends('layouts.master')
@section('page-header')
<br>
<style>
.fondo {
    background: url('{{ asset('img/fondo2.png')}}');
    background-position: center center;
    background-repeat: repeat;
    background-size: cover;
}

.fa{
  font-size: 25px !important;
}

.containers {
  max-width: 1550px;
  margin: 0% auto;
  padding: 20px;
  background-color: #fff;
  border-radius: 10px;
  overflow: hidden;
  box-sizing: border-box;
  box-shadow: 0 10px 35px rgba(0, 0, 0, 0.4);
}

.lightbox-gallery {
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
}

.lightbox-gallery div > embed {
  max-width: 900px;
  display: block;
}

.lightbox-gallery div {
  margin: 20px;
  flex-basis: 180px;
}

@media only screen and (max-width: 480px) {
  .lightbox-gallery {
    flex-direction: column;
    align-items: center;
  }

  .lightbox > div {
    margin-bottom: 10px;
  }
}

/*Lighbox CSS*/

.lightbox {
  display: none;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.7);
  position: fixed;
  top: 0;
  left: 0;
  z-index: 20;
  padding-top: 30px;
  box-sizing: border-box;
}

.lightbox embed {
  display: block;
  margin: auto;
}

.lightbox .caption {
  margin: 15px auto;
  width: 50%;
  text-align: center;
  font-size: 1em;
  line-height: 1.5;
  font-weight: 700;
  color: #eee;
}

.btn-outline-primary{
  color:#006FB3;
  background-color:transparent;
  background-image:none;
  border-color:#006FB3
}

.btn-outline-primary:hover{
  color:#fff;
  background-color:#006FB3;
  border-color:#006FB3
}

.btn-outline-primary.focus,.btn-outline-primary:focus{
  box-shadow:0 0 0 .2rem rgba(0,123,255,.5)
}

.btn-outline-primary.disabled,.btn-outline-primary:disabled{
  color:#006FB3;
  background-color:transparent
}

.btn-outline-primary:not(:disabled):not(.disabled).active,.btn-outline-primary:not(:disabled):not(.disabled):active,.show>.btn-outline-primary.dropdown-toggle{
  color:#fff;
  background-color:#006FB3;
  border-color:#006FB3
}

.btn-outline-primary:not(:disabled):not(.disabled).active:focus,.btn-outline-primary:not(:disabled):not(.disabled):active:focus,.show>.btn-outline-primary.dropdown-toggle:focus{
  box-shadow:0 0 0 .2rem rgba(0,123,255,.5)
}

</style>

<div class="container-fluid fondo">  

  <div>
    <br><br><br><br>
      <h1 class="text-center"><b><i class="fa fa-book"></i>  Manuales</h1><br>
      <center><img src="{{ asset('img/linea_gob.PNG') }}" alt="linea"></center> 
  </div>

  <br><br><br><br>
  
  <div class="containers row">
    <div class="lightbox-gallery">
    <br><br>
      <div class="form-group">
        <div class="col-sm-12">
          <center><a href="https://documentos-digitales-std.s3.us-west-2.amazonaws.com/Manuales/Guardar+Plantilla+como+Word+y+PDF2.pdf"  target="_blank"><h4><b style="color:DarkRed;">Guardar Plantilla como <i style="color:#006FB3">Word</i> y <i style="color:#006FB3">PDF</i></b></h4></a></center>
          <center><embed src="https://documentos-digitales-std.s3.us-west-2.amazonaws.com/Manuales/Guardar+Plantilla+como+Word+y+PDF2.pdf" type="application/pdf" width="400px" height="350px"/></center>
          <br>
          <center><a type="button" href="https://documentos-digitales-std.s3.us-west-2.amazonaws.com/Manuales/Guardar+Plantilla+como+Word+y+PDF2.pdf" class="btn btn-outline-primary" target="_blank" download>Descargar</a></center>
        </div>
      </div>   

      <div class="form-group">
        <div class="col-sm-12">
          <center><a href="https://documentos-digitales-std.s3.us-west-2.amazonaws.com/Manuales/Generar+Enlaces+Adjuntos2.pdf"  target="_blank"><h4><b style="color:DarkRed;">Generar Enlaces Adjuntos</b></h4></a></center>
          <center><embed src="https://documentos-digitales-std.s3.us-west-2.amazonaws.com/Manuales/Generar+Enlaces+Adjuntos2.pdf" type="application/pdf" width="400px" height="350px"/></center>
          <br>
          <center><a type="button" href="https://documentos-digitales-std.s3.us-west-2.amazonaws.com/Manuales/Generar+Enlaces+Adjuntos2.pdf" class="btn btn-outline-primary" target="_blank" download>Descargar</a></center>
        </div>
      </div>

      <div class="form-group">
        <div class="col-sm-12">
          <center><a href="https://documentos-digitales-std.s3.us-west-2.amazonaws.com/Manuales/Comprimir+Documentos2.pdf"  target="_blank"><h4><b style="color:DarkRed;">Comprimir Documentos</b></h4></a></center>
          <center><embed src="https://documentos-digitales-std.s3.us-west-2.amazonaws.com/Manuales/Comprimir+Documentos2.pdf" type="application/pdf" width="400px" height="350px"/></center>
          <br>
          <center><a type="button" href="https://documentos-digitales-std.s3.us-west-2.amazonaws.com/Manuales/Comprimir+Documentos2.pdf" class="btn btn-outline-primary" target="_blank" download>Descargar</a></center>
        </div>
      </div>

      <div class="form-group">
        <div class="col-sm-12">
          <center><a href="https://documentos-digitales-std.s3.us-west-2.amazonaws.com/Manuales/Bot%C3%B3n+para+Corregir+Documento+Cargado.pdf"  target="_blank"><h4><b style="color:DarkRed;">Botón para Corregir Documento Cargado</b></h4></a></center>
          <center><embed src="https://documentos-digitales-std.s3.us-west-2.amazonaws.com/Manuales/Bot%C3%B3n+para+Corregir+Documento+Cargado.pdf" type="application/pdf" width="400px" height="350px"/></center>
          <br>
          <center><a type="button" href="https://documentos-digitales-std.s3.us-west-2.amazonaws.com/Manuales/Bot%C3%B3n+para+Corregir+Documento+Cargado.pdf" class="btn btn-outline-primary" target="_blank" download>Descargar</a></center>
        </div>
      </div>

      <div class="form-group">
        <div class="col-sm-12">
          <center><a href="https://documentos-digitales-std.s3.us-west-2.amazonaws.com/Manuales/Manual_Documentos_Digitales_v1.0.pdf"  target="_blank"><h4><b style="color:DarkRed;">Manual Documentos Digitales v1.0</b></h4></a></center>
          <center><embed src="https://documentos-digitales-std.s3.us-west-2.amazonaws.com/Manuales/Manual_Documentos_Digitales_v1.0.pdf" type="application/pdf" width="400px" height="350px"/></center>
          <br>
          <center><a type="button" href="https://documentos-digitales-std.s3.us-west-2.amazonaws.com/Manuales/Manual_Documentos_Digitales_v1.0.pdf" class="btn btn-outline-primary" target="_blank" download>Descargar</a></center>
        </div>
      </div>
    </div>   
      
    <div class="container">    
      <div class="card-body">
        <div class="box-body">
          <br><br>
            <div class="form-group">
              <div class="col-sm-12">
                <div class="text-center">
                  {!! link_to_route('recursos_apoyo', 'Volver', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>  

    <br><br><br>

  
  <br><br>
</div>
@endsection
@section('before-scripts-end')
@stop
