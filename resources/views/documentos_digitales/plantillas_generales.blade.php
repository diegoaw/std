
@extends('layouts.master')
@section('page-header')
<br>
<style>
.fondo {
    background: url('{{ asset('img/fondo2.png')}}');
    background-position: center center;
    background-repeat: repeat;
    background-size: cover;
}

.fa{
  font-size: 25px !important;
}

.containers {
  max-width: 1000px;
  margin: 0% auto;
  padding: 20px;
  background-color: #fff;
  border-radius: 10px;
  overflow: hidden;
  box-sizing: border-box;
  box-shadow: 0 10px 35px rgba(0, 0, 0, 0.4);
}

.btn-link{
  color:#006FB3;
}

td {
  text-align: justify;
  font-size: 15px !important;
}

thead {
  text-align: center;
}

.eso {
  text-align: center;
}

.center {
  margin-left: auto;
  margin-right: auto;
}

</style>

<div class="container-fluid fondo">  

<div>
  <br><br><br><br>
    <h1 class="text-center"><b><i class="fa fa-text-height"></i>  Plantillas sin Personalizar</h1><br>
    <center><img src="{{ asset('img/linea_gob.PNG') }}" alt="linea"></center> 
</div>

<br><br>
  
<div class="containers row"> 
  <div class="card center">           
    <div class="card-body p-0">
      <div class="table-responsive">
        <br><br>
        <table class="table m-0">
          <thead>
          <tr style="background-color:#0A132D;">
            <th style="color:White;">Nombre Plantilla</th>
            <th style="color:White;">Tipo Documento</th>
            <th style="color:White;">Descargar</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>Plantilla Memorándum Electrónico</td>
            <td class="eso"><b class="badge badge-info">Memorándum Electrónico</b></td>
            <td class="eso"><a href="https://documentos-digitales-std.s3.us-west-2.amazonaws.com/Plantillas+No+Bloqueadas/Plantilla%2BMemorandums2.docx" target="_blank" download><i class="fa fa-file-text-o"></i></a></td>
          </tr>
          <tr>  
            <td>Plantilla Memorándum Electrónico <b style="color:DarkRed;">Espacios Bloqueados</b></td>
            <td class="eso"><b class="badge badge-info">Memorándum Electrónico</b></td>
            <td class="eso"><a href="https://documentos-digitales-std.s3.us-west-2.amazonaws.com/Plantillas+Bloqueadas/Plantilla%2BMemorandums%2BBloqueada.docx" target="_blank" download><i class="fa fa-file-text-o"></i></a></td>
          </tr>
          <tr>
            <td>Plantilla Memorándum Electrónico <b style="color:DarkBlue;">Rectifica Folios</b></td>
            <td class="eso"><b class="badge badge-info">Memorándum Electrónico</b></td>
            <td class="eso"><a href="https://documentos-digitales-std.s3.us-west-2.amazonaws.com/Plantilla+Memo+Rectificaci%C3%B3n/Memoramdum%2BElectronico%2BRectifica%2BFolio.docx" target="_blank" download><i class="fa fa-file-text-o"></i></a></td>
          </tr>
          <tr>
            <td>Plantilla Oficio Electrónico</td>
            <td class="eso"><b class="badge badge-danger">Oficio Electrónico</b></td>
            <td class="eso"><a href="https://documentos-digitales-std.s3.us-west-2.amazonaws.com/Plantillas+No+Bloqueadas/Plantilla%2BOficios2.docx" target="_blank" download><i class="fa fa-file-text-o"></i></a></td>
          </tr>
          <tr>
            <td>Plantilla Oficio Electrónico <b style="color:DarkRed;">Espacios Bloqueados</b></td>
            <td class="eso"><b class="badge badge-danger">Oficio Electrónico</b></td>
            <td class="eso"><a href="https://documentos-digitales-std.s3.us-west-2.amazonaws.com/Plantillas+Bloqueadas/Plantilla%2BOficios%2BBloqueado.docx" target="_blank" download><i class="fa fa-file-text-o"></i></a></td>
          </tr>
          <tr>
            <td>Plantilla Resolución Exenta Electrónica</td>
            <td class="eso"><b class="badge badge-success">Resolución Exenta Electrónica</b></td>
            <td class="eso"><a href="https://documentos-digitales-std.s3.us-west-2.amazonaws.com/Plantillas+No+Bloqueadas/Plantilla%2BResoluci%C3%B3n%2BExentas2.docx" target="_blank" download><i class="fa fa-file-text-o"></i></a></td>
          </tr>
          <tr>
            <td>Plantilla Resolución Exenta Electrónica <b style="color:DarkRed;">Espacios Bloqueados</b></td>
            <td class="eso"><b class="badge badge-success">Resolución Exenta Electrónica</b></td>
            <td class="eso"><a href="https://documentos-digitales-std.s3.us-west-2.amazonaws.com/Plantillas+Bloqueadas/Plantilla%2BResoluci%C3%B3n%2BBloqueada.docx" target="_blank" download><i class="fa fa-file-text-o"></i></a></td>
          </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="container">    
    <div class="card-body">
      <div class="box-body">
        <br>
          <div class="form-group">
            <div class="col-sm-12">
              <div class="text-center">
                {!! link_to_route('recursos_apoyo', 'Volver', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>  

  <br><br><br><br>
</div>
@endsection
@section('before-scripts-end')
@stop
