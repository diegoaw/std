@extends('layouts.master')
@section('page-header')
<br>
<style>
.fondo {
    background: url('{{ asset('img/fondo2.png')}}');
    background-position: center center;
    background-repeat: repeat !important;
    background-size: cover;
}

.fa{
  font-size: 25px !important;
}

.containers {
  max-width: 1000px;
  margin: 0% auto;
  padding: 20px;
  background-color: #fff;
  border-radius: 10px;
  overflow: hidden;
  box-sizing: border-box;
  box-shadow: 0 10px 35px rgba(0, 0, 0, 0.4);
}

.containerss {
  max-width: 1800px;
  margin: 0% auto;
  padding: 20px;
  background-color: #fff;
}

.btn-link{
  color:#006FB3;
}

td {
  text-align: justify;
  font-size: 18px !important;
}

thead {
  text-align: center;
  font-size: 18px !important;
}

.eso {
  text-align: center;
}

.center {
  margin-left: auto;
  margin-right: auto;
}

.nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
    color: #495057;
    background-color: #fff;
    border-color: #dee2e6 #dee2e6 #fff;
}

.nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
    color: #495057;
    background-color: #EEEEEE;
    border-color: #dee2e6 #dee2e6 #f8fafc;
}

</style>

<div class="container-fluid fondo">  

<div>
    <br><br><br><br>
      <h1 class="text-center"><b><i class="fa fa-text-width"></i>  Plantillas Personalizadas por Firmantes</h1><br>
      <center><img src="{{ asset('img/linea_gob.PNG') }}" alt="linea"></center> 
  </div>

<br><br>
  
<div class="containers row"> 

<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="memos-tab" data-toggle="tab" href="#memos" role="tab" aria-controls="memos" aria-selected="true"><h6><b class="badge badge-info">Memorándums Electrónicos</b></h6></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="oficios-tab" data-toggle="tab" href="#oficios" role="tab" aria-controls="oficios" aria-selected="false"><h6><b class="badge badge-danger">Oficios Electrónicos</b></h6></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="resoluciones-tab" data-toggle="tab" href="#resoluciones" role="tab" aria-controls="resoluciones" aria-selected="false"><h6><b class="badge badge-success">Resoluciones Exentas Electrónicas</b></h6></a>
  </li>
</ul>

<div class="containerss">
  <br>
  <div class="tab-content" id="myTabContent">

    <div class="tab-pane fade show active" id="memos" role="tabpanel" aria-labelledby="memos-tab">
      <div class="card center">           
        <div class="card-body p-0">
          <div class="table-responsive">
          <br><br>
            <table class="table m-0">
              <thead>
              <tr style="background-color:#0A132D;">
                <th style="color:White;">Plantilla Firmante</th>
                <th style="color:White;">Tipo Documento</th>
                <th style="color:White;">Descargar Plantilla</th>
                <th style="color:White;">Descargar Plantilla<br>Espacios Bloqueados</th>
              </tr>
              </thead>
              <tbody>
              @if(count($plantillasMemo)>0)
                @foreach ($plantillasMemo as $indice => $tramite)
                  <tr>
                    <td>@if($tramite->nombre){{$tramite->nombre}}@endif</td>
                    <td class="eso">@if($tramite->tipo_doc)<b class="badge badge-info">{{$tramite->tipo_doc}}</b>@endif</td>
                    <td class="eso">@if($tramite->link)<a href="{{$tramite->link}}" target="_blank" download><i style="color:Green;" class="fa fa-file-text-o"></i></a>@endif</td>
                    <td class="eso">@if($tramite->link_blocqueada)<a href="{{$tramite->link_blocqueada}}" target="_blank" download><i style="color:DarkRed;" class="fa fa-file-text-o"></i></a>@endif</td>
                  </tr>
                @endforeach
              @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="tab-pane fade" id="oficios" role="tabpanel" aria-labelledby="oficios-tab"> 
      <div class="card center">           
          <div class="card-body p-0">
            <div class="table-responsive">
            <br><br>
              <table class="table m-0">
                <thead>
                <tr style="background-color:#0A132D;">
                <th style="color:White;">Plantilla Firmante</th>
                <th style="color:White;">Tipo Documento</th>
                <th style="color:White;">Descargar Plantilla</th>
                <th style="color:White;">Descargar Plantilla<br>Espacios Bloqueados</th>
                </tr>
                </thead>
                <tbody>
                @if(count($plantillasOficio)>0)
                  @foreach ($plantillasOficio as $indice => $tramite2)
                    <tr>
                      <td>@if($tramite2->nombre){{$tramite2->nombre}}@endif</td>
                      <td class="eso">@if($tramite2->tipo_doc)<b class="badge badge-danger">{{$tramite2->tipo_doc}}</b>@endif</td>
                      <td class="eso">@if($tramite2->link)<a href="{{$tramite2->link}}" target="_blank" download><i style="color:Green;" class="fa fa-file-text-o"></i></a>@endif</td>
                      <td class="eso">@if($tramite2->link_blocqueada)<a href="{{$tramite2->link_blocqueada}}" target="_blank" download><i style="color:DarkRed;" class="fa fa-file-text-o"></i></a>@endif</td>
                    </tr>
                  @endforeach
                @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </div>

    <div class="tab-pane fade" id="resoluciones" role="tabpanel" aria-labelledby="resoluciones-tab"> 
      <div class="card center">           
          <div class="card-body p-0">
            <div class="table-responsive">
            <br><br>
              <table class="table m-0">
                <thead>
                <tr style="background-color:#0A132D;">
                <th style="color:White;">Plantilla Firmante</th>
                <th style="color:White;">Tipo Documento</th>
                <th style="color:White;">Descargar Plantilla</th>
                <th style="color:White;">Descargar Plantilla<br>Espacios Bloqueados</th>
                </tr>
                </thead>
                <tbody>
                @if(count($plantillasResolucion)>0)
                  @foreach ($plantillasResolucion as $indice => $tramite3)
                    <tr>
                      <td>@if($tramite3->nombre){{$tramite3->nombre}}@endif</td>
                      <td class="eso">@if($tramite3->tipo_doc)<b class="badge badge-success">{{$tramite3->tipo_doc}}</b>@endif</td>
                      <td class="eso">@if($tramite3->link)<a href="{{$tramite3->link}}" target="_blank" download><i style="color:Green;" class="fa fa-file-text-o"></i></a>@endif</td>
                      <td class="eso">@if($tramite3->link_blocqueada)<a href="{{$tramite3->link_blocqueada}}" target="_blank" download><i style="color:DarkRed;" class="fa fa-file-text-o"></i></a>@endif</td>
                    </tr>
                  @endforeach
                @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </div>

  </div>

</div>  

  <div class="container">    
    <div class="card-body">
      <div class="box-body">
        <br>
          <div class="form-group">
            <div class="col-sm-12">
              <div class="text-center">
                {!! link_to_route('recursos_apoyo', 'Volver', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>  

  <br><br><br><br><br><br><br>
</div>
@endsection
@section('before-scripts-end')
@stop
