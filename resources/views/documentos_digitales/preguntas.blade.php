
@extends('layouts.master')
@section('page-header')
<br>
<style>
.fondo {
    background: url('{{ asset('img/fondo2.png')}}');
    background-position: center center;
    background-repeat: repeat;
    background-size: cover;
}

.fa2{
  font-size: 25px !important;
}

.containers {
  max-width: 1000px;
  margin: 0% auto;
  padding: 20px;
  background-color: #fff;
  border-radius: 10px;
  overflow: hidden;
  box-sizing: border-box;
  box-shadow: 0 10px 35px rgba(0, 0, 0, 0.4);
}


.btn-link{
  color:#006FB3;
}

.pt{
  text-align: justify;
}

.eso{
  margin:0px auto;
}

</style>

<div class="container-fluid fondo">  

  <div>
    <br><br><br><br>
      <h1 class="text-center"><b><i class="fa2 fa fa-question-circle"></i>  Preguntas Frecuentes</h1><br>
      <center><img src="{{ asset('img/linea_gob.PNG') }}" alt="linea"></center> 
  </div>

  <br><br><br>
  
  <div class="containers row">
       
  <div id="accordion" class="eso">
  <br>
  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
          <h5><b>¿Qué debe hacer si el/la Firmante no puede firmar un documento digital en STD?</b></h5>
        </button>
      </h5>
    </div>

    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body pt">
        <p>Como primer paso si no puede firmar un documento electrónico es corroborar el uso correcto de la plantilla respetando los espacios, encabezado y pie de página en la plantilla. 
        Si lo anterior es corregido o estaba bien, el segundo paso es verificar haber guardado correctamente el archivo <a href="https://documentos-digitales-std.s3.us-west-2.amazonaws.com/Manuales/Guardar+Plantilla+como+Word+y+PDF2.pdf"  target=" "><b>(LINK MANUAL)</b></a>. 
        En caso de que las dos opciones anteriores no corrijan el problema, deberá llamar al Área de Informática parar revisar si su Certificado de Firma ha vencido. 
        Si lo anterior esta correcto puede ocurrir que los servidores de FirmaGob estén caídos, 
        por lo que el Área de Informática contactara a Gobierno Digital.</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          <h5><b>¿Qué debe hacer si se equivocó en la plantilla cargada a STD?</b></h5>
        </button>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body pt">
        <p>Debe Corregir el documento y luego cargarlo a STD sustituyendo el que esta errado en el sistema y para realizara los siguientes pasos:</p>
          
        <ul><li>Debe dirigirse hasta el menú a STD Interno.</li></ul>
        <ul><li>Luego ir al submenu del tipo de documento correspondiente.</li></ul>
        <ul><li>Despues debe ubicar el registro del documento que debe corregir.</li></ul>
        <ul><li>Luego debe presionar el botón de detalles del registro.</li></ul>
        <ul><li>En la ventana de detalles debe presionar el botón amarillo que dice “Botón para corregir documento cargado”.</li></ul>
        <ul><li>En la siguiente vista habrá una previsualización del documento que desea cambiar y abajo tendrá un formulario para cargar el nuevo archivo corregido.</li></ul>
        <ul><li>Siguiente debe presionar el botón “Enviar”, con esto el documento quedará cargado y listo para ser firmado.</li></ul>
          
        <p>Para visualizarlo de mejor manera descargue el Manual <a href="https://documentos-digitales-std.s3.us-west-2.amazonaws.com/Manuales/Bot%C3%B3n+para+Corregir+Documento+Cargado.pdf"  target=" "><b>(LINK MANUAL)</b></a></p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          <h5><b>¿Cómo obtiene el Folio y la Firma Avanzada el Documento Digital en STD?</b></h5>
        </button>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body pt">
        <p>Luego de ser cargado el documento al sistema, el firmante ingresará usando su clave única, donde firmará el documento, 
        una vez hecho esto el sistema automáticamente le asignará folio y la firma avanzada.</p>
      </div>
    </div>
  </div>
  
  <div class="card">
    <div class="card-header" id="headingCuatro2">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseCuatro2" aria-expanded="false" aria-controls="collapseCuatro2">
          <h5><b>¿Qué significa plantilla bloqueda o plantilla con espacios bloqueados?</b></h5>
        </button>
      </h5>
    </div>

    <div id="collapseCuatro2" class="collapse" aria-labelledby="headingCuatro2" data-parent="#accordion">
      <div class="card-body pt">
        <p>Es una plantilla para los documentos digitales que no permite hacer variaciones en los campos que interfieran con el sistema de firmado, 
        el uso de estas plantillas reduce la posibilidad de errores. Aunque no es obligatorio el uso de estas plantillas, es recomendado.</p>
        
        <p style="color:#006FB3">NOTA: Estas plantillas tienen un sombreado amarillo que indica los lugares en donde los/las funcionarios/as podrán editar el documento,
        en el momento en que este documento sea guardo como PDF, desaparecerá el sombreado amarillo automáticamente.</p>
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header" id="headingCuatro">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseCuatro" aria-expanded="false" aria-controls="collapseCuatro">
          <h5><b>¿Cómo se deben llenar las plantillas de los Documentos digitales?</b></h5>
        </button>
      </h5>
    </div>

    <div id="collapseCuatro" class="collapse" aria-labelledby="headingCuatro" data-parent="#accordion">
      <div class="card-body pt">
        <p>Debe mantener los espacios en blanco, el encabezado, el pie de página y márgenes de la misma. Evite no cambiar el tipo de letra del documento.</p>

        <p style="color:#006FB3">En caso de usar los documentos bloqueados estos archivos tienen un sombreado amarillo en las áreas que se deben rellenar, 
        con esto podrá cambiar el contenido y el archivo no le permitirá hacer variaciones en los espacios que el sistema usará para 
        foliar y agregar la firma electrónica avanzada.</p>
      </div>
    </div>
  </div>
  
  <div class="card">
    <div class="card-header" id="headingCinco">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseCinco" aria-expanded="false" aria-controls="collapseCinco">
          <h5><b>¿Cómo se distribuyen los Memorándums Electrónicos?</b></h5>
        </button>
      </h5>
    </div>

    <div id="collapseCinco" class="collapse" aria-labelledby="headingCinco" data-parent="#accordion">
      <div class="card-body pt">
        <p>El sistema distribuirá al <i>destinatario</i> del Memorándum Electrónico de manera automática el Documento.<br>
        En caso de necesitar distribuir el archivo a más personas, deberá distribuirlo a través de Outlook.</p>
        
        <p style="color:#006FB3">NOTA: Los Oficios Electrónicos y las Resoluciones Exentas Electrónicas serán distribuidos por el Área de Gestión Documental.</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingSeis">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseSeis" aria-expanded="false" aria-controls="collapseSeis">
          <h5><b>Características de los Documentos Digitales</b></h5>
        </button>
      </h5>
    </div>

    <div id="collapseSeis" class="collapse" aria-labelledby="headingSeis" data-parent="#accordion">
      <div class="card-body pt">
        <p>Si el documento no es necesario mantenerlo reservado seleccione <i>“Sin Categoría Especifica”</i>. 
        Cuando el archivo debe ser mantenido reservado, deberá seleccionar una categoría dependiendo del tipo de trámite. 
        Cualquier categoría que no sea <i>“Sin Categoría Especifica”</i> no permitirá la visualización del documento por otros/as funcionarios/as.</p>
      </div>
    </div>
  </div>
</div>
      
    <div class="container">    
      <div class="card-body">
        <div class="box-body">
          <br>
            <div class="form-group">
              <div class="col-sm-12">
                <div class="text-center">
                  {!! link_to_route('recursos_apoyo', 'Volver', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>  

    <br><br><br>

  
  <br><br>
</div>
@endsection
@section('before-scripts-end')
@stop
