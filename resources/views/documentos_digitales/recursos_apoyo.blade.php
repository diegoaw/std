
@extends('layouts.master')
@section('page-header')
<br>
<style>
.fondo {
    background: url('{{ asset('img/fondo_mindep_blanco.png')}}');
    background-position: center center;
    background-repeat: repeat;
    background-size: cover;
}

.loginBtn {
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 2.2rem 2.2rem;
}

.fa1{
  font-size: 28px !important;
}

</style>
<div class="container-fluid fondo">
  <div>
    <div>
      <div>
        <br><br><br>
        <h1 class="text-center"><b><i class="fa fa-info-circle"></i> Recursos De Apoyo</h1><br>
        <center><img src="{{ asset('img/linea_gob.PNG') }}" alt="linea"></center> 
      </div>
    </div>
     <br><br><br><br><br><br>
    
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
        <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <a type="button" class="btn btn-warning btn-lg loginBtn" href="{{ route('plantillas_generales') }}"><i class="fa1 fa fa-text-height"></i></a>


              <div class="info-box-content">
              <br>
                <span class="info-box-text">Plantillas sin</span>
                <span class="info-box-text">Personalizar</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <a type="button" class="btn btn-success btn-lg loginBtn" href="{{ route('plantillas_personalizadas') }}"><i class="fa1 fa fa-text-width"></i></a>


              <div class="info-box-content">
              <br>
                <span class="info-box-text">Plantillas Personalizadas</span>
                <span class="info-box-text">por Firmantes</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box">
              <a type="button" class="btn btn-info btn-lg loginBtn" href="{{ route('manuales') }}"><i class="fa1 fa fa-book"></i></a>

              <div class="info-box-content">
                <br>
                <span class="info-box-text">Manuales</span>
                <span class="info-box-text">de Apoyo</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <a type="button" class="btn btn-danger btn-lg loginBtn" href="{{ route('preguntas') }}"><i class="fa1 fa fa-question-circle"></i></a>

              <div class="info-box-content">
              <br>
                <span class="info-box-text">Preguntas</span>
                <span class="info-box-text">Frecuentes</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          
        </div>
        <!-- /.row -->

        <center>
        <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <a type="button" class="btn btn-light btn-lg loginBtn" href="{{ route('galeria_firmas') }}"><img src="{{ asset('img/signature.png') }}" alt="firma" style=" color:#ffffff " width="40" height="40"></a>

              <div class="info-box-content">
              <br>
                <span style="text-align:left;" class="info-box-text">Galería</span>
                <span style="text-align:left;" class="info-box-text">Firmas</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          </center>

      </div><!--/. container-fluid -->
    </section>

  <br><br><br><br><br><br><br><br>

  </div>
</div>
@endsection
@section('before-scripts-end')
@stop
