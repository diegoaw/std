@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-file-text-ot"></i> Documentos Digitales - Registro de Documentos Digitales</h1><br> 
    </div>    
  </div>
</div>

<style>
  .form-control-custom {
    display: block;
    width: 100%;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    color: #495057;
    background-color: #edd8eb;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    box-shadow: inset 0 0 0 transparent;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
  }
</style>

@endsection
@section('after-styles-end')
@endsection
@section('content')
{!! Form::open(['route' => 'registro_nuevo', 'files' => true, 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','id'=>'form-create']) !!}
<link rel="stylesheet" href="{{asset('css/chosen/chosen.css')}}">
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-danger card-outline">
        <h3 class="card-title">Crear Registro de Documento Digital</h3>
      </div>
      <div class="card-body">

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>

        <div class="callout col-sm-8 offset-sm-2" style="color: #737373;">
          <h4 style="font-size: 14px;"><i class="fa fa-info-circle" style="color: #ff0000;"></i> Nota:</h4>
          <span>Los campos marcados con un asterisco (*) son obligatorios.</span>
        </div><br>

        <div class="form-group row">
          <label for="nombre" class="col-sm-3 col-form-label">Nombre Usuario que Registra: </label>
            <div class="col-sm-6">
              <input type="text" name="nombre" class="form-control" readonly="readonly" id="nombre" placeholder="{{ Auth::user()->name }}">
            </div>
        </div>

        
        <div class="form-group row">
          <label for="email" class="col-sm-3 col-form-label">Email Usuario que Registra: </label>
            <div class="col-sm-6">
              <input type="text" name="nombre" class="form-control" readonly="readonly" id="email" placeholder="{{ Auth::user()->email }}">
            </div>
        </div>

        <div class="form-group row">
           {!! Form::label('Tipo Documento', 'Tipo de Documento(*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
          {!! Form::select('id_tipos_documentos_digitales',$selectTipo, $idTipoDcoumento, ['id'=>'id_tipos_documentos_digitales','name'=>'id_tipos_documentos_digitales', 'class'=>'form-control', 'readonly'=>'readonly']) !!}
          </div>
        </div>

        
        
        @if (auth()->user()->hasPermissionNotAdmin(['15-16']))
        <div style="color:#800850; border:3px solid #a30d67; border-radius:10px; text-align:center">
        <div class="form-group row">
          {!! Form::label('fR','¿Desea Utilizar un Folio Apartado? (*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            <label class="radio-inline">
                {{ Form::radio('fR', '1')}} Si
            </label>
            <label class="radio-inline">
                {{ Form::radio('fR', '0', 'active')}} No
            </label>
          </div>
        </div>

        <div class="form-group row" id="foliosR" style="display:none;">
          {!! Form::label('foliosR','Folios Apartados (*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
            {!! Form::select('foliosR',$selectFoliosR,null,['id'=>'foliosR','name'=>'foliosR', 'class'=>'form-control-custom' ]) !!}
            </div>
        </div>
        </div>
        @endif
        




        <br>

        <div class="form-group row">
          {!! Form::label('lugar','Lugar (*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
            {!! Form::select('lugar',$selectLugar,null,['id'=>'lugar','name'=>'lugar', 'class'=>'form-control' ]) !!}
            </div>
        </div>

        <div class="form-group row">
           {!! Form::label('Categoria', 'Categoría del Documento(*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
          {!! Form::select('id_categoria',$categoriaDoc, 1, ['id'=>'id_categoria','name'=>'id_categoria', 'class'=>'form-control']) !!}
          </div>
        </div>
        
        @if($idTipoDcoumento != 16)
        <div class="form-group row">
          {!! Form::label('reservado_distribucion','¿Desea que el Documento sea Distribuido Después de Ser Firmado? (*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6" style="text-align:center">
            <label class="radio-inline">
                {{ Form::radio('reservado_distribucion', '1', 'active')}} Si
            </label>
            <label class="radio-inline">
                {{ Form::radio('reservado_distribucion', '0')}} No
            </label>
          </div>
        </div>
        @endif
        
        <div class="form-group row" style="text-align:center">
          {!! Form::label('memo','¿Este documento tiene un número de Memo asociado? (*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            <label class="radio-inline">
                {{ Form::radio('memo', '1')}} Si
            </label>
            <label class="radio-inline">
                {{ Form::radio('memo', '0', 'active')}} No
            </label>
          </div>
        </div>     

        <div class="form-group row" id="n_memo" style="display:none;">
          {!! Form::label('n_memo', 'N° de Memo', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('n_memo', null, ['class' => 'form-control', 'placeholder' => 'Ingrese N° de Memo...']) !!}
          </div>
        </div><br>
    
        <div class="form-group row">
          {!! Form::label('titulo', 'Materia(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('titulo', null, ['class' => 'form-control', 'placeholder' => 'Ingrese la Materia del Documento, Copie y Pegue, servirá para poder buscarla más tarde.']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('descripcion', 'Observaciones Internas', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'placeholder' =>'Estas no forman parte del Documento, es solo para comunicaciones internas con el firmante...']) !!}
          </div>
        </div>   
        
        <div class="field_wrapper">
        <div class="form-group row col-md-12">
          {!! Form::label('filename', 'Cargar PDF de Documento Digital listo(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-md-6">
          {!! Form::file('filename', ['class' => 'form-control']) !!}
          </div>
        </div>
        </div>    
        <br>
        
        <!-- <div class="form-group row" style="text-align:center">
          {!! Form::label('sub','¿Va a Firmar Como Subrogante?', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            <label class="radio-inline">
                {{ Form::radio('sub', '1')}} Si
            </label>
            <label class="radio-inline">
                {{ Form::radio('sub', '0', 'active')}} No
            </label>
          </div>
        </div> -->

        <div class="form-group row" id="firma" style="display:flex;">
           {!! Form::label('firmantes', 'Enviar para Firma de(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('id_user_firmante',$selectFirmantes, $varNull, ['id'=>'id_user_firmante','name'=>'id_user_firmante', 'class'=>'form-control']) !!}
          </div>
        </div>  

        @if($idTipoDcoumento == 16)

        <div class="form-group row" id="firma" style="display:flex;">
           {!! Form::label('destinatarios', 'Seleccione Destinatario del Memorándum(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('destinatario_memo',$selectDestinatariosM, $varNull, ['id'=>'destinatario_memo','name'=>'destinatario_memo', 'class'=>'form-control']) !!}

            <p style="color:Red;">* Seleccione el Destinatario del Memorándum Electrónico para que reciba el Documento luego de ser Firmado de forma automática.</p>
          </div>
        </div>

        <div class="form-group row" id="distribuye" style="text-align:center;">
          {!! Form::label('distribuye','¿Desea distribuir el Memorándum Electrónico a otros Funcionarios, otras Divisiones o Áreas?', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            <label class="radio-inline">
                {{ Form::radio('distribuye', '1')}} Si
            </label>
            <label class="radio-inline">
                {{ Form::radio('distribuye', '0', 'active')}} No
            </label>
          </div>
        </div>

        <div class="form-group row" id="correos" style="display:none; text-align:center;">
          <label for="correos_internos" class="col-sm-3 col-form-label">Seleccione Funcionarios, Divisiones o Áreas</label>
          <div class="col-sm-6" style="text-align:left;" >
          {{ Form::select('correos_internos[]', $selectUsuarios, null, ['class' => 'chosen-select', 'multiple','col-sm-10', 'data-placeholder' => 'Seleccione uno o más destinatarios internos','style'=>'width:nowrap; !important']) }}
          
          <p style="color:Red;">* Enviar solo hasta 8 destinatarios.</p>
          </div>
        </div>

        @endif
        
        <!--<div class="form-group row" id="subrogante" style="display:none;">
           {!! Form::label('firmantesS', 'Enviar para Firma de Subrogante de(*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
          {!! Form::select('id_user_firmante_s',$selectFirmantesS, $varNull, ['id'=>'id_user_firmante_s','name'=>'id_user_firmante_s', 'class'=>'form-control']) !!}
          </div>
        </div>  

         <div class="form-group row">
          {!! Form::label('derivar','¿Desea Derivar a otro Funcionario?', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            <label class="radio-inline">
                {{ Form::radio('derivar', '1')}} Si
            </label>
            <label class="radio-inline">
                {{ Form::radio('derivar', '0')}} No
            </label>
          </div>
        </div>

        <div class="field_wrapper">
        <div class="form-group row col-md-12" id="documentoW" style="display:none;">
          {!! Form::label('filename2', 'Cargar Word editable del Documento Digital(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-md-6">
          {!! Form::file('filename2', ['class' => 'form-control']) !!}
          </div>
        </div>
        </div>

        <div class="form-group row" id="observacion" style="display:none;">
          {!! Form::label('observacion', 'Observación', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('observacion', null, ['class' => 'form-control', 'placeholder' =>'Escriba una breve observacion...']) !!}
          </div>
        </div>

        <div class="user_wrapper">
        <div class="form-group row" id="destinatario" style="display:none;">
           {!! Form::label('usuario destinatario', 'Seleccione Destinatario(s)(*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
          {!! Form::select('id_user_destino[]',$selectUsuariosid, $varNull, ['id'=>'id_user_destino[]','name'=>'id_user_destino[]', 'class'=>'form-control']) !!}
          <a href="javascript:void(0);" class="add_button" title="Add signers"><i class="fa fa fa-plus"></i></a>
          </div>
        </div>
        </div> -->
        
        {{  Form::hidden('url',URL::previous())  }}
        
        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! Form::submit('Enviar', ['class' => 'btn btn-success btn-sm','title'=>'Enviar']) !!}
              <a href="{{ url()->previous() }}" class="btn btn-danger">Volver</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{!! Form::close() !!}

@stop
@section('after-scripts-end')
<script type="text/javascript">
$(document).ready(function(){
    var maxUser = 10; //Input users increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.user_wrapper'); //Input user wrapper
    var userHTML = '<div class="form-group row col-md-12"><div class="col-md-3"></div><div class="col-md-6">{!! Form::select("id_user_destino[]",$selectUsuariosid, $varNull, ["id"=>"id_user_destino[]","name"=>"id_user_destino[]", "class"=>"form-control"]) !!}<a href="javascript:void(0);" class="remove_button" title="Remove signers"><i class="fa fa fa-minus"></i></a></div></div>'; //New input user html
    var x = 1; //Initial user counter is 1
    $(addButton).click(function(){ //Once add button is clicked
        if(x < maxUser){ //Check maximum number of input users
            x++; //Increment user counter
            $(wrapper).append(userHTML); // Add user html
        }
    });
    $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove user html
        x--; //Decrement user counter
    });
});
</script>
<script>
$(document).ready(function(){
  $('input[type=radio][name=derivar]').on('change', function(){
    let derivar = this.value;
    if(derivar == 1){
      $("#destinatario").css("display", "flex");
      $("#documentoW").css("display", "flex");
      $("#observacion").css("display", "flex");
    }
    if(derivar == 0){
      $("#destinatario").css("display", "none");
      $("#documentoW").css("display", "none");
      $("#observacion").css("display", "none");
    }
  });
});
</script>
<script>
$(document).ready(function(){
  $('input[type=radio][name=fR]').on('change', function(){
    let fR = this.value;
    if(fR == 1){
      $("#foliosR").css("display", "flex");     
    }
    if(fR == 0){
      $("#foliosR").css("display", "none");
    }
  });
});
</script>

<script>
$(document).ready(function(){
  $('#id_categoria').on('change', function(){
    let id_categoria = this.value;
    console.log(id_categoria)
    if(id_categoria != 1){
      $("#reservadoDistribucion").css("display", "flex");     
    }
    if(id_categoria == 1){
      $("#reservadoDistribucion").css("display", "none");
    }
    console.log('si')
  });
});
</script>
<script>
$(document).ready(function(){
  $('input[type=radio][name=memo]').on('change', function(){
    let memo = this.value;
    if(memo == 1){
      $("#n_memo").css("display", "flex");     
    }
    if(memo == 0){
      $("#n_memo").css("display", "none");
    }
  });
});
</script>
<!-- <script>
$(document).ready(function(){
  $('input[type=radio][name=sub]').on('change', function(){
    let sub = this.value;
    if(sub == 1){
      $("#firma").css("display", "none");
      $("#subrogante").css("display", "flex");
    }
    if(sub == 0){
      $("#firma").css("display", "flex");
      $("#subrogante").css("display", "none");
    }
  });
});
</script> -->
<script type="text/javascript" src="{{asset('js/chosen/chosen.jquery.js')}}"></script>
<script type="text/javascript">
    $(".chosen-select").chosen({
      width: "100%",
      max_selected_options: 9,
      no_results_text: "No se encontraron resultados para "
    });
  </script>
  <script>
$(document).ready(function(){
  $('input[type=radio][name=distribuye]').on('change', function(){
    let distribuye = this.value;
    if(distribuye == 1){
      $("#correos").css("display", "flex");
    }
    if(distribuye == 0){
      $("#correos").css("display", "none");
    }
  });
});
</script>
@endsection
