@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-5">
      <h1><i class="fa fa-exchange"></i><b> Derivaciones - Registros de Documentos Digitales</b></h1>
    </div><br>
    <div class="col-sm-7">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-exchang"></i> Derivaciones</li>
        <li class="breadcrumb-item active">Detalles</li>
      </ol>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
@if(count($derivaciones)>0)
@foreach ($derivaciones as $indice => $tramite)
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title"><b> Derivacion N° {{$indice+1}}</b></h5>
      </div>
      
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">

            <table id="example1" class="table table-striped">
                <tr>
                    <td style="width:450px"><b>Fecha de la Derivación:</b></td>
                    <td >{{$tramite->created_at}}</td>
                </tr>
                <tr>
                    <td style="width:450px"><b>Tipo de Documento Registrado:</b></td>
                    <td >{{$tramite->tipo->tiposdocumentosdigitales->nombre}}</td>
                </tr>
                <tr>
                    <td style="width:450px"><b>Categoría del Documento Registrado:</b></td>
                    <td >{{$tramite->tipo->categoriadocumentosdigitales->nombre}}</td>
                </tr>
                <tr>
                    <td style="width:450px"><b>Usuario que Registro el documento:</b></td>
                    <td >{{$tramite->usergenerador->name}}</td>
                </tr>
                <tr>
                    <td style="width:450px"><b>Estado de la Derivación:</b></td>
                    @if($tramite->estatus->nombre == 'Completada') 
                    <td style="color:DarkBlue;">
                        Completada
                    </td>
                    @endif
                    @if($tramite->estatus->nombre == 'Pendiente')
                    <td style="color:DarkRed;">
                        Pendiente
                    </td>
                    @endif
                </tr>
                <tr>
                    <td style="width:450px"><b>Usario Remitende de la Derivación:</b></td>
                    <td >{{$tramite->useremisor->name}}</td>
                </tr>
                <tr>
                    <td style="width:450px"><b>Usario Destinatario de la Derivación:</b></td>
                    <td >{{$tramite->userdestino->name}}</td>
                </tr>
                <tr>
                    <td style="width:450px"><b>Observación de la Derivación:</b></td>
                    <td >{{$tramite->observacion}}</td>
                </tr>
                <tr>
                    <td style="width:450px"><b>Documento Word de la Derivación:</b></td>
                    <td ><a type="button" class="btn accion btn-success accion" href="{{($tramite->anexo->url_s3)}}" target="_blank" download><i class="fa fa-download"></i></a></td>
                </tr>
            </table>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endforeach
@endif
<div class="form-group">
    <div class="col-sm-12">
        <div class="text-center">
        <a href="{{ url()->previous() }}" class="btn btn-danger">Volver</a>
        </div>
    </div>
</div>
@endsection
@section('before-scripts-end')
@stop