
@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-file-text-o"></i> Registros de Documentos Digitales - Log IP´s</h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')


<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title"> Log IP´s - Registro de Documentos Digitales</h5>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">



          </div>
        </div>
      </div>
    </div>
  </div>
</div>



<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">Log IP´s</h5>

          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm">
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th nowrap style="color:white"><center>ID Registro</center></th>
                        <th nowrap style="color:white"><center>ID Usuario</center></th>
                        <th nowrap style="color:white"><center>Correlativo/Firma</center></th>
                        <th nowrap style="color:white"><center>IP</center></th>
                        <th nowrap style="color:white"><center>Fecha</center></th>

                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($log)>0)
                  @foreach ($log as $indice => $tramite)
                    <tr>
                        <td nowrap><center>{{$tramite->auditable_id}}</center></td>
                        <td nowrap><center>{{$tramite->user->name}}</center></td>
                        @if(property_exists(json_decode($tramite->new_values), 'correlativo'))
                        <td ><center>Asignacion de correlativo N° {{json_decode($tramite->new_values)->correlativo}}</center></td>
                        @endif
                        @if(property_exists(json_decode($tramite->new_values), 'estatus'))
                        <td ><center> Firma de documento </center></td>
                        @endif
                        <td nowrap><center>{{$tramite->ip_address}}</center></td> 
                        <td nowrap><center>{{$tramite->updated_at}}</center></td> 
                    </tr>  
                  @endforeach
                @endif
                </tbody>
              </table>
          </div>
          
      </div>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-12">
        <div class="text-center">
        <a href="{{ url()->previous() }}" class="btn btn-danger">Volver</a>
        </div>
    </div>
</div>

@endsection
@section('before-scripts-end')
@stop