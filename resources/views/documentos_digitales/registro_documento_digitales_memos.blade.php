
@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-7">
      <div class="col-sm-11">
        <h1><b><i class="fa fa-file-text-o"></i> Registros de Documentos Digitales - </b><b style="color:DarkRed;">Memorándums Electrónicos</b></h1><br>
      </div>
    </div>
      <div class="col-sm-5">
        <div class="alert alert-primary" role="alert">
          <h5 class="alert-heading"><b style="color:#FF5747;">¡Recuerde!</b></h5>
          <p>1- Los Memorándums Electrónicos <b style="color:#FF5747;">no son distribuidos por el Área de Gestión de Documentos</b>, 
          por lo cual luego de ser Firmado este deberá ser derivado por el(la) generador(a) del documento o el(la) Firmante. 
          Esta acción podrá realizarla por medio de su correo Outlook.</p>
          <p class="mb-0">2- Descargue la plantilla de Memorándum y <b style="color:#FF5747;">respete los espacios en blanco y márgenes de la misma, por favor 
          intente no cambiar el tipo de letra del documento.</b></p>
        </div>
      </div>
  </div>
  
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<style>

</style>

<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title"> Registro de Documentos Digitales</h5>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">

          {!! Form::open(['route' => 'registro_memos', 'method' => 'get' , 'class'=>"form-horizontal"]) !!}
                  <div class="form-row">
                    <div class="col-sm-0">
                      <label for="tramite" class=" col-form-label">ID Trámite: </label>
                      <input type="number" class="form-control" id="tramite" name="tramite" value="{{$tramite}}" placeholder="N° Trámite">
                    </div>

                    <div class="col-sm-2">
                      <label for="correlativo" class=" col-form-label">Correlativo/Folio:</label>
                      <input type="number" class="form-control" id="correlativo" name="correlativo"  value="{{$correlativo}}" placeholder="Correlativo">
                    </div>

                    <div class="form-group   {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                    <div class="input-group date">
                      <div class="col">
                          <label for="FechaDesde" class=" col-form-label">Fecha Desde: </label>
                          <input type="text" class="form-control group-date" id="FechaDesde" name="FechaDesde" readonly="readonly" value="{!! date('Y-m-d', strtotime($feDesde))  !!}">
                      </div>
                    </div>
                    </div>

                    <div class="form-group   {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                    <div class="input-group date">
                      <div class="col">
                          <label for="FechaHasta" class=" col-form-label">Fecha Hasta: </label>
                          <input type="text" class="form-control group-date" id="FechaHasta" name="FechaHasta" readonly="readonly" value="{!! date('Y-m-d', strtotime($feHasta ))  !!}">
                      </div>
                    </div>
                    </div>

                    <div class="col-sm-3">
                      <label for="etapa" class=" col-form-label">Etapa Registro: </label>
                      {!! Form::select('etapa',$etapas, $etapa,['id'=>'etapa','name'=>'etapa', 'class'=>'form-control']) !!}
                    </div>

                    <div class="col-sm-3">
                      <label for="categoria" class=" col-form-label">Categoría Registro: </label>
                      {!! Form::select('categoria',$categorias, $categoria,['id'=>'categoria','name'=>'categoria', 'class'=>'form-control']) !!}
                    </div>

                    <div class="col-sm-3">
                      <label for="user" class=" col-form-label">Cargado por: </label>
                      {!! Form::select('user',$users, $user,['id'=>'user','name'=>'user', 'class'=>'form-control']) !!}
                    </div>

                    <div class="col-sm-3">
                      <label for="user" class=" col-form-label">Lugar: </label>
                      {!! Form::select('lugar',$lugares, $lugar,['id'=>'lugar','name'=>'lugar', 'class'=>'form-control']) !!}
                    </div>

                    <div class="col-sm-2">
                      <label for="materia" class=" col-form-label">Materia Registro:</label>
                      <textarea class="form-control" id="materia" name="materia" placeholder="Materia" >{{$materia}}</textarea>
                    </div>

                    <div class="col-sm-2">
                    <button type="submit" class="btn btn-primary pull-right">Buscar</button>
                    </div>

                    
                  </div>
                  <div class="form-group">
                  </div>
                  
             
              {!! Form::close() !!}

             

                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="text-center">
                      
                      {!! link_to_route('registro_crear', 'Gestionar Nuevo Memo', [16], ['class' => 'btn btn-success btn-sm','title'=>'Gestionar Nuevo Memo']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="text-center">
                      <a type="button" class="btn accion btn-danger accion" href="https://mindep.s3.amazonaws.com/plantillas/Plantilla%2BMemorandums2.docx" target="_blank" download>Plantilla Memorándum</a>
                      </div>
                    </div>
                  </div>
                  @if (auth()->user()->hasPermission(['15-14']))
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="text-center">
                      {!! link_to_route('registro_log', 'Logs Registros', [], ['class' => 'btn btn-warning btn-sm','title'=>'Logs Registros']) !!}
                      </div>
                    </div>
                  </div>
                  @endif

                  

          </div>
        </div>
      </div>
    </div>
  </div>
</div>



<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title"><b style="color:DarkRed;">Historial de Memorándums Electrónicos</b></h5>

          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm">
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th nowrap style="color:white"><center>ID</center></th>
                        <th nowrap style="color:white"><center>Fecha Actualización</center></th>
                        <th nowrap style="color:white"><center>Correlativo/Folio</center></th>
                        <th style="color:white"><center>Tipo</center></th>
                        <th style="color:white"><center>Lugar</center></th>                       
                        <th nowrap style="color:white"><center>Cargado Por</center></th>
                        <th style="color:white"><center>Etapa</center></th>
                        <th nowrap style="color:white"><center>Categoría</center></th>
                        <th style="color:white">Materia</th>
                        <th nowrap style="color:white"><center>Detalles</center> </th>
                        <th nowrap style="color:white"><center>Firmado</center></th>
                        <!-- <th nowrap style="color:white"><center>Distribución</center></th>
                        <th nowrap style="color:white"><center>Derivaciones Documento</center></th> -->
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($registrosRservados)>0)
                  @foreach ($registrosRservados as $indice => $tramite)
                    <tr>
                        <td nowrap><center>{{$tramite->id}}</center></td>
                        <td nowrap><center>{!! date('d-m-y', strtotime($tramite->updated_at)) !!}</center></td>
                        <td nowrap><center>{{$tramite->correlativo}}</center></td>
                        <td><center>{{$tramite->tiposdocumentosdigitales->nombre}}</center></td>
                        <td><center>{{$tramite->lugar}}</center></td>
                        <td nowrap><center>{{$tramite->iduser->name}}</center></td>
                        <td ><center>{{$tramite->etapadocumentosdigitales->nombre}}</center></td>   
                        
                        @switch($tramite->categoriadocumentosdigitales->descripcion) 
                          @case('sumarios')
                            @if(auth()->user()->hasPermissionNotAdmin(['15-01'])) 
                              <td nowrap><center> 
                                {{$tramite->categoriadocumentosdigitales->nombre}} 
                              </center></td>    
                              <td>
                                {{$tramite->titulo}}
                                </td>
                                <td nowrap><center>
                                <a type="button" class="btn btn-info" data-toggle="modal" title="Ver detalles" data-target="#modal-xl{{$tramite->id}}"><i class="fa fa-file-text-o"></i></a>
                                </center></td>
                                <!-- <td nowrap><center>
                                @if($tramite->derivacion == 1)
                                <a type="button" class="btn btn-warning" title="Ver derivaciones" href="{{route('registro_detalles_derivaciones_oficios',$tramite->id)}}"><i class="fa fa-exchange"></i></a>
                                @endif
                                @if($tramite->derivacion != 1)
                                Sin Derivaciones
                                @endif
                              </center></td> -->
                            @else
                              <td nowrap style="background-color: DarkRed; color:white"><center> 
                                Reservado
                              </center></td>    
                              <td nowrap></td>
                              <td nowrap><center></center></td>           
                            @endif  
                          @break

                          @case('reclamos')
                          @if(auth()->user()->hasPermissionNotAdmin(['15-02'])) 
                              <td nowrap><center> 
                                {{$tramite->categoriadocumentosdigitales->nombre}} 
                              </center></td>    
                              <td >
                                {{$tramite->titulo}}
                                </td>
                                <td nowrap><center>
                                <a type="button" class="btn btn-info" data-toggle="modal" title="Ver detalles" data-target="#modal-xl{{$tramite->id}}"><i class="fa fa-file-text-o"></i></a>
                                </center></td>
                                <!-- <td nowrap><center>
                                @if($tramite->derivacion == 1)
                                <a type="button" class="btn btn-warning" title="Ver derivaciones" href="{{route('registro_detalles_derivaciones_oficios',$tramite->id)}}"><i class="fa fa-exchange"></i></a>
                                @endif
                                @if($tramite->derivacion != 1)
                                Sin Derivaciones
                                @endif
                              </center></td> -->
                            @else
                              <td nowrap style="background-color: DarkRed; color:white"><center> 
                                Reservado
                              </center></td>    
                              <td nowrap></td>
                              <td nowrap><center></center></td>             
                            @endif
                          @break

                          @case('demeritos')
                          @if(auth()->user()->hasPermissionNotAdmin(['15-03'])) 
                              <td nowrap><center> 
                                {{$tramite->categoriadocumentosdigitales->nombre}} 
                              </center></td>    
                              <td >
                                {{$tramite->titulo}}
                                </td>
                                <td nowrap><center>
                                <a type="button" class="btn btn-info" data-toggle="modal" title="Ver detalles" data-target="#modal-xl{{$tramite->id}}"><i class="fa fa-file-text-o"></i></a>
                                </center></td>
                                <!-- <td nowrap><center>
                                @if($tramite->derivacion == 1)
                                <a type="button" class="btn btn-warning" title="Ver derivaciones" href="{{route('registro_detalles_derivaciones_oficios',$tramite->id)}}"><i class="fa fa-exchange"></i></a>
                                @endif
                                @if($tramite->derivacion != 1)
                                Sin Derivaciones
                                @endif
                              </center></td> -->
                            @else
                              <td nowrap style="background-color: DarkRed; color:white"><center> 
                                Reservado
                              </center></td>    
                              <td nowrap></td>
                              <td nowrap><center></center></td>            
                            @endif
                          @break

                          @case('denuncias')
                          @if(auth()->user()->hasPermissionNotAdmin(['15-04'])) 
                              <td nowrap><center> 
                                {{$tramite->categoriadocumentosdigitales->nombre}} 
                              </center></td>    
                              <td >
                                {{$tramite->titulo}}
                                </td>
                                <td nowrap><center>
                                <a type="button" class="btn btn-info" data-toggle="modal" title="Ver detalles" data-target="#modal-xl{{$tramite->id}}"><i class="fa fa-file-text-o"></i></a>
                                </center></td>
                                <!-- <td nowrap><center>
                                @if($tramite->derivacion == 1)
                                <a type="button" class="btn btn-warning" title="Ver derivaciones" href="{{route('registro_detalles_derivaciones_oficios',$tramite->id)}}"><i class="fa fa-exchange"></i></a>
                                @endif
                                @if($tramite->derivacion != 1)
                                Sin Derivaciones
                                @endif
                              </center></td> -->
                            @else
                              <td nowrap style="background-color: DarkRed; color:white"><center> 
                                Reservado
                              </center></td>    
                              <td nowrap></td>
                              <td nowrap><center></center></td>          
                            @endif
                          @break

                          @case('sospechas')
                          @if(auth()->user()->hasPermissionNotAdmin(['15-05'])) 
                              <td nowrap><center> 
                                {{$tramite->categoriadocumentosdigitales->nombre}} 
                              </center></td>    
                              <td >
                                {{$tramite->titulo}}
                                </td>
                                <td nowrap><center>
                                <a type="button" class="btn btn-info" data-toggle="modal" title="Ver detalles" data-target="#modal-xl{{$tramite->id}}"><i class="fa fa-file-text-o"></i></a>
                                </center></td>
                                <!-- <td nowrap><center>
                                @if($tramite->derivacion == 1)
                                <a type="button" class="btn btn-warning" title="Ver derivaciones" href="{{route('registro_detalles_derivaciones_oficios',$tramite->id)}}"><i class="fa fa-exchange"></i></a>
                                @endif
                                @if($tramite->derivacion != 1)
                                Sin Derivaciones
                                @endif
                              </center></td> -->
                            @else
                              <td nowrap style="background-color: DarkRed; color:white"><center> 
                                Reservado
                              </center></td>    
                              <td nowrap></td>
                              <td nowrap><center></center></td>         
                            @endif
                          @break

                          @case('hallazgos')
                          @if(auth()->user()->hasPermissionNotAdmin(['15-06'])) 
                              <td nowrap><center> 
                                {{$tramite->categoriadocumentosdigitales->nombre}} 
                              </center></td>    
                              <td >
                                {{$tramite->titulo}}
                                </td>
                                <td nowrap><center>
                                <a type="button" class="btn btn-info" data-toggle="modal" title="Ver detalles" data-target="#modal-xl{{$tramite->id}}"><i class="fa fa-file-text-o"></i></a>
                                </center></td>
                                <!-- <td nowrap><center>
                                @if($tramite->derivacion == 1)
                                <a type="button" class="btn btn-warning" title="Ver derivaciones" href="{{route('registro_detalles_derivaciones_oficios',$tramite->id)}}"><i class="fa fa-exchange"></i></a>
                                @endif
                                @if($tramite->derivacion != 1)
                                Sin Derivaciones
                                @endif
                              </center></td> -->
                            @else
                              <td nowrap style="background-color: DarkRed; color:white"><center> 
                                Reservado
                              </center></td>    
                              <td nowrap></td>
                              <td nowrap><center></center></td>             
                            @endif
                          @break

                          @case('auditorias')
                          @if(auth()->user()->hasPermissionNotAdmin(['15-07'])) 
                              <td nowrap><center> 
                                {{$tramite->categoriadocumentosdigitales->nombre}} 
                              </center></td>    
                              <td >
                                {{$tramite->titulo}}
                                </td>
                                <td nowrap><center>
                                <a type="button" class="btn btn-info" data-toggle="modal" title="Ver detalles" data-target="#modal-xl{{$tramite->id}}"><i class="fa fa-file-text-o"></i></a>
                                </center></td>
                                <!-- <td nowrap><center>
                                @if($tramite->derivacion == 1)
                                <a type="button" class="btn btn-warning" title="Ver derivaciones" href="{{route('registro_detalles_derivaciones_oficios',$tramite->id)}}"><i class="fa fa-exchange"></i></a>
                                @endif
                                @if($tramite->derivacion != 1)
                                Sin Derivaciones
                                @endif
                              </center></td> -->
                            @else
                              <td nowrap style="background-color: DarkRed; color:white"><center> 
                                Reservado
                              </center></td>    
                              <td nowrap></td>
                              <td nowrap><center></center></td>          
                            @endif
                          @break

                          @case('riesgos')
                          @if(auth()->user()->hasPermissionNotAdmin(['15-08'])) 
                              <td nowrap><center> 
                                {{$tramite->categoriadocumentosdigitales->nombre}} 
                              </center></td>    
                              <td >
                                {{$tramite->titulo}}
                                </td>
                                <td nowrap><center>
                                <a type="button" class="btn btn-info" data-toggle="modal" title="Ver detalles" data-target="#modal-xl{{$tramite->id}}"><i class="fa fa-file-text-o"></i></a>
                                </center></td>
                                <!-- <td nowrap><center>
                                @if($tramite->derivacion == 1)
                                <a type="button" class="btn btn-warning" title="Ver derivaciones" href="{{route('registro_detalles_derivaciones_oficios',$tramite->id)}}"><i class="fa fa-exchange"></i></a>
                                @endif
                                @if($tramite->derivacion != 1)
                                Sin Derivaciones
                                @endif
                              </center></td> -->
                            @else
                              <td nowrap style="background-color: DarkRed; color:white"><center> 
                                Reservado
                              </center></td>    
                              <td nowrap></td>
                              <td nowrap><center></center></td>           
                            @endif
                          @break

                          @case('remuneraciones')
                          @if(auth()->user()->hasPermissionNotAdmin(['15-09'])) 
                              <td nowrap><center> 
                                {{$tramite->categoriadocumentosdigitales->nombre}} 
                              </center></td>    
                              <td >
                                {{$tramite->titulo}}
                                </td>
                                <td nowrap><center>
                                <a type="button" class="btn btn-info" data-toggle="modal" title="Ver detalles" data-target="#modal-xl{{$tramite->id}}"><i class="fa fa-file-text-o"></i></a>
                                </center></td>
                                <!-- <td nowrap><center>
                                @if($tramite->derivacion == 1)
                                <a type="button" class="btn btn-warning" title="Ver derivaciones" href="{{route('registro_detalles_derivaciones_oficios',$tramite->id)}}"><i class="fa fa-exchange"></i></a>
                                @endif
                                @if($tramite->derivacion != 1)
                                Sin Derivaciones
                                @endif
                              </center></td> -->
                            @else
                              <td nowrap style="background-color: DarkRed; color:white"><center> 
                                Reservado
                              </center></td>    
                              <td nowrap></td>
                              <td nowrap><center></center></td>        
                            @endif
                          @break
                          @case('todos')
                          @if(auth()->user()->hasPermissionNotAdmin(['15-10'])) 
                              <td nowrap><center> 
                                {{$tramite->categoriadocumentosdigitales->nombre}} 
                              </center></td>    
                              <td>
                                {{$tramite->titulo}}
                                </td>
                                <td nowrap><center>
                                <a type="button" class="btn btn-info" data-toggle="modal" title="Ver detalles" data-target="#modal-xl{{$tramite->id}}"><i class="fa fa-file-text-o"></i></a>
                                </center></td>
                                <!-- <td nowrap><center>
                                @if($tramite->derivacion == 1)
                                <a type="button" class="btn btn-warning" title="Ver derivaciones" href="{{route('registro_detalles_derivaciones_oficios',$tramite->id)}}"><i class="fa fa-exchange"></i></a>
                                @endif
                                @if($tramite->derivacion != 1)
                                Sin Derivaciones
                                @endif
                              </center></td> -->
                            @else
                              <td nowrap style="background-color: DarkRed; color:white"><center> 
                                Reservado
                              </center></td>    
                              <td nowrap></td>
                              <td nowrap><center></center></td>         
                            @endif
                          @break
                      @endswitch
                      <td> <center>
                      @if ($tramite->url_firmado == null)
                      <i style=" color: red;  " class="fa fa-times-circle-o" aria-hidden="true"></i>
                        <b style="color:Red;">NO</b>
                      @else
                      <i  style=" color: green;  " class="fa fa-check-circle-o" aria-hidden="true"></i>
                        <b style="color:green;">SI</b>
                      @endif
                      </center></td>
                      <!-- <td>@if($tramite->distribuido == 1)
                      <center><b style="color:DarkBlue;">DISTRIBUIDO</b></center>
                      @endif</td> -->
                      </tr>
                  @endforeach
                @endif
                </tbody>
              </table>
          </div>
          @foreach ($registrosRservados as $indice => $tramite)
          <div class="modal fade" id="modal-xl{{$tramite->id}}">
            <div class="modal-dialog modal-xl"  style="padding-top: 100px; padding-right: 850px;">
              <div class="modal-content" style="width: 1024px; height: 700px;">
                <div class="modal-header">
                  <h4 class="modal-title">Detalles de Memo Electrónico</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                <div class="row">
                @if($tramite->url_firmado != null)
                  <div class="col-md-6">
                @else
                <div class="col-md-12">
                @endif
                  <table id="example1" class="table table-striped">
                  <tr>
                      <td><b>Tipo de Documento</b></td>
                      <td>{{$tramite->tiposdocumentosdigitales->nombre}}</td>
                  </tr>
                  @if ($tramite->correlativo == null)
                  <tr>
                  <td><b>Correlativo</b></td>
                      <td style="color:DarkRed;">Pendiente por Correlativo</td>
                  </tr>
                  @endif
                  @if ($tramite->correlativo != null)
                  <tr>
                      <td><b>Correlativo</b></td>
                      <td>{{$tramite->correlativo}}</td>
                  </tr>
                  @endif
                  <tr>
                      <td><b>Categoría del Documento</b></td>
                      <td>{{$tramite->categoriadocumentosdigitales->nombre}}</td>
                  </tr>
                  @if ($tramite->n_memo != null)
                  <tr>
                      <td><b>N° Memo</b></td>
                      <td>{{$tramite->n_memo}}</td>
                  </tr>
                  @endif
                  @if ($tramite->n_memo == null)
                  <tr>
                      <td><b>N° Memo</b></td>
                      <td style="color:DarkRed;">No tiene relación con un Memo.</td>
                  </tr>
                  @endif
                  <tr>
                      <td><b>Materia</b></td>
                      <td>{{$tramite->titulo}}</td>
                  </tr>
                  @if ($tramite->descripcion != null)
                  <tr>
                      <td><b>Observaciones Internas</b></td>
                      <td>{{$tramite->descripcion}}</td>
                  </tr>
                  @endif
                  @if ($tramite->url_vista != null)
                  <tr>
                      <td><b>Documento Cargado Sin Firma Avanzada</b></td>
                      <td><a type="button" class="btn accion btn-danger accion" href="{{asset($tramite->url_vista)}}" target="_blank" download><i class="fa fa-download"></i></a></td>
                  </tr>
                  @endif
                  @if ($tramite->url_firmado == null && $tramite->id_user == Auth::user()->id)
                  <tr>
                      <td><b>Botón para Corregir Documento Cargado</b></td>
                      <td><a class="btn accion btn-warning accion" href="{{route('corregir_documento',$tramite->id)}}"><i class="fa fa-pencil"></i></a></td>
                  </tr>
                  @endif
                  @if ($tramite->url_firmado != null)
                  <tr>
                      <td><b>Documento con Firma Avanzada</b></td>
                      <td><a type="button" class="btn accion btn-success accion" href="{{asset($tramite->url_firmado)}}" target="_blank" download><i class="fa fa-download"></i></a></td>
                  </tr>
                  @endif
                  @if ($tramite->url_firmado == null)
                  <tr>
                      <td><b>Estado de la Firma</b></td>
                      <td style="color:DarkRed;">Firma Pendiente</td>
                  </tr>
                  @endif
                  </table>
                </div>
                
                @if($tramite->url_firmado != null)
                  <div class="col-sm-6">
                    <div class="text-center">
                      <embed src="{{asset($tramite->url_firmado)}}" type="application/pdf" width="100%" height="400px" />
                      <br>  
                    </div>  
                </div>
                @endif
              </div>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /.modal -->
        @endforeach
        <div class="col-sm-12">
              <div class="pull-right">

              {!! str_replace('/?', '?', $registrosRservados->appends(Input::except('page'))->render()) !!}

              </div>
        </div>
      </div>
    </div>
</div>

@endsection
@section('before-scripts-end')
@include('includes.scripts.script_fecha.scripts_filtro_fecha')
@stop
