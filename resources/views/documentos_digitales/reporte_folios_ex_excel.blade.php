<table>
  <tr>
   <th colspan="8" style="text-align: center;">MINISTERIO DEL DEPORTE</th>
  </tr>
  <tr>
   <th colspan="8" style="text-align: center;">ÁREA GESTIÓN DOCUMENTAL</th>
  </tr>
  <tr>
   <th colspan="8" style="text-align: center;">FOLIOS APARTADOS DEL MES</th>
  </tr>
</table >
<br>

@if(count($foliosApartadosMes)>0)
<table>
    <thead >
      <tr>
        <td colspan="3" style="color:#ffff; text-align: center; background-color: #013273;"><b>Folios Apartados</b></td>
        <td colspan="3" style="color:#ffff; text-align: center; background-color: #013273;"><b>Tipo Documento</b></td>
        <td colspan="2" style="color:#ffff; text-align: center; background-color: #013273;"><b>Estado</b></td>
      </tr>
    </thead>
    <tbody>
      @foreach($foliosApartadosMes as $indice => $datos)
      <tr>
        <td colspan="3"><center>{{$datos->correlativo}}</center></td>
        <td colspan="3"><center>{{$datos->tipoDocumento->nombre}}</center></td>
        <td colspan="2"><center>{{$datos->estatus}}</center></td>
      </tr>
      @endforeach
    </tbody>
</table>
@else
<h4 colspan="8" style="text-align: center;"><center><b>NO SE APARTARON FOLIOS EN EL MES.</b></center></h4>
@endif