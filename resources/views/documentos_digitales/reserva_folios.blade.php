@extends ('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1></h1><br>
    </div>
    <div class="col-sm-1">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item active"><i class="fa fa-list-ol"></i> Apartado de Folios</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
{!! Form::open(['route' => 'reservar_folios', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','id'=>'form-create']) !!}
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-danger card-outline">
        <h3 class="card-title">Apartar Folios</h3>
      </div>
      <div class="card-body">

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>
        

        <div class="callout col-sm-8 offset-sm-2" style="color: #737373;">
          <h4 style="font-size: 14px;"><i class="fa fa-info-circle" style="color: #ff0000;"></i> Nota:</h4>
          <span>Los campos marcados con un asterisco (*) son obligatorios.<br>
        </div>


    
        <div class="form-group row">
          {!! Form::label('tipo_documento','Tipo de documento (*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
            {!! Form::select('tipo_documento',$TiposDocumentos,null,['id'=>'tipo_documento','name'=>'tipo_documento', 'class'=>'form-control' ]) !!}
            </div>
        </div>
        @if (auth()->user()->hasPermissionNotAdmin(['15-23']))
        <div class="form-group row">
          {!! Form::label('lugar','Lugar (*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
            {!! Form::select('lugar',$selectLugar,null,['id'=>'lugar','name'=>'lugar', 'class'=>'form-control' ]) !!}
            </div>
        </div>
        @endif
        @if (auth()->user()->hasPermissionNotAdmin(['15-18']))
        <div class="form-group row">
          {!! Form::label('lugar','Lugar (*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
            {!! Form::select('lugar',$lugaresCentral,null,['id'=>'lugar','name'=>'lugar', 'class'=>'form-control' ]) !!}
            </div>
        </div>
        @endif
        @if (auth()->user()->hasPermissionNotAdmin(['15-17']))
        <div class="form-group row">
          {!! Form::label('lugar','Lugar (*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
            {!! Form::select('lugar',$lugaresRegiones,null,['id'=>'lugar','name'=>'lugar', 'class'=>'form-control' ]) !!}
            </div>
        </div>
        @endif

        <div class="form-group row" >
          {!! Form::label('n_folios', 'Cantidad de Folio(s) a Apartar (*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('n_folios', null, ['class' => 'form-control', 'placeholder' => 'Numero de Folios a Apartar']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('descripcion', 'Motivo del Apartado de Folio(s) (*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'placeholder' =>'Indique el motivo de su Reserva de folio(s)...']) !!}
          </div>
        </div>  

        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! Form::submit('Apartar', ['class' => 'btn btn-success btn-sm' , 'title'=>'Apartar']) !!}
              {!! link_to_route('folios2', 'Volver', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
  {!! Form::close() !!}
  @stop

@section('after-scripts-end')
@endsection