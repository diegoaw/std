@extends ('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1>Firma Avanzada Electrónica SEGPRES para instrumentos públicos MINDEP</h1><br>
    </div>
    <div class="col-sm-1">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item active"><i class="fa fa-file-text"></i> Añadir Firma</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
{!! Form::open(['route' => 'firmarDocumentos.firmarv2', 'files' => true, 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','id'=>'form-create']) !!}
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-danger card-outline">
        <h3 class="card-title">Añadir Firma</h3>
        <!-- <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse">
            <i class="fa fa-minus"></i>
          </button>
        </div> -->
      </div>
      <div class="card-body">

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>

        <div class="callout col-sm-8 offset-sm-2" style="color: #737373;">
          <h4 style="font-size: 14px;"><i class="fa fa-info-circle" style="color: #ff0000;"></i> Nota:</h4>
          <span>Los campos marcados con un asterisco (*) son obligatorios.<br>
            <span>Usted debe estar habilitado en la plataforma de firma digital para firma deshatendida.<br>
             <span>Su rut debe introducirlo sin puntos y sin el dígito verificador (solo números).<br>
        </div><br>

        <div class="form-group row">
          {!! Form::label('id_doc','ID Documento (*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
            {!! Form::text('id_doc',$id_doct,['id'=>'id_doc','name'=>'id_doc', 'class'=>'form-control' , 'readonly'=>'readonly' ]) !!}
            </div>
        </div>

         <div class="form-group row">
          {!! Form::label('tipo_firma','Tipo de firma (*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
            {!! Form::select('tipo_firma',$tipoFirma,1,['id'=>'tipo_firma','name'=>'tipo_firma', 'class'=>'form-control' , 'readonly'=>'readonly' ]) !!}
            </div>
        </div>

<center><span> <i class="fa fa-info-circle" style="color: #ff0000;"></i> Recuerde introducir el rut con solo numeros y no coloque el dígito verificador</span></center><br>
        <div class="form-group row">

          {!! Form::label('rut', 'Rut(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('rut', $rut, ['class' => 'form-control', 'placeholder' => '27124769' , 'readonly'=>'readonly']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('dv', 'Digitoverificador(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('dv', $dv, ['class' => 'form-control', 'placeholder' => 'K' , 'readonly'=>'readonly' ]) !!}
          </div>
        </div>

          <div class="form-group row">
          {!! Form::label('nombreFirmante', 'Nombre del Firmante(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('nombreFirmante', $nombreFirmante, ['class' => 'form-control', 'placeholder' => 'Pedro Perez' , 'readonly'=>'readonly' ]) !!}
          </div>
        </div>

           <div class="form-group row">
          <div class="col-sm-6">
            {!! Form::hidden('correo', $correo, ['class' => 'form-control', 'placeholder' => 'ejemplo.ejemplo@mindep.cl' ]) !!}
          </div>
        </div>


        <div class="form-group row">
          {!! Form::label('folio', 'Folio del documento(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('folio', $docSearc->folio_documento, ['class' => 'form-control', 'placeholder' => '' , 'readonly'=>'readonly']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('tipo_documento','Tipo de documento (*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
            {!! Form::select('tipo_documento',$TiposDocumentos,$docSearc->id_tipo_documento,['id'=>'tipo_documento','name'=>'tipo_documento', 'class'=>'form-control' , 'readonly'=>'readonly' ]) !!}
            </div>
        </div>

         <div class="form-group row">
          {!! Form::label('reservado','Reservado', ['class' => 'col-sm-3 col-form-label']) !!}

@if($docSearc->reservado == 1)
          <div class="col-sm-6">
            {!! Form::checkbox('reservado', '1', true) !!}
          </div>
        </div>
@else
      <div class="col-sm-6">
            {!! Form::checkbox('reservado', '1', false) !!}
          </div>
        </div>
@endif
        <div class="form-group row">
          {!! Form::label('nombre', 'Nombre del documento(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('nombre', $docSearc->nombre, ['class' => 'form-control', 'placeholder' => 'Titulo'  , 'readonly'=>'readonly']) !!}
          </div>
        </div>


        <div class="form-group row">
          {!! Form::label('descripcion', 'Descripción o materia (*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('descripcion', $docSearc->descripcion, ['class' => 'form-control', 'placeholder' => 'Descripción o materia' , 'readonly'=>'readonly']) !!}
          </div>
        </div>

         
         <div class="form-group row col-md-12">
                {!! Form::label('contenido_principal', 'Documento(*)', ['class' => 'col-lg-3 col-form-label']) !!}
              <div class="col-md-6">
                {!! Form::file('contenido_principal', ['class' => 'form-control' , 'required'=>'required']) !!}
              </div>
          </div>

        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! Form::submit('Firmar', ['class' => 'btn btn-success btn-sm',  'onclick'=>"inload()" , 'title'=>'Firmar']) !!}
              {!! link_to_route('firmarDocumentos', 'Volver', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
  {!! Form::close() !!}
  @stop

@section('after-scripts-end')
<script type="text/javascript">
 window.location.href="https://accounts.claveunica.gob.cl/api/v1/accounts/app/logout"; 
 document.getElementById("load").style.display="none"; 
$("#rut").keypress(soloNumeros);
$("#folio").keypress(soloNumeros);
  function soloNumeros(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57  ) && charCode!=107)
      return false;
    return true;
  }

  function inload(){
  swal("Espere por Favor se esta firmando su documento", " en breves momentos se descargará");
 
  setTimeout(function(){ 
    document.getElementById("load").style.display="none"; 
    window.location = '/FirmarDocumentos';
    }, 60000);
}
</script>
@endsection