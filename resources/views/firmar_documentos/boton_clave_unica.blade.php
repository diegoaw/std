{{-- @Nombre del programa: Vista Principal--}}
@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1>Firma Avanzada Electrónica SEGPRES para instrumentos públicos MINDEP</h1><br>
    </div>
    <div class="col-sm-1">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item active"><i class="fa fa-lock"></i> Clave Única</li>
      </ol>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
  <style>
.icon-claveunica:before {
  content: '\e801';
}
  </style>
@endsection
@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-primary card-outline">
        <h3 class="card-title">Autenticación Clave Única</h3>
        <div class="card-tools">
          <!-- <button type="button" class="btn btn-tool" data-widget="collapse">
            <i class="fa fa-minus"></i>
          </button> -->
        </div>
      </div>
      <br>
      <div class="callout col-sm-8 offset-sm-2" style="color: #737373;">
          <h4 style="font-size: 14px;"><i class="fa fa-info-circle" style="color: #ff0000;"></i>Nota:</h4>
          <span>Para poder firmar un documento debe Autenticarse con su Clave Única.<br>
        </div>
        <br>
      <div class="card-body">
        <center>
        <a href="{{route('login.claveunica')}}" class="nav-link"> <img src="{{asset('/assets/btn_claveunica_202.png')}}"></a>
        </center>
      </div>
      <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! link_to_route('firmarDocumentos', 'volver', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
@endsection
@section('before-scripts-end')

@stop