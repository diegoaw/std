{{-- @Nombre del programa: Vista Principal--}}
@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1>Firma Avanzada Electrónica SEGPRES para instrumentos públicos MINDEP</h1><br>
    </div>
    <div class="col-sm-1">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item active"><i class="fa fa-file-text"></i> Documentos Firmados</li>
      </ol>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title"> Documentos Firmados</h5>
      </div>
      <br>
        <div class="row">
          <div class="col-md-8 offset-md-2">
            <center><a title="Firmar Documento" class="btn btn-primary" href="{{route('firmarDocumentosInvividual.create')}}"><i class="fa fa-plus"></i> Firmar Documento </a></center>
          </div>
        </div>
      <br/>

  <div class="card-body">
        <div class="table-responsive">
          <table id="example1" class="table table-bordered table-striped table-hover table-sm">
            <thead class ="theadtable">
              <tr>
                <th>Folio</th>
                <th>Nombre</th>
                <th>Descripción</th>
                <th>Tipo de Documento</th>
                <th>Reservado</th>
                <th>Tipo de Firma</th>
                <th>Firmante</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>    
             @foreach($documentosFirmados as $registro)
              <tr>
                <td nowrap>{{ $registro->folio_documento }}</td>
                <td nowrap>{{ $registro->nombre }}</td>
                <td >{{ $registro->descripcion }}</td>
                <td nowrap>{{ $registro->tipoDocumento->nombre }}</td>
                <td nowrap>{{ $registro->ReservadoC }}</td>
                <td nowrap>{{ $registro->tipoFirma->nombre }}</td>
                <td nowrap>{{ $registro->rut_firmante }}</td>
                <td nowrap >
                  @if ($registro->firmantes->contains('usuario_id', auth()->user()->id)||$registro->reservado==0)
                  <center>
                  <a href="{{asset('firmados/'.$registro->url_documento_firmado)}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a> 
                  </center> 
                  @endif
                </td>
              </tr>      
                @endforeach  
            </tbody>
          </table>
        </div>
        <br>
        <div class="col-sm-12">
          <div class="pull-right">

          </div>
        </div>
  </div>
  </div>
  </div>
</div>

 
@endsection
@section('before-scripts-end')

@stop