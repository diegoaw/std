@extends ('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1>Firma Avanzada Electrónica SEGPRES para instrumentos públicos MINDEP</h1><br>
    </div>
    <div class="col-sm-1">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item active"><i class="fa fa-file-text"></i> Tipo de Firma</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
{!! Form::open(['route' => 'firmarDocumentos.createC', 'files' => true, 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','id'=>'form-create']) !!}
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-primary card-outline">
        <h3 class="card-title"> Tipo de Firma </h3>
        <!-- <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse">
            <i class="fa fa-minus"></i>
          </button>
        </div> -->
      </div>
      <div class="card-body">

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>

        <div class="callout col-sm-8 offset-sm-2" style="color: #737373;">
          <h4 style="font-size: 14px;"><i class="fa fa-info-circle" style="color: #ff0000;"></i> Nota:</h4>
          <span>Los campos marcados con un asterisco (*) son obligatorios.<br>
          <span>Seleccione Primera Firma si su documento no ha sido firmado antes.<br>
          <span>Seleccione Añadir Firma si su documento ya ha sido firmado anteriormente.<br>
        </div><br>

         <div class="form-group row">
          {!! Form::label('select_firma','Tipo de firma (*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
            {!! Form::select('select_firma',$selectFirma,null,['id'=>'select_firma','name'=>'select_firma', 'class'=>'form-control' ]) !!}
            </div>
        </div>
<div id="id_documento_div" style="display: none">
        <span> <i class="fa fa-info-circle" style="color: #ff0000;"></i> Por favor introduzca el id del documento para añadir firma.</span><br>
        <span> <i class="fa fa-info-circle" style="color: #ff0000;"></i> Puede encontrarlo en la última página de documento firmado.</span><br>

        <img class="img-thumbnail img-home-page" src="{{asset('/assets/ejem1.png')}}" alt="ejemplo id">


        <div class="form-group row">

          {!! Form::label('id_doc', 'ID docuemnto a firmar(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('id_doc', null, ['class' => 'form-control', 'placeholder' => '54840c30']) !!}
          </div>
        </div>
  </div>


  <div class="form-group row">
    
          <div class="col-sm-6">
            {!! Form::hidden('rut', $rut, ['class' => 'form-control', 'placeholder' => '27124769' ]) !!}
          </div>
  </div>
   <div class="form-group row">
       
          <div class="col-sm-6">
            {!! Form::hidden('dv', $digitoVerificador, ['class' => 'form-control', 'placeholder' => 'K' ]) !!}
          </div>
  </div>
    <div class="form-group row">
         
          <div class="col-sm-6">
            {!! Form::hidden('nombre', $nombre, ['class' => 'form-control', 'placeholder' => 'Pedro Perez' ]) !!}
          </div>
  </div>
     <div class="form-group row">
   
          <div class="col-sm-6">
            {!! Form::hidden('correo', $correoElectronico, ['class' => 'form-control', 'placeholder' => 'Pedro Perez' ]) !!}
          </div>
  </div>



        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! Form::submit('Siguiente', ['class' => 'btn btn-success btn-sm', 'onclick'=>"inload()",  'title'=>'Siguiente']) !!}
              {!! link_to_route('firmarDocumentos', 'Volver', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
  {!! Form::close() !!}
  @stop

@section('after-scripts-end')
<script type="text/javascript">
  //window.location.href="https://accounts.claveunica.gob.cl/api/v1/accounts/app/logout";
  // document.getElementById("load").style.display="none";
 $(document).ready(function(){
      $('#select_firma').on('change', function(){
        var firma = $(this).val();
        if(firma == 1){
          $('#id_documento_div').show();
        }else{
          $('#id_documento_div').hide();
        }         
        });
      });
</script>
@endsection