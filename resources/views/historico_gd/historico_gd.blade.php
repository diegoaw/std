@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-search"></i> Consulta Histórico Gestión Documental Antiguo</h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title">Filtros Histórico Gestión Documental Antiguo</h5>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">

             {!! Form::open(['route' => 'historico_gd', 'method' => 'get' , 'class'=>"form-horizontal"]) !!}

                  <div class="form-row">

                    <div class="col-sm-2">
                      <label for="Expediente" class=" col-form-label">N° Expediente:</label>
                      <input class="form-control rounded-0" id="Expediente" name="Expediente" rows="3" value="{{ $Expediente }}">
                    </div>

                    <div class="col-sm-2">
                      <label for="Departamento" class=" col-form-label">Departamento:</label>
                      <input class="form-control rounded-0" id="Departamento" name="Departamento" rows="3" value="{{ $Departamento }}">
                    </div>

                    <div class="col-sm-2">
                      <label for="Division" class=" col-form-label">Division:</label>
                      <input class="form-control rounded-0" id="Division" name="Division" rows="3" value="{{ $Division }}">
                    </div>

                    <!--  <div class="col-sm-2">
                      <label for="Nombre" class=" col-form-label">Nombre:</label>
                      <input class="form-control rounded-0" id="Nombre" name="Nombre"rows="3" value="{{ $Nombre }}">
                    </div>

                    <div class="col-sm-2">
                      <label for="Usuario" class=" col-form-label">Usuario:</label>
                      <input class="form-control rounded-0" id="Usuario" name="Usuario" rows="3" value="{{ $Usuario }}">
                    </div> -->

                    <div class="col-sm-2">
                      <label for="Cargo" class=" col-form-label">Cargo:</label>
                      <input class="form-control rounded-0" id="Cargo" name="Cargo" rows="3" value="{{ $Cargo }}">
                    </div>

                    <div class="col-sm-2">
                      <label for="Correo" class=" col-form-label">Correo:</label>
                      <input class="form-control rounded-0" id="Correo" name="Correo" rows="3" value="{{ $Correo }}">
                    </div>

                    <div class="col-sm-2">
                      <label for="OrigendelDocto" class=" col-form-label">Origen:</label>
                      <input class="form-control rounded-0" id="OrigendelDocto" name="OrigendelDocto" rows="3" value="{{ $OrigendelDocto }}">
                    </div>

                    <div class="col-sm-2">
                      <label for="Remitente" class=" col-form-label">Remitente:</label>
                      <input class="form-control rounded-0" id="Remitente" name="Remitente" rows="3" value="{{ $Remitente }}">
                    </div>

                    <div class="col-sm-2">
                      <label for="CargoRemitente" class=" col-form-label">Cargo Remitente:</label>
                      <input class="form-control rounded-0" id="CargoRemitente" name="CargoRemitente" rows="3" value="{{ $CargoRemitente }}">
                    </div>

                    <div class="col-sm-2">
                      <label for="Procedencia" class=" col-form-label">Procedencia:</label>
                      <input class="form-control rounded-0" id="Procedencia" name="Procedencia" rows="3" value="{{ $Procedencia }}">
                    </div>

                    <div class="col-sm-2">
                      <label for="Tipo" class=" col-form-label">Tipo:</label>
                      <input class="form-control rounded-0" id="Tipo" name="Tipo" rows="3" value="{{ $Tipo }}">
                    </div>

                    <div class="col-sm-2">
                      <label for="Numero" class=" col-form-label">N° Docto/Folio:</label>
                      <input class="form-control rounded-0" id="Numero" name="Numero" rows="3" value="{{ $Numero }}">
                    </div>

                    <div class="col-sm-2">
                      <label for="Contenido" class=" col-form-label">Contenido:</label>
                      <textarea class="form-control rounded-0" id="Contenido" name="Contenido" rows="3">{{ $Contenido }} </textarea>
                    </div>

                  </div>
                  <br>
                  <button type="submit" class="btn btn-primary pull-right">Buscar</button>

            {!! Form::close() !!}

          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header card-danger card-outline">
          <h5 class="card-title">Histórico Gestión Documental Antiguo</h5>

        </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">

                        
                        <th nowrap style="color:white"><center>N° Expediente</center></th>
                        <th nowrap style="color:white"><center>Fecha</center></th>
                        <th nowrap style="color:white">División</th>
                        <th nowrap style="color:white">Departamento</th>
                        <th nowrap style="color:white">Remitente</th>
                        <th nowrap style="color:white">Cargo Remitente</th>
                        <th nowrap style="color:white">Correo</th>
                        <th nowrap style="color:white">Procedencia</th>
                        <th nowrap style="color:white">Origen</th>
                        <th nowrap style="color:white"><center>Tipo</center></th>
                        <!-- <th nowrap style="color:white">Nombre</th>
                        <th nowrap style="color:white">Usuario</th>
                        <th nowrap style="color:white">Cargo</th> -->
                        <th nowrap style="color:white"><center>N° Docto/Folio</center></th>
                        <th style="color:white"><center>Tipo de Contenido</center></th>
                        <th nowrap style="color:white">Contenido</th>
                        <th nowrap style="color:white">Archivo</th>
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($historico)>0)
                      @foreach ($historico as $indice => $tramite)
                          <tr>

                           
                           <td><center>{{$tramite->Expediente}}</center></td>
                           <td nowrap>{{$tramite->FechaCarpeta}} </td>
                           <td>{{$tramite->Division}}</td>
                           <td>{{$tramite->Departamento}}</td>
                           <td>{{$tramite->Remitente}}</td>
                           <td>{{$tramite->CargoRemitente}}</td>
                           <td>{{$tramite->Correo}}</td>
                           <td>{{$tramite->Procedencia}}</td>
                           <td>{{$tramite->OrigendelDocto}}</td>
                           <td><center>{{$tramite->Tipo}}</center></td>
                           <!-- <td>{{$tramite->Nombre}}</td>
                           <td>{{$tramite->Usuario}}</td> 
                           <td>{{$tramite->Cargo}}</td> -->
                           <td><center>{{$tramite->Numero}}</center></td>
                           <td><center>{{$tramite->TipoContenido}}</center></td>
                           @if($tramite->TipoContenido != 'contenido normal' && !auth()->user()->hasPermission(['10-02']))

                           <td>Contenido <b>Reservado</b> solicite información al Remitente {{$tramite->Remitente}}</td>

                           @else

                           <td>{{$tramite->Contenido}}</td>

                           @endif
                           <td>
                             <center>
                            @if ($tramite->Archivo != null && $tramite->Archivo != '' && $tramite->TipoContenido == 'contenido normal')
                               <a class="btn accion btn-success accion" href="{{$tramite->UrlArchivo}}" target="_blank"><i class="fa fa-download"></i></a>
                             @endif
                          </center>
                           </td>
                          </tr>
                      @endforeach
                @endif
                </tbody>
              </table>
              </div>
              <div class="col-sm-12">
                <div class="pull-right">

                  {{ $historico->links() }}

                </div>
          </div>



    </div>
  </div>
</div>

@endsection
@section('before-scripts-end')
@stop
