@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-search"></i> Consulta Archivos Histórico Gestión Documental Antiguo</h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title">Filtros Archivos Histórico Gestión Documental Antiguo</h5>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">

             {!! Form::open(['route' => 'historico_gd_archivos', 'method' => 'get' , 'class'=>"form-horizontal"]) !!}

                  <div class="form-row">

                  <div class="col-sm-3">
                      <label for="ano" class=" col-form-label">Año: </label>
                      {!! Form::select('ano',$selectAno,$ano,['id'=>'ano','name'=>'ano', 'class'=>'form-control']) !!}
                    </div>


                    <div class="col-sm-3">
                      <label for="ano" class=" col-form-label">Confidencialidad: </label>
                      {!! Form::select('confidencialidad',$selectConfi,$confidencialidad,['id'=>'confidencialidad','name'=>'confidencialidad', 'class'=>'form-control']) !!}
                    </div>

                    <div class="col-sm-3">
                      <label for="ano" class=" col-form-label">ClasificaciÓn: </label>
                      {!! Form::select('clasificacion',$selectClasi, $clasificacion,['id'=>'clasificacion','name'=>'clasificacion', 'class'=>'form-control']) !!}
                    </div>


                    <div class="col-sm-2">
                      <label for="archivoNombre" class=" col-form-label">Archivo Nombre:</label>
                      <input class="form-control rounded-0" id="archivoNombre" name="archivoNombre" rows="3" value="{{ $archivoNombre }}">
                    </div>

                    <!-- Falta Fecha (fecha_modificacion , fechaModificacion) -->

                    <div class="form-group   {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                    <div class="input-group date">
                      <div class="col">
                          <label for="FechaDesde" class=" col-form-label">Fecha Desde: </label>
                          <input type="text" class="form-control group-date" id="FechaDesde" name="FechaDesde" readonly="readonly" value="{!! date('Y-m-d', strtotime($feDesde))  !!}">
                      </div>
                    </div>
                    </div>

                    <div class="form-group   {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                    <div class="input-group date">
                      <div class="col">
                          <label for="FechaHasta" class=" col-form-label">Fecha Hasta: </label>
                          <input type="text" class="form-control group-date" id="FechaHasta" name="FechaHasta" readonly="readonly" value="{!! date('Y-m-d', strtotime($feHasta ))  !!}">
                      </div>
                    </div>
                    </div>

                    <div class="col-sm-2">
                         <label for="materia" class=" col-form-label">Materia:</label>
                         <textarea class="form-control rounded-0" id="materia" name="materia" rows="3">{{ $materia }} </textarea>
                    </div>

                  </div>
                  <br>
                  <button type="submit" class="btn btn-primary pull-right">Buscar</button>

            {!! Form::close() !!}

          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header card-danger card-outline">
          <h5 class="card-title">Achivos Histórico Gestión Documental Antiguo</h5>

        </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th nowrap style="color:white">Año</th>
                        <th  style="color:white"><center>Confidencialidad</center></th>
                        <th  style="color:white">Clasificación/Tipo Documento</th>
                        <th  style="color:white">Materia</th>
                        <th  style="color:white">Fecha Modificación</th>
                        <!-- <th nowrap style="color:white">Archivo Nombre</th> -->
                        <th  style="color:white"><center>Descarga Archivo</center></th>
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($historico)>0)
                      @foreach ($historico as $indice => $tramite)
                          <tr>
                           <td>{{$tramite->ano}}</td>
                           <td><center>{{$tramite->confidencialidad}}</center></td>
                           <td><center>{{$tramite->clasificacion}}</center></td>
                           <td>{{$tramite->materia}}</td>
                           <td>{{$tramite->fecha_modificacion}}</td>
                           <!-- <td>{{$tramite->archivo_nombre}}</td> -->
                            @if($tramite->confidencialidad != 'Contenido Normal' && !auth()->user()->hasPermission(['10-07']))
                          <td>

                           </td>
                           @else
                           <td>
                              <center>
                                <a class="btn accion btn-success accion" href="https://historicogd.mindep.cl/{{$tramite->ruta}}" target="_blank"><i class="fa fa-download"></i></a>
                              </center>
                            </td>
                            @endif
                          </tr>
                      @endforeach
                @endif
                </tbody>
              </table>
              </div>
              <div class="col-sm-12">
                <div class="pull-right">

                  {{ $historico->links() }}

                </div>
             </div>
    </div>
  </div>
</div>

@endsection
@section('before-scripts-end')
@include('includes.scripts.script_fecha.scripts_filtro_fecha')
@stop
