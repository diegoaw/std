@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-5">
      <h1><b><i class="fa fa-id-badge"></i> Consultas sobre Incentivo al Retiro</b></h1>
    </div>
    <div class="col-sm-7">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-id-badge"></i> Consultas sobre Incentivo al Retiro</li>
        <li class="breadcrumb-item active">Detalles</li>
      </ol>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <!-- Line chart -->
    <div class="card card-danger card-outline">
      <div class="card-header">
        <h3 class="card-title"><b>Consulta sobre Incentivo al Retiro </b><b style="color:DarkRed;">N° {{$data->id}}</b></h3>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <br>
        <h4><b style="color:DarkRed;">Datos Funcionario(a)</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b style="color:DarkBlue;">Fecha Solicitud:</b></td>
              <td nowrap>{{$data->fecha_solicitud}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Seleccione a el(la) Funcionario(a):</b></td>
              <td nowrap>{{$data->seleccione}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Nombre y Cargo Funcionario(a):</b></td>
              <td >{{$data->cargo}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>División/Área:</b></td>
              <td nowrap>{{$data->division}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Correo Funcionario(a):</b></td>
              <td nowrap>{{$data->correo}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Fecha de Nacimiento:</b></td>
              <td nowrap>{{$data->fecha_nacimiento}}</td>
          </tr>
		      <tr>
              <td style="width:450px"><b>RUT Funcionario:</b></td>
              <td nowrap>{{$data->rut}}</td>
          </tr>          
          <tr>
              <td style="width:450px"><b>Calidad Jurídica:</b></td>
              <td nowrap>{{$data->calidad_juridica}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Grado:</b></td>
              <td nowrap>{{$data->grado}}°</td>
          </tr> 
          </table>
          
        <br>
        <br>
        <h4><b style="color:DarkRed;">Datos Laborales</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>¿Cuántos Años de Servicio Tiene?:</b></td>
              <td nowrap>
                @if($data->rango == '20 Anos o Mas') 20 Años o Mas @endif
                @if($data->rango == '18 a 20 Anos') 18 a 20 Años @endif
                @if($data->rango == 'Menos de 18') Menos de 18 @endif
              </td>
          </tr>
          <tr>
              <td style="width:450px"><b>
                @if($data->selec_previ)Seleccione su Sistema de Previsión:@endif
                @if($data->nombre_afp_18)Afiliación Sistema de Pensiones:@endif
                @if($data->nombre_afp)Afiliación Sistema de Pensiones:@endif
              </b></td>
              <td nowrap>
                @if($data->selec_previ) {{$data->selec_previ}} @endif
                @if($data->nombre_afp_18) {{$data->nombre_afp_18}} @endif
                @if($data->nombre_afp) {{$data->nombre_afp}} @endif
              </td>
          </tr>
          <tr>
              <td style="width:450px"><b>Archivo de Afiliación al Sistema de Pensiones AFP/Capredena/IPS:</b></td>
              <td nowrap>
              @if($data->afiliacion_afp_18)
              <a href="{{$data->afiliacion_afp_18}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
              @if($data->afiliacion_afp)
              <a href="{{$data->afiliacion_afp}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
              </td>
          </tr> 
          <tr>
              <td style="width:450px"><b>Archivo de las Cotizaciones Previsionales:</b></td>
              <td nowrap>@if($data->cotixaciones_afp)
              <a href="{{$data->cotixaciones_afp}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Archivo de Certificado Emitido por la Entidad Empleadora que Verifique los Años de Antigüedad en la Administración Central del Estado:</b></td>
              <td nowrap>@if($data->certificado_antiguedad)
              <a href="{{$data->certificado_antiguedad}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif</td>
          </tr>     
        </table>

        <br>
        <br>
        <h4><b style="color:DarkRed;">Voluntariamente</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b ALIGN="justify">Manifiesto Mi Voluntad de Acceder a las Siguientes Bonificaciones, para lo Cual el Servicio me Podrá Solicitar Otros Antecedentes no Adjuntos a esta Postulación, para Verificar el Cumplimiento de los Demás Requisitos Establecidos en Cada una de las Normas que da Origen a los Demás Beneficios:</b></td>
              <td nowrap>{{implode(", ", $data->manifiesto)}}</td>
          </tr>
          @if($data->declaracion_jurada)
          <tr>
              <td style="width:450px"><b>Declaración Jurada Simple:</b></td>
              <td nowrap>
              <a href="{{$data->declaracion_jurada}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              </td>
          </tr>
          @endif
          <tr>
              <td style="width:450px"><b>Archivo Carta de Renuncia Voluntaria:</b></td>
              <td nowrap>@if($data->carta_renuncia)
              <a href="{{$data->carta_renuncia}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif</td>
          </tr>            
        </table>

        <br>
        <br>
        <h4><b style="color:DarkRed;">Edad y Género Funcionario(a)</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Seleccione Género:</b></td>
              <td nowrap>{{$data->genero}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Cargue Certificado de Nacimiento:</b></td>
              <td nowrap>@if($data->certificado_nacimiento)
              <a href="{{$data->certificado_nacimiento}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif</td>
          </tr> 
          <tr>
              <td style="width:450px"><b>Seleccione su Edad:</b></td>
              <td nowrap>
                @if($data->edad_f) {{$data->edad_f}} @endif
                @if($data->edad_m) {{$data->edad_m}} @endif
                @if($data->edad_o) {{$data->edad_o}} @endif  
              </td>
          </tr>
        </table>

        <br>
        <br>
        <h4><b style="color:DarkRed;">Formulario Único de Postulación a los Beneficios Indicados por la Ley N°20.948</b></h4>
        <br>
        <table id="example1" class="table table-striped">
            <tr>
              <td style="width:450px"><b>Formulario Único Firmado:</b></td>
              <td nowrap>@if($data->formulario_firmado)
              <a href="{{$data->formulario_firmado}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif</td>
            </tr>               
        </table>

        <br>
        <br>
        <h4><b style="color:DarkRed;">Comprobante de Solicitud</b></h4>
        <br>
        <table id="example1" class="table table-striped">
            <tr>
              <td style="width:450px"><b>Descargar Comprobante:</b></td>
              <td nowrap><a href="{{$data->comprobante}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a></td>
            </tr>               
        </table>

        
        <br>
        <br>
        <h4><b style="color:DarkBlue;">Recepción y Revisión Solicitud Bono Escolaridad</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Fecha Revisión:</b</td>
              <td nowrap>{{$data->fecha_revision}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>¿Solicitud Bono de Escolaridad Cumple con los Requisitos?</b></td>
              <td nowrap>{{$data->cumple_requisitos}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Observaciones:</b></td>
              <td nowrap>{{$data->observaciones_correccion}}</td>
          </tr>            
        </table>

      </div>

        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! link_to_route('incentivoretiro', 'Volver', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
            </div>
          </div>
        </div>
    </div>
  </div>
</div>


@endsection
@section('before-scripts-end')
@stop