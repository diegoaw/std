					<footer id="footer-simple" style="background: #001c41">
					    <div class="container">
					        <div class="row">
					            <div class="col-lg-12">
					                <div class="row align-items-center">
					                    <div class="col-lg-2">
					                        <div class="logos-container">
					                            <img class="ft-logo-dgd" src="{{ asset('assets/logo-mindep-blanco.svg') }}" alt="Logo MINDEP" width="100" height="153">
					                        </div>
					                    </div>
                    <div class="col-lg-5" style="color: #ffffff">
                         Ministerio del Deporte<br>
                         Fidel Oteíza 1956, Piso 2<br>
                         Providencia, Santiago, Chile.<br>
                         Teléfono 227540200 / Fax 223689685
                    </div>
                </div>
            </div>
            
                    <div class="col-lg-12">
                        <div class="gob-lines">
                            <div class="blue"></div>
                            <div class="red"></div>
                        </div>
                    </div>
        </div>
    </div>
</footer>

