<script type="text/javascript">

var Eliminar = function(id){
  console.log('este es el num del calendario', id);
 swal({
  title: '¿Está seguro de eliminar el calendario de programas sociales del sistema?',
  text: "El mismo no se podrá recuperar",
  icon: 'warning',
  buttons: ['Cancelar', "Aceptar"]
}).then((result) => {

  if (result) {
      var route = "{{url('SIGC/ProgramasSociales/Borrar/CalendarioEventos')}}/"+id+"";
      console.log(route);
      var token = $("#token").val();
      $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'GET',
        dataType: 'json',
        success: function(data) {
          if (data.success == 'true')
          {
            //Alerta de Confirmación
            swal({
              title: 'Calendario eliminado satisfactoriamente',
              text: "",
              icon: 'success'
            });
            var url='{{ url('SIGC/ProgramasSociales/CalendarioEventos')}}';
            setTimeout(function(){
              location.href = url;
              //location.reload(true);
            },500)
          }
        },
        error:function(data)
        {
          switch (data.status) {
            case 400:
            $("#message-error-delete").show().html("Servidor ha entendido la solicitud, pero el contenido de solicitud no es válida.");
            break;
            case 422:
            var errors = '<ul>';
            for (datos in data.responseJSON)
            {
              errors += '<li>' +data.responseJSON[datos] + '</li>';
            }
            errors += '</ul>';
            $("#message-error-delete").show().html(errors);
            break;
            case 401:
            $("#message-error-delete").show().html("Acceso no autorizado.");
            break;
            case 403:
            $("#message-error-delete").show().html("Prohibido recurso no se puede acceder.");
            break;
            case 405:
            $("#message-error-delete").show().html("Ha ocurrido un error en la aplicación.");
            break;
            case 500:
            $("#message-error-delete").show().html("Error Interno del Servidor.");
            break;
            case 503:
            $("#message-error-delete").show().html("Servicio no disponible.");
            break;
          }
        }
      });
    }
  });
};

</script>
