<script type="text/javascript">

var Eliminar = function(id ,idMedicion){
 swal({
  title: '¿Está seguro de eliminar el archivo de esta medición?',
  text: "El mismo no podrá recuperar nuevamente!",
  icon: 'warning',
  buttons: ['Cancelar', "Aceptar"]
}).then((result) => {

  if (result) {
      var route = "{{url('SIGC/Mediciones/ArchivosAsociados/Eliminar')}}/"+id+"";
      console.log(route);
      var token = $("#token").val();
      $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'GET',
        dataType: 'json',
        success: function(data) {
          if (data.success == 'true')
          {
            //Alerta de Confirmación
            swal({
              title: 'Archivo Eliminado Exitosamente',
              text: "",
              icon: 'success'
            });
            var url='{{ url('/SIGC/Mediciones/ArchivosAsociados/')}}'+"/"+idMedicion;
            setTimeout(function(){
              location.href = url;
              //location.reload(true);
            },500)
          }
        },
        error:function(data)
        {
          switch (data.status) {
            case 400:
            $("#message-error-delete").show().html("Servidor ha entendido la solicitud, pero el contenido de solicitud no es válida.");
            break;
            case 422:
            var errors = '<ul>';
            for (datos in data.responseJSON)
            {
              errors += '<li>' +data.responseJSON[datos] + '</li>';
            }
            errors += '</ul>';
            $("#message-error-delete").show().html(errors);
            break;
            case 401:
            $("#message-error-delete").show().html("Acceso no autorizado.");
            break;
            case 403:
            $("#message-error-delete").show().html("Prohibido recurso no se puede acceder.");
            break;
            case 405:
            $("#message-error-delete").show().html("Ha ocurrido un error en la aplicación.");
            break;
            case 500:
            $("#message-error-delete").show().html("Error Interno del Servidor.");
            break;
            case 503:
            $("#message-error-delete").show().html("Servicio no disponible.");
            break;
          }
        }
      });
    }
  });
};

var Eliminar2 = function(id ,idMedicion){
 swal({
  title: '¿Está seguro de eliminar el archivo de esta medición?',
  text: "El mismo no podrá recuperar nuevamente!",
  icon: 'warning',
  buttons: ['Cancelar', "Aceptar"]
}).then((result) => {

  if (result) {
      var route = "{{url('SIGC/Mediciones/ArchivosAsociados/Eliminar')}}/"+id+"";
      console.log(route);
      var token = $("#token").val();
      $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'GET',
        dataType: 'json',
        success: function(data) {
          if (data.success == 'true')
          {
            //Alerta de Confirmación
            swal({
              title: 'Archivo Eliminado Exitosamente',
              text: "",
              icon: 'success'
            });
            var url='{{ url('/SIGC/Editar/ReporteBorrador/')}}'+"/"+idMedicion;
            setTimeout(function(){
              location.href = url;
              //location.reload(true);
            },500)
          }
        },
        error:function(data)
        {
          switch (data.status) {
            case 400:
            $("#message-error-delete").show().html("Servidor ha entendido la solicitud, pero el contenido de solicitud no es válida.");
            break;
            case 422:
            var errors = '<ul>';
            for (datos in data.responseJSON)
            {
              errors += '<li>' +data.responseJSON[datos] + '</li>';
            }
            errors += '</ul>';
            $("#message-error-delete").show().html(errors);
            break;
            case 401:
            $("#message-error-delete").show().html("Acceso no autorizado.");
            break;
            case 403:
            $("#message-error-delete").show().html("Prohibido recurso no se puede acceder.");
            break;
            case 405:
            $("#message-error-delete").show().html("Ha ocurrido un error en la aplicación.");
            break;
            case 500:
            $("#message-error-delete").show().html("Error Interno del Servidor.");
            break;
            case 503:
            $("#message-error-delete").show().html("Servicio no disponible.");
            break;
          }
        }
      });
    }
  });
};

var Eliminar3 = function(id ,idMedicion){
 swal({
  title: '¿Está seguro de eliminar el archivo de esta medición?',
  text: "El mismo no podrá recuperar nuevamente!",
  icon: 'warning',
  buttons: ['Cancelar', "Aceptar"]
}).then((result) => {

  if (result) {
      var route = "{{url('SIGC/Mediciones/ArchivosAsociados/Eliminar')}}/"+id+"";
      console.log(route);
      var token = $("#token").val();
      $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'GET',
        dataType: 'json',
        success: function(data) {
          if (data.success == 'true')
          {
            //Alerta de Confirmación
            swal({
              title: 'Archivo Eliminado Exitosamente',
              text: "",
              icon: 'success'
            });
            var url='{{ url('/SIGC/Revision/')}}'+"/"+idMedicion;
            setTimeout(function(){
              location.href = url;
              //location.reload(true);
            },500)
          }
        },
        error:function(data)
        {
          switch (data.status) {
            case 400:
            $("#message-error-delete").show().html("Servidor ha entendido la solicitud, pero el contenido de solicitud no es válida.");
            break;
            case 422:
            var errors = '<ul>';
            for (datos in data.responseJSON)
            {
              errors += '<li>' +data.responseJSON[datos] + '</li>';
            }
            errors += '</ul>';
            $("#message-error-delete").show().html(errors);
            break;
            case 401:
            $("#message-error-delete").show().html("Acceso no autorizado.");
            break;
            case 403:
            $("#message-error-delete").show().html("Prohibido recurso no se puede acceder.");
            break;
            case 405:
            $("#message-error-delete").show().html("Ha ocurrido un error en la aplicación.");
            break;
            case 500:
            $("#message-error-delete").show().html("Error Interno del Servidor.");
            break;
            case 503:
            $("#message-error-delete").show().html("Servicio no disponible.");
            break;
          }
        }
      });
    }
  });
};

var Eliminar4 = function(id ,idMedicion){
 swal({
  title: '¿Está seguro de eliminar el archivo de esta medición?',
  text: "El mismo no podrá recuperar nuevamente!",
  icon: 'warning',
  buttons: ['Cancelar', "Aceptar"]
}).then((result) => {

  if (result) {
      var route = "{{url('SIGC/Mediciones/ArchivosAsociados/Eliminar')}}/"+id+"";
      console.log(route);
      var token = $("#token").val();
      $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'GET',
        dataType: 'json',
        success: function(data) {
          if (data.success == 'true')
          {
            //Alerta de Confirmación
            swal({
              title: 'Archivo Eliminado Exitosamente',
              text: "",
              icon: 'success'
            });
            var url='{{ url('/SIGC/Editar/Revision/')}}'+"/"+idMedicion;
            setTimeout(function(){
              location.href = url;
              //location.reload(true);
            },500)
          }
        },
        error:function(data)
        {
          switch (data.status) {
            case 400:
            $("#message-error-delete").show().html("Servidor ha entendido la solicitud, pero el contenido de solicitud no es válida.");
            break;
            case 422:
            var errors = '<ul>';
            for (datos in data.responseJSON)
            {
              errors += '<li>' +data.responseJSON[datos] + '</li>';
            }
            errors += '</ul>';
            $("#message-error-delete").show().html(errors);
            break;
            case 401:
            $("#message-error-delete").show().html("Acceso no autorizado.");
            break;
            case 403:
            $("#message-error-delete").show().html("Prohibido recurso no se puede acceder.");
            break;
            case 405:
            $("#message-error-delete").show().html("Ha ocurrido un error en la aplicación.");
            break;
            case 500:
            $("#message-error-delete").show().html("Error Interno del Servidor.");
            break;
            case 503:
            $("#message-error-delete").show().html("Servicio no disponible.");
            break;
          }
        }
      });
    }
  });
};

var Eliminar5 = function(id ,idMedicion){
 swal({
  title: '¿Está seguro de eliminar el archivo de esta medición?',
  text: "El mismo no podrá recuperar nuevamente!",
  icon: 'warning',
  buttons: ['Cancelar', "Aceptar"]
}).then((result) => {

  if (result) {
      var route = "{{url('SIGC/Mediciones/ArchivosAsociados/Eliminar')}}/"+id+"";
      console.log(route);
      var token = $("#token").val();
      $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'GET',
        dataType: 'json',
        success: function(data) {
          if (data.success == 'true')
          {
            //Alerta de Confirmación
            swal({
              title: 'Archivo Eliminado Exitosamente',
              text: "",
              icon: 'success'
            });
            var url='{{ url('/SIGC/Editar/Reporte/')}}'+"/"+idMedicion;
            setTimeout(function(){
              location.href = url;
              //location.reload(true);
            },500)
          }
        },
        error:function(data)
        {
          switch (data.status) {
            case 400:
            $("#message-error-delete").show().html("Servidor ha entendido la solicitud, pero el contenido de solicitud no es válida.");
            break;
            case 422:
            var errors = '<ul>';
            for (datos in data.responseJSON)
            {
              errors += '<li>' +data.responseJSON[datos] + '</li>';
            }
            errors += '</ul>';
            $("#message-error-delete").show().html(errors);
            break;
            case 401:
            $("#message-error-delete").show().html("Acceso no autorizado.");
            break;
            case 403:
            $("#message-error-delete").show().html("Prohibido recurso no se puede acceder.");
            break;
            case 405:
            $("#message-error-delete").show().html("Ha ocurrido un error en la aplicación.");
            break;
            case 500:
            $("#message-error-delete").show().html("Error Interno del Servidor.");
            break;
            case 503:
            $("#message-error-delete").show().html("Servicio no disponible.");
            break;
          }
        }
      });
    }
  });
};

</script>