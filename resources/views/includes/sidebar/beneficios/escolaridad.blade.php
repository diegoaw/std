@if (auth()->user()->hasPermissionModule('Beneficios'))
<li class="nav-item">
  <a href="{{route('escolaridad')}}" class="nav-link {{routeIs(['Escolaridad'], 'active')}}">
    <i class="nav-icon fa fa-id-badge"></i>
    <p>
      Bono Escolaridad
    </p>
  </a>
</li>
@endif