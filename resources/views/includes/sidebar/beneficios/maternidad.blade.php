@if (auth()->user()->hasPermissionModule('Beneficios'))
<li class="nav-item">
  <a href="{{route('maternidad')}}" class="nav-link {{routeIs(['Maternidad'], 'active')}}">
    <i class="nav-icon fa fa-female"></i>
    <p>
      Beneficios de Maternidad
    </p>
  </a>
</li>
@endif