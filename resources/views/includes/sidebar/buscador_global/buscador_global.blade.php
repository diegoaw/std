
<li class="nav-item">
  <a href="{{route('buscador_global')}}" class="nav-link {{routeIs(['BuscadorGlobal', 'BuscadorGlobal/*'], 'active')}}">
    <i class="nav-icon fa fa-search"></i>
    <p>
      Buscador Global
    </p>
  </a>
</li>
