@if (auth()->user()->hasPermissionModule('Cevs'))
<li class="nav-item">
  <a href="{{route('cevs')}}" class="nav-link {{routeIs(['Cevs', 'Cevs/*'], 'active')}}">
    <i class="nav-icon fa fa-map-marker"></i>
    <p>
      CDC
    </p>
  </a>
</li>
@endif