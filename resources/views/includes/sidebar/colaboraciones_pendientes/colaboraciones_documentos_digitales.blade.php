@if (auth()->user()->hasPermissionModule('DocumentosDigitales'))
<li class="nav-item">
  <a href="{{url('/DocumentosDigitales/ColaboracionesPendientes')}}" class="nav-link {{routeIs(['DocumentosDigitales/ColaboracionesPendientes', 'DocumentosDigitales/ColaboracionesPendientes/*'], 'active')}}">
    <i class="nav-icon fa fa-exchange"></i>
    <p>
      Modificaciones Pendientes
    </p>
  </a>
</li>
@endif