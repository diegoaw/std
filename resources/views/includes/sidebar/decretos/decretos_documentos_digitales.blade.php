@if (auth()->user()->hasPermissionNotAdmin(['15-22']))
<li class="nav-item">
  <a href="{{url('/DocumentosDigitales/Registro/Decretos')}}" class="nav-link {{routeIs(['DocumentosDigitales/Registro/Decretos', 'DocumentosDigitales/Registro/Decretos/*'], 'active')}}">
    <i class="nav-icon fa fa-file-text"></i>
    <p>
      Decretos Electrónicos
    </p>
  </a>
</li>
<li class="nav-item">
  <a href="{{url('/DocumentosDigitales/Registro/DecretosSupremos')}}" class="nav-link {{routeIs(['DocumentosDigitales/Registro/DecretosSupremos', 'DocumentosDigitales/Registro/DecretosSupremos/*'], 'active')}}">
    <i class="nav-icon fa fa-file-text"></i>
    <p>
      Decretos Supremos Electrónicos
    </p>
  </a>
</li>
<li class="nav-item">
  <a href="{{url('/DocumentosDigitales/Registro/DecretosExentos')}}" class="nav-link {{routeIs(['DocumentosDigitales/Registro/DecretosExentos', 'DocumentosDigitales/Registro/DecretosExentos/*'], 'active')}}">
    <i class="nav-icon fa fa-file-text"></i>
    <p>
      Decretos Exentos Electrónicos
    </p>
  </a>
</li>
@endif