@if (auth()->user()->hasPermissionModule('FirmarDocumentos'))
<li class="nav-item">
  <a href="{{route('firmarDocumentos')}}" class="nav-link {{routeIs(['FirmarDocumentos', 'FirmarDocumentos/*'], 'active')}}">
    <i class="nav-icon fa fa-file-text"></i>
    <p>
      Firma Avanzada SEGPRES
    </p>
  </a>
</li>
@endif