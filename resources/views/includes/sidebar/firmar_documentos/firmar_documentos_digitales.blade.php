@if (auth()->user()->hasPermissionModule('DocumentosDigitales') && auth()->user()->hasPermissionModule('Firmantes'))
<li class="nav-item">
  <a href="{{route('firmarDocumentos.LoginClaveUnica')}}" class="nav-link {{routeIs(['FirmarDocumentos/LoginClaveUnica', 'FirmarDocumentos/LoginClaveUnica/*'], 'active')}}">
    <i class="nav-icon fa fa-pencil"></i>
    <p>
      Firma Documentos Digitales
    </p>
  </a>
</li>
@endif