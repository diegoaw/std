@if (auth()->user()->hasPermissionModule('DocumentosDigitales') && auth()->user()->hasPermissionModule('Folios'))
<li class="nav-item">
  <a href="{{route('folios')}}" class="nav-link {{routeIs(['Folios', 'Folios/*'], 'active')}}">
    <i class="nav-icon fa fa-sort-numeric-desc"></i>
    <p>
      Folios Simple
    </p>
  </a>
</li>
@endif