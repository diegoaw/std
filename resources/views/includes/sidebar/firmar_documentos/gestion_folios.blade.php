@if (auth()->user()->hasPermissionModule('DocumentosDigitales') && auth()->user()->hasPermissionModule('Folios'))
<li class="nav-item">
  <a href="{{route('folios2')}}" class="nav-link {{routeIs(['GestionFolios', 'GestionFolios/*'], 'active')}}">
    <i class="nav-icon fa fa-list-ol"></i>
    <p>
      Gestión Folios
    </p>
  </a>
</li>
@endif