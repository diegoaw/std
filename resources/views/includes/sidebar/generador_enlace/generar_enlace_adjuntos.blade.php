@if (auth()->user()->hasPermissionModule('DocumentosDigitales'))
<li class="nav-item">
  <a href="{{route('generar_enlace')}}" class="nav-link {{routeIs(['DocumentosDigitales/GenerarEnlaceAdjunto', 'DocumentosDigitales/GenerarEnlaceAdjunto/*'], 'active')}}">
    <i class="nav-icon fa fa-link"></i>
    <p>
      Generardor Enlaces Adjuntos
    </p>
  </a>
</li>
@endif
@if (auth()->user()->hasPermissionModule('DocumentosDigitales'))
<li class="nav-item">
  <a href="{{route('galeria_firmas')}}" class="nav-link {{routeIs(['DocumentosDigitales/GaleriaFirmas', 'DocumentosDigitales/GaleriaFirmas/*'], 'active')}}">
    <i class="nav-icon fa fa-picture-o"></i> 
    <p>
      Galería Firmas
    </p>
  </a>
</li>
@endif