@if (auth()->user()->hasPermissionModule('IncentivoRetiro'))
<li class="nav-item">
  <a href="{{route('incentivoretiro')}}" class="nav-link {{routeIs(['IncentivoRetiro'], 'active')}}">
    <i class="nav-icon fa fa-folder-open"></i>
    <p>
      Incentivo al Retiro
    </p>
  </a>
</li>
@endif