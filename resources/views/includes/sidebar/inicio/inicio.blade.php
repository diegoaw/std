<li class="nav-item">
  <a href="{{route('inicio')}}" class="nav-link {{routeIs(['Inicio', 'Inicio/*'], 'active')}}">
    <i class="nav-icon fa fa-home"></i>
    <p>
      Inicio
    </p>
  </a>
</li>