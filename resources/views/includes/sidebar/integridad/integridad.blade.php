@if (auth()->user()->hasPermission(['12-01']))
<li class="nav-item">
  <a href="{{route('consulta')}}" class="nav-link {{routeIs(['Consulta'], 'active')}}">
    <i class="nav-icon fa fa-comments-o"></i>
    <p>
      Consultas
    </p>
  </a>
</li>
@endif
@if (auth()->user()->hasPermission(['12-02']))
<li class="nav-item">
  <a href="{{route('denuncia')}}" class="nav-link {{routeIs(['Denuncia'], 'active')}}">
    <i class="nav-icon fa fa-exclamation-circle"></i>
    <p>
      Denuncias
    </p>
  </a>
</li>
@endif