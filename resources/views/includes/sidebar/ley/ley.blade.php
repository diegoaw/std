@if (auth()->user()->hasPermissionModule('Ley'))

<li class="nav-item">
  <a href="{{route('Ley')}}" class="nav-link {{routeIs('Ley', 'active')}}">
    <i class="nav-icon fa fa-file"></i>
    <p>
      Ley 20.594
    </p>
  </a>
</li>

@endif