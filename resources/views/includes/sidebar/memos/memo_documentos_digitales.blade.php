@if (auth()->user()->hasPermissionModule('DocumentosDigitales'))
<li class="nav-item">
  <a href="{{route('registro_memos')}}" class="nav-link {{routeIs(['DocumentosDigitales/Registro/Memorandums', 'DocumentosDigitales/Registro/Memorandums/*'], 'active')}}">
    <i class="nav-icon fa fa-file-text"></i>
    <p>
    Memorándums Electrónicos
    </p>
  </a>
</li>
@endif