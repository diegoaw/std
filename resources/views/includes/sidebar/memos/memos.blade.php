@if (auth()->user()->hasPermissionModule('memos'))

    <li class="nav-item">
      <a href="{{url('Memoss')}}" class="nav-link {{routeIs(['Memoss' , 'Memoss/Filtross'], 'active')}}">
        <i class="fa fa-file-text nav-icon"></i>
        <p>Memos Electrónicos</p>
      </a>
    </li>

@endif
