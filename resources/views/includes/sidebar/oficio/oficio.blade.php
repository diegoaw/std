@if (auth()->user()->hasPermissionModule('Oficios'))

  <li class="nav-item">
      <a href="{{url('Oficioss')}}" class="nav-link {{routeIs(['Oficioss' , 'Oficioss/Filtross'], 'active')}}">
        <i class="fa fa-file-text nav-icon"></i>
        <p>Oficios Electrónicos</p>
      </a>
    </li>
 

@endif
