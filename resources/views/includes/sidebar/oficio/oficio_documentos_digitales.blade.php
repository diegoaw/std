@if (auth()->user()->hasPermissionModule('DocumentosDigitales'))
<li class="nav-item">
  <a href="{{route('registro_ofi')}}" class="nav-link {{routeIs(['DocumentosDigitales/Registro/Oficios', 'DocumentosDigitales/Registro/Oficios/*'], 'active')}}">
    <i class="nav-icon fa fa-file-text"></i>
    <p>
      Oficios Electrónicos
    </p>
  </a>
</li>
@endif