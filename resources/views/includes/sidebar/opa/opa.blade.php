
<li class="nav-item">
  <a href="{{route('opa')}}" class="nav-link {{routeIs(['GestionDocumental', 'GestionDocumental/*'], 'active')}}">
    <i class="nav-icon fa fa-exchange"></i>
    <p>
      Entrada/Salida Físicos
    </p>
  </a>
</li>
