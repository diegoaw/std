@if (auth()->user()->hasPermissionModule('Patrocinio'))
<li class="nav-item">
  <a href="{{route('patrocinio')}}" class="nav-link {{routeIs('Patrocinio', 'active')}}">
    <i class="nav-icon fa fa-users"></i>
    <p>
      Solicitud de Patrocinio
    </p>
  </a>
</li>
@endif
