@if (auth()->user()->hasPermissionModule('Protocolo'))
<li class="nav-item">
  <a href="{{route('protocolo')}}" class="nav-link {{routeIs('Protocolo', 'active')}}">
    <i class="nav-icon fa fa-commenting-o"></i>
    <p>
      Consultas sobre Protocolo
    </p>
  </a>
</li>
@endif
