@if (auth()->user()->hasPermissionModule('Reclamos'))
<li class="nav-item">
  <a href="{{route('reclamos')}}" class="nav-link {{routeIs('Reclamos', 'active')}}">
    <i class="nav-icon fa fa-comments"></i>
    <p>
      Reclamos de Bienes o Servicios
    </p>
  </a>
</li>
@endif
