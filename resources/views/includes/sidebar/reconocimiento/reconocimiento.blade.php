@if (auth()->user()->hasPermissionModule('Reconocimiento'))
<li class="nav-item">
  <a href="{{route('reconocimiento')}}" class="nav-link {{routeIs('Reconocimiento', 'active')}}">
    <i class="nav-icon fa fa-file-text-o"></i>
    <p>
      Solicitud Reconocimiento
    </p>
  </a>
</li>
@endif
