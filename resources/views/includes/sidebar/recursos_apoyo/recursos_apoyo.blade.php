@if (auth()->user()->hasPermissionModule('DocumentosDigitales'))
<li class="nav-item">
  <a href="{{route('recursos_apoyo')}}" class="nav-link {{routeIs(['DocumentosDigitales/RecursosDeApoyo', 'DocumentosDigitales/RecursosDeApoyo/*'], 'active')}}">
    <i class="nav-icon fa fa-info-circle"></i>
    <p>
      Recursos De Apoyo
    </p>
  </a>
</li>
@endif