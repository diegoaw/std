
<li class="nav-item">
  <a href="{{route('dashboardrequecompras')}}" class="nav-link {{routeIs(['RequerimientosCompras/Dashboard/*'], 'active')}}">
    <i class="nav-icon fa fa-desktop"></i>
    <p>
      Dashboard Requerimientos Compras
    </p>
  </a>
</li>
<li class="nav-item">
  <a href="{{route('requecompras')}}" class="nav-link {{routeIs(['RequerimientosCompras'], 'active')}}">
    <i class="nav-icon fa fa-cart-plus"></i>
    <p>
      Requerimientos de Compras
    </p>
  </a>
</li>
<li class="nav-item">
  <a href="{{route('acta')}}" class="nav-link {{routeIs(['acta'], 'active')}}">
    <i class="nav-icon fa fa-file"></i>
    <p>
      Actas de Recepción conforme
    </p>
  </a>
</li>
