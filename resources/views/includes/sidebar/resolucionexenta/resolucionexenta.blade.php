@if (auth()->user()->hasPermissionModule('ResolucionesExentas'))

    <li class="nav-item">
      <a href="{{url('ResolucionesExentas')}}" class="nav-link {{routeIs(['ResolucionesExentas' , 'ResolucionesExentas/Filtros'], 'active')}}">
        <i class="fa fa-file-text nav-icon"></i>
        <p>Resoluciones Exentas Electrónicas</p>
      </a>
    </li>

@endif
