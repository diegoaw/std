
@if (auth()->user()->hasPermissionModule('DocumentosDigitales'))
<li class="nav-item">
  <a href="{{route('registro_resex')}}" class="nav-link {{routeIs(['DocumentosDigitales/Registro/ResolucionesExentas', 'DocumentosDigitales/Registro/ResolucionesExentas/*'], 'active')}}">
    <i class="nav-icon fa fa-file-text"></i>
    <p>
      Resoluciones Exentas Electrónicas
    </p>
  </a>
</li>
@endif