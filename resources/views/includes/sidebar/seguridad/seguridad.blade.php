@if (auth()->user()->hasPermissionModule('seguridad'))
  <!-- Seguridad -->
    <li class="nav-item has-treeview {{ routeIs('Seguridad/*') }}">
      <a href="#" class="nav-link">
        <i class="nav-icon fa fa-lock"></i>
        <p>Seguridad<i class="right fa fa-angle-left"></i></p>
      </a>
      <ul class="nav nav-treeview">
        
          <li class="nav-item has-treeview {{ routeIs('Seguridad/Usuarios/*') }}">
            <a href="#" class="nav-link {{routeIs('Seguridad/Usuarios/*','active')}}">
              <i class="nav-icon fa fa-users"></i>
              <p><small>Usuarios</small>
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              
              <li class="nav-item">
                <a href="{{url('Seguridad/Usuarios/Activos')}}" class="nav-link {{ routeIs(['Seguridad/Usuarios/Activos', 'Seguridad/Usuarios/Activos/Crear', 'Seguridad/Usuarios/Activos/Edit/*', 'Seguridad/Usuarios/Activos/CambiarContrasena/*'], 'active') }}">
                  <i class="fa fa-lock nav-icon"></i>
                  <p><small>Listado</small></p>
                </a>
              </li>
             
              <!-- <li class="nav-item">
                <a href="{{url('Seguridad/Usuarios/Inactivos')}}" class="nav-link {{ routeIs(['Seguridad/Usuarios/Inactivos', 'Seguridad/Usuarios/Inactivos/Edit/*', 'Seguridad/Usuarios/Inactivos/CambiarContrasena/*'], 'active') }}">
                  <i class="fa fa-lock nav-icon"></i>
                  <p>Inactivos</p>
                </a>
              </li> -->
             
             <!--  <li class="nav-item">
                <a href="{{url('Seguridad/Usuarios/Eliminados')}}" class="nav-link {{ routeIs('Seguridad/Usuarios/Eliminados', 'active') }}">
                  <i class="fa fa-lock nav-icon"></i>
                  <p>Eliminados</p>
                </a>
              </li>-->
              
            </ul>
          </li>
        
          <li class="nav-item">
            <a href="{{url('Seguridad/RolesRegistrados')}}" class="nav-link {{ routeIs(['Seguridad/RolesRegistrados', 'Seguridad/RolesRegistrados/*'], 'active') }}">
              <i class="fa fa-lock nav-icon"></i>
              <p><small>Roles registrados</small></p>
            </a>
          </li>
        
         <!--  <li class="nav-item has-treeview">
            <a href="{{url('Seguridad/Reportes')}}" class="nav-link {{ routeIs(['Seguridad/Reportes','Seguridad/Reportes/*'], 'active') }}">
              <i class="fa fa-lock nav-icon"></i>
              <p>Reportes</p>
            </a>
          </li>
        
          <li class="nav-item has-treeview {{ routeIs('Seguridad/Contrasena/*') }}">
            <a href="#" class="nav-link {{routeIs('Seguridad/Contrasena/*','active')}}">
              <i class="nav-icon fa fa-lock"></i>
              <p>contraseña
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('Seguridad/Contrasena/Complejidad')}}" class="nav-link {{ routeIs(['Seguridad/Contrasena/Complejidad','Seguridad/Contrasena/Complejidad/*'], 'active') }}">
                  <i class="fa fa-lock nav-icon"></i>
                  <p>Complejidad</p>
                </a>
              </li>
            </ul>
          </li> -->
        
      </ul>
    </li>
@endif
  <!-- /.Seguridad -->

