@if (auth()->user()->hasPermissionModule('Siac'))
<li class="nav-item">
  <a href="{{route('siac')}}" class="nav-link {{routeIs('SIAC', 'active')}}">
    <i class="nav-icon fa fa-comments"></i>
    <p>
    SIAC
    </p>
  </a>
</li>
@endif