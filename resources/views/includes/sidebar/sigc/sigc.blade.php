
@if (auth()->user()->hasPermissionModule('SIGC') || auth()->user()->hasPermission(['09-07']))

<li class="nav-item has-treeview {{routeIs(['SIGC/Calendario' , 'SIGC/Indicadores', 'SIGC/Mediciones', 'SIGC/Mediciones/Revision', 'SIGC/Indicadores/Usuario', 'SIGC/Inicio'], 'active')}}">
  <a href="#" class="nav-link">
    <i class="nav-icon fa fa-bar-chart"></i>
    <p>SIGC<i class="right fa fa-angle-left"></i></p>
  </a>
  @if (auth()->user()->hasPermissionModule('SIGC'))
  @if (auth()->user()->hasPermission(['09-09']) || auth()->user()->hasPermission(['09-13']) )
  <ul class="nav nav-treeview {{routeIs(['SIGC/Inicio'], 'active')}}">
    <li class="nav-item">
      <a href="{{url('SIGC/Inicio')}}" class="nav-link {{routeIs(['SIGC/Inicio'], 'active')}}">
        <i class="fa fa-bar-chart nav-icon"></i>
        <p>SIGC Inicio</p>
      </a>
    </li>
  </ul> 
  @endif
  






  @if (auth()->user()->hasPermission(['09-14' , '09-13'])) 
  <ul class="nav nav-treeview {{routeIs(['SIGC/ProgramasSociales/SCCD', 'SIGC/ProgramasSociales/SNCD', 'SIGC/ProgramasSociales/FDRCP', 'SIGC/ProgramasSociales/ACD', 'SIGC/ProgramasSociales/FONDEPORTE', 'SIGC/ProgramasSociales/ProgramasUsuario', 'SIGC/ProgramasSociales/ProgramasMesReportes', 'SIGC/ProgramasSociales/Gastos', 'SIGC/ProgramasSociales/Poblacion', 'SIGC/ProgramasSociales/Componentes', 'SIGC/ProgramasSociales/Programas','SIGC/ProgramasSociales/DPS', 'SIGC/ProgramasSociales/CEM','SIGC/ProgramasSociales/Inicio','SIGC/ProgramasSociales/CalendarioEventos','SIGC/ProgramasSociales/Eventos','SIGC/ProgramasSociales/Indicadores'], 'active')}}"> 
  <li class="nav-item">
      <a href="{{url('SIGC/ProgramasSociales/SCCD', 'SIGC/ProgramasSociales/SNCD', 'SIGC/ProgramasSociales/FDRCP', 'SIGC/ProgramasSociales/ACD', 'SIGC/ProgramasSociales/FONDEPORTE', 'SIGC/ProgramasSociales/ProgramasUsuario', 'SIGC/ProgramasSociales/ProgramasMesReportes', 'SIGC/ProgramasSociales/Gastos', 'SIGC/ProgramasSociales/Poblacion', 'SIGC/ProgramasSociales/Componentes', 'SIGC/ProgramasSociales/Programas', 'SIGC/ProgramasSociales/DPS', 'SIGC/ProgramasSociales/CEM','SIGC/ProgramasSociales/Inicio','SIGC/ProgramasSociales/CalendarioEventos','SIGC/ProgramasSociales/Eventos','SIGC/ProgramasSociales/Indicadores','active')}}" class="nav-link {{routeIs(['SIGC/ProgramasSociales/SCCD', 'SIGC/ProgramasSociales/SNCD', 'SIGC/ProgramasSociales/FDRCP', 'SIGC/ProgramasSociales/ACD', 'SIGC/ProgramasSociales/FONDEPORTE', 'SIGC/ProgramasSociales/ProgramasUsuario', 'SIGC/ProgramasSociales/ProgramasMesReportes', 'SIGC/ProgramasSociales/Gastos', 'SIGC/ProgramasSociales/Poblacion', 'SIGC/ProgramasSociales/Componentes', 'SIGC/ProgramasSociales/Programas', 'SIGC/ProgramasSociales/DPS', 'SIGC/ProgramasSociales/CEM','SIGC/ProgramasSociales/Inicio','SIGC/ProgramasSociales/CalendarioEventos','SIGC/ProgramasSociales/Eventos','SIGC/ProgramasSociales/Indicadores'], 'active')}}">
        <i class="fa fa-users nav-icon"></i>
        <p>Programas Sociales<i class="right fa fa-angle-left"></i></p>
      </a>
      
      <ul class="nav nav-treeview {{routeIs(['SIGC/ProgramasSociales/Inicio'], 'active')}}">
        <li class="nav-item">
          <a href="{{url('SIGC/ProgramasSociales/Inicio')}}" class="nav-link {{routeIs(['SIGC/ProgramasSociales/Inicio'], 'active')}}">
            <p>Inicio</p>
          </a>
        </li>
      </ul> 
      
      <ul class="nav nav-treeview {{routeIs(['SIGC/ProgramasSociales/DPS'], 'active')}}">
        <li class="nav-item">
          <a href="{{url('SIGC/ProgramasSociales/ProgramasUsuario')}}" class="nav-link {{routeIs(['SIGC/ProgramasSociales/ProgramasUsuario'], 'active')}}">
            <p>Reportes</p>
          </a>
        </li>
      </ul> 
      
      <ul class="nav nav-treeview {{routeIs(['SIGC/ProgramasSociales/CEM'], 'active')}}">
        <li class="nav-item">
          <a href="{{url('SIGC/ProgramasSociales/CEM')}}" class="nav-link {{routeIs(['SIGC/ProgramasSociales/CEM'], 'active')}}">
            <p>CEM</p>
          </a>
        </li>
      </ul> 

      <ul class="nav nav-treeview {{routeIs(['SIGC/ProgramasSociales/DPS'], 'active')}}">
        <li class="nav-item">
          <a href="{{url('SIGC/ProgramasSociales/DPS')}}" class="nav-link {{routeIs(['SIGC/ProgramasSociales/DPS'], 'active')}}">
            <p>DPS</p>
          </a>
        </li>
      </ul> 

      <ul class="nav nav-treeview {{routeIs(['SIGC/ProgramasSociales/FONDEPORTE'], 'active')}}">
        <li class="nav-item">
          <a href="{{url('SIGC/ProgramasSociales/FONDEPORTE')}}" class="nav-link {{routeIs(['SIGC/ProgramasSociales/FONDEPORTE'], 'active')}}">
            <p>FONDEPORTE</p>
          </a>
        </li>
      </ul>

      <ul class="nav nav-treeview {{routeIs(['SIGC/ProgramasSociales/ACD'], 'active')}}">
        <li class="nav-item">
          <a href="{{url('SIGC/ProgramasSociales/ACD')}}" class="nav-link {{routeIs(['SIGC/ProgramasSociales/ACD'], 'active')}}">
            <p>ACD</p>
          </a>
        </li>
      </ul>

      <ul class="nav nav-treeview {{routeIs(['SIGC/ProgramasSociales/FDRCP'], 'active')}}">
        <li class="nav-item">
          <a href="{{url('SIGC/ProgramasSociales/FDRCP')}}" class="nav-link {{routeIs(['SIGC/ProgramasSociales/FDRCP'], 'active')}}">
            <p>FDRCP</p>
          </a>
        </li>
      </ul>

      <ul class="nav nav-treeview {{routeIs(['SIGC/ProgramasSociales/SNCD'], 'active')}}">
        <li class="nav-item">
          <a href="{{url('SIGC/ProgramasSociales/SNCD')}}" class="nav-link {{routeIs(['SIGC/ProgramasSociales/SNCD'], 'active')}}">
            <p>SNCD</p>
          </a>
        </li>
      </ul>

      <ul class="nav nav-treeview {{routeIs(['SIGC/ProgramasSociales/SCCD'], 'active')}}">
        <li class="nav-item">
          <a href="{{url('SIGC/ProgramasSociales/SCCD')}}" class="nav-link {{routeIs(['SIGC/ProgramasSociales/SCCD'], 'active')}}">
            <p>SCCD</p>
          </a>
        </li>
      </ul>
      @if (auth()->user()->hasPermission(['09-14'])) 
      <ul class="nav nav-treeview {{ routeIs('SIGC/ProgramasSociales/ProgramasMesReportes', 'SIGC/ProgramasSociales/Gastos', 'SIGC/ProgramasSociales/Poblacion','SIGC/ProgramasSociales/Componentes','SIGC/ProgramasSociales/Programas','SIGC/ProgramasSociales/CalendarioEventos','SIGC/ProgramasSociales/Eventos','SIGC/ProgramasSociales/Indicadores','active')}}">
        <li class="nav-item has-treeview {{ routeIs('SIGC/ProgramasSociales/ProgramasMesReportes', 'SIGC/ProgramasSociales/Gastos', 'SIGC/ProgramasSociales/Poblacion','SIGC/ProgramasSociales/Componentes','SIGC/ProgramasSociales/Programas','SIGC/ProgramasSociales/CalendarioEventos','SIGC/ProgramasSociales/Eventos','SIGC/ProgramasSociales/Indicadores','active')}}">
          <a href="#" class="nav-link">
              <p>Configuración<i class="right fa fa-angle-left"></i></p>
          </a>

          <ul class="nav nav-treeview {{routeIs(['SIGC/ProgramasSociales/CalendarioEventos'], 'active')}}">
            <li class="nav-item">
              <a href="{{url('SIGC/ProgramasSociales/CalendarioEventos')}}" class="nav-link {{routeIs(['SIGC/ProgramasSociales/CalendarioEventos'], 'active')}}">
                <p>- Calendario Eventos</p>
              </a>
            </li>
          </ul>  

          <ul class="nav nav-treeview {{routeIs(['SIGC/ProgramasSociales/Eventos'], 'active')}}">
            <li class="nav-item">
              <a href="{{url('SIGC/ProgramasSociales/Eventos')}}" class="nav-link {{routeIs(['SIGC/ProgramasSociales/Eventos'], 'active')}}">
                <p>- Eventos</p>
              </a>
            </li>
          </ul> 

          <ul class="nav nav-treeview {{routeIs(['SIGC/ProgramasSociales/ProgramasMesReportes'], 'active')}}">
            <li class="nav-item">
              <a href="{{url('SIGC/ProgramasSociales/ProgramasMesReportes')}}" class="nav-link {{routeIs(['SIGC/ProgramasSociales/ProgramasMesReportes'], 'active')}}">
                <p>- Mes Reporte</p>
              </a>
            </li>
          </ul> 

          <ul class="nav nav-treeview {{routeIs(['SIGC/ProgramasSociales/Programas'], 'active')}}">
            <li class="nav-item">
              <a href="{{url('SIGC/ProgramasSociales/Programas')}}" class="nav-link {{routeIs(['SIGC/ProgramasSociales/Programas'], 'active')}}">
                <p>- Programas</p>
              </a>
            </li>
          </ul>

          <ul class="nav nav-treeview {{routeIs(['SIGC/ProgramasSociales/Componentes'], 'active')}}">
            <li class="nav-item">
              <a href="{{url('SIGC/ProgramasSociales/Componentes')}}" class="nav-link {{routeIs(['SIGC/ProgramasSociales/Componentes'], 'active')}}">
                <p>- Componentes</p>
              </a>
            </li>
          </ul>

          <ul class="nav nav-treeview {{routeIs(['SIGC/ProgramasSociales/Indicadores'], 'active')}}">
            <li class="nav-item">
              <a href="{{url('SIGC/ProgramasSociales/Indicadores')}}" class="nav-link {{routeIs(['SIGC/ProgramasSociales/Indicadores'], 'active')}}">
                <p>- Indicadores</p>
              </a>
            </li>
          </ul> 

          <ul class="nav nav-treeview {{routeIs(['SIGC/ProgramasSociales/Poblacion'], 'active')}}">
            <li class="nav-item">
              <a href="{{url('SIGC/ProgramasSociales/Poblacion')}}" class="nav-link {{routeIs(['SIGC/ProgramasSociales/Poblacion'], 'active')}}">
                <p>- Población</p>
              </a>
            </li>
          </ul>

          <ul class="nav nav-treeview {{routeIs(['SIGC/ProgramasSociales/Gastos'], 'active')}}">
            <li class="nav-item">
              <a href="{{url('SIGC/ProgramasSociales/Gastos')}}" class="nav-link {{routeIs(['SIGC/ProgramasSociales/Gastos'], 'active')}}">
                <p>- Gastos</p>
              </a>
            </li>
          </ul>
          </li>
      </ul> 
      @endif


    </li>
  </ul> 
  @endif












  @if (auth()->user()->hasPermission(['09-01']))
  <ul class="nav nav-treeview {{routeIs(['SIGC/Indicadores/Usuario'], 'active')}}">
    <li class="nav-item">
      <a href="{{url('SIGC/Indicadores/Usuario')}}" class="nav-link {{routeIs(['SIGC/Indicadores/Usuario'], 'active')}}">
        <i class="fa fa-sliders nav-icon"></i>
        <p>Reportar</p>
      </a>
    </li>
  </ul>
  @endif
  @if (auth()->user()->hasPermission(['09-02']))
  <ul class="nav nav-treeview {{routeIs(['SIGC/Mediciones/Revision'], 'active')}}">
    <li class="nav-item">
      <a href="{{url('SIGC/Mediciones/Revision')}}" class="nav-link {{routeIs(['SIGC/Mediciones/Revision'], 'active')}}">
        <i class="fa fa-check-square-o nav-icon"></i>
        <p>Revisión</p>
      </a>
    </li>
  </ul>
  @endif
  @if (auth()->user()->hasPermission(['09-10']))
  <ul class="nav nav-treeview {{routeIs(['SIGC/Calendario'], 'active')}}">
    <li class="nav-item">
      <a href="{{url('SIGC/Calendario')}}" class="nav-link {{routeIs(['SIGC/Calendario'], 'active')}}">
        <i class="fa fa-calendar-check-o nav-icon"></i>
        <p>Calendario</p>
      </a>
    </li>
  </ul>
  @endif
  @if (auth()->user()->hasPermission(['09-11']))
   <ul class="nav nav-treeview {{routeIs(['SIGC/Indicadores'], 'active')}}">
    <li class="nav-item">
      <a href="{{url('SIGC/Indicadores')}}" class="nav-link  {{routeIs(['SIGC/Indicadores'], 'active')}}">
        <i class="fa fa-list nav-icon"></i>
        <p>Indicadores</p>
      </a>
    </li>
  </ul>
  @endif
  @if (auth()->user()->hasPermission(['09-12']))
  <ul class="nav nav-treeview {{routeIs(['SIGC/Mediciones'], 'active')}}">
    <li class="nav-item">
      <a href="{{url('SIGC/Mediciones')}}" class="nav-link  {{routeIs(['SIGC/Mediciones'], 'active')}}">
        <i class="fa fa-line-chart nav-icon"></i>
        <p>Mediciones</p>
      </a>
    </li>
  </ul>
  @endif
  @endif

  @if (auth()->user()->hasPermission(['09-07']))
  <ul class="nav nav-treeview {{routeIs(['SIGC/Reportabilidad'], 'active')}}">
    <li class="nav-item">
      <a href="{{url('SIGC/Reportabilidad')}}" class="nav-link  {{routeIs(['SIGC/Reportabilidad'], 'active')}}">
        <i class="fa fa-file-text-o nav-icon"></i>
        <p>Estado Reportes</p>
      </a>
    </li>
  </ul>
  <ul class="nav nav-treeview {{ routeIs('SIGC/Ambitos','SIGC/Categorias','SIGC/Equipos','SIGC/Dimensiones','SIGC/Productos','SIGC/Tipos','SIGC/Unidades','SIGC/Documentos','SIGC/Usuarios','SIGC/DocumentoTipos','SIGC/EquipoMiembroRoles','active')}}">
    <li class="nav-item has-treeview {{ routeIs('SIGC/Ambitos','SIGC/Categorias','SIGC/Equipos','SIGC/Dimensiones','SIGC/Productos','SIGC/Tipos','SIGC/Unidades', 'SIGC/Documentos','SIGC/Usuarios','SIGC/DocumentoTipos','SIGC/EquipoMiembroRoles','active')}}">
      <a href="#" class="nav-link">
        <i class="fa fa-cog nav-icon"></i>
          <p>Configuración<i class="right fa fa-angle-left"></i></p>
      </a>

    <ul class="nav nav-treeview {{routeIs(['SIGC/Ambitos'], 'active')}}">
      <li class="nav-item">
      <a href="{{url('SIGC/Ambitos')}}" class="nav-link  {{routeIs(['SIGC/Ambitos'], 'active')}}">
        <p>Ambitos</p>
      </a>
      </li>
    </ul>

    <ul class="nav nav-treeview {{routeIs(['SIGC/Categorias'], 'active')}}">
      <li class="nav-item">
      <a href="{{url('SIGC/Categorias')}}" class="nav-link  {{routeIs(['SIGC/Categorias'], 'active')}}">
        <p>Categorías</p>
      </a>
      </li>
    </ul>

    <ul class="nav nav-treeview {{routeIs(['SIGC/Dimensiones'], 'active')}}">
      <li class="nav-item">
      <a href="{{url('SIGC/Dimensiones')}}" class="nav-link  {{routeIs(['SIGC/Dimensiones'], 'active')}}">
        <p>Dimensiones</p>
      </a>
      </li>
    </ul>

    <ul class="nav nav-treeview {{routeIs(['SIGC/Documentos'], 'active')}}">
      <li class="nav-item">
      <a href="{{url('SIGC/Documentos')}}" class="nav-link  {{routeIs(['SIGC/Documentos'], 'active')}}">
        <p>Documentos</p>
      </a>
      </li>
    </ul>

    <ul class="nav nav-treeview {{routeIs(['SIGC/DocumentoTipos'], 'active')}}">
      <li class="nav-item">
      <a href="{{url('SIGC/DocumentoTipos')}}" class="nav-link  {{routeIs(['SIGC/DocumentoTipos'], 'active')}}">
        <p>Documento Tipos</p>
      </a>
      </li>
    </ul>

    <ul class="nav nav-treeview {{routeIs(['SIGC/Equipos'], 'active')}}">
      <li class="nav-item">
      <a href="{{url('SIGC/Equipos')}}" class="nav-link  {{routeIs(['SIGC/Equipos'], 'active')}}">
        <p>Equipos</p>
      </a>
      </li>
    </ul>

    <ul class="nav nav-treeview {{routeIs(['SIGC/EquipoMiembroRoles'], 'active')}}">
      <li class="nav-item">
      <a href="{{url('SIGC/EquipoMiembroRoles')}}" class="nav-link  {{routeIs(['SIGC/EquipoMiembroRoles'], 'active')}}">
        <p>Equipo Miembro Roles</p>
      </a>
      </li>
    </ul>

    <ul class="nav nav-treeview {{routeIs(['SIGC/Productos'], 'active')}}">
      <li class="nav-item">
      <a href="{{url('SIGC/Productos')}}" class="nav-link  {{routeIs(['SIGC/Productos'], 'active')}}">
        <p>Productos</p>
      </a>
      </li>
    </ul>

    <ul class="nav nav-treeview {{routeIs(['SIGC/Tipos'], 'active')}}">
      <li class="nav-item">
      <a href="{{url('SIGC/Tipos')}}" class="nav-link  {{routeIs(['SIGC/Tipos'], 'active')}}">
        <p>Tipos</p>
      </a>
      </li>
    </ul>

    <ul class="nav nav-treeview {{routeIs(['SIGC/Unidades'], 'active')}}">
      <li class="nav-item">
      <a href="{{url('SIGC/Unidades')}}" class="nav-link  {{routeIs(['SIGC/Unidades'], 'active')}}">
        <p>Unidades</p>
      </a>
      </li>
    </ul>

    <ul class="nav nav-treeview {{routeIs(['SIGC/Usuarios'], 'active')}}">
      <li class="nav-item">
      <a href="{{url('SIGC/Usuarios')}}" class="nav-link  {{routeIs(['SIGC/Usuarios'], 'active')}}">
        <p>Usuarios</p>
      </a>
      </li>
    </ul>

    </li>
  </ul>
  @endif
</li>

@endif
