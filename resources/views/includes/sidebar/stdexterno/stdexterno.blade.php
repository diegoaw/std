@if (auth()->user()->hasPermissionModule('Ley') || auth()->user()->hasPermissionModule('Reconocimiento') || auth()->user()->hasPermissionModule('Patrocinio') || auth()->user()->hasPermissionModule('Reclamos') || auth()->user()->hasPermissionModule('Protocolo') || auth()->user()->hasPermissionModule('Cevs'))

    <li class="nav-item has-treeview {{ routeIs('Ley/*') }}">
      <a href="#" class="nav-link">
        <i class="nav-icon fa fa-tasks"></i>
        <p>STD Externo<i class="right fa fa-angle-left"></i></p>
      </a>
      <ul class="nav nav-treeview">


          <li class="nav-item has-treeview {{ routeIs('Ley/*') }}">
            <a href="#" class="nav-link {{routeIs('Ley/*','active')}}">
              <i class="nav-icon fa fa-file-text"></i>
              <p><small>Básicos Trámites Externos</small>
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">

            <small>@include('includes.sidebar.reconocimiento.reconocimiento')</small>
            <small>@include('includes.sidebar.patrocinio.patrocinio')</small>
            <small>@include('includes.sidebar.reclamos.reclamos')</small>
            <small>@include('includes.sidebar.siac.siac')</small>
            <small>@include('includes.sidebar.protocolo.protocolo')</small>
            <small>@include('includes.sidebar.cevs.cevs')</small>
            <small>@include('includes.sidebar.ley.ley')</small>

            </ul>
          </li>


      </ul>
    </li>

  @endif
