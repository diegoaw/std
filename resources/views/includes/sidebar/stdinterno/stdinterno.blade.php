@if (auth()->user()->hasPermissionModule('DocumentosDigitales')||auth()->user()->hasPermissionModule('seguridad')||auth()->user()->hasPermissionModule('tramites')||auth()->user()->hasPermissionModule('derivaciones')||auth()->user()->hasPermissionModule('memos')||auth()->user()->hasPermissionModule('xml')||auth()->user()->hasPermissionModule('ResolucionesExentas')||auth()->user()->hasPermissionModule('Oficios')||auth()->user()->hasPermissionModule('FirmarDocumentos')||auth()->user()->hasPermissionModule('SigcConfig') || auth()->user()->hasPermissionModule('SigcReportador'))

    <li class="nav-item has-treeview {{ routeIs('Memos/*','Tramites/*','Trazabilidad/*','FirmarDocumentos/*','Oficios/*','Memos/Colaboraciones/*','Oficios/Colaboraciones/*','ResolucionesExentas/*','ResolucionesExentas/Colaboraciones/*','XMLPDF/*', 'GestionDocumental/*', 'Maternidad/*', 'DocumentosDigitales/*')}}">
      <a href="#" class="nav-link">
        <i class="nav-icon fa fa-tasks"></i>
        <p>STD Interno<i class="right fa fa-angle-left"></i></p>
      </a>
      <ul class="nav nav-treeview">


@if (auth()->user()->hasPermissionModule('DocumentosDigitales'))
          <li class="nav-item has-treeview {{ routeIs('DocumentosDigitales/*') }}">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-file-text-o"></i>
              <p><small>Documentos Digitales</small>
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">

            <!-- <small>@include('includes.sidebar.recursos_apoyo.recursos_apoyo')</small>  -->

            <small>@include('includes.sidebar.oficio.oficio_documentos_digitales')</small>

            <small>@include('includes.sidebar.resolucionexenta.resolucionexenta_documentos_digitales')</small>

            <small>@include('includes.sidebar.memos.memo_documentos_digitales')</small>

            <small>@include('includes.sidebar.decretos.decretos_documentos_digitales')</small>

            <small>@include('includes.sidebar.colaboraciones_pendientes.colaboraciones_documentos_digitales')</small>

            <small>@include('includes.sidebar.generador_enlace.generar_enlace_adjuntos')</small>

            <small>@include('includes.sidebar.firmar_documentos.firmar_documentos_digitales')</small>

            <small>@include('includes.sidebar.firmar_documentos.folios_simple')</small>

            <small>@include('includes.sidebar.firmar_documentos.gestion_folios')</small>

            </ul>
          </li>
@endif

@if (auth()->user()->hasPermissionModule('FirmarDocumentos'))
<li class="nav-item">
  <a href="{{route('firmarDocumentos')}}" class="nav-link {{routeIs('FirmarDocumentos', 'active')}}">
    <i class="nav-icon fa fa-pencil"></i>
    <p><small>
      Firma Avanzada SEGPRES
    </small></p>
  </a>
</li>
@endif


@if (auth()->user()->hasPermissionModule('tramites') || auth()->user()->hasPermissionModule('derivaciones'))
          <li class="nav-item has-treeview {{ routeIs('Tramites/*','HistoricoGdArchivos/*') }}">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-tasks"></i>
              <p><small>Derivación de Documentos</small>
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">


            <small>@include('includes.sidebar.tramites.tramites')</small>

            <small>@include('includes.sidebar.historico_gd.historico_gd')</small>

            </ul>
          </li>
@endif



@if (auth()->user()->hasPermissionModule('FirmarDocumentos') || auth()->user()->hasPermissionModule('memos') || auth()->user()->hasPermissionModule('Oficios'))
          <li class="nav-item has-treeview {{ routeIs('Memos/*','FirmarDocumentos/*','Oficios/*','Memos/Colaboraciones/*','Oficios/Colaboraciones/*') }}">
            <a href="#" class="nav-link {{routeIs('Memos/*','FirmarDocumentos/*','Oficios/*','Memos/Colaboraciones/*','Oficios/Colaboraciones/*','active')}}">
              <i class="nav-icon fa fa-file-text"></i>
              <p><small>Básicos</small>
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">

            <small>@include('includes.sidebar.memos.memos')</small>

            <small>@include('includes.sidebar.oficio.oficio')</small>

            </ul>
          </li>
@endif

@if (auth()->user()->hasPermissionModule('tramites'))
<li class="nav-item">
  <a href="{{route('tickets2')}}" class="nav-link {{routeIs(['TicketsSoporteMINDEP'], 'active')}}">
    <i class="nav-icon fa fa-ticket"></i>
    <p><small>
      Soporte Informático
    </small></p>
  </a>
</li>
@endif

<!-- @if (auth()->user()->hasPermissionModule('ResolucionesExentas'))
          <li class="nav-item has-treeview {{ routeIs('ResolucionesExentas/*','ResolucionesExentas/Colaboraciones/*') }}">
            <a href="#" class="nav-link {{routeIs('ResolucionesExentas/*','ResolucionesExentas/Colaboraciones/*','active')}}">
              <i class="nav-icon fa fa-file-text"></i>
              <p><small>Decisión</small>
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">


            <small>@include('includes.sidebar.resolucionexenta.resolucionexenta')</small>

            </ul>
          </li>
@endif -->

<small>@include('includes.sidebar.traslados.traslados')</small>

@if (auth()->user()->hasPermissionModule('xml'))
        <li class="nav-item has-treeview {{ routeIs('XMLPDF/*') }}">
            <a href="#" class="nav-link {{routeIs('XMLPDF/*','active')}}">
              <i class="nav-icon fa fa-shopping-cart"></i>
              <p><small>Compras</small>
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">


            <small>@include('includes.sidebar.xmlapdf.xmlapdf')</small>
            <small>@include('includes.sidebar.requerimientos_compras.requerimientos_compras')</small>

            </ul>
          </li>
@endif
@if (auth()->user()->hasPermission(['11-01']))
          <li class="nav-item has-treeview {{ routeIs('GestionDocumental/*') }}">
              <a href="#" class="nav-link {{routeIs('GestionDocumental/*','active')}}">
                <i class="nav-icon fa fa-clipboard"></i>
                <p><small>Gestión Documental</small>
                  <i class="right fa fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">

              <small>@include('includes.sidebar.opa.opa')</small>

              </ul>
            </li>
@endif

@if (auth()->user()->hasPermissionModule('Integridad'))
        <li class="nav-item has-treeview {{ routeIs('Consulta/*','Denuncia/*') }}">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-users"></i>
              <p><small>Sistema Integridad</small>
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">

            <small>@include('includes.sidebar.integridad.integridad')</small>

            </ul>
          </li>
@endif

<small>@include('includes.sidebar.incentivo_retiro.incentivo_retiro')</small>

@if (auth()->user()->hasPermissionModule('Beneficios'))
        <li class="nav-item has-treeview {{ routeIs('Maternidad/*') }}">
            <a href="#" class="nav-link {{routeIs('Maternidad/*','active')}}">
              <i class="nav-icon fa fa-handshake-o"></i>
              <p><small>Sistema de Beneficios</small>
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">


            <small>@include('includes.sidebar.beneficios.maternidad')</small>
            <small>@include('includes.sidebar.beneficios.escolaridad')</small>

            </ul>
          </li>
@endif


      </ul>
    </li>
@endif
