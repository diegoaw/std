@if (auth()->user()->hasPermissionModule('Tickets'))
<li class="nav-item">
  <a href="{{route('tickets')}}" class="nav-link {{routeIs('TicketsMINDEP', 'active')}}">
    <i class="nav-icon fa fa-ticket"></i>
    <p>
    Tickets MINDEP
    </p>
  </a>
</li>
@endif