@if (auth()->user()->hasPermissionModule('tramites'))
<li class="nav-item">
  <a href="{{route('tramitess')}}" class="nav-link {{routeIs(['Tramitess' , 'Tramitess/Filtross'], 'active')}}">
    <i class="nav-icon fa fa-file-text"></i>
    <p>
      Derivación de Documentos
    </p>
  </a>
</li>
@endif
