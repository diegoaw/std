<li class="nav-item">
  <a href="{{route('transparencia_documentos_gestiones')}}" class="nav-link {{routeIs(['Transparencia/DocumentosGestiones', 'Transparencia/DocumentosGestiones/*'], 'active')}}">
    <i class="nav-icon fa fa-file"></i>
    <p>
        Documentos Gestiones
    </p>
  </a>
</li>