
<li class="nav-item">
  <a href="{{route('periodosfechas')}}" class="nav-link {{routeIs(['Transparencia/PeriodosFechas', 'Transparencia/PeriodosFechas/*'], 'active')}}">
    <i class="nav-icon fa fa-calendar"></i>
    <p>
        Fechas Periodos
    </p>
  </a>
</li>