<li class="nav-item">
  <a href="{{route('transparencia_generar_enlace')}}" class="nav-link {{routeIs(['Transparencia/GenerarEnlace', 'Transparencia/GenerarEnlace/*'], 'active')}}">
    <i class="nav-icon fa fa-link"></i>
    <p>
      Generar Enlace
    </p>
  </a>
</li>