<li class="nav-item">
  <a href="{{route('transparencia_items')}}" class="nav-link {{routeIs(['Transparencia/Items', 'Transparencia/Items/*'], 'active')}}">
    <i class="nav-icon fa fa-list"></i>
    <p>
      Items
    </p>
  </a>
</li>