<li class="nav-item">
  <a href="{{route('transparencia_materias')}}" class="nav-link {{routeIs(['Transparencia/Materias', 'Transparencia/Materias/*'], 'active')}}">
    <i class="nav-icon fa fa-th-list"></i>
    <p>
      Materias
    </p>
  </a>
</li>