
<li class="nav-item">
  <a href="{{route('Periodos.index')}}" class="nav-link {{routeIs(['Transparencia/Periodos', 'Transparencia/Periodos/*'], 'active')}}">
    <i class="nav-icon fa fa-calendar-o"></i>
    <p>
        Periodos
    </p>
  </a>
</li>