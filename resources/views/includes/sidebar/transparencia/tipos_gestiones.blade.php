
<li class="nav-item">
  <a href="{{route('transparencia_tipos')}}" class="nav-link {{routeIs(['Transparencia/TiposGestiones', 'Transparencia/TiposGestiones/*'], 'active')}}">
    <i class="nav-icon fa fa-bars"></i>
    <p>
      Tipos Gestiones
    </p>
  </a>
</li>