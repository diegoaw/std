
@if (auth()->user()->hasPermissionModule('Transparencia'))
<li class="nav-item has-treeview {{ routeIs('Transparencia/TiposGestiones/*') }}">
    <a href="#" class="nav-link {{routeIs('Transparencia/TiposGestiones/*','active')}}">
      <i class="nav-icon fa fa-file-text"></i>
        <p>Transparencia
           <i class="right fa fa-angle-left"></i>
        </p>
    </a>

    <!-- <ul class="nav nav-treeview">
        @include('includes.sidebar.transparencia.documentos_gestiones')
    </ul>
    <ul class="nav nav-treeview">
        @include('includes.sidebar.transparencia.tipos_gestiones')
    </ul>
    <ul class="nav nav-treeview">
        @include('includes.sidebar.transparencia.periodos')
    </ul>
    <ul class="nav nav-treeview">
        @include('includes.sidebar.transparencia.fechas_periodos')
    </ul> -->
    <ul class="nav nav-treeview">
        @include('includes.sidebar.transparencia.generar_enlace')
    </ul>

    @if (auth()->user()->hasPermission(['14-01']))
    <ul class="nav nav-treeview">
        @include('includes.sidebar.transparencia.materias')
    </ul>

    <ul class="nav nav-treeview">
        @include('includes.sidebar.transparencia.items')
    </ul>
    @endif
    

</li> 
@endif


