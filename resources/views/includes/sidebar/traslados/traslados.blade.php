@if (auth()->user()->hasPermissionModule('Traslados'))
<li class="nav-item">
  <a href="{{route('traslados')}}" class="nav-link {{routeIs('traslados', 'active')}}">
    <i class="nav-icon fa fa-car"></i>
    <p>
    Solicitud Traslados
    </p>
  </a>
</li>
@endif