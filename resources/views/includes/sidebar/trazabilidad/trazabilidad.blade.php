@if (auth()->user()->hasPermissionModule('derivaciones'))
  <li class="nav-item">
  <a href="{{route('trazabilidad')}}" class="nav-link {{routeIs(['Trazabilidad' , 'Trazabilidad/Filtros'], 'active')}}">
    <i class="nav-icon fa fa-tasks"></i>
    <p>
      Trazabilidad Derivaciones
    </p>
  </a>
</li>
@endif
