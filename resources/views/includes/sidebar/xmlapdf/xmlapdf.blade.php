@if (auth()->user()->hasPermissionModule('xml'))
   <li class="nav-item">
  <a href="{{route('xmlpdf')}}" class="nav-link {{routeIs(['XMLPDF' , 'XMLPDF/*'], 'active')}}">
    <i class="nav-icon fa fa-file-text"></i>
    <p>
      Visualizar XML
    </p>
  </a>
</li>
@endif
  

