{{-- @Nombre del programa: Vista Principal--}}
@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1>Consultas Sistema de Trazabilidad Documental</h1><br>
    </div>
    <div class="col-sm-1">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item active"><i class="fa fa fa-home"></i> Inicio</li>
      </ol>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
  <style>
    .productos-icons {
      color: #f4a329 !important;
      font-size: 50px !important;
    }
    .img-home-page {
      height: 500px;
      width: 100%;
    }
    
  </style>
@endsection
@section('content')

<div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>

{{-- <iframe width="1200" height="700" src="https://app.powerbi.com/view?r=eyJrIjoiMzY4ZmFkYjktNDM5NC00MjdjLTgwZjctZWJhNjg5ZmM4ZTY1IiwidCI6IjBjMDJjMzQ3LTE0OGItNDMwNC04N2I5LTExMDFhZmMyOGM1MyIsImMiOjR9&pageName=ReportSection" frameborder="0" allowFullScreen="true"></iframe> --}}
  
  
  @if (auth()->user()->hasPermissionModule('DocumentosDigitales'))
<section class="content">
  <div class="container-fluid">
    <div class="row justify-content-center align-items-center">
      <div class="col-lg-3 col-6">
     
      <div class="small-box bg-info">
      <div class="inner">
      <h5><center><b> Memorándum Electrónico </b></center></h5>
      <h5><center> Último Folio {{$ultimoMemo}} </center></h5>
      </div>
      <div class="icon">
      <i class="ion ion-bag"></i>
      </div>
      <a href="{{ route('registro_crear', 16) }}" class="small-box-footer"><b>Gestionar Nuevo Memorándum Electrónico</b></a>
      </div>
      </div>

      <div class="col-lg-3 col-6">

      <div class="small-box bg-danger">
      <div class="inner">
      <h5><center><b> Oficio Electrónico </b></center></h5>
      <h5><center> Último Folio {{$ultimoOficio}} </center></h5>
      </div>
      <div class="icon">
      <i class="ion ion-stats-bars"></i>
      </div>
      <a href="{{ route('registro_crear', 6) }}" class="small-box-footer"><b>Gestionar Nuevo Oficio Electrónico</b></a>
      </div>
      </div>

      <div class="col-lg-3 col-6">

      <div class="small-box bg-success">
      <div class="inner">
      <h5><center><b> Resolución Exenta Electrónica </b></center></h5>
      <h5><center> Último Folio {{$ultimaResolucion}} </center></h5>
      </div>
      <div class="icon">
      <i class="ion ion-stats-bars"></i>
      </div>
      <a href="{{ route('registro_crear', 2) }}" class="small-box-footer"><b>Gestionar Nueva Resolución Exenta Electrónica</b></a>
      </div>
      </div>

    </div>
  </div>
  @endif

  @if (auth()->user()->hasPermissionNotAdmin(['15-22']))
<section class="content">
  <div class="container-fluid">
    <div class="row justify-content-center align-items-center">
      <div class="col-lg-3 col-6">
     
      <div class="small-box bg-primary">
      <div class="inner">
      <h5><center><b> Decreto Electrónico </b></center></h5>
      <h5><center> Último Folio {{$ultimoDecreto}} </center></h5>
      </div>
      <div class="icon">
      <i class="ion ion-bag"></i>
      </div>
      <a href="{{ route('registro_crear', 3) }}" class="small-box-footer"><b>Gestionar Nuevo Decreto Electrónico</b></a>
      </div>
      </div>

      <div class="col-lg-3 col-6">

      <div class="small-box bg-secondary">
      <div class="inner">
      <h5><center><b> Decreto Supremo Electrónico </b></center></h5>
      <h5><center> Último Folio {{$ultimoDecretoSupremo}} </center></h5>
      </div>
      <div class="icon">
      <i class="ion ion-stats-bars"></i>
      </div>
      <a href="{{ route('registro_crear', 4) }}" class="small-box-footer"><b>Gestionar Nuevo Decreto Supremo Electrónico</b></a>
      </div>
      </div>

      <div class="col-lg-3 col-6">

      <div class="small-box bg-dark">
      <div class="inner">
      <h5><center><b> Decreto Exento Electrónico </b></center></h5>
      <h5><center> Último Folio {{$ultimoDecretoExento}} </center></h5>
      </div>
      <div class="icon">
      <i class="ion ion-stats-bars"></i>
      </div>
      <a href="{{ route('registro_crear', 5) }}" class="small-box-footer"><b>Gestionar Nueva Decreto Exento Electrónico</b></a>
      </div>
      </div>

    </div>
  </div>
  @endif

<br>


  <div class="container-fluid">
    <div class="row justify-content-center align-items-center">
      <div class="col-lg-3 col-6">

      <!-- <div class="alert alert-warning" role="alert">
        <h5 class="alert-heading"><b style="color:#FF5747;">¡IMPORTANTE!</b></h5>
        <p><center><b>Se informa que la API de FirmaGob se encuentra caída por lo cual no se puede Firmar Documentos Digitales en este momento. <br>
        De igual forma pueden Gestionar Nuevos Documentos Digitales y dejarlos listos para ser firmados cuando se levante el servicio de FirmaGob.</b></center></p>
      </div>-->
      @if (auth()->user()->hasPermissionModule('DocumentosDigitales') && auth()->user()->hasPermissionModule('Firmantes'))
      <div class="small-box bg-warning">
      <div class="inner">
      <h5><center><b> Firmar <br>Documentos Digitales </b></center></h5>
      <div class="info-icons">
      <center>
      <img src="{{ asset('img/signature.png') }}" alt="firma" style=" color:#ffffff " width="50" height="50">  
      </div>
      </div>
      <a href="{{ route('firmarDocumentos.LoginClaveUnica') }}" class="small-box-footer"><b>Firmar Documentos Digitales Pendientes</b></a>
      </div>
      @endif 
      </div>

    </div>
  </div>
  
</section>




  <!--<div class="row">
    <div class="col-md-12">
      <img class="img-thumbnail img-home-page" src="{{asset('/img/financial.jpg')}}" alt="Electronic Payment Suite (EPS)">
    </div>
  </div>
  <br />-->
  <!--<div class="container-fluid">
    <div class="card-deck">

      <div class="card card-primary card-outline collapsed-card">
        <div class="card-header">
          <h3 class="card-title">
          </h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-plus"></i>
            </button>
          </div>
          <center><span class="fa fa-exchange productos-icons"></span>
          <h4>CRÉDITOS DIRECTOS</h4>
          <p>Transferencia electrónica de fondos</p></center>
            <strong><center>PERÍODO DE COMPENSACIÓN </center></strong><br>

        </div>
        <div class="card-body flex-fill">
          <p ALIGN="justify">
            Operaciones de movilización de fondos entre instituciones bancarias, de un mismo cliente o de clientes distintos, las cuales son instruidas por el cliente de la Institución Bancaria Ordenante y depositados en la cuenta del cliente de la Institución Bancaria Receptora.</p>
        </div>
      </div>

      <div class="card card-primary card-outline collapsed-card">
        <div class="card-header">
          <h3 class="card-title">
          </h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-plus"></i>
            </button>
          </div>
          <center><span class="fa fa-clock-o productos-icons"></span>
          <h4>DOMICILIACIONES</h4>
          <p>Débito automático</p></center>
          <strong><center>PERÍODO DE COMPENSACIÓN </center></strong><br>

        </div>
        <div class="card-body flex-fill">
          <p ALIGN="justify">
            Débitos directos ejecutados, con cierta regularidad, mediante una orden de cobro por la prestación de servicios o adquisición de bienes, emitida por la empresa prestadora de servicios o proveedora de bienes, en virtud de la autorización emanada de un cliente ordenante, para que sea debitado automáticamente el monto del servicio prestado o bien adquirido de la cuenta que éste expresamente señale, en los términos acordados previamente por dicho cliente con la empresa o con una Institución Bancaria Participante.</p>
        </div>
      </div>

      <div class="card card-primary card-outline collapsed-card">
        <div class="card-header">
          <h3 class="card-title">
          </h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-plus"></i>
            </button>
          </div>
          <center><span class="fa fa-lock productos-icons"></span>
          <h4>CHEQUES</h4>
          <p>Procesamiento de cheques con y sin imágenes</p></center>
          <strong><center>PERÍODO DE COMPENSACIÓN </center></strong><br>

        </div>
        <div class="card-body flex-fill">
          <p ALIGN="justify">
            Una orden escrita librada por una de las partes (el librador) hacia otra (el librado, generalmente un banco) que requiere que el librado pague una suma específica a pedido del librador o a un tercero que éste especifique.</p>
        </div>
      </div>
    </div>
  </div>-->
@endsection
@section('before-scripts-end')

@stop
