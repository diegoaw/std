@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-5">
      <h1><b><i class="fa fa-comments-o"></i> Consultas sobre el Sistema de Integridad</b></h1>
    </div>
    <div class="col-sm-7">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-comments-o"></i> Consultas sobre el Sistema de Integridad</li>
        <li class="breadcrumb-item active">Detalles</li>
      </ol>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <!-- Line chart -->
    <div class="card card-danger card-outline">
      <div class="card-header">
        <h3 class="card-title"><b>Consulta sobre el Sistema de Integridad N° {{$data->id}}</b></h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <br>
        <h4><b>Datos Consulta</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Nombre Solicitante:</b></td>
              <td nowrap>{{$data->nombres_solicitante}} {{$data->apellidos_solicitante}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Rut Solicitante:</b></td>
              <td nowrap>{{$data->rut}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Correo Solicitante:</b></td>
              <td nowrap>{{$data->correo_solicitante}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Unidad o Seremía Solicitante:</b></td>
              <td nowrap>{{$data->unidad_o_seremia}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Genero:</b></td>
              <td nowrap>{{$data->opcion_sexual}} @if($data->otra_cual) {{$data->otra_cual}} @endif</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Consulta es sobre:</b></td>
              <td nowrap>
                @if($data->tipo_de_consulta)
                  @foreach ($data->tipo_de_consulta as $indice => $tramite)
                  {{$tramite}}                 
                  @endforeach
                @endif
              </td>
          </tr>
          <tr>
              <td style="width:450px"><b>Consulta (Relato de la Consulta):</b></td>
              <td nowrap>{{$data->consulta}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Archivos Adjuntos:</b></td>
              <td >
              @if($data->antecedente_1)
              <a href="{{$data->antecedente_1}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
              @if($data->antecedente_2)
              <a href="{{$data->antecedente_2}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
          </tr>
          <tr>
              <td style="width:450px"><b>Descargar Comprobante de Consulta:</b></td>
              <td >
              @if($data->descargar)
              <a href="{{$data->descargar}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
              </td>
          </tr>
        </table>
        <br>
        <br>
        <h4><b>Revisión Consulta</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Fecha de Respuesta:</b></td>
              <td nowrap>{{$data->fecha_de_respuesta}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Respuesta:</b></td>
              <td nowrap>{{$data->respuesta}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Archivo Adicional:</b></td>
              <td >
              @if($data->adicional)
              <a href="{{$data->adicional}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
          </tr>
        </table>
      </div>

      </div>

        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! link_to_route('consulta', 'Volver', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
            </div>
          </div>
        </div>
    </div>
  </div>
</div>


@endsection
@section('before-scripts-end')
@stop