@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-5">
      <h1><b><i class="fa fa-exclamation-circle"></i> Denuncias</b></h1>
    </div>
    <div class="col-sm-7">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-exclamation-circle"></i> Denuncias</li>
        <li class="breadcrumb-item active">Detalles</li>
      </ol>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <!-- Line chart -->
    <div class="card card-danger card-outline">
      <div class="card-header">
        <h3 class="card-title"><b>Denuncia N° {{$data->id}}</b></h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <br>
        <h4><b>Datos Denuncia</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Nombre Denunciante:</b></td>
              <td nowrap>{{$data->nombres_solicitante}} {{$data->apellidos_solicitante}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Rut Denunciante:</b></td>
              <td nowrap>{{$data->rut}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Correo Denunciante:</b></td>
              <td nowrap>{{$data->correo_solicitante}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Unidad o Seremía Denunciante:</b></td>
              <td nowrap>{{$data->unidad_o_seremia}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Genero Denunciante:</b></td>
              <td nowrap>{{$data->opcion_sexual}} @if($data->indique) {{$data->indique}} @endif</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Teléfono Móvil Denunciante:</b></td>
              <td nowrap>{{$data->telefono_movil}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Dirección Denunciante:</b></td>
              <td nowrap>{{$data->direccion}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Tipo de Denuncia:</b></td>
              <td nowrap>{{$data->tipo_de_denuncia}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Especifique ¿Cual es su denuncia?:</b></td>
              <td nowrap>{{$data->especifique}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Fecha Inicio:</b></td>
              <td nowrap>{{$data->fecha_inicio}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Fecha Fin:</b></td>
              <td nowrap>{{$data->fecha_fin}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Archivos Adjuntos:</b></td>
              <td >
              @if($data->archivo_de_respaldo_1)
              <a href="{{$data->archivo_de_respaldo_1}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
              @if($data->archivo_de_respaldo_2)
              <a href="{{$data->archivo_de_respaldo_2}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
              @if($data->archivo_de_respaldo_3)
              <a href="{{$data->archivo_de_respaldo_3}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
          </tr>
          <tr>
              <td style="width:450px"><b>Descargar Comprobante de Denuncia:</b></td>
              <td >
              @if($data->descargar)
              <a href="{{$data->descargar}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
              </td>
          </tr>
        </table>
        <br>
        <br>
        <h4><b>Recepción y Revisión de Denuncia</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Fecha de Recepción:</b></td>
              <td nowrap>{{$data->fecha_de_recepcion}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Observación:</b></td>
              <td nowrap>{{$data->observacion}}</td>
          </tr>
        </table>
        <br>
        <br>
        <h4><b>Respuesta a Denuncia</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Fecha de Respuesta:</b></td>
              <td nowrap>{{$data->fecha_de_respuesta}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Resultado de análisis e investigación a la denuncia:</b></td>
              <td nowrap>{{$data->resultado}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Decisión tomada para la denuncia:</b></td>
              <td nowrap>{{$data->respuesta}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Archivo Adjunto:</b></td>
              <td >
              @if($data->adjuntar_doc_final)
              <a href="{{$data->adjuntar_doc_final}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
          </tr>
        </table>
        <br>
        <br>
        <h4><b>Comprobante de Respuesta a Denuncia</b></h4>
        <br>
        <table id="example1" class="table table-striped">
        <td style="width:450px"><b>Descargar Comprobante Respuesta a Denuncia:</b></td>
              <td >
              @if($data->comprobante_aprobacion)
              <a href="{{$data->comprobante_aprobacion}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
              @if($data->comprobante_rechazo)
              <a href="{{$data->comprobante_rechazo}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
              </td>
        </table>
      </div>

        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! link_to_route('denuncia', 'Volver', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
            </div>
          </div>
        </div>
    </div>
  </div>
</div>


@endsection
@section('before-scripts-end')
@stop