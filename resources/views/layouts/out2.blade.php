<!doctype html>
  @section('htmlheader')
      @include('includes.htmlheader')
  @show


  <nav class="navbar navbar-expand bg-white navbar-light border-bottom fixed-navbar">
  <!-- Left navbar links -->
  <ul class="navbar-nav">
    


    <div class="logos-container">
          <img class="ft-logo-dgd" src="{{ asset('assets/mindep.png') }}" alt="Logo MINDEP" width="50" height="50">&nbsp;&nbsp;&nbsp;
    </div>
    <div class="media">
        <div class="media-body align-self-center name-institution">
          <h5 class="">Ministerio del Deporte</h5>
        </div>
     </div>
      
  </ul>

      
                              
  <!-- Right navbar links -->
  <ul class="navbar-nav ml-auto">
    <!-- Notifications Dropdown Menu -->
    <li class="nav-item dropdown">
      
      <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
        
        <div style="padding: 3%;content: ' ';display: table;width: 100%;">
          
          
        </div>
      </div>
    </li>
  </ul>
</nav>



<body class="hold-transition">

  <div class="wrapper">
    <!-- Content Wrapper. Contains page content -->
    <div class="col-sm-12">
      <!-- loader -->
      

      <!-- Content Header (Page header) -->
      <section class="content-header">
        @yield('page-header')
      </section>

      <!-- Main content -->
      <section class="content">
          <div id="app"></div>
        @yield('content')
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
    @include('includes.footer')
  </div><!-- ./wrapper -->

  @section('scripts')
      @include('includes.scripts')
  @show
</body>
</html>