{{-- @Nombre del programa: Vista Principal--}}
@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1>Ley 20.594</h1><br>
    </div>
    <div class="col-sm-1">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item active"><i class="fa fa fa-file"></i> Ley 20.594</li>
      </ol>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
  <style>
    .productos-icons {
      color: #f4a329 !important;
      font-size: 50px !important;
    }
    .img-home-page {
      height: 500px;
      width: 100%;
    }
  </style>
@endsection
@section('content')
  <!--<div class="row">
    <div class="col-md-12">
      <img class="img-thumbnail img-home-page" src="{{asset('/img/financial.jpg')}}" alt="Electronic Payment Suite (EPS)">
    </div>
  </div>
  <br />-->
  <!--<div class="container-fluid">
    <div class="card-deck">

      <div class="card card-primary card-outline collapsed-card">
        <div class="card-header">
          <h3 class="card-title">
          </h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-plus"></i>
            </button>
          </div>
          <center><span class="fa fa-exchange productos-icons"></span>
          <h4>CRÉDITOS DIRECTOS</h4>
          <p>Transferencia electrónica de fondos</p></center>
            <strong><center>PERÍODO DE COMPENSACIÓN </center></strong><br>

        </div>
        <div class="card-body flex-fill">
          <p ALIGN="justify">
            Operaciones de movilización de fondos entre instituciones bancarias, de un mismo cliente o de clientes distintos, las cuales son instruidas por el cliente de la Institución Bancaria Ordenante y depositados en la cuenta del cliente de la Institución Bancaria Receptora.</p>
        </div>
      </div>

      <div class="card card-primary card-outline collapsed-card">
        <div class="card-header">
          <h3 class="card-title">
          </h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-plus"></i>
            </button>
          </div>
          <center><span class="fa fa-clock-o productos-icons"></span>
          <h4>DOMICILIACIONES</h4>
          <p>Débito automático</p></center>
          <strong><center>PERÍODO DE COMPENSACIÓN </center></strong><br>

        </div>
        <div class="card-body flex-fill">
          <p ALIGN="justify">
            Débitos directos ejecutados, con cierta regularidad, mediante una orden de cobro por la prestación de servicios o adquisición de bienes, emitida por la empresa prestadora de servicios o proveedora de bienes, en virtud de la autorización emanada de un cliente ordenante, para que sea debitado automáticamente el monto del servicio prestado o bien adquirido de la cuenta que éste expresamente señale, en los términos acordados previamente por dicho cliente con la empresa o con una Institución Bancaria Participante.</p>
        </div>
      </div>

      <div class="card card-primary card-outline collapsed-card">
        <div class="card-header">
          <h3 class="card-title">
          </h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-plus"></i>
            </button>
          </div>
          <center><span class="fa fa-lock productos-icons"></span>
          <h4>CHEQUES</h4>
          <p>Procesamiento de cheques con y sin imágenes</p></center>
          <strong><center>PERÍODO DE COMPENSACIÓN </center></strong><br>

        </div>
        <div class="card-body flex-fill">
          <p ALIGN="justify">
            Una orden escrita librada por una de las partes (el librador) hacia otra (el librado, generalmente un banco) que requiere que el librado pague una suma específica a pedido del librador o a un tercero que éste especifique.</p>
        </div>
      </div>
    </div>
  </div>-->
@endsection
@section('before-scripts-end')

@stop