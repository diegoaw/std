<div class="row">
  <div class="col-md-12">
    <!-- Line chart -->
    <div class="card card-danger card-outline">
      <div class="card-header">
        <h3 class="card-title">Detalles Memo Electrónico N° {{$dat['detalle']->correlativo}}</h3>
      </div>
      <div class="card-body">
        <table id="example1" class="egt" border="1">
          <tbody>
          <tr>
            <th scope="row"><strong>Fecha</strong></th>
            <td>{{$dat['detalle']->fecha}}</td>
          </tr>
          <tr>
            <th scope="row"><strong>Trámite N°</strong></th>
            <td>{{$dat['detalle']->tramite_id}}</td>
          </tr>
          <tr>
            <th scope="row"><strong>Correlativo N°</strong></th>
            <td>{{$dat['detalle']->correlativo}}</td>
          </tr>
          <tr>
            <th scope="row"><strong>Lugar</strong></th>
            <td>{{$dat['detalle']->lugar}}</td>
          </tr>
          <tr>
            <th scope="row"><strong>División</strong></th>
            <td>{{$dat['detalle']->division}} </td>
          </tr>
          <tr>
            <th scope="row"><strong>Remitente</strong></th>
            <td>{{$dat['detalle']->remitente}}</td>
          </tr>
          <tr>
            <th scope="row"><strong>Destinatario</strong></th>
            <td>{{$dat['detalle']->destinatario}}</td>
          </tr>
          <tr>
            <th scope="row"><strong>Materia</strong></th>
            <td>{!!$dat['detalle']->materia!!}</td>
          </tr>
          <tr>
            <th scope="row"><strong>Contenido</strong></th>
            <td>{!!$dat['detalle']->texto!!}</td>
          </tr>
          <tr>
            <th scope="row"><strong>Vistos</strong></th>
            <td>{{$dat['detalle']->vistos}}</td>
          </tr>
          <tr>
            <th scope="row"><strong>Distribución</strong></th>
            <td>{!!$dat['detalle']->distribucion!!}</td>
          </tr>
          <tr>
            <th scope="row"><strong>Anexos Nombres</strong></th>
            <td>{{$dat['detalle']->anexos}}</td>
          </tr>
          <tr>
            <th scope="row"><strong>Memo Electrónico</strong></th>
            <td>
            @if($dat['detalle']->doc_memo)
            <a href="{{$dat['detalle']->doc_memo}}" target="_blank" download>Descargar Memo Electrónico</a>
            @endif
            </td>
          </tr>
          <tr>
            <th scope="row"><strong>Anexos</strong></th>
            <td>

                @if($dat['detalle']->doc_anexo_a)
                <a href="{{$dat['detalle']->doc_anexo_a}}" target="_blank" download>Descargar Anexo A</a>
                @endif
                @if($dat['detalle']->doc_anexo_b)
                <a href="{{$dat['detalle']->doc_anexo_b}}" target="_blank" download>Descargar Anexo B</a>
                @endif
                @if($dat['detalle']->doc_anexo_c)
                <a href="{{$dat['detalle']->doc_anexo_c}}" target="_blank" download>Descargar Anexo C</a>
                @endif
                @if($dat['detalle']->doc_anexo_d)
                <a href="{{$dat['detalle']->doc_anexo_d}}" target="_blank" download>Descargar Anexo D</a>
                @endif

          </tr>
          <tr>
              <th scope="row"><strong>Observación</strong></th>
              <td>{!! $dat['observaciones'] !!}</td>
            </tr>
        </tbody>
        </table>
      </div>

    </div>
  </div>
</div>
