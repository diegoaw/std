<div class="row">
  <div class="col-md-12">
    <!-- Line chart -->
    <div class="card card-danger card-outline">
      <div class="card-header">
        <h3 class="card-title">Detalles Resolución Exenta Electrónica N° @if($detalle->correlativo=='') {{$detalle->correlativo}} @else {{$detalle->correlativo->correlativo}} @endif</h3>
      </div>
      <div class="card-body">
         <table id="example1" class="egt" border="1">
          <tr>
            <th>Fecha</th>
            <td>{{$detalle->fecha}}</td>
          </tr>
          <tr>
            <th>Trámite N°</th>
            <td>{{$detalle->tramite_id}}</td>
          </tr>
           <tr>
            <th>Correlativo N°</th>
            @if($detalle->correlativo=='')
            <td>{{$detalle->correlativo}}</td>
            @else
            <td>{{$detalle->correlativo->correlativo}}</td>
            @endif
          </tr>
          <tr>
            <th>Lugar</th>
            <td>{{$detalle->lugar}}</td>
          </tr>
          <tr>
            <th>Remitente</th>
            <td>{{$detalle->remitente}}</td>
          </tr>
          <tr>
            <th>División</th>
            <td>{{$detalle->division}} </td>
          </tr>
          <tr>
            <th>Referencia</th>
            <td>{!!$detalle->referencia!!}</td>
          </tr>
          <tr>
            <th>Vistos</th>
            <td>{!!$detalle->vistos!!}</td>
          </tr>
          <tr>
            <th>Considerando</th>
            <td>{!!$detalle->considerando!!}</td>
          </tr>
          <tr>
            <th>Resuelvo</th>
            <td>{!!$detalle->resuelvo!!}</td>
          </tr>
          <tr>
            <th>Vistos de Resposabilidad</th>
            <td>{{$detalle->vistos_responsabilidad}}</td>
          </tr>
          <tr>
            <th>Distribución</th>
            <td>{!!$detalle->distribucion!!}</td>
          </tr>
          <tr>
            <th>Resolución Exenta Electrónica</th>
            <td>
            @if($detalle->doc_resolucion_exenta)
            <a href="{{$detalle->doc_resolucion_exenta}}" target="_blank" download>Descargar Resolución Exenta Electrónica </a>
           
            @endif
          </td>
          </tr>
          <tr>
            <th>Anexos</th>
            <td>
             
                @if($detalle->doc_anexo_a)
                <a href="{{$detalle->doc_anexo_a}}" target="_blank" download>Descargar Anexo A</a>
                @endif
                @if($detalle->doc_anexo_b)
                <a href="{{$detalle->doc_anexo_b}}" target="_blank" download>Descargar Anexo B</a>
                @endif
                @if($detalle->doc_anexo_c)
                <a href="{{$detalle->doc_anexo_c}}" target="_blank" download>Descargar Anexo C</a>
                @endif
                @if($detalle->doc_anexo_d)
                <a href="{{$detalle->doc_anexo_d}}" target="_blank" download>Descargar Anexo D</a>
                @endif
          </tr>
          </tr>
        </table>

      </div>

    </div>
  </div>
</div>
