<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>

      <!-- Latest compiled and minified CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

      <!-- Optional theme -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

      <!-- Latest compiled and minified JavaScript -->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    </head>
    <body>
      <div class="col-12">
         <div class="card">
            <div class="card-body">
               <div>
                  <p> </p>
                  <table border="0" width="100%" cellspacing="0" cellpadding="0">
                     <tbody>
                        <tr>
                           <td style="padding: 10px 0 30px 0;">
                              <table style="border: 1px solid #dceaf5; border-collapse: collapse;" border="0" width="600" cellspacing="0" cellpadding="0" align="center">
                                 <tbody>
                                   <tr>
                                       <td style="padding: 30px 0 20px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif; border-bottom: 5px solid #0463ac;" align="center" bgcolor="#e1e3e5"> <img  style="width: 120px; height: 110px;" src="https://webservices.mindep.cl/Firmas/mindeplogo2.png" alt="mindep" border="0" /></a></td>
                                    </tr>
                                    <tr>
                                       <td style="padding: 40px 30px 40px 30px;" bgcolor="#ffffff">
                                          <table border="0" width="100%" cellspacing="0" cellpadding="0">
                                             <tbody>
                                                <tr>
                                                   <td style="color: #546575; font-family: Open Sans; font-size: 16px; -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale;"><strong>Estimado(a) funcionario(a)</strong></td>
                                                </tr>
                                                <tr>
                                                   <td style="padding: 20px 0 30px 0; color: #546575; font-family: Open Sans; font-size: 14px; font-weight: 500; line-height: 30px; -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale;">
                                                      <p>Se informa que ha <b>ingresado</b> una nueva <b>documentación</b> a su nombre con la siguientes especificaciones:</p>
                                                      <p></p>
                                                      <table>
                                                        <thead>
                                                          <tbody>
                                                            <tr>
                                                                <td><center><b>ID</b></center></td>
                                                                <td><center>{{$datos->id}}</center></td>
                                                            </tr>
                                                            <tr>
                                                                <td><center><b>Tipo</b></center></td>
                                                                <td><center>{{$datos->OpaTipo->nombre}}</center></td>
                                                            </tr>
                                                            <tr>
                                                                <td><center><b>Clasificación</b></i></center></td>
                                                                <td><center>{{$datos->OpaClasificacion->nombre}}</center></td>
                                                            </tr>
                                                              <tr>
                                                                  <td><center><b>Tipo de Documentación</b></center></td>
                                                                  <td><center>{{$datos->OpaTipoDocumentacion->nombre}}</center></td>
                                                              </tr>
                                                              @if($datos->OpaModalidadDocumento)
                                                              <tr>
                                                                  <td><center><b>Modalidad</b></center></td>
                                                                  <td><center>{{$datos->OpaModalidadDocumento->nombre}}</center></td>
                                                              </tr>
                                                              @endif
                                                              @if($datos->de)
                                                              <tr>
                                                                  <td><center><b>De</b></center></td>
                                                                  <td><center>{{$datos->de}}</center></td>
                                                              </tr>
                                                              @endif
                                                              <tr>
                                                                  <td><center><b>Para</b></center></td>
                                                                  <td><center>{{$datos->para}}</center></td>
                                                              </tr>
                                                              @if($datos->descripcion)
                                                              <tr>
                                                                <td><center><b>Descripción</b></center></td>
                                                                <td><center>{{$datos->descripcion}}</center></td>
                                                              </tr>
                                                              @endif
                                                              <tr>
                                                                <td><center><b>STD Interno</b></center></td>
                                                                <td><center>{{$datos->n_doc}}</center></td>
                                                              </tr>
                                                              <tr>
                                                                <td><center><b>Fecha/Hora Entrada</b></center></td>
                                                                <td><center>{{$datos->created_at}}</center></td>
                                                              </tr>
                                                              <!-- <tr>
                                                                <td><center><b>Fecha/Hora Actualizado</b></center></td>
                                                                <td><center>{{$datos->updated_at}}</center></td>
                                                              </tr> -->
                                                              @if($datos->fecha_cierre)
                                                              <tr>
                                                                <td><center><b>Fecha/Hora Salida</b></center></td>
                                                                <td><center>{{$datos->fecha_cierre}} </center></td>
                                                              </tr>
                                                              @endif
                                                              <tr>
                                                                <td><center><b>Estado</b></center></td>
                                                                <td><center>
                                                                  @if($datos->estado == 'En Proceso')
                                                                  Disponible para Retiro
                                                                  @else
                                                                  Distribuido por STD
                                                                  @endif
                                                                </center></td>
                                                              </tr>
                                                            </tr>
                                                          </tbody>
                                                        </thead>
                                                      </table>
                                                      <p></p>
                                                      <p>Atentamente,</p>
                                                      <p style="text-align: center"><b>DAF - Gestión de Documentos | Ministerio del Deporte 2021</b></p>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>

                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  <p> </p>
               </div>

            </div>
         </div>
      </div>

    </body>
</html>
