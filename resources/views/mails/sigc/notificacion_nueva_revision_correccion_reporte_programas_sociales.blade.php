<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>

      <!-- Latest compiled and minified CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

      <!-- Optional theme -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

      <!-- Latest compiled and minified JavaScript -->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    </head>
    <body>
      <div class="col-12">
         <div class="card">
            <div class="card-body">
               <div>
                  <p> </p>
                  <table border="0" width="100%" cellspacing="0" cellpadding="0">
                     <tbody>
                        <tr>
                           <td style="padding: 10px 0 30px 0;">
                              <table style="border: 1px solid #dceaf5; border-collapse: collapse;" border="0" width="600" cellspacing="0" cellpadding="0" align="center">
                                 <tbody>
                                   <tr>
                                       <td style="padding: 30px 0 20px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif; border-bottom: 5px solid #0463ac;" align="center" bgcolor="#e1e3e5"> <img  style="width: 120px; height: 110px;" src="https://webservices.mindep.cl/Firmas/mindeplogo2.png" alt="mindep" border="0" /></a></td>
                                    </tr>
                                    <tr>
                                       <td style="padding: 40px 30px 40px 30px;" bgcolor="#ffffff">
                                          <table border="0" width="100%" cellspacing="0" cellpadding="0">
                                             <tbody>
                                                <tr>
                                                   <td style="color: #546575; font-family: Open Sans; font-size: 16px; -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale;"><strong>Estimado(a) funcionario(a)</strong></td>
                                                </tr>
                                                <tr>
                                                   <td style="padding: 20px 0 30px 0; color: #546575; font-family: Open Sans; font-size: 14px; font-weight: 500; line-height: 30px; -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale;">
                                                      <p>Se informa que ha ingresado un nueva Revisión Correción de Programa Social con los siguientes datos: </p>
                                                      <table>
                                                        <tbody>
                                                          <tr>
                                                            <td></td>
                                                            <td><strong>REVISIÓN</strong></td>
                                                          </tr>
                                                          <tr>
                                                            <td><strong>FECHA DE REVISIÓN:</strong></td>
                                                            <td>{{$datos['programa']->updated_at}}</td>
                                                          </tr>
	                                                         <tr>
                                                             <td><strong>ESTADO:</strong></td>
                                                             <td>{{$datos['programa']->Estados->nombre}}</td>
                                                           </tr>
                                                           <tr>
                                                             <td><strong>OBSERVACIÓN:</strong></td>
                                                             <td>{{$datos['programa']->observacion}}</td>
                                                           </tr>
                                                        </tbody>
                                                      </table>
                                                      <table>
                                                        <tbody>
	                                                         <tr>
                                                             <td></td>
                                                             <td></td>
                                                           </tr>
                                                        </tbody>
                                                      </table>
                                                      <table>
                                                        <tbody>
                                                          <tr>
                                                            <td></td>
                                                            <td><strong>REPORTE</strong></td>
                                                          </tr>
	                                                         <tr>
                                                             <td><strong>PROGRAMA:</strong></td>
                                                             <td>{{$datos['programa']->Programas->nombre}}</td>
                                                           </tr>
                                                        </tbody>
                                                      </table>
                                                      <p>Atentamente,</p>
                                                      <p style="text-align: center"><b>División de Planificación y Control de Gestión | Ministerio del Deporte 2022</b></p>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>

                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  <p> </p>
               </div>

            </div>
         </div>
      </div>

    </body>
</html>
