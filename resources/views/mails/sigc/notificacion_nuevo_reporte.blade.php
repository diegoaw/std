<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>

      <!-- Latest compiled and minified CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

      <!-- Optional theme -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

      <!-- Latest compiled and minified JavaScript -->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    </head>
    <body>
      <div class="col-12">
         <div class="card">
            <div class="card-body">
               <div>
                  <p> </p>
                  <table border="0" width="100%" cellspacing="0" cellpadding="0">
                     <tbody>
                        <tr>
                           <td style="padding: 10px 0 30px 0;">
                              <table style="border: 1px solid #dceaf5; border-collapse: collapse;" border="0" width="600" cellspacing="0" cellpadding="0" align="center">
                                 <tbody>
                                   <tr>
                                       <td style="padding: 30px 0 20px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif; border-bottom: 5px solid #0463ac;" align="center" bgcolor="#e1e3e5"> <img  style="width: 120px; height: 110px;" src="https://webservices.mindep.cl/Firmas/mindeplogo2.png" alt="mindep" border="0" /></a></td>
                                    </tr>
                                    <tr>
                                       <td style="padding: 40px 30px 40px 30px;" bgcolor="#ffffff">
                                          <table border="0" width="100%" cellspacing="0" cellpadding="0">
                                             <tbody>
                                                <tr>
                                                   <td style="color: #546575; font-family: Open Sans; font-size: 16px; -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale;"><strong>Estimado(a) funcionario(a)</strong></td>
                                                </tr>
                                                <tr>
                                                   <td style="padding: 20px 0 30px 0; color: #546575; font-family: Open Sans; font-size: 14px; font-weight: 500; line-height: 30px; -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale;">
                                                      <p>Se informa que ha ingresado un nuevo reporte con los siguientes datos: </p>
                                                      <p></p>
                                                      <table>
                                                        <tbody>
                                                          <tr>
                                                            <td></td>
                                                            <td><strong>REPORTE</strong></td>
                                                          </tr>
	                                                         <tr>
                                                             <td><strong>INDICADOR:</strong></td>
                                                             <td>{{$datos['indicador']->nombre}}</td>
                                                           </tr>
                                                           <tr>
                                                             <td><strong>NUMERADOR:</strong></td>
                                                             <td>{{$datos['medicion']->valor_numerador}}</td>
                                                           </tr>
                                                           @if($datos['indicador']->categoria_id == 1)
                                                           <tr>
                                                             <td><strong>DENOMINADOR:</strong></td>
                                                             <td>{{$datos['indicador']->denominador}}</td>
                                                           </tr>
                                                           @else
                                                           <tr>
                                                             <td><strong>DENOMINADOR:</strong></td>
                                                             <td>{{$datos['medicion']->denominador}}</td>
                                                           </tr>
                                                           @endif
                                                           <tr>
                                                             <td><strong>RESULTADO:</strong></td>
                                                             <td>{{$datos['medicion']->resultado}}</td>
                                                           </tr>
                                                           <tr>
                                                             <td><strong>ANÁLISIS:</strong></td>
                                                             <td>{{$datos['medicion']->analisis}}</td>
                                                           </tr>
                                                           <tr>
                                                             <td><strong>ACCIONES:</strong></td>
                                                             <td>{{$datos['medicion']->acciones}}</td>
                                                           </tr>
                                                           @if($datos['medicion']->archivos)
                                                           @foreach($datos['medicion']->archivos as $indice => $archivos)
                                                           <tr>
                                                             <td><strong>MEDIO DE VERIFICACIÓN:</strong></td>
                                                             <td><a href="{{$archivos->filename_aws}}" target="_blank" download>{{$archivos->filename}}</a></td>
                                                           </tr>
                                                           @endforeach
                                                           @endif
                                                        </tbody>
                                                      </table>
                                                      <p>Atentamente,</p>
                                                      <p style="text-align: center"><b>División de Planificación y Control de Gestión | Ministerio del Deporte 2022</b></p>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>

                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  <p> </p>
               </div>

            </div>
         </div>
      </div>

    </body>
</html>
