
@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-5">
      <h1><i class="fa fa-user"></i> Memo Electrónico Colaboración</h1>
    </div>
    <div class="col-sm-7">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-user"></i> Memo Electrónico Colaboración</li>
        <li class="breadcrumb-item active">Detalles</li>
      </ol>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <!-- Line chart -->
    <div class="card card-danger card-outline">
      <div class="card-header">
        <h3 class="card-title">Detalles Memo Electrónico Colaboración N° {{$detalle->correlativo->correlativo}}</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <table id="example1" class="table table-bordered table-striped table-hover table-sm">
          
          <tr>
            <th>Fecha</th>
            <td>{{$detalle->fecha}}</td>
          </tr>
          <tr>
            <th>Trámite N°</th>
            <td>{{$detalle->tramite_id}}</td>
          </tr>
          <tr>
            <th>Correlativo N°</th>
            <td>{{$detalle->correlativo->correlativo}}</td>
          </tr>
          <tr>
            <th>Lugar</th>
            <td>{{$detalle->lugar}}</td>
          </tr>
          <tr>
            <th>Destinatario</th>
            <td>{{$detalle->destinatario}}</td>
          </tr>
        
          <tr>
            <th>Remitente</th>
            <td>{{$detalle->remitente}}</td>
          </tr>
      
          <tr>
            <th>Materia</th>
            <td>{!!$detalle->materia!!}</td>
          </tr>
          <tr>
            <th>Contenido</th>
            <td>{!!$detalle->texto!!}</td>
          </tr>
          <tr>
            <th>Vistos</th>
            <td>{{$detalle->vistos}}</td>
          </tr>
             <tr>
            <th>Distribución</th>
            <td>{!!$detalle->distribucion!!}</td>
          </tr>
             <tr>
            <th>Colaborador</th>
            <td>{{$detalle->colaborador}}</td>
          </tr>
             <tr>
            <th>Observación general</th>
            <td>{{$detalle->observacion_general}}</td>
          </tr>
                <tr>
            <th>Lista de colaboradores</th>
            <td>{!!$detalle->lista_colaboradores_aux !!}</td>
          </tr>
             <tr>
            <th>Anexos Nombres</th>
            <td>{{$detalle->anexos}}</td>
          </tr>
          <tr>
            <th> Anexos</th>
            <td>
             
                @if($detalle->doc_anexo_a) 
                <a href="{{$detalle->doc_anexo_a}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
                @endif 
                @if($detalle->doc_anexo_b) 
                <a href="{{$detalle->doc_anexo_b}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
                @endif 
                @if($detalle->doc_anexo_c) 
                <a href="{{$detalle->doc_anexo_c}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
                @endif
                @if($detalle->doc_anexo_d)
                <a href="{{$detalle->doc_anexo_d}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
                @endif 
          
          </tr>
          
        </table>
      </div>


        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! link_to_route('MemosColaboraciones', 'Volver', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
            </div>
          </div>
        </div>


    </div>
  </div>
</div>


@endsection
@section('before-scripts-end')
@stop
