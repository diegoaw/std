
@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-file-text"></i> Consulta Colaboraciones Memos Electrónicos</h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title">Filtrar Colaboraciones Memos Electrónicos por </h5>

      </div>
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">

                {!! Form::open(['route' => 'MemosCobaloracionesFiltros', 'method' => 'post' , 'class'=>"form-horizontal"]) !!}


                  <div class="form-row">

                    <div class="col-sm-0">
                      <label for="nRegistro" class=" col-form-label">N° Trámite: </label>
                      <input type="text" class="form-control" id="nRegistro" name="nRegistro" value="{{ $requestNRegistro }}" placeholder="N° Trámite">
                    </div>

                    <div class="col-sm-2">
                      <label for="correlativo" class=" col-form-label">Correlativo:</label>
                      <input class="form-control rounded-0" id="correlativo" name="correlativo" placeholder="Correlativo" rows="3" value="{{ $correlativoView }}">
                    </div>

                    <div class="form-group   {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                    <div class="input-group date">
                      <div class="col">
                          <label for="FechaDesde" class=" col-form-label">Fecha Desde: </label>
                          <input type="text" class="form-control group-date" id="FechaDesde" name="FechaDesde" readonly="readonly" value="{!! date('Y-m-d', strtotime($feDesde))  !!}">
                      </div>
                    </div>
                    </div>

                    <div class="form-group   {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                    <div class="input-group date">
                      <div class="col">
                          <label for="FechaHasta" class=" col-form-label">Fecha Hasta: </label>
                          <input type="text" class="form-control group-date" id="FechaHasta" name="FechaHasta" readonly="readonly" value="{!! date('Y-m-d', strtotime($feHasta ))  !!}">
                      </div>
                    </div>
                    </div>

                    <div class="col-sm-2">
                      <label for="remitente" class=" col-form-label">Remitente:</label>
                      <input class="form-control rounded-0" id="remitente" name="remitente" placeholder="Remitente" rows="3">{{ $remitenteView }} </input>
                    </div>

                    <div class="col-sm-2">
                      <label for="destinatario" class=" col-form-label">Destinatario:</label>
                      <input class="form-control rounded-0" id="destinatario" name="destinatario" placeholder="destinatario" rows="3" value="{{ $destinatarioView }}">
                    </div>

                    <div class="col-sm-2">
                      <label for="materia" class=" col-form-label">Materia:</label>
                      <textarea class="form-control rounded-0" id="materia" name="materia" placeholder="Materia" rows="3">{{ $materiaView }} </textarea>
                    </div>

                  </div>
                  <button type="submit" class="btn btn-primary pull-right">Buscar</button>
                {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



@if(count($listado)>0)
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">Listado de Memos Electrónicos y sus Colaboraciones</h5>

          </div>
          <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th nowrap style="color:white">Fecha</th>
                        <th nowrap style="color:white">N° Trámite</th>
                        <th nowrap style="color:white">Correlativo</th>
                        <th nowrap style="color:white">Materia</th>
                        <th nowrap style="color:white">Remitente</th>
                        <th nowrap style="color:white">División Remitente</th>
                        <th nowrap style="color:white">Destinatario</th>
                        <th nowrap style="color:white">Estatus</th>
                        <th nowrap style="color:white">Detalles</th>
                        <th nowrap style="color:white">Colaboraciones</th>
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($listado)>0)
                         @foreach ($listado as $indice => $registro)
                          <tr>
                            <td nowrap>{{$registro->fecha}}</td>
                            <td nowrap>{{$registro->tramite_id}}</td>
                            <td nowrap>{{$registro->correlativo->correlativo}}</td>
                            @if($registro->reservado != 1 || $registro->remitente==$nombrecargo || $registro->destinatario==$nombrecargo)
                            <td>{!!$registro->materia!!}</td>
                            @else
                            <td>Contenido <b>Reservado</b> solicite información al Remitente.</td>
                            @endif
                            <td>{{$registro->remitente}}</td>
                            <td>{{$registro->division}}</td>
                            <td>{{$registro->destinatario}}</td>
                              @if ($registro->doc_memo) <td> {{$registro->estatus}} @else <td style="background-color: #cd0b27; color: white;">{{$registro->estatus}}</td></td> @endif
                            <td>
                             <center>
                              @if($registro->reservado != 1 || $registro->remitente==$nombrecargo || $registro->destinatario==$nombrecargo)<a class="btn accion btn-info accion" href="{{route('Memosshow',$registro->id)}}"><i class="fa fa-eye"></i></a>
                              @endif
                             </center>
                           </td>

                           <td><center>
                            @if($registro->reservado != 1 || $registro->remitente==$nombrecargo || $registro->destinatario==$nombrecargo)
                            <button type="button" class="btn btn-primary" data-toggle="collapse" {!! "data-target='#demo".$indice."'" !!}   {!!"data-parent='#myTable".$registro->tramite_id."'" !!}
                           ><i class="fa fa-eye"></i></button>
                            @endif
                           </center></td>
                          </tr>
                        <tr  {!! "id='demo".$indice."'"  !!} class="collapse">
                        <td colspan="10" class="hiddenRow">
                          <table class="table table-striped table-sm" {!!  "id='myTable".$registro->tramite_id."'" !!}  >
                            <thead class="thead-primary">
                              <tr style="background-color: #fddadf;">
                              <tr style="background-color: #cd0b27;">
                              <th style="color:white">N° Trámite</th>
                              <th nowrap style="color:white">Fecha Colaboracion</th>
                              <th nowrap style="color:white">Colaborador</th>
                              <th nowrap style="color:white">Detalles</th>
                              </tr>

                              </tr>
                            </thead>
                          <tbody>
                             @foreach ($registro->colaboraciones as $indice1 => $registro2)
                            <tr>
                            <tr>
                            <td nowrap>{{$registro2->tramite_id}}</td>
                            <td nowrap>{{$registro2->created_at}}</td>
                            <td nowrap>{{$registro2->colaborador}}</td>
                            <td>
                             <a class="btn accion btn-info accion" href="{{route('MemosshowColaboraciones',$registro2->id)}}"><i class="fa fa-eye"></i></a>
                           </td>
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </td>
                    </tr>
                          @endforeach
                        @endif
                </tbody>
            </table>
          <br>
        </div>






      </div>
    </div>
</div>
<br>
@else
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">Listado de Memos Electrónicos</h5>
          </div>
          <div class="card-body table-responsive p-0">
              <center>  No se encontraron resultados.</center>
          <br>
        </div>
      </div>
    </div>
</div>
@endif






@endsection
@section('before-scripts-end')
@include('includes.scripts.script_fecha.scripts_filtro_fecha')
@stop
