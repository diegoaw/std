{{-- @Nombre del programa: Vista Principal de re->Tiempo Liquidación--}}
{{-- @Funcion: --}}
{{-- @Autor:btc  --}}
{{-- @Fecha Creacion: 01/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 01/01/19--}}
{{-- @Modificado por:    --}}


@extends ('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa-lock" style="font-size: 24px !important;"></i>Listado de Ordenes </h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-lock"></i> Mercado Publico</li>
        <li class="breadcrumb-item active">Listado Ordenes </li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content') 
@if (auth()->user()->hasPermission(['35-04-01-03']))
<div class="row">
<div class="col-md-12">
  <div class="card card-warning card-outline">
            <div class="card-header">
              <h3 class="card-title">Filtros de busqueda</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>
              </div>
            </div>

              <div class="card-body">
                  <div class="box-body">
                    <div class="col-sm-12">
                   


                    {!! Form::open(['route'=>'ordenesListado','method'=>'POST', 'class' => 'form-horizontal', 'role' =>'form']) !!}
                     <div class="form-group form-inline">

                      <div class="form-group form-inline  {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                      {!!Form::label('FechaDesde', 'Fecha desde', array('class' => 'col-sm-3 col-form-label'))!!}
                      <div class="col-sm-12">
                        <div class="input-group date">
                          <input type="text" class="form-control group-date" id="FechaDesde" name="FechaDesde" readonly="readonly" value="{!! date('Y-m-d', strtotime($fe_desde))  !!}">
                          <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="form-group form-inline  {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                      {!!Form::label('FechaHasta', 'Fecha hasta', array('class' => 'col-sm-3 col-form-label'))!!}
                      <div class="col-sm-12">
                        <div class="input-group date">
                          <input type="text" class="form-control group-date" id="FechaHasta" name="FechaHasta" readonly="readonly" value="{!! date('Y-m-d', strtotime($fe_hasta))  !!}">
                          <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                          </div>
                        </div>
                      </div>
                    </div>


              
                        <div class="form-inline">
                          {!!Form::label('Organismo', 'Organismo', array('class' => 'col-sm-3 control-label'))!!}
                          <div class="col-sm-12">
                           {!! Form::select('organismo',$organismos,$organismo,['id'=>'codigo','class'=>'form-control']) !!}
                          </div>
                        </div>
                   
                 

                  </div>

               
                
             
                

                   

                          <div class="form-group">
                              <div class="col-sm-12">
                                  <div class="pull-right">
                                  {!! Form::submit('Consultar', ['class' => 'btn btn-primary btn-sm','title' => 'Consultar']) !!} 
                                  
                                  </div>
                              </div>
                          </div>
{!! Form::close() !!} 

              </div>
      </div>
    </div>
  </div>
</div>
</div>
 
@endauth
<div class="row">
          <div class="col-md-12">
            <div class="card card-primary card-outline">


                    <div class="card-header">
                      <h3 class="card-title">
                        Listado de Ordenes
                      </h3>
                        <div class="card-tools">
                          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                          </button>
                        </div>
                    </div>

              <div class="card-body">
                  <div class="box-body">
                    <div class="col-sm-12">
                   


                   <div class="table-responsive">
                      <table class="table no-margin">
                        <thead>
                          <tr>
                            <th>N° </th>
                            <th>fecha </th>
                            <th>Codigo</th>
                            <th>Cod Organismo</th>
                            <th>Organismo</th>
                            <th>Nombre</th>
                            <th>Codigo Estado</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                          </tr>
                        </thead>
                        <tbody>
                          
                          @foreach($ordenes as $orden)
                          <tr>
                            <td>{{$orden->id}}</td>
                            <td>{{$orden->fecha}}</td>
                            <td>{{$orden->codigo}}</td>
                            <td>{{$orden->codigo_organismo}}</td>
                            <td>{{$orden->organismo->nombre_organismo}}</td>
                            <td>{{$orden->nombre}}</td>
                            <td>{{$orden->codigo_estado}}</td>
                            <td>{{$orden->detalles->estado }}</td>
                            <td> <a data-toggle="tooltip" data-placement="top" title="Ver detalles" class="btn accion btn-info accion" href="{{route('ordenDetalle',$orden->id)}}"><i class="fa fa-eye"></i></a></td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>


                     

              </div>
          </div>
       </div>
    </div>
  </div>
</div>



  @endsection
  @section('after-scripts-end')
  @include('includes.scripts.mercado_publico.scripts_filtro_fecha')
  @stop
