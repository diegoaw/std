{{-- @Nombre del programa: Vista Principal de re->Tiempo Liquidación--}}
{{-- @Funcion: --}}
{{-- @Autor:btc  --}}
{{-- @Fecha Creacion: 01/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 01/01/19--}}
{{-- @Modificado por:    --}}


@extends ('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa-lock" style="font-size: 24px !important;"></i> Ordenes </h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-lock"></i> Mercado Publico</li>
        <li class="breadcrumb-item active">Listado Ordenes </li>
        <li class="breadcrumb-item active">Detalle orden </li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content') 


    <div class="row">
          <div class="col-md-4">
            <div class="card card-primary card-outline">
                    <div class="card-header">
                      <h3 class="card-title">
                        Detalles Orden
                      </h3>
                        <div class="card-tools">
                          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                          </button>
                        </div>
                    </div>

                  <div class="card-body">
                    <div class="box-body">
                        <div class="col-sm-12">
                             


                      <div class="table-responsive">
                      <table class="table">
                        <tr>
                          <td> <strong> Nombre Completo : </strong> </td>
                          <td > {{$orden->nombre_completo}}</td>
                        </tr>
                        <tr>
                          <td> <strong> Descripcion :  </strong> </td>
                          <td > {{$orden->descripcion}}</td>
                        </tr>
                      
                         <tr>
                          <td> <strong> Estado : </strong> </td>
                          <td> {{$orden->codigo_estado}} - {{$orden->estado}}</td>
                        </tr>
                        <tr>
                          <td> <strong> Codigo de licitacion: </strong> </td>
                          <td > {{$orden->codigo_licitacion}}</td>
                        <tr>
                          <td> <strong> tipo : </strong></td>
                          <td > {{$orden->codigo_tipo}} -{{$orden->tipo}} - {{$orden->tipo_orden_compra}} </td>
                        </tr>
                    
                         <tr>
                          <td><strong> Justificacion : </strong></td>
                          <td> {{$orden->codigo_tipo_justificacion}} - {{$orden->tipo_justificacion}} </td>
                        </tr>
                        <tr>
                          <td><strong> Moneda : </strong></td>
                          <td> {{$orden->tipo_moneda}} </td>
                        </tr>
                        <tr>
                          <td><strong> Estado Proveedor : </strong></td>
                          <td> {{$orden->codigo_estado_provedor}}- {{$orden->estado_provedor}} </td>
                        </tr>
                        <tr>
                           <td><strong>Promedio Calificacion :</strong></td>
                          <td> {{$orden->promedio_calificacion}} </td>
                        </tr>
                        <tr>
                          <td><strong>Cantidad evaluacion :</strong></td>
                          <td> <strong>{{$orden->cantidad_evaluacion}}</strong> </td>
                        </tr>
                        <tr>
                        <td><strong>Pais :</strong></td>
                        <td> {{$orden->pais}} </td>
                        <tr>
                         </table>
                       </div>

                        </div>
                    </div>
                 </div>
    </div>
  </div>
            <div class="col-md-4">
            <div class="card card-primary card-outline">
                    <div class="card-header">
                      <h3 class="card-title">
                        Detalles Orden
                      </h3>
                        <div class="card-tools">
                          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                          </button>
                        </div>
                    </div>

                  <div class="card-body">
                    <div class="box-body">
                        <div class="col-sm-12">
                             


                      <div class="table-responsive">
                        <table class="table">
                         <tr>
                          <td><strong>Descuentos :</strong></td>
                          <td> {{$orden->descuentos}} </td>
                        </tr>
                        <tr>
                          <td><strong>Cargos :</strong></td>
                          <td> {{$orden->cargos}} </td>
                        </tr>
                        <tr>
                          <td><strong>Total neto :</strong></td>
                          <td>{{$orden->total_neto}} </td>
                        </tr>
                        <tr>
                          <td><strong>Porcentaje iva :</strong></td>
                          <td>{{$orden->porcentaje_iva}} </td>
                        </tr>
                        <tr>
                          <td><strong>Impuestos :</strong></td>
                          <td>{{$orden->impuestos}} </td>
                        </tr>
                        <tr>
                          <td><strong>Total :</strong></td>
                          <td>{{$orden->total}} </td>
                        </tr>
                         <tr>
                          <td><strong>Observaciones descuentos :</strong></td>
                          <td> {{$orden->observaciones_descuentos}} </td>
                        </tr>
                        <tr>
                          <td><strong>Observaciones cargos : </strong></td>
                          <td> {{$orden->observaciones_cargos}} </td>
                        </tr>
                  
                        
                         <tr>
                          <td><strong>Financiamiento :</strong></td>
                          <td> {{$orden->financiamiento}} </td>
                         </tr>
                        <tr>
                          <td><strong>Compromiso :</strong></td>
                          <td> {{$orden->id_compromiso}} </td>
                        </tr>
                         <tr>
                           <td><strong>Obra publica :</strong></td>
                          <td> {{$orden->obra_publica}} </td>
                        </tr>
                      
                        </table>

                        </div>
                    </div>
                 </div>
    </div>
  </div>
</div>

            <div class="col-md-4">
            <div class="card card-primary card-outline">
                    <div class="card-header">
                      <h3 class="card-title">
                        Detalles Orden
                      </h3>
                        <div class="card-tools">
                          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                          </button>
                        </div>
                    </div>

                  <div class="card-body">
                    <div class="box-body">
                        <div class="col-sm-12">
                             


                      <div class="table-responsive">
                        <table class="table">
                         <tr>
                          <td><strong>Fecha creacion :</strong></td>
                          <td> {{$ordenFechas->fecha_creacion}} </td>
                        </tr>
                        <tr>
                          <td><strong>Fecha aceptacion :</strong></td>
                          <td> {{$ordenFechas->fecha_aceptacion}} </td>
                        </tr>
                         <tr>
                          <td><strong>Fecha envio :</strong></td>
                          <td> {{$ordenFechas->fecha_envio}} </td>
                        </tr>
                        <tr>
                          <td><strong>Fecha cancelacion :</strong></td>
                          <td> {{$ordenFechas->fecha_cancelacion}} </td>
                        </tr>
                        <tr>
                          <td><strong>Fecha ultima modificacion :</strong></td>
                          <td> {{$ordenFechas->fecha_ultima_modificacion}} </td>
                        </tr>
                      
                        </table>

                        </div>
                    </div>
                 </div>
    </div>
  </div>
</div>
</div>

    <div class="row">
            <div class="col-md-12">
            <div class="card card-primary card-outline">
                    <div class="card-header">
                      <h3 class="card-title">
                        Detalles productos orden
                      </h3>
                        <div class="card-tools">
                          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                          </button>
                        </div>
                    </div>

                  <div class="card-body">
                    <div class="box-body">
                        <div class="col-sm-12">
                      <div class="table-responsive">
                        <table class="table">
                          <thead>
                            <tr>
                            <th>Correlativo</th>
                            <th>Categoria producto</th>
                            <th>Producto</th>
                            <th>Espeficicacion comprador</th>
                            <th>Espeficicacion proveedor</th>
                            <th>Cantidad</th>
                            <th>Unidad</th>
                            <th>Moneda</th>
                            <th>Precio neto</th>
                            <th>Cargos</th>
                            <th>Descuentos</th>
                            <th>Impuestos</th>
                            <th>total</th>
                          </tr>
                          </thead>
                        <tbody>
                        @foreach($ordenProducto as $item)
                        <tr>
                          <td>{{$item->correlativo_productos}}</td>
                          <td>{{$item->codigo_categoria_producto}}-{{$item->categoria_producto}}</td>
                          <td>{{$item->codigo_producto}}-{{$item->producto}}</td>
                          <td>{{$item->especificacion_comprador}}</td>
                          <td>{{$item->especificacion_proveedor}}</td>
                          <td>{{$item->cantidad_items}}</td>
                          <td>{{$item->unidad_producto}}</td>
                          <td>{{$item->moneda_producto}}</td>
                          <td>{{$item->precio_neto_producto}}</td>
                          <td>{{$item->total_cargos_producto}}</td>
                          <td>{{$item->total_descuentos_producto}}</td>
                          <td>{{$item->total_impuestos_producto}}</td>
                          <td>{{$item->total_producto}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                        </table>

                        </div>
                    </div>
                 </div>
    </div>
  </div>
</div>
</div>

 <div class="row">
          <div class="col-md-6">
            <div class="card card-primary card-outline">
                    <div class="card-header">
                      <h3 class="card-title">
                        Detalles Proveedor
                      </h3>
                        <div class="card-tools">
                          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                          </button>
                        </div>
                    </div>

                  <div class="card-body">
                    <div class="box-body">
                        <div class="col-sm-12">
                             


                     <div class="table-responsive">
                        <table class="table">
                         <tr>
                          <td><strong>Proveedor :</strong></td>
                          <td> {{$ordenProvedor->codigo_proveedor}}-{{$ordenProvedor->nombre_proveedor}} </td>
                        </tr>
                        <tr>
                          <td><strong>Rut unidad proveedor :</strong></td>
                          <td> {{$ordenProvedor->rut_sucursal_proveedor}} </td>
                        </tr>
                        <tr>
                          <td><strong>unidad proveedor :</strong></td>
                          <td>{{$ordenProvedor->codigo_sucursal_proveedor}} - {{$ordenProvedor->nombre_sucursal_proveedor}} </td>
                        </tr>
                        <tr>
                          <td><strong>Actividad proveedor :</strong></td>
                          <td>{{ substr($ordenProvedor->actividad_proveedor ,0,15 ) }} ...</td>
                        </tr>
                        <tr>
                          <td><strong>Direcccion unidad de compra :</strong></td>
                          <td>{{$ordenProvedor->direccion_unidad_proveedor}} </td>
                        </tr>
                        <tr>
                          <td><strong>Comuna :</strong></td>
                          <td>{{$ordenProvedor->comuna_unidad_proveedor}} </td>
                        </tr>
                         <tr>
                          <td><strong>Region :</strong></td>
                          <td> {{$ordenProvedor->region_unidad_proveedor}} </td>
                        </tr>
                        <tr>
                          <td><strong>Pais : </strong></td>
                          <td> {{$ordenProvedor->pais_unidad_proveedor}} </td>
                        </tr>
                  
                        
                         <tr>
                          <td><strong>Nombre contacto :</strong></td>
                          <td>{{$ordenProvedor->nombre_contacto_unidad_proveedor }} </td>
                         </tr>
                        <tr>
                          <td><strong>Cargo contacto :</strong></td>
                          <td>{{$ordenProvedor->cargo_contacto_unidad_proveedor }} </td>
                        </tr>
                         <tr>
                           <td><strong>tlfn contacto :</strong></td>
                          <td> {{$ordenProvedor->tlfn_contacto_unidad_proveedor }}} </td>
                        </tr>
                         <tr>
                           <td><strong>email contacto :</strong></td>
                          <td> {{$ordenProvedor->email_contacto_unidad_proveedor }}} </td>
                        </tr>
                      
                        </table>

                        </div>

                        </div>
                    </div>
                 </div>
    </div>
  </div>
            <div class="col-md-6">
            <div class="card card-primary card-outline">
                    <div class="card-header">
                      <h3 class="card-title">
                        Detalles Comprador
                      </h3>
                        <div class="card-tools">
                          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                          </button>
                        </div>
                    </div>

                  <div class="card-body">
                    <div class="box-body">
                        <div class="col-sm-12">
                             


                      <div class="table-responsive">
                        <table class="table">
                         <tr>
                          <td><strong>Comprador :</strong></td>
                          <td> {{$ordenComprador->codigo_comprador}}-{{$ordenComprador->nombre_comprador}} </td>
                        </tr>
                        <tr>
                          <td><strong>Rut unidad comprador :</strong></td>
                          <td> {{$ordenComprador->rut_unidad_comprador}} </td>
                        </tr>
                        <tr>
                          <td><strong>unidad comprador :</strong></td>
                          <td>{{$ordenComprador->codigo_unidad_comprador}} - {{$ordenComprador->nombre_unidad_comprador}} </td>
                        </tr>
                        <tr>
                          <td><strong>Actividad Comprador :</strong></td>
                          <td>{{$ordenComprador->actividad_comprador}} </td>
                        </tr>
                        <tr>
                          <td><strong>Direcccion unidad de compra :</strong></td>
                          <td>{{$ordenComprador->direccion_unidad_comprador}} </td>
                        </tr>
                        <tr>
                          <td><strong>Comuna :</strong></td>
                          <td>{{$ordenComprador->comuna_unidad_comprador}} </td>
                        </tr>
                         <tr>
                          <td><strong>Region :</strong></td>
                          <td> {{$ordenComprador->region_unidad_comprador}} </td>
                        </tr>
                        <tr>
                          <td><strong>Pais : </strong></td>
                          <td> {{$ordenComprador->pais_unidad_comprador}} </td>
                        </tr>
                  
                        
                         <tr>
                          <td><strong>Nombre contacto :</strong></td>
                          <td>{{$ordenComprador->nombre_contacto_unidad_comprador }} </td>
                         </tr>
                        <tr>
                          <td><strong>Cargo contacto :</strong></td>
                          <td>{{$ordenComprador->cargo_contacto_unidad_comprador }} </td>
                        </tr>
                         <tr>
                           <td><strong>tlfn contacto :</strong></td>
                          <td> {{$ordenComprador->tlfn_contacto_unidad_comprador }}} </td>
                        </tr>
                         <tr>
                           <td><strong>email contacto :</strong></td>
                          <td> {{$ordenComprador->email_contacto_unidad_comprador }}} </td>
                        </tr>
                      
                        </table>

                        </div>
                    </div>
                 </div>
    </div>
  </div>
</div>

 
</div>



  @endsection
  @section('after-scripts-end')
  @stop
