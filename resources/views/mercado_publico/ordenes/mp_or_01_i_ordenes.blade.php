{{-- @Nombre del programa: Vista Principal de re->Tiempo Liquidación--}}
{{-- @Funcion: --}}
{{-- @Autor:btc  --}}
{{-- @Fecha Creacion: 01/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 01/01/19--}}
{{-- @Modificado por:    --}}


@extends ('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa-lock" style="font-size: 24px !important;"></i> Ordenes </h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-lock"></i> Mercado Publico</li>
        <li class="breadcrumb-item active">Ordenes </li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content') 
@if (auth()->user()->hasPermission(['35-04-01-03']))
<div class="row">
<div class="col-md-12">
  <div class="card card-warning card-outline">
            <div class="card-header">
              <h3 class="card-title">Acciones</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>
              </div>
            </div>

              <div class="card-body">
                  <div class="box-body">

                    <div class="col-sm-6">
                   


                    {!! Form::open(['route'=>'ordenesbuscar','method'=>'POST', 'class' => 'form-horizontal', 'role' =>'form']) !!}

                     <div class="form-group form-row">
                      <div class="form-group form-row {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                      {!!Form::label('FechaDesde', 'Fecha desde', array('class' => 'col-sm-3 col-form-label'))!!}
                      <div class="col-sm-6">
                        <div class="input-group date">
                          <input type="text" class="form-control group-date" id="FechaDesde" name="FechaDesde" readonly="readonly" value="{!! date('Y-m-d', strtotime($fe_desde))  !!}">
                          <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="form-group form-row">
                      <div class="form-group form-row {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                      {!!Form::label('FechaHasta', 'Fecha hasta', array('class' => 'col-sm-3 col-form-label'))!!}
                      <div class="col-sm-6">
                        <div class="input-group date">
                          <input type="text" class="form-control group-date" id="FechaHasta" name="FechaHasta" readonly="readonly" value="{!! date('Y-m-d', strtotime($fe_hasta))  !!}">
                          <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                

                    <div class="row">
                      <div class="col-md-8">
                        <div class="form-inline">
                          {!!Form::label('Organismo', 'Organismo', array('class' => 'col-sm-2 control-label'))!!}
                          <div class="col-sm-8">
                           {!! Form::select('organismo',$organismos,['id'=>'codigo','class'=>'form-control']) !!}
                          </div>
                        </div>
                      </div>
                    {!! Form::close() !!} 

                          <div class="form-group">
                              <div class="col-sm-12">
                                  <div class="pull-right">
                                  {!! Form::submit('Guardar', ['class' => 'btn btn-primary btn-sm','title' => 'Guardar']) !!} 
                                  <a href="{{url('MercadoPublico/ordenes//BD/clear')}}" class="btn btn-primary btn-success margin " role="button">Clear BD</a>
                                  </div>
                              </div>
                          </div>
                  </div>
              </div>
      </div>
  </div>
</div>
@endauth
@if($mostrar)
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card card-primary card-outline">
        <div class="card-header">
          <h3 class="card-title">
            Resultado
          </h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
        </div>
        <div class="card-body">
              codigo de importacion {{ $CodImportacion }} - Se han importado un total de {{$contadorOrdenes}} ordenes de compra a la base de datos correspondientes a las fechas {{ $fe_desde }} hasta {{$fe_hasta}}
            <br>
            @if($contadorError>0)
              han ocurrido algunos errores durante la importacion , peticiones a la api ,  para mas detalle descargue el archivo de log   <a title="Descargar" target="_blank" class="btn btn-success" href="{{route('descargarLog' , $CodImportacion)}}"></i> Descargar</a>
            @endif
            <br>
        </div>
      </div>
    </div>
  </div>
</div>
@endif
</div>
</div> 


  @endsection
  @section('after-scripts-end')
  @include('includes.scripts.mercado_publico.scripts_filtro_fecha')
  @stop
