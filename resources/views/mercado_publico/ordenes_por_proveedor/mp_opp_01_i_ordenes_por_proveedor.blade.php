{{-- @Nombre del programa: Vista Principal de re->Tiempo Liquidación--}}
{{-- @Funcion: --}}
{{-- @Autor:btc  --}}
{{-- @Fecha Creacion: 01/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 01/01/19--}}
{{-- @Modificado por:    --}}


@extends ('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa-lock" style="font-size: 24px !important;"></i> Ordenes por proveedor</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-lock"></i> Mercado Publico</li>
        <li class="breadcrumb-item active">Ordenes por proveedor</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content') 
@if (auth()->user()->hasPermission(['35-04-01-03']))
<div class="row">
<div class="col-md-12">
  <!-- Line chart -->
  <div class="card card-warning card-outline">
    <div class="card-header">
      <h3 class="card-title">Acciones</h3>

      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="card-body">
        <div class="box-body">
                <div class="form-group">
                    <div class="col-sm-3 offset-sm-6">
                        <div class="pull-right">
                        <a title="Crear usuario" class="btn btn-success" href="{{route('ordenesporproveedorbuscar')}}"></i> Buscar ordenes por proveedor</a>
                        </div>
                    </div>
                </div>
    </div>
  </div>
</div>
</div>
@endauth
 
<div class="row">
    <div class="col-md-12">
     
    </div>
</div> 


  @endsection
  @section('after-scripts-end')
    @include('includes.scripts.seguridad.scripts_complejidad')
    @include('includes.scripts.scripts_ordenamiento_tablas')
  @stop
