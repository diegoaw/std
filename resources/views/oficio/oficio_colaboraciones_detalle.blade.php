
@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-5">
      <h1><i class="fa fa-user"></i> Oficio Electrónico Colaboraciones</h1>
    </div>
    <div class="col-sm-7">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-user"></i> Oficio Electrónico Colaboraciones</li>
        <li class="breadcrumb-item active">Detalles</li>
      </ol>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <!-- Line chart -->
    <div class="card card-danger card-outline">
      <div class="card-header">
        <h3 class="card-title">Detalles Oficio Electrónico N° @if($detalle->correlativo=='') {{$detalle->correlativo}} @else {{$detalle->correlativo->correlativo}} @endif</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <table id="example1" class="table table-bordered table-striped table-hover table-sm">
          <tr>
            <th>Fecha</th>
            <td>{{$detalle->fecha}}</td>
          </tr>
          <tr>
            <th>Trámite N°</th>
            <td>{{$detalle->tramite_id}}</td>
          </tr>
          <tr>
            <th>Correlativo N°</th>
            @if($detalle->correlativo=='')
            <td>{{$detalle->correlativo}}</td>
            @else
            <td>{{$detalle->correlativo->correlativo}}</td>
            @endif
          </tr>
          <tr>
            <th>Lugar</th>
            <td>{{$detalle->lugar}}</td>
          </tr>
          <tr>
            <th>Remitente</th>
            <td>{{$detalle->remitente}}</td>
          </tr>
          <tr>
            <th>Destinatario</th>
            <td>{{$detalle->destinatario}}</td>
          </tr>
          <tr>
            <th>Antecedentes</th>
            <td>{!!$detalle->antecedentes!!}</td>
          </tr>
          <tr>
            <th>Materia</th>
            <td>{!!$detalle->materia!!}</td>
          </tr>
          <tr>
            <th>Contenido</th>
            <td>{!!$detalle->contenido!!}</td>
          </tr>
          <tr>
            <th>Vistos de Resposabilidad</th>
            <td>{{$detalle->vistos_responsabilidad}}</td>
          </tr>
          <tr>
            <th>Distribucion</th>
            <td >{!!$detalle->distribucion!!}</td>
          </tr>
             <tr>
            <th>Colaborador</th>
            <td>{{$detalle->colaborador}}</td>
          </tr>
             <tr>
            <th>Observación general</th>
            <td>{{$detalle->observacion_general}}</td>
          </tr>
          <tr>
            <th>Lista de colaboradores</th>
            <td>{!!$detalle->lista_colaboradores_aux !!}</td>
          </tr>
          <tr>
            <th>Anexos</th>
            <td>
             
                @if($detalle->doc_anexo_a) 
                <a href="{{$detalle->doc_anexo_a}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
                @endif 
                @if($detalle->doc_anexo_b) 
                <a href="{{$detalle->doc_anexo_b}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
                @endif 
                @if($detalle->doc_anexo_c) 
                <a href="{{$detalle->doc_anexo_c}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
                @endif
                @if($detalle->doc_anexo_d)
                <a href="{{$detalle->doc_anexo_d}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
                @endif 
          
          </tr>
          
        </table>
      </div>

        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! link_to_route('OficiosColaboraciones', 'Volver', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
            </div>
          </div>
        </div>



    </div>
  </div>
</div>


@endsection
@section('before-scripts-end')
@stop
