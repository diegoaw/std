
@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-5">
      <h1><i class="fa fa-user"></i> Oficio Electrónico </h1>
    </div>
    <div class="col-sm-7">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-user"></i> Oficio Electrónico </li>
        <li class="breadcrumb-item active">Detalles</li>
      </ol>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <!-- Line chart -->
    <div class="card card-danger card-outline">
      <div class="card-header">
        <h3 class="card-title">Detalles Oficio Electrónico N° {{$detalle->correlativo}}</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <table id="example1" class="table table-bordered table-striped table-hover table-sm">
          <tr>
            <th>Fecha</th>
            <td>{{$detalle->fecha}}</td>
          </tr>
          <tr>
            <th>Trámite N°</th>
            <td>{{$detalle->tramite_id}}</td>
          </tr>
          <tr>
            <th>Correlativo N°</th>
            <td>{{$detalle->correlativo}}</td>
          </tr>
          <tr>
            <th>Lugar</th>
            <td>{{$detalle->lugar}}</td>
          </tr>
          <tr>
            <th>Remitente</th>
            <td>{{$detalle->remitente}}</td>
          </tr>
          <tr>
            <th>División</th>
            <td>{{$detalle->division}} </td>
          </tr>
          <tr>
            <th>Destinatario</th>
            <td>{{$detalle->destinatario}}</td>
          </tr>
          <tr>
            <th>Antecedentes</th>
            <td>{!!$detalle->antecedentes!!}</td>
          </tr>
          <tr>
            <th>Materia</th>
            <td>{!!$detalle->materia!!}</td>
          </tr>
          <tr>
            <th>Contenido</th>
            <td>{!!$detalle->contenido!!}</td>
          </tr>
          <tr>
            <th>Vistos de Resposabilidad</th>
            <td>{{$detalle->vistos_responsabilidad}}</td>
          </tr>
          <tr>
            <th>Distribución</th>
            <td>{!!$detalle->distribucion!!}</td>
          </tr>
          <tr>
            <th>Oficio Electrónico</th>
            <td>
            @if($detalle->doc_oficio)
            <a href="{{$detalle->doc_oficio}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
            @endif
          </td>
          </tr>
          <tr>
            <th>Anexos</th>
            <td>

                @if($detalle->doc_anexo_a)
                <a href="{{$detalle->doc_anexo_a}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
                @endif
                @if($detalle->doc_anexo_b)
                <a href="{{$detalle->doc_anexo_b}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
                @endif
                @if($detalle->doc_anexo_c)
                <a href="{{$detalle->doc_anexo_c}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
                @endif
                @if($detalle->doc_anexo_d)
                <a href="{{$detalle->doc_anexo_d}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
                @endif
          </tr>
          </tr>
        </table>
      </div>


        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              <!--@if (auth()->user()->hasPermission(['99-01']))
              <a data-toggle="tooltip" data-placement="left" title="Eliminar" class="btn btn-sm btn-primary" href="#" OnClick="Eliminar('{{$detalle->tramite_id}}')"><i class="fa fa-trash"></i></a>
              @endif-->
              {!! link_to_route('Oficios2', 'Volver', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
              <!-- <a class="btn accion btn-success accion" href="{{route('OficiosDistribucion2', $detalle->tramite_id)}}">Reenviar Oficio Electrónico</a> -->
              <!--@if (auth()->user()->hasPermission(['99-02']))
              <a data-toggle="tooltip" data-placement="left" title="Restaurar" class="btn btn-sm btn-success" href="{{route('Oficiosrestaurar', $detalle->tramite_id)}}"><i class="fa fa-pencil"></i></a>
              @endif-->
            </div>
          </div>
        </div>


    </div>
  </div>
</div>


@endsection
@section('before-scripts-end')
@include('includes.scripts.tramites')
@stop
