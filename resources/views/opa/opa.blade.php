@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><b> Unidad de Gestión Documental</b></h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
<link rel="stylesheet" herf="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css">

@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header card-primary card-outline">
          <h3>Entrada/Salida de Elementos Físicos y Digitales en Gestión Documental</h3>
          <div class="form-group">
            <div class="col-sm-12">
              <div class="text-right">
                @if (auth()->user()->hasPermission(['11-02']))
                  {!! link_to_route('opa_crear', 'Nuevo Ingreso/Salida', [], ['class' => 'btn btn-success btn-sm','title'=>'Nuevo Ingreso/Salida']) !!}
                @endif
              </div>
            </div>
        </div>
        <!-- /.card-header -->
    
        <div class="card-body table-responsive p-0">
            <table class="table table-striped table-bordered" id="example">
              <thead >
                  <tr style="background-color:#013273;">
                      <th nowrap style="color:white; width:80px"><center>ID &nbsp;<i class="fa fa-sort" aria-hidden="true"></i></center></th>
                      <th nowrap style="color:white; width:100px"><center>Tipo &nbsp;<i class="fa fa-sort" aria-hidden="true"></i></center></th>
                      <th nowrap style="color:white; width:100px"><center>Clasificación &nbsp;<i class="fa fa-sort" aria-hidden="true"></i></center></th>
                      <th nowrap style="color:white; width:100px"><center>De &nbsp;<i class="fa fa-sort" aria-hidden="true"></i></center></th>
                      <th nowrap style="color:white; width:100px"><center>Para &nbsp;<i class="fa fa-sort" aria-hidden="true"></i></center></th>
                      <th nowrap style="color:white; width:100px"><center>Descripción &nbsp;<i class="fa fa-sort" aria-hidden="true"></i></center></th>
                      <th style="color:white; width:80px"><center>STD/DocDigital &nbsp;<i class="fa fa-sort" aria-hidden="true"></i></center></th>
                      <th style="color:white; width:100px"><center>Fecha/Hora Entrada &nbsp;<i class="fa fa-sort" aria-hidden="true"></i></center></th>
                      <th style="color:white; width:100px"><center>Fecha/Hora Actualizado &nbsp;<i class="fa fa-sort" aria-hidden="true"></i></center></th>
                      <th style="color:white; width:100px"><center>Fecha/Hora Salida &nbsp;<i class="fa fa-sort" aria-hidden="true"></i></center></th>
                      <th nowrap style="color:white; width:100px"><center>Estado &nbsp;<i class="fa fa-sort" aria-hidden="true"></i></center></th>
                  </tr>
              </thead>
              <tbody class="panel">
              @if(count($dataDefinitiva)>0)
                    @foreach ($dataDefinitiva as $indice => $tramite)
                        <tr>
                         <td style="width:80px"><center>{{$tramite->id}}</center></td>
                         <td style="width:100px"><center>{{$tramite->OpaTipo->nombre}}</center></td>
                         <td style="width:100px"><center>{{$tramite->OpaClasificacion->nombre}}</center></td>
                         <td style="width:100px"><center>{{$tramite->de}}</center></td>
                         <td style="width:100px"><center>{{$tramite->para}}</center></td>
                         <td style="width:100px"><center>{{$tramite->descripcion}}</center></td>
                         <td style="width:80px"><center>{{$tramite->n_doc}}</center></td>
                         <td style="width:100px"><center>{{$tramite->created_at}}</center></td>
                         <td style="width:100px"><center>{{$tramite->updated_at}}</center></td>
                         <td style="width:100px"><center>{{$tramite->fecha_cierre}} </center></td>
                         <td style="width:100px"><center>
                           @if($tramite->estado == 'En Proceso')
                            @if (auth()->user()->hasPermission(['11-02']))
                              <a class="btn accion btn-danger accion" href="{{route('opa_actualizar',$tramite->id)}}">Actualizar</a>
                              @else
                                <b style="color:#6b0511;">Pendiente por opa Actualizar</b>
                              @endif
                           @else
                                 <b style="color:#076b17;">{{$tramite->estado}}</b>
                           @endif
                         </center></td>
                        </tr>
                    @endforeach
              @endif
              </tbody>
            </table>
        </div>
    </div>
  </div>
</div>

@endsection
@section('before-scripts-end')
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap4.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable( {
        "language": {
            "lengthMenu": "Cantidad Registros por Páginas _MENU_   ",
            "zeroRecords": "No se Encontro Registro",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No se Encontro Registro",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "search": "Buscar:",
            "paginate": {
              "previous": "Anterior",
              "next": "Siguiente"
            }
        }
    } );
} );
</script>
@stop
