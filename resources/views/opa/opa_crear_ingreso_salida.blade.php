@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-clipboard"></i><b> Unidad de Gestión Documental - Crear Entrada/Salida de Físicos en Gestión Documental</b></h1><br>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
{!! Form::open(['route' => 'opa_nuevo', 'files' => true, 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','id'=>'form-create']) !!}
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-danger card-outline">
        <h3 class="card-title">Crear Entrada/Salida</h3>
      </div>
      <div class="card-body">

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>

        <div class="callout col-sm-8 offset-sm-2" style="color: #737373;">
          <h4 style="font-size: 14px;"><i class="fa fa-info-circle" style="color: #ff0000;"></i> Nota:</h4>
          <span>Los campos marcados con un asterisco (*) son obligatorios.</span>
        </div><br>

        <div class="form-group row">
          {!! Form::label('Tipo (*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            @foreach($radioTipo as $indice => $tipor)
            <label class="radio-inline">
                {{ Form::radio('id_opa_tipo', $tipor->id) }} {{$tipor->nombre}}
            </label>
            @endforeach
          </div>
        </div>

        <div class="form-group row" id="clasificacion1" style="display:none;">
          {!! Form::label('Clasificación (*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            @foreach($radioClasificacion as $indice => $clasificacionr)
            <label class="radio-inline">
                {{ Form::radio('id_opa_clasificacion', $clasificacionr->id) }} {{$clasificacionr->nombre}}
            </label>
            @endforeach
          </div>
        </div>

        <div class="form-group row" id="clasificacion2" style="display:none;">
          {!! Form::label('Clasificación (*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            @foreach($radioClasificacion as $indice => $clasificacionr)
            <label class="radio-inline">
                {{ Form::radio('id_opa_clasificacion2', $clasificacionr->id) }} {{$clasificacionr->nombre}}
            </label>
            @endforeach
          </div>
        </div>

        <!-- Cuando es Documentación -->

        <div class="form-group row" id="tipodoc1" style="display:none;">
          {!! Form::label('Tipo Documentación(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            @foreach($radioTipoDoc as $indice => $tipodocr)
            <label class="radio-inline">
                {{ Form::radio('id_opa_tipo_documentacion', $tipodocr->id) }} {{$tipodocr->nombre}}
            </label>
            @endforeach
          </div>
        </div>

        <div class="form-group row" id="modalidad1" style="display:none;">
          {!! Form::label('Modalidad(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            @foreach($radioModalidad as $indice => $modalidadr)
            <label class="radio-inline">
                {{ Form::radio('id_opa_modalidad_documento', $modalidadr->id) }} {{$modalidadr->nombre}}
            </label>
            @endforeach
          </div>
        </div>

        <div class="form-group row" id="plataforma1" style="display:none;">
          {!! Form::label('Plataforma(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            @foreach($radioPlataforma as $indice => $plataformar)
            <label class="radio-inline">
                {{ Form::radio('id_opa_plataforma_documento', $plataformar->id) }} {{$plataformar->nombre}}
            </label>
            @endforeach
          </div>
        </div>

        <div class="form-group row" id="n_doc1" style="display:none;">
            {!! Form::label('N° Trámite / Folio', 'N° Trámite / Folio(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('n_doc', null, ['class' => 'form-control', 'placeholder' => 'N° Trámite / Folio']) !!}
          </div>
        </div>

        <!-- fin Cuando es Documentación -->

        <div class="form-group row" id="de1" style="display:none;">
            {!! Form::label('De', 'De(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('de', null, ['class' => 'form-control', 'placeholder' => 'De']) !!}
          </div>
        </div>

        <div class="form-group row" id="de_correo1" style="display:none;">
            {!! Form::label('De Correo', 'De Correo(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('de_correo', null, ['class' => 'form-control', 'placeholder' => 'De Correo']) !!}
          </div>
        </div>

        <div class="form-group row" id="para1" style="display:none;">
            {!! Form::label('Para', 'Para(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('para', null, ['class' => 'form-control', 'placeholder' => 'Para']) !!}
          </div>
        </div>

        <div class="form-group row" id="para_correo1" style="display:none;">
            {!! Form::label('Para Correo', 'Para Correo(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('para_correo', null, ['class' => 'form-control', 'placeholder' => 'Para Correo']) !!}
          </div>
        </div>

        <div class="form-group row" id="descripcion1" style="display:none;">
            {!! Form::label('Descripcion', 'Descripcion(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'placeholder' => 'Descripcion']) !!}
          </div>
        </div>

        <!-- Cuando es Encomienda -->

        <div class="form-group row" id="prioridad1" style="display:none;">
          {!! Form::label('alta_prioridad','Alta Prioridad', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::checkbox('alta_prioridad', '1') !!}
          </div>
        </div>

        <div class="form-group row" id="empaquetado1" style="display:none;">
          {!! Form::label('empaquetado','Requirió Empaquetado', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::checkbox('empaquetado', '1') !!}
          </div>
        </div>


        <!-- Cuando es Documentación -->

        <div class="form-group row" id="en_ruta1" style="display:none;">
          {!! Form::label('en_ruta','En Ruta', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::checkbox('en_ruta', '1') !!}
          </div>
        </div>


        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! Form::submit('Guardar', ['class' => 'btn btn-success btn-sm','title'=>'Guardar']) !!}
              {!! link_to_route('opa', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) !!}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{!! Form::close() !!}
@stop
@section('after-scripts-end')
<script>
$(document).ready(function(){
  $('input[type=radio][name=id_opa_tipo]').on('change', function(){
    let tipo = this.value;
    if(tipo == 1){
      $("#clasificacion1").css("display", "flex");
      $("#clasificacion2").css("display", "none");
      $("#tipodoc1").css("display", "none");
      $("#modalidad1").css("display", "none");
      $("#de1").css("display", "flex");
      $("#de_correo1").css("display", "flex");
      $("#para1").css("display", "flex");
      $("#para_correo1").css("display", "flex");
      $("#descripcion1").css("display", "flex");
      $("#retirado1").css("display", "flex");
      $("#plataforma1").css("display", "none");
      $("#n_doc1").css("display", "none");
      $("#en_ruta1").css("display", "none");
      $("#enviado1").css("display", "none");

    }
    if(tipo == 2){
      $("#clasificacion2").css("display", "flex");
      $("#clasificacion1").css("display", "none");
      $("#tipodoc1").css("display", "flex");
      $("#de1").css("display", "none");
      $("#de_correo1").css("display", "none");
      $("#para1").css("display", "none");
      $("#para_correo1").css("display", "none");
      $("#descripcion1").css("display", "none");
      $("#retirado1").css("display", "none");
      $("#empaquetado1").css("display", "none");
      $("#prioridad1").css("display", "none");
    }
  });
});

$(document).ready(function(){
  $('input[type=radio][name=id_opa_clasificacion]').on('change', function(){
    let clasificacion = this.value;
    if(clasificacion == 1){
      $("#prioridad1").css("display", "flex");
      $("#empaquetado1").css("display", "none");
    }
    if(clasificacion == 2){
      $("#empaquetado1").css("display", "flex");
      $("#prioridad1").css("display", "none");

    }
  });
});

$(document).ready(function(){
  $('input[type=radio][name=id_opa_modalidad_documento]').on('change', function(){
    let modalidad = this.value;
    if(modalidad == 1){
      $("#de1").css("display", "flex");
      $("#de_correo1").css("display", "flex");
      $("#para1").css("display", "flex");
      $("#para_correo1").css("display", "flex");
      $("#descripcion1").css("display", "flex");
      $("#en_ruta1").css("display", "flex");
      $("#enviado1").css("display", "flex");
      $("#plataforma1").css("display", "none");
      $("#n_doc1").css("display", "none");
    }
    if(modalidad == 2){
      $("#de1").css("display", "flex");
      $("#de_correo1").css("display", "flex");
      $("#para1").css("display", "flex");
      $("#para_correo1").css("display", "flex");
      $("#descripcion1").css("display", "none");
      $("#en_ruta1").css("display", "none");
      $("#enviado1").css("display", "none");
      $("#plataforma1").css("display", "flex");
      $("#n_doc1").css("display", "flex");

    }
  });
});

$(document).ready(function(){
  $('input[type=radio][name=id_opa_tipo_documentacion]').on('change', function(){
    let tipodoc = this.value;
    if(tipodoc == 1){
      $("#modalidad1").css("display", "flex");
      $("#plataforma1").css("display", "none");
      $("#n_doc1").css("display", "none");
    }
    if(tipodoc == 2){
      $("#de1").css("display", "flex");
      $("#de_correo1").css("display", "flex");
      $("#para1").css("display", "flex");
      $("#para_correo1").css("display", "flex");
      $("#descripcion1").css("display", "none");
      $("#en_ruta1").css("display", "none");
      $("#enviado1").css("display", "none");
      $("#modalidad1").css("display", "none");
      $("#plataforma1").css("display", "flex");
      $("#n_doc1").css("display", "flex");

    }
  });
});
</script>
@endsection
