@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-clipboard"></i><b> Unidad de Gestión Documental - Actualizar Entrada/Salida de Físicos en Gestión Documental</b></h1><br>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
{!! Form::open(['route' => ['opa_update' , $editOpa->id], 'files' => true, 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','id'=>'form-create']) !!}

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-danger card-outline">
        <h3 class="card-title">Actualizar Entrada/Salida</h3>
      </div>
      <div class="card-body">

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>

        <div class="callout col-sm-8 offset-sm-2" style="color: #737373;">
          <h4 style="font-size: 14px;"><i class="fa fa-info-circle" style="color: #ff0000;"></i> Nota:</h4>
          <span>Los campos marcados con un asterisco (*) son obligatorios.</span>
        </div><br>

        <div class="form-group row">
          {!! Form::label('Tipo (*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            @foreach($radioTipo as $indice => $tipor)
            <label class="radio-inline">
                {{ Form::radio('id_opa_tipo', $tipor->id, $editOpa->id_opa_tipo == $tipor->id, ['disabled']) }} {{$tipor->nombre}}
            </label>
            @endforeach
          </div>
        </div>

        @if($editOpa->id_opa_tipo == 1)
        <div class="form-group row">
          {!! Form::label('Clasificación (*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            @foreach($radioClasificacion as $indice => $clasificacionr)
            <label class="radio-inline">
                {{ Form::radio('id_opa_clasificacion', $clasificacionr->id, $editOpa->id_opa_clasificacion == $clasificacionr->id, ['disabled']) }} {{$clasificacionr->nombre}}
            </label>
            @endforeach
          </div>
        </div>
        @endif
        @if($editOpa->id_opa_tipo == 2)
        <div class="form-group row">
          {!! Form::label('Clasificación (*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            @foreach($radioClasificacion as $indice => $clasificacionr)
            <label class="radio-inline">
                {{ Form::radio('id_opa_clasificacion2', $clasificacionr->id, $editOpa->id_opa_clasificacion == $clasificacionr->id, ['disabled'])}} {{$clasificacionr->nombre}}
            </label>
            @endforeach
          </div>
        </div>
        @endif
        <!-- Cuando es Documentación -->
        @if($editOpa->id_opa_tipo == 2)
        <div class="form-group row">
          {!! Form::label('Tipo Documentación(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            @foreach($radioTipoDoc as $indice => $tipodocr)
            <label class="radio-inline">
                {{ Form::radio('id_opa_tipo_documentacion', $tipodocr->id, $editOpa->id_opa_tipo_documentacion == $tipodocr->id, ['disabled']) }} {{$tipodocr->nombre}}
            </label>
            @endforeach
          </div>
        </div>
        @endif
        @if($editOpa->id_opa_tipo == 2 && $editOpa->id_opa_tipo_documentacion == 1)
        <div class="form-group row">
          {!! Form::label('Modalidad(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            @foreach($radioModalidad as $indice => $modalidadr)
            <label class="radio-inline">
                {{ Form::radio('id_opa_modalidad_documento', $modalidadr->id, $editOpa->id_opa_modalidad_documento == $modalidadr->id, ['disabled']) }} {{$modalidadr->nombre}}
            </label>
            @endforeach
          </div>
        </div>
        @endif
        @if($editOpa->id_opa_tipo == 2 && $editOpa->id_opa_tipo_documentacion  == 2 || $editOpa->id_opa_tipo == 2 && $editOpa->id_opa_modalidad_documento  == 2)
        <div class="form-group row">
          {!! Form::label('Plataforma(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            @foreach($radioPlataforma as $indice => $plataformar)
            <label class="radio-inline">
                {{ Form::radio('id_opa_plataforma_documento', $plataformar->id, $editOpa->id_opa_plataforma_documento == $plataformar->id , ['disabled']) }} {{$plataformar->nombre}}
            </label>
            @endforeach
          </div>
        </div>
        @endif
        @if($editOpa->id_opa_tipo == 2 && $editOpa->id_opa_tipo_documentacion  == 2 || $editOpa->id_opa_tipo == 2 && $editOpa->id_opa_modalidad_documento  == 2)
        <div class="form-group row">
            {!! Form::label('N° Trámite / Folio', 'N° Trámite / Folio(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('n_doc', $editOpa->n_doc, ['class' => 'form-control', 'placeholder' => 'N° Trámite / Folio', 'readonly'=>'readonly']) !!}
          </div>
        </div>
        @endif
        <!-- fin Cuando es Documentación -->
        @if($editOpa->id_opa_tipo == 1 || $editOpa->id_opa_tipo_documentacion  == 1 && $editOpa->id_opa_modalidad_documento  == 1)
        <div class="form-group row">
            {!! Form::label('De', 'De(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('de', $editOpa->de, ['class' => 'form-control', 'placeholder' => 'De', 'readonly'=>'readonly']) !!}
          </div>
        </div>
        @endif
        @if($editOpa->id_opa_tipo == 1 || $editOpa->id_opa_tipo_documentacion  == 1 && $editOpa->id_opa_modalidad_documento  == 1)
        <div class="form-group row">
            {!! Form::label('De Correo', 'De Correo(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('de_correo', $editOpa->de_correo, ['class' => 'form-control', 'placeholder' => 'De Correo', 'readonly'=>'readonly']) !!}
          </div>
        </div>
        @endif
        @if($editOpa->id_opa_tipo == 1 || $editOpa->id_opa_tipo_documentacion  == 1 && $editOpa->id_opa_modalidad_documento  == 1)
        <div class="form-group row">
            {!! Form::label('Para', 'Para(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('para', $editOpa->para, ['class' => 'form-control', 'placeholder' => 'Para', 'readonly'=>'readonly']) !!}
          </div>
        </div>
        @endif
        @if($editOpa->id_opa_tipo == 1 || $editOpa->id_opa_tipo_documentacion  == 1 && $editOpa->id_opa_modalidad_documento  == 1)
        <div class="form-group row">
            {!! Form::label('Para Correo', 'Para Correo(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('para_correo', $editOpa->para_correo, ['class' => 'form-control', 'placeholder' => 'Para Correo', 'readonly'=>'readonly']) !!}
          </div>
        </div>
        @endif
        @if($editOpa->id_opa_tipo == 1 || $editOpa->id_opa_tipo_documentacion  == 1 && $editOpa->id_opa_modalidad_documento  == 1)
        <div class="form-group row">
            {!! Form::label('Descripcion', 'Descripcion(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('descripcion', $editOpa->descripcion, ['class' => 'form-control', 'placeholder' => 'Descripcion', 'readonly'=>'readonly']) !!}
          </div>
        </div>
        @endif

        <!-- Cuando es Encomienda -->
        @if($editOpa->id_opa_tipo == 1 && $editOpa->id_opa_clasificacion == 1)
        <div class="form-group row">
          {!! Form::label('alta_prioridad','Alta Prioridad', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::checkbox('alta_prioridad', '1', $editOpa->alta_prioridad == 1, ['disabled']) !!}
          </div>
        </div>
        @endif
        @if($editOpa->id_opa_tipo == 1 && $editOpa->id_opa_clasificacion == 2)
        <div class="form-group row">
          {!! Form::label('empaquetado','Requirió Empaquetado por OPA', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::checkbox('empaquetado', '1', $editOpa->empaquetado == 1, ['disabled']) !!}
          </div>
        </div>
        @endif
        @if($editOpa->id_opa_tipo == 1)
        <div class="form-group row">
          {!! Form::label('retirado','Entregado/Retirado', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::checkbox('retirado', '1', $editOpa->retirado == 1) !!}
          </div>
        </div>
        @endif

        <!-- Cuando es Documentación -->
        @if($editOpa->id_opa_tipo == 2 && $editOpa->id_opa_modalidad_documento  == 1)
        <div class="form-group row">
          {!! Form::label('en_ruta','En Ruta', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::checkbox('en_ruta', '1', $editOpa->en_ruta == 1, ['disabled']) !!}
          </div>
        </div>
        @endif
        @if($editOpa->id_opa_tipo == 2 && $editOpa->id_opa_modalidad_documento  == 1)
        <div class="form-group row">
          {!! Form::label('enviado','Enviado', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::checkbox('enviado', '1', $editOpa->enviado == 1) !!}
          </div>
        </div>
        @endif

        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! Form::submit('Guardar', ['class' => 'btn btn-success btn-sm','title'=>'Guardar']) !!}
              {!! link_to_route('opa', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) !!}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{!! Form::close() !!}
@stop
@section('after-scripts-end')
@endsection
