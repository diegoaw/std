<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Organigrama MINDEP</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="style.css" rel="stylesheet" type="text/css">

<style type="text/css">
.modal-contenido{
  background-color:rgba(0, 255, 255, 0);
  width:500px;
  padding: 10px 20px;
  margin: 20% auto;
  position: relative;
}
.modal{
  background-color: rgba(0,0,0,.8);
  position:fixed;
  top:0;
  right:0;
  bottom:0;
  left:0;
  opacity:0;
  pointer-events:none;
  transition: all 1s;
}
#ministra:target{
  opacity:1;
  pointer-events:auto;
}
#gabineteMinistra:target{
  opacity:1;
  pointer-events:auto;
}
#relacionesInteracionales:target{
  opacity:1;
  pointer-events:auto;
}
#subsecretario:target{
  opacity:1;
  pointer-events:auto;
}
#auditoriaInterna:target{
  opacity:1;
  pointer-events:auto;
}
#seremias:target{
  opacity:1;
  pointer-events:auto;
}
#gabineteSubsecretario:target{
  opacity:1;
  pointer-events:auto;
}
#coordinacionSeremias:target{
  opacity:1;
  pointer-events:auto;
}
#daf:target{
  opacity:1;
  pointer-events:auto;
}
#juridica:target{
  opacity:1;
  pointer-events:auto;
}
#planificacion:target{
  opacity:1;
  pointer-events:auto;
}
#comunicaciones:target{
  opacity:1;
  pointer-events:auto;
}
#infraestructura:target{
  opacity:1;
  pointer-events:auto;
}
#politica:target{
  opacity:1;
  pointer-events:auto;
}
#finanzas:target{
  opacity:1;
  pointer-events:auto;
}
#areapolitica:target{
  opacity:1;
  pointer-events:auto;
}
#areapersonas:target{
  opacity:1;
  pointer-events:auto;
}
#estudios:target{
  opacity:1;
  pointer-events:auto;
}
#areainformatica:target{
  opacity:1;
  pointer-events:auto;
}
#altorendimiento:target{
  opacity:1;
  pointer-events:auto;
}
#documentos:target{
  opacity:1;
  pointer-events:auto;
}


:root {
  --lightgray: #efefef;
  --blue: steelblue;
  --white: #fff;
  --black: rgba(0, 0, 0, 0.8);
  --bounceEasing: cubic-bezier(0.51, 0.92, 0.24, 1.15);
}

* {
  padding: 0;
  margin: 0;
}

a {
  color: #efefef;
  text-decoration: none;
}

button {
  cursor: pointer;
  background: transparent;
  border: none;
  outline: none;
  font-size: inherit;
}

body {
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100vh;
  font: 16px/1.5 sans-serif;
}

.btn-group {
  text-align: center;
}

.open-modal {
  font-weight: bold;
  background: var(--blue);
  color: var(--white);
  padding: 0.75rem 1.75rem;
  margin-bottom: 1rem;
  border-radius: 5px;
}


/* MODAL
–––––––––––––––––––––––––––––––––––––––––––––––––– */
.modal.is-visible {
  visibility: visible;
  opacity: 1;
}

.modal-dialog {
  position: relative;
  max-width: 800px;
  max-height: 80vh;
  border-radius: 5px;
  background: var(--white);
  overflow: auto;
  cursor: default;
}

.modal-dialog > * {
  padding: 1rem;
}

.modal-header,
.modal-footer {
  background: var(--lightgray);
}

.modal-header {
  display: flex;
  align-items: center;
  justify-content: space-between;
}

.modal-header .close-modal {
  font-size: 1.5rem;
}

.modal p + p {
  margin-top: 1rem;
}


/* ANIMATIONS
–––––––––––––––––––––––––––––––––––––––––––––––––– */
[data-animation] .modal-dialog {
  opacity: 0;
  transition: all 0.5s var(--bounceEasing);
}

[data-animation].is-visible .modal-dialog {
  opacity: 1;
  transition-delay: 0.2s;
}

[data-animation="slideInOutDown"] .modal-dialog {
  transform: translateY(100%);
}

[data-animation="slideInOutTop"] .modal-dialog {
  transform: translateY(-100%);
}

[data-animation="slideInOutLeft"] .modal-dialog {
  transform: translateX(-100%);
}

[data-animation="slideInOutRight"] .modal-dialog {
  transform: translateX(100%);
}

[data-animation="zoomInOut"] .modal-dialog {
  transform: scale(0.2);
}

[data-animation="rotateInOutDown"] .modal-dialog {
  transform-origin: top left;
  transform: rotate(-1turn);
}

[data-animation="mixInAnimations"].is-visible .modal-dialog {
  animation: mixInAnimations 2s 0.2s linear forwards;
}

[data-animation="slideInOutDown"].is-visible .modal-dialog,
[data-animation="slideInOutTop"].is-visible .modal-dialog,
[data-animation="slideInOutLeft"].is-visible .modal-dialog,
[data-animation="slideInOutRight"].is-visible .modal-dialog,
[data-animation="zoomInOut"].is-visible .modal-dialog,
[data-animation="rotateInOutDown"].is-visible .modal-dialog {
  transform: none;
}

@keyframes mixInAnimations {
  0% {
    transform: translateX(-100%);
  }

  10% {
    transform: translateX(0);
  }

  20% {
    transform: rotate(20deg);
  }

  30% {
    transform: rotate(-20deg);
  }

  40% {
    transform: rotate(15deg);
  }

  50% {
    transform: rotate(-15deg);
  }

  60% {
    transform: rotate(10deg);
  }

  70% {
    transform: rotate(-10deg);
  }

  80% {
    transform: rotate(5deg);
  }

  90% {
    transform: rotate(-5deg);
  }

  100% {
    transform: rotate(0deg);
  }
}


/* FOOTER
–––––––––––––––––––––––––––––––––––––––––––––––––– */
.page-footer {
  position: absolute;
  bottom: 1rem;
  right: 1rem;
}

.page-footer span {
  color: #e31b23;
}
</style>

</head>
<body bgcolor="#ffffff">

<div id="ministra" class="modal">
  <div class="modal-contenido">
    <a href="#">X</a>
    <img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/500-ministra.png"/>
  </div>  
</div>

<div id="gabineteMinistra" class="modal">
  <div class="modal-contenido">
    <a href="#">X</a>
    <img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/500-gabinete-ministra.png"/>
  </div>  
</div>

<div id="relacionesInteracionales" class="modal">
  <div class="modal-contenido">
    <a href="#">X</a>
    <img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/500-relaciones-internacionales.png"/>
  </div>  
</div>

<div id="subsecretario" class="modal">
  <div class="modal-contenido">
    <a href="#">X</a>
    <img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/500-secretario.png"/>
  </div>  
</div>

<div id="auditoriaInterna" class="modal">
  <div class="modal-contenido">
    <a href="#">X</a>
    <img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/500-auditoría-interna.png"/>
  </div>  
</div>

<div id="gabineteSubsecretario" class="modal">
  <div class="modal-contenido">
    <a href="#">X</a>
    <img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/500-gabinete-subsecretario.png"/>
  </div>  
</div>

<div id="coordinacionSeremias" class="modal">
  <div class="modal-contenido">
    <a href="#">X</a>
    <img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/500-coordinacion-seremias.png"/>
  </div>  
</div>

<div id="daf" class="modal">
  <div class="modal-contenido">
    <a href="#">X</a>
    <img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/500-daf.png"/>
  </div>  
</div>

<div id="juridica" class="modal">
  <div class="modal-contenido">
    <a href="#">X</a>
    <img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/500-juridica.png"/>
  </div>  
</div>

<div id="planificacion" class="modal">
  <div class="modal-contenido">
    <a href="#">X</a>
    <img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/500-planificacion.png"/>
  </div>  
</div>

<div id="comunicaciones" class="modal">
  <div class="modal-contenido">
    <a href="#">X</a>
    <img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/500-comunicaciones.png"/>
  </div>  
</div>

<div id="infraestructura" class="modal">
  <div class="modal-contenido">
    <a href="#">X</a>
    <img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/500-infraestructura.png"/>
  </div>  
</div>

<div id="politica" class="modal">
  <div class="modal-contenido">
    <a href="#">X</a>
    <img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/500-politica.png"/>
  </div>  
</div>

<div id="finanzas" class="modal">
  <div class="modal-contenido">
    <a href="#">X</a>
    <img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/500-finanzas.png"/>
  </div>  
</div>

<div id="areapolitica" class="modal">
  <div class="modal-contenido">
    <a href="#">X</a>
    <img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/500-areapolitica.png"/>
  </div>  
</div>

<div id="areapersonas" class="modal">
  <div class="modal-contenido">
    <a href="#">X</a>
    <img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/500-personas.png"/>
  </div>  
</div>

<div id="estudios" class="modal">
  <div class="modal-contenido">
    <a href="#">X</a>
    <img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/500-estudios.png"/>
  </div>  
</div>

<div id="areainformatica" class="modal">
  <div class="modal-contenido">
    <a href="#">X</a>
    <img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/500-informatica.png"/>
  </div>  
</div>

<div id="altorendimiento" class="modal">
  <div class="modal-contenido">
    <a href="#">X</a>
    <img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/500-altorendimiento.png"/>
  </div>  
</div>

<div id="documentos" class="modal">
  <div class="modal-contenido">
    <a href="#">X</a>
    <img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/500-documentos.png"/>
  </div>  
</div>




<table border="0" cellpadding="0" cellspacing="0" width="1024" class="center">
  <tr>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r1_c1.png" width="43" height="94" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r1_c2.png" width="98" height="94" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r1_c3.png" width="43" height="94" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r1_c4.png" width="18" height="94" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r1_c5.png" width="63" height="94" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r1_c6.png" width="19" height="94" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r1_c7.png" width="60" height="94" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r1_c8.png" width="7" height="94" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r1_c9.png" width="10" height="94" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r1_c10.png" width="45" height="94" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r1_c11.png" width="34" height="94" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r1_c12.png" width="12" height="94" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r1_c13.png" width="42" height="94" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r1_c14.png" width="10" height="94" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r1_c15.png" width="17" height="94" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r1_c16.png" width="9" height="94" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r1_c17.png" width="40" height="94" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r1_c18.png" width="13" height="94" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r1_c19.png" width="30" height="94" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r1_c20.png" width="10" height="94" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r1_c21.png" width="40" height="94" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r1_c22.png" width="10" height="94" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r1_c23.png" width="6" height="94" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r1_c24.png" width="61" height="94" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r1_c25.png" width="16" height="94" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r1_c26.png" width="9" height="94" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r1_c27.png" width="57" height="94" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r1_c28.png" width="16" height="94" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r1_c29.png" width="44" height="94" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r1_c30.png" width="99" height="94" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r1_c31.png" width="43" height="94" border="0"/></td>
  </tr>
  <tr>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r2_c1.png" width="43" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r2_c2.png" width="98" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r2_c3.png" width="43" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r2_c4.png" width="18" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r2_c5.png" width="63" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r2_c6.png" width="19" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r2_c7.png" width="60" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r2_c8.png" width="7" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r2_c9.png" width="10" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r2_c10.png" width="45" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r2_c11.png" width="34" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r2_c12.png" width="12" height="32" border="0"/></td>

   <td><a href="#ministra"><img name="ministra" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r2_c13.png" width="42" height="32" border="0" id="ministra" /></a></td>	<!-- img/500-ministra.png -->
   <td><a href="#ministra"><img name="ministra" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r2_c14.png" width="10" height="32" border="0" id="ministra" /></a></td> <!-- img/500-ministra.png -->
   <td><a href="#ministra"><img name="ministra" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r2_c15.png" width="17" height="32" border="0" id="ministra" /></a></td> <!-- img/500-ministra.png -->
   <td><a href="#ministra"><img name="ministra" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r2_c16.png" width="9" height="32" border="0" id="ministra" /></a></td> 	<!-- img/500-ministra.png -->
   <td><a href="#ministra"><img name="ministra" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r2_c17.png" width="40" height="32" border="0" id="ministra" /></a></td> <!-- img/500-ministra.png -->
   
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r2_c18.png" width="13" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r2_c19.png" width="30" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r2_c20.png" width="10" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r2_c21.png" width="40" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r2_c22.png" width="10" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r2_c23.png" width="6" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r2_c24.png" width="61" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r2_c25.png" width="16" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r2_c26.png" width="9" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r2_c27.png" width="57" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r2_c28.png" width="16" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r2_c29.png" width="44" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r2_c30.png" width="99" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r2_c31.png" width="43" height="32" border="0"/></td>
  </tr>
  <tr>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r3_c1.png" width="43" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r3_c2.png" width="98" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r3_c3.png" width="43" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r3_c4.png" width="18" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r3_c5.png" width="63" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r3_c6.png" width="19" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r3_c7.png" width="60" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r3_c8.png" width="7" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r3_c9.png" width="10" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r3_c10.png" width="45" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r3_c11.png" width="34" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r3_c12.png" width="12" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r3_c13.png" width="42" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r3_c14.png" width="10" height="35" border="0" /></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r3_c15.png" width="17" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r3_c16.png" width="9" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r3_c17.png" width="40" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r3_c18.png" width="13" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r3_c19.png" width="30" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r3_c20.png" width="10" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r3_c21.png" width="40" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r3_c22.png" width="10" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r3_c23.png" width="6" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r3_c24.png" width="61" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r3_c25.png" width="16" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r3_c26.png" width="9" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r3_c27.png" width="57" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r3_c28.png" width="16" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r3_c29.png" width="44" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r3_c30.png" width="99" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r3_c31.png" width="43" height="35" border="0"/></td>
  </tr>
  <tr>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r4_c1.png" width="43" height="39" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r4_c2.png" width="98" height="39" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r4_c3.png" width="43" height="39" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r4_c4.png" width="18" height="39" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r4_c5.png" width="63" height="39" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r4_c6.png" width="19" height="39" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r4_c7.png" width="60" height="39" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r4_c8.png" width="7" height="39" border="0"/></td>

   <td><a href="#relacionesInteracionales"><img name="elaciones-internacionales" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r4_c9.png" width="10" height="39" border="0" id="relaciones-internacionales" /></a></td>	<!-- img/500-relaciones-internacionales.png -->
   <td><a href="#relacionesInteracionales"><img name="elaciones-internacionales" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r4_c10.png" width="45" height="39" border="0" id="relaciones-internacionales" /></a></td>	<!-- img/500-relaciones-internacionales.png -->
   <td><a href="#relacionesInteracionales"><img name="elaciones-internacionales" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r4_c11.png" width="34" height="39" border="0" id="relaciones-internacionales" /></a></td>	<!-- img/500-relaciones-internacionales.png -->
   <td><a href="#relacionesInteracionales"><img name="elaciones-internacionales" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r4_c12.png" width="12" height="39" border="0" id="relaciones-internacionales" /></a></td>	<!-- img/500-relaciones-internacionales.png -->
   <td><a href="#relacionesInteracionales"><img name="elaciones-internacionales" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r4_c13.png" width="42" height="39" border="0" id="relaciones-internacionales" /></a></td>	<!-- img/500-relaciones-internacionales.png -->
   
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r4_c14.png" width="10" height="39" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r4_c15.png" width="17" height="39" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r4_c16.png" width="9" height="39" border="0"/></td>
   
   <td><a href="#gabineteMinistra"><img name="gabinete-ministra" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r4_c17.png" width="40" height="39" border="0" id="gabinete-ministra" /></a></td>	<!-- img/500-gabinete-ministra.png -->
   <td><a href="#gabineteMinistra"><img name="gabinete-ministra" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r4_c18.png" width="13" height="39" border="0" id="gabinete-ministra" /></a></td>	<!-- img/500-gabinete-ministra.png -->
   <td><a href="#gabineteMinistra"><img name="gabinete-ministra" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r4_c19.png" width="30" height="39" border="0" id="gabinete-ministra" /></a></td>	<!-- img/500-gabinete-ministra.png -->
   <td><a href="#gabineteMinistra"><img name="gabinete-ministra" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r4_c20.png" width="10" height="39" border="0" id="gabinete-ministra" /></a></td>	<!-- img/500-gabinete-ministra.png -->
   <td><a href="#gabineteMinistra"><img name="gabinete-ministra" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r4_c21.png" width="40" height="39" border="0" id="gabinete-ministra" /></a></td>	<!-- img/500-gabinete-ministra.png -->
   <td><a href="#gabineteMinistra"><img name="gabinete-ministra" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r4_c22.png" width="10" height="39" border="0" id="gabinete-ministra" /></a></td>	<!-- img/500-gabinete-ministra.png -->
   
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r4_c23.png" width="6" height="39" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r4_c24.png" width="61" height="39" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r4_c25.png" width="16" height="39" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r4_c26.png" width="9" height="39" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r4_c27.png" width="57" height="39" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r4_c28.png" width="16" height="39" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r4_c29.png" width="44" height="39" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r4_c30.png" width="99" height="39" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r4_c31.png" width="43" height="39" border="0"/></td>
  </tr>
  <tr>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r5_c1.png" width="43" height="36" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r5_c2.png" width="98" height="36" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r5_c3.png" width="43" height="36" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r5_c4.png" width="18" height="36" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r5_c5.png" width="63" height="36" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r5_c6.png" width="19" height="36" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r5_c7.png" width="60" height="36" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r5_c8.png" width="7" height="36" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r5_c9.png" width="10" height="36" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r5_c10.png" width="45" height="36" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r5_c11.png" width="34" height="36" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r5_c12.png" width="12" height="36" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r5_c13.png" width="42" height="36" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r5_c14.png" width="10" height="36" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r5_c15.png" width="17" height="36" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r5_c16.png" width="9" height="36" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r5_c17.png" width="40" height="36" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r5_c18.png" width="13" height="36" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r5_c19.png" width="30" height="36" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r5_c20.png" width="10" height="36" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r5_c21.png" width="40" height="36" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r5_c22.png" width="10" height="36" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r5_c23.png" width="6" height="36" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r5_c24.png" width="61" height="36" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r5_c25.png" width="16" height="36" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r5_c26.png" width="9" height="36" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r5_c27.png" width="57" height="36" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r5_c28.png" width="16" height="36" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r5_c29.png" width="44" height="36" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r5_c30.png" width="99" height="36" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r5_c31.png" width="43" height="36" border="0"/></td>
  </tr>
  <tr>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r6_c1.png" width="43" height="2" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r6_c2.png" width="98" height="2" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r6_c3.png" width="43" height="2" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r6_c4.png" width="18" height="2" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r6_c5.png" width="63" height="2" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r6_c6.png" width="19" height="2" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r6_c7.png" width="60" height="2" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r6_c8.png" width="7" height="2" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r6_c9.png" width="10" height="2" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r6_c10.png" width="45" height="2" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r6_c11.png" width="34" height="2" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r6_c12.png" width="12" height="2" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r6_c13.png" width="42" height="2" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r6_c14.png" width="10" height="2" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r6_c15.png" width="17" height="2" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r6_c16.png" width="9" height="2" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r6_c17.png" width="40" height="2" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r6_c18.png" width="13" height="2" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r6_c19.png" width="30" height="2" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r6_c20.png" width="10" height="2" border="0"/></td>
   
   <td><a href="#auditoriaInterna"><img name="auditoria" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r6_c21.png" width="40" height="2" border="0" id="auditoria" /></a></td>	<!-- img/500-auditoría-interna.png -->
   <td><a href="#auditoriaInterna"><img name="auditoria" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r6_c22.png" width="10" height="2" border="0" id="auditoria" /></a></td>	<!-- img/500-auditoría-interna.png -->
   <td><a href="#auditoriaInterna"><img name="auditoria" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r6_c23.png" width="6" height="2" border="0" id="auditoria" /></a></td>		<!-- img/500-auditoría-interna.png -->
   <td><a href="#auditoriaInterna"><img name="auditoria" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r6_c24.png" width="61" height="2" border="0" id="auditoria" /></a></td>	<!-- img/500-auditoría-interna.png -->
   <td><a href="#auditoriaInterna"><img name="auditoria" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r6_c25.png" width="16" height="2" border="0" id="auditoria" /></a></td>	<!-- img/500-auditoría-interna.png -->
   <td><a href="#auditoriaInterna"><img name="auditoria" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r6_c26.png" width="9" height="2" border="0" id="auditoria" /></a></td>		<!-- img/500-auditoría-interna.png -->
   
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r6_c27.png" width="57" height="2" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r6_c28.png" width="16" height="2" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r6_c29.png" width="44" height="2" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r6_c30.png" width="99" height="2" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r6_c31.png" width="43" height="2" border="0"/></td>
  </tr>
  <tr>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r7_c1.png" width="43" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r7_c2.png" width="98" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r7_c3.png" width="43" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r7_c4.png" width="18" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r7_c5.png" width="63" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r7_c6.png" width="19" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r7_c7.png" width="60" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r7_c8.png" width="7" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r7_c9.png" width="10" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r7_c10.png" width="45" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r7_c11.png" width="34" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r7_c12.png" width="12" height="32" border="0"/></td>
   
   <td><a href="#subsecretario"><img name="subsecretario" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r7_c13.png" width="42" height="32" border="0" id="subsecretario" /></a></td>	<!-- img/500-secretario.png -->
   <td><a href="#subsecretario"><img name="subsecretario" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r7_c14.png" width="10" height="32" border="0" id="subsecretario" /></a></td>	<!-- img/500-secretario.png -->
   <td><a href="#subsecretario"><img name="subsecretario" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r7_c15.png" width="17" height="32" border="0" id="subsecretario" /></a></td>	<!-- img/500-secretario.png -->
   <td><a href="#subsecretario"><img name="subsecretario" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r7_c16.png" width="9" height="32" border="0" id="subsecretario" /></a></td>	<!-- img/500-secretario.png -->
   <td><a href="#subsecretario"><img name="subsecretario" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r7_c17.png" width="40" height="32" border="0" id="subsecretario" /></a></td>	<!-- img/500-secretario.png -->
   
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r7_c18.png" width="13" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r7_c19.png" width="30" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r7_c20.png" width="10" height="32" border="0"/></td>
   
   <td><a href="#auditoriaInterna"><img name="auditoria" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r7_c21.png" width="40" height="32" border="0" id="auditoria" /></a></td>		<!-- img/500-auditoría-interna.png -->
   <td><a href="#auditoriaInterna"><img name="auditoria" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r7_c22.png" width="10" height="32" border="0" id="auditoria" /></a></td>		<!-- img/500-auditoría-interna.png -->
   <td><a href="#auditoriaInterna"><img name="auditoria" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r7_c23.png" width="6" height="32" border="0" id="auditoria" /></a></td>		<!-- img/500-auditoría-interna.png -->
   <td><a href="#auditoriaInterna"><img name="auditoria" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r7_c24.png" width="61" height="32" border="0" id="auditoria" /></a></td>		<!-- img/500-auditoría-interna.png -->
   <td><a href="#auditoriaInterna"><img name="auditoria" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r7_c25.png" width="16" height="32" border="0" id="auditoria" /></a></td>		<!-- img/500-auditoría-interna.png -->
   <td><a href="#auditoriaInterna"><img name="auditoria" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r7_c26.png" width="9" height="32" border="0" id="auditoria" /></a></td>		<!-- img/500-auditoría-interna.png -->
   
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r7_c27.png" width="57" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r7_c28.png" width="16" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r7_c29.png" width="44" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r7_c30.png" width="99" height="32" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r7_c31.png" width="43" height="32" border="0"/></td>
  </tr>
  <tr>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r8_c1.png" width="43" height="1" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r8_c2.png" width="98" height="1" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r8_c3.png" width="43" height="1" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r8_c4.png" width="18" height="1" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r8_c5.png" width="63" height="1" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r8_c6.png" width="19" height="1" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r8_c7.png" width="60" height="1" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r8_c8.png" width="7" height="1" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r8_c9.png" width="10" height="1" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r8_c10.png" width="45" height="1" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r8_c11.png" width="34" height="1" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r8_c12.png" width="12" height="1" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r8_c13.png" width="42" height="1" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r8_c14.png" width="10" height="1" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r8_c15.png" width="17" height="1" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r8_c16.png" width="9" height="1" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r8_c17.png" width="40" height="1" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r8_c18.png" width="13" height="1" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r8_c19.png" width="30" height="1" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r8_c20.png" width="10" height="1" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r8_c21.png" width="40" height="1" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r8_c22.png" width="10" height="1" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r8_c23.png" width="6" height="1" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r8_c24.png" width="61" height="1" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r8_c25.png" width="16" height="1" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r8_c26.png" width="9" height="1" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r8_c27.png" width="57" height="1" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r8_c28.png" width="16" height="1" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r8_c29.png" width="44" height="1" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r8_c30.png" width="99" height="1" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r8_c31.png" width="43" height="1" border="0"/></td>
  </tr>
  <tr>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r9_c1.png" width="43" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r9_c2.png" width="98" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r9_c3.png" width="43" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r9_c4.png" width="18" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r9_c5.png" width="63" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r9_c6.png" width="19" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r9_c7.png" width="60" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r9_c8.png" width="7" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r9_c9.png" width="10" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r9_c10.png" width="45" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r9_c11.png" width="34" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r9_c12.png" width="12" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r9_c13.png" width="42" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r9_c14.png" width="10" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r9_c15.png" width="17" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r9_c16.png" width="9" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r9_c17.png" width="40" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r9_c18.png" width="13" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r9_c19.png" width="30" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r9_c20.png" width="10" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r9_c21.png" width="40" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r9_c22.png" width="10" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r9_c23.png" width="6" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r9_c24.png" width="61" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r9_c25.png" width="16" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r9_c26.png" width="9" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r9_c27.png" width="57" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r9_c28.png" width="16" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r9_c29.png" width="44" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r9_c30.png" width="99" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r9_c31.png" width="43" height="35" border="0"/></td>
  </tr>
  <tr>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r10_c1.png" width="43" height="50" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r10_c2.png" width="98" height="50" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r10_c3.png" width="43" height="50" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r10_c4.png" width="18" height="50" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r10_c5.png" width="63" height="50" border="0"/></td>
   
   <td><img name="seremias" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r10_c6.png" width="19" height="50" border="0" id="seremias" /></td>	<!-- NO TIENE IMAGEN PARA MOSTRAR -->
   <td><img name="seremias" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r10_c7.png" width="60" height="50" border="0" id="seremias" /></td>	<!-- NO TIENE IMAGEN PARA MOSTRAR -->
   <td><img name="seremias" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r10_c8.png" width="7" height="50" border="0" id="seremias" /></td>	<!-- NO TIENE IMAGEN PARA MOSTRAR -->
   <td><img name="seremias" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r10_c9.png" width="10" height="50" border="0" id="seremias" /></td>	<!-- NO TIENE IMAGEN PARA MOSTRAR -->
   <td><img name="seremias" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r10_c10.png" width="45" height="50" border="0" id="seremias" /></td>	<!-- NO TIENE IMAGEN PARA MOSTRAR -->
   
   <td><img name="organigramaimagen_r10_c11" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r10_c11.png" width="34" height="50" border="0" id="organigramaimagen_r10_c11" /></td>
   
   <td><a href="#gabineteSubsecretario"><img name="gabinete-subse" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r10_c12.png" width="12" height="50" border="0" id="gabinete-subse" /></a></td>	<!-- 500-gabinete-subsecretario.png -->
   <td><a href="#gabineteSubsecretario"><img name="gabinete-subse" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r10_c13.png" width="42" height="50" border="0" id="gabinete-subse" /></a></td>	<!-- 500-gabinete-subsecretario.png -->
   <td><a href="#gabineteSubsecretario"><img name="gabinete-subse" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r10_c14.png" width="10" height="50" border="0" id="gabinete-subse" /></a></td>	<!-- 500-gabinete-subsecretario.png -->
   <td><a href="#gabineteSubsecretario"><img name="gabinete-subse" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r10_c15.png" width="17" height="50" border="0" id="gabinete-subse" /></a></td>	<!-- 500-gabinete-subsecretario.png -->
   <td><a href="#gabineteSubsecretario"><img name="gabinete-subse" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r10_c16.png" width="9" height="50" border="0" id="gabinete-subse" /></a></td>		<!-- 500-gabinete-subsecretario.png -->
   <td><a href="#gabineteSubsecretario"><img name="gabinete-subse" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r10_c17.png" width="40" height="50" border="0" id="gabinete-subse" /></a></td>	<!-- 500-gabinete-subsecretario.png -->
   <td><a href="#gabineteSubsecretario"><img name="gabinete-subse" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r10_c18.png" width="13" height="50" border="0" id="gabinete-subse" /></a></td>	<!-- 500-gabinete-subsecretario.png -->
   
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r10_c19.png" width="30" height="50" border="0"/></td>
   
   <td><a href="#coordinacionSeremias"><img name="coordinacion-seremias" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r10_c20.png" width="10" height="50" border="0" id="coordinacion-seremias" /></a></td>	<!-- 500-coordinacion-seremias.png -->
   <td><a href="#coordinacionSeremias"><img name="coordinacion-seremias" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r10_c21.png" width="40" height="50" border="0" id="coordinacion-seremias" /></a></td>	<!-- 500-coordinacion-seremias.png -->
   <td><a href="#coordinacionSeremias"><img name="coordinacion-seremias" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r10_c22.png" width="10" height="50" border="0" id="coordinacion-seremias" /></a></td>	<!-- 500-coordinacion-seremias.png -->
   <td><a href="#coordinacionSeremias"><img name="coordinacion-seremias" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r10_c23.png" width="6" height="50" border="0" id="coordinacion-seremias" /></a></td>	<!-- 500-coordinacion-seremias.png -->
   <td><a href="#coordinacionSeremias"><img name="coordinacion-seremias" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r10_c24.png" width="61" height="50" border="0" id="coordinacion-seremias" /></a></td>	<!-- 500-coordinacion-seremias.png -->
   <td><a href="#coordinacionSeremias"><img name="coordinacion-seremias" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r10_c25.png" width="16" height="50" border="0" id="coordinacion-seremias" /></a></td>	<!-- 500-coordinacion-seremias.png -->
   
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r10_c26.png" width="9" height="50" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r10_c27.png" width="57" height="50" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r10_c28.png" width="16" height="50" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r10_c29.png" width="44" height="50" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r10_c30.png" width="99" height="50" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r10_c31.png" width="43" height="50" border="0"/></td>
  </tr>
  <tr>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r11_c1.png" width="43" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r11_c2.png" width="98" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r11_c3.png" width="43" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r11_c4.png" width="18" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r11_c5.png" width="63" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r11_c6.png" width="19" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r11_c7.png" width="60" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r11_c8.png" width="7" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r11_c9.png" width="10" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r11_c10.png" width="45" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r11_c11.png" width="34" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r11_c12.png" width="12" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r11_c13.png" width="42" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r11_c14.png" width="10" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r11_c15.png" width="17" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r11_c16.png" width="9" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r11_c17.png" width="40" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r11_c18.png" width="13" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r11_c19.png" width="30" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r11_c20.png" width="10" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r11_c21.png" width="40" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r11_c22.png" width="10" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r11_c23.png" width="6" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r11_c24.png" width="61" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r11_c25.png" width="16" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r11_c26.png" width="9" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r11_c27.png" width="57" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r11_c28.png" width="16" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r11_c29.png" width="44" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r11_c30.png" width="99" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r11_c31.png" width="43" height="51" border="0"/></td>
  </tr>
  <tr>
   <td><imgsrc="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r12_c1.png" width="43" height="50" border="0"/></td>
   <td><a href="#daf"><img name="daf" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r12_c2.png" width="98" height="50" border="0" id="daf" /></a></td>	<!-- img/500-daf.png -->
   <td><a href="#daf"><img name="daf" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r12_c3.png" width="43" height="50" border="0" id="daf" /></a></td>	<!-- img/500-daf.png -->
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r12_c4.png" width="18" height="50" border="0"/></td>
   <td><a href="#juridica"><img name="juridica" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r12_c5.png" width="63" height="50" border="0" id="juridica" /></a></td>	<!-- img/500-juridica.png -->
   <td><a href="#juridica"><img name="juridica" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r12_c6.png" width="19" height="50" border="0" id="juridica" /></a></td>	<!-- img/500-juridica.png -->
   <td><a href="#juridica"><img name="juridica" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r12_c7.png" width="60" height="50" border="0" id="juridica" /></a></td>	<!-- img/500-juridica.png -->
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r12_c8.png" width="7" height="50" border="0" /></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r12_c9.png" width="10" height="50" border="0" /></td>
   <td><a href="#planificacion"><img name="planificacion" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r12_c10.png" width="45" height="50" border="0" id="planificacion" /></a></td>	<!-- img/500-planificacion.png -->
   <td><a href="#planificacion"><img name="planificacion" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r12_c11.png" width="34" height="50" border="0" id="planificacion" /></a></td>	<!-- img/500-planificacion.png -->
   <td><a href="#planificacion"><img name="planificacion" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r12_c12.png" width="12" height="50" border="0" id="planificacion" /></a></td>	<!-- img/500-planificacion.png -->
   <td><a href="#planificacion"><img name="planificacion" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r12_c13.png" width="42" height="50" border="0" id="planificacion" /></a></td>	<!-- img/500-planificacion.png -->
   <td><a href="#planificacion"><img name="planificacion" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r12_c14.png" width="10" height="50" border="0" id="planificacion" /></a></td>	<!-- img/500-planificacion.png -->
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r12_c15.png" width="17" height="50" border="0"/></td>
   <td><a href="#comunicaciones"><img name="comunicaciones" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r12_c16.png" width="9" height="50" border="0" id="comunicaciones" /></a></td>		<!-- img/500-comunicaciones.png -->
   <td><a href="#comunicaciones"><img name="comunicaciones" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r12_c17.png" width="40" height="50" border="0" id="comunicaciones" /></a></td>	<!-- img/500-comunicaciones.png -->
   <td><a href="#comunicaciones"><img name="comunicaciones" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r12_c18.png" width="13" height="50" border="0" id="comunicaciones" /></a></td>	<!-- img/500-comunicaciones.png -->
   <td><a href="#comunicaciones"><img name="comunicaciones" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r12_c19.png" width="30" height="50" border="0" id="comunicaciones" /></a></td>	<!-- img/500-comunicaciones.png -->
   <td><a href="#comunicaciones"><img name="comunicaciones" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r12_c20.png" width="10" height="50" border="0" id="comunicaciones" /></a></td>	<!-- img/500-comunicaciones.png -->
   <td><a href="#comunicaciones"><img name="comunicaciones" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r12_c21.png" width="40" height="50" border="0" id="comunicaciones" /></a></td>	<!-- img/500-comunicaciones.png -->
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r12_c22.png" width="10" height="50" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r12_c23.png" width="6" height="50" border="0"/></td>
   <td><a href="#infraestructura"><img name="infraestructura" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r12_c24.png" width="61" height="50" border="0" id="infraestructura" /></a></td>	<!-- img/500-infraestructura.png -->
   <td><a href="#infraestructura"><img name="infraestructura" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r12_c25.png" width="16" height="50" border="0" id="infraestructura" /></a></td>	<!-- img/500-infraestructura.png -->
   <td><a href="#infraestructura"><img name="infraestructura" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r12_c26.png" width="9" height="50" border="0" id="infraestructura" /></a></td>	<!-- img/500-infraestructura.png -->
   <td><a href="#infraestructura"><img name="infraestructura" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r12_c27.png" width="57" height="50" border="0" id="infraestructura" /></a></td>	<!-- img/500-infraestructura.png -->
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r12_c28.png" width="16" height="50" border="0"/></td>
   <td><a href="#politica"><img name="politica1" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r12_c29.png" width="44" height="50" border="0" id="politica1" /></a></td>	<!-- img/500-politica.png -->
   <td><a href="#politica"><img name="politica" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r12_c30.png" width="99" height="50" border="0" id="politica" /></a></td>	<!-- img/500-politica.png -->
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r12_c31.png" width="43" height="50" border="0"/></td>
  </tr>
  <tr>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r13_c1.png" width="43" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r13_c2.png" width="98" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r13_c3.png" width="43" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r13_c4.png" width="18" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r13_c5.png" width="63" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r13_c6.png" width="19" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r13_c7.png" width="60" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r13_c8.png" width="7" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r13_c9.png" width="10" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r13_c10.png" width="45" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r13_c11.png" width="34" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r13_c12.png" width="12" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r13_c13.png" width="42" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r13_c14.png" width="10" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r13_c15.png" width="17" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r13_c16.png" width="9" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r13_c17.png" width="40" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r13_c18.png" width="13" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r13_c19.png" width="30" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r13_c20.png" width="10" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r13_c21.png" width="40" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r13_c22.png" width="10" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r13_c23.png" width="6" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r13_c24.png" width="61" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r13_c25.png" width="16" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r13_c26.png" width="9" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r13_c27.png" width="57" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r13_c28.png" width="16" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r13_c29.png" width="44" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r13_c30.png" width="99" height="35" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r13_c31.png" width="43" height="35" border="0"/></td>
  </tr>
  <tr>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r14_c1.png" width="43" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r14_c2.png" width="98" height="49" border="0"/></td>
   
   <td><a href="#finanzas"><img name="areafinanzas" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r14_c3.png" width="43" height="49" border="0" id="areafinanzas" /></a></td>	<!-- img/500-finanzas.png -->
   <td><a href="#finanzas"><img name="areafinanzas" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r14_c4.png" width="18" height="49" border="0" id="areafinanzas" /></a></td>	<!-- img/500-finanzas.png -->
   <td><a href="#finanzas"><img name="areafinanzas" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r14_c5.png" width="63" height="49" border="0" id="areafinanzas" /></a></td>	<!-- img/500-finanzas.png -->
   <td><a href="#finanzas"><img name="areafinanzas" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r14_c6.png" width="19" height="49" border="0" id="areafinanzas" /></a></td>	<!-- img/500-finanzas.png -->
   
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r14_c7.png" width="60" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r14_c8.png" width="7" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r14_c9.png" width="10" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r14_c10.png" width="45" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r14_c11.png" width="34" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r14_c12.png" width="12" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r14_c13.png" width="42" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r14_c14.png" width="10" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r14_c15.png" width="17" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r14_c16.png" width="9" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r14_c17.png" width="40" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r14_c18.png" width="13" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r14_c19.png" width="30" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r14_c20.png" width="10" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r14_c21.png" width="40" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r14_c22.png" width="10" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r14_c23.png" width="6" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r14_c24.png" width="61" height="49" border="0"/></td>
   
   <td><a href="#areapolitica"><img name="areapolitica" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r14_c25.png" width="16" height="49" border="0" id="areapolitica" /></a></td>	<!-- img/500-politica.png -->
   <td><a href="#areapolitica"><img name="areapolitica" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r14_c26.png" width="9" height="49" border="0" id="areapolitica" /></a></td>		<!-- img/500-politica.png -->
   <td><a href="#areapolitica"><img name="areapolitica" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r14_c27.png" width="57" height="49" border="0" id="areapolitica" /></a></td>	<!-- img/500-politica.png -->
   <td><a href="#areapolitica"><img name="areapolitica" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r14_c28.png" width="16" height="49" border="0" id="areapolitica" /></a></td>	<!-- img/500-politica.png -->
   <td><a href="#areapolitica"><img name="areapolitica" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r14_c29.png" width="44" height="49" border="0" id="areapolitica" /></a></td>	<!-- img/500-politica.png -->
   
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r14_c30.png" width="99" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r14_c31.png" width="43" height="49" border="0"/></td>
  </tr>
  <tr>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r15_c1.png" width="43" height="15" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r15_c2.png" width="98" height="15" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r15_c3.png" width="43" height="15" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r15_c4.png" width="18" height="15" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r15_c5.png" width="63" height="15" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r15_c6.png" width="19" height="15" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r15_c7.png" width="60" height="15" border="0"></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r15_c8.png" width="7" height="15" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r15_c9.png" width="10" height="15" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r15_c10.png" width="45" height="15" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r15_c11.png" width="34" height="15" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r15_c12.png" width="12" height="15" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r15_c13.png" width="42" height="15" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r15_c14.png" width="10" height="15" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r15_c15.png" width="17" height="15" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r15_c16.png" width="9" height="15" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r15_c17.png" width="40" height="15" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r15_c18.png" width="13" height="15" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r15_c19.png" width="30" height="15" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r15_c20.png" width="10" height="15" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r15_c21.png" width="40" height="15" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r15_c22.png" width="10" height="15" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r15_c23.png" width="6" height="15" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r15_c24.png" width="61" height="15" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r15_c25.png" width="16" height="15" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r15_c26.png" width="9" height="15" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r15_c27.png" width="57" height="15" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r15_c28.png" width="16" height="15" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r15_c29.png" width="44" height="15" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r15_c30.png" width="99" height="15" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r15_c31.png" width="43" height="15" border="0"/></td>
  </tr>
  <tr>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r16_c1.png" width="43" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r16_c2.png" width="98" height="49" border="0"/></td>
   <td><a href="#areapersonas"><img name="areapersonas" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r16_c3.png" width="43" height="49" border="0" id="areapersonas" /></a></td>	<!-- areapersonas -->
   <td><a href="#areapersonas"><img name="areapersonas" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r16_c4.png" width="18" height="49" border="0" id="areapersonas" /></a></td>	<!-- areapersonas -->
   <td><a href="#areapersonas"><img name="areapersonas" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r16_c5.png" width="63" height="49" border="0" id="areapersonas" /></a></td>	<!-- areapersonas -->
   <td><a href="#areapersonas"><img name="areapersonas" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r16_c6.png" width="19" height="49" border="0" id="areapersonas" /></a></td>	<!-- areapersonas -->
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r16_c7.png" width="60" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r16_c8.png" width="7" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r16_c9.png" width="10" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r16_c10.png" width="45" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r16_c11.png" width="34" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r16_c12.png" width="12" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r16_c13.png" width="42" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r16_c14.png" width="10" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r16_c15.png" width="17" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r16_c16.png" width="9" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r16_c17.png" width="40" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r16_c18.png" width="13" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r16_c19.png" width="30" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r16_c20.png" width="10" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r16_c21.png" width="40" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r16_c22.png" width="10" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r16_c23.png" width="6" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r16_c24.png" width="61" height="49" border="0"/></td>
   <td><a href="#estudios"><img name="areaestudios" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r16_c25.png" width="16" height="49" border="0" id="areaestudios" /></a></td>	<!-- img/500-estudios.png -->
   <td><a href="#estudios"><img name="areaestudios" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r16_c26.png" width="9" height="49" border="0" id="areaestudios" /></a></td>		<!-- img/500-estudios.png -->
   <td><a href="#estudios"><img name="areaestudios" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r16_c27.png" width="57" height="49" border="0" id="areaestudios" /></a></td>	<!-- img/500-estudios.png -->
   <td><a href="#estudios"><img name="areaestudios" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r16_c28.png" width="16" height="49" border="0" id="areaestudios" /></a></td>	<!-- img/500-estudios.png -->
   <td><a href="#estudios"><img name="areaestudios" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r16_c29.png" width="44" height="49" border="0" id="areaestudios" /></a></td>	<!-- img/500-estudios.png -->
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r16_c30.png" width="99" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r16_c31.png" width="43" height="49" border="0"/></td>
  </tr>
  <tr>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r17_c1.png" width="43" height="13" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r17_c2.png" width="98" height="13" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r17_c3.png" width="43" height="13" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r17_c4.png" width="18" height="13" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r17_c5.png" width="63" height="13" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r17_c6.png" width="19" height="13" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r17_c7.png" width="60" height="13" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r17_c8.png" width="7" height="13" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r17_c9.png" width="10" height="13" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r17_c10.png" width="45" height="13" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r17_c11.png" width="34" height="13" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r17_c12.png" width="12" height="13" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r17_c13.png" width="42" height="13" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r17_c14.png" width="10" height="13" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r17_c15.png" width="17" height="13" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r17_c16.png" width="9" height="13" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r17_c17.png" width="40" height="13" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r17_c18.png" width="13" height="13" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r17_c19.png" width="30" height="13" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r17_c20.png" width="10" height="13" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r17_c21.png" width="40" height="13" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r17_c22.png" width="10" height="13" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r17_c23.png" width="6" height="13" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r17_c24.png" width="61" height="13" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r17_c25.png" width="16" height="13" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r17_c26.png" width="9" height="13" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r17_c27.png" width="57" height="13" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r17_c28.png" width="16" height="13" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r17_c29.png" width="44" height="13" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r17_c30.png" width="99" height="13" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r17_c31.png" width="43" height="13" border="0"/></td>
  </tr>
  <tr>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r18_c1.png" width="43" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r18_c2.png" width="98" height="51" border="0"/></td>
   <td><a href="#areainformatica"><img name="areainformatica" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r18_c3.png" width="43" height="51" border="0" id="areainformatica" /></a></td>	<!-- areainformatica -->
   <td><a href="#areainformatica"><img name="areainformatica" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r18_c4.png" width="18" height="51" border="0" id="areainformatica" /></a></td>	<!-- areainformatica -->
   <td><a href="#areainformatica"><img name="areainformatica" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r18_c5.png" width="63" height="51" border="0" id="areainformatica" /></a></td>	<!-- areainformatica -->
   <td><a href="#areainformatica"><img name="areainformatica" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r18_c6.png" width="19" height="51" border="0" id="areainformatica" /></a></td>	<!-- areainformatica -->
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r18_c7.png" width="60" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r18_c8.png" width="7" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r18_c9.png" width="10" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r18_c10.png" width="45" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r18_c11.png" width="34" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r18_c12.png" width="12" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r18_c13.png" width="42" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r18_c14.png" width="10" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r18_c15.png" width="17" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r18_c16.png" width="9" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r18_c17.png" width="40" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r18_c18.png" width="13" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r18_c19.png" width="30" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r18_c20.png" width="10" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r18_c21.png" width="40" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r18_c22.png" width="10" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r18_c23.png" width="6" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r18_c24.png" width="61" height="51" border="0"/></td>
   <td><a href="#altorendimiento"><img name="areaaltorendimiento" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r18_c25.png" width="16" height="51" border="0" id="areaaltorendimiento" /></a></td>	<!-- img/500-altorendimiento.png -->
   <td><a href="#altorendimiento"><img name="areaaltorendimiento" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r18_c26.png" width="9" height="51" border="0" id="areaaltorendimiento" /></a></td>	<!-- img/500-altorendimiento.png -->
   <td><a href="#altorendimiento"><img name="areaaltorendimiento" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r18_c27.png" width="57" height="51" border="0" id="areaaltorendimiento" /></a></td>	<!-- img/500-altorendimiento.png -->
   <td><a href="#altorendimiento"><img name="areaaltorendimiento" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r18_c28.png" width="16" height="51" border="0" id="areaaltorendimiento" /></a></td>	<!-- img/500-altorendimiento.png -->
   <td><a href="#altorendimiento"><img name="areaaltorendimiento" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r18_c29.png" width="44" height="51" border="0" id="areaaltorendimiento" /></a></td>	<!-- img/500-altorendimiento.png -->
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r18_c30.png" width="99" height="51" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r18_c31.png" width="43" height="51" border="0"/></td>
  </tr>
  <tr>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r19_c1.png" width="43" height="10" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r19_c2.png" width="98" height="10" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r19_c3.png" width="43" height="10" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r19_c4.png" width="18" height="10" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r19_c5.png" width="63" height="10" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r19_c6.png" width="19" height="10" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r19_c7.png" width="60" height="10" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r19_c8.png" width="7" height="10" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r19_c9.png" width="10" height="10" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r19_c10.png" width="45" height="10" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r19_c11.png" width="34" height="10" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r19_c12.png" width="12" height="10" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r19_c13.png" width="42" height="10" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r19_c14.png" width="10" height="10" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r19_c15.png" width="17" height="10" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r19_c16.png" width="9" height="10" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r19_c17.png" width="40" height="10" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r19_c18.png" width="13" height="10" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r19_c19.png" width="30" height="10" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r19_c20.png" width="10" height="10" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r19_c21.png" width="40" height="10" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r19_c22.png" width="10" height="10" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r19_c23.png" width="6" height="10" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r19_c24.png" width="61" height="10" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r19_c25.png" width="16" height="10" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r19_c26.png" width="9" height="10" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r19_c27.png" width="57" height="10" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r19_c28.png" width="16" height="10" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r19_c29.png" width="44" height="10" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r19_c30.png" width="99" height="10" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r19_c31.png" width="43" height="10" border="0"/></td>
  </tr>
  <tr>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r20_c1.png" width="43" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r20_c2.png" width="98" height="49" border="0"/></td>
   <td><a href="#documentos"><img name="documentos" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r20_c3.png" width="43" height="49" border="0" id="documentos" /></a></td> <!-- 500-documentos.png -->
   <td><a href="#documentos"><img name="documentos" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r20_c4.png" width="18" height="49" border="0" id="documentos" /></a></td> <!-- 500-documentos.png -->
   <td><a href="#documentos"><img name="documentos" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r20_c5.png" width="63" height="49" border="0" id="documentos" /></a></td> <!-- 500-documentos.png -->
   <td><a href="#documentos"><img name="documentos" src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r20_c6.png" width="19" height="49" border="0" id="documentos" /></a></td> <!-- 500-documentos.png -->
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r20_c7.png" width="60" height="49" border="0" id="documentos" /></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r20_c8.png" width="7" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r20_c9.png" width="10" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r20_c10.png" width="45" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r20_c11.png" width="34" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r20_c12.png" width="12" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r20_c13.png" width="42" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r20_c14.png" width="10" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r20_c15.png" width="17" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r20_c16.png" width="9" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r20_c17.png" width="40" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r20_c18.png" width="13" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r20_c19.png" width="30" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r20_c20.png" width="10" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r20_c21.png" width="40" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r20_c22.png" width="10" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r20_c23.png" width="6" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r20_c24.png" width="61" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r20_c25.png" width="16" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r20_c26.png" width="9" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r20_c27.png" width="57" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r20_c28.png" width="16" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r20_c29.png" width="44" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r20_c30.png" width="99" height="49" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r20_c31.png" width="43" height="49" border="0"/></td>
  </tr>
  <tr>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r21_c1.png" width="43" height="68" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r21_c2.png" width="98" height="68" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r21_c3.png" width="43" height="68" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r21_c4.png" width="18" height="68" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r21_c5.png" width="63" height="68" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r21_c6.png" width="19" height="68" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r21_c7.png" width="60" height="68" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r21_c8.png" width="7" height="68" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r21_c9.png" width="10" height="68" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r21_c10.png" width="45" height="68" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r21_c11.png" width="34" height="68" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r21_c12.png" width="12" height="68" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r21_c13.png" width="42" height="68" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r21_c14.png" width="10" height="68" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r21_c15.png" width="17" height="68" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r21_c16.png" width="9" height="68" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r21_c17.png" width="40" height="68" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r21_c18.png" width="13" height="68" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r21_c19.png" width="30" height="68" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r21_c20.png" width="10" height="68" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r21_c21.png" width="40" height="68" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r21_c22.png" width="10" height="68" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r21_c23.png" width="6" height="68" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r21_c24.png" width="61" height="68" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r21_c25.png" width="16" height="68" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r21_c26.png" width="9" height="68" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r21_c27.png" width="57" height="68" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r21_c28.png" width="16" height="68" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r21_c29.png" width="44" height="68" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r21_c30.png" width="99" height="68" border="0"/></td>
   <td><img src="https://mindep.s3.amazonaws.com/ImagenesOrganigrama/organigrama-imagen_r21_c31.png" width="43" height="68" border="0"/></td>
  </tr>
</table>
</body>
</html>