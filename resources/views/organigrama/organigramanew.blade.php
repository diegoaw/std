<!doctype html>
<html lang="en">
<head>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<style type="text/css">
		body {
			display: flex;
			font-family: sans-serif;
			font: 11px/1.5 sans-serif;
			justify-content: center;
			height: 100vh;
		}
		.right-line {
		border-right: 1.5px #0f0f4b solid;
		height:3em
		}
		.left-line {
		border-left: 1.5px #0f0f4b solid;
		height:3em
		}

		.top-line {
		border-top: 1.5px #0f0f4b solid;
		}

		.halved {
		width: 50%;
		float:left;
		}
		.divcircleministerio{
			border: 1.5px #0f0f4b solid;
			border-radius: 5px;
			padding: 5px;

		}
		.divcircleministra{
			border: 1.5px #0f0f4b solid;
			border-radius: 5px;
			padding: 5px;
		}
		.level3{
			border: 1.5px #0f0f4b solid;
			border-radius: 5px;
			padding: 5px;
			text-align: center;
		
		}
		.level4{
			border: 1.5px #0f0f4b solid;
			border-radius: 5px;
			padding: 5px;
			text-align: center;
		}

		.divcircleministerio:hover {
				border: 1.5px solid #fff;
				color: #fff;
				background-color: #0f0f4b;
				display: inline-block;
		 }
		 .divcircleministra:hover {
				border: 1.5px solid #fff;
				color: #fff;
				background-color: #0f0f4b;
				display: inline-block;
		 }
		 .level3:hover {
				border: 1.5px solid #fff;
				color: #fff;
				background-color: #0f0f4b;
				display: inline-block;
		 }
		.level4:hover {
				border: 1.5px solid #fff;
				color: #fff;
				background-color: #0f0f4b;
				display: inline-block;
		 }
		 .modal-dialog {
			max-width: 1501px !important;
		}
        .modal-dialog2 {
            min-height: 1000px !important;
		}
		.modal{
		background-color: rgba(0,0,0,.8);
	}
	.separador{
		height: 20px;
	}
	.parrafo{
		padding: 40px;
		text-align: justify;
		font-size:13px;
		font-family: Arial,sans-serif;
	}
	.right-line-especial1 {
		border-right: 1.5px #0f0f4b solid;
		height:6em
		}
	</style>
	</head>
<body>
<div class="container text-center">
	<div class="row">
	  <div class="col-4"></div> 
	  <div class="col-4 divcircleministerio" style="background-color: #0f0f4b; color:white; width:10%" data-toggle="modal" data-target=".ministerio" ><b>MINISTERIO DEL DEPORTE</b></div>
	  <div class="col-4"></div> 
	</div>
	<div class="row">
	  <div class="col-6 right-line"></div>
	  <div class="col-6"></div>
	</div>
	<div class="row">
	  <div class="col-2"></div>
	  <div class="col-2"></div> 
	  <div class="col-1"></div>
	  <div class="col-2 divcircleministra" data-toggle="modal" data-target=".ministra"><b>Ministro/a</b></div>
	  <div class="col-1"></div>
 	  <div class="col-2"></div>
	  <div class="col-2"></div>
	</div>
	<div class="row">
		<div class="col-6 right-line"></div>
		<div class="col-6"></div>
	  </div>
	  <div class="row">
		<div class="col-1"></div>
		<div class="col-1"></div>
		<div class="col-1 right-line"></div>
		<div class="col-1 top-line"></div>
		<div class="col-1 top-line"></div>
		<div class="col-1 right-line top-line"></div>
		<div class="col-1 top-line"></div>
		<div class="col-1 top-line"></div>
		<div class="col-1 top-line"></div>
		<div class="col-1 top-line left-line"></div>
		<div class="col-1 right-line top-line"></div>
		<div class="col-1 "></div>
	  </div>
	  <div class="row">		
		<div class="col-1 "></div>
		<div class="col-1 "></div>
		<div class="col-2 level3" data-toggle="modal" data-target=".inter" >Área de Relaciones Internacionales</div>
		<div class="col-1 "></div>
		<div class="col-1 right-line-especial1"></div>
		<div class="col-1 "></div>
		<!-- <div class="col-2 level3" data-toggle="modal" data-target=".subse" ><b>SUBSECRETARIA</b></div> -->
		<div class="col-1 "></div>
		<div class="col-2 level3" data-toggle="modal" data-target=".gabineteministra" > Gabinete Ministerial</div>
		<div class="col-1 right-line-especial1"></div>
		<div class="col-1 "></div>
	  </div>

	  <div class="row">
		<div class="col-1"></div>
		<div class="col-1"></div>
		<div class="col-1 "></div>
		<div class="col-1 "></div>
		<div class="col-1 "></div>
		<div class="col-1 right-line"></div>
		<div class="col-1 "></div>
		<div class="col-1 "></div>
		<div class="col-1 "></div>
		<div class="col-1 "></div>
		<div class="col-1 right-line"></div>
		<div class="col-1 "></div>
	  </div>

	  <div class="row">
		<div class="col-1"></div>
		<div class="col-1"></div>
		<div class="col-1 "></div>
		<div class="col-1 "></div>
		<div class="col-1 "></div>
		<div class="col-2 level3" data-toggle="modal" data-target=".subse"> Subsecretario/a</div>
		<div class="col-1 top-line"></div>
		<div class="col-1 top-line"></div>
		<div class="col-1 top-line"></div>
	
		<div class="col-2 level3" data-toggle="modal" data-target=".auditoria" >Departamento de Auditoría</div>
	  </div>

	  <div class="row">
		<div class="col-2"></div>
		<div class="col-2"></div> 
		<div class="col-1"></div>
		<div class="col-1"></div>
		<div class="col-1"></div>
		<div class="col-2"></div>
		<div class="col-2"></div>
	  </div>
	<div class="row">
	<div class="col-1 "></div>
	<div class="col-1 "></div>
	<div class="col-1 "></div>
	<div class="col-1 "></div>
	<div class="col-1 "></div>
	<div class="col-1 right-line"></div>
	<div class="col-1 "></div>
	<div class="col-1 "></div>
	<div class="col-1 "></div>
	<div class="col-1 "></div>
	<div class="col-1 "></div>
	<div class="col-1 "></div>
	</div>
	<div class="row">
		<div class="col-1  right-line"></div>
		<div class="col-1 top-line"></div>
		<div class="col-1 top-line "></div>
		<div class="col-1  right-line top-line"></div>
		<div class="col-1 top-line"></div>
		<div class="col-1 right-line top-line"></div>
		<div class="col-1 top-line"></div>
		<div class="col-1 top-line"></div>
		<div class="col-1 left-line "></div>
		<div class="col-1 "></div>
		<div class="col-1 "></div>
	  </div>
	  <div class="row">
		<div class="col-2 level3" data-toggle="modal" data-target=".regionales">Secretarías Regionales Ministeriales</div>
		<div class="col-1 "></div>
		<div class="col-2 level3" data-toggle="modal" data-target=".gabinetesubsecretaria" >Gabinete Subsecretaría</div>
		<div class="col-1 right-line-especial1"></div>
		<div class="col-1 "></div>
		<div class="col-2 level3" data-toggle="modal" data-target=".seremias">Departamento de Coordinación de Seremías</div>
		<div class="col-1 "> </div>
		<!-- <div class="col-2 level3" data-toggle="modal" data-target=".auditoria" >Departamento de Auditoría</div> -->
	  </div>
	  <div class="row">
	  <div class="col-1 "></div>
		  <div class="col-1 left-line"></div>
		  <div class="col-1 "></div>
		  <div class="col-1 "></div>
		  <div class="col-1 "></div>
		  <div class="col-1  right-line"></div>
		  <div class="col-1 "></div>
		  <div class="col-1 "></div>
		  <div class="col-1 "></div>
		  <div class="col-1 "></div>
		  <div class="col-1 "></div>
	  </div>
	  
	  <div class="row">
		<div class="col-2 level3" data-toggle="modal" data-target=".equiposregionales"> Equipos de Apoyo de las Secretarías Regionales Ministeriales </div>
		<div class="col-1"></div>
		<div class="col-1"></div>
		<div class="col-1"></div>
		<div class="col-1 right-line-especial1"></div>
		<div class="col-1"></div>
		<div class="col-1"></div>
		<div class="col-1"></div>
		<div class="col-1"></div>
		<div class="col-1"></div>
		<div class="col-1"></div>
	  </div>
	  <div class="row">
		<div class="col-1"></div>
		<div class="col-1"></div>
		<div class="col-1"></div>
		<div class="col-1"></div>
		<div class="col-1"></div>
		<div class="col-1 right-line"></div>
		<div class="col-1"></div>
		<div class="col-1"></div>
		<div class="col-1"></div>
		<div class="col-1"></div>
		<div class="col-1"></div>
		<div class="col-1"></div>
	  </div>
	  <div class="row">
		<div class="col-1 "></div>
		<div class="col-1 top-line"></div>
		<div class="col-1 top-line"></div>
		<div class="col-1 top-line"></div>
		<div class="col-1 top-line"></div>
		<div class="col-1 top-line"></div>
		<div class="col-1 top-line"></div>
		<div class="col-1 top-line"></div>
		<div class="col-1 top-line"></div>
		<div class="col-1 top-line"></div>
		<div class="col-1 top-line"></div>
		<div class="col-1 "></div>
	  </div>
	  <div class="row">
		<div class="col-1 right-line"></div>
		<div class="col-1 "></div>
		<div class="col-1 right-line"></div>
		<div class="col-1 "></div>
		<div class="col-1 right-line"></div>
		<div class="col-1 "></div>
		<div class="col-1 right-line"></div>
		<div class="col-1 "></div>
		<div class="col-1 right-line"></div>
		<div class="col-1 "></div>
		<div class="col-1 right-line"></div>
		<div class="col-1 "></div>
	  </div>
	  <div class="row">
		<div class="col-2 level4" data-toggle="modal" data-target=".daf">División de Administración y Finanzas</div>
		<div class="col-2 level4" data-toggle="modal" data-target=".juridica">División Jurídica</div>
		<div class="col-2 level4" data-toggle="modal" data-target=".planificacion">División de Planificación y Control de Gestión</div>
		<div class="col-2 level4" data-toggle="modal" data-target=".comunicaciones">División de Comunicaciones y Relaciones Públicas</div>
		<div class="col-2 level4" data-toggle="modal" data-target=".infraestructura">División de Infraestructura Deportiva</div>
		<div class="col-2 level4" data-toggle="modal" data-target=".pgd">División de Política y Gestión Deportiva</div>
	  </div>
	  <div class="row">
		<div class="col-1 right-line"></div>
		<div class="col-1 "></div>
		<div class="col-1 "></div>
		<div class="col-1 "></div>
		<div class="col-1 "></div>
		<div class="col-1 "></div>
		<div class="col-1 "></div>
		<div class="col-1 "></div>
		<div class="col-1 "></div>
		<div class="col-1 "></div>
		<div class="col-1 right-line"></div>
		<div class="col-1 "></div>
	  </div>
	
	  <div class="row">
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 top-line"></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 top-line"></div>
				<div class="col-6 top-line"></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 top-line"></div>
				<div class="col-6 top-line"></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 top-line"></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 top-line"></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 top-line"></div>
				<div class="col-6 top-line"></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 top-line"></div>
				<div class="col-6 "></div>
			</div>
		</div>
	  </div>
	  <div class="row">
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 right-line"></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 right-line"></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 right-line"></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 right-line"></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 right-line"></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 right-line"></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 right-line"></div>
				<div class="col-6 "></div>
			</div>
		</div>
	  </div>
	  <div class="row">
		<div class="col-1 level4" data-toggle="modal" data-target=".personas">Área de Gestión y Desarrollo de Personas</div>
		<div class="col-1 level4" data-toggle="modal" data-target=".finanzas">Área de Finanzas</div>
		<div class="col-1 level4" data-toggle="modal" data-target=".informatica">Área de Informática </div>
		<div class="col-1 level4" data-toggle="modal" data-target=".gd">Área de Gestión de Documentos</div>
		<div class="col-1 "></div>
		<div class="col-1 "></div>
		<div class="col-1 "></div>
		<div class="col-1 "></div>
		<div class="col-1 "> </div>
		<div class="col-1 level4" data-toggle="modal" data-target=".estudios">Área de Estudios</div>
		<div class="col-1 level4" data-toggle="modal" data-target=".altorendimiento">Área de Competencia y Alto Rendimiento</div>
		<div class="col-1 level4" data-toggle="modal" data-target=".politicas">Área de Políticas</div>
	  </div>
	  <div class="row">
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
	  </div>
	  <div class="row">
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
	  </div>

	  <div class="row">
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
		<div class="col-1 ">
			<div class="row">
				<div class="col-6 "></div>
				<div class="col-6 "></div>
			</div>
		</div>
	  </div>

	  <div class="row">
		<div class="col-1 "></div>
		<div class="col-1 "></div>
		<div class="col-1 "> </div>
		<div class="col-1 "></div>
		<div class="col-1 "></div>
		<div class="col-1 "></div>
		<div class="col-1 "></div>
		<div class="col-1 "></div>
		<div class="col-1 "></div>
		<div class="col-1"></div>
		<div class="col-1 "></div>
		<div class="col-1 "></div>
	  </div>
</div>


<!-- <div class="modal fade ministerio" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
	  <div class="modal-content">
		MINISTERIO DEL DEPORTE
	  </div>
	</div>
  </div> -->

  <div class="modal fade ministra" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" style="width: 700px; height: 500px;">
	  <div class="modal-content">
		  <div class="modal-header">
			  <h5></h5>
			  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
			  </button>
		  </div>
		  <div class="row">
			  <div class="col-1 separador"></div>
		  </div>
		  <div class="row">
			  <div class="col-3"></div>
			  <div class="col-6">
			<img src="https://mindep.s3.amazonaws.com/nuevoorganigrama/ministra.png" width="285" height="285" border="0" class="cerrar"/>
		  </div>
		  </div>
		  <div class="row">
			  <div class="col-1 separador"></div>
		  </div>
		  <div class="row">
			  <div class="col-12 parrafo">
				<strong>Alexandra Benado Vergara, Ministra del Deporte</strong> Licenciada en Educación y Pedagogía en Educación Física, Deportes y Recreación por la Universidad Metropolitana de Ciencias de la Educación (UMCE) y actualmente realiza un diplomado sobre Gestión en la Industria del Deporte, en la Universidad de Chile. Se inició en el deporte a los 8 años, cuando en Francia integró el club Gradignan, siendo una de las pocas mujeres en participar. A comienzos de la década de los años 90’s formó parte del plantel del club amateur Sportivo Milano, para luego incorporarse a la rama de fútbol femenino de Palestino, club desde el que fue convocada para integrar la Selección Chilena. Al asumir el 11 de marzo del 2022, se convirtió en la primera mujer lesbiana en liderar una cartera ministerial en nuestro país.
		  </div>
		  </div>
	  </div>
	</div>
  </div>


  <div class="modal fade inter" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" style="width: 700px; height: 500px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<!-- <div class="row">
				<div class="col-1 separador"></div>
			</div>
			<div class="row">
				<div class="col-3"></div>
				<div class="col-6">
			  <img src="https://mindep.s3.amazonaws.com/nuevoorganigrama/subsecretaria.png"  width="285" height="285" border="0" class="cerrar"/>
			</div>
			</div>
			<div class="row">
				<div class="col-1 separador"></div>
			</div> -->
			
			<div class="row">
				<div class="col-12 parrafo">
					<strong>Área de Relaciones Internacionales</strong> A cargo de proponer a las autoridades los lineamientos y acciones para desarrollar la diplomacia deportiva del Ministerio del Deporte con otros organismos públicos y privados, nacionales e internacionales, con el objetivo de participar, tanto en programas de cooperación internacional en materia deportiva, como en la elaboración de convenios y acuerdos bilaterales o multilaterales sobre dichas materias.
					<br><br>
					<strong>Encargada/o <br> Andrea Navea Valenzuela </strong>
				
				</div>
			</div>

			
		</div>
	  </div>
  </div>



  <div class="modal fade subse" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" style="width: 700px; height: 500px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="row">
				<div class="col-1 separador"></div>
			</div>
			<div class="row">
				<div class="col-3"></div>
				<div class="col-6">
			  <img src="https://mindep.s3.amazonaws.com/nuevoorganigrama/subsecretaria.png"  width="285" height="285" border="0" class="cerrar"/>
			</div>
			</div>
			<div class="row">
				<div class="col-1 separador"></div>
			</div>
			<div class="row">
				<div class="col-12 parrafo">
					<strong>Antonia Illanes Riquelme, Subsecretaria del Deporte</strong> Abogada de la Universidad de Chile titulada el año 2008. Master en Estudios Jurídicos Avanzados, con especialidad Derecho Internacional de la Universidad de Barcelona. Fue abogada en el Vigésimo Segundo Juzgado Civil de Santiago. Se desempeñó también como abogada de la Corporación de Asistencia Judicial en el Centro de Atención Jurídico y Social de Lo Espejo. Desde 2018 y hasta el 10 de marzo de 2022 se ha desempeñado como jefa de gabinete de la diputación del Presidente de la República, Gabriel Boric, dirigiendo al equipo y desarrollando sus funciones en la Región de Magallanes y en el Congreso Nacional. Desde 1986 hasta la fecha practica ballet clásico.
				</div>
			</div>
		</div>
	  </div>
  </div>

  <div class="modal fade gabineteministra" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog2 modal-dialog-centered" style="width: 700px; height: 500px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="row">
				<div class="col-1 separador"></div>
			</div>
			<div class="row">
				<div class="col-3"></div>
				<div class="col-6">
			  <img src="https://mindep.s3.amazonaws.com/nuevoorganigrama/gabineteministra.png"  width="285" height="285" border="0" class="cerrar"/>
			</div>
			</div>
			<div class="row">
				<div class="col-1 separador"></div>
			</div>
			<div class="row">
				<div class="col-12 parrafo">
					<strong>Natalia Bravo Peña, Jefa de Gabinete de la Ministra Alexandra Benado</strong> Abogada de la Universidad de Chile, diplomada en Gestión Deportiva en la Universidad Católica de Valparaíso, especialista en derecho del trabajo y candidata a magister en Derecho del Trabajo y Seguridad Social de la Universidad de Talca. 
			    </div>
			</div>
            
            <div class="row">
				<div class="col-6 parrafo">
                    <center>
			        <img src="https://mindep.s3.amazonaws.com/nuevoorganigrama/asesoragenero.png"  width="180" height="180" border="0" class="cerrar"/>
                    </center>
                    <br><br><strong>Loreto Fuenzalida Navarrete, Encargada de Género Mindep </strong>Trabajadora Social de la Pontificia Universidad Católica de Chile, diplomada en Género y Violencia de la Universidad de Chile y actualmente realiza un diplomado en Género y Deporte en la Universidad de Buenos Aires.  Tiene experiencia en deporte social y educativo, trabajando en programas "El Partido de mi Barrio" y "Retorno al Aula Seguro" de Fundación Fútbol Más, además de su paso por Cruzados, fomentando el vínculo de los clubes de fútbol profesional con la comunidad. 
                    

			    </div>
                <div class="col-6 parrafo">
                    <center>
			        <img src="https://mindep.s3.amazonaws.com/nuevoorganigrama/asesorministra.png"  width="180" height="180" border="0" class="cerrar"/>
                    </center>
                    <br><br><strong>José Miguel Guerrero Meza, Asesor de Comunicaciones Ministra </strong>Periodista de la Universidad Alberto Hurtado, 40 años.  Experiencia en medios de comunicación (Diario Siete, Terra, La Tercera). Asesorías en comunicación de crisis, estratégica, gestión de prensa en el Parlamento con la bancada de diputadas y diputados del PPD, las diputadas Loreto Carvajal y Ximena Vidal. También en el Senado de la República con las parlamentarias Loreto Carvajal y Ximena Órdenes. Parte del equipo de comunicaciones de la Subsecretaria del Interior entre el 2014 y el 2018. 
                    
			    </div>
			</div>

			<div class="row">
				<div class="col-12 parrafo">
					<strong>Funciones Gabinete Ministerial: </strong> Estará a cargo de organizar, en coordinación con el/la Ministro/a, su agenda de actividades tanto externas como al interior del Ministerio y, colaborar, en general, mediante un equipo de trabajo compuesto por un/a Jefe/a de Gabinete y un staff de asesores que contribuyan a cumplir con los objetivos y funciones del Ministerio. 
			    </div>
			</div>

            <div class="row">
				<div class="col-1 separador"></div>
			</div>

		</div>
	  </div>
  </div>

  <div class="modal fade gabinetesubsecretaria" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" style="width: 700px; height: 500px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="row">
			<div class="col-1 separador"></div>
			</div>
			<div class="row">
				<div class="col-3"></div>
				<div class="col-6">
			  <img src="https://mindep.s3.amazonaws.com/nuevoorganigrama/gabinetesubsecretaria.png"  width="285" height="285" border="0" class="cerrar"/>
			</div>
			</div>
			
			

			<div class="row">
				<div class="col-12 parrafo">
					<strong>Paulette Jara Pérez, Jefa de Gabinete de la Subsecretaria Antonia Illanes </strong> San Felipeña, abogada y dibujante. Magister en derecho público de la Universidad Diego Portales y LLM en la Universidad de Wisconsin Madison en Estados Unidos.  Jefa de Gabinete en el segundo periodo de la diputación del Presidente Gabriel Boric; ayudante y profesora de derecho constitucional y abogada litigante en causas de derecho público. Tiene bajo su responsabilidad la jefatura del gabinete de la Subsecretaria del Deporte.
				</div>
			</div>
			<div class="row">
				<div class="col-12 parrafo">
                    <center>
			        <img src="https://mindep.s3.amazonaws.com/nuevoorganigrama/prensasubsecretaria.png"  width="180" height="180" border="0" class="cerrar"/>
                    </center>
                    <br><br><strong>Carla Ramírez Méndez, Jefa de Prensa Subsecretaria </strong>Relacionadora Pública de la Universidad Santo Tomás, diplomado en Desarrollo, Pobreza y Territorio de la Universidad Alberto Hurtado. Fue parte del equipo territorial en Magallanes de la diputación del Presidente de la República, Gabriel Boric. Previamente tuvo experiencia en diversos servicios públicos como la Subsecretaría de Pesca y Acuicultura en Punta Arenas y el Instituto Nacional de Estadísticas en Curicó, desarrollando las áreas de Comunicaciones, Capacitaciones y Trabajo con Comunidades, entre otros. 
			   
					<br><br>
					<strong>Funciones Gabinete Subsecretaria: </strong> Estará a cargo de la gestión institucional del Servicio. Tiene por función el control y supervisión general de la gestión del Ministerio del Deporte, mediante un equipo de trabajo compuesto por un/a Jefe/a de Gabinete y un staff de asesores, que brindan apoyo profesional y técnico en las acciones necesarias para la elaboración, implementación, coordinación y seguimiento de los programas y planes que contribuyan a cumplir con los objetivos y funciones del Ministerio. <br> 
					
					<br>Asimismo, y por tratarse de una función estratégica del Servicio, le corresponderá al Gabinete de la Subsecretaría la articulación intersectorial con otras instituciones públicas y privadas con la finalidad de contribuir al diseño, formulación, implementación y evaluación de políticas, planes y programas de actividad física y deporte. 
				
				</div>
			</div>

	
            <div class="row">
				
			</div>
		</div>
	  </div>
  </div>

  <div class="modal fade auditoria" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" style="width: 700px; height: 500px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<!-- <div class="row">
				<div class="col-1 separador"></div>
			</div>
			<div class="row">
				<div class="col-3"></div>
				<div class="col-6">
			  <img src="https://mindep.s3.amazonaws.com/nuevoorganigrama/subsecretaria.png"  width="285" height="285" border="0" class="cerrar"/>
			</div>
			</div>
			<div class="row">
				<div class="col-1 separador"></div>
			</div> -->
			<div class="row">
				<div class="col-12 parrafo">
					<strong>Departamento de Auditoría</strong> Responsable de asesorar a las autoridades de la Institución en materias de control interno para el cumplimiento de sus objetivos, funciones y responsabilidades. Lo anterior, a través de la elaboración, ejecución y seguimiento de programas de auditorías, desplegando una estrategia preventiva que proponga acciones que contribuyan a la mejora continua de los procesos, verificando el cumplimiento normativo y el uso eficiente y eficaz de los recursos públicos por parte de cada una de las áreas del Ministerio, manteniendo una coordinación permanente con el Consejo de Auditoría General de Gobierno, las Unidades de Auditoría Interna de los Servicios Públicos y organismos privados relacionados.
			</div>
			</div>
		</div>
	  </div>
  </div>


  <div class="modal fade seremias" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" style="width: 700px; height: 500px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="row">
				<div class="col-1 separador"></div>
			</div>
			<div class="row">
				<div class="col-3"></div>
				<div class="col-6">
			  <img src="https://mindep.s3.amazonaws.com/nuevoorganigrama/seremia.png"  width="285" height="285" border="0" class="cerrar"/>
			</div>
			</div>
			<div class="row">
				<div class="col-1 separador"></div>
			</div>
			<div class="row">
				<div class="col-12 parrafo">
					<strong>Cristián Bahamonde Ortega, Jefe de Coordinación Seremías</strong> Magallánico, Profesor de Educación Física de la Universidad de Magallanes, Magister en Gestión y Liderazgo, Diplomados en: “Control de Gestión”, “Gestión estratégica”, “Políticas y Gestión Educacional”, “Liderazgo para la mejora del aprendizaje” y postítulo en “Orientación Escolar”. Ex encargado de Estrategias y Control de Gestión en JUNJI Región de Magallanes, sostenedor académico de la Comuna de Río Verde y en diferentes programas de niñez y adolescentes del Área de Atención al Menor de la Corporación Municipal de Punta Arenas. Tiene a su cargo la coordinación y gestión administrativa y técnica de la información, planes y programas de las Secretarías Regionales Ministeriales.
					<br><br>
					<strong>Funciones Departamento de Coordinación de Seremías: </strong> Estará a cargo de un/a Jefe/a de Departamento y tendrá bajo su responsabilidad la coordinación, gestión administrativa y técnica de la información, planes y programas de cada región.Tendrá como funciones específicas las siguientes: <br>

					<br><b>a.</b> Coordinar, solucionar y realizar seguimiento a las solicitudes en materias administrativas de los/as Secretarios/as Regionales Ministeriales con las distintas divisiones. 
					<br><b>b.</b> Recepcionar, ordenar y efectuar seguimiento de la documentación del departamento. 
					<br><b>c.</b> Efectuar seguimiento y coordinar los indicadores ministeriales comprometidos por el Departamento. (Ver todas las funciones en la Resolución Exenta N° 404, al final de la página).
				
				</div>
			</div>

			
		</div>
	  </div>
  </div>


  <div class="modal fade regionales" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" style="width: 700px; height: 500px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<!-- <div class="row">
				<div class="col-1 separador"></div>
			</div>
			<div class="row">
				<div class="col-3"></div>
				<div class="col-6">
			  <img src="https://mindep.s3.amazonaws.com/nuevoorganigrama/subsecretaria.png"  width="285" height="285" border="0" class="cerrar"/>
			</div>
			</div>
			<div class="row">
				<div class="col-1 separador"></div>
			</div> -->
			<div class="row">
				<div class="col-12 parrafo">
					<strong>Secretarías Regionales Ministeriales</strong> Son órganos desconcentrados del Ministerio del Deporte, distribuidos en cada una de las regiones del país. Encabezadas por un/a Secretario/a Regional Ministerial (SEREMI), quien representa al/a la Jefe/a de la Cartera Ministerial en la región designada. Los/as Secretarios/as Regionales Ministeriales cumplen un rol fundamental en la puesta en marcha de las políticas del Ministerio del Deporte, en cada una de las diversas zonas del país, a través de la aplicación de las medidas necesarias que permitan impulsar el desarrollo y bienestar de la actividad deportiva. Asimismo, son responsables de la administración general de las dependencias y de la vinculación del Ministerio con las Intendencias y Gobiernos Regionales. 
			</div>
			</div>
		</div>
	  </div>
  </div>

  <div class="modal fade equiposregionales" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" style="width: 700px; height: 500px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<!-- <div class="row">
				<div class="col-1 separador"></div>
			</div>
			<div class="row">
				<div class="col-3"></div>
				<div class="col-6">
			  <img src="https://mindep.s3.amazonaws.com/nuevoorganigrama/subsecretaria.png"  width="285" height="285" border="0" class="cerrar"/>
			</div>
			</div>
			<div class="row">
				<div class="col-1 separador"></div>
			</div> -->
			<div class="row">
				<div class="col-12 parrafo">
					<strong>Equipos de Apoyo de las Secretarías Regionales Ministeriales</strong> Les corresponderá efectuar las labores dispuestas en el artículo precedente encomendadas por el/la respectivo Secretario/a Regional Ministerial del Deporte.
			</div>
			</div>
		</div>
	  </div>
  </div>

  <div class="modal fade daf" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" style="width: 700px; height: 500px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="row">
				<div class="col-1 separador"></div>
			</div>
			<div class="row">
				<div class="col-3"></div>
				<div class="col-6">
			  <img src="https://mindep.s3.amazonaws.com/nuevoorganigrama/daf.png"  width="285" height="285" border="0" class="cerrar"/>
			</div>
			</div>
			<div class="row">
				<div class="col-1 separador"></div>
			</div>
			<div class="row">
				<div class="col-12 parrafo">
					<strong>Francisco Atenas, Jefe de División de Administración y Finanzas</strong> Administrador Público de la Universidad de Chile como estudios de diplomado en estrategias de control y Gestión. Su carrera profesional la desarrolló en el Ministerio del Interior, en el Departamento de Recursos Humanos, en Finanzas y, posteriormente, como Jefe del Departamento de Control de Gestión. Posteriormente y hasta antes de asumir, se desempeñó como responsable del área de Recursos Humanos del Diario Oficial. Tiene bajo su responsabilidad gestionar y administrar los recursos físicos y financieros, tecnológicos y humanos del Ministerio, de forma eficiente y de acuerdo con el marco normativo y regulatorio vigente, contribuyendo al cumplimiento de los objetivos institucionales.
			</div>
			</div>
		</div>
	  </div>
  </div>

  <div class="modal fade personas" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" style="width: 700px; height: 500px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<!-- <div class="row">
				<div class="col-1 separador"></div>
			</div>
			<div class="row">
				<div class="col-3"></div>
				<div class="col-6">
			  <img src="https://mindep.s3.amazonaws.com/nuevoorganigrama/subsecretaria.png"  width="285" height="285" border="0" class="cerrar"/>
			</div>
			</div>
			<div class="row">
				<div class="col-1 separador"></div>
			</div> -->
			<div class="row">
				<div class="col-12 parrafo">
					<strong>Área de Gestión y Desarrollo de Personas</strong> Le corresponde liderar y coordinar la definición, elaboración e implementación de estrategias, políticas y planes definidos para el desarrollo del Servicio en materia de personas, estableciendo procedimientos adecuados para la administración, mejoramiento y desarrollo del personal. Asimismo, articula los diferentes actores que intervienen en la vida laboral de los trabajadores y trabajadoras procurando su desarrollo integral, buen clima laboral y correcta tramitación de sus requerimientos. Para ello, cumple una labor de coordinación tanto al interior del Servicio como con instituciones relacionadas al Ministerio en estas materias.
			</div>
			</div>
		</div>
	  </div>
  </div>

  <div class="modal fade finanzas" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" style="width: 700px; height: 500px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<!-- <div class="row">
				<div class="col-1 separador"></div>
			</div>
			<div class="row">
				<div class="col-3"></div>
				<div class="col-6">
			  <img src="https://mindep.s3.amazonaws.com/nuevoorganigrama/subsecretaria.png"  width="285" height="285" border="0" class="cerrar"/>
			</div>
			</div>
			<div class="row">
				<div class="col-1 separador"></div>
			</div> -->
			<div class="row">
				<div class="col-12 parrafo">
					<strong>Área de Finanzas</strong> Le corresponde dirigir, controlar y administrar los recursos físicos y financieros de la Institución, gestionando y ejecutando procesos contables, presupuestarios y administrativos de su área, además de generar información estratégica y confiable para la toma de decisiones de la División y de las autoridades del Servicio.
			</div>
			</div>
		</div>
	  </div>
  </div>

  <div class="modal fade informatica" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" style="width: 700px; height: 500px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<!-- <div class="row">
				<div class="col-1 separador"></div>
			</div>
			<div class="row">
				<div class="col-3"></div>
				<div class="col-6">
			  <img src="https://mindep.s3.amazonaws.com/nuevoorganigrama/subsecretaria.png"  width="285" height="285" border="0" class="cerrar"/>
			</div>
			</div>
			<div class="row">
				<div class="col-1 separador"></div>
			</div> -->
			<div class="row">
				<div class="col-12 parrafo">
					<strong>Área de Informática</strong> Le corresponde gestionar y supervisar el funcionamiento y desarrollo de los sistemas informáticos de la Subsecretaría en coordinación con el Instituto Nacional del Deportes de Chile (IND), de manera de asegurar el correcto funcionamiento de la infraestructura tecnológica en cumplimiento con la normativa e instructivos vigentes.
			</div>
			</div>
		</div>
	  </div>
  </div>

  <div class="modal fade gd" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" style="width: 700px; height: 500px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<!-- <div class="row">
				<div class="col-1 separador"></div>
			</div>
			<div class="row">
				<div class="col-3"></div>
				<div class="col-6">
			  <img src="https://mindep.s3.amazonaws.com/nuevoorganigrama/subsecretaria.png"  width="285" height="285" border="0" class="cerrar"/>
			</div>
			</div>
			<div class="row">
				<div class="col-1 separador"></div>
			</div> -->
			<div class="row">
				<div class="col-12 parrafo">
					<strong>Área de Gestión de Documentos</strong> Le corresponde recepcionar, despachar, archivar y custodiar el flujo de documentación que se recibe y envía desde el Ministerio del Deporte, procurando cumplir con los plazos asignados y con los procedimientos correspondientes para ello.
			</div>
			</div>
		</div>
	  </div>
  </div>

  <div class="modal fade juridica" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" style="width: 700px; height: 500px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="row">
				<div class="col-1 separador"></div>
			</div>
			<div class="row">
				<div class="col-3"></div>
				<div class="col-6">
			  <img src="https://mindep.s3.amazonaws.com/nuevoorganigrama/juridica.png"  width="285" height="285" border="0" class="cerrar"/>
			</div>
			</div>
			<div class="row">
				<div class="col-1 separador"></div>
			</div>
			<div class="row">
				<div class="col-12 parrafo">
					<strong>Verónica Puentes Sáenz, Jefa de División Jurídica</strong> Abogada de la Universidad de Chile, 60 años. Diplomada en Reforma Procesal Penal en la Pontificia Universidad Católica de Chile. Trabajó como oficial político de derechos humanos para la Misión de Paz y la Comisión de la Verdad de Naciones Unidas en Guatemala y para la Comisión de la Verdad y la Reconciliación en Honduras. En Chile se ha desempeñado como Jefa del Departamento Jurídico del Consejo Nacional de la Cultura y las Artes; Jefa de la Unidad de decretos y resoluciones, Encargada del Departamento de Adquisición y Administración de Bienes del Ministerio de Bienes Nacionales y abogada del comité normativo de la División Jurídica del Ministerio de Educación.
			</div>
			</div>
		</div>
	  </div>
  </div>

  <div class="modal fade planificacion" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" style="width: 700px; height: 500px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="row">
				<div class="col-1 separador"></div>
			</div>
			<div class="row">
				<div class="col-3"></div>
				<div class="col-6">
			  <img src="https://mindep.s3.amazonaws.com/nuevoorganigrama/planificacion3.png"  width="285" height="285" border="0" class="cerrar"/>
			</div>
			</div>
			<div class="row">
				<div class="col-1 separador"></div>
			</div>
			<div class="row">
				<div class="col-12 parrafo">
					<strong>Evelyn Freire Ramos, Jefa de División de Planificación y Control de Gestión</strong> Administradora Pública de la Universidad de Santiago, 40 años, Diplomada en Gerencia Pública y Magíster en Gestión y Políticas Públicas del Departamento de Ingeniería Industrial de la Universidad de Chile. Ha sido Jefa de Planificación y Control de Gestión en la Subsecretaría de Obras Públicas, el Programa Nacional de Fiscalización del Ministerio de Transportes y Telecomunicaciones; Auditora Sectorial del Ministerio de Justicia y Derechos Humanos, entre otros cargos en distintos ministerios y subsecretarías; además fue Directora de la Escuela de Administración de la Universidad Mayor. Es académica hace 10 años en distintas universidades. Tiene bajo su responsabilidad dirigir y administrar los procesos y sistemas de planificación estratégica y control de gestión de la institución, asegurando su calidad y oportunidad.
			</div>
			</div>
		</div>
	  </div>
  </div>

  <div class="modal fade comunicaciones" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" style="width: 700px; height: 500px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="row">
				<div class="col-1 separador"></div>
			</div>
			<div class="row">
				<div class="col-3"></div>
				<div class="col-6">
			  <img src="https://mindep.s3.amazonaws.com/nuevoorganigrama/comunicaciones.jpeg"  width="285" height="285" border="0" class="cerrar"/>
			</div>
			</div>
			<div class="row">
				<div class="col-1 separador"></div>
			</div>
			<div class="row">
				<div class="col-12 parrafo">
					<strong>Felipe Gianoni Jorquera, Jefe División de Comunicaciones y Relaciones Públicas</strong> Licenciado en Comunicación Social y Periodista de la Universidad Católica del Norte. Diplomado en la Universidad Católica de Chile en Gestión de Situaciones de Crisis y Emergencia. Actualmente, se encuentra concluyendo el Magister en Comunicación Política y Asuntos Públicos en la Universidad Adolfo Ibáñez.<br>
					Se ha desempeñado como asesor de comunicaciones y prensa en la Subsecretaria de Desarrollo Regional (Subdere), Subsecretaria del Interior y Fiscalía Nacional. También ha prestado asesorías como consultor independiente en materia comunicacional y trabajado en medios de prensa nacional.<br><br>
					<strong>Funciones División de Comunicaciones y Relaciones Públicas.</strong><br>
					Estará a cargo de un/a Jefe/a de División que tendrá bajo su responsabilidad asesorar a las autoridades elaborando una estrategia comunicacional, con el fin de visualizar la labor institucional, a través de una planificación estratégica que levante hitos estratégicos, que permitan instalar a las autoridades como una referencia en deporte y actividad física.<br><br>
					<strong>Tendrá como funciones específicas:</strong><br>
					<strong>a. </strong>Difundir las iniciativas y proyectos del sector, tanto externa como internamente.<br>
					<strong>b. </strong>Asesorar y apoyar comunicacionalmente a las Autoridades del Deporte y a las<br>
					Secretarías Regionales Ministeriales<br>
					(Ver todas las funciones en la Resolución Exenta N° 404, al final de la página).
			</div>
			</div>
		</div>
	  </div>
  </div>

  <div class="modal fade infraestructura" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" style="width: 700px; height: 500px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<!--<div class="row">
				<div class="col-1 separador"></div>
			</div>
			 <div class="row">
				<div class="col-3"></div>
				<div class="col-6">
			  <img src="https://mindep.s3.amazonaws.com/nuevoorganigrama/infraestructura.png"  width="285" height="285" border="0" class="cerrar"/>
			</div>
			</div> -->
			<div class="row">
				<div class="col-1 separador"></div>
			</div>
			<div class="row">
				<div class="col-12 parrafo">
					<strong>División de Infraestructura y Recintos Deportivos</strong><br><br>
					Tendrá como funciones específicas las siguientes:<br><br>
					<b>a.</b> Proponer a las Autoridades Ministeriales las pautas para la definición de la Política de Infraestructura Deportiva, en concordancia con la Política Nacional del Deporte.<br>

					<b>b.</b> Colaborar en la formulación de planes y programas de infraestructura deportiva, en 
					concordancia con los planes y programas de actividad física y deportes.<br>

					<b>c.</b> Desarrollar metodologías de evaluación, definición de estándares y requisitos para 
					otorgar pertinencia técnica a los proyectos de infraestructura de deportiva pública.<br>

					<b>d.</b> Confeccionar y administrar un catastro de infraestructura deportiva, a nivel nacional y regional; distinguiendo aquellas cuya construcción, 
					reparación, mantención o administración, se financie total o parcialmente con recursos públicos.<br>

					<b>e.</b> Elaborar los informes que se remitirán al Ministerio de Desarrollo Social, sobre las 
					iniciativas de inversión en infraestructura deportiva, sometidas a evaluación que sean 
					coherentes con la política nacional de infraestructura deportiva pública, así como 
					comunicar la disponibilidad de recintos para la práctica del deporte en el sector territorial de cada una de las referidas iniciativas.<br>

					<b>f.</b> Proporcionar a la División de Administración y Finanzas los antecedentes necesarios 
					para la formulación de propuestas del presupuesto ministerial para los Programas de 
					Infraestructura Deportiva.<br>

					<b>g.</b> Cumplir con las demás tareas y funciones que las Autoridades del Ministerio le asignen o deleguen.<br>

			</div>
			</div>
		</div>
	  </div>
  </div>

  <div class="modal fade pgd" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" style="width: 700px; height: 500px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5><strong> </strong></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="row">
				<div class="col-1 separador"></div>
			</div>
			<div class="row">
				<div class="col-3"></div>
				<div class="col-6">
			<!--img src=""  width="285" height="285" border="0" class="cerrar"/-->
			</div>
			</div>
			<div class="row">
				
			</div>
			<div class="row">
				<div class="col-12 parrafo">
				<strong>División de Política y Gestión Deportiva</strong><br><br>
			    Entre sus principales funciones destacan: <br><br>
				•	Asesorar al/a la Ministro/a en la función de proponer y evaluar la Política Nacional del Deporte y los planes generales en materia deportiva e informar periódicamente sobre sus avances y cumplimiento. <br>
				•	Proponer al/a la Ministro/a políticas y planes en materia deportiva, y, colaborarle en la formulación de programas y acciones destinados al desarrollo de la actividad física y deportiva de la población, tanto de la práctica del deporte convencional como adaptado. <br>
				•	Colaborar en la evaluación del avance y cumplimiento de las políticas, planes, programas y acciones destinados al desarrollo de la actividad física y deportiva. 

			</div>
			</div>
		</div>
	  </div>
  </div>

  <div class="modal fade estudios" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" style="width: 700px; height: 500px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<!-- <div class="row">
				<div class="col-1 separador"></div>
			</div>
			<div class="row">
				<div class="col-3"></div>
				<div class="col-6">
			  <img src="https://mindep.s3.amazonaws.com/nuevoorganigrama/subsecretaria.png"  width="285" height="285" border="0" class="cerrar"/>
			</div>
			</div>
			<div class="row">
				<div class="col-1 separador"></div>
			</div> -->
			<div class="row">
				<div class="col-12 parrafo">
					<strong>Área de Estudios</strong> Está a cargo de la realización de estudios relacionados con la actividad física y ciencias del deporte. Además, debe implementar seminarios nacionales e internacionales, que den cuenta del desarrollo de la actividad física y deporte.
			</div>
			</div>
		</div>
	  </div>
  </div>

  <div class="modal fade politicas" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" style="width: 700px; height: 500px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<!-- <div class="row">
				<div class="col-1 separador"></div>
			</div>
			<div class="row">
				<div class="col-3"></div>
				<div class="col-6">
			  <img src="https://mindep.s3.amazonaws.com/nuevoorganigrama/subsecretaria.png"  width="285" height="285" border="0" class="cerrar"/>
			</div>
			</div>
			<div class="row">
				<div class="col-1 separador"></div>
			</div> -->
			<div class="row">
				<div class="col-12 parrafo">
					<strong>Área de Políticas</strong> Está a cargo de la gestión de las políticas públicas a nivel nacional, regional y local, respecto a la actividad física y deporte recreativo formativo, generando procesos de inclusión.
			</div>
			</div>
		</div>
	  </div>
  </div>

  <div class="modal fade altorendimiento" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" style="width: 700px; height: 500px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<!-- <div class="row">
				<div class="col-1 separador"></div>
			</div>
			<div class="row">
				<div class="col-3"></div>
				<div class="col-6">
			  <img src="https://mindep.s3.amazonaws.com/nuevoorganigrama/subsecretaria.png"  width="285" height="285" border="0" class="cerrar"/>
			</div>
			</div>
			<div class="row">
				<div class="col-1 separador"></div>
			</div> -->
			<div class="row">
				<div class="col-12 parrafo">
					<strong>Área de Competencia y Alto Rendimiento</strong> <br><br>
			    Entre sus principales funciones destacan: <br><br>
				•	Proponer políticas y planes destinados al desarrollo del deporte de alto rendimiento, tanto convencional como adaptado.<br> 
				•	Diseñar, evaluar y proponer ajustes a los programas vinculados con el desarrollo del deporte competitivo y de alto rendimiento, tendiendo a fortalecer a todos sus estamentos.<br>
				•	Desarrollar acciones de coordinación y articulación intersectorial que favorezcan el desarrollo del deporte de alto rendimiento y del deportista.<br> 
				•	Monitorear y evaluar permanentemente la implementación del Plan Estratégico Nacional de Deporte Competitivo y de Alto Rendimiento.<br>
				•	Coordinar los principales actores que forman parte de la institucionalidad deportiva del deporte competitivo y de alto rendimiento.<br>
				•	Colaborar en la formulación de programas y acciones destinadas a la detección y proyección de atletas para el alto rendimiento, convencional y paralímpico, como su post carrera. 
			</div>
			</div>
		</div>
	  </div>
  </div>

</body>
</html>