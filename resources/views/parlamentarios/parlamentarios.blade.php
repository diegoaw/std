@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><b><i class="fa fa-plane"></i> Agenda Viajes Parlamentarios </b><b style="color:DarkRed;">{{date("Y")}}/{{date("Y")+1}} </b></h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title">Filtar Viajes Parlamentario(s)</h5>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">

          {!! Form::open(['route' => 'parlamentarios', 'method' => 'get' , 'class'=>"form-horizontal"]) !!}
                <div class="form-row">
                    

                    <div class="form-group   {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                    <div class="input-group date">
                      <div class="col">
                          <label for="FechaDesde" class=" col-form-label">Fecha Desde: </label>
                          <input type="text" class="form-control group-date" id="FechaDesde" name="FechaDesde" readonly="readonly" value="{!! date('Y-m-d', strtotime($feDesde))  !!}">
                      </div>
                    </div>
                    </div>

                    <div class="form-group   {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                    <div class="input-group date">
                      <div class="col">
                          <label for="FechaHasta" class=" col-form-label">Fecha Hasta: </label>
                          <input type="text" class="form-control group-date" id="FechaHasta" name="FechaHasta" readonly="readonly" value="{!! date('Y-m-d', strtotime($feHasta ))  !!}">
                      </div>
                    </div>
                    </div>

                    
                    <div class="form-group align-self-end">
                  <div class="col-sm-12">
                      <div class="text-center">
                        <button type="submit" class="btn btn-primary">Buscar</button>
                      </div>
                  </div>
                </div>
                    
                    
                    
                </div>  
                
                <div class="form-row">
                  <div class="col-sm-12">
                      
                  </div>
                </div>

              {!! Form::close() !!}

          <div class="form-row">
            <div class="col-sm-12">
              <div class="text-center">
                {!! link_to_route('parlamento_crear', 'Nuevo Viaje Parlamentario(s)', [], ['class' => 'btn btn-success btn-sm','title'=>'Nuevo Viaje Parlamentario(s)']) !!}
                </div>
              </div>
            </div>


          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@if(count($parlamentarios)>0)
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
          <div class="card-header ">
        <h5 class="card-title"><b style="color:DarkRed;">Viajes Parlamentario(s)</b></h5>
      

          <div class="form-group">
            <div class="col-sm-12">
              <div class="text-center">
                  {!! Form::open(['route' => 'parlamento_reporte', 'method' => 'get' , 'class'=>"form-horizontal"]) !!}
                    <input type="submit" onclick="inload()" class= "btn btn-info" name="imprimir" value='XLS' id='imprimir'>
                  {!! Form::close() !!}
                <!--<a href="{{url('AgendaViajesParlamentos/Reporte', array('tiporeporte' => 'XLS'))}}" class="btn btn-primary btn-info " role="button">XLS</a>-->
              </div>
            </div>
          </div>

   
          </div>

          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th nowrap style="color:white"><center>ID</center></th>
                        <th style="color:white"><center>Ministerio</center></th>
                        <th style="color:white"><center>Nombre de La Actividad</center></th>
                        <th style="color:white"><center>Lugar</center></th>
                        <th style="color:white"><center>Fecha de Realización</center></th>
                        <th style="color:white"><center>Parlamentario/a Invitado/a</center></th>
                        <th style="color:white"><center>Institución que Convoca</center></th>
                        <th style="color:white"><center>Fecha y Participación Estimada de Participación de parlamentaria/o</center></th>
                        <th style="color:white"><center>Observación</center></th>
                        <th style="color:white"><center>Editar</center></th>
                        <th style="color:white"><center>Eliminar</center></th>
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($parlamentarios)>0)
                      @foreach ($parlamentarios as $indice => $tramite)
                          <tr>
                           <td nowrap><center>{{$tramite->id}}</center></td>
                           <td><center>{{$tramite->ministerio}}</center></td>
                           <td><center>{{$tramite->nombre_actividad}}</center></td>
                           <td><center>{{$tramite->pais}}</center></td>
                           <td><center>{{$tramite->fecha_realizacion}}</center></td>
                           <td><center>{{$tramite->parlamentario_invitado}}</center></td>
                           <td><center>{{$tramite->institucion_convoca}}</center></td>
                           <td><center>desde {{$tramite->fecha_inicio}}  a  {{$tramite->fecha_fin}}</center></td>
                           <td><center>{{$tramite->observacion}}</center></td>
                           <td><center><a class="btn accion btn-success accion" href="{{route('parlamento_editar',$tramite->id)}}"><i class="fa fa-pencil"></i></a></center></td>
                           <td><center><a class="btn accion btn-danger accion" href="{{route('parlamento_destroy',$tramite->id)}}"><i class="fa fa-trash"></i></a></center></td>
                          </tr>
                      @endforeach
                @endif
                </tbody>
              </table>
          </div>
      </div>
  </div>
</div>
@endif
@endsection
@section('before-scripts-end')
<script type="text/javascript">
  function inload(){
  setTimeout(function(){ document.getElementById("load").style.display="none"; }, 5000);

  }
</script>
@include('includes.scripts.parlamentarios')
@include('includes.scripts.script_fecha.scripts_filtro_fecha')
@stop
