@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
    <h1><i class="fa fa-plane"></i> Editar Viajes Parlamentarios</h1><br>
    </div>    
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')

{!! Form::open(['route' => ['parlamento_update' , $editParlamento->id], 'files' => true, 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','id'=>'form-create']) !!}

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-danger card-outline">
        <h3 class="card-title">Editar Viajes Parlamentarios</h3>
      </div>
      <div class="card-body">

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>

        <div class="callout col-sm-8 offset-sm-2" style="color: #737373;">
          <h4 style="font-size: 14px;"><i class="fa fa-info-circle" style="color: #ff0000;"></i> Nota:</h4>
          <span>Los campos marcados con un asterisco (*) son obligatorios.</span>
        </div><br>

        <div class="form-group row">
          {!! Form::label('Ministerio', 'Ministerio(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('ministerio', null, ['class' => 'form-control', 'placeholder' => 'Ministerio del Deporte', 'readonly' => 'readonly']) !!}
          </div>
        </div>   

        <div class="form-group row">
          {!! Form::label('Nombre de La Actividad', 'Nombre de La Actividad(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('nombre_actividad', $editParlamento->nombre_actividad, ['class' => 'form-control', 'placeholder' => 'Nombre de La Actividad']) !!}
          </div>
        </div> 

        <div class="form-group row">
           {!! Form::label('Seleccione País', 'Seleccione País(*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
          {!! Form::select('pais',$selectPais, $editParlamento->pais, ['id'=>'pais','name'=>'pais', 'class'=>'form-control', 'onchange'=>'function()']) !!}
          </div>
        </div>

       <!--  <div class="form-group row">
          <label for="ciudad" class="col-sm-3 col-form-label">Ciudad(*): </label>
            <div class="col-sm-6">
              <input type="text" name="ciudad" class="form-control" readonly="readonly" id="ciudad">
            </div>
        </div> -->

        <div class="form-group row {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
          {!!Form::label('FechaRealizacion', 'Fecha Realización(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
          <div class="input-group date">
                <input type="text" class="form-control group-date" id="fecha_realizacion" name="fecha_realizacion"  value="{!! date('Y-m-d', strtotime($editParlamento->fecha_realizacion))  !!}">
                <div class="input-group-append">
                  <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
              </div>
          </div>
        </div> 
      
        <div class="form-group row">
          {!! Form::label('Parlamentario/a Invitado/a', 'Parlamentario/a Invitado/a(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('parlamentario_invitado', $editParlamento->parlamentario_invitado, ['class' => 'form-control', 'placeholder' => 'Parlamentario/a Invitado/a']) !!}
          </div>
        </div> 

        <div class="form-group row">
          {!! Form::label('Institución que Convoca', 'Institución que Convoca(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('institucion_convoca', $editParlamento->institucion_convoca, ['class' => 'form-control', 'placeholder' => 'Institución que Convoca']) !!}
          </div>
        </div>   

        <p style="text-align:center; color:DarkRed;"><b>FECHA Y PARTICIPACIÓN ESTIMADA DE PARTICIPACIÓN DE PARLAMENTARIA/O.</b></p>
        <div class="form-group row {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
            {!!Form::label('FechaInicio', 'Fecha Inicio(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
          <div class="input-group date">
              <input type="text" class="form-control group-date" id="fecha_inicio" name="fecha_inicio"  value="{!! date('Y-m-d', strtotime($editParlamento->fecha_inicio))  !!}">
                <div class="input-group-append">
                  <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
              </div>
          </div>
        </div> 

        <div class="form-group row {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
        {!!Form::label('FechaFin', 'Fecha Fin(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
          <div class="input-group date">
               <input type="text" class="form-control group-date" id="fecha_fin" name="fecha_fin"  value="{!! date('Y-m-d', strtotime($editParlamento->fecha_fin))  !!}">
                <div class="input-group-append">
                  <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
              </div>
          </div>
        </div> <br>

        <div class="form-group row">
          {!! Form::label('Observación', 'Observación(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('observacion', $editParlamento->observacion, ['class' => 'form-control', 'placeholder' => 'Observación']) !!}
          </div>
        </div> 
  
        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! Form::submit('Guardar', ['class' => 'btn btn-success btn-sm','title'=>'Guardar']) !!}
              {!! link_to_route('parlamentarios', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) !!}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{!! Form::close() !!}
@stop
@section('after-scripts-end')
<script type="text/javascript">
$(document).ready(function(){
  $('#pais').on('change', function(){
       var pais = $(this).val();
       console.log(pais);
        if(pais !== ''){
          $.get('https://restcountries.com/v2/alpha/'+pais, function(datos){
             $('#ciudad').val(pais)
         })
        }
  });
});
</script>
@include('includes.scripts.script_fecha.scripts_filtro_fecha')
@endsection