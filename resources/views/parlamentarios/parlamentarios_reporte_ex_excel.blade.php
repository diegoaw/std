<table>
  <tr>
   <th colspan="41" style="text-align: center;"></th>
  </tr>
  <tr>
   <th colspan="41" style="text-align: center;">AGENDA VIAJES PARLAMENTARIOS {{date("Y")}}/{{date("Y")+1}}</th>
  </tr>
</table >
<table>
      <tr>
        <th colspan="4" style="color:#ffff; text-align: center; background-color: #013273;">MINISTERIO</th>
        <th colspan="4" style="color:#ffff; text-align: center; background-color: #013273;">NOMBRE DE LA ACTIVIDAD</th>
        <th colspan="4" style="color:#ffff; text-align: center; background-color: #013273;">LUGAR (PAIS)</th>
        <th colspan="3" style="color:#ffff; text-align: center; background-color: #013273;">FECHA REALIZACIÓN</th>
        <th colspan="4" style="color:#ffff; text-align: center; background-color: #013273;">PARLAMENTARIO/A INVITADO/A</th>
        <th colspan="5" style="color:#ffff; text-align: center; background-color: #013273;">INSTITUCIÓN QUE CONVOCA</th>
        <th colspan="10" style="color:#ffff; text-align: center; background-color: #013273;">FECHA Y PARTICIPACIÓN ESTIMADA DE PARTICIPACIÓN DE PARLAMENTARIAS/OS</th>
        <th colspan="7" style="color:#ffff; text-align: center; background-color: #013273;">OBSERVACIÓN</th>
      </tr>
    @if(count($dataFinal)>0)
      @foreach ($dataFinal as $indice => $tramite)
      <tr>
        <td colspan="4" valign="middle" style="text-align: center;" ><center>{{$tramite->ministerio}}</center></td>
        <td colspan="4" valign="middle" style="text-align: center;" ><center>{{$tramite->nombre_actividad}}</center></td>
        <td colspan="4" valign="middle" style="text-align: center;" ><center>{{$tramite->pais}}</center></td>
        <td colspan="3" valign="middle" style="text-align: center;" ><center>{{$tramite->fecha_realizacion}}</center></td>
        <td colspan="4" valign="middle" style="text-align: center;" ><center>{{$tramite->parlamentario_invitado}}</center></td>
        <td colspan="5" valign="middle" style="text-align: center;" ><center>{{$tramite->institucion_convoca}}</center></td>
        <td colspan="10" valign="middle" style="text-align: center;" ><center>desde {{$tramite->fecha_inicio}} a {{$tramite->fecha_fin}}</center></td>
        <td colspan="7" valign="middle" style="text-align: center;" ><center>{{$tramite->observacion}}</center></td>
      </tr>
      @endforeach
    @endif
</table>