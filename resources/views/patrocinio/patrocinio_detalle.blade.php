
@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-5">
      <h1><i class="fa fa-user"></i> Solicitud de Patrocinio</h1>
    </div>
    <div class="col-sm-7">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-user"></i> Solicitud de Patrocinio</li>
        <li class="breadcrumb-item active">Detalles</li>
      </ol>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <!-- Line chart -->
    <div class="card card-danger card-outline">
      <div class="card-header">
        <h4 class="card-title"><b>Detalles Solicitud de Patrocinio N° {{$data->id}}</b></h4>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <br>
        <h4><b>Datos del Solicitante</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Estado de Solicitud</b></td>
              <td nowrap>{{$data->estado}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Nombre Solicitante</b></td>
              <td nowrap>{{$data->nombre}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Teléfono Solicitante</b></td>
              <td nowrap>{{$data->telefono}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Región Solicitante</b></td>
              <td nowrap>{{$data->region}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Comuna Solicitante</b></td>
              <td nowrap>{{$data->comuna}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Dirección Solicitante</b></td>
              <td>{{$data->direccion}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Email Solicitante</b></td>
              <td nowrap>{{$data->email}}</td>
          </tr>
        </table>
        <br>
        <h4><b>Datos de la Solicitud</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Descripción</b></td>
              <td>{{$data->descripcion}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Objetivos</b></td>
              <td>{{$data->objetivos}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Fines de Lucro</b></td>
              <td nowrap>{{$data->fines_de_lucro}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Libre Acceso</b></td>
              <td nowrap>{{$data->libre_acceso}}</td>
          </tr>
          @if($data->libre_acceso == 'no')
          <tr>
              <td style="width:450px"><b>Si Requiere Pago</b></td>
              <td>{{$data->si_requiere_pago}}</td>
          </tr>
          @endif
          <tr>
              <td style="width:450px"><b>Lugar de Desarrollo</b></td>
              <td>{{$data->lugar_de_desarrollo}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Fecha de Desarrollo</b></td>
              <td nowrap>{{$data->fecha_de_desarrollo}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>¿El solicitante y/o actividad se encuentra postulando a fondos concursables del Instituto Nacional de Deportes de Chile o es beneficiaria de estos fondos?</b></td>
              <td nowrap>{{$data->postulando_a_otros_beneficios}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Identificar la Capacidad de Atraer Demanda</b></td>
              <td>{{$data->capacidad_de_atraer_demanda}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Posicionamiento del PAÍS</b></td>
              <td>{{$data->posicionamiento_del_pais}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Carta, dirigida a Ministro(a) del Deporte</b></td>
              <td>
                @if($data->carta_ministra)
                <a href="{{$data->carta_ministra}}" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                @endif
              </td>
          </tr>
          <tr>
              <td style="width:450px"><b>Tipo de Persona</b></td>
              <td nowrap>{{$data->tipo_de_persona}}</td>
          </tr>
          @if($data->tipo_de_persona == 'natural')
          <tr>
              <td style="width:450px"><b>Fotocopia, por ambos lados, de cédula de identidad vigente del solicitante</b></td>
              <td>
                @if($data->fotocopia_cedula)
                <a href="{{$data->fotocopia_cedula}}" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                @endif
              </td>
          </tr>
          @endif
          @if($data->tipo_de_persona == 'juridica')
          <tr>
              <td style="width:450px"><b>Tipo de Organización</b></td>
              <td nowrap>{{$data->tipo_de_organizacion}}</td>
          </tr>
                @if($data->tipo_de_organizacion == 'Organización Deportiva.')
                <tr>
                    <td style="width:450px"><b>Fotocopia por ambos lados de la cédula nacional de identidad vigente del representante legal de la organización.</b></td>
                    <td>
                      @if($data->{'1_a'})
                      <a href="<?php echo $data->{'1_a'} ?>" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                      @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:450px"><b>Fotocopia simple del RUT de la persona jurídica.</b></td>
                    <td>
                      @if($data->{'2_a'})
                      <a href="<?php echo $data->{"2_a"} ?>" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                      @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:450px"><b>Número de registro en el IND</b></td>
                    <td nowrap><?php echo $data->{"3_a"} ?></td>
                </tr>
                <tr>
                    <td style="width:450px"><b>RUT de la Organización</b></td>
                    <td nowrap><?php echo $data->{"4_a"} ?></td>
                </tr>
                @endif

                @if($data->tipo_de_organizacion == 'Sociedad Civil o Comercial (EIRL, Ltda., etc.).')
                <tr>
                    <td style="width:450px"><b>Fotocopia por ambos lados de la cédula nacional de identidad vigente del representante legal de la organización.</b></td>
                    <td>
                      @if($data->{'1_b'})
                      <a href="<?php echo $data->{'1_b'} ?>" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                      @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:450px"><b>Fotocopia simple del RUT de la persona jurídica.</b></td>
                    <td>
                      @if($data->{'2_b'})
                      <a href="<?php echo $data->{"2_b"} ?>" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                      @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:450px"><b>Copia de la escritura pública de constitución, donde conste personería.</b></td>
                    <td>
                      @if($data->{'3_b'})
                      <a href="<?php echo $data->{'3_b'} ?>" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                      @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:450px"><b>Copia simple de publicación en el Diario Oficial.</b></td>
                    <td>
                      @if($data->{'4_b'})
                      <a href="<?php echo $data->{"4_b"} ?>" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                      @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:450px"><b>Certificado de vigencia de la sociedad o modificación del pacto social con una data no superior a 30 días corridos, otorgado por el Registro de Comercio del Conservador de Bienes Raíces respectivo, o por el Ministerio de Economía, Fomento y Turismo, en su caso.</b></td>
                    <td>
                      @if($data->{'5_b'})
                      <a href="<?php echo $data->{'5_b'} ?>" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                      @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:450px"><b>Copia de escritura pública donde consten modificaciones del pacto social, cuando éstas afecten la representación.</b></td>
                    <td>
                      @if($data->{'6_b'})
                      <a href="<?php echo $data->{"6_b"} ?>" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                      @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:450px"><b>Certificado de vigencia de poderes sociales, con una data no superior a 30 días corridos otorgado por el Registro de Comercio del Conservador de Bienes Raíces, en su caso.</b></td>
                    <td>
                      @if($data->{'7_b'})
                      <a href="<?php echo $data->{"7_b"} ?>" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                      @endif
                    </td>
                </tr>
                @endif

                @if($data->tipo_de_organizacion == 'Sociedad Anónima Abierta o Cerradas.')
                <tr>
                    <td style="width:450px"><b>Fotocopia por ambos lados de la cédula nacional de identidad vigente del representante legal de la organización.</b></td>
                    <td>
                      @if($data->{'1_c'})
                      <a href="<?php echo $data->{'1_c'} ?>" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                      @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:450px"><b>Fotocopia simple del RUT de la persona jurídica.</b></td>
                    <td>
                      @if($data->{'2_c'})
                      <a href="<?php echo $data->{"2_c"} ?>" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                      @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:450px"><b>Copia de la escritura pública de constitución, donde conste personería.</b></td>
                    <td>
                      @if($data->{'3_c'})
                      <a href="<?php echo $data->{'3_c'} ?>" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                      @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:450px"><b>Copia simple de publicación en el Diario Oficial.</b></td>
                    <td>
                      @if($data->{'4_c'})
                      <a href="<?php echo $data->{"4_c"} ?>" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                      @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:450px"><b>Certificado de vigencia de la sociedad y/o de sus modificaciones de pacto social con data no superior a 30 días corridos, otorgado por el Registro de Comercio del Conservador de Bienes Raíces respectivo.</b></td>
                    <td>
                      @if($data->{'5_c'})
                      <a href="<?php echo $data->{'5_c'} ?>" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                      @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:450px"><b>Copia de acta de directorio reducida a escritura pública donde conste la designación de los representantes legales y la extensión de sus poderes.</b></td>
                    <td>
                      @if($data->{'6_c'})
                      <a href="<?php echo $data->{"6_c"} ?>" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                      @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:450px"><b>Certificado de vigencia de poderes sociales con una data no mayor a 30 días corridos, otorgado por el Registro de Comercio del Conservador de Bienes Raíces respectivo.</b></td>
                    <td>
                      @if($data->{'7_c'})
                      <a href="<?php echo $data->{"7_c"} ?>" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                      @endif
                    </td>
                </tr>
                @endif

                @if($data->tipo_de_organizacion == 'Asociación Gremial.')
                <tr>
                    <td style="width:450px"><b>Fotocopia por ambos lados de la cédula nacional de identidad vigente del representante legal de la organización.</b></td>
                    <td>
                      @if($data->{'1_d'})
                      <a href="<?php echo $data->{'1_d'} ?>" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                      @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:450px"><b>Fotocopia simple del RUT de la persona jurídica.</b></td>
                    <td>
                      @if($data->{'2_d'})
                      <a href="<?php echo $data->{"2_d"} ?>" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                      @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:450px"><b>Copia de la escritura pública de acta constitutiva donde consten estatutos y directiva.</b></td>
                    <td>
                      @if($data->{'3_d'})
                      <a href="<?php echo $data->{'3_d'} ?>" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                      @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:450px"><b>Certificado de vigencia de la asociación gremial, que acredite la inscripción, el mandato del directorio y el número de socios, otorgado por el Ministerio de Economía.</b></td>
                    <td>
                      @if($data->{'4_d'})
                      <a href="<?php echo $data->{"4_d"} ?>" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                      @endif
                    </td>
                </tr>
                @endif

                @if($data->tipo_de_organizacion == 'Corporación o Fundación Sin Fines de Lucro.')
                <tr>
                    <td style="width:450px"><b>Copia de la escritura pública del acta de constitución y de los estatutos aprobados.</b></td>
                    <td>
                      @if($data->{'1_e'})
                      <a href="<?php echo $data->{'1_e'} ?>" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                      @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:450px"><b>Certificado de vigencia y directorio emitidos por el Servicio de Registro Civil e Identificación, con una data no superior a 30 días corridos.</b></td>
                    <td>
                      @if($data->{'2_e'})
                      <a href="<?php echo $data->{"2_d"} ?>" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                      @endif
                    </td>
                </tr>
                @endif

                @if($data->tipo_de_organizacion == 'Organización territoriales, comunitarias y funcionales.')
                <tr>
                    <td style="width:450px"><b>Fotocopia por ambos lados de la cédula nacional de identidad vigente del representante legal de la organización.</b></td>
                    <td>
                      @if($data->{'1_f'})
                      <a href="<?php echo $data->{'1_f'} ?>" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                      @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:450px"><b>Fotocopia simple del RUT de la persona jurídica.</b></td>
                    <td>
                      @if($data->{'2_f'})
                      <a href="<?php echo $data->{"2_f"} ?>" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                      @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:450px"><b>Copia simple de acta de asamblea y estatutos aprobados.</b></td>
                    <td>
                      @if($data->{'3_f'})
                      <a href="<?php echo $data->{'3_f'} ?>" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                      @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:450px"><b>Certificados de vigencia y directorio emitidos por el Servicio de Registro Civil e Identificación, con una data no superior a 30 días corridos.</b></td>
                    <td>
                      @if($data->{'4_f'})
                      <a href="<?php echo $data->{"4_f"} ?>" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                      @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:450px"><b>Certificados emitidos por la Municipalidad en que dicha organización se encuentra registrada (Debe ingresar comprimido para adjuntar el o los certificado(s) ).</b></td>
                    <td>
                      @if($data->{'5_f'})
                      <a href="<?php echo $data->{'5_f'} ?>" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                      @endif
                    </td>
                </tr>
                @endif

                @if($data->tipo_de_organizacion == 'Sin Personalidad.')
                <tr>
                    <td style="width:450px"><b>Fotocopia por ambos lados de la cédula nacional de identidad vigente del representante legal de la organización.</b></td>
                    <td>
                      @if($data->{'1_g'})
                      <a href="<?php echo $data->{'1_g'} ?>" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                      @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:450px"><b>Fotocopia simple del RUT de la persona jurídica.</b></td>
                    <td>
                      @if($data->{'2_g'})
                      <a href="<?php echo $data->{"2_g"} ?>" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                      @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:450px"><b>Fotocopia legalizada de la cédula nacional de identidad vigente de cada uno de los miembros de la agrupación, por ambos lados (Debe ingresar comprimido para adjuntar todas las fotocopias).</b></td>
                    <td>
                      @if($data->{'3_g'})
                      <a href="<?php echo $data->{"3_g"} ?>" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                      @endif
                    </td>
                </tr>
                @endif
          @endif
          <tr>
              <td style="width:450px"><b>¿Su Solicitud Refiere a un Evento?</b></td>
              <td nowrap>{{$data->evento}}</td>
          </tr>
          
          <tr>
              <td style="width:450px"><b>Ficha de Solicitud de Patrocinio por Evento.</b></td>
              <td>
                @if($data->ficha_de_solicitud)
                <a href="{{$data->ficha_de_solicitud}}" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                @endif
              </td>
          </tr>
          
        </table>
      </div>

        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! link_to_route('patrocinio', 'Volver', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
            </div>
          </div>
        </div>
    </div>
  </div>
</div>


@endsection
@section('before-scripts-end')
@stop
