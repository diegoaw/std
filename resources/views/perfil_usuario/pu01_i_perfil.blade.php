{{-- @Nombre del programa: Vista Principal perfil de usuarios/Mi Perfil --}}
{{-- @Funcion: --}}
{{-- @Autor: btc --}}
{{-- @Fecha Creacion: 01/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 01/01/2019 --}}
{{-- @Modificado por:  --}}

@extends('layouts.master')

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-5">
      <h1><i class="fa fa-user"></i> Mi perfil</h1>
    </div>
    <div class="col-sm-7">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-user"></i> Perfil de usuarios</li>
        <li class="breadcrumb-item active">Mi perfil</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <!-- Line chart -->
    <div class="card card-danger card-outline">
      <div class="card-header">
        <h3 class="card-title">Perfil Active Directory</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <table id="example1" class="table table-bordered table-striped table-hover table-sm">
          <tr>
            <th>Nombre</th>
            <td>{{ $user->name }}</td>
          </tr>
          <tr>
            <th>Usuario</th>
            <td>{{ $user->email }}</td>
          </tr>
            <tr>
            <th>Departamento</th>
            <td>{{ $user->department }}</td>
          </tr>

          <tr>
            <th>Primer inicio de sesión</th>
            <td>{{ $user->created_at->format('d-m-Y h:i:s') }}</td>
          </tr>
          <tr>
            <th>Último inicio de sesión</th>
            <td>{{ $user->updated_at->format('d-m-Y h:i:s') }}</td>
          </tr>
          <tr>
            <th>Acciones</th>
            <td>
          <?php /*  {!! link_to_route('PerfilUsuario.changePasword', 'Cambiar la contraseña', ['id' => $user->id], ['class' => 'btn btn-primary btn-xs']) !!} */ ?>

            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <!-- Line chart -->
    <div class="card card-danger card-outline">
      <div class="card-header">
        <h3 class="card-title">Perfil simple.mindep.cl</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <table id="example1" class="table table-bordered table-striped table-hover table-sm">
          <tr>
            <th>Nombre</th>
            <td>{{ $userSimple->nombres }}  {{ $userSimple->apellido_paterno }} {{ $userSimple->apellido_materno }}</td>
          </tr>
          <tr>
            <th>Usuario</th>
            <td>{{ $userSimple->usuario }}</td>
          </tr>
            <tr>
            <th>Departamento</th>
            <td></td>
          </tr>

          <tr>
            <th>Creado</th>
            <td>{{ $userSimple->created_at->format('d-m-Y h:i:s') }}</td>
          </tr>
          <tr>
            <th>actualizado</th>
            <td>{{ $userSimple->updated_at->format('d-m-Y h:i:s') }}</td>
          </tr>
          <tr>
            <th>Acciones</th>
            <td>
           {!! link_to_route('PerfilUsuario.changePasword', 'Cambiar la contraseña', ['id' => $userSimple->id], ['class' => 'btn btn-primary btn-xs']) !!}

            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
