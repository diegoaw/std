@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-5">
      <h1><i class="fa fa-comments"></i> Consulta sobre Protocolo Contra el Abuso Sexual, Acoso Sexual Maltrato y Discriminación en el Deporte</h1>
    </div>
    <div class="col-sm-7">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-user"></i> Consulta sobre Protocolo Contra el Abuso Sexual, Acoso Sexual Maltrato y Discriminación en el Deporte</li>
        <li class="breadcrumb-item active">Detalles</li>
      </ol>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <!-- Line chart -->
    <div class="card card-danger card-outline">
      <div class="card-header">
        <h4 class="card-title"><b>Detalles Consulta sobre Protocolo Contra el Abuso Sexual, Acoso Sexual Maltrato y Discriminación en el Deporte</b></h4>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <br>
        <h4><b>Datos de la Consulta N° {{$data->id}}</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Nombre Completo</b></td>
              <td nowrap>{{$data->nombrecompleto}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Email</b></td>
              <td nowrap>{{$data->email}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Teléfono</b></td>
              <td nowrap>{{$data->telefono}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Mensaje</b></td>
              <td>{{$data->mensaje}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Archivo a Adjunto</b></td>
              <td>
                @if($data->archivo_adjuntar)
                <a href="{{$data->archivo_adjuntar}}" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                @endif
              </td>
          </tr>
        </table>
        <br>
        <h4><b>Respuesta a la Consulta N° {{$data->id}}</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Mensaje de Respuesta</b></td>
              <td>{{$data->mensaje_respuesta}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Archivo de Respuesta Adjunto</b></td>
              <td>
                @if($data->adjuntar_archivo)
                <a href="{{$data->adjuntar_archivo}}" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                @endif
              </td>
          </tr>
        </table>
      </div>

        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! link_to_route('protocolo', 'Volver', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
            </div>
          </div>
        </div>
    </div>
  </div>
</div>


@endsection
@section('before-scripts-end')
@stop
