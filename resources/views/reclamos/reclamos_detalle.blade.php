@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-5">
      <h1><i class="fa fa-comments"></i> Reclamos de Bienes o Servicios</h1>
    </div>
    <div class="col-sm-7">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-user"></i> Reclamos de Bienes o Servicios</li>
        <li class="breadcrumb-item active">Detalles</li>
      </ol>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <!-- Line chart -->
    <div class="card card-danger card-outline">
      <div class="card-header">
        <h4 class="card-title"><b>Detalles Reclamo de Bienes o Servicios</b></h4>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <br>
        <h4><b>Datos del Reclamo N° {{$data->id}}</b></h4>
        <br>
        <table id="example1" class="table table-striped">
        <tr>
              <td style="width:450px"><b>Fecha Inicio</b></td>
              <td nowrap>{{$data->fecha_inicio}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Servicio/Bien</b></td>
              <td nowrap>{{$data->servicio_bien}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Nombre</b></td>
              <td nowrap>{{$data->nombre}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Región</b></td>
              <td nowrap>{{$data->region}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Comuna</b></td>
              <td nowrap>{{$data->comuna}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Número de Contacto</b></td>
              <td nowrap>{{$data->numero}}</td>
          </tr
          <tr>
              <td style="width:450px"><b>Correo Electrónico</b></td>
              <td>{{$data->correo_electronico}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Mensaje</b></td>
              <td>{{$data->mensaje}}</td>
          </tr>
        </table>
      </div>

        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! link_to_route('reclamos', 'Volver', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
            </div>
          </div>
        </div>
    </div>
  </div>
</div>


@endsection
@section('before-scripts-end')
@stop
