@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-search"></i> Consulta de Estado de Trámites de Solicitud de Reconocimiento</h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <div>
        <a class="btn btn-success pull-right" href="https://reconocimiento.mindep.cl/" target="_blank">reconocimiento.mindep.cl</a>
        </div>
        <h5 class="card-title">Filtros </h5>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">
            {!! Form::open(['route' => 'reconocimiento', 'method' => 'get' , 'class'=>"form-horizontal"]) !!}

              <div class="form-row">

                <div class="col-sm-0">
                  <label for="nRegistro" class=" col-form-label">N° Trámite: </label>
                  <input type="text" class="form-control" id="nRegistro" name="nRegistro" value="{{ $idTramiteFiltro }}" placeholder="N° Trámite">
                </div>

              </div>
              <!--<button type="submit" class="btn btn-primary pull-right">Buscar</button>-->
              <input type="submit" class="btn btn-primary pull-right" name="Buscar" value="Buscar">

            {!! Form::close() !!}


          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header card-danger card-outline">
          <h5 class="card-title">Trámites de Solicitud de Reconocimiento</h5>

        </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
            <table class="table table-striped table-sm"  >
              <thead >
                  <tr style="background-color: #cd0b27;">
                      <th nowrap style="color:white">Tramite ID</th>
                      <th nowrap style="color:white">Estado</th>
                      <th nowrap style="color:white">Fecha de Inicio</th>
                      <th nowrap style="color:white">Etapa</th>
                      <th nowrap style="color:white">Fecha de Actualización</th>
                      <th nowrap style="color:white">Datos Solicitante</th>
                  </tr>
              </thead>
              <tbody class="panel">
              @if(count($tramitesD)>0)
                @foreach ($tramitesD as $indice => $tramites)
                    <tr>
                         <td nowrap>{{$tramites->numero_tramite}}</td>
                         @if($tramites->pendiente == 1)
                         <td nowrap style="background-color: #cd0b27; color: white;">Pendiente</td>
                         @else
                         <td nowrap>Completado</td>
                         @endif
                         <td nowrap>{{$tramites->fecha_inicio}}</td>
                         <td>{{$tramites->etapa}}</td>
                         <td nowrap>{{$tramites->fecha_ultima_modificacion}}</td>
                         <td><center>
                          <a class="btn accion btn-info accion" href="{{route('reconocimientoDetalle',$tramites->numero_tramite)}}"><i class="fa fa-eye"></i></a>
                         </center></td>
                    </tr>
                @endforeach
              @endif
              </tbody>
            </table>
          </div>



    </div>
  </div>
</div>

@endsection
@section('before-scripts-end')
@stop
