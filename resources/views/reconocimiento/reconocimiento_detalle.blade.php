
@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-5">
      <h1><i class="fa fa-user"></i> Solicitud de Reconocimiento</h1>
    </div>
    <div class="col-sm-7">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-user"></i> Solicitud de Reconocimiento</li>
        <li class="breadcrumb-item active">Detalles</li>
      </ol>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <!-- Line chart -->
    <div class="card card-danger card-outline">
      <div class="card-header">
        <h3 class="card-title">Detalles Solicitud de Reconocimiento</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <br>
        <h4><b>Trámite N° {{$data->id}}</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Tipo de Persona</b></td>
              <td nowrap>{{$data->tipo_de_persona}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Fecha de Solicitud</b></td>
              <td nowrap>{{$data->fecha_solicitud}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Estado de Solicitud</b></td>
              <td nowrap>{{$data->estado}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>RUT Solicitante</b></td>
              <td nowrap>{{$data->rut_solicitante}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Nombres Solicitante</b></td>
              <td nowrap>{{$data->nombres_solicitante}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Apellidos Solicitante</b></td>
              <td nowrap>{{$data->apellidos_solicitante}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Email Solicitante</b></td>
              <td nowrap>{{$data->email_solicitante}}</td>
          </tr>
          @if($data->tipo_de_persona == 'juridica')
          <tr>
              <td style="width:450px"><b>Personalidad Jurídica</b></td>
              <td nowrap>{{$data->personalidad_juridica}}</td>
          </tr>
          @endif
          <tr>
              <td style="width:450px"><b>Domicilio</b></td>
              <td nowrap>{{$data->domicilio}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Región</b></td>
              <td nowrap>{{$data->region}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Comuna</b></td>
              <td nowrap>{{$data->comuna}}</td>
          </tr>
          @if($data->tipo_de_persona == 'juridica')
          <tr>
              <td style="width:450px"><b>Misión</b></td>
              <td nowrap>{{$data->mision}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Visión</b></td>
              <td nowrap>{{$data->vision}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Objetivos</b></td>
              <td nowrap>{{$data->objetivos}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Rut (Representante Legal)</b></td>
              <td nowrap>{{$data->rut_del_representante_legal}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Nombres y Apellidos (Representante Legal)</b></td>
              <td nowrap>{{$data->nombre_del_representante_legal}}</td>
          </tr>
          @endif
          <tr>
              <td style="width:450px"><b>Carta Formal dirigida a la Ministra del Deporte con la información mencionada al inicio de la solicitud</b></td>
              <td >
              @if($data->carta)
              <a href="{{$data->carta}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
              </td>
          </tr>
          <tr>
              <td style="width:450px"><b>Resto de los documentos desde la "a" a la "j" en un solo archivo</b></td>
              <td >
              @if($data->documentos)
              <a href="{{$data->documentos}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
              </td>
          </tr>    
        </table>
      </div>

        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
            <a class="btn accion btn-success accion" title="Enviar Aviso" href="{{route('reconocimientonoti', $data)}}"><i class="fa fa-envelope-o"></i></a>
              <a data-toggle="tooltip" data-placement="left" title="Eliminar" class="btn btn-sm btn-danger" href="#" OnClick="Eliminar('{{$data->id}}')"><i class="fa fa-trash"></i></a>
              
              {!! link_to_route('reconocimiento', 'Volver', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
            </div>
          </div>
        </div>
    </div>
  </div>
</div>


@endsection
@section('before-scripts-end')
@include('includes.scripts.tramites_reconocimiento')

@stop
