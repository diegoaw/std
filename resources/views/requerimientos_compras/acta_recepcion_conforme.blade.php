@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-search"></i> Consulta de Estado de Trámites de Actas de Recepción Conforme</h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        
        <h5 class="card-title">Filtros </h5>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">
            {!! Form::open(['route' => 'acta', 'method' => 'get' , 'class'=>"form-horizontal"]) !!}

              <div class="form-row">

                <div class="col-sm-0">
                  <label for="nRegistro" class=" col-form-label">N° Trámite: </label>
                  <input type="text" class="form-control" id="nRegistro" name="nRegistro" value="{{ $idTramiteFiltro }}" placeholder="N° Trámite">
                </div>
                
                <div class="col-sm-0">
                  <label for="titulo" class=" col-form-label">Título Acta: </label>
                  <input type="text" class="form-control" id="titulo" name="titulo" value="{{ $tituloFiltro }}" placeholder="Título Acta">
                </div>
              </div>
              <!--<button type="submit" class="btn btn-primary pull-right">Buscar</button>-->
              <input type="submit" class="btn btn-primary pull-right" name="Buscar" value="Buscar">

            {!! Form::close() !!}


          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header card-danger card-outline">
          <h5 class="card-title">Trámites de Actas de Recepción Conforme</h5>

        </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
            <table class="table table-striped table-sm"  >
              <thead >
                  <tr style="background-color: #cd0b27;">
                      <th nowrap style="color:white">Trámite ID</th>
                      <th nowrap style="color:white">Estado</th>
                      <th nowrap style="color:white">Fecha de Inicio</th>
                      <th nowrap style="color:white">Título Acta</th>
                      <th nowrap style="color:white">Etapa</th>
                      <th nowrap style="color:white">Fecha de Actualización</th>
                      <th nowrap style="color:white">Datos Actas</th>
                  </tr>
              </thead>
              <tbody class="panel">
              @if(count($tramitesD)>0)
                @foreach ($tramitesD as $indice => $tramites)
                    <tr>
                         <td nowrap>{{$tramites->numero_tramite}}</td>
                         @if($tramites->pendiente == 1)
                         <td nowrap style="background-color: #cd0b27; color: white;">Pendiente</td>
                         @else
                         <td nowrap>Completado</td>
                         @endif
                         <td nowrap>{{$tramites->fecha_inicio}}</td>
                         <td>{{$tramites->tituloActa}}</td>
                         <td>{{$tramites->etapa}}</td>
                         <td nowrap>{{$tramites->fecha_ultima_modificacion}}</td>
                         <td><center>
                          <a class="btn accion btn-info accion" href="{{route('actaDetalle',$tramites->numero_tramite)}}"><i class="fa fa-eye"></i></a>
                          @if (auth()->user()->hasPermission(['99-06']))
                          <a data-toggle="tooltip" data-placement="left" title="Eliminar" class="btn btn-sm btn-danger" href="#" OnClick="Eliminar('{{$tramites->numero_tramite}}')"><i class="fa fa-trash"></i></a>
                          @endif
                         </center></td>
                    </tr>
                @endforeach
              @endif
              </tbody>
            </table>
          </div>



    </div>
  </div>
</div>

@endsection
@section('before-scripts-end')
@include('includes.scripts.actas')
@stop
