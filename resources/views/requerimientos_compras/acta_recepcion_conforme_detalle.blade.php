@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-5">
      <h1><i class="fa fa-file"></i> Acta de Recepción Conforme</h1>
    </div>
    <div class="col-sm-7">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-file"></i> Acta de Recepción Conforme</li>
        <li class="breadcrumb-item active">Detalles</li>
      </ol>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <!-- Line chart -->
    <div class="card card-danger card-outline">
      <div class="card-header">
        <h3 class="card-title">Detalles Acta de Recepción Conforme</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <br>
        <h4><b>Trámite N° {{$data->id}}</b></h4>
        <br>
        <br>
        <h4><b>Datos para llenar Recepción Conforme</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>¿Es un Acta de Recepción Conforme para un Requerimiento de Compra en Simple?</b></td>
              <td nowrap>{{$data->acta_recepcion}}</td>
          </tr>
          @if($data->acta_recepcion == 'si')
          <tr>
              <td style="width:450px"><b>N° de Trámite:</b></td>
              <td nowrap>{{$data->tramite}}  <a data-toggle="tooltip" data-placement="top" target_blank title="Ver Detalles del Requerimiento" class="btn accion btn-warning accion" href="{{route('requecomprasshow', $data->tramite)}}"><i class="fa fa-eye"></i></a></td>
          </tr>
          @endif
          <tr>
              <td style="width:450px"><b>Titulo de la Compra:</b></td>
              <td nowrap>{{$data->titulocompra}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Certificado de Disponibilidad Presupuestaria:</b></td>
              <td nowrap><a href="{{$data->adjuntar_certificado}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a></td>
          </tr>
          <tr>
              <td style="width:450px"><b>Orden de Compra:</b></td>
              <td nowrap><a href="{{$data->orden_de_compra}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a></td>
          </tr>
          <tr>
              <td style="width:450px"><b>Factura:</b></td>
              <td nowrap><a href="{{$data->adjuntar_factura}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a></td>
          </tr>
		  <tr>
              <td style="width:450px"><b>Dependencia:</b></td>
              <td nowrap>{{$data->api_area}}</td>
          </tr>
		  <tr>
              <td style="width:450px"><b>Observaciones:</b></td>
              <td>{{$data->observaciones}}</td>
          </tr>
		  </table>

          <br>
		  <h4><b>Datos de Recepción Conforme</b></h4>
		  <br>  
		  <table id="example1" class="table table-striped">
		  <tr>
              <td style="width:450px"><b>Nombre de Proveedor:</b></td>
              <td nowrap>{{$data->nombre_de_proveedor}}</td>
          </tr><tr>
              <td style="width:450px"><b>Número de Orcom: (Orden de Compra)</b></td>
              <td nowrap>{{$data->numero_de_orcom}}</td>
          </tr>
		  <tr>
              <td style="width:450px"><b>Fecha de Orden de Compra:</b></td>
              <td nowrap>{{$data->fecha_orden_compra}}</td>
          </tr>
		  <tr>
              <td style="width:450px"><b>Número de Documento: (Factura)</b></td>
              <td nowrap>{{$data->n_factura}}</td>
          </tr>
		  <tr>
              <td style="width:450px"><b>Fecha de Documento:(Factura)</b></td>
              <td nowrap>{{$data->fecha_factura}}</td>
          </tr>
		  <tr>
              <td style="width:450px"><b>Monto de Documento: (Factura)</b></td>
              <td nowrap>{{$data->monto_factura}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Listado de Requerimiento</b></td>
              <td>
                <table class="table table-striped table-sm"  >
                    <thead >
                        <tr style="background-color: #013273;">
  					    <th  style="color:white"><center>Cantidad</center></th>
                          <th  style="color:white"><center>Detalle</center></th>
                        </tr>
                    </thead>
                    <tbody class="panel">
                      @if($data->listado_requerimiento)
                      @foreach ($data->listado_requerimiento as $indice => $tramite)
                    <tr>
                       <td ><center>{{$tramite[0]}}</center></td>
  					   <td ><center>{{$tramite[1]}}</center></td>
                    </tr>
                     @endforeach
                     @endif
                    <tbody>
                 </table>
              </td>
          </tr>
          <tr>
              <td style="width:450px"><b>Como requirente del producto y/o servicio, declaro recibir en conformidad a lo solicitado:</b></td>
              <td nowrap>{{$data->declaracion}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Dependencia:</b></td>
              <td nowrap>{{$data->dependencia}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>¿Quién Suscribe el Acta de Recepción Conforme es Subrogante?</b></td>
              <td nowrap>{{$data->subrogante}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Suscribe Acta de Recepción Conforme:</b></td>
              <td nowrap>
              @if($data->firma)
              {{$data->nombreGrupo}}
              @endif
              @if($data->firma_sub)
              {{$data->nombreGrupoSub}}
              @endif
              </td>
              <td >
              
              
            </td>
          </tr>
          <tr>
              <td style="width:450px"><b>Archivos Adjuntos:</b></td>
              <td >
              @if($data->archivo_a)
              <a href="{{$data->archivo_a}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
              @if($data->archivo_b)
              <a href="{{$data->archivo_b}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
              @if($data->archivo_c)
              <a href="{{$data->archivo_c}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
              @if($data->archivo_d)
              <a href="{{$data->archivo_d}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
              @if($data->archivo_e)
              <a href="{{$data->archivo_e}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
              </td>
          </tr>
          <tr>
              <td style="width:450px"><b>Descargar Acta de Recepción Conforme:</b></td>
              <td >
              @if($data->act_rc_descarga)
              <a href="{{$data->act_rc_descarga}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
              </td>
          </tr>
        </table>
      </div>

        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! link_to_route('acta', 'Volver', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
            </div>
          </div>
        </div>
    </div>
  </div>
</div>


@endsection
@section('before-scripts-end')
@stop
