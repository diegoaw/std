
@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-pie-chart"></i> Dashboard Requerimientos de Compras</h1><br> 
    </div>
    
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-danger card-outline">
      <div class="card-header ">
        <h5 class="card-title">Requerimientos de Compras</h5>        
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">

                
        <div class="row">
         

          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-success elevation-1"><i class="fa fa-thumbs-o-up"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Requerimientos Aprobados</span>
                <span class="info-box-number"><h4>{{ $cantidadAprobados }}</h4></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->


          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-danger elevation-1"><i class="fa fa-thumbs-o-down"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Requerimientos Rechazados</span>
                <span class="info-box-number"><h4>{{ $cantidadRechazados }}</h4></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-warning elevation-1"><i class="fa fa-hourglass-half"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Requerimientos en Trámite</span>
                <span class="info-box-number"><h4>{{ $cantidadenTramite }}</h4></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          
          
        </div>


          </div>  
        </div> 
      </div> 
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title">Requerimientos de Compras por Unidades Requirentes</h5>        
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">
            <div class="row">
            @foreach ($divisiones as $key => $value)
             
              <div class="col-md-3 col-sm-6 col-12">
                <div class="info-box bg-light">
                  <span class="info-box-icon"><i class="fa fa-users"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">{{$value->nombre}}</span>
                    <span class="info-box-number">Pendientes: @if($value->cantidadTramitesPendientes) <a href="https://std.mindep.cl/RequerimientosCompras/Filtros?id={{$value->idsTramitesPendientes}}" target="_blank" ><b style="color:GoldenRod;">{{$value->cantidadTramitesPendientes}} </b></a>@else 0 @endif</span>
                    <span class="info-box-number">Rechazados: @if($value->cantidadTramitesRechazados) <a href="https://std.mindep.cl/RequerimientosCompras/Filtros?id={{$value->idsTramitesRechazados}}" target="_blank" ><b style="color:Red;">{{$value->cantidadTramitesRechazados}} </b></a>@else 0 @endif</span>
                    <span class="info-box-number">Aprobados: @if($value->cantidadTramitesAprobado)    <a href="https://std.mindep.cl/RequerimientosCompras/Filtros?id={{$value->idsTramitesAprobado}}" target="_blank" ><b style="color:Green;">{{$value->cantidadTramitesAprobado}}   </b></a>@else 0 @endif</span> 
                    <!-- <span class="info-box-number">Pendientes: @if($value->cantidadTramitesPendientes) <a href="http://10.0.88.47/gestordocumentalcomplemento/public/RequerimientosCompras/Filtros?id={{$value->idsTramitesPendientes}}" target="_blank" ><b style="color:GoldenRod;">{{$value->cantidadTramitesPendientes}} </b></a>@else 0 @endif</span>
                    <span class="info-box-number">Rechazados: @if($value->cantidadTramitesRechazados) <a href="http://10.0.88.47/gestordocumentalcomplemento/public/RequerimientosCompras/Filtros?id={{$value->idsTramitesRechazados}}" target="_blank" ><b style="color:Red;">{{$value->cantidadTramitesRechazados}} </b></a>@else 0 @endif</span>
                    <span class="info-box-number">Aprobados: @if($value->cantidadTramitesAprobado)    <a href="http://10.0.88.47/gestordocumentalcomplemento/public/RequerimientosCompras/Filtros?id={{$value->idsTramitesAprobado}}" target="_blank" ><b style="color:Green;">{{$value->cantidadTramitesAprobado}}   </b></a>@else 0 @endif</span>-->
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
              <!-- /.col -->
                 
            @endforeach
            </div>
          </div>  
        </div> 
      </div> 
    </div>
  </div>
</div>

    

   


@endsection
@section('before-scripts-end')
@stop
