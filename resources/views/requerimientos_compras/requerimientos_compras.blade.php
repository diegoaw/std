
@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-file-text"></i> Requerimientos de Compras</h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title">Filtros de Requerimientos de Compras</h5>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">

                {!! Form::open(['route' => 'requecomprasFiltros', 'method' => 'get' , 'class'=>"form-horizontal"]) !!}


                  <div class="form-row">

                    <div class="col-sm-0">
                      <label for="id" class=" col-form-label">N° Trámite: </label>
                      <input type="text" class="form-control" id="id" name="id" value="{{ $vistaId }}" placeholder="N° Trámite">
                    </div>

                    <div class="col-sm-0">
                      <label for="titulo" class=" col-form-label">Título de la Compra: </label>
                      <input type="text" class="form-control" id="titulo" name="titulo" value="{{ $tituloC }}" placeholder="Título de la Compra">
                    </div>

                    <div class="col-sm-3">
                      <label for="division" class=" col-form-label">Estado Requerimiento: </label>
                      {!! Form::select('estado',$selectEstado,$estadoView,['id'=>'estado','name'=>'estado', 'class'=>'form-control']) !!}
                    </div>

                    <div class="col-sm-0">
                      <label for="asignado" class=" col-form-label">Asignado A: </label>
                      <input type="text" class="form-control" id="asignado" name="asignado" value="{{ $asignadoA }}" placeholder="Asignado A">
                    </div>

                    <div class="col-sm-3">
                      <label for="division" class=" col-form-label">Unidad Requirente: </label>
                      {!! Form::select('division',$selectD,$divisionView,['id'=>'division','name'=>'division', 'class'=>'form-control']) !!}
                    </div>

                  </div>

                  <button type="submit" class="btn btn-primary pull-right">Buscar</button>

                {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


@if(count($dataFinal)>0)
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">Listado de Requerimientos de Compras</h5>

          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th  style="color:white"><center>Fecha Requerimiento</center></th>
                        <th  nowrap style="color:white"><center>N° Trámite</center></th>
                        <th  style="color:white"><center>Estado</center></th>
                        <th  style="color:white"><center>Título de la Compra</center></th>
                        <th  style="color:white"><center>Etapa</center></th>
                        <th  style="color:white"><center>Asignado a</center></th>
                        <th  nowrap style="color:white"><center>Fecha Etapa</center></th>
                        <th  style="color:white"><center>Días Transcurridos</center></th>
                        <th  style="color:white"><center>Unidad Requirente</center></th>
                        <th  style="color:white"><center>Con Cargo A</center></th>
                        <th  style="color:white"><center>Pasaje Aéreo</center></th>
                        <th  style="color:white"><center>Monto Estimado</center></th>
                        <th  style="color:white"><center>Ver Productos</center></th>
                        <th nowrap style="color:white"><center>Acciones</center></th>
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($dataFinal)>0)
                         @foreach ($dataFinal as $indice => $tramite)
                          <tr>
                           <td ><center>{{$tramite->fecha_requerimiento}}</center></td>
                           <td ><center>{{$tramite->id}}</center></td>
                           @if($tramite->estatusVista == 'Rechazado')
                           <td style="color:DarkRed;"><center><b>{{$tramite->estatusVista}}</b></center></td>
                          @endif
                          @if($tramite->estatusVista == 'Cancelado')
                           <td style="color:DarkRed;"><center><b>{{$tramite->estatusVista}}</b></center></td>
                          @endif
                          @if($tramite->estatusVista == 'Completado')
                           <td style="color:DarkGreen;"><center><b>{{$tramite->estatusVista}}</b></center></td>
                          @endif
                          @if($tramite->estatusVista == 'En Trámite')
                           <td style="color:DarkBlue;"><center><b>{{$tramite->estatusVista}}</b></center></td>
                          @endif
                           <td ><center>{{$tramite->titulo_compra}}</center></td>
                           <td ><center>{{$tramite->etapa_actual}}</center></td>
                           <td ><center>{{$tramite->usuario_asignado_etapa}}</center></td>
                           <td ><center>{{$tramite->fecha_modificacion}}</center></td>
                           <td ><center>{{$tramite->tiempo_transcurrido}} días</center></td>
                           <td ><center>{{$tramite->unidad_requirente}}</center></td>
                           <td ><center>{{$tramite->con_cargo_a}}</center></td>
                           <td ><center>{{$tramite->pasaje_aereo}}</center></td>
                           <td ><center>{{$tramite->monto}}</center></td>
                           <td ><center>
                          @if(is_array($tramite->listado_requerimiento))
                            <button type="button" class="btn btn-primary" data-toggle="collapse" {!! "data-target='#demo".$indice."'" !!}   {!!"data-parent='#myTable".$tramite->id."'" !!}
                           ><i class="fa fa-eye"></i></button>
                          @endif
                           </center></td>
                           <td >
                             <center>
                              <a data-toggle="tooltip" data-placement="top" title="Ver detalles" class="btn accion btn-info accion" href="{{route('requecomprasshow',$tramite->id)}}"><i class="fa fa-eye"></i></a>
                              @if (auth()->user()->hasPermission(['99-03']))
                              <a data-toggle="tooltip" data-placement="left" title="Eliminar" class="btn btn-sm btn-danger" href="#" OnClick="Eliminar('{{$tramite->id}}')"><i class="fa fa-trash"></i></a>
                              @endif
                          </center>
                           </td>

                          </tr>
                        <tr  {!! "id='demo".$indice."'"  !!} class="collapse">
                        <td colspan="10" class="hiddenRow">
                          <table class="table table-striped table-sm" {!!  "id='myTable".$tramite->id."'" !!}  >
                            <thead class="thead-primary">
                              <tr style="background-color: #fddadf;">
                              <tr style="background-color: #cd0b27;">
                              <th style="color:white">Cantidad</th>
                              <th style="color:white">Producto</th>
                              <th style="color:white">Convenio de Marco</th>
                              </tr>

                              </tr>
                            </thead>
                          <tbody>
                            @if(is_array($tramite->listado_requerimiento))
                             @foreach ($tramite->listado_requerimiento as $indice1 => $registro2)
                            <tr>
                            <tr>
                            <td >{{$registro2[0]}}</td>
                            <td >{{$registro2[1]}}</td>
                            <td >{{$registro2[2]}}</td>
                            </tr>
                            @endforeach
                           @endif
                          </tbody>
                        </table>
                      </td>
                    </tr>
                          @endforeach
                        @endif
                </tbody>
            </table>
          <br>

          <div class="card-footer">
              <div class="card-tools text-center">
                <a href="{{url('RequerimientosCompras/Reporte/Compras',
                  array(
                    'tiporeporte' => 'XLS'
                  )
                )}}" class="btn btn-primary btn-success" role="button">XLS</a>
              </div>
            </div>
        </div>
      </div>
    </div>
</div>
<br>
@else
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">Listado de compras</h5>

          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <center>  No se encontraron resultados.</center>
          <br>
        </div>
        <br>
      

      </div>
    </div>
</div>
@endif






@endsection
@section('before-scripts-end')
@include('includes.scripts.tramites_compras')
@stop
