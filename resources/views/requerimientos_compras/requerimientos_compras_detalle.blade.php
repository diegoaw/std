
@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-5">
      <h1><i class="fa fa-user"></i><b> Requerimiento de Compra</b></h1>
    </div>
    <div class="col-sm-7">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-user"></i> Requerimiento de Compra</li>
        <li class="breadcrumb-item active">Detalles</li>
      </ol>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <!-- Line chart -->
    <div class="card card-primary card-outline">
      <div class="card-header">
        <h4><b>Detalles Requerimiento de Compra</b></h4>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <br>
        <h4><b>Trámite N° {{$data->id}}</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Estado del Trámite</b></td>
              <td >{{$data->estado}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Origen del Requerimiento</b></td>
              <td >{{$data->origen_requerimiento}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Fecha del Requerimiento</b></td>
              <td >{{$data->fecha_requerimiento}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Nombres Solicitante</b></td>
              <td >{{$data->nombres_solicitante}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Apellidos Solicitante</b></td>
              <td >{{$data->apellidos_solicitante}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>correo solicitante</b></td>
              <td >{{$data->correo_solicitante}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Unidad Requirente</b></td>
              <td >{{$data->unidad_requirente}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Con Cargo A</b></td>
              <td >{{$data->con_cargo_a}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Tipo de Compra</b></td>
              <td >{{$data->compra_por}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>¿Está en el Plan de Compras?</b></td>
              <td >{{$data->objeto}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Ítem Plan de Compras</b></td>
              <td >{{$data->item_plan_de_compras}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>¿Pasaje Aéreo?</b></td>
              <td >{{$data->pasaje_aereo}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Fecha de Vuelo</b></td>
              <td >{{$data->fecha_vuelo}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Listado de Requerimiento</b></td>
              <td>
                <table class="table table-striped table-sm"  >
                    <thead >
                        <tr style="background-color: #013273;">
  					    <th  style="color:white"><center>Cantidad</center></th>
                          <th  style="color:white"><center>Bien o Servicio</center></th>
                          <th  style="color:white"><center>Convenio Marco</center></th>
                          <th  style="color:white"><center>Imputación</center></th>
                          <th  style="color:white"><center>Valor Unitario</center></th>
                        </tr>
                    </thead>
                    <tbody class="panel">
                      @if($data->listado_requerimiento)
                      @foreach ($data->listado_requerimiento as $indice => $tramite)
                    <tr>
                       <td ><center>{{$tramite[0]}}</center></td>
  					           <td ><center>{{$tramite[1]}}</center></td>
                       <td ><center>{{$tramite[2]}}</center></td>
                       <td ><center>{{$tramite[3]}}</center></td>
                       <td ><center>{{$tramite[4]}}</center></td>
                    </tr>
                     @endforeach
                     @endif
                    <tbody>
                 </table>
              </td>
          </tr>
          <tr>
              <td style="width:450px"><b>Monto</b></td>
              <td >{{$data->monto}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Observaciones de la Compra</b></td>
              <td >{{$data->observaciones}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Objeto de la Compra</b></td>
              <td >{{$data->objeto}}</td>
          </tr>

          <tr>
              <td style="width:450px"><b>Descargar Resumen de la Compra:</b></td>
              <td >
              @if($data->descargar)
              <a href="{{$data->descargar}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
              </td>
          </tr>
        </table>
        <br><br>
        <h4><b>Antecedentes</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>¿Adjunta Antecedentes?</b></td>
              <td >{{$data->adjunta_antecedentes}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Bases Ténicas</b></td>
              <td >
              @if($data->bases_tecnicas)
              <a href="{{$data->bases_tecnicas}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
            </td>
          </tr>
          <tr>
              <td style="width:450px"><b>Planos</b></td>
              <td >
              @if($data->planos)
              <a href="{{$data->planos}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
            </td>
          </tr>
          <tr>
              <td style="width:450px"><b>Justificación (Trato Directo)</b></td>
              <td >
              @if($data->justificacion_td)
              <a href="{{$data->justificacion_td}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
            </td>
          </tr>
          <tr>
              <td style="width:450px"><b>Cotizaciones (Trato Directo)</b></td>
              <td >
              @if($data->cotizaciones)
              <a href="{{$data->cotizaciones}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
            </td>
          </tr>
          <tr>
              <td style="width:450px"><b>Documento Digital Adicional 1</b></td>
              <td >
              @if($data->archivos_digitales_1)
              <a href="{{$data->archivos_digitales_1}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
            </td>
          </tr>
          <tr>
              <td style="width:450px"><b>Documento Digital Adicional 2</b></td>
              <td >
              @if($data->archivos_digital_adicional_2)
              <a href="{{$data->archivos_digital_adicional_2}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
            </td>
          </tr>
        </table>
        <br><br>
        <h4><b>Etapas</b></h4>
        <br>
        <table class="table table-striped">
              <thead>
                <tr>
                  <th>Fecha Inicio</th>
                  <th> </th>
                  <th>Fecha Término</th>
                  <th> </th>
                  <th>Nombre Etapa</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data->etapas as $indice7 => $tramite7)
                <tr>
                 <td>{{$tramite7->fecha_inicio}}<td>
                 <td>{{$tramite7->fecha_termino}}<td>
                 <td>{{$tramite7->tarea->nombre}}<td>
               </tr>
               @endforeach
             </tbody>
        </table>
        <br><br>
        <h4><b>Aprobaciones y Observaciones de Etapas</b></h4>
        <br>
        <table id="example2" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Autorización SEREMI</b></td>
              <td >{{$data->seremi_rev}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Observaciones SEREMI</b></td>
              <td >{{$data->observaciones_seremi}}</td>
          </tr>
          </table> <br><br>
          <table id="example2" class="table table-striped">
            <tr>
                <td style="width:450px"><b>Autorización Jefe División o Encargado Unidad</b></td>
                <td >{{$data->jefatura_rev}}</td>
            </tr>
            <tr>
                <td style="width:450px"><b>Observaciones Jefe División o Encargado Unidad</b></td>
                <td >{{$data->observaciones_jefatura}}</td>
            </tr>
            </table> <br><br>
          <table id="example2" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Autorización del Jefe de Finanzas (Revisión 1)</b></td>
              <td >{{$data->revision_jf}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Observaciones Jefe de Finanzas (Revisión 1)</b></td>
              <td >{{$data->observaciones_jf}}</td>
          </tr>
          </table> <br><br>
          <table id="example2" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Acciones de Contabilidad y Presupuesto</b></td>
              <td >{{$data->accion_a_tomar}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Observaciones de Contabilidad y Presupuesto</b></td>
              <td >{{$data->observaciones_cont_presu}}</td>
          </tr>
          </table> <br><br>
          <table id="example2" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Autorización del Jefe de Finanzas (Revisión 2)</b></td>
              <td >{{$data->aprueba_solicitud_certificado}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Observaciones Jefe de Finanzas (Revisión 2)</b></td>
              <td >{{$data->observaciones_jf2}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Certificado</b></td>
              <td >
              @if($data->certificado_documento_descarga)
              <a href="{{$data->certificado_documento_descarga}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
            </td>
          </tr>
          <!-- <tr>
              <td style="width:450px"><b>Certificado de Disponibilidad Presupuestaria</b></td>
              <td >{{$data->certificado_documento}}</td>
          </tr> -->
          </table> <br><br>
          <table id="example2" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Autorización del Jefe DAF</b></td>
              <td >{{$data->daf}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Observaciones Jefe DAF</b></td>
              <td >{{$data->observacion_jefedaf}}</td>
          </tr>
        </table><br><br>
        <table id="example2" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Autorización Gabinete Subsecretario</b></td>
              <td >{{$data->autorizacion_jefe_de_servicio}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Observaciones Gabinete Subsecretario</b></td>
              <td >{{$data->observaciones_jefe_de_servicio}}</td>
          </tr>
          </table> <br><br>
        <table id="example2" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Autorización de Area de Compras</b></td>
              <td >{{$data->cumple_los_requisitos}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Tipo de Compra</b></td>
              <td >{{$data->tipo_de_compra}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Observaciones Area de Compras</b></td>
              <td >{{$data->observacion_compras}}</td>
          </tr>
        </table>
        <br><br>
        <h4><b>Orden(es) de Compra</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Fecha Orden(es) de Compra</b></td>
              <td >{{$data->fecha_orden_compra}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Número de Orden(es) de Compra</b></td>
              <td >{{$data->numero_de_orcom}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Orden(es) de Compra</b></td>
              <td >
              @if($data->orden_de_compra)
              <a href="{{$data->orden_de_compra}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
            </td>
          </tr>
          <tr>
              <td style="width:450px"><b>Bases de Aprobación de Licitación</b></td>
              <td >
              @if($data->bases_de_doc_li)
              <a href="{{$data->bases_de_doc_li}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
            </td>
          </tr>
          <tr>
              <td style="width:450px"><b>Bases de Aprobación de Trato Directo</b></td>
              <td >
              @if($data->bases_de_doc_tra)
              <a href="{{$data->bases_de_doc_tra}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
            </td>
          </tr>
        </table>
        <!--<br><br>
        <h4><b>Factura(s) de la Compra</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Fecha Factura(s) de la Compra</b></td>
              <td >{{$data->fecha_factura}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Número de Factura</b></td>
              <td >{{$data->n_factura}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Factura(s) de la Compra</b></td>
              <td >
              @if($data->adjuntar_factura)
              <a href="{{$data->adjuntar_factura}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
            </td>
          </tr>
          <tr>
              <td style="width:450px"><b>Archivos Adicionales</b></td>
              <td >
              @if($data->archivos_adicionales)
              <a href="{{$data->archivos_adicionales}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
            </td>
          </tr>
        </table>
         <br><br>
        <h4><b>Acta de Recepción Conforme</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Acta de Recepción Conforme Firmada</b></td>
              <td >
              @if($data->act_rc_descarga)
              <a href="{{$data->act_rc_descarga}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
            </td>
          </tr>
          <tr>
              <td style="width:450px"><b>Archivo A</b></td>
              <td >
              @if($data->archivo_a)
              <a href="{{$data->archivo_a}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
            </td>
          </tr>
          <tr>
              <td style="width:450px"><b>Archivo B</b></td>
              <td >
              @if($data->archivo_b)
              <a href="{{$data->archivo_b}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
            </td>
          </tr>
          <tr>
              <td style="width:450px"><b>Archivo C</b></td>
              <td >
              @if($data->archivo_c)
              <a href="{{$data->archivo_c}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
            </td>
          </tr>
          <tr>
              <td style="width:450px"><b>Archivo D</b></td>
              <td >
              @if($data->archivo_d)
              <a href="{{$data->archivo_d}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
            </td>
          </tr>
          <tr>
              <td style="width:450px"><b>Archivo E</b></td>
              <td >
              @if($data->archivo_e)
              <a href="{{$data->archivo_e}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
            </td>
          </tr>
        </table> -->
      </div>

        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! link_to_route('requecompras', 'Volver', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
            </div>
          </div>
        </div>
    </div>
  </div>
</div>


@endsection
@section('before-scripts-end')
@stop
