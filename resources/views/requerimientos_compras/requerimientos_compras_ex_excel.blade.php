<table>
  <tr>
   <th colspan="29" style="text-align: center;">MINISTERIO DEL DEPORTE</th>
  </tr>
  <tr>
   <th colspan="29" style="text-align: center;">DAF - Unidad de Finanzas</th>
  </tr>
  <tr>
   <th colspan="29" style="text-align: center;">Requerimientos de Compras</th>
  </tr>
</table >

<table>
      <tr>
        <th colspan="3" style="color:#ffff; text-align: center; background-color: #013273;">Fecha Requerimiento</th>
        <th colspan="2" style="color:#ffff; text-align: center; background-color: #013273;">N° Trámite</th>
        <th colspan="3" style="color:#ffff; text-align: center; background-color: #013273;">Estado</th>
        <th colspan="9" style="color:#ffff; text-align: center; background-color: #013273;">Etapa</th>
        <th colspan="5" style="color:#ffff; text-align: center; background-color: #013273;">Asignado A</th>
        <th colspan="7" style="color:#ffff; text-align: center; background-color: #013273;">Unidad Requirente</th>
      </tr>
    @if(count($dataFinal)>0)
      @foreach ($dataFinal as $indice => $tramite)
      <tr>
        <td colspan="3" valign="middle" style="text-align: center;" ><center>{{$tramite->fecha_requerimiento}}</center></td>
        <td colspan="2" valign="middle" style="text-align: center;" ><center>{{$tramite->id}}</center></td>
        @if($tramite->estado == 'pendiente')
        <td colspan="3" valign="middle" style="text-align: center; color:#cd0b27;" ><center>{{$tramite->estado}}</center></td>
        @else
        <td colspan="3" valign="middle" style="text-align: center; color:#013273;" ><center>{{$tramite->estado}}</center></td>
        @endif
        <td colspan="9" valign="middle" style="text-align: center;" ><center>{{$tramite->etapa_actual}}</center></td>
        <td colspan="5" valign="middle" style="text-align: center;" ><center>{{$tramite->usuario_asignado_etapa}}</center></td>
        <td colspan="7" valign="middle" style="text-align: center;" ><center>{{$tramite->unidad_requirente}}</center></td>
      </tr>
      @endforeach
    @endif
</table>
