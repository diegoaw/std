
@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-5">
      <h1><i class="fa fa-envelope"></i> Distibución Resolución Excenta Electrónica</h1>
    </div>
    <div class="col-sm-7">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-envelope"></i> Resolución Excenta Electrónica </li>
        <li class="breadcrumb-item active">Detalles</li>
      </ol>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
<link rel="stylesheet" href="{{asset('css/chosen/chosen.css')}}">
@endsection
@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title"> Envio Resolución Excenta Electrónica Electrónico N° @if($detalle->correlativo=='') {{$detalle->correlativo}} @else {{$detalle->correlativo->correlativo}} @endif</h5>

      </div>
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">

                {!! Form::open(['route' => ['ResolucionesExentasEnvioDistribucion' , $detalle->id], 'method' => 'post' , 'class'=>"form-horizontal"]) !!}
                <div class="col-sm-10">
                  <label for="correos_internos" class=" col-form-label">Destinatarios Internos:</label>
                  {{ Form::select('correos_internos[]', $selectUsuarios, null, ['class' => 'chosen-select', 'multiple','col-sm-10', 'data-placeholder' => 'Seleccione uno o más destinatarios internos','style'=>'width:nowrap; !important']) }}
                </div><br><br>

                    <div class="col-sm-10">
                      <label for="correos_externos" class=" col-form-label">Destinatarios Externos:</label>
                    </div>
                        <div class="col-sm-10">
                        <input class="form-control rounded-0" id="correos_externos[]" name="correos_externos[]" placeholder="Ingrese Destinatario Externo 1" rows="3" >
                      </div><br>
                        <div class="col-sm-10">
                        <input class="form-control rounded-0" id="correos_externos[]" name="correos_externos[]" placeholder="Ingrese Destinatario Externo 2" rows="3" >
                        </div><br>
                        <div class="col-sm-10">
                        <input class="form-control rounded-0" id="correos_externos[]" name="correos_externos[]" placeholder="Ingrese Destinatario Externo 3" rows="3" >
                        </div><br>
                        <div class="col-sm-10">
                        <input class="form-control rounded-0" id="correos_externos[]" name="correos_externos[]" placeholder="Ingrese Destinatario Externo 4" rows="3" >
                        </div><br>
                        <div class="col-sm-10">
                        <input class="form-control rounded-0" id="correos_externos[]" name="correos_externos[]" placeholder="Ingrese Destinatario Externo 5" rows="3" >
                        </div><br>
                      <div id='inputsce'></div>


                  <button type="submit" class="btn btn-primary pull-right">Enviar</button>
                {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-md-12">
    <!-- Line chart -->
    <div class="card card-danger card-outline">
      <div class="card-header">
        <h3 class="card-title">Detalles Resolución Exenta Electrónica N° @if($detalle->correlativo=='') {{$detalle->correlativo}} @else {{$detalle->correlativo->correlativo}} @endif</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <table id="example1" class="table table-bordered table-striped table-hover table-sm">
          <tr>
            <th>Fecha</th>
            <td>{{$detalle->fecha}}</td>
          </tr>
          <tr>
            <th>Trámite N°</th>
            <td>{{$detalle->tramite_id}}</td>
          </tr>
           <tr>
            <th>Correlativo N°</th>
            @if($detalle->correlativo=='')
            <td>{{$detalle->correlativo}}</td>
            @else
            <td>{{$detalle->correlativo->correlativo}}</td>
            @endif
          </tr>
          <tr>
            <th>Lugar</th>
            <td>{{$detalle->lugar}}</td>
          </tr>
          <tr>
            <th>Remitente</th>
            <td>{{$detalle->remitente}}</td>
          </tr>
          <tr>
            <th>División</th>
            <td>{{$detalle->division}} </td>
          </tr>
          <tr>
            <th>Referencia</th>
            <td>{!!$detalle->referencia!!}</td>
          </tr>
          <tr>
            <th>Vistos</th>
            <td>{!!$detalle->vistos!!}</td>
          </tr>
          <tr>
            <th>Considerando</th>
            <td>{!!$detalle->considerando!!}</td>
          </tr>
          <tr>
            <th>Resuelvo</th>
            <td>{!!$detalle->resuelvo!!}</td>
          </tr>
          <tr>
            <th>Vistos de Resposabilidad</th>
            <td>{{$detalle->vistos_responsabilidad}}</td>
          </tr>
          <tr>
            <th>Distribución</th>
            <td>{!!$detalle->distribucion!!}</td>
          </tr>
          <tr>
            <th>Resolución Exenta Electrónica</th>
            <td>
            @if($detalle->doc_resolucion_exenta)
            <a href="{{$detalle->doc_resolucion_exenta}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
            @endif
          </td>
          </tr>
          <tr>
            <th>Anexos</th>
            <td>
             
                @if($detalle->doc_anexo_a) 
                <a href="{{$detalle->doc_anexo_a}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
                @endif 
                @if($detalle->doc_anexo_b) 
                <a href="{{$detalle->doc_anexo_b}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
                @endif 
                @if($detalle->doc_anexo_c) 
                <a href="{{$detalle->doc_anexo_c}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
                @endif
                @if($detalle->doc_anexo_d)
                <a href="{{$detalle->doc_anexo_d}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
                @endif 
          </tr>
          </tr>
        </table>
      </div>

        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! link_to_route('ResolucionesExentas', 'Volver', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
              <a class="btn accion btn-success accion" href="{{route('ResolucionesExentasDistribucion',['id'=>$detalle->id,'tramite_id'=>$detalle->tramite_id])}}">Reenviar Resolución Excenta Electrónica</a>
            </div>
          </div>
        </div>



    </div>
  </div>
</div>


@endsection
@section('before-scripts-end')
<script type="text/javascript" src="{{asset('js/chosen/chosen.jquery.js')}}"></script>
<script type="text/javascript">
    $(".chosen-select").chosen({
      width: "100%",
      max_selected_options: 5,
      no_results_text: "No se encontraron resultados para "
    });
  </script>

@stop
