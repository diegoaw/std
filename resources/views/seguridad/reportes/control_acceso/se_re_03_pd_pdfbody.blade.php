{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

{!! Html::style('css/pdf.css') !!}
<table style="width: 100%" border="0" cellspacing="0">
    <thead style="background-color: #C2E7FC;">
      <tr class="class="thead-dark"" role="alert">
        <th>Usuario</th>
        <th>IP</th>
        <th>Fecha</th>
        <th>Acción</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($logs as $log)
      <tr>
        <td>{{ $log->username }}</td>
        <td>{{ $log->ip_address }}</td>
        <td>{{ $log->created_at }}</td>
        <td>{{ $log->action }}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
