{{-- @Nombre del programa: Vista Excel re->Listado General --}}
{{-- @Funcion: --}}
{{-- @Autor: --}}
{{-- @Fecha Creacion: 05/09/18--}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 05/09/18--}}
{{-- @Modificado por: Ericcor--}}

<table >
  <tr>
   <td><b>Banco Central de Venezuela</b></td>
  </tr>
  <tr>
   <td><b>Gerencia de Tesorería</b></td>
  </tr>
  <tr>
   <td><b>Departamento Cámara de Compensación Electrónica</b></td>
  </tr>
  <tr>
   <th colspan="8" style="text-align: center;">CÁMARA DE COMPENSACIÓN REPORTES-SEGURIDAD</th>
  </tr>
  <tr>
   <th colspan="8" style="text-align: center;">Listado de reportes</th>
  </tr>
   <tr>
   <th> </th>

  </tr>
</table >
          <table>
            <thead>
              <tr style="background-color: #C2E7FC">
                   <th style="text-align: center;" >Código</p></th>
                   <th style="text-align: center;" >Nombre del reporte </p></th>
                   <th style="text-align: center;" >Formato </p></th>
                   <th style="text-align: center;" >Estado </p></th>
                </tr>
              </thead>
              <tbody>
                 <tr>
                 <td><center>R0280</center></td>
                 <td><center>Usuarios</center></td>
                 <td><center>PDF/EXCEL</center></td>
                 <td><center>Disponible</center></td>
                 </tr>

                 <tr>
                 <td><center>R0281</center></td>
                 <td><center>Roles</center></td>
                 <td><center>PDF/EXCEL</center></td>
                 <td><center>Disponible</center></td>
                 </tr>

                <tr>
                 <td><center>R0282</center></td>
                 <td><center>Control de acceso</center></td>
                 <td><center>PDF/EXCEL</center></td>
                 <td><center>Disponible</center></td>
                 </tr>

                 <tr>
                 <td><center>R0283</center></td>
                 <td><center>Histórico de cambios</center></td>
                 <td><center>PDF/EXCEL</center></td>
                 <td><center>Disponible</center></td>
                 </tr>

              </tbody>
            </table>
