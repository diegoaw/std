{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

<!DOCTYPE html>
<html>
<head>
  <title>rReporteUsuarios</title>
  {!! Html::style('css/pdf.css') !!}
</head>
<body>

<div style="display:block">
  <div style="float:left;width: 50%;">
    <p>Banco Central de Venezuela</p>
    <p>Gerencia de Tesorer&#237;a</p>
    <p>Departamento C&#225;mara de Compensaci&#243;n Electr&#243;nica</p>
  </div>
</div>
<div style="display:block">
  <div style="float:right;width: 50%;text-align:right;">
    <p>Fecha:&nbsp;{{ $fecha_actual }}</p>
  </div>
</div>
<br>
<div style="display: block">
    <div style="width: 100%;margin-top:100px;">
      <p align=center>C&#193;MARA DE COMPENSACI&#211;N REPORTE ROLES</p>
  </div>
</div>
