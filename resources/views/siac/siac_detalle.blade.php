
@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-5">
      <h1><i class="fa fa-user"></i> SIAC - Sistema Integral de Atención Ciudadana</h1>
    </div>
    <div class="col-sm-7">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-user"></i><b> SIAC - Sistema Integral de Atención Ciudadana</b></li>
        <li class="breadcrumb-item active">Detalles</li>
      </ol>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <!-- Line chart -->
    <div class="card card-danger card-outline">
      <div class="card-header">
        <h3 class="card-title"><b>Detalles SIAC - Sistema Integral de Atención Ciudadana</b></h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <br>
        <h4><b style="color:DarkBlue;">Requerimiento Usuario - Trámite N° {{$data->id}}</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Origen del Requerimiento:</b></td>
              <td nowrap>{{$data->origen_requerimiento}}</td>
          </tr>
          @if($data->origen_requerimiento == 'Otra Institucion')
          <tr>
              <td style="width:450px"><b>Nombre de la Institución:</b></td>
              <td nowrap>{{$data->cual_origen}}</td>
          </tr>
          @endif
          <tr>
              <td style="width:450px"><b>Tipificación:</b></td>
              <td nowrap>{{$data->tipificacion}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Detalle de la Solicitud:</b></td>
              <td nowrap>{{$data->detalle_solicitud}}</td>
          </tr>
        </table>
        <br>
        <h4><b style="color:DarkBlue;">Datos Usuario</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Nombre Completo:</b></td>
              <td nowrap>{{$data->nombre_completo}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Nacionalidad:</b></td>
              <td nowrap>{{$data->nacionalidad}}</td>
          </tr>
          @if($data->nacionalidad == 'Chilena')
          <tr>
              <td style="width:450px"><b>RUT:</b></td>
              <td nowrap>{{$data->rut}}</td>
          </tr>
          @endif
          @if($data->nacionalidad == 'Otra')
          <tr>
              <td style="width:450px"><b>País de su Nacionalidad:</b></td>
              <td nowrap>{{$data->indique_nacionalidad}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>RUT o Pasaporte:</b></td>
              <td nowrap>{{$data->pasaporte}}</td>
          </tr>
          @endif
        </table>
        <br>
        <h4><b style="color:DarkBlue;">Dirección Contacto</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>País de Residencia:</b></td>
              <td nowrap>{{$data->pais_residencia}}</td>
          </tr>
          @if($data->pais_residencia == 'Chile')
          <tr>
              <td style="width:450px"><b>Región:</b></td>
              <td nowrap>{{$data->region}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Comuna:</b></td>
              <td nowrap>{{$data->comuna}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>N° Calle / N° Casa o Dpto.:</b></td>
              <td nowrap>{{$data->calle_casa}}</td>
          </tr>
          @endif
          @if($data->pais_residencia != 'Chile')
          <tr>
              <td style="width:450px"><b>Dirección Completa:</b></td>
              <td nowrap>{{$data->direccion_completa}}</td>
          </tr>
          @endif
          <tr>
              <td style="width:450px"><b>Teléfono:</b></td>
              <td nowrap>{{$data->telefono}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Email:</b></td>
              <td nowrap>{{$data->correo2}}</td>
          </tr>
        </table>
        <br>
        <h4><b style="color:DarkBlue;">Datos Estadísticos</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Zona en donde vive:</b></td>
              <td nowrap>{{$data->zona}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Nivel Educacional:</b></td>
              <td nowrap>{{$data->nivel_educacional}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Ocupación:</b></td>
              <td nowrap>{{$data->ocupacion}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Participa en alguna Organización Deportiva:</b></td>
              <td nowrap>{{$data->participa_organizacion}}</td>
          </tr>
          @if($data->participa_organizacion == 'Si')
          <tr>
              <td style="width:450px"><b>¿Cuál?:</b></td>
              <td nowrap>{{$data->cual_organizacion}}</td>
          </tr>
          @endif
          <tr>
              <td style="width:450px"><b>Respuesta Mediante:</b></td>
              <td nowrap>{{$data->respuesta_mediante}}</td>
          </tr>   
          @if($data->respuesta_mediante == 'Email')
          <tr>
              <td style="width:450px"><b>Emal:</b></td>
              <td nowrap>{{$data->correo}}</td>
          </tr>
          @endif
          @if($data->respuesta_mediante == 'Carta Certificada')
          <tr>
              <td style="width:450px"><b>Dirección:</b></td>
              <td nowrap>{{$data->direccion_carta}}</td>
          </tr>
          @endif
        </table>
      </div>

        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              <a class="btn accion btn-success accion" title="Enviar Aviso" href="{{route('siacnoti', $data)}}"><i class="fa fa-envelope-o"></i></a>   
              <a data-toggle="tooltip" data-placement="left" title="Eliminar" class="btn btn-sm btn-danger" href="#" OnClick="Eliminar('{{$data->id}}')"><i class="fa fa-trash"></i></a>           
              {!! link_to_route('siac', 'Volver', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
            </div>
          </div>
        </div>
    </div>
  </div>
</div>


@endsection
@section('before-scripts-end')
@include('includes.scripts.tramites_siac')

@stop
