@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-file-text"></i> SIGC - Programas Sociales - Beneficiarios</h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
     <!-- <div class="card-header ">
        <h3 class="card-title"><b>Reporte Excel</b></h3>
      </div>

       <div class="card-footer">
            <div class="card-tools text-center">
                <a href="{{url('/SIGC/ProgramasSociales/Beneficiarios/Reporte',
                  array(
                    'tiporeporte' => 'XLS'
                  )
                )}}" class="btn btn-primary btn-success" role="button">XLS</a>
            </div>
        </div> -->
      
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">



          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@if(count($dataBeneficiarios)>0)
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">Programas Sociales - Beneficiarios</h5>

          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th nowrap style="color:white">Rut Beneficiario</th>
                        <th nowrap style="color:white">Programa</th>
                        <th nowrap style="color:white">Componente</th>
                        <th nowrap style="color:white">Región</th>
                        <th nowrap style="color:white">Comuna</th>
                        <th nowrap style="color:white">Deporte/Discilina</th>
                        <th nowrap style="color:white">Género</th>
                        <th nowrap style="color:white">Edad</th>
                        <th nowrap style="color:white">Comuna Recinto</th>
                        <th nowrap style="color:white">Asistencia Promedio</th>
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($dataBeneficiarios)>0)
                      @foreach ($dataBeneficiarios as $indice => $tramite)
                          <tr>
                           <td>{{$tramite->rut_identificador}}</td>
                           <td>{{$tramite->programa}}</td>
                           <td>{{$tramite->componente}}</td>
                           <td>{{$tramite->region}}</td>
                           <td>{{$tramite->comuna}}</td>
                           <td>{{$tramite->deporte_disciplina}}</td>
                           <td>{{$tramite->genero}}</td>
                           <td>{{$tramite->edad}}</td>
                           <td>{{$tramite->comuna_recinto}}</td>
                           <td>{{$tramite->asistencia_promedio_fecha}}</td>
                          </tr>
                      @endforeach
                @endif
                </tbody>
              </table>
              <br>
              <div class="col-sm-12">
                <div class="pull-right">
                  {{ $dataBeneficiarios->links() }}
                </div><br><br>
              </div>
         

          </div>
      </div>
  </div>
</div>
@endif
@endsection
@section('before-scripts-end')
@stop