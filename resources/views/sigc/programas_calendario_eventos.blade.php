@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-calendar-check-o"></i><b> Programas Sociales - Calendario Eventos</b></h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
<style>
.header-col{
      background: #E3E9E5 !important;
      color:#536170 !important;
      text-align: center !important;
      font-size: 20px !important;
      font-weight: bold !important;
    }
    .header-calendar{
      background: #f0464c!important;
      color:white !important;
    }
    .box-day{
      border:1px solid #E3E9E5 !important;
      height:70px !important;
    }
    .box-dayoff{
      border:1px solid #E3E9E5 !important;
      height:70px !important;
      background-color: #ccd1ce !important;
    }
</style>
@endsection
@section('content')

@if (auth()->user()->hasPermission(['09-14']))
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title">Programas Sociales - Nuevo Calendario Evento</h5>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">
            <div class="col-sm-12">
            <div class="text-center">
              <a class="btn btn-primary" href="{{route('programas_calendario_eventos_crear')}}">Nuevo Calendario Evento</a>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endif

@if(count($dataDefinitivaA)>0)

<div class="row">
      <div class="col-md-12">

        <div class="card">
          <div class="card-header card-danger card-outline">

            <h5 class="card-title">Programas Sociales - Calendario Eventos</h5>

          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                      <th  style="color:white">Año</th>
                      <th  style="color:white">Mes</th>
                      <th  style="color:white">Evento</th>
                      <th  style="color:white">Fecha Inicio</th>
                      <th  style="color:white">Fecha Fin</th>
                      @if (auth()->user()->hasPermission(['09-03']))
                      <th  style="color:white">Editar</th>
                      <th  style="color:white">Eliminar</th>
                      @endif
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($dataDefinitivaA)>0)
                  @foreach ($dataDefinitivaA as $indice => $tramite)
                  <tr>
                    <td nowrap>{{$tramite->ano}}</td>
                    <td nowrap>{{$tramite->meses->nombre}}</td>
                    <td nowrap>{{$tramite->evento->nombre}}</td>
                    <td nowrap>{{date("d-m-Y", strtotime($tramite->fecha_inicio)) }}</td>
                    <td nowrap>{{date("d-m-Y", strtotime($tramite->fecha_fin)) }}</td>
                    @if (auth()->user()->hasPermission(['09-03']))
                    <td>
                    <a class="btn accion btn-success accion" title="Editar" href="{{route('programas_calendario_eventos_editar',$tramite->id)}}"><i class="fa fa-pencil"></i></a>
                    </td>
                    <td>
                    <a data-toggle="tooltip" data-placement="left" title="Eliminar" class="btn btn-sm btn-danger" href="#" OnClick="Eliminar('{{$tramite->id}}')"><i class="fa fa-trash"></i></a>
                    </td>
                    @endif
                  </tr>
                  @endforeach
                @endif
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
@endif

@if(count($dataDefinitivaO)>0)
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title"><b>Programas Sociales - Calendario Eventos Años Anteriores</b></h5>

          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                      <th  style="color:white">Año</th>
                      <th  style="color:white">Mes</th>
                      <th  style="color:white">Evento</th>
                      <th  style="color:white">Fecha Inicio</th>
                      <th  style="color:white">Fecha Fin</th>
                      @if (auth()->user()->hasPermission(['09-03']))
                      <th  style="color:white">Editar</th>
                      @endif
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($dataDefinitivaO)>0)
                  @foreach ($dataDefinitivaO as $indice2 => $tramite1)
                  <tr>
                    <td nowrap>{{$tramite1->ano}}</td>
                    <td nowrap>{{$tramite1->meses->nombre}}</td>
                    <td nowrap>{{$tramite1->evento->nombre}}</td>
                    <td nowrap>{{$tramite1->fecha_inicio}}</td>
                    <td nowrap>{{$tramite1->fecha_fin}}</td>
                    @if (auth()->user()->hasPermission(['09-03']))
                    <td>
                    <a class="btn accion btn-success accion" title="Editar" href="{{route('programas_calendario_eventos_editar',$tramite1->id)}}"><i class="fa fa-pencil"></i></a>
                    </td>
                    @endif
                  </tr>
                  @endforeach
                @endif
                </tbody>
              </table>
          </div>
          <div class="col-sm-12">
            <div class="pull-right">
              {{ $dataDefinitivaO->links() }}
            </div>
          </div>
        </div>
      </div>
    </div>
@endif

@endsection
@section('before-scripts-end')
@include('includes.scripts.sigc.programaseventoscalendario')
@stop
