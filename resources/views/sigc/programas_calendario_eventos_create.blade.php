@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-calendar-plus-o"></i><b> Programas Sociales - Crear Calendario Evento</b></h1><br>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
{!! Form::open(['route' => 'programas_calendario_eventos_nuevo', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','id'=>'form-create']) !!}
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-primary card-outline">
        <h3 class="card-title">Crear Calendario Evento</h3>
      </div>
      <div class="card-body">
        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>

        <div class="callout col-sm-8 offset-sm-2" style="color: #737373;">
          <h4 style="font-size: 14px;"><i class="fa fa-info-circle" style="color: #ff0000;"></i> Nota:</h4>
          <span>Los campos marcados con un asterisco (*) son obligatorios.</span>
        </div><br>

        <div class="form-group row">
          {!! Form::label('Año', 'Año(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
          {!! Form::select('ano',$selectAno,null,['id'=>'ano','name'=>'ano', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Mes', 'Mes(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
          {!! Form::select('mes',$selectMes,null,['id'=>'mes','name'=>'mes', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Evento', 'Evento(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
          {!! Form::select('id_programas_eventos',$selectEvento,null,['id'=>'id_programas_eventos','name'=>'id_programas_eventos', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row" >
          <div class="col-sm-2"></div>
            <div class="form-group row {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
            {!!Form::label('FechaInicio', 'Fecha Inicio', array('class' => 'col-sm-6 col-form-label'))!!}
            <div class="col-sm-6">
              <div class="input-group date">
                <input type="text" class="form-control group-date" id="fecha_inicio" name="fecha_inicio" readonly="readonly" value="{!! date('Y-m-d', strtotime($fe_desde))  !!}">
                <div class="input-group-append">
                  <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group row">
          <div class="col-sm-2"></div>
            <div class="form-group row {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
            {!!Form::label('FechaFin', 'Fecha Fin', array('class' => 'col-sm-6 col-form-label'))!!}
            <div class="col-sm-6">
              <div class="input-group date">
                <input type="text" class="form-control group-date" id="fecha_fin" name="fecha_fin" readonly="readonly" value="{!! date('Y-m-d', strtotime($fe_hasta))  !!}">
                <div class="input-group-append">
                  <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <br> <br>
        
        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! Form::submit('Guardar', ['class' => 'btn btn-success btn-sm','title'=>'Guardar']) !!}
              {!! link_to_route('programas_calendario_eventos', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) !!}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
  {!! Form::close() !!}
  @stop

@section('after-scripts-end')
@include('includes.scripts.script_fecha.scripts_filtro_fecha')
@endsection
