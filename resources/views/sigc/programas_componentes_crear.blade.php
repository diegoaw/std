@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-edit"></i><b>Programas Sociales - Crear Componente</h1><br> 
    </div>    
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
{!! Form::open(['route' => 'programas_componentes_nuevo', 'files' => true, 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','id'=>'form-create']) !!}
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-danger card-outline">
        <h3 class="card-title">Crear Componente</h3>
      </div>
      <div class="card-body">

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>

        <div class="callout col-sm-8 offset-sm-2" style="color: #737373;">
          <h4 style="font-size: 14px;"><i class="fa fa-info-circle" style="color: #ff0000;"></i> Nota:</h4>
          <span>Los campos marcados con un asterisco (*) son obligatorios.</span>
        </div><br>

        <div class="form-group row">
          {!! Form::label('Año', 'Año(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
          {!! Form::select('ano',$selectAno,null,['id'=>'ano','name'=>'ano', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Nombre', 'Nombre(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Nombre']) !!}
          </div>
        </div>   

        <div class="form-group row">
          {!! Form::label('Programa', 'Programa(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
          {!! Form::select('id_programa',$selectPrograma,null,['id'=>'id_programa','name'=>'id_programa', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Unidad', 'Unidad(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('unidad', null, ['class' => 'form-control', 'placeholder' => 'Unidad']) !!}
          </div>
        </div>  
        
        <div class="form-group row">
          <div class="col-sm-3 col-form-label">
            <b></b>
          </div>
          <div class="col-sm-6">
            <b style="color:DarkRed;">Efectivo 1: </b>Ingrese en números el Efectivo Total del {{date("Y")-2}}.
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Efectivo1', 'Efectivo 1(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('efectivo_1', null, ['class' => 'form-control', 'placeholder' => 'Efectivo 1']) !!}
          </div>
        </div> 

        <div class="form-group row">
          <div class="col-sm-3 col-form-label">
            <b></b>
          </div>
          <div class="col-sm-6">
            <b style="color:DarkRed;">Efectivo 2: </b>Ingrese en números el Efectivo Total del {{date("Y")-1}}.
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Efectivo2', 'Efectivo 2(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('efectivo_2', null, ['class' => 'form-control', 'placeholder' => 'Efectivo 2']) !!}
          </div>
        </div> 
  
        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! Form::submit('Guardar', ['class' => 'btn btn-success btn-sm','title'=>'Guardar']) !!}
              {!! link_to_route('programas_componentes', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) !!}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{!! Form::close() !!}
@stop
@section('after-scripts-end')
@endsection