@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-calendar-check-o"></i><b> Programas Sociales - Eventos</b></h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
@if (auth()->user()->hasPermission(['09-14']))
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title">Nuevo Evento</h5>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">
              <div class="form-group">
                <div class="col-sm-12">
                  <div class="text-center">
                  {!! link_to_route('programas_eventos_crear', 'Nuevo Evento', [], ['class' => 'btn btn-primary btn-sm','title'=>'Nuevo Evento']) !!}
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endif

@if(count($dataDefinitiva)>0)
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">Programas Sociales - Eventos</h5>

          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th nowrap style="color:white">ID</th>
                        <th nowrap style="color:white">Nombre</th>
                        <th nowrap style="color:white">Descripción</th>
                        <th nowrap style="color:white">Encargados</th>
                        <th nowrap style="color:white">Editar</th>
                        <th nowrap style="color:white">Eliminar</th>
                        <th nowrap style="color:white">Suplencia</th>
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($dataDefinitiva)>0)
                      @foreach ($dataDefinitiva as $indice => $tramite)
                          <tr>
                           <td nowrap>{{$tramite->id}}</td>
                           <td>{{$tramite->nombre}}</td>
                           <td>{{$tramite->descripcion}}</td>
                           <td><a type="button" title="Encargados" class="btn btn-info" data-toggle="collapse" {!! "data-target='#demo".$indice."'" !!}   {!!"data-parent='#myTable".$tramite->id."'" !!}
                           ><i class="fa fa-users"></i></a></td>
                           <td><a class="btn accion btn-success accion" href="{{route('programas_eventos_editar',$tramite->id)}}"><i class="fa fa-pencil"></i></a></td>
                           <td><a data-toggle="tooltip" data-placement="left" title="Eliminar" class="btn btn-sm btn-danger" href="#" OnClick="Eliminar('{{$tramite->id}}')"><i class="fa fa-trash"></i></a></td>
                           <td>@if($tramite->habilitar_suplencia == 0)
                            <a class="btn accion btn-primary accion" href="{{route('sigc_eventos_habilitar_suplencia',$tramite->id)}}">Habilitar Suplencia</a>
                           @endif
                           @if($tramite->habilitar_suplencia == 1)
                            <a class="btn accion btn-warning accion" href="{{route('sigc_eventos_deshabilitar_suplencia',$tramite->id)}}">Deshabilitar Suplencia</a>
                           @endif</td>
                          </tr>
                          <tr {!! "id='demo".$indice."'"  !!} class="collapse">
                            <td colspan="10" class="hiddenRow">
                              <table class="table table-striped table-sm" {!!  "id='myTable".$tramite->id."'" !!}  >
                                <tr>
                                  <td colspan="2" style="background-color: #013273; color:white; width:10%" ><b>Encargado</b></td>
                                  <td style="background-color: #fff;">{{$tramite->Encargado->nombres}} {{$tramite->Encargado->ap_paterno}} {{$tramite->Encargado->ap_materno}}</td>
                                </tr>
                                <tr>
                                  <td colspan="2" style="background-color: #013273; color:white; width:10%" ><b>Suplente</b></td>
                                  <td style="background-color: #fff;" >{{$tramite->Suplente->nombres}} {{$tramite->Suplente->ap_paterno}} {{$tramite->Suplente->ap_materno}}</td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                      @endforeach
                @endif
                </tbody>
              </table>
          </div>
      </div>
  </div>
</div>
@endif
@endsection
@section('before-scripts-end')
@include('includes.scripts.sigc.programaseventos')
@stop
