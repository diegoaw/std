@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-bars"></i><b> Programas Sociales - Indicadores</b></h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
<style>
.header-col{
      background: #E3E9E5 !important;
      color:#536170 !important;
      text-align: center !important;
      font-size: 20px !important;
      font-weight: bold !important;
    }
    .header-calendar{
      background: #f0464c!important;
      color:white !important;
    }
    .box-day{
      border:1px solid #E3E9E5 !important;
      height:70px !important;
    }
    .box-dayoff{
      border:1px solid #E3E9E5 !important;
      height:70px !important;
      background-color: #ccd1ce !important;
    }
</style>
@endsection
@section('content')
@if (auth()->user()->hasPermission(['09-14']))
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title">Nuevo Indicador</h5>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">
            <div class="form-group">
              <div class="col-sm-12">
                <div class="text-center">
                {!! link_to_route('programas_indicadores_crear', 'Nuevo Indicador', [], ['class' => 'btn btn-primary btn-sm','title'=>'Nuevo Indicador']) !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endif

@if(count($dataDefinitivaA)>0)
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">Programas Sociales - Indicadores Año {{date("Y")}}</h5>

          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th nowrap style="color:white"><center>ID</center></th>
                        <th nowrap style="color:white"><center>Año</center></th>
                        <th nowrap style="color:white"><center>Nivel</center></th>
                        <th nowrap style="color:white"><center>Nombre</center></th>
                        <th nowrap style="color:white"><center>Programa</center></th>
                        <th nowrap style="color:white"><center>Propósito/Componente</center></th>
                        <th nowrap style="color:white"><center>Editar</center></th>
                        <th nowrap style="color:white"><center>Eliminar</center></th>
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($dataDefinitivaA)>0)
                      @foreach ($dataDefinitivaA as $indice => $tramite)
                          <tr>
                           <td><center>{{$tramite->id}}</center></td>
                           <td><center>{{$tramite->ano}}</center></td>
                           @if($tramite->id_proposito == '0')
                            <td><center>C.{{$tramite->id_componente}}</center></td>
                           @endif
                           @if($tramite->id_proposito == '1')
                            <td><center>P.{{$tramite->id_programa}}</center></td>
                           @endif
                           <td><center>{{$tramite->nombre}}</center></td>
                           <td><center>{{$tramite->programas->nombre}}</center></td>
                           @if($tramite->id_proposito == '1')
                            <td nowrap><center>Propósito</center></td>
                           @endif
                           @if($tramite->id_proposito == '0')
                            <td nowrap><center>{{$tramite->componentes->nombre}}</center></td>
                           @endif
                           <td><center><a class="btn accion btn-success accion" href="{{route('programas_indicadores_editar',$tramite->id)}}"><i class="fa fa-pencil"></i></a></center></td>
                           <td><center><a data-toggle="tooltip" data-placement="left" title="Eliminar" class="btn btn-sm btn-danger" href="#" OnClick="Eliminar('{{$tramite->id}}')"><i class="fa fa-trash"></i></a></center></td>
                          </tr>
                      @endforeach
                @endif
                </tbody>
              </table>
          </div>
      </div>
  </div>
</div>
@endif

@if(count($dataDefinitivaO)>0)
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">Programas Sociales - Indicadores Años Anteriores</h5>

          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th nowrap style="color:white"><center>ID</center></th>
                        <th nowrap style="color:white"><center>Año</center></th>
                        <th nowrap style="color:white"><center>Nivel</center></th>
                        <th nowrap style="color:white"><center>Nombre</center></th>
                        <th nowrap style="color:white"><center>Programa</center></th>
                        <th nowrap style="color:white"><center>Propósito/Componente</center></th>
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($dataDefinitivaO)>0)
                      @foreach ($dataDefinitivaO as $indice => $tramite1)
                          <tr>
                           <td><center>{{$tramite1->id}}</center></td>
                           <td><center>{{$tramite1->ano}}</center></td>
                           @if($tramite1->id_proposito == '0')
                            <td><center>C.{{$tramite1->id_componente}}</center></td>
                           @endif
                           @if($tramite1->id_proposito == '1')
                            <td><center>P.{{$tramite1->id_programa}}</center></td>
                           @endif
                           <td><center>{{$tramite1->nombre}}</center></td>
                           <td nowrap><center>{{$tramite1->programas->nombre}}</center></td>
                           @if($tramite1->id_proposito == '1')
                            <td nowrap><center>Propósito</center></td>
                           @endif
                           @if($tramite1->id_proposito == '0')
                            <td nowrap><center>{{$tramite1->componentes->nombre}}</center></td>
                           @endif
                          </tr>
                      @endforeach
                @endif
                </tbody>
              </table>
          </div>
          <div class="col-sm-12">
            <div class="pull-right">

              {{ $dataDefinitivaO->links() }}

            </div>
          </div>
      </div>
  </div>
</div>
@endif

@endsection
@section('before-scripts-end')
@include('includes.scripts.sigc.programasindicadores')
@stop
