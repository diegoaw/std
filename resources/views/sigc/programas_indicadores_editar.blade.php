@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-edit"></i><b> Programas Sociales - Editar Indicador</h1><br>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
{!! Form::open(['route' => ['programas_indicadores_update' , $editIndicador->id], 'files' => true, 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','id'=>'form-create']) !!}

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-danger card-outline">
        <h3 class="card-title">Editar Indicador</h3>
      </div>
      <div class="card-body">

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>

        <div class="callout col-sm-8 offset-sm-2" style="color: #737373;">
          <h4 style="font-size: 14px;"><i class="fa fa-info-circle" style="color: #ff0000;"></i> Nota:</h4>
          <span>Los campos marcados con un asterisco (*) son obligatorios.</span>
        </div><br>

        <div class="form-group row">
          {!! Form::label('Año', 'Año(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
          {!! Form::select('ano',$selectAno,$editIndicador->ano,['id'=>'ano','name'=>'ano', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Programa', 'Programa(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
          {!! Form::select('id_programa',$selectPrograma,$editIndicador->id_programa,['id'=>'id_programa','name'=>'id_programa', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row" id="proposito" >
          {!! Form::label('proposito','¿Este Indicador es de Propósito?', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            <label class="radio-inline">
                {{ Form::radio('proposito', '1', $editIndicador->id_proposito == 1)}} Si
            </label>
            <label class="radio-inline">
                {{ Form::radio('proposito', '0', $editIndicador->id_proposito == 0)}} No
            </label>
          </div>
        </div>

        @if($editIndicador->id_proposito == 0)
        <div class="form-group row" id="id_componente" style="display:flex;">
          {!! Form::label('Componente', 'Componente(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
          {!! Form::select('id_componente',$selectComponente,$editIndicador->id_componente,['id'=>'id_componente','name'=>'id_componente', 'class'=>'form-control']) !!}
          </div>
        </div>
        @endif

        @if($editIndicador->id_proposito == 1)
        <div class="form-group row" id="id_componente" style="display:none;">
          {!! Form::label('Componente', 'Componente(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
          {!! Form::select('id_componente',$selectComponente,$editIndicador->id_componente,['id'=>'id_componente','name'=>'id_componente', 'class'=>'form-control']) !!}
          </div>
        </div>
        @endif

        <div class="form-group row">
          {!! Form::label('Nombre', 'Nombre(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('nombre', $editIndicador->nombre,  ['class' => 'form-control', 'placeholder' => 'Nombre']) !!}
          </div>
        </div>
        
        <div class="form-group row">
          <div class="col-sm-3 col-form-label">
            <b></b>
          </div>
          <div class="col-sm-6">
            <b style="color:DarkRed;">Efectivo 1: </b>Ingrese en números el Efectivo Total del {{date("Y")-2}}.
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Efectivo1', 'Efectivo 1(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('efectivo_1', $editIndicador->efectivo_1, ['class' => 'form-control', 'placeholder' => 'Efectivo 1']) !!}
          </div>
        </div> 

        <div class="form-group row">
          <div class="col-sm-3 col-form-label">
            <b></b>
          </div>
          <div class="col-sm-6">
            <b style="color:DarkRed;">Efectivo 2: </b>Ingrese en números el Efectivo Total del {{date("Y")-1}}.
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Efectivo2', 'Efectivo 2(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('efectivo_2', $editIndicador->efectivo_2, ['class' => 'form-control', 'placeholder' => 'Efectivo 2']) !!}
          </div>
        </div> 

        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! Form::submit('Guardar', ['class' => 'btn btn-success btn-sm','title'=>'Guardar']) !!}
              {!! link_to_route('programas_indicadores', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) !!}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{!! Form::close() !!}
@stop
@section('after-scripts-end')
<script>
$(document).ready(function(){
  $('input[type=radio][name=proposito]').on('change', function(){
    let proposito = this.value;
    if(proposito == 1){
      $("#id_componente").css("display", "none");
    }
    if(proposito == 0){
      $("#id_componente").css("display", "flex");
    }
  });
});
</script>
@endsection
