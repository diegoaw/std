@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-users"></i><b> Programas Sociales - Inicio</b></h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')

<style>
    .header-col{
      background: #E3E9E5;
      color:#536170;
      text-align: center;
      font-size: 12px;
      font-weight: bold;

    }
    .header-calendar{
      background: #EE192D;color:white;
      height:70px;
    }
    .box-day{
      border:1px solid #E3E9E5;
      height:70px;
      font-size: 13px;
      overflow: hidden !important;

    }
    .box-dayoff{
      border:1px solid #E3E9E5;
      height:70px;
      background-color: #ccd1ce;
      overflow: hidden !important;

    }
</style>
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-danger card-outline">
      <div class="card-header ">
          <h5 class="card-title">Calendario Actual</h5>
          
          <a class="btn btn-primary pull-right" href="https://portalmindep.sharepoint.com/sites/planificacion" target="_blank">Sitio Planificación</a>
          
      </div>
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">
            <div class="row align-items-center">
              <div class="col-sm-0">
              </div>
              <div class="col-sm-6">
                <div class="container">
                  <div class="row header-calendar"  >
                    <div class="col" style="display: flex; justify-content: space-between; padding: 20px;">
                      <a  href="{{ asset('/SIGC/ProgramasSociales/Inicio/') }}/<?= $data['last']; ?>" style="margin:20px;"><i class="nav-icon fa fa-hand-o-left" style="color : #FFF"></i></a>
                        <h2 style="font-weight:bold;margin:10px;"><?= $mespanish; ?> <small><?= $data['year']; ?></small></h2>
                      <a  href="{{ asset('/SIGC/ProgramasSociales/Inicio/') }}/<?= $data['next']; ?>" style="margin:10px;"><i class="nav-icon fa fa-hand-o-right" style="color : #FFF" ></i></a>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col header-col">Lunes</div>
                    <div class="col header-col">Martes</div>
                    <div class="col header-col">Miércoles</div>
                    <div class="col header-col">Jueves</div>
                    <div class="col header-col">Viernes</div>
                    <div class="col header-col">Sábado</div>
                    <div class="col header-col">Domingo</div>
                  </div>
                    @foreach ($data['calendar'] as $index01 => $weekdata)
                    @if($index01 < 6)
                      <div class="row">
                        @foreach  ($weekdata['datos'] as $index2 => $dayweek)
                          @if  ($dayweek['mes']==$mes)
                            <div class="col box-day">
                              {{ $dayweek['dia']  }}
                              @foreach  ($dayweek['evento'] as $event)
                              <b class="{{$event->class}}">
                                    {!!str_replace(' ','<br>', $event->title )!!}
                              </b>
                              @endforeach
                            </div>
                          @else

                          <div class="col box-dayoff">
                          </div>
                          @endif
                          @endforeach
                      </div>
                      @endif
                      @endforeach
                </div>
              </div>
              <div class="col-sm-4">
                <table class="table table-striped table-sm"  >
                    <thead >
                      <tr style="background-color: #cd0b27;">
                        <th nowrap style="color:white">Evento</th>
                        <th nowrap style="color:white"><center>Fecha Inicio</center></th>
                        <th nowrap style="color:white"><center>Fecha Fin</center></th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($eventosMes as $evento)
                      <tr>
                        <td class="{{$evento->class}}">{{$evento->title}}</td>
                        <td><center>{{date("d-m-Y", strtotime($evento->start_date))}}</center></td>
                        <td><center>{{date("d-m-Y", strtotime($evento->end_date))}}</center></td>
                      </tr>
                      @endforeach
                    </tbody>
                </table>
              </div>
              <div class="col-sm-2">
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title">Filtros Programas Sociales</h5>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">
            {!! Form::open(['route' => 'programas_inicio', 'method' => 'get' , 'class'=>"form-horizontal"]) !!}
                  <div class="form-row">

                  <div class="col-sm-3">
                    <label for="programa" class=" col-form-label">Programas: </label>
                    {!! Form::select('programa',$selectPrograma,null,['id'=>'programa','name'=>'programa', 'class'=>'form-control']) !!}
                  </div>

                  <div class="col-sm-3">
                    <label for="proposito" class=" col-form-label">Propositos: </label>
                    {!! Form::select('proposito',$selectProposito,null,['id'=>'proposito','name'=>'proposito', 'class'=>'form-control']) !!}
                  </div>

                  <div class="col-sm-3">
                    <label for="componente" class=" col-form-label">Componentes: </label>
                    {!! Form::select('componente',$selectComponente,null,['id'=>'componente','name'=>'componente', 'class'=>'form-control']) !!}
                  </div>

                  </div>
                  <div class="form-group">
                  </div>
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="text-right">
                        <input type="submit" class= "btn btn-primary" value="Buscar" name="Buscar">
                      </div>
                    </div>
                  </div>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@if(count($dataDefinitiva)>0)
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">Programas Sociales - Indicadores</h5>

          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th nowrap style="color:white">ID</th>
                        <th nowrap style="color:white">Nombre</th>
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($dataDefinitiva)>0)
                      @foreach ($dataDefinitiva as $indice => $tramite)
                          <tr>
                           <td nowrap>{{$tramite->id}}</td>
                           <td nowrap>{{$tramite->nombre}}</td>
                          </tr>
                      @endforeach
                @endif
                </tbody>
              </table>
          </div>
      </div>
  </div>
</div>
@endif
@endsection
@section('before-scripts-end')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="{{ asset('js/jquery.knob.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/demo.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/Chart.min.js') }}"></script>

<script type="text/javascript">
      function inload(){
      setTimeout(function(){ document.getElementById("load").style.display="none"; }, 20000);

      }
      $(document).ready(function(){
      $('#programa').on('change', function(){
        var programa = $(this).val();
            if(programa !== ''){ 
              $.get('/SIGC/SelectProgramasSocialesC/'+programa, function(componentes){
                $('#componente').empty();
                $('#componente').append("<option value>SELECCIONE COMPONENTE</option>");
                $.each(componentes, function(index , value){
                $('#componente').append("<option value='"+index+"'>"+value+" </option>");
                })
              })
            }else{
              $.get('/SIGC/SelectProgramasSocialesCS', function(componentes){
                $('#componente').empty();
                $('#componente').append("<option value>SELECCIONE COMPONENTE</option>");
                $.each(componentes, function(index , value){
                $('#componente').append("<option value='"+index+"'>"+value+" </option>");
                })
              })
            }

            if(programa !== ''){ 
              $.get('/SIGC/SelectProgramasSocialesP/'+programa, function(propositos){
                $('#proposito').empty();
                $('#proposito').append("<option value>SELECCIONE PROPÓSITO</option>");
                $.each(propositos, function(index , value){
                $('#proposito').append("<option value='"+index+"'>"+value+" </option>");
                })
              })
            }else{
              $.get('/SIGC/SelectProgramasSocialesPS', function(propositos){
                $('#proposito').empty();
                $('#proposito').append("<option value>SELECCIONE PROPÓSITO</option>");
                $.each(propositos, function(index , value){
                $('#proposito').append("<option value='"+index+"'>"+value+" </option>");
                })
              })
            }
      });  

    });
</script>
@stop
