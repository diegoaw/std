@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-bars"></i><b> Programas Sociales - Gastos</b></h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
<style>
.header-col{
      background: #E3E9E5 !important;
      color:#536170 !important;
      text-align: center !important;
      font-size: 20px !important;
      font-weight: bold !important;
    }
    .header-calendar{
      background: #f0464c!important;
      color:white !important;
    }
    .box-day{
      border:1px solid #E3E9E5 !important;
      height:70px !important;
    }
    .box-dayoff{
      border:1px solid #E3E9E5 !important;
      height:70px !important;
      background-color: #ccd1ce !important;
    }
</style>
@endsection
@section('content')
@if (auth()->user()->hasPermission(['09-14']))
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title">Nuevo Gasto</h5>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">
            <div class="form-group">
              <div class="col-sm-12">
                <div class="text-center">
                {!! link_to_route('programas_presupuesto_crear', 'Nuevo Gasto', [], ['class' => 'btn btn-primary btn-sm','title'=>'Nuevo Gasto']) !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endif

@if(count($dataDefinitivaA)>0)
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">Programas Sociales - Gastos Año {{date("Y")}}</h5>

          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th nowrap style="color:white"><center>ID</center></th>
                        <th nowrap style="color:white"><center>Año</center></th>
                        <th nowrap style="color:white"><center>Programa</center></th>
                        <th nowrap style="color:white"><center>Fecha</center></th>
                        <th nowrap style="color:white"><center>Ley (M$)</center></th>
                        <th nowrap style="color:white"><center>Vigente (M$)</center></th>
                        <th nowrap style="color:white"><center>Ejecutado (M$)</center></th>
                        <th nowrap style="color:white"><center>Editar</center></th>
                        <th nowrap style="color:white"><center>Eliminar</center></th>
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($dataDefinitivaA)>0)
                      @foreach ($dataDefinitivaA as $indice => $tramite)
                          <tr>
                           <td><center>{{$tramite->id}}</center></td>
                           <td><center>{{$tramite->ano}}</center></td>
                           <td><center>{{$tramite->Programas->nombre}}</center></td>
                           <td><center>{{$tramite->fecha}}</center></td>
                           <td><center>{{$tramite->ley}}</center></td>
                           <td><center>{{$tramite->vigente}}</center></td>
                           <td><center>{{$tramite->ejecutados}}</center></td>
                           <td><center><a class="btn accion btn-success accion" href="{{route('programas_presupuesto_editar',$tramite->id)}}"><i class="fa fa-pencil"></i></a></center></td>
                           <td><center><a data-toggle="tooltip" data-placement="left" title="Eliminar" class="btn btn-sm btn-danger" href="#" OnClick="Eliminar('{{$tramite->id}}')"><i class="fa fa-trash"></i></a></center></td>
                          </tr>
                      @endforeach
                @endif
                </tbody>
              </table>
          </div>
      </div>
  </div>
</div>
@endif

@if(count($dataDefinitivaO)>0)
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">Programas Sociales - Gastos Años Anteriores</h5>

          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th nowrap style="color:white"><center>ID</center></th>
                        <th nowrap style="color:white"><center>Año</center></th>
                        <th nowrap style="color:white"><center>Programa</center></th>
                        <th nowrap style="color:white"><center>Fecha</center></th>
                        <th nowrap style="color:white"><center>Ley (M$)</center></th>
                        <th nowrap style="color:white"><center>Vigente (M$)</center></th>
                        <th nowrap style="color:white"><center>Ejecutado (M$)</center></th>
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($dataDefinitivaO)>0)
                      @foreach ($dataDefinitivaO as $indice => $tramite)
                          <tr>
                           <td><center>{{$tramite->id}}</center></td>
                           <td><center>{{$tramite->ano}}</center></td>
                           <td><center>{{$tramite->Programas->nombre}}</center></td>
                           <td><center>{{$tramite->fecha}}</center></td>
                           <td><center>{{$tramite->ley}}</center></td>
                           <td><center>{{$tramite->vigente}}</center></td>
                           <td><center>{{$tramite->ejecutados}}</center></td>
                          </tr>
                      @endforeach
                @endif
                </tbody>
              </table>
          </div>
          <div class="col-sm-12">
            <div class="pull-right">

              {{ $dataDefinitivaO->links() }}

            </div>
          </div>
      </div>
  </div>
</div>
@endif

@endsection
@section('before-scripts-end')
@include('includes.scripts.sigc.programasgastos')
@stop
