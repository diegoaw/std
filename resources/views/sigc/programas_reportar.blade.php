@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-users"></i><b> Programas Sociales - Reportar</b></h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
<style>
.header-col{
      background: #E3E9E5 !important;
      color:#536170 !important;
      text-align: center !important;
      font-size: 20px !important;
      font-weight: bold !important;
    }
    .header-calendar{
      background: #f0464c!important;
      color:white !important;
    }
    .box-day{
      border:1px solid #E3E9E5 !important;
      height:70px !important;
    }
    .box-dayoff{
      border:1px solid #E3E9E5 !important;
      height:70px !important;
      background-color: #ccd1ce !important;
    }
</style>
@endsection
@section('content')

{!! Form::open(['route' => ['programas_reportar_programa' ,$idProgramaReportar], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','id'=>'form-create']) !!}
@if(count($dataDefinitivaA)>0)
<div class="row">
      <div class="col-md-12">
        <div class="card">
        <div class="card-header  card-outline">

        </div>
          <!-- /.card-header -->
          
          <table class="table table-striped table-sm">
            @if(count($dataDefinitivaA)>0)
              @foreach ($dataDefinitivaA as $indice => $tramite)
              <tr>
                <td colspan="1" style="background-color: #5b6eba; color:white; width:10%" ><center><b>Programa</b></center></td>
                <td colspan="7" style="background-color: #fff;"><b>{{$tramite->nombre}}</b></td>
              </tr>
              <tr>
                <td colspan="1" style="background-color: #5b6eba; color:white; width:10%" ><center><b>Año Inicio</b></center></td>
                <td colspan="7" style="background-color: #fff;" >{{$tramite->ano_inicio}}</td>
              </tr>
              <tr>
                <td colspan="1" style="background-color: #5b6eba; color:white; width:10%" ><center><b>Propósito</b></center></td>
                <td colspan="7" style="background-color: #fff;">{{$tramite->proposito}}</td>
              </tr>
              @endforeach
            @endif
          
            <tr>
              <td colspan="1" style="background-color: #5b6eba; color:white;" ><center><b>I.</b></center></td>
              <td colspan="7" style="background-color: #5b6eba; color:white;" ><b><center>II.   Producción por Componente</center></b></td>
            </tr>
          
              <tr style="background-color: #013273;">
                <th nowrap style="color:white"><b><center>Componentes</center></b></th>
                <th nowrap style="color:white"><b><center>Unidad</center></b></th>
                <th nowrap style="color:white"><b><center>Efectivo {{date("Y")-2}}</center></b></th>
                <th nowrap style="color:white"><b><center>Efectivo {{date("Y")-1}}</center></b></th>
                <th nowrap style="color:white"><b><center>{{$mesReporte}}</center></b></th>               
              </tr>
            
            @if(count($dataDefinitivaB)>0)
              @foreach ($dataDefinitivaB as $indice => $tramite1)              
              {!! Form::hidden('anoc[]' , $tramite1->ano) !!}
              @if($eventoReporte)
                {!! Form::hidden('id_calendarioc[]' , $eventoReporte->id) !!}
                {!! Form::hidden('id_mesc[]' , $eventoReporte->mes) !!} 
              @endif
                <tr>
                  <td align="justify"><center>{{$tramite1->id}} - {{$tramite1->nombre}}<center></td>
                  <td align="justify"><center>{{$tramite1->unidad}}</center></td>
                  <td align="justify"><center>{{$tramite1->efectivo_1}}</center></td>
                  <td align="justify"><center>{{$tramite1->efectivo_2}}</center></td>
                  <td align="justify"><center>
                    {!! Form::hidden('id_componente[]' , $tramite1->id) !!} 
                    {!! Form::text('reporte_componente[]', null, ['class' => 'form-control text-center']) !!}
                  </center></td>
                </tr>
              @endforeach
            @endif
            
            <tr>
              <td colspan="1" style="background-color: #5b6eba; color:white;" ><center><b>III.</b></center></td>
              <td colspan="7" style="background-color: #5b6eba; color:white;" ><b><center>IV.	Indicadores de Desempeño</center></b></td>
            </tr>
            
              <tr style="background-color: #013273;">
                <th nowrap style="color:white"><center><b>Nivel</b></center></th>
                <th nowrap style="color:white"><center><b>Indicador</b></center></th>
                <th nowrap style="color:white"><b><center>Efectivo {{date("Y")-2}}</center></b></th>
                <th nowrap style="color:white"><b><center>Efectivo {{date("Y")-1}}</center></b></th>
                <th nowrap style="color:white"><b><center></center></b></th>
                <th nowrap style="color:white"><b><center>{{$mesReporte}}</center></b></th>
                <th nowrap style="color:white"><b><center></center></b></th>
              </tr>
            
            @if(count($dataDefinitivaC)>0)
                  @foreach ($dataDefinitivaC as $indice => $tramite2)
                  {!! Form::hidden('anoi[]' , $tramite2->ano) !!}
                  @if($eventoReporte)
                    {!! Form::hidden('id_calendarioi[]' , $eventoReporte->id) !!}
                    {!! Form::hidden('id_mesi[]' , $eventoReporte->mes) !!} 
                  @endif
                      <tr>
                      @if($tramite2->id_proposito == '0')
                        <td align="justify"><center>C.{{$tramite2->id_componente}}</center></td>
                      @endif
                      @if($tramite2->id_proposito == '1')
                        <td align="justify"><center>P.{{$tramite2->id_programa}}</center></td>
                      @endif
                        <td align="justify"><center>{{$tramite2->nombre}}</center></td>
                        <td align="justify"><center>{{$tramite2->efectivo_1}}</center></td>
                        <td align="justify"><center>{{$tramite2->efectivo_2}}</center></td>
                        <td align="justify"><center>
                          {!! Form::hidden('id_indicador[]' , $tramite2->id) !!} 
                          <td>{!! Form::label('numerador', 'Númerador')!!}  {!! Form::text('numerador[]', null, ['class' => 'form-control text-center']) !!} </td> 
                          <td>{!! Form::label('denominador', 'Denominador')!!} {!! Form::text('denominador[]', null, ['class' => 'form-control text-center']) !!}</td>
                        </center></td>
                      </tr>
                  @endforeach
            @endif

            <tr>
              <td colspan="1" style="background-color: #5b6eba; color:white;" ><center><b>V.</b></center></td>
              <td colspan="7" style="background-color: #5b6eba; color:white;" ><b><center>VI.	Cobertura</center></b></td>
            </tr>

            <tr style="background-color: #013273;">
              <th nowrap style="color:white"><b><center>Población</center></b></th>
              <th nowrap style="color:white"><b><center>Unidad</center></b></th>
              <th nowrap style="color:white"><b><center>Efectivo {{date("Y")-2}}</center></b></th>
              <th nowrap style="color:white"><b><center>Efectivo {{date("Y")-1}}</center></b></th>
              <th nowrap style="color:white"><b><center>Efectivo {{date("Y")}}</center></b></th>
              <th nowrap style="color:white"><b><center>{{$mesReporte}}</center></b></th>
            </tr>

            @if(count($dataDefinitivaD)>0)
              @foreach ($dataDefinitivaD as $indice => $tramite3)
              {!! Form::hidden('anop' , $tramite3->ano) !!}
              @if($eventoReporte)
                {!! Form::hidden('id_calendariop' , $eventoReporte->id) !!}
                {!! Form::hidden('id_mesp' , $eventoReporte->mes) !!}
              @endif
                <tr>
                  <td align="justify"><center>{{$tramite3->nombre}}<center></td>
                  <td align="justify"><center>{{$tramite3->unidad}}</center></td>
                  <td align="justify"><center>
                    @if(($tramite3->cod_interno != 't'))
                    {{$tramite3->efectivo_1}}
                    @endif
                    @if($tramite3->cod_interno == 't')
                    {{$tramite3->efectivo_1}}
                    @endif
                  </center></td>
                  <td align="justify"><center>
                    @if(($tramite3->cod_interno != 't'))
                    {{$tramite3->efectivo_2}}
                    @endif
                    @if($tramite3->cod_interno == 't')
                    {{$tramite3->efectivo_2}}
                    @endif
                  </center></td>
                  <td align="justify"><center>
                    @if(($tramite3->cod_interno != 't'))
                    {{$tramite3->efectivo_3}}
                    @endif
                    @if($tramite3->cod_interno == 't')
                    {{$tramite3->efectivo_3}}
                    @endif
                  </center></td>
                  <td align="justify"><center>                    
                    @if($tramite3->cod_interno == 'bnf')
                    {!! Form::hidden('id_cobertura' , $tramite3->id) !!} 
                    {!! Form::text('reporte_cobertura', null, ['class' => 'form-control text-center']) !!}
                    @endif
                    @if(($tramite3->cod_interno != 'bnf') && ($tramite3->cod_interno != 't'))
                    {{$tramite3->efectivo_3}}
                    @endif
                    @if($tramite3->cod_interno == 't')
                    
                    @endif
                  </center></td>
                </tr>
              @endforeach
            @endif            

            <tr>
              <td colspan="1" style="background-color: #5b6eba; color:white;" ><center><b>VII.</b></center></td>
              <td colspan="7" style="background-color: #5b6eba; color:white;" ><b><center>VIII.	Presupuesto (M$)</center></b></td>
            </tr>

            <tr style="background-color: #013273;">
              <th nowrap style="color:white"><b><center>Fecha</center></b></th>
              <th nowrap style="color:white"><b><center>Ley (M$)</center></b></th>
              <th nowrap style="color:white"><b><center>Vigente (M$)</center></b></th>
              <th nowrap style="color:white"><b><center>Ejecutado (M$)</center></b></th>
            </tr>

            
              @if($eventoReporte)
                {!! Form::hidden('id_calendariog' , $eventoReporte->id) !!}
                {!! Form::hidden('id_mesg' , $eventoReporte->mes) !!} 
              @endif
                <tr>
                  {!! Form::hidden('id_gasto' , $dataDefinitivaE->id) !!} 
                  {!! Form::hidden('anog' , $dataDefinitivaE->ano) !!} 
                  <td align="justify"><center>{{$mesReporte}}<center></td>
                  <td align="justify"><center>{{$dataDefinitivaE->ley}}</center></td>
                  <td ><center>{!! Form::text('reporte_vigente', null, ['class' => 'form-control text-center']) !!}</center></td>
                  <td ><center>{!! Form::text('reporte_ejecutados', null, ['class' => 'form-control text-center']) !!}</center></td>
                </tr>
               
            
          </table>
          <br><br>

        <div class="form-group row">
          {!! Form::label('observacion_rep', 'Obsevaciones del Reporte(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('observacion_rep', null, ['class' => 'form-control', 'placeholder' => 'Obsevaciones del Reporte...']) !!}
          </div>
        </div> 

      </div>
      <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! Form::submit('Guardar', ['class' => 'btn btn-success btn-sm','title'=>'Guardar']) !!}
              {!! link_to_route('programas_reportar_usuario', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) !!}
            </div>
          </div>
        </div>
  </div>
</div>
@endif
{!! Form::close() !!}
@endsection
@section('before-scripts-end')
@stop