@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-bars"></i><b> Programas Sociales - Programas por Reportar</b></h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')

<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title"><b>Programas Sociales - Programas</b></h5>


          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                      <th nowrap style="color:white">ID</th>
                        <th nowrap style="color:white">Año</th>
                        <th nowrap style="color:white">Nombre</th>
                        <th style="color:white"><center>Acciones</center></th>
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($dataDefinitiva)>0)
                  @foreach ($dataDefinitiva as $indice => $tramite)
                    <tr>
                      <td nowrap>{{$tramite->id}}</td>
                      <td nowrap>{{$tramite->ano}}</td>
                      <td>{{$tramite->nombre}}</td>
                      <td><center>
                        @if($eventoHoy && $usuarioSigc)
                          @if($eventoHoy->evento->nombre == 'Reportar' && ($eventoHoy->evento->encargado == $usuarioSigc->id || $eventoHoy->evento->suplente == $usuarioSigc->id))
                          @if($tramite->habReporte)
                          <a class="btn btn-primary" style="margin: 10px" href="{{route('programas_reportar_reportar',[$eventoHoy->id ,$tramite->id])}}">{{$eventoHoy->evento->nombre}}</a>
                          @endif
                          @if($tramite->habEnviarReporte)
                          <a class="btn btn-success" style="margin: 10px" href="{{route('programas_enviar_reporte',$tramite->id)}}">Enviar Reporte</a>
                          <a class="btn btn-warning" style="margin: 10px" href="{{route('programas_editar_reporte',[$eventoHoy->id ,$tramite->id])}}">Editar Reporte</a>
                          @endif
                          @endif
                          @if($eventoHoy->evento->nombre == 'Revisión' && ($eventoHoy->evento->encargado == $usuarioSigc->id || $eventoHoy->evento->suplente == $usuarioSigc->id))
                          @if($tramite->habResion)
                          <a class="btn btn-primary" style="margin: 10px" href="{{route('programas_revision_reporte',[$eventoHoy->id ,$tramite->id])}}">{{$eventoHoy->evento->nombre}}</a>
                          @endif
                          @if($tramite->habEnviarResion)
                          <a class="btn btn-success" style="margin: 10px" href="{{route('programas_enviar_revision_reporte',$tramite->id)}}">Enviar Revisión</a>
                          @endif
                          @endif
                          @if($eventoHoy->evento->nombre == 'Corrección' && ($eventoHoy->evento->encargado == $usuarioSigc->id || $eventoHoy->evento->suplente == $usuarioSigc->id))
                          @if($tramite->habCorregir)
                          <a class="btn btn-primary" style="margin: 10px" href="{{route('programas_correccion_reporte',[$eventoHoy->id ,$tramite->id])}}">{{$eventoHoy->evento->nombre}}</a>
                          @endif
                          @endif
                          @if($eventoHoy->evento->nombre == 'Revisión Corrección' && ($eventoHoy->evento->encargado == $usuarioSigc->id || $eventoHoy->evento->suplente == $usuarioSigc->id))
                          @if($tramite->habRevisionCorreccion)
                          <a class="btn btn-primary" style="margin: 10px" href="{{route('programas_revisarcorreccion_reporte',[$eventoHoy->id ,$tramite->id])}}">{{$eventoHoy->evento->nombre}}</a>
                          @endif
                          @endif
                        @endif
                      </center></td>
                    </tr>
                  @endforeach
                @endif
                </tbody>
              </table>
          </div>

@endsection
@section('before-scripts-end')
@stop
