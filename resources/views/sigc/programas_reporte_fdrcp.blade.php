<!DOCTYPE html>

<style>
table {
   width: 100%;
   border: 1px solid #ccc;
   border-spacing: 0 !important;
}
th, td {

   text-align: left;
   vertical-align: middle;
   border: 1px solid #ccc;
   border-spacing: 0;
   height: 60px;
}
</style>

@if(count($dataDefinitivaA)>0)
<div class="row">
      <div class="col-md-12">
        <div class="card">
        <div class="card-header  card-outline">

        </div>
          <!-- /.card-header -->
          <table class="table table-striped table-sm">
          @if(count($dataDefinitivaA)>0)
              @foreach ($dataDefinitivaA as $indice => $tramite)
              <tr>
                <td colspan="1" style="background-color: #5b6eba; color:white; width:10%" ><center><b>Programa</b></center></td>
                <td colspan="8" style="background-color: #fff;"><b>{{$tramite->nombre}}</b></td>
              </tr>
              <tr>
                <td colspan="1" style="background-color: #5b6eba; color:white; width:10%" ><center><b>Año Inicio</b></center></td>
                <td colspan="8" style="background-color: #fff;" >{{$tramite->ano_inicio}}</td>
              </tr>
              <tr>
                <td colspan="1" style="background-color: #5b6eba; color:white; width:10%" ><center><b>Propósito</b></center></td>
                <td colspan="8" style="background-color: #fff;">{{$tramite->proposito}}</td>
              </tr>
              @endforeach
            @endif
          
            <tr>
              <td colspan="1" style="background-color: #5b6eba; color:white;" ><center><b>I.</b></center></td>
              <td colspan="8" style="background-color: #5b6eba; color:white;" ><b><center>II.   Producción por Componente</center></b></td>
            </tr>
          
              <tr style="background-color: #013273;">
                <th nowrap style="color:white"><b><center>Componentes</center></b></th>
                <th nowrap colspan="2" style="color:white"><b><center>Unidad</center></b></th>
                <th nowrap style="color:white"><b><center>Efectivo {{date("Y")-2}}</center></b></th>
                <th nowrap style="color:white"><b><center>Efectivo {{date("Y")-1}}</center></b></th>
                @if(count($dataDefinitivaF)>0)
                  @foreach ($dataDefinitivaF as $indice => $tramiteF)
                    <th nowrap style="color:white"><b><center>{{$tramiteF->nombre}}</center></b></th>
                  @endforeach
                @endif
              </tr>
            
            @if(count($dataDefinitivaB)>0)
              @foreach ($dataDefinitivaB as $indice => $tramite1)
                <tr>
                  <td align="justify"><center>{{$tramite1->id}} - {{$tramite1->nombre}}<center></td>
                  <td colspan="2" align="justify"><center>{{$tramite1->unidad}}</center></td>
                  <td align="justify"><center>{{$tramite1->efectivo_1}}</center></td>
                  <td align="justify"><center>{{$tramite1->efectivo_2}}</center></td>
                  @if(count($dataDefinitivaF)>0)
                    @foreach ($dataDefinitivaF as $indice => $tramiteF)
                      <td align="justify"><center>  {{$tramite1[str_replace(' ' ,'',$tramiteF->nombre)]}}   </center></td>
                    @endforeach
                  @endif
                </tr>
              @endforeach
            @endif
            
            <tr>
              <td colspan="1" style="background-color: #5b6eba; color:white;" ><center><b>III.</b></center></td>
              <td colspan="8" style="background-color: #5b6eba; color:white;" ><b><center>IV.	Indicadores de Desempeño</center></b></td>
            </tr>
            
              <tr style="background-color: #013273;">
                <th nowrap style="color:white"><center><b>Nivel</b></center></th>
                <th nowrap colspan="2" style="color:white"><center><b>Indicador</b></center></th>
                <th nowrap style="color:white"><b><center>Efectivo {{date("Y")-2}}</center></b></th>
                <th nowrap style="color:white"><b><center>Efectivo {{date("Y")-1}}</center></b></th>
                @if(count($dataDefinitivaF)>0)
                  @foreach ($dataDefinitivaF as $indice => $tramiteF)
                    <th nowrap style="color:white"><b><center>{{$tramiteF->nombre}}</center></b></th>
                  @endforeach
                @endif
              </tr>
            
            @if(count($dataDefinitivaC)>0)
                  @foreach ($dataDefinitivaC as $indice => $tramite2)
                      <tr>
                      @if($tramite2->id_proposito == '0')
                        <td align="justify"><center>C.{{$tramite2->id_componente}}</center></td>
                      @endif
                      @if($tramite2->id_proposito == '1')
                        <td align="justify"><center>P.{{$tramite2->id_programa}}</center></td>
                      @endif
                        <td colspan="2" align="justify"><center>{{$tramite2->nombre}}</center></td>
                        <td align="justify"><center>{{$tramite2->efectivo_1}}</center></td>
                        <td align="justify"><center>{{$tramite2->efectivo_2}}</center></td>
                        @if(count($dataDefinitivaF)>0)
                          @foreach ($dataDefinitivaF as $indice => $tramiteF)
                            <td align="justify"><center>
                              {{$tramite2[str_replace(' ' ,'',$tramiteF->nombre)]}} <br>
                              {{$tramite2[str_replace(' ' ,'',$tramiteF->nombre).'_numerador']}}@if($tramite2[str_replace(' ' ,'',$tramiteF->nombre).'_numerador'])/@endif{{$tramite2[str_replace(' ' ,'',$tramiteF->nombre).'_denominador']}} 
                            </center></td>
                          @endforeach
                        @endif
                      </tr>
                  @endforeach
            @endif


            </table>    
            
            
            <div style="page-break-after: always"></div>

            
            <table class="table table-striped table-sm">
            <tr>
              <td colspan="1" style="background-color: #5b6eba; color:white;" ><center><b>V.</b></center></td>
              <td colspan="8" style="background-color: #5b6eba; color:white;" ><b><center>VI.	Cobertura</center></b></td>
            </tr>

            <tr style="background-color: #013273;">
              <th nowrap style="color:white"><b><center>Población</center></b></th>
              <th nowrap style="color:white"><b><center>Unidad</center></b></th>
              <th nowrap style="color:white"><b><center>Efectivo {{date("Y")-2}}</center></b></th>
                <th nowrap style="color:white"><b><center>Efectivo {{date("Y")-1}}</center></b></th>
                <th nowrap style="color:white"><b><center>Efectivo {{date("Y")}}</center></b></th>
                @if(count($dataDefinitivaF)>0)
                  @foreach ($dataDefinitivaF as $indice => $tramiteF)
                    <th nowrap style="color:white"><b><center>{{$tramiteF->nombre}}</center></b></th>
                  @endforeach
                @endif
            </tr>

            @if(count($dataDefinitivaD)>0)
              @foreach ($dataDefinitivaD as $indice => $tramite3)
                <tr>
                  <td align="justify"><center>{{$tramite3->nombre}}<center></td>
                  <td align="justify"><center>{{$tramite3->unidad}}</center></td>
                  <td align="justify"><center>
                  @if(($tramite3->cod_interno != 't'))
                      {{$tramite3->efectivo_1}}
                    @endif
                    @if($tramite3->cod_interno == 't')
                    {{$tramite3->efectivo_1}}
                    @endif
                  </center></td>
                  <td align="justify"><center>
                  @if(($tramite3->cod_interno != 't'))
                      {{$tramite3->efectivo_2}}
                    @endif
                    @if($tramite3->cod_interno == 't')
                    {{$tramite3->efectivo_2}}
                    @endif
                  </center></td>
                  <td align="justify"><center>
                  @if(($tramite3->cod_interno != 't'))
                      {{$tramite3->efectivo_3}}
                    @endif
                    @if($tramite3->cod_interno == 't')
                    {{$tramite3->efectivo_3}}
                    @endif
                  </center></td>
                  @if(count($dataDefinitivaF)>0)
                    @foreach ($dataDefinitivaF as $indice => $tramiteF)
                      <td align="justify"><center>
                      @if($tramite3->cod_interno == 'bnf')  
                        {{$tramite3[str_replace(' ' ,'',$tramiteF->nombre).'_numeradorp']}}   
                      @endif
                      @if(($tramite3->cod_interno != 'bnf') && ($tramite3->cod_interno != 't'))
                        {{$tramite3->efectivo_3}}
                      @endif
                      @if($tramite3->cod_interno == 't')
                        {{$tramite3[str_replace(' ' ,'',$tramiteF->nombre).'_resultadop']}} 
                      @endif
                      </center></td>
                    @endforeach
                  @endif
                </tr>
              @endforeach
            @endif            
            <tr>
              <td colspan="1" style="background-color: #5b6eba; color:white;" ><center><b>VII.</b></center></td>
              <td colspan="8" style="background-color: #5b6eba; color:white;" ><b><center>VIII.	Presupuesto (M$)</center></b></td>
            </tr>

            <tr style="background-color: #013273;">
              <th nowrap style="color:white"><b><center>Fecha</center></b></th>
              <th nowrap colspan="2" style="color:white"><b><center>Ley (M$)</center></b></th>
              <th nowrap colspan="2" style="color:white"><b><center>Vigente (M$)</center></b></th>
              <th nowrap colspan="2" style="color:white"><b><center>Ejecutado (M$)</center></b></th>
              <th nowrap colspan="2" style="color:white"><b><center>% Ejecución</center></b></th>
            </tr>

            @if(count($dataDefinitivaE3)>0) 
              @foreach ($dataDefinitivaE3 as $indice => $tramite5)
                <tr>
                <td align="justify"><center>{{$tramite5->fecha}}</center></td>
                <td colspan="2" align="justify"><center>{{$tramite5->ley}}</center></td> 
                <td colspan="2" align="justify"><center>{{$tramite5->vigente}}</center></td> 
                <td colspan="2" align="justify"><center>{{$tramite5->ejecutados}}</center></td> 
                <td colspan="2" align="justify"><center>{{$tramite5->ejecucion}}%</center></td> 
                </tr>
              @endforeach
            @endif
            @if(count($dataDefinitivaF)>0)
              @foreach ($dataDefinitivaF as $indice => $tramiteF)
              <tr>
                <td align="justify"><center>{{$tramiteF->nombre}}</center></td>
                <td colspan="2" align="justify"><center>{{$dataDefinitivaE2->ley}}</center></td>  
                @if(count($dataDefinitivaE)>0)
                  @foreach ($dataDefinitivaE as $indice => $tramite4)
                    <td colspan="2"  align="justify"><center>{{$tramite4[str_replace(' ' ,'',$tramiteF->nombre).'_vigente']}}</center></td>
                    <td colspan="2"  align="justify"><center>{{$tramite4[str_replace(' ' ,'',$tramiteF->nombre).'_ejecutados']}}</center></td>
                    <td colspan="2"  align="justify"><center>{{$tramite4[str_replace(' ' ,'',$tramiteF->nombre).'_ejecucion']}}@if($tramite4[str_replace(' ' ,'',$tramiteF->nombre).'_ejecucion'])%@endif</center></td>
                  @endforeach
                @endif 
              </tr>
              @endforeach
            @endif

            <tr>
              <th colspan="2" align="justify" style="background-color: #013273; color:white"><center>Observación del Reporte</center></th>
              <td colspan="7" align="justify"><center>{{$observacionReporte}}</center></td>
            </tr>
            <tr>
              <th colspan="2" align="justify" style="background-color: #013273; color:white"><center>Observación de Revisión</center></th>
              <td colspan="7" align="justify"><center>{{$observacion}}</center></td>
            </tr>
          </table>
          
      </div>
  </div>
</div>
@endif




