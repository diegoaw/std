@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-users"></i><b> Programas Sociales - Revisión Reporte</b></h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
<style>
.header-col{
      background: #E3E9E5 !important;
      color:#536170 !important;
      text-align: center !important;
      font-size: 20px !important;
      font-weight: bold !important;
    }
    .header-calendar{
      background: #f0464c!important;
      color:white !important;
    }
    .box-day{
      border:1px solid #E3E9E5 !important;
      height:70px !important;
    }
    .box-dayoff{
      border:1px solid #E3E9E5 !important;
      height:70px !important;
      background-color: #ccd1ce !important;
    }
</style>
@endsection
@section('content')

{!! Form::open(['route' => ['programas_revisado_reporte' , $idProgramaReportar], 'files' => true, 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','id'=>'form-create']) !!}

@if(count($dataDefinitivaA)>0)
<div class="row">
      <div class="col-md-12">
        <div class="card">
        <div class="card-header  card-outline">

        </div>
          <!-- /.card-header -->
          <table class="table table-striped table-sm">
          @if(count($dataDefinitivaA)>0)
              @foreach ($dataDefinitivaA as $indice => $tramite)
              <tr>
                <td colspan="1" style="background-color: #5b6eba; color:white; width:10%" ><center><b>Programa</b></center></td>
                <td colspan="8" style="background-color: #fff;"><b>{{$tramite->nombre}}</b></td>
              </tr>
              <tr>
                <td colspan="1" style="background-color: #5b6eba; color:white; width:10%" ><center><b>Año Inicio</b></center></td>
                <td colspan="8" style="background-color: #fff;" >{{$tramite->ano_inicio}}</td>
              </tr>
              <tr>
                <td colspan="1" style="background-color: #5b6eba; color:white; width:10%" ><center><b>Propósito</b></center></td>
                <td colspan="8" style="background-color: #fff;">{{$tramite->proposito}}</td>
              </tr>
              @endforeach
            @endif
          
            <tr>
              <td colspan="1" style="background-color: #5b6eba; color:white;" ><center><b>I.</b></center></td>
              <td colspan="8" style="background-color: #5b6eba; color:white;" ><b><center>II.   Producción por Componente</center></b></td>
            </tr>
          
              <tr style="background-color: #013273;">
                <th nowrap style="color:white"><b><center>Componentes</center></b></th>
                <th nowrap colspan="2" style="color:white"><b><center>Unidad</center></b></th>
                <th nowrap style="color:white"><b><center>Efectivo {{date("Y")-2}}</center></b></th>
                <th nowrap style="color:white"><b><center>Efectivo {{date("Y")-1}}</center></b></th>
                 @if(count($dataDefinitivaF)>0)
                  @foreach ($dataDefinitivaF as $indice => $tramiteF)
                    <th nowrap style="color:white"><b><center>{{$tramiteF->nombre}}</center></b></th>
                  @endforeach
                @endif
              </tr>
              
            @if(count($dataDefinitivaB)>0)
              @foreach ($dataDefinitivaB as $indice => $tramite1)           
                <tr>
                  <td align="justify"><center>{{$tramite1->id}} - {{$tramite1->nombre}}<center></td>
                  <td colspan="2" align="justify"><center>{{$tramite1->unidad}}</center></td>
                  <td align="justify"><center>{{$tramite1->efectivo_1}}</center></td>
                  <td align="justify"><center>{{$tramite1->efectivo_2}}</center></td>
                  @if(count($dataDefinitivaF)>0)
                    @foreach ($dataDefinitivaF as $indice => $tramiteF)
                      <td align="justify"><center>  {{$tramite1[str_replace(' ' ,'',$tramiteF->nombre)]}}   </center></td>
                    @endforeach
                  @endif
                </tr>
              @endforeach
            @endif
            
            <tr>
              <td colspan="1" style="background-color: #5b6eba; color:white;" ><center><b>III.</b></center></td>
              <td colspan="8" style="background-color: #5b6eba; color:white;" ><b><center>IV.	Indicadores de Desempeño</center></b></td>
            </tr>
            
              <tr style="background-color: #013273;">
                <th nowrap style="color:white"><center><b>Nivel</b></center></th>
                <th nowrap colspan="2" style="color:white"><center><b>Indicador</b></center></th>
                <th nowrap style="color:white"><b><center>Efectivo {{date("Y")-2}}</center></b></th>
                <th nowrap style="color:white"><b><center>Efectivo {{date("Y")-1}}</center></b></th>
                @if(count($dataDefinitivaF)>0)
                  @foreach ($dataDefinitivaF as $indice => $tramiteF)
                    <th nowrap style="color:white"><b><center>{{$tramiteF->nombre}}</center></b></th>
                  @endforeach
                @endif
              </tr>
            
            @if(count($dataDefinitivaC)>0)
                  @foreach ($dataDefinitivaC as $indice => $tramite2)
                      <tr>
                      @if($tramite2->id_proposito == '0')
                        <td align="justify"><center>C.{{$tramite2->id_componente}}</center></td>
                      @endif
                      @if($tramite2->id_proposito == '1')
                        <td align="justify"><center>P.{{$tramite2->id_programa}}</center></td>
                      @endif
                        <td colspan="2" align="justify"><center>{{$tramite2->nombre}}</center></td>
                        <td align="justify"><center>{{$tramite2->efectivo_1}}</center></td>
                        <td align="justify"><center>{{$tramite2->efectivo_2}}</center></td>
                        @if(count($dataDefinitivaF)>0)
                          @foreach ($dataDefinitivaF as $indice => $tramiteF)
                            <td align="justify"><center> 
                            {{$tramite2[str_replace(' ' ,'',$tramiteF->nombre)]}} <br>
                            {{$tramite2[str_replace(' ' ,'',$tramiteF->nombre).'_numerador']}}@if($tramite2[str_replace(' ' ,'',$tramiteF->nombre).'_numerador'])/@endif{{$tramite2[str_replace(' ' ,'',$tramiteF->nombre).'_denominador']}}
                            </center></td>
                          @endforeach
                        @endif
                      </tr>
                  @endforeach
            @endif

            <tr>
              <td colspan="1" style="background-color: #5b6eba; color:white;" ><center><b>V.</b></center></td>
              <td colspan="8" style="background-color: #5b6eba; color:white;" ><b><center>VI.	Cobertura</center></b></td>
            </tr>

            <tr style="background-color: #013273;">
              <th nowrap style="color:white"><b><center>Población</center></b></th>
              <th nowrap style="color:white"><b><center>Unidad</center></b></th>
              <th nowrap style="color:white"><b><center>Efectivo {{date("Y")-2}}</center></b></th>
              <th nowrap style="color:white"><b><center>Efectivo {{date("Y")-1}}</center></b></th>
              <th nowrap style="color:white"><b><center>Efectivo {{date("Y")}}</center></b></th>
              @if(count($dataDefinitivaF)>0)
                @foreach ($dataDefinitivaF as $indice => $tramiteF)
                  <th nowrap style="color:white"><b><center>{{$tramiteF->nombre}}</center></b></th>
                @endforeach
              @endif
            </tr>

            @if(count($dataDefinitivaD)>0)
              @foreach ($dataDefinitivaD as $indice => $tramite3)
                <tr>
                  <td align="justify"><center>{{$tramite3->nombre}}<center></td>
                  <td align="justify"><center>{{$tramite3->unidad}}</center></td>
                  <td align="justify"><center>
                  @if(($tramite3->cod_interno != 't'))
                      {{$tramite3->efectivo_1}}
                    @endif
                    @if($tramite3->cod_interno == 't')
                    {{$tramite3->efectivo_1}}
                    @endif
                  </center></td>
                  <td align="justify"><center>
                  @if(($tramite3->cod_interno != 't'))
                      {{$tramite3->efectivo_2}}
                    @endif
                    @if($tramite3->cod_interno == 't')
                    {{$tramite3->efectivo_2}}
                    @endif
                  </center></td>
                  <td align="justify"><center>
                  @if(($tramite3->cod_interno != 't'))
                      {{$tramite3->efectivo_3}}
                    @endif
                    @if($tramite3->cod_interno == 't')
                    {{$tramite3->efectivo_3}}
                    @endif
                  </center></td>
                  @if(count($dataDefinitivaF)>0)
                    @foreach ($dataDefinitivaF as $indice => $tramiteF)
                    <td align="justify"><center>  
                      @if($tramite3->cod_interno == 'bnf')
                      {{$tramite3[str_replace(' ' ,'',$tramiteF->nombre).'_numeradorp']}}   
                      @endif
                      @if(($tramite3->cod_interno != 'bnf') && ($tramite3->cod_interno != 't'))
                      {{$tramite3->efectivo_3}}
                      @endif
                      @if($tramite3->cod_interno == 't')
                      {{$tramite3[str_replace(' ' ,'',$tramiteF->nombre).'_resultadop']}} 
                      @endif
                    </center></td>
                    @endforeach
                  @endif
                </tr>
              @endforeach
            @endif            

            <tr>
              <td colspan="1" style="background-color: #5b6eba; color:white;" ><center><b>VII.</b></center></td>
              <td colspan="8" style="background-color: #5b6eba; color:white;" ><b><center>VIII.	Presupuesto (M$)</center></b></td>
            </tr>

            <tr style="background-color: #013273;">
              <th nowrap style="color:white"><b><center>Fecha</center></b></th>
              <th nowrap colspan="2" style="color:white"><b><center>Ley (M$)</center></b></th>
              <th nowrap colspan="2" style="color:white"><b><center>Vigente (M$)</center></b></th>
              <th nowrap colspan="2" style="color:white"><b><center>Ejecutado (M$)</center></b></th>
              <th nowrap colspan="2" style="color:white"><b><center>% Ejecución</center></b></th>
            </tr>

            @if($eventoReporte)
                {!! Form::hidden('id_calendariog' , $eventoReporte->id) !!}
                {!! Form::hidden('id_mesg' , $eventoReporte->mes) !!} 
              @endif
                
                  {!! Form::hidden('id_gasto' , $dataDefinitivaE2->id) !!} 
                  {!! Form::hidden('anog' , $dataDefinitivaE2->ano) !!} 
                  @if(count($dataDefinitivaF)>0)
                  @foreach ($dataDefinitivaF as $indice => $tramiteF)
                  <tr>
                    <td align="justify"><center>{{$tramiteF->nombre}}</center></td>
                    <td colspan="2" align="justify"><center>{{$dataDefinitivaE2->ley}}</center></td>  
                    @if(count($dataDefinitivaE)>0)
                      @foreach ($dataDefinitivaE as $indice => $tramite4)
                      {!! Form::hidden('id_gastos[]' , $tramite4->id) !!} 
                        <td colspan="2"  align="justify"><center>{{$tramite4[str_replace(' ' ,'',$tramiteF->nombre).'_vigente']}}</center></td>
                        <td colspan="2"  align="justify"><center>{{$tramite4[str_replace(' ' ,'',$tramiteF->nombre).'_ejecutados']}}</center></td>
                        <td colspan="2"  align="justify"><center>{{$tramite4[str_replace(' ' ,'',$tramiteF->nombre).'_ejecucion']}}</center></td>
                      @endforeach
                    @endif 
                  </tr>
                  @endforeach
                @endif
           
          </table>
          <br><br>
          <div class="form-group row">
            {!! Form::label('observacion_rep', 'Obsevaciones del Reportador', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
              {!! Form::textarea('observacion_rep', $observacionReporte, ['class' => 'form-control', 'disabled'=>'disabled', 'placeholder' => 'Obsevaciones del Reporte...']) !!}
            </div>
          </div> 
      
          <div class="form-group row">
            {!! Form::label('id_estado', 'Estado(*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
            {!! Form::select('id_estado',$selectEstado,null,['id'=>'id_estado','name'=>'id_estado', 'class'=>'form-control']) !!}
            </div>
          </div>

          <div class="form-group row">
            {!! Form::label('observacion', 'Obsevaciones de la Revisión(*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
              {!! Form::textarea('observacion', null, ['class' => 'form-control', 'placeholder' => 'Obsevaciones de la Revisión...']) !!}
              {!! Form::hidden('id_calendario' , $eventoReporte->id) !!}
              {!! Form::hidden('id_mes' , $eventoReporte->meses->id) !!}
            </div>
          </div> 

          <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! Form::submit('Guardar', ['class' => 'btn btn-success btn-sm','title'=>'Guardar']) !!}
              {!! link_to_route('programas_reportar_usuario', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) !!}
            </div>
          </div>
        </div>
      
      </div>
  </div>
</div>
@endif
{!! Form::close() !!}
@endsection
@section('before-scripts-end')
@stop