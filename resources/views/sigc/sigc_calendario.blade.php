@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-calendar-check-o"></i> SIGC - Calendario</h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
<style>
.header-col{
      background: #E3E9E5 !important;
      color:#536170 !important;
      text-align: center !important;
      font-size: 20px !important;
      font-weight: bold !important;
    }
    .header-calendar{
      background: #f0464c!important;
      color:white !important;
    }
    .box-day{
      border:1px solid #E3E9E5 !important;
      height:70px !important;
    }
    .box-dayoff{
      border:1px solid #E3E9E5 !important;
      height:70px !important;
      background-color: #ccd1ce !important;
    }
</style>
@endsection
@section('content')

@if (auth()->user()->hasPermission(['09-04']))
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title"><b>SIGC - Nuevo Calendario</b></h5>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">

                  <div class="col-sm-12">
                  <div class="text-center">
                    <a class="btn btn-primary" href="{{route('sigc_calendario_crear')}}">Nuevo Calendario</a>
                  </div>
                  </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endif

@if(count($dataDefinitivaA)>0)

<div class="row">
      <div class="col-md-12">

        <div class="card">
          <div class="card-header card-danger card-outline">

            <h5 class="card-title"><b>SIGC - Calendario Año {{date("Y")}}</b></h5>

          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th  style="color:white">Año</th>
                        <th  style="color:white">Mes</th>
                        <th  style="color:white">Reporta Desde</th>
                        <th  style="color:white">Reporta Hasta</th>
                        <th  style="color:white">Revisión Desde</th>
                        <th  style="color:white">Revisión Hasta</th>
                        <th  style="color:white">Corrección Desde</th>
                        <th  style="color:white">Corrección Hasta</th>
                        <th  style="color:white">Revisión Corrección Desde</th>
                        <th  style="color:white">Revisión Corrección Hasta</th>
                        @if (auth()->user()->hasPermission(['09-03']))
                        <th  style="color:white">Editar</th>
                        @endif
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($dataDefinitivaA)>0)
                         @foreach ($dataDefinitivaA as $indice => $tramite)
                          <tr>
                           <td nowrap>{{$tramite->ano}}</td>
                           <td nowrap>{{$tramite->mes}}</td>
                           <td nowrap>{{date("d-m-Y", strtotime($tramite->rep_desde)) }}</td>
                           <td nowrap>{{ date("d-m-Y", strtotime($tramite->rep_hasta)) }}</td>
                           <td nowrap>{{  date("d-m-Y", strtotime($tramite->rev_desde))}}</td>
                           <td nowrap>{{ date("d-m-Y", strtotime($tramite->rev_hasta))}}</td>
                           <td nowrap>{{ date("d-m-Y", strtotime($tramite->corrige_desde))}}</td>
                           <td nowrap>{{ date("d-m-Y", strtotime($tramite->corrige_hasta))}}</td>
                           <td nowrap>{{ date("d-m-Y", strtotime($tramite->rev_correccion_desde))}}</td>
                           <td nowrap>{{ date("d-m-Y", strtotime($tramite->rev_correccion_hasta))}}</td>
                           @if (auth()->user()->hasPermission(['09-03']))
                           <td><center>
                            <a class="btn accion btn-success accion" title="Editar" href="{{route('sigc_calendario_editar',$tramite->id)}}"><i class="fa fa-pencil"></i></a>
                           </center></td>
                           @endif
                          </tr>
                          @endforeach
                @endif
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
@endif

@if(count($dataDefinitivaO)>0)
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title"><b>SIGC - Calendario 2021 y Años Anteriores</b></h5>

          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th  style="color:white">Año</th>
                        <th  style="color:white">Mes</th>
                        <th  style="color:white">Reporta Desde</th>
                        <th  style="color:white">Reporta Hasta</th>
                        <th  style="color:white">Revisión Desde</th>
                        <th  style="color:white">Revisión Hasta</th>
                        <th  style="color:white">Corrección Desde</th>
                        <th  style="color:white">Corrección Hasta</th>
                        <th  style="color:white">Revisión Corrección Desde</th>
                        <th  style="color:white">Revisión Corrección Hasta</th>
                        @if (auth()->user()->hasPermission(['09-03']))
                        <th  style="color:white">Editar</th>
                        @endif
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($dataDefinitivaO)>0)
                         @foreach ($dataDefinitivaO as $indice2 => $tramite1)
                          <tr>
                           <td nowrap>{{$tramite1->ano}}</td>
                           <td nowrap>{{$tramite1->mes}}</td>
                           <td nowrap>{{$tramite1->rep_desde}}</td>
                           <td nowrap>{{$tramite1->rep_hasta}}</td>
                           <td nowrap>{{$tramite1->rev_desde}}</td>
                           <td nowrap>{{$tramite1->rev_hasta}}</td>
                           <td nowrap>{{$tramite1->corrige_desde}}</td>
                           <td nowrap>{{$tramite1->corrige_hasta}}</td>
                           <td nowrap>{{$tramite1->rev_correccion_desde}}</td>
                           <td nowrap>{{$tramite1->rev_correccion_hasta}}</td>
                           @if (auth()->user()->hasPermission(['09-03']))
                           <td><center>
                            <a class="btn accion btn-success accion" title="Editar" href="{{route('sigc_calendario_editar',$tramite1->id)}}"><i class="fa fa-pencil"></i></a>
                           </center></td>
                           @endif
                          </tr>
                          @endforeach
                @endif
                </tbody>
              </table>
          </div>

          <div class="col-sm-12">
                <div class="pull-right">

                  {{ $dataDefinitivaO->links() }}

                </div>
          </div>

        </div>
      </div>
    </div>
@endif



@endsection
@section('before-scripts-end')
@stop
