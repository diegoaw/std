@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-calendar"></i> SIGC - Editar Calendario</h1><br>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
{!! Form::open(['route' => ['sigc_calendario_update' , $editCalendario->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','id'=>'form-create']) !!}
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-primary card-outline">
        <h3 class="card-title">Editar Calendario</h3>
      </div>
      <div class="card-body">

                  <div class="form-group row">
                    <div class="col-sm-6 offset-sm-3">
                      @include('includes.messages')
                    </div>
                  </div>

                  <div class="callout col-sm-8 offset-sm-2" style="color: #737373;">
                    <h4 style="font-size: 14px;"><i class="fa fa-info-circle" style="color: #ff0000;"></i> Nota:</h4>
                    <span>Los campos marcados con un asterisco (*) son obligatorios.</span>
                  </div><br>

                  <div class="form-group row">
                    {!! Form::label('Año', 'Año(*)', ['class' => 'col-sm-3 col-form-label']) !!}
                    <div class="col-sm-6">
                    {!! Form::select('ano',$selectAno,$editCalendario->ano,['id'=>'ano','name'=>'ano', 'class'=>'form-control']) !!}
                    </div>
                  </div>

                  <div class="form-group row">
                    {!! Form::label('Mes', 'Mes(*)', ['class' => 'col-sm-3 col-form-label']) !!}
                    <div class="col-sm-6">
                    {!! Form::select('mes',$selectMes,$editCalendario->mes,['id'=>'mes','name'=>'mes', 'class'=>'form-control']) !!}
                    </div>
                  </div>

                  <br> <br>
                  <div class="form-group row" >
                  <div class="col-sm-2"></div>
                  <h5><b>Fechas Reportar</b></h5>
                  </div>

                  <div class="form-group row" >
                    <div class="col-sm-2"></div>
                      <div class="form-group row {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                      {!!Form::label('FechaDesde', 'Reportar Desde', array('class' => 'col-sm-6 col-form-label'))!!}
                      <div class="col-sm-6">
                        <div class="input-group date">
                          <input type="text" class="form-control group-date" id="FechaReporteDesde" name="FechaReporteDesde" readonly="readonly" value="{!! date('Y-m-d', strtotime($editCalendario->rep_desde))  !!}">
                          <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                 <div class="form-group row">
                    <div class="col-sm-2"></div>
                      <div class="form-group row {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                      {!!Form::label('FechaHasta', 'Reportar Hasta', array('class' => 'col-sm-6 col-form-label'))!!}
                      <div class="col-sm-6">
                        <div class="input-group date">
                          <input type="text" class="form-control group-date" id="FechaReporteHasta" name="FechaReporteHasta" readonly="readonly" value="{!! date('Y-m-d', strtotime($editCalendario->rep_hasta))  !!}">
                          <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <br> <br>
                  <div class="form-group row" >
                  <div class="col-sm-2"></div>
                  <h5><b>Fechas Revisión</b></h5>
                  </div>



                  <div class="form-group row">
                    <div class="col-sm-2"></div>
                      <div class="form-group row {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                      {!!Form::label('FechaDesde', 'Revisión Desde', array('class' => 'col-sm-6 col-form-label'))!!}
                      <div class="col-sm-6">
                        <div class="input-group date">
                          <input type="text" class="form-control group-date" id="FechaRevisonDesde" name="FechaRevisonDesde" readonly="readonly" value="{!! date('Y-m-d', strtotime($editCalendario->rev_desde))  !!}">
                          <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                          </div>
                        </div>
                      </div>
                    </div>
                    </div>

                 <div class="form-group row">
                    <div class="col-sm-2"></div>
                      <div class="form-group row {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                      {!!Form::label('FechaHasta', 'Revisión Hasta', array('class' => 'col-sm-6 col-form-label'))!!}
                      <div class="col-sm-6">
                        <div class="input-group date">
                          <input type="text" class="form-control group-date" id="FechaRevisonHasta" name="FechaRevisonHasta" readonly="readonly" value="{!! date('Y-m-d', strtotime($editCalendario->rev_hasta))  !!}">
                          <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <br> <br>
                  <div class="form-group row" >
                  <div class="col-sm-2"></div>
                  <h5><b>Fechas Corrección</b></h5>
                  </div>


                  <div class="form-group row">
                    <div class="col-sm-2"></div>
                      <div class="form-group row {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                      {!!Form::label('FechaDesde', 'Corrección Desde', array('class' => 'col-sm-6 col-form-label'))!!}
                      <div class="col-sm-6">
                        <div class="input-group date">
                          <input type="text" class="form-control group-date" id="FechaCorreccionDesde" name="FechaCorreccionDesde" readonly="readonly" value="{!! date('Y-m-d', strtotime($editCalendario->corrige_desde))  !!}">
                          <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                          </div>
                        </div>
                      </div>
                    </div>
                    </div>

                 <div class="form-group row">
                    <div class="col-sm-2"></div>
                      <div class="form-group row {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                      {!!Form::label('FechaHasta', 'Corrección Hasta', array('class' => 'col-sm-6 col-form-label'))!!}
                      <div class="col-sm-6">
                        <div class="input-group date">
                          <input type="text" class="form-control group-date" id="FechaCorreccionHasta" name="FechaCorreccionHasta" readonly="readonly" value="{!! date('Y-m-d', strtotime($editCalendario->corrige_hasta))  !!}">
                          <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <br> <br>
                  <div class="form-group row" >
                  <div class="col-sm-2"></div>
                  <h5><b>Fechas Revisión Corrección</b></h5>
                  </div>



                  <div class="form-group row">
                    <div class="col-sm-2"></div>
                      <div class="form-group row {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                      {!!Form::label('FechaDesde', 'Revisión Corrección Desde', array('class' => 'col-sm-6 col-form-label'))!!}
                      <div class="col-sm-6">
                        <div class="input-group date">
                          <input type="text" class="form-control group-date" id="FechaRevisionCorreccionDesde" name="FechaRevisionCorreccionDesde" readonly="readonly" value="{!! date('Y-m-d', strtotime($editCalendario->rev_correccion_desde))  !!}">
                          <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                          </div>
                        </div>
                      </div>
                      </div>
                      </div>

                 <div class="form-group row">
                    <div class="col-sm-2"></div>
                      <div class="form-group row {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                      {!!Form::label('FechaHasta', 'Revisión Corrección Hasta', array('class' => 'col-sm-6 col-form-label'))!!}
                      <div class="col-sm-6">
                        <div class="input-group date">
                          <input type="text" class="form-control group-date" id="FechaRevisionCorreccionHasta" name="FechaRevisionCorreccionHasta" readonly="readonly" value="{!! date('Y-m-d', strtotime($editCalendario->rev_correccion_hasta))  !!}">
                          <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <br> <br>

                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="text-center">
                        {!! Form::submit('Guardar', ['class' => 'btn btn-success btn-sm','title'=>'Guardar']) !!}
                        {!! link_to_route('sigc_calendario', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) !!}
                      </div>
                    </div>
                  </div>
      </div>
    </div>
  </div>
</div>
  {!! Form::close() !!}
  @stop

@section('after-scripts-end')
@include('includes.scripts.script_fecha.scripts_filtro_fecha')
@endsection
