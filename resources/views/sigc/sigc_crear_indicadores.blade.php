@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-list"></i> SIGC - Crear Indicador</h1><br>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
{!! Form::open(['route' => 'sigc_indicadores_guardar', 'files' => true, 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','id'=>'form-create']) !!}
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-danger card-outline">
        <h3 class="card-title">Crear Indicador</h3>
      </div>
      <div class="card-body">

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>

        <div class="callout col-sm-8 offset-sm-2" style="color: #737373;">
          <h4 style="font-size: 14px;"><i class="fa fa-info-circle" style="color: #ff0000;"></i> Nota:</h4>
          <span>Los campos marcados con un asterisco (*) son obligatorios.</span>
        </div><br>

         <div class="form-group row">
          {!! Form::label('Año', 'Año(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('ano',$selectAno, $varNull, ['id'=>'ano','name'=>'ano', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Nombre', 'Nombre(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Nombre']) !!}
          </div>
        </div>

         <div class="form-group row">
          {!! Form::label('Tipo', 'Tipo(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('tipo',$selectTipo, $varNull , ['id'=>'tipo','name'=>'tipo', 'class'=>'form-control']) !!}
          </div>
          </div>

         <div class="form-group row">
          {!! Form::label('Numerador', 'Numerador(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('numerador', null, ['class' => 'form-control', 'placeholder' => 'Ejemplo :  "Registros / contratos firmados / actividades realizadas"']) !!}
          </div>
        </div>

         <div class="form-group row">
          {!! Form::label('Categoria', 'Categoria(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('categoria',$selectCategoria, $varNull, ['id'=>'categoria','name'=>'categoria', 'class'=>'form-control']) !!}
          </div>
        </div>

         <div class="form-group row" id="denominadordiv">
          {!! Form::label('Denominador', 'Denominador', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('denominador', null, ['id'=>'denominador', 'name'=>'denominador' , 'class' => 'form-control', 'placeholder' => ' Ingrese en número el denominador , es decir el resultado esperable anual , Ejemplo ;85 ']) !!}
          </div>
        </div>

         <div class="form-group row">
          {!! Form::label('Multiplicador', 'Multiplicador', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('multiplicador', null, ['class' => 'form-control', 'placeholder' =>' Ingrese en número el multiplicador correspondienete, Ejemplo 100 ']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Ponderador', 'Ponderador', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('ponderador', null, ['class' => 'form-control', 'placeholder' => ' Ingrese en número el ponderador correspondienete, Ejemplo 25 ']) !!}
          </div>
        </div>



        <div class="form-group row">
          {!! Form::label('Meta', 'Meta', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('meta', null, ['class' => 'form-control', 'placeholder' =>' Ingrese en número el meta correspondienete, Ejemplo 100 ']) !!}
          </div>
        </div>

        <div class="form-group row">
         {!! Form::label('Fórmula de Cálculo', 'Fórmula de Cálculo(*)', ['class' => 'col-sm-3 col-form-label']) !!}
         <div class="col-sm-6">
           {!! Form::select('formula',$selectFormula, $varNull , ['id'=>'formula','name'=>'formula', 'class'=>'form-control']) !!}
         </div>
         </div>


        <div class="form-group row">
          {!! Form::label('Unidad', 'Unidad(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('unidad',$selectUnidad, $varNull , ['id'=>'unidad','name'=>'unidad', 'class'=>'form-control']) !!}
          </div>
          </div>

        <div class="form-group row">
          {!! Form::label('Medio Verificacion', 'Medio Verificacion', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('medio_verificacion', null, ['class' => 'form-control', 'placeholder' =>' Escriba los medios de verificacion que corresponden. Ejemplo: "Correo , informe em archivo word/excel , etc "  ']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Notas', 'Notas', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('notas', null, ['class' => 'form-control', 'placeholder' =>' Escriba las notas descriptivas relacionadas con el indicador . Ejemplo : "El plan de Auditoría consiste en la realizacion de catorce auditorías"']) !!}
          </div>
        </div>

         <div class="form-group row">
          {!! Form::label('Producto', 'Producto(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('producto',$selectProducto, $varNull , ['id'=>'producto','name'=>'producto', 'class'=>'form-control']) !!}
          </div>
          </div>

         <div class="form-group row">
          {!! Form::label('Ambito', 'Ambito(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('ambito',$selectAmbito, $varNull , ['id'=>'ambito','name'=>'ambito', 'class'=>'form-control']) !!}
          </div>
          </div>

           <div class="form-group row">
          {!! Form::label('Dimension', 'Dimension(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('dimension',$selectDimension, $varNull , ['id'=>'dimension','name'=>'dimension', 'class'=>'form-control']) !!}
          </div>
          </div>

           <div class="form-group row">
          {!! Form::label('Equipo', 'Equipo(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('equipo',$selectEquipo, $varNull , ['id'=>'equipo','name'=>'equipo', 'class'=>'form-control']) !!}
          </div>
          </div>

          <div class="form-group row">
           {!! Form::label('División', 'División(*)', ['class' => 'col-sm-3 col-form-label']) !!}
           <div class="col-sm-6">
             {!! Form::select('divisionId',$selectDivision, $varNull , ['id'=>'divisionId','name'=>'divisionId', 'class'=>'form-control']) !!}
           </div>
           </div>

          <div class="form-group row">
           {!! Form::label('Reportador', 'Reportador(*)', ['class' => 'col-sm-3 col-form-label']) !!}
           <div class="col-sm-6">
             {!! Form::select('reportertId',$selectUsuariosid, $varNull , ['id'=>'reportertId','name'=>'reportertId', 'class'=>'form-control']) !!}
           </div>
         </div>

         <div class="form-group row">
          {!! Form::label('Suplente Reportador', 'Suplente Reportador', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('reportersId',$selectUsuariosid, $varNull , ['id'=>'reportersId','name'=>'reportersId', 'class'=>'form-control']) !!}
          </div>
        </div>


        <div class="form-group row">
        {!! Form::label('Revisor', 'Revisor(*)', ['class' => 'col-sm-3 col-form-label']) !!}
        <div class="col-sm-6">
          {!! Form::select('revisorId',$selectUsuariosid, $varNull , ['id'=>'revisorId','name'=>'revisorId', 'class'=>'form-control']) !!}
        </div>
      </div>


        <div class="form-group row">
         {!! Form::label('Suplente Revisor', 'Suplente Revisor', ['class' => 'col-sm-3 col-form-label']) !!}
         <div class="col-sm-6">
           {!! Form::select('responsabletId',$selectUsuariosid, $varNull , ['id'=>'responsabletId','name'=>'responsabletId', 'class'=>'form-control']) !!}
         </div>
       </div>




        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! Form::submit('Guardar', ['class' => 'btn btn-success btn-sm','title'=>'Guardar']) !!}
              {!! link_to_route('sigc_indicadores', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) !!}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{!! Form::close() !!}
@stop
@section('after-scripts-end')


<script>
$('#categoria').on('change', function(){
    var cat = $(this).val();
    if(cat == 1){
      $('#denominadordiv').show();
      }else{
        if(cat == 2){
           $('#denominadordiv').hide();
        }
      }
});

</script>
@endsection
