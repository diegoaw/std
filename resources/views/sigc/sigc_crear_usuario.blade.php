@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-user"></i> SIGC - Crear Usuario</h1><br>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
{!! Form::open(['route' => 'sigc_nuevo_usuario', 'files' => true, 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','id'=>'form-create']) !!}
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-danger card-outline">
        <h3 class="card-title">Crear Usuario</h3>
      </div>
      <div class="card-body">

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>

        <div class="callout col-sm-8 offset-sm-2" style="color: #737373;">
          <h4 style="font-size: 14px;"><i class="fa fa-info-circle" style="color: #ff0000;"></i> Nota:</h4>
          <span>Los campos marcados con un asterisco (*) son obligatorios.</span>
        </div><br>

        <div class="form-group row">
          {!! Form::label('Usuarios', 'Seleccione Usuario(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
          {!! Form::select('user',$selectUser,null,['id'=>'user','name'=>'user', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Nombres', 'Nombres(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
          {!! Form::text('nombres', null, ['class' => 'form-control', 'placeholder' => 'Nombres', 'readonly' => 'readonly' , 'id'=>'nombres','name'=>'nombres']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Apellido Paterno', 'Apellido Paterno(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
          {!! Form::text('apellidoP', null, ['class' => 'form-control', 'placeholder' => 'Apellido Paterno', 'readonly' => 'readonly' , 'id'=>'apellidoP','name'=>'apellidoP']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Apellido Materno', 'Apellido Materno(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
          {!! Form::text('apellidoM', null, ['class' => 'form-control', 'placeholder' => 'Apellido Materno', 'readonly' => 'readonly' , 'id'=>'apellidoM','name'=>'apellidoM']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Email', 'Email(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
          {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email', 'readonly' => 'readonly' , 'id'=>'email','name'=>'email']) !!}
          </div>
        </div>

        <div class="form-group row">
          <div class="col-sm-6">
          </div><!--col-sm-10-->
        </div><!--form control-->

        <div class="form-group row">
          {!! Form::label('status', 'Roles asociados(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            @if (count($roles) > 0)
              @foreach($roles as $role)
                <div class="col-sm-12">
                  <input class="form-check-input col-form-label" type="checkbox" value="{{$role->id}}" name="assignees_roles[]" id="role-{{$role->id}}" />
                  <label class="form-check-label col-sm-9" for="role-{{$role->id}}">{{ $role->name }}</label>
                  <a href="#" data-role="role_{{$role->id}}" class="show-permissions small">
                    (<span class="show-text"> Mostrar</span>
                    <span class="hide-text hidden">Ocultar</span> Permisos )
                  </a>
                </div><br>
                <div class="permission-list hidden " data-role="role_{{$role->id}}">
                  @if ($role->todos)
                  <p style="margin-left: 3%;">Todos los Permisos asignados.</p><br/><br/>
                  @else
                    @if (count($role->permissions) > 0)
                      <blockquote style="margin-left: 3%;">
                        <p>
                          @foreach ($role->permissions as $perm)
                            {{$perm->display_name}}<br/>
                          @endforeach
                        </p>
                      </blockquote>
                    @else
                      <p style="margin-left: 3%;">Sin Permisos asignados.</p><br/><br/>
                    @endif
                  @endif
                </div>
              @endforeach
            @else
              No hay Roles disponibles.
            @endif
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! Form::submit('Guardar', ['class' => 'btn btn-success btn-sm','title'=>'Guardar']) !!}
              {!! link_to_route('sigc_usuarios', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) !!}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{!! Form::close() !!}
@stop
@section('after-scripts-end')
<style type="text/css">
  .hidden {
  display: none !important; }
</style>
<script type="text/javascript">
$(document).ready(function(){
  $('#user').on('change', function(){
        var usuario = $(this).val();
        if(usuario){
          $.get('Usuario/UsuarioMindep/'+usuario , function(usuarioM){
            $('#nombres').empty();
            $('#apellidoP').empty();
            $('#apellidoM').empty();
            $('#email').empty();

            $('#nombres').val(usuarioM.nombres);
            $('#apellidoP').val(usuarioM.apellido_paterno);
            $('#apellidoM').val(usuarioM.apellido_materno);
            $('#email').val(usuarioM.email);
          })

        }else{
           $('#nombres').empty();
        }
  });
});
</script>
@endsection
