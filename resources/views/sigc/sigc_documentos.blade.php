@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-file-text"></i> SIGC - Documentos</h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title">Nuevo Documento</h5>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">


                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="text-center">
                      {!! link_to_route('sigc_crear_documento', 'Nuevo Documento', [], ['class' => 'btn btn-primary btn-sm','title'=>'Nuevo Documento']) !!}
                      </div>
                    </div>
                  </div>


          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@if(count($dataDefinitiva)>0)
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">SIGC - Documentos</h5>

          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th nowrap style="color:white">Nombre</th>
                        <th nowrap style="color:white">Archivo</th>
                        <th nowrap style="color:white">Tipo de Documento</th>
                        <th nowrap style="color:white">Año</th>
                        <th nowrap style="color:white">Opciones</th>
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($dataDefinitiva)>0)
                      @foreach ($dataDefinitiva as $indice => $tramite)
                          <tr>
                           <td nowrap>{{$tramite->nombre}}</td>
                           <td nowrap>{{$tramite->filename}}</td>
                           <td nowrap>{{$tramite->documento_tipo_id}}</td>
                           <td nowrap>{{$tramite->ano}}</td>
                           <td>
                             <a class="btn accion btn-success accion" href="{{$tramite->filename}}"><i class="fa fa-download"></i></a>
                             <a class="btn accion btn-info accion" href="{{route('sigc_editar_documento',$tramite->id)}}"><i class="fa fa-pencil"></i></a></td>
                          </tr>
                      @endforeach
                @endif
                </tbody>
              </table>
          </div>
      </div>
  </div>
</div>
@endif
@endsection
@section('before-scripts-end')
@stop
