@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-user"></i> SIGC - Editar Usuario</h1><br>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
{!! Form::open(['route' => ['sigc_update_usuario', $editUsuario->id], 'files' => true, 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','id'=>'form-create']) !!}
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-danger card-outline">
        <h3 class="card-title">Editar Usuario</h3>
      </div>
      <div class="card-body">

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>

        <div class="callout col-sm-8 offset-sm-2" style="color: #737373;">
          <h4 style="font-size: 14px;"><i class="fa fa-info-circle" style="color: #ff0000;"></i> Nota:</h4>
          <span>Los campos marcados con un asterisco (*) son obligatorios.</span>
        </div><br>

        <div class="form-group row">
          {!! Form::label('Nombres', 'Nombres(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
          {!! Form::text('nombres', $editUsuario->nombres, ['class' => 'form-control', 'placeholder' => 'Nombres', 'readonly' => 'readonly' , 'id'=>'nombres','name'=>'nombres']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Apellido Paterno', 'Apellido Paterno(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
          {!! Form::text('apellidoP', $editUsuario->ap_paterno, ['class' => 'form-control', 'placeholder' => 'Apellido Paterno', 'readonly' => 'readonly' , 'id'=>'apellidoP','name'=>'apellidoP']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Apellido Materno', 'Apellido Materno(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
          {!! Form::text('apellidoM', $editUsuario->ap_materno, ['class' => 'form-control', 'placeholder' => 'Apellido Materno', 'readonly' => 'readonly' , 'id'=>'apellidoM','name'=>'apellidoM']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Email', 'Email(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
          {!! Form::text('email', $editUsuario->email, ['class' => 'form-control', 'placeholder' => 'Email', 'readonly' => 'readonly' , 'id'=>'email','name'=>'email']) !!}
          </div>
        </div>


        <div class="form-group row">
          {!! Form::label('estado','Activo', ['class' => 'col-sm-3 col-form-label']) !!}

          <div class="col-sm-6">
            {!! Form::checkbox('estado', '1', $editUsuario->estado == 1) !!}
          </div><!--col-lg-1-->
        </div>



        <div class="form-group row">
          <div class="col-sm-6">
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('status', 'Roles asociados(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            @if (count($roles) > 0)
              @foreach($roles as $role)
                <div class="col-sm-12">
                  <input class="form-check-input col-form-label" type="checkbox" value="{{$role->id}}" name="assignees_roles[]" {{in_array($role->id, $user_roles) ? 'checked' : ''}} id="role-{{$role->id}}" />
                  <label class="form-check-label col-sm-9" for="role-{{$role->id}}">{{ $role->name }}</label>
                  <a href="#" data-role="role_{{$role->id}}" class="show-permissions small">
                    (<span class="show-text"> Mostrar</span>
                    <span class="hide-text hidden">Ocultar</span> Permisos )
                  </a>
                </div><br>
                <div class="permission-list hidden " data-role="role_{{$role->id}}">
                  @if ($role->todos)
                  <p style="margin-left: 3%;">Todos los Permisos asignados.</p><br/><br/>
                  @else
                    @if (count($role->permissions) > 0)
                      <blockquote style="margin-left: 3%;">
                        <p>
                          @foreach ($role->permissions as $perm)
                            {{$perm->display_name}}<br/>
                          @endforeach
                        </p>
                      </blockquote>
                    @else
                      <p style="margin-left: 3%;">Sin Permisos asignados.</p><br/><br/>
                    @endif
                  @endif
                </div>
              @endforeach
            @else
              No hay Roles disponibles.
            @endif
          </div>
        </div>




        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! Form::submit('Guardar', ['class' => 'btn btn-success btn-sm','title'=>'Guardar']) !!}
              {!! link_to_route('sigc_usuarios', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) !!}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{!! Form::close() !!}
@stop
@section('after-scripts-end')
<style type="text/css">
  .hidden {
  display: none !important; }
</style>
@endsection
