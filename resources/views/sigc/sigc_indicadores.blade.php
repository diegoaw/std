@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-list"></i><b> SIGC - Indicadores</b></h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title"><b>Filtros SIGC - Indicadores</b></h5>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">
            {!! Form::open(['route' => 'sigc_indicadores', 'method' => 'get' , 'class'=>"form-horizontal"]) !!}
                  <div class="form-row">


                   <div class="col-sm-3">
                      <label for="ano" class=" col-form-label">Año Indicador: </label>
                      {!! Form::select('ano',$selectAno,$anoView,['id'=>'ano','name'=>'ano', 'class'=>'form-control']) !!}
                    </div>

                    <div class="col-sm-3">
                      <label for="tipo" class=" col-form-label">Tipo Indicador: </label>
                      {!! Form::select('tipo',$selectTipo,$tipoView,['id'=>'tipo','name'=>'tipo', 'class'=>'form-control']) !!}
                    </div>

                    <div class="col-sm-3">
                      <label for="equipo" class=" col-form-label">Equipo / Objetivo de Gestión: </label>
                      {!! Form::select('equipo',$selectEquipo,$equipoView,['id'=>'equipo','name'=>'equipo', 'class'=>'form-control']) !!}
                    </div>

                  

                    <div class="col-sm-3">
                      <label for="nombre" class=" col-form-label">Indicador: </label>
                      {!! Form::select('indicador',$selectIndicador,$indicadorView,['id'=>'indicador','name'=>'indicador', 'class'=>'form-control']) !!}
                    </div>

                    <!-- <div class="col-sm-3">
                      <label for="equipo" class=" col-form-label">División del Indicador: </label>
                      {!! Form::select('divisionId',$selectDivision,$divisionView,['id'=>'divisionId','name'=>'divisionId', 'class'=>'form-control']) !!}
                    </div> -->
                    <div class="form-group">
                    <div class="col-sm-12">
                     <br>
                    </div> 
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="text-right">
                        <input type="submit" class= "btn btn-primary" value="Buscar" name="Buscar">
                      </div>
                    </div>
                  </div>

            {!! Form::close() !!}

            @if (auth()->user()->hasPermission(['09-06']))
            <div class="form-group">
                  <div class="col-sm-12">
                    <div class="text-center">
                    {!! link_to_route('sigc_indicadores_crear', 'Nuevo Indicador', [], ['class' => 'btn btn-success btn-sm','title'=>'Nuevo Indicador']) !!}
                    </div>
                  </div>
            </div>
            @endif

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@if(count($dataDefinitiva)>0 && is_null($anoView) || $anoView == date("Y"))
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title"><b>SIGC - Indicadores Año {{date("Y")}}</b></h5>


          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                      <th nowrap style="color:white">ID</th>
                        <th nowrap style="color:white">Año</th>
                        <th nowrap style="color:white">Tipo</th>
                        <th nowrap style="color:white">Nombre</th>
                        <th nowrap style="color:white">División</th>
                        <th nowrap style="color:white">Equipo / Objetivo de Gestión</th>
                        <th nowrap style="color:white">Detalles</th>
                        @if (auth()->user()->hasPermission(['09-05']))
                        <th nowrap style="color:white">Acciones</th>
                        @endif
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($dataDefinitiva)>0)
                         @foreach ($dataDefinitiva as $indice => $tramite)
                          <tr>
                           <td nowrap>{{$tramite->id}}</td>
                           <td nowrap>{{$tramite->ano}}</td>
                           <td >{{$tramite->tipo->alias}}</td>
                           <td>{{$tramite->nombre}}</td>
                           <td>@if($tramite->division) {{$tramite->division->nombre}} @endif</td>
                           <td >@if($tramite->equipo) {{$tramite->equipo->nombre}} @endif</td>
                           <td><center>

                            <a type="button" title="Ver Detalles" class="btn btn-primary" data-toggle="collapse" {!! "data-target='#demo".$indice."'" !!}   {!!"data-parent='#myTable".$tramite->id."'" !!}
                           ><i class="fa fa-eye"></i></a>

                           </center></td>
                           @if (auth()->user()->hasPermission(['09-05']))
                           <td nowrap><a class="btn accion btn-success accion" title="Editar Indicador" href="{{route('sigc_indicadores_editar',$tramite->id)}}"><i class="fa fa-pencil"></i></a>
                           @if($tramite->habilitar_suplencia == 0)
                            <a class="btn accion btn-danger accion" href="{{route('sigc_habilitar_suplencia',$tramite->id)}}">Habilitar Suplencia</a>
                           @endif
                           @if($tramite->habilitar_suplencia == 1)
                            <a class="btn accion btn-warning accion" href="{{route('sigc_deshabilitar_suplencia',$tramite->id)}}">Deshabilitar Suplencia</a>
                           @endif
                         </td>
                           @endif
                          </tr>

                           <tr  {!! "id='demo".$indice."'"  !!} class="collapse">
                        <td colspan="10" class="hiddenRow">
                          <table class="table table-striped table-sm" {!!  "id='myTable".$tramite->id."'" !!}  >
                              <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Categoría</b>
                              </td>
                                <td style="background-color: #fff;">
                                @if($tramite->categoria)
                                {{$tramite->categoria->nombre}}
                                @endif
                              </td>
                            </tr>
                               <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Numerador</b>
                              </td>
                                <td style="background-color: #fff;" >
                                {{$tramite->numerador}}
                              </td>
                            </tr>
                               <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Denominador</b>
                              </td>
                                <td style="background-color: #fff;" >
                                {{$tramite->denominador}}
                              </td>
                            </tr>
                            <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Multiplicador</b>
                              </td>
                                <td style="background-color: #fff;" >
                                {{$tramite->multiplicador}}
                              </td>
                            </tr>
                            <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Ponderador</b>
                              </td>
                                <td style="background-color: #fff;" >
                                {{$tramite->ponderador}}
                              </td>
                            </tr>
                            <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Meta</b>
                              </td>
                                <td style="background-color: #fff;" >
                                {{$tramite->meta}}
                              </td>
                            </tr>
                            <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Unidad</b>
                              </td>
                                <td style="background-color: #fff;" >
                                 @if($tramite->unidad)
                                {{$tramite->unidad->nombre}}
                                @endif
                              </td>
                            </tr>
                             <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Supuestos</b>
                              </td>
                                <td style="background-color: #fff;" >
                                {{$tramite->supuestos}}
                              </td>
                            </tr>
                             <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Medios Verificación</b>
                              </td>
                                <td style="background-color: #fff;" >
                                {{$tramite->medio_verif}}
                              </td>
                            </tr>
                                <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Notas</b>
                              </td>
                                <td style="background-color: #fff;" >
                                {{$tramite->notas}}
                              </td>
                            </tr>
                            <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Producto</b>
                              </td>
                                <td style="background-color: #fff;" >
                                 @if($tramite->producto)
                                {{$tramite->producto->nombre}}
                                @endif
                              </td>
                            </tr>
                              <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Ámbito</b>
                              </td>
                                <td style="background-color: #fff;" >
                                 @if($tramite->ambito)
                                {{$tramite->ambito->nombre}}
                                @endif
                              </td>
                            </tr>
                                 <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Dimensión</b>
                              </td>
                                <td style="background-color: #fff;" >
                                 @if($tramite->dimension)
                                {{$tramite->dimension->nombre}}
                                @endif
                              </td>
                            </tr>
                            <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>División</b>
                              </td>
                                <td style="background-color: #fff;" >
                                 @if($tramite->division)
                                {{$tramite->division->nombre}}
                                @endif
                              </td>
                            </tr>
                              <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Revisor</b>
                              </td>
                                <td style="background-color: #fff;" >
                                 @if($tramite->revisor)
                                {{$tramite->revisor->nombres}} {{$tramite->revisor->ap_paterno}} {{$tramite->revisor->ap_materno}}
                                @endif
                              </td>
                            </tr>
                            <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Suplente Revisor</b>
                              </td>
                                <td style="background-color: #fff;" >
                                 @if($tramite->responsable1)
                                {{$tramite->responsable1->nombres}} {{$tramite->responsable1->ap_paterno}} {{$tramite->responsable1->ap_materno}}
                                @endif
                              </td>
                            </tr>
                            <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Reportador</b>
                              </td>
                                <td style="background-color: #fff;" >
                                 @if($tramite->reportador1)
                                {{$tramite->reportador1->nombres}} {{$tramite->reportador1->ap_paterno}} {{$tramite->reportador1->ap_materno}}
                                @endif
                              </td>
                            </tr>
                            <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Suplente Reportador</b>
                              </td>
                                <td style="background-color: #fff;" >
                                 @if($tramite->reportadores1)
                                {{$tramite->reportadores1->nombres}} {{$tramite->reportadores1->ap_paterno}} {{$tramite->reportadores1->ap_materno}}
                                @endif
                              </td>
                            </tr>
                        </table>
                      </td>


                    </tr>
                          @endforeach
                @endif
                </tbody>
              </table>
          </div>
@endif
<br>

@if(count($dataDefinitivaO)>0)
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title"><b>SIGC - Indicadores Años Anteriores</b></h5>

          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                       <th nowrap style="color:white">ID</th>
                        <th nowrap style="color:white">Año</th>
                        <th nowrap style="color:white">Tipo</th>
                        <th nowrap style="color:white">Nombre</th>
                        <th nowrap style="color:white">Equipo / Objetivo de Gestión</th>
                        <th nowrap style="color:white">Detalles</th>

                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($dataDefinitivaO)>0)
                         @foreach ($dataDefinitivaO as $indice1 => $tramite)
                          <tr>
                           <td nowrap>{{$tramite->id}}</td>
                           <td nowrap>{{$tramite->ano}}</td>
                           <td nowrap> {{$tramite->tipo->alias}}</td>
                           <td>{{$tramite->nombre}}</td>
                           <td nowrap>@if($tramite->equipo) {{$tramite->equipo->nombre}} @endif</td>
                           <td><center>

                            <a type="button" class="btn btn-primary" title="Ver Detalles" data-toggle="collapse" {!! "data-target='#demo".($indice1+999)."'" !!}   {!!"data-parent='#myTable".$tramite->id."'" !!}
                           ><i class="fa fa-eye"></i></a>

                           </center></td>

                          </tr>

                           <tr  {!! "id='demo".($indice1+999)."'"  !!} class="collapse">
                        <td colspan="10" class="hiddenRow">
                          <table class="table table-striped table-sm" {!!  "id='myTable".$tramite->id."'" !!}  >
                            <tr>
                            <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                              <b>Categoría</b>
                            </td>
                              <td style="background-color: #fff;">
                              @if($tramite->categoria)
                              {{$tramite->categoria->nombre}}
                              @endif
                            </td>
                          </tr>
                             <tr>
                            <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                              <b>Numerador</b>
                            </td>
                              <td style="background-color: #fff;" >
                              {{$tramite->numerador}}
                            </td>
                          </tr>
                             <tr>
                            <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                              <b>Denominador</b>
                            </td>
                              <td style="background-color: #fff;" >
                              {{$tramite->denominador}}
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                              <b>Multiplicador</b>
                            </td>
                              <td style="background-color: #fff;" >
                              {{$tramite->multiplicador}}
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                              <b>Ponderador</b>
                            </td>
                              <td style="background-color: #fff;" >
                              {{$tramite->ponderador}}
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                              <b>Meta</b>
                            </td>
                              <td style="background-color: #fff;" >
                              {{$tramite->meta}}
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                              <b>Unidad</b>
                            </td>
                              <td style="background-color: #fff;" >
                               @if($tramite->unidad)
                              {{$tramite->unidad->nombre}}
                              @endif
                            </td>
                          </tr>
                           <tr>
                            <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                              <b>Supuestos</b>
                            </td>
                              <td style="background-color: #fff;" >
                              {{$tramite->supuestos}}
                            </td>
                          </tr>
                           <tr>
                            <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                              <b>Medios Verificación</b>
                            </td>
                              <td style="background-color: #fff;" >
                              {{$tramite->medio_verif}}
                            </td>
                          </tr>
                              <tr>
                            <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                              <b>Notas</b>
                            </td>
                              <td style="background-color: #fff;" >
                              {{$tramite->notas}}
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                              <b>Producto</b>
                            </td>
                              <td style="background-color: #fff;" >
                               @if($tramite->producto)
                              {{$tramite->producto->nombre}}
                              @endif
                            </td>
                          </tr>
                            <tr>
                            <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                              <b>Ámbito</b>
                            </td>
                              <td style="background-color: #fff;" >
                               @if($tramite->ambito)
                              {{$tramite->ambito->nombre}}
                              @endif
                            </td>
                          </tr>
                               <tr>
                            <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                              <b>Dimensión</b>
                            </td>
                              <td style="background-color: #fff;" >
                               @if($tramite->dimension)
                              {{$tramite->dimension->nombre}}
                              @endif
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                              <b>División</b>
                            </td>
                              <td style="background-color: #fff;" >
                               @if($tramite->division)
                              {{$tramite->division->nombre}}
                              @endif
                            </td>
                          </tr>
                            <tr>
                            <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                              <b>Revisor</b>
                            </td>
                              <td style="background-color: #fff;" >
                               @if($tramite->revisor)
                              {{$tramite->revisor->nombres}} {{$tramite->revisor->ap_paterno}} {{$tramite->revisor->ap_materno}}
                              @endif
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                              <b>Suplente Revisor</b>
                            </td>
                              <td style="background-color: #fff;" >
                               @if($tramite->responsable1)
                              {{$tramite->responsable1->nombres}} {{$tramite->responsable1->ap_paterno}} {{$tramite->responsable1->ap_materno}}
                              @endif
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                              <b>Reportador</b>
                            </td>
                              <td style="background-color: #fff;" >
                               @if($tramite->reportador1)
                              {{$tramite->reportador1->nombres}} {{$tramite->reportador1->ap_paterno}} {{$tramite->reportador1->ap_materno}}
                              @endif
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                              <b>Suplente Reportador</b>
                            </td>
                              <td style="background-color: #fff;" >
                               @if($tramite->reportadores1)
                              {{$tramite->reportadores1->nombres}} {{$tramite->reportadores1->ap_paterno}} {{$tramite->reportadores1->ap_materno}}
                              @endif
                            </td>
                          </tr>
                        </table>
                      </td>


                    </tr>
                          @endforeach
                @endif
                </tbody>
              </table>
          </div>

          <div class="col-sm-12">
                <div class="pull-right">

                  {{ $dataDefinitivaO->links() }}

                </div>
          </div>
        </div>
    </div>
@endif



@endsection
@section('before-scripts-end')

<script type="text/javascript">
$(document).ready(function(){
  $('#ano').on('change', function(){
        var ano = $(this).val();
        if(ano !== ''){

       //   $.get('/SIGC/Indicadores/GetTipoPorAno/'+ano , function(tipos){
            $.get('/SIGC/Indicadores/GetTipoPorAno/'+ano , function(tipos){
            console.log('tipos de acuerdo al ' ,tipos)
            $('#tipo').empty();
            $('#tipo').append("<option>SELECCIONE TIPO</option>");
            $.each(tipos, function(index , value){
            $('#tipo').append("<option value='"+index+"'>"+value+" </option>");
            })
          })

        }else{
           $('#tipo').empty();
        }
  });
});

$(document).ready(function(){
  $('#ano').on('change', function(){
        var ano = $(this).val();
        if(ano !== ''){
          $.get('/SIGC/Indicadores/GetEquiposPorAno/'+ano , function(equipos){
            $('#equipo').empty();
            $('#equipo').append("<option>SELECCIONE EQUIPO</option>");
            $.each(equipos, function(index , value){
            $('#equipo').append("<option value='"+index+"'>"+value+" </option>");
            })
          })

        }else{
           $('#equipo').empty();
        }
  });
});

$(document).ready(function(){
  $('#ano').on('change', function(){
        var equipo = 0;
        var tipo = 0;
        var ano = $(this).val();
        if($("#equipo").val()) {
              equipo = $("#equipo").val();
        }
        if($("#tipo").val()) {
              tipo = $("#tipo").val();
        }


        console.log('dddddddd' ,equipo , tipo)
        if(ano !== ''){
          $.get('/SIGC/Indicadores/GetIndicadoresSelect/'+ano+'/'+tipo+'/'+equipo, function(indicadores){
            $('#indicador').empty();
            $('#indicador').append("<option>SELECCIONE INDICADORES</option>");
            $.each(indicadores, function(index , value){
            $('#indicador').append("<option value='"+index+"'>"+value+" </option>");
            })
          })

        }else{
           $('#indicador').empty();
        }
  });
});


$(document).ready(function(){
  $('#tipo').on('change', function(){
        var equipo = 0;
        var ano = 0;
        var tipo = $(this).val();
        if($("#equipo").val()) {
              equipo = $("#equipo").val();
        }
        if($("#ano").val()) {
              ano = $("#ano").val();
        }



        console.log('dddddddd' ,equipo , tipo)
        if(tipo !== ''){
          $.get('/SIGC/Indicadores/GetIndicadoresSelect/'+ano+'/'+tipo+'/'+equipo, function(indicadores){
            $('#indicador').empty();
            $('#indicador').append("<option>SELECCIONE INDICADORES</option>");
            $.each(indicadores, function(index , value){
            $('#indicador').append("<option value='"+index+"'>"+value+" </option>");
            })
          })

        }else{
           $('#indicador').empty();
        }
  });
});

$(document).ready(function(){
  $('#equipo').on('change', function(){
        var tipo = 0;
        var ano = 0;
        var equipo = $(this).val();

        if($("#tipo").val()) {
              tipo = $("#tipo").val();
        }
        if($("#ano").val()) {
              ano = $("#ano").val();
        }
        console.log('dddddddd' ,equipo , tipo)
        if(equipo !== ''){
          $.get('/SIGC/Indicadores/GetIndicadoresSelect/'+ano+'/'+tipo+'/'+equipo, function(indicadores){
            $('#indicador').empty();
            $('#indicador').append("<option>SELECCIONE INDICADORES</option>");
            $.each(indicadores, function(index , value){
            $('#indicador').append("<option value='"+index+"'>"+value+" </option>");
            })
          })

        }else{
           $('#indicador').empty();
        }
  });
});
</script>
@stop
