@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-file-text"></i> SIGC - Histórico Indicadores</h1><br> 
    </div>
    
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title">Filtros SIGC - Histórico Indicadores</h5>        
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">

               {!! Form::open(['route' => 'sigc_indicadores_historico', 'method' => 'get' , 'class'=>"form-horizontal"]) !!}
                  <div class="form-row">
                   
                    <div class="col-sm-0">
                      <label for="ano" class=" col-form-label">Año: </label>
                      <input type="text" class="form-control" id="ano" name="ano" value="{{ $requestano }}" placeholder="Año">
                    </div>

                    <div class="col-sm-3">
                      <label for="tipo" class=" col-form-label">Tipo Indicador: </label>
                      {!! Form::select('tipo',$selectTipo,$tipoView,['id'=>'tipo','name'=>'tipo', 'class'=>'form-control']) !!}
                    </div>

                    <div class="col-sm-3">
                      <label for="equipo" class=" col-form-label">Equipo / División: </label>
                      {!! Form::select('equipo',$selectEquipo,$equipoView,['id'=>'equipo','name'=>'equipo', 'class'=>'form-control']) !!}
                    </div>

                    <div class="col-sm-0">
                      <label for="nombre" class=" col-form-label">Nombre Indicador: </label>
                      <input type="text" class="form-control" id="nombre" name="nombre" value="{{ $requestnombreIndicador }}" placeholder="Nombre Indicador">
                    </div>

                  </div>
                  <div class="col-sm-12">
                  <div class="text-right">
                  <input type="submit" class= "btn btn-primary" name="enviar">
                  </div>
                  </div>

            {!! Form::close() !!} 

          </div>  
        </div> 
      </div> 
    </div>
  </div>
</div>

@if(count($dataDefinitiva)>0)
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">SIGC - Histórico Indicadores</h5>
            
          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th nowrap style="color:white">Año</th>
                        <th nowrap style="color:white">Tipo</th>
                        <th nowrap style="color:white">Nombre</th>
                        <th nowrap style="color:white">Equipo / Centro de Resposabilidad</th>
                        <th nowrap style="color:white">Detalles</th>
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($dataDefinitiva)>0)
                         @foreach ($dataDefinitiva as $indice => $tramite)
                          <tr>
                           <td nowrap>{{$tramite->ano}}</td>
                           <td nowrap>{{$tramite->nombre_tipo}}</td>
                           <td>{{$tramite->nombre}}</td>
                           <td nowrap>{{$tramite->nombre_equipo}}</td>
                           <td><center> 
                        
                            <button type="button" class="btn btn-primary" data-toggle="collapse" {!! "data-target='#demo".$indice."'" !!}   {!!"data-parent='#myTable".$tramite->id."'" !!} 
                           ><i class="fa fa-eye"></i></button>
                          
                           </center></td>
                          </tr>

                           <tr  {!! "id='demo".$indice."'"  !!} class="collapse">
                        <td colspan="10" class="hiddenRow">
                          <table class="table table-striped table-sm" {!!  "id='myTable".$tramite->id."'" !!}  >
                            <thead class="thead-primary">
                              <tr style="background-color: #fddadf;">
                              <tr style="background-color: #cd0b27;"> 
                              <th nowrap style="color:white">ID</th>
                              <th nowrap style="color:white">Categoría</th>
                              <th nowrap style="color:white">Numerador</th>
                              <th nowrap style="color:white">Denominador</th>                              
                              </tr>

                              </tr>
                            </thead>
                          <tbody>
                            
                            <tr>
                            <tr>
                            <td nowrap>{{$tramite->id}}</td>
                            <td nowrap>{{$tramite->nombre_categoria}}</td>
                            <td>{{$tramite->numerador}}</td>
                            <td nowrap>{{$tramite->denominador}}</td>                            
                          </tbody>
                        </table>
                      </td>
                    </tr>

                          @endforeach
                @endif
                </tbody>
              </table>
          </div>              
@endif
@endsection
@section('before-scripts-end')
@stop