@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-list"></i><b> SIGC - Indicadores por Reportar</b></h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')

<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title"><b>SIGC - Indicadores</b></h5>


          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                      <th nowrap style="color:white">ID</th>
                        <th nowrap style="color:white">Año</th>
                        <th nowrap style="color:white">Tipo</th>
                        <th nowrap style="color:white">Nombre</th>
                        <th nowrap style="color:white">Equipo / Centro de Resposabilidad</th>
                        <th style="color:white"><center>Detalles Indicador</center></th>
                        <th style="color:white"><center>Acciones Medición</center></th>
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($dataDefinitiva)>0)
                         @foreach ($dataDefinitiva as $indice => $tramite)
                          <tr>
                           <td nowrap>{{$tramite->id}}</td>
                           <td nowrap>{{$tramite->ano}}</td>
                           <td >{{$tramite->tipo->alias}}</td>
                           <td>{{$tramite->nombre}}</td>
                           <td nowrap>@if($tramite->equipo) {{$tramite->equipo->nombre}} @endif</td>
                           <td><center>
                            <a type="button" class="btn btn-info" title="Ver Detalles" data-toggle="collapse" {!! "data-target='#demo".$indice."'" !!}   {!!"data-parent='#myTable".$tramite->id."'" !!}
                           ><i class="fa fa-eye"></i></a>
                           </center></td>
                           <td nowrap>
                           <!-- <a class="btn btn-info pull-right" style="margin: 10px" href="#"> Mediciones </a> -->
                           @if($tramite->habilitar_estado)
                           <a type="button" class="btn btn-warning" title="Ver Historial" href="{{route('sigc_mediciones_estado',$tramite->id_medicion_estado)}}"><i class="fa fa-clock-o"></i></a>
                           @endif
                           @if($tramite->reportar && $habititarReportar)
                           <a class="btn btn-primary pull-right" style="margin: 10px" href="{{route('sigc_reportar',$tramite->id)}}">Reportar</a>
                           @endif
                           @if($tramite->corregir && $habititarCorreccion)
                           <a class="btn btn-danger pull-right" style="margin: 10px" href="{{route('sigc_editar_reporte',$tramite->id_medicion_correccion)}}">Corregir</a>
                           @endif
                           @if($habititarReportar && $tramite->editar)
                           <a class="btn btn-primary pull-right" style="margin: 10px" href="{{route('sigc_editar_borrador_reporte',$tramite->id_medicion_editar)}}">Editar Reporte</a>
                           @endif
                           @if($habititarReportar && $tramite->editar )
                           <a class="btn btn-success pull-right" style="margin: 10px" href="{{route('sigc_enviar_reporte',$tramite->id_medicion_editar)}}">Enviar Reporte</a>
                           @endif
                          </td>
                          </tr>

                           <tr  {!! "id='demo".$indice."'"  !!} class="collapse">
                        <td colspan="10" class="hiddenRow">
                          <table class="table table-striped table-sm" {!!  "id='myTable".$tramite->id."'" !!}  >
                              <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Categoría</b>
                              </td>
                                <td style="background-color: #fff;">
                                @if($tramite->categoria)
                                {{$tramite->categoria->nombre}}
                                @endif
                              </td>
                            </tr>
                               <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Numerador</b>
                              </td>
                                <td style="background-color: #fff;" >
                                {{$tramite->numerador}}
                              </td>
                            </tr>
                               <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Denominador</b>
                              </td>
                                <td style="background-color: #fff;" >
                                {{$tramite->denominador}}
                              </td>
                            </tr>
                            <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Multiplicador</b>
                              </td>
                                <td style="background-color: #fff;" >
                                {{$tramite->multiplicador}}
                              </td>
                            </tr>
                            <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Ponderador</b>
                              </td>
                                <td style="background-color: #fff;" >
                                {{$tramite->ponderador}}
                              </td>
                            </tr>
                            <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Meta</b>
                              </td>
                                <td style="background-color: #fff;" >
                                {{$tramite->meta}}
                              </td>
                            </tr>
                            <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Unidad</b>
                              </td>
                                <td style="background-color: #fff;" >
                                 @if($tramite->unidad)
                                {{$tramite->unidad->nombre}}
                                @endif
                              </td>
                            </tr>
                             <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Supuestos</b>
                              </td>
                                <td style="background-color: #fff;" >
                                {{$tramite->supuestos}}
                              </td>
                            </tr>
                             <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Medios Verificación</b>
                              </td>
                                <td style="background-color: #fff;" >
                                {{$tramite->medio_verif}}
                              </td>
                            </tr>
                                <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Notas</b>
                              </td>
                                <td style="background-color: #fff;" >
                                {{$tramite->notas}}
                              </td>
                            </tr>
                            <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Producto</b>
                              </td>
                                <td style="background-color: #fff;" >
                                 @if($tramite->producto)
                                {{$tramite->producto->nombre}}
                                @endif
                              </td>
                            </tr>
                              <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Ámbito</b>
                              </td>
                                <td style="background-color: #fff;" >
                                 @if($tramite->ambito)
                                {{$tramite->ambito->nombre}}
                                @endif
                              </td>
                            </tr>
                                 <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Dimensión</b>
                              </td>
                                <td style="background-color: #fff;" >
                                 @if($tramite->dimension)
                                {{$tramite->dimension->nombre}}
                                @endif
                              </td>
                            </tr>
                            <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>División</b>
                              </td>
                                <td style="background-color: #fff;" >
                                 @if($tramite->division)
                                {{$tramite->division->nombre}}
                                @endif
                              </td>
                            </tr>
                              <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Revisor</b>
                              </td>
                                <td style="background-color: #fff;" >
                                 @if($tramite->revisor)
                                {{$tramite->revisor->nombres}} {{$tramite->revisor->ap_paterno}} {{$tramite->revisor->ap_materno}}
                                @endif
                              </td>
                            </tr>
                            <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Suplente Revisor</b>
                              </td>
                                <td style="background-color: #fff;" >
                                 @if($tramite->responsable1)
                                {{$tramite->responsable1->nombres}} {{$tramite->responsable1->ap_paterno}} {{$tramite->responsable1->ap_materno}}
                                @endif
                              </td>
                            </tr>
                            <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Reportador</b>
                              </td>
                                <td style="background-color: #fff;" >
                                 @if($tramite->reportador1)
                                {{$tramite->reportador1->nombres}} {{$tramite->reportador1->ap_paterno}} {{$tramite->reportador1->ap_materno}}
                                @endif
                              </td>
                            </tr>
                            <tr>
                              <td colspan="2" style="background-color: #013273; color:white; width:10%" >
                                <b>Suplente Reportador</b>
                              </td>
                                <td style="background-color: #fff;" >
                                 @if($tramite->reportadores1)
                                {{$tramite->reportadores1->nombres}} {{$tramite->reportadores1->ap_paterno}} {{$tramite->reportadores1->ap_materno}}
                                @endif
                              </td>
                            </tr>
                        </table>
                      </td>


                    </tr>
                          @endforeach
                @endif
                </tbody>
              </table>
          </div>



@endsection
@section('before-scripts-end')
@stop
