@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-list"></i><b> SIGC - Inicio</b></h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')

<style>
    .header-col{
      background: #E3E9E5;
      color:#536170;
      text-align: center;
      font-size: 12px;
      font-weight: bold;

    }
    .header-calendar{
      background: #EE192D;color:white;
      height:70px;
    }
    .box-day{
      border:1px solid #E3E9E5;
      height:70px;
      font-size: 13px;
      overflow: hidden !important;

    }
    .box-dayoff{
      border:1px solid #E3E9E5;
      height:70px;
      background-color: #ccd1ce;
      overflow: hidden !important;

    }
    </style>
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-danger card-outline">
      <div class="card-header ">
          <h5 class="card-title"><b>SIGC - Calendario Actual</b></h5>
          <div>
          <a class="btn btn-primary pull-right" href="https://portalmindep.sharepoint.com/sites/planificacion" target="_blank">Sitio Planificación</a>
          </div>
      </div>
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">
            <div class="row align-items-center">
              <div class="col-sm-0">
              </div>
              <div class="col-sm-6">
                <div class="container">
                  <div class="row header-calendar"  >
                    <div class="col" style="display: flex; justify-content: space-between; padding: 20px;">
                      <a  href="{{ asset('/SIGC/Inicio/') }}/<?= $data['last']; ?>" style="margin:20px;"><i class="nav-icon fa fa-hand-o-left" style="color : #FFF"></i></a>
                        <h2 style="font-weight:bold;margin:10px;"><?= $mespanish; ?> <small><?= $data['year']; ?></small></h2>
                      <a  href="{{ asset('/SIGC/Inicio/') }}/<?= $data['next']; ?>" style="margin:10px;"><i class="nav-icon fa fa-hand-o-right" style="color : #FFF" ></i></a>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col header-col">Lunes</div>
                    <div class="col header-col">Martes</div>
                    <div class="col header-col">Miércoles</div>
                    <div class="col header-col">Jueves</div>
                    <div class="col header-col">Viernes</div>
                    <div class="col header-col">Sábado</div>
                    <div class="col header-col">Domingo</div>
                  </div>
                    @foreach ($data['calendar'] as $index01 => $weekdata)
                    @if($index01 < 6)
                      <div class="row">
                        @foreach  ($weekdata['datos'] as $index2 => $dayweek)
                          @if  ($dayweek['mes']==$mes)
                            <div class="col box-day">
                              {{ $dayweek['dia']  }}
                              @foreach  ($dayweek['evento'] as $event)
                              <b class="{{$event->class}}">
                                    {!!str_replace(' ','<br>', $event->title )!!}
                              </b>
                              @endforeach
                            </div>
                          @else

                          <div class="col box-dayoff">
                          </div>
                          @endif
                          @endforeach
                      </div>
                      @endif
                      @endforeach
                </div>
              </div>
              <div class="col-sm-4">
                <table class="table table-striped table-sm"  >
                    <thead >
                      <tr style="background-color: #cd0b27;">
                        <th nowrap style="color:white">Evento</th>
                        <th nowrap style="color:white"><center>Fecha Inicio</center></th>
                        <th nowrap style="color:white"><center>Fecha Fin</center></th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($eventosMes as $evento)
                      <tr>
                        <td class="{{$evento->class}}">{{$evento->title}}</td>
                        <td><center>{{date("d-m-Y", strtotime($evento->start_date))}}</center></td>
                        <td><center>{{date("d-m-Y", strtotime($evento->end_date))}}</center></td>
                      </tr>
                      @endforeach
                    </tbody>
                </table>
              </div>
              <div class="col-sm-2">
                @if (auth()->user()->hasPermission(['09-01']))
                <div class="form-group">
                  <div class="col-sm-12">
                    <div class="text-center">
                    {!! link_to_route('sigc_indicadores_usuario', 'Reportar', [], ['class' => 'btn btn-success btn-sm','title'=>'Reportar']) !!}
                    </div>
                  </div>
                </div>
                @endif
                @if (auth()->user()->hasPermission(['09-02']))
                <div class="form-group">
                  <div class="col-sm-12">
                    <div class="text-center">
                    {!! link_to_route('sigc_mediciones_revision', 'Revisar', [], ['class' => 'btn btn-success btn-sm','title'=>'Revisar']) !!}
                    </div>
                  </div>
                </div>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title"><b>Filtros SIGC - Indicadores 2022</b></h5>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">
            {!! Form::open(['route' => 'sigc_inicio', 'method' => 'get' , 'class'=>"form-horizontal"]) !!}
                  <div class="form-row">

                    <div class="col-sm-3">
                      <label for="tipo" class=" col-form-label">Tipo Indicador: </label>
                      {!! Form::select('tipo',$selectTipo,null,['id'=>'tipo','name'=>'tipo', 'class'=>'form-control']) !!}
                    </div>

                    <div class="col-sm-3">
                      <label for="equipo" class=" col-form-label">Equipo / Objetivo de Gestión: </label>
                      {!! Form::select('equipo',$selectEquipo,null,['id'=>'equipo','name'=>'equipo', 'class'=>'form-control']) !!}
                    </div>

                    <div class="col-sm-3">
                      <label for="division" class=" col-form-label">División del Indicador: </label>
                      {!! Form::select('divisionId',$selectDivision,null,['id'=>'divisionId','name'=>'divisionId', 'class'=>'form-control']) !!}
                    </div>

                    <div class="col-sm-3">
                      <label for="nombre" class=" col-form-label">Nombre Indicador: </label>
                      <input type="text" class="form-control" id="nombre" name="nombre" value="" placeholder="Nombre Indicador">
                    </div>

                  </div>
                  <div class="form-group">
                  </div>
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="text-right">
                        <input type="submit" class= "btn btn-primary" value="Buscar" name="Buscar">
                      </div>
                    </div>
                  </div>

            {!! Form::close() !!}

            {!! Form::open(['route' => 'sigc_reporte_indicadores', 'method' => 'get' , 'class'=>"form-horizontal"]) !!}
            <input type="hidden" class="form-control" id="nombre1" name="nombre1" value="{{ $requestnombreIndicador }}" >
            <input type="hidden" class="form-control" id="tipo1" name="tipo1" value="{{ $tipoView }}" >
            <input type="hidden" class="form-control" id="equipo1" name="equipo1" value="{{ $equipoView }}">
            <input type="hidden" class="form-control" id="division1" name="division1" value="{{ $divisionView }}">

            <input type="submit" onclick="inload()" class= "btn btn-success" name="imprimir" value='XLS' id='imprimir'>
            <input type="submit" onclick="inload()" class= "btn btn-danger" name="imprimir" value='PDF'  id='imprimir'>
            {!! Form::close() !!}


          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="card card-danger card-outline">
      <div class="card-header ">
      <h6><b>
        @if($tipoNombre)
                {{$tipoNombre->nombre}} 
        @endif
        @if($equipoNombre)
                 - {{$equipoNombre->nombre}}
        @endif
        @if($divisionNombre)
                 - {{$divisionNombre->nombre}}
        @endif
        </b></h6>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">

            @if(count($dataDefinitiva)>0)
              <div class="card-body table-responsive p-0" style="overflow-x:auto;">
                <div class="card-body">
                
                </div>

                  <table class="table table-striped table-sm table-responsive"  >
                    <thead >
                        <tr style="background-color: #cd0b27;">
                            <th  style="color:white; width:400px"><center>Id</center></th>
                            <th  style="color:white; width:500px"><center>Nombre Indicadores</center></th>
                            <th  style="color:white; width:350px"><center>Estado Mes Actual</center></th>
                            <th  style="color:white; width:350px"><center>Meta {{$anoO}}</center></th>
                            <th  style="color:white; width:350px"><center>Avance Cumplimiento de la Meta</center></th>
                            <th  style="color:white; width:350px"><center>Ponderación {{$anoO}}</center></th>
                            <th  style="color:white; width:350px"><center>Avance Ponderado de la Meta</center></th>

                        </tr>
                    </thead>
                    <tbody class="panel">
                    @if(count($dataDefinitiva)>0)
                            @foreach ($dataDefinitiva as $indice => $tramite)
                              <tr>
                              <td style="width:400px"><center>{{$tramite->id}}</center></td>
                              <td style="width:500px"><center>{{$tramite->nombre}}</center></td>
                              <td nowrap style="width:350px; font-size:18px; color:#04015c;"><b><center>{{$tramite->estado}}</center></b></td>
                              <td nowrap style="width:350px"><center>

                              <?php
                              if ($tramite->id == 227){
                            //    dd($dataDefinitiva);
                              }
                               ?>
                              @if($tramite->unidad_id == 2)
                                @if($tramite->meta  == 0)
                                Mide (indicador sin meta asociada)
                                @else
                                {{$tramite->meta_calculada}} días
                                @endif
                              @else
                                @if($tramite->meta == 0)
                                Mide (indicador sin meta asociada)
                                @else
                                {{$tramite->meta_calculada}} %
                                @endif
                              @endif
                              </center></td>
                              <td style="width:350px"><center>
                                <div class=" text-center">
                                  <input type="text" readonly class="knob" data-thickness="0.2" data-angleArc="250" data-angleOffset="-125" value="{{$arregloGraficaCumplimiento[$tramite->id][$mesO]}}" data-width="120" data-height="100" data-fgColor="#04015c">
                                  @if($tramite->meta == 0)
                                  <div class="knob-label" style="font-size:18px;"><b>Sin Meta Asociada</b></div>
                                  @endif
                                  @if($tramite->unidad_id_meta == 2 && $tramite->meta != 0)
                                    <div class="knob-label" style="font-size:18px;"><b>Meta {{$tramite->meta}} días</b></div>
                                  @endif
                                  @if($tramite->unidad_id_meta == 5 && $tramite->meta != 0)
                                    <div class="knob-label" style="font-size:18px;"><b>Meta {{$tramite->meta}} %</b></div>
                                  @endif
                                  @if($tramite->unidad_id_meta == 6 && $tramite->meta != 0)
                                    <div class="knob-label" style="font-size:18px;"><b>Meta {{$tramite->meta}}</b></div>
                                  @endif
                                </div>
                              </center></td>
                              <td nowrap style="width:350px"><center>
                                @if($tramite->equipo_id == 64)
                                  No Aplica
                                @else
                                  {{$tramite->ponderador}} %
                                @endif
                              </center></td>
                              <td style="width:350px"><center>
                                @if($tramite->equipo_id == 64)
                                  <b style="font-size:18px; color:#04015c;">No Aplica</b>
                                @else
                                  @if($tramite->reportada == true)
                                  <b style="font-size:18px; color:#04015c;">{{$tramite->ponderador_calculado}} %</b>
                                  @endif
                                <!-- <div class="card-body">
                                  <canvas id="{{$tramite->graf_rtdlda}}" style="min-height: 150px; height: 150px; max-height: 150px; max-width: 100%;"></canvas>
                                </div> -->
                                @endif
                              </center></td>


                              </tr>
                              @endforeach
                    @endif
                    </tbody>
                  </table>

              </div>
              @endif


          </div>
        </div>
      </div>
    </div>
  </div>
</div>



<!-- <div class="row">
  <div class="col-md-12">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Cumplimiento</a></li>
                  <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Ponderacion</a></li>
                  <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Reportabilidad</a></li>
                </ul>
              </div>
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">

                        @if(count($dataDefinitiva)>0)
                          <div class="card-body table-responsive p-0">
                            <div class="card-body">

                            </div>

                              <table class="table table-striped table-sm"  >
                                <thead >
                                    <tr style="background-color: #cd0b27;">
                                    <th nowrap style="color:white">Id</th>
                                        <th nowrap style="color:white">Nombre Indicadores</th>
                                        <th nowrap style="color:white"><center>Progreso</center></th>
                                    </tr>
                                </thead>
                                <tbody class="panel">
                                @if(count($dataDefinitiva)>0)
                                        @foreach ($dataDefinitiva as $indice => $tramite)
                                          <tr>
                                          <td>{{$tramite->id}}</td>
                                          <td>{{$tramite->nombre}}</td>
                                          <td><center>
                                            <html>
                                              <head>
                                              </head>
                                              <body>
                                              <div id="{{$tramite->id}}" style="width: 500px; height: 180px; "></div>
                                              </body>
                                              </html>
                                          </center></td>
                                          </tr>
                                          @endforeach
                                @endif
                                </tbody>
                              </table>
                          </div>
                          @endif



                  </div>

                  <div class="tab-pane" id="timeline">



                  </div>


                  <div class="tab-pane" id="settings">

                    <div class="card-body table-responsive p-0">
                      <div class="card-body">
                      </div>

                        <table class="table table-striped table-sm"  >
                          <thead >
                              <tr style="background-color: #cd0b27;">
                                  <th nowrap style="color:white">Nombre Indicadores</th>
                                  <th nowrap style="color:white"><center>Cantidad</center></th>
                                  <th nowrap style="color:white"><center>Aprobadas</center></th>
                                  <th nowrap style="color:white"><center>Rechazadas</center></th>
                                  <th nowrap style="color:white"><center>Total Reportadas</center></th>
                                  <th nowrap style="color:white"><center>Progreso</center></th>
                              </tr>
                          </thead>
                          <tbody class="panel">
                          @if(count($dataDefinitiva)>0)
                                  @foreach ($dataDefinitiva as $indice => $tramite)
                                    <tr>
                                    <td>{{$tramite->nombre}}</td>
                                    <td><center>{{$tramite->total}}</center></td>
                                    <td><center>{{$tramite->mediciones_apobadas}}</center></td>
                                    <td><center>{{$tramite->mediciones_rechazadas}}</center></td>
                                    <td><center>{{$tramite->total_reportadas}}</center></td>
                                    <td><center>
                                      <html>
                                        <head>
                                        </head>
                                        <body>
                                        <div id="rr{{$tramite->id}}" style="width: 500px; height: 180px; "></div>
                                        </body>
                                        </html>
                                    </center></td>
                                    </tr>
                                    @endforeach
                          @endif
                          </tbody>
                        </table>
                    </div>
                  </div>


              </div>


            </div>

          </div>
      </div>
  </div> -->

@endsection
@section('before-scripts-end')
<script>
  $(function () {
    /* jQueryKnob */

    $('.knob').knob({
      /*change : function (value) {
       //console.log("change : " + value);
       },
       release : function (value) {
       console.log("release : " + value);
       },
       cancel : function () {
       console.log("cancel : " + this.value);
       },*/
      draw: function () {

        // "tron" case
        if (this.$.data('skin') == 'tron') {

          var a   = this.angle(this.cv)  // Angle
            ,
              sa  = this.startAngle          // Previous start angle
            ,
              sat = this.startAngle         // Start angle
            ,
              ea                            // Previous end angle
            ,
              eat = sat + a                 // End angle
            ,
              r   = true

          this.g.lineWidth = this.lineWidth

          this.o.cursor
          && (sat = eat - 0.3)
          && (eat = eat + 0.3)

          if (this.o.displayPrevious) {
            ea = this.startAngle + this.angle(this.value)
            this.o.cursor
            && (sa = ea - 0.3)
            && (ea = ea + 0.3)
            this.g.beginPath()
            this.g.strokeStyle = this.previousColor
            this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false)
            this.g.stroke()
          }

          this.g.beginPath()
          this.g.strokeStyle = r ? this.o.fgColor : this.fgColor
          this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false)
          this.g.stroke()

          this.g.lineWidth = 2
          this.g.beginPath()
          this.g.strokeStyle = this.o.fgColor
          this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false)
          this.g.stroke()

          return false
        }
      }
    })
    /* END JQUERY KNOB */

  })

</script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="{{ asset('js/jquery.knob.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/demo.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/Chart.min.js') }}"></script>
@if(count($dataDefinitiva)>0)
        @foreach ($dataDefinitiva as $indice1 => $tramite1)
<script>
$(function () {
    var donutData        = {
      labels: [

      ],
      datasets: [
        {
          data: [700,500],
          backgroundColor : ['#cf0606', '#03279c', '#ad8305', '#299409', '#3c8dbc', '#d2d6de'],
        }
      ]
    }

    var pieChartCanvas = $('#{{$tramite1->graf_rtdlda}}').get(0).getContext('2d')
    var pieData        = donutData;
    var pieOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }

    var pieChart = new Chart(pieChartCanvas, {
      type: 'pie',
      data: pieData,
      options: pieOptions
    })

  })
</script>
@endforeach
@endif

<!-- <script type="text/javascript">
google.charts.load('current', {'packages':['corechart']});
</script>
@if(count($dataDefinitiva)>0)
                         @foreach ($dataDefinitiva as $indice => $tramite)
                          <script type="text/javascript">
                          google.charts.setOnLoadCallback(drawChart);

                            function drawChart() {
                              var data = google.visualization.arrayToDataTable([
                                ['Estado', 'Mediciones'],
                                ['Aprobadas', {{$tramite->porcentage_aprobadas}}],
                                ['Rechazadas', {{$tramite->porcentage_rechazadas}}],
                                ['Sin Medir',   {{$tramite->porcentage_sin_medir}}]
                              ]);

                              var options = {
                                pieHole: 0.4,
                                is3D: true,
                                backgroundColor: 'transparent',
                                pieSliceTextStyle: {
                                  color: 'white',
                                },
                                legend: 'none'
                              };

                              var chart = new google.visualization.PieChart(document.getElementById('rr'+{{$tramite->id}}));
                              chart.draw(data, options);
                            }
                          </script>
      @endforeach
@endif
<script type="text/javascript">
google.charts.load('current', {'packages':['gauge']});
</script>
@if(count($dataDefinitiva)>0)
                         @foreach ($dataDefinitiva as $indice => $tramite)
                          <script type="text/javascript">
                          google.charts.setOnLoadCallback(drawChart1);
                            function drawChart1() {
                              var data = google.visualization.arrayToDataTable([
                                ['Label', 'Value'],
                                ['Cumplimiento', {{$arregloGraficaCumplimiento[$tramite->id][$mesO]}}]
                              ]);
                              var options = {
                                width: 500, height: 180,
                                redFrom: 0, redTo: 5,
                                yellowFrom: 5, yellowTo: 10,
                                minorTicks: 25
                              };
                              var chart = new google.visualization.Gauge(document.getElementById({{$tramite->id}}));
                                 chart.draw(data, options);
                              }
                          </script>

      @endforeach
@endif -->

<script type="text/javascript">
      function inload(){
      setTimeout(function(){ document.getElementById("load").style.display="none"; }, 5000);

      }
      $(document).ready(function(){
      $('#tipo').on('change', function(){
            var tipo = $(this).val();
            if(tipo !== ''){ 
              $.get('/SIGC/Inicio/SelectEquipo/'+tipo, function(equipos){
                $('#equipo').empty();
                $('#equipo').append("<option value>SELECCIONE EQUIPOS</option>");
                $.each(equipos, function(index , value){
                $('#equipo').append("<option value='"+index+"'>"+value+" </option>");
                })
              })
            }else{
              $.get('/SIGC/SelectEquipos', function(equipos){
                $('#equipo').empty();
                $('#equipo').append("<option value>SELECCIONE EQUIPOS</option>");
                $.each(equipos, function(index , value){
                $('#equipo').append("<option value='"+index+"'>"+value+" </option>");
                })
              })
            }

            if(tipo !== ''){ 
              $.get('/SIGC/SelectDivisionPT/'+tipo, function(equipos){
                $('#divisionId').empty();
                $('#divisionId').append("<option value>SELECCIONE DIVISIÓN</option>");
                $.each(equipos, function(index , value){
                $('#divisionId').append("<option value='"+index+"'>"+value+" </option>");
                })
              })
            }else{
              $.get('/SIGC/SelectDivision', function(equipos){
                $('#divisionId').empty();
                $('#divisionId').append("<option value>SELECCIONE DIVISIÓN</option>");
                $.each(equipos, function(index , value){
                $('#divisionId').append("<option value='"+index+"'>"+value+" </option>");
                })
              })
            }
      });

      $('#equipo').on('change', function(){
        var equipo = $(this).val();
            if(equipo !== ''){ 
              $.get('/SIGC/SelectDivisionPE/'+equipo, function(disisiones){
                $('#divisionId').empty();
                $('#divisionId').append("<option value>SELECCIONE DIVISIÓN</option>");
                $.each(disisiones, function(index , value){
                $('#divisionId').append("<option value='"+index+"'>"+value+" </option>");
                })
              })
            }else{
              $.get('/SIGC/SelectDivision', function(disisiones){
                $('#divisionId').empty();
                $('#divisionId').append("<option value>SELECCIONE DIVISIÓN</option>");
                $.each(disisiones, function(index , value){
                $('#divisionId').append("<option value='"+index+"'>"+value+" </option>");
                })
              })
            }
      });
      

    });
</script>
@stop
