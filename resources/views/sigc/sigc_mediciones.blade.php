@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-line-chart"></i><b> SIGC - Mediciones</b></h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title"><b>Filtros SIGC - Mediciones</b></h5>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">

            {!! Form::open(['route' => 'sigc_mediciones', 'method' => 'get' , 'class'=>"form-horizontal"]) !!}
                  <div class="form-row">

                  <div class="col-sm-3">
                      <label for="ano" class=" col-form-label">Año: </label>
                      {!! Form::select('ano',$selectAno,$anoView,['id'=>'ano','name'=>'ano', 'class'=>'form-control']) !!}
                    </div>

                    <div class="col-sm-3">
                      <label for="tipo" class=" col-form-label">Tipo: </label>
                      {!! Form::select('tipo',$selectTipo,$tipoView,['id'=>'tipo','name'=>'tipo', 'class'=>'form-control']) !!}
                    </div>



                    <div class="col-sm-3">
                      <label for="ano" class=" col-form-label">Mes: </label>
                      {!! Form::select('mes',$selectMes,$mesView,['id'=>'mes','name'=>'mes', 'class'=>'form-control']) !!}
                    </div>

                    <div class="col-sm-3">
                      <label for="equipo" class=" col-form-label">Equipo / Objetivo de Gestión: </label>
                      {!! Form::select('equipo',$selectEquipo,$equipoView,['id'=>'equipo','name'=>'equipo', 'class'=>'form-control']) !!}
                    </div>

                    <div class="col-sm-3">
                      <label for="nombre" class=" col-form-label">Indicador: </label>
                      {!! Form::select('indicador',$selectIndicador,$indicadorView,['id'=>'indicador','name'=>'indicador', 'class'=>'form-control']) !!}
                    </div>

                  </div>
                  <div class="col-sm-12">
                  <div class="text-right">
                  <input type="submit" class= "btn btn-primary" value="Buscar" name="Buscar">
                  </div>
                  </div>
            {!! Form::close() !!}

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@if(count($dataDefinitiva)>0 && is_null($anoView) || $anoView == date("Y"))
<div class="row">
    <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title"><b>SIGC - Mediciones Año {{date("Y")}}</b></h5>

          </div>
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th  style="color:white">ID Medición</th>
                        <th nowrap style="color:white">Año</th>
                        <th nowrap style="color:white">Mes</th>
                        <th style="color:white">Tipo</th>
                        <th style="color:white">Equipo / Objetivo de Gestión</th>
                        <th style="color:white">Nombre Indicador</th>
                        <th nowrap style="color:white">Estado</th>
                        <th nowrap style="color:white">Fecha Revisión</th>
                        <th style="color:white">Detalles</th>
                    </tr>
                </thead>
                <tbody class="panel">
                  @if(count($dataDefinitiva)>0)
                        @foreach ($dataDefinitiva as $indice => $tramite)
                          <tr>
                           <td >{{$tramite->id}}</td>
                           <td nowrap>{{$tramite->Indicador->ano}}</td>
                           <td nowrap>{{$tramite->Calendario->Meses->nombre}}</td>
                           <td >{{$tramite->Indicador->Tipo->alias}}</td>
                           <td >{{$tramite->Indicador->Equipo->nombre}}</td>
                           <td >{{$tramite->Indicador->nombre}}</td>
                           <td nowrap>{{$tramite->MedicionEstado->nombre}}</td>
                           <td nowrap>{{$tramite->revision_fecha}}</td>
                           <td><center>
                            <a type="button" class="btn btn-primary" data-toggle="modal" title="Ver detalles" data-target="#modal-xl{{$tramite->id}}"><i class="fa fa-eye"></i></a>
                            <a type="button" class="btn btn-warning" title="Ver Historial" href="{{route('sigc_mediciones_estado',$tramite->id)}}"><i class="fa fa-clock-o"></i></a>
                            @if (auth()->user()->hasPermission(['09-08']))
                            <a type="button" class="btn btn-success" title="Editar Medición" href="{{route('sigc_editar_medicion',$tramite->id)}}"><i class="fa fa-pencil"></i></a>
                            @endif
                            @if (auth()->user()->hasPermission(['09-08']))
                            <a type="button" class="btn btn-danger" title="Archivos Adjuntos" href="{{route('sigc_mediciones_archivos_asociados',$tramite->id)}}"> <i class="fa fa-file"></i></a>
                            @endif
                           </center></td>
                          </tr>
                          @endforeach
                    @endif
                </tbody>
              </table>
          </div>
        @foreach ($dataDefinitiva as $indice => $tramite)
          <div class="modal fade" id="modal-xl{{$tramite->id}}">
            <div class="modal-dialog modal-xl"  style=" padding-top: 100px; ">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Detalles de la Medición {{$tramite->id}}</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <table id="example1" class="table table-striped">
                  <tr>
                      <td><b>Mes</b></td>
                      <td>{{$tramite->Calendario->Meses->nombre}}</td>
                  </tr>
                  <tr>
                      <td><b>Indicador</b></td>
                      <td>{{$tramite->Indicador->nombre}}</td>
                  </tr>
                  <tr>
                      <td><b>Fecha</b></td>
                      <td>{{$tramite->fecha}}</td>
                  </tr>
                  <tr>
                      <td><b>Valor numerador</b></td>
                      <td>{{$tramite->valor_numerador}}</td>
                  </tr>
                  <tr>
                      <td><b>Denominador</b></td>
                      @if($tramite->Indicador->categoria_id == 2)
                      <td>{{$tramite->denominador}}</td>
                      @else
                      <td>{{$tramite->Indicador->denominador}}</td>
                      @endif
                  </tr>
                  <tr>
                      <td><b>Analisis</b></td>
                      <td>{{$tramite->analisis}}</td>
                  </tr>
                  <tr>
                      <td><b>Acciones</b></td>
                      <td>{{$tramite->acciones}}</td>
                  </tr>
                  <tr>
                      <td><b>Medicion Estado</b></td>
                      <td>{{$tramite->MedicionEstado->nombre}}</td>
                  </tr>
                  <tr>
                      <td><b>Revision Fecha</b></td>
                      <td>{{$tramite->revision_fecha}}</td>
                  </tr>
                  <tr>
                      <td><b>Revision Obs.</b></td>
                      <td>{{$tramite->revision_obs}}</td>
                  </tr>

                  </table>
                  <table class='table table-striped'>
                      <thead>
                      <tr>
                      <th>Archivo(s)</th>
                      <th> </th>
                      <tr>
                      </thead>
                      <tbody>
                       @if ($tramite->archivos != null)
                        @if(count($tramite->archivos)>0)
                          @foreach($tramite->archivos as $key => $value)
                            @if($value->visible == 1)
                            <tr>
                            <td><small>{{$value->filename}}</small></td>
                            <td>

                            <a type="button" class="btn accion btn-success accion" href="{{route('sigc_descarga',$value->id)}}" target="_blank"><i class="fa fa-download"></i></a>
                            </td>
                            </tr>
                            @endif
                          @endforeach
                        @endif
                      @endif
                      </tbody>
                 </table>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /.modal -->
        @endforeach
        </div>
    </div>
    <div class="col-sm-12">
            <div class="pull-right">

              {{ $dataDefinitiva->links() }}

            </div>
      </div>
  </div>


@endif


@if(count($dataDefinitivaO)>0 && !($anoView == date("Y")))
<div class="row">
    <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">SIGC - Mediciones Años Anteriores</h5>

          </div>
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                      <th nowrap style="color:white">ID Medición</th>
                      <th nowrap style="color:white">Año</th>
                      <th nowrap style="color:white">Mes</th>
                      <th nowrap style="color:white">Tipo</th>
                      <th nowrap style="color:white">Equipo / Objetivo de Gestión</th>
                      <th style="color:white">Nombre Indicador</th>
                      <th nowrap style="color:white">Estado</th>
                      <th nowrap style="color:white">Fecha Revisión</th>
                      <th style="color:white">Detalles</th>
                    </tr>
                </thead>
                <tbody class="panel">
                  @if(count($dataDefinitivaO)>0)
                        @foreach ($dataDefinitivaO as $indice => $tramite)
                        <tr>
                         <td nowrap>{{$tramite->id}}</td>
                         <td nowrap>{{$tramite->Indicador->ano}}</td>
                         <td nowrap>{{$tramite->Calendario->Meses->nombre}}</td>
                         <td nowrap>{{$tramite->Indicador->Tipo->alias}}</td>
                         <td nowrap>{{$tramite->Indicador->Equipo->nombre}}</td>
                         <td>{{$tramite->Indicador->nombre}}</td>
                         <td nowrap>{{$tramite->MedicionEstado->nombre}}</td>
                         <td nowrap>{{$tramite->revision_fecha}}</td>
                         <td><center>
                          <a type="button" class="btn btn-primary" title="Ver detalles" data-toggle="modal" data-target="#modal-xl{{$tramite->id}}"><i class="fa fa-eye"></i></a>
                          @if (auth()->user()->hasPermission(['09-08']))
                          <a type="button" class="btn btn-success" title="Editar Medición" href="{{route('sigc_editar_medicion',$tramite->id)}}"><i class="fa fa-pencil"></i></a>
                          @endif

                          <a type="button" class="btn btn-danger" title="Archivos Adjuntos" href="{{route('sigc_mediciones_archivos_asociados',$tramite->id)}}"> <i class="fa fa-file"></i></a>

                         </center></td>
                         @endforeach
                   @endif
               </tbody>
             </table>
         </div>
       @foreach ($dataDefinitivaO as $indice => $tramite)
         <div class="modal fade" id="modal-xl{{$tramite->id}}">
           <div class="modal-dialog modal-xl"  style="padding-top: 100px;">
             <div class="modal-content"  style="width: 100%;">
               <div class="modal-header">
                 <h4 class="modal-title">Detalles de la Medición {{$tramite->id}}</h4>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
                 </button>
               </div>
               <div class="modal-body">
                 <table id="example1" class="table table-striped">
                 <tr>
                     <td><b>Mes</b></td>
                     <td >{{$tramite->Calendario->Meses->nombre}}</td>
                 </tr>
                 <tr>
                     <td><b>Indicador</b></td>
                     <td>{{$tramite->Indicador->nombre}}</td>
                 </tr>
                 <tr>
                     <td><b>Fecha</b></td>
                     <td>{{$tramite->fecha}}</td>
                 </tr>
                 <tr>
                     <td><b>Valor numerador</b></td>
                     <td>{{$tramite->valor_numerador}}</td>
                 </tr>
                 <tr>
                     <td><b>Denominador</b></td>
                     @if($tramite->Indicador->categoria_id == 2)
                     <td>{{$tramite->denominador}}</td>
                     @else
                     <td>{{$tramite->Indicador->denominador}}</td>
                     @endif
                 </tr>
                 <tr>
                     <td><b>Analisis</b></td>
                     <td>{{$tramite->analisis}}</td>
                 </tr>
                 <tr>
                     <td><b>Acciones</b></td>
                     <td>{{$tramite->acciones}}</td>
                 </tr>
                 <tr>
                     <td><b>Medicion Estado</b></td>
                     <td>{{$tramite->MedicionEstado->nombre}}</td>
                 </tr>
                 <tr>
                     <td><b>Revision Fecha</b></td>
                     <td>{{$tramite->revision_fecha}}</td>
                 </tr>
                 <tr>
                     <td><b>Revision Obs.</b></td>
                     <td>{{$tramite->revision_obs}}</td>
                 </tr>

                 </table>
                 <table class='table table-striped'>
                     <thead>
                     <tr>
                     <th>Archivo(s)</th>
                     <th> </th>
                     <tr>
                     </thead>
                     <tbody>
                      @if ($tramite->archivos != null)
                       @if(count($tramite->archivos)>0)
                         @foreach($tramite->archivos as $key => $value)
                           @if($value->visible == 1)
                           <tr>
                           <td><small>{{$value->filename}}</small></td>
                           <td>
                           <a type="button" class="btn accion btn-success accion" href="{{route('sigc_descarga',$value->id)}}" target="_blank"><i class="fa fa-download"></i></a>
                           </td>
                           </tr>
                           @endif
                         @endforeach
                       @endif
                     @endif
                     </tbody>
                </table>
               </div>
             </div>
             <!-- /.modal-content -->
           </div>
           <!-- /.modal-dialog -->
         </div>
         <!-- /.modal -->
       @endforeach
       </div>
       <div class="col-sm-12">
               <div class="pull-right">

                 {{ $dataDefinitivaO->links() }}

               </div>
         </div>
    </div>
  </div>

@endif
@endsection
@section('before-scripts-end')

<script type="text/javascript">
$(document).ready(function(){
  $('#ano').on('change', function(){
        var ano = $(this).val();
        if(ano !== ''){
          $.get('/SIGC/Mediciones/GetTipoPorAno/'+ano , function(tipos){
            console.log('tipos de acuerdo al ' ,tipos)
            $('#tipo').empty();
            $('#tipo').append("<option value>SELECCIONE TIPO</option>");
            $.each(tipos, function(index , value){
            $('#tipo').append("<option value='"+index+"'>"+value+" </option>");
            })
          })

        }else{
           $('#tipo').empty();
        }
  });
});

$(document).ready(function(){
  $('#ano').on('change', function(){
        var ano = $(this).val();
        if(ano !== ''){
          $.get('/SIGC/Mediciones/GetEquiposPorAno/'+ano , function(equipos){
            $('#equipo').empty();
            $('#equipo').append("<option value>SELECCIONE EQUIPO</option>");
            $.each(equipos, function(index , value){
            $('#equipo').append("<option value='"+index+"'>"+value+" </option>");
            })
          })

        }else{
           $('#equipo').empty();
        }
  });
});


$(document).ready(function(){
  $('#ano').on('change', function(){
        var equipo = 0;
        var tipo = 0;
        var ano = $(this).val();
        if($("#equipo").val()) {
              equipo = $("#equipo").val();
        }
        if($("#tipo").val()) {
              tipo = $("#tipo").val();
        }



        console.log('dddddddd' ,equipo , tipo)
        if(ano !== ''){
          $.get('/SIGC/Mediciones/GetIndicadoresSelect/'+ano+'/'+tipo+'/'+equipo, function(indicadores){
            $('#indicador').empty();
            $('#indicador').append("<option value>SELECCIONE INDICADORES</option>");
            $.each(indicadores, function(index , value){
            $('#indicador').append("<option value='"+index+"'>"+value+" </option>");
            })
          })

        }else{
           $('#indicador').empty();
        }
  });
});


$(document).ready(function(){
  $('#tipo').on('change', function(){
        var equipo = 0;
        var ano = 0;
        var tipo = $(this).val();

        if(tipo !== ''){ 
              $.get('/SIGC/Inicio/SelectEquipo/'+tipo, function(equipos){
                $('#equipo').empty();
                $('#equipo').append("<option value>SELECCIONE EQUIPOS</option>");
                $.each(equipos, function(index , value){
                $('#equipo').append("<option value='"+index+"'>"+value+" </option>");
                })
              })
            }else{
              $.get('/SIGC/SelectEquipos', function(equipos){
                $('#equipo').empty();
                $('#equipo').append("<option value>SELECCIONE EQUIPOS</option>");
                $.each(equipos, function(index , value){
                $('#equipo').append("<option value='"+index+"'>"+value+" </option>");
                })
              })
            }


        if($("#equipo").val()) {
              equipo = $("#equipo").val();
        }
        if($("#ano").val()) {
              ano = $("#ano").val();
        }



        console.log('dddddddd' ,equipo , tipo)
        if(tipo !== ''){
          $.get('/SIGC/Mediciones/GetIndicadoresSelect/'+ano+'/'+tipo+'/'+equipo, function(indicadores){
            $('#indicador').empty();
            $('#indicador').append("<option value>SELECCIONE INDICADORES</option>");
            $.each(indicadores, function(index , value){
            $('#indicador').append("<option value='"+index+"'>"+value+" </option>");
            })
          })

        }else{
           $('#indicador').empty();
        }
  });
});

$(document).ready(function(){
  $('#equipo').on('change', function(){
        var tipo = 0;
        var ano = 0;
        var equipo = $(this).val();

        if($("#tipo").val()) {
              tipo = $("#tipo").val();
        }
        if($("#ano").val()) {
              ano = $("#ano").val();
        }
        console.log('dddddddd' ,equipo , tipo)
        if(equipo !== ''){
          $.get('/SIGC/Mediciones/GetIndicadoresSelect/'+ano+'/'+tipo+'/'+equipo, function(indicadores){
            $('#indicador').empty();
            $('#indicador').append("<option value>SELECCIONE INDICADORES</option>");
            $.each(indicadores, function(index , value){
            $('#indicador').append("<option value='"+index+"'>"+value+" </option>");
            })
          })

        }else{
           $('#indicador').empty();
        }
  });
});
</script>

@stop
