@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-line-chart"></i><b> SIGC - Archivos Asociado a la Medicion</b></h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h3 class="card-title"><b> Medición N° {{$editRevision->id}}</b></h3>
      </div>
            <div class="card-body">
                    <div class="box-body">
                      <table class="table table-striped">
                          <tr>
                              <td style="color:white; width:260px; text-align: center; background-color: #013273;"><b>Fecha</b></td>
                              <td>{{$editRevision->fecha}}</td>
                          </tr>
                          <tr>
                              <td style="color:white; width:260px; text-align: center; background-color: #013273;"><b>Nombre Indicador</b></td>
                              <td>{{$editRevision->indicador->nombre}}</td>
                          </tr>
                          <tr>
                              <td style="color:white; width:260px; text-align: center; background-color: #013273;"><b>Mes</b></td>
                              <td>{{$editRevision->mescalendario}}</td>
                          </tr>
                          <tr>
                              <td style="color:white; width:260px; text-align: center; background-color: #013273;"><b>Numerador</b></td>
                              <td>{{$editRevision->valor_numerador}}</td>
                          </tr>
                          <tr>
                              <td style="color:white; width:260px; text-align: center; background-color: #013273;"><b>Análisis</b></td>
                              <td>{{$editRevision->analisis}}</td>
                          </tr>
                          <tr>
                              <td style="color:white; width:260px; text-align: center; background-color: #013273;"><b>Accciones</b></td>
                              <td>{{$editRevision->acciones}}</td>
                          </tr>
                          <tr>
                              <td style="color:white; width:260px; text-align: center; background-color: #013273;"><b>Estado</b></td>
                              <td>{{$editRevision->medicionestado->nombre}}</td>
                          </tr>
                        </table>
                    </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h3 class="card-title"><b>SIGC - Archivos Asociados a la Medición N° {{$editRevision->id}}</b></h3>

          </div>
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th nowrap style="color:white">Id</th>
                        <th nowrap style="color:white">Nombre Archivo</th>
                        <th nowrap style="color:white">Ver/Descargar</th>
                        <th nowrap style="color:white">Eliminar</th>
                    </tr>
                </thead>
                <tbody class="panel">

                @if ($editRevision->archivos != null)
                  @if(count($editRevision->archivos)>0)
                  @foreach($editRevision->archivos as $key => $value)
                  @if($value->visible == 1)
                  <tr>
                  <td>{{$value->id}}</td>
                  <td>{{$value->filename}}</td>
                  <td><a type="button" title="Descargar" class="btn accion btn-success accion" href="{{route('sigc_descarga',$value->id)}}" target="_blank"><i class="fa fa-download"></i></a></td>
                  <td><a title="Eliminar" class="btn btn-sm btn-danger"  OnClick="Eliminar5('{{$value->id}}' , '{{$editRevision->id}}')"><i class="fa fa-trash"></i></a></td>
                </tr>
                @endif
                @endforeach
                @endif
                @endif

                </tbody>
              </table>
          </div>
        </div>
    </div>
@endsection
@section('before-scripts-end')
@include('includes.scripts.sigc.script_mediciones')
@stop
