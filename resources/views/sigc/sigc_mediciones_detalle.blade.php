
@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-5">
      <h1><i class="fa fa-user"></i> SIGC -Mediciones</h1>
    </div>
    <div class="col-sm-7">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-user"></i> SIGC -Mediciones</li>
        <li class="breadcrumb-item active">Detalles</li>
      </ol>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <!-- Line chart -->
    <div class="card card-danger card-outline">
      <div class="card-header">
        <h3 class="card-title">Detalles Medición</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body">

        <br>
        <h4><b>Medición N° {{$data->id}}</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Fecha:</b></td>
              <td nowrap>{{$data->fecha}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Indicador:</b></td>
              <td nowrap>{{$data->nombre_indicador}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Numerador:</b></td>
              <td nowrap>{{$data->valor_numerador}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Denominador Variable:</b></td>
              <td nowrap>{{$data->denominador}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Análisis:</b></td>
              <td>{{$data->analisis}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Acciones:</b></td>
              <td>{{$data->acciones}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Estado:</b></td>
              <td nowrap>{{$data->nombre_medicion_estado}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>fecha Revisión:</b></td>
              <td nowrap>{{$data->revision_fecha}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Observaciones Revisión:</b></td>
              <td>{{$data->revision_obs}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Archivos:</b></td>
              <td>{{$data->archivos->filename}}</td>
          </tr>
          </table>
          <br>


          @if($data->plataforma == 'v')
            @if(property_exists($data, 'archivos'))

            <br>
        <h4><b>Medios de Verificación</b></h4>
        <br>
          <table id="example1" class="table table-striped">

                @foreach($data->archivos as $indice => $archivo)
                <tr>
                  <td>
                    {{ str_replace('"', '' , $archivo->filename)  }}
                  </td>
                  <td>
                   <!--<a href="https://sigc.s3.amazonaws.com/medicion_archivo/{{$archivo->id}}/{{ str_replace('"', '' , $archivo->filename) }}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>-->
                   <a href="" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
                  </td>
               </tr>
                @endforeach
            @endif
          @endif

        </table>
      </div>

        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! link_to_route('sigc_mediciones', 'Volver', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
            </div>
          </div>
        </div>
    </div>
  </div>
</div>


@endsection
@section('before-scripts-end')
@stop
