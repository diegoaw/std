@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-clock-o"></i><b> SIGC - Estado de la Medición {{$dataDefinitiva->id}}<b></h1><br>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
<style>

/******************* Timeline Demo - 8 *****************/
.main-timeline8{overflow:hidden;position:relative}
.main-timeline8:after,.main-timeline8:before{content:"";display:block;width:100%;clear:both}
.main-timeline8:before{content:"";width:3px;height:100%;background:#d6d5d5;position:absolute;top:30px;left:50%}
.main-timeline8 .timeline2{width:50%;float:left;padding-right:30px;position:relative}
.main-timeline8 .timeline2-icon{width:32px;height:32px;border-radius:50%;background:#fff;border:3px solid #b0090c;position:absolute;top:5.5%;right:-17.5px}
.main-timeline8 .year{display:block;padding:10px;margin:0;font-size:30px;color:#fff;border-radius:0 50px 50px 0;background:#b0090c;text-align:center;position:relative}
.main-timeline8 .year:before{content:"";border-top:35px solid #f57f93;border-left:35px solid transparent;position:absolute;bottom:-35px;left:0}
.main-timeline8 .timeline2-content{padding:30px 20px;margin:0 45px 0 35px;background:#f2f2f2}
.main-timeline8 .title{font-size:19px;font-weight:700;color:#504f54;margin:0 0 10px}
.main-timeline8 .description{font-size:14px;color:#7d7b7b;margin:0}
.main-timeline8 .timeline2:nth-child(2n){padding:0 0 0 30px}
.main-timeline8 .timeline2:nth-child(2n) .timeline2-icon{right:auto;left:-14.5px}
.main-timeline8 .timeline2:nth-child(2n) .year{border-radius:50px 0 0 50px;background:#239c1f}
.main-timeline8 .timeline2:nth-child(2n) .year:before{border-left:none;border-right:35px solid transparent;left:auto;right:0}
.main-timeline8 .timeline2:nth-child(2n) .timeline2-content{text-align:right;margin:0 35px 0 45px}
.main-timeline8 .timeline2:nth-child(2){margin-top:170px}
.main-timeline8 .timeline2:nth-child(odd){margin:-175px 0 0}
.main-timeline8 .timeline2:nth-child(even){margin-bottom:80px}
.main-timeline8 .timeline2:first-child,.main-timeline8 .timeline2:last-child:nth-child(even){margin:0}
.main-timeline8 .timeline2:nth-child(2n) .timeline2-icon{border-color:#239c1f}
.main-timeline8 .timeline2:nth-child(2n) .year:before{border-top-color:#8abf90}
.main-timeline8 .timeline2:nth-child(3n) .timeline2-icon{border-color:#2c60bf}
.main-timeline8 .timeline2:nth-child(3n) .year{background:#2c60bf}
.main-timeline8 .timeline2:nth-child(3n) .year:before{border-top-color:#8fade3}
.main-timeline8 .timeline2:nth-child(4n) .timeline2-icon{border-color:#bf9602}
.main-timeline8 .timeline2:nth-child(4n) .year{background:#bf9602}
.main-timeline8 .timeline2:nth-child(4n) .year:before{border-top-color:#cfbe86}
@media only screen and (max-width:767px){.main-timeline8{overflow:visible}
.main-timeline8:before{top:0;left:0}
.main-timeline8 .timeline2:nth-child(2),.main-timeline8 .timeline2:nth-child(even),.main-timeline8 .timeline2:nth-child(odd){margin:0}
.main-timeline8 .timeline2{width:100%;float:none;padding:0 0 0 30px;margin-bottom:20px!important}
.main-timeline8 .timeline2:last-child{margin:0!important}
.main-timeline8 .timeline2-icon{right:auto;left:-14.5px}
.main-timeline8 .year{border-radius:50px 0 0 50px}
.main-timeline8 .year:before{border-left:none;border-right:35px solid transparent;left:auto;right:0}
.main-timeline8 .timeline2-content{margin:0 35px 0 45px}
}
</style>
@endsection
@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-danger card-outline">
        <h3 class="card-title"><b>{{$dataDefinitiva->Indicador->nombre}} </b></h3>
      </div>
      <div class="card-body">



          <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="main-timeline8">


                      @foreach ($dataDefinitivaHisto as $indice => $tramite)
                        <div class="timeline2">
                            <span class="timeline2-icon"></span>
                            <span class="year">Medición {{$tramite->MedicionEvento->nombre}}</span>
                            <div class="timeline2-content">
                                <h3 class="title">{{$tramite->fecha}}</h3>
                                <p class="description">
                                  Medición {{$tramite->MedicionEvento->nombre}} por {{$tramite->usuario}}
                                </p>
                                @if($tramite->revision_obs)
                                <p class="description">
                                  <b>Observaciones Revisión: </b> {{$tramite->revision_obs}}
                                </p>
                                @endif
                            </div>
                        </div>
                      @endforeach

                    </div>
                </div>
            </div>
        </div>

        <hr>

        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              <a type="button" class="btn btn-danger" href="{{url()->previous()}}"> volver </a>
            </div>
          </div>
        </div>


      </div>
    </div>
  </div>
</div>

  @stop

@section('after-scripts-end')
@endsection
