@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-file-text"></i> SIGC - Histórico Mediciones</h1><br> 
    </div>
    
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title">Filtros SIGC - Histórico Mediciones</h5>        
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">

               {!! Form::open(['route' => 'sigc_mediciones_historico', 'method' => 'get' , 'class'=>"form-horizontal"]) !!}
                  <div class="form-row">
                   
                    <div class="col-sm-3">
                      <label for="tipo" class=" col-form-label">Tipo Indicador: </label>
                      {!! Form::select('tipo',$selectTipo,$tipoView,['id'=>'tipo','name'=>'tipo', 'class'=>'form-control']) !!}
                    </div>

                    <div class="col-sm-3">
                      <label for="equipo" class=" col-form-label">Equipo / División: </label>
                      {!! Form::select('equipo',$selectEquipo,$equipoView,['id'=>'equipo','name'=>'equipo', 'class'=>'form-control']) !!}
                    </div>


                    <div class="col-sm-3">
                      <label for="mes" class=" col-form-label">Mes: </label>
                      {!! Form::select('equipo',$selectMes,$mesView,['id'=>'mes','name'=>'mes', 'class'=>'form-control']) !!}
                    </div>

                  </div>
                  <div class="col-sm-12">
                  <div class="text-right">
                  <input type="submit" class= "btn btn-primary" name="enviar">
                  </div>
                  </div>

            {!! Form::close() !!} 

          </div>  
        </div> 
      </div> 
    </div>
  </div>
</div>

@if(count($dataDefinitiva)>0)
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">SIGC - Histórico Mediciones</h5>
            
          </div>
         @foreach ($dataDefinitiva as $indice2 => $indicador)
          <br><br>
          <h6><b> {{$indice2}} - {{$dataSinFiltro->where('indicador_id' , $indice2)->first()->nombre_indicador}} </b></h6>
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th nowrap style="color:white">Mes</th>
                        <th nowrap style="color:white">ID Medición</th>
                        <th nowrap style="color:white">ID Indicador</th>
                        <th nowrap style="color:white">Indicador</th>
                        <th nowrap style="color:white">Detalle</th>
                    </tr>
                </thead>
                <tbody class="panel">
               
                         @foreach ($indicador as $indice => $tramite)
                          <tr>
                           <td nowrap>{{$tramite->mes}}</td>
                           <td nowrap>{{$tramite->id}}</td>
                           <td nowrap>{{$tramite->indicador_id}}</td>
                           <td nowrap>{{$tramite->nombre_indicador}}</td>
                           <td>
                             <center>
                              <a  title="Ver detalles" class="btn accion btn-info accion" href="{{route('sigc_mediciones_historico_detalle',$tramite->id)}}"><i class="fa fa-eye"></i></a>
                          </center>
                           </td>
                          </tr>
                          @endforeach
               
                </tbody>
              </table>
          </div>   
        @endforeach

@endif
@endsection
@section('before-scripts-end')
@stop