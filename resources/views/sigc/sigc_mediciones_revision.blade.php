@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-check-square-o"></i><b> SIGC - Mediciones del Mes por Revisar</b></h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')


<div class="row">
    <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title"><b>SIGC - Mediciones {{date("Y")}}</b></h5>

          </div>
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th nowrap style="color:white">ID Medición</th>
                        <th nowrap style="color:white">Año</th>
                        <th nowrap style="color:white">Mes</th>
                        <th nowrap style="color:white">Tipo</th>
                        <th nowrap style="color:white">Equipo</th>
                        <th style="color:white">Nombre Indicador</th>
                        <th nowrap style="color:white">Estado</th>
                        <th nowrap style="color:white">Fecha Revisión</th>
                        <th style="color:white"><center>Detalles</center></th>
                        <th style="color:white"><center>Acciones</center></th>
                    </tr>
                </thead>
                <tbody class="panel">
                  @if(count($dataDefinitiva)>0)
                        @foreach ($dataDefinitiva as $indice => $tramite)
                          <tr>
                           <td nowrap>{{$tramite->id}}</td>
                           <td nowrap>{{$tramite->Indicador->ano}}</td>
                           <td nowrap>{{$tramite->Calendario->Meses->nombre}}</td>
                           <td nowrap>{{$tramite->Indicador->Tipo->alias}}</td>
                           <td nowrap>{{$tramite->Indicador->Equipo->nombre}}</td>
                           <td nowrap>{{$tramite->Indicador->nombre}}</td>
                           <td nowrap>{{$tramite->MedicionEstado->nombre}}</td>
                           <td nowrap>{{$tramite->revision_fecha}}</td>
                           <td><center>
                            <a type="button" class="btn btn-primary" title="Ver Detalles" data-toggle="modal" data-target="#modal-xl{{$tramite->id}}"><i class="fa fa-eye"></i></a>
                           </center></td>
                           <td>
                           @if($habititarRevisar && $tramite->medicion_estado_id == 5)
                            <a class="btn accion btn-success accion" href="{{route('sigc_revision',$tramite->id)}}">Revisar</a>
                           @endif
                           @if($habititarRevisarCorrecion && $tramite->medicion_estado_id == 3)
                            <a class="btn accion btn-success accion" href="{{route('sigc_editar_revision',$tramite->id)}}">Revisar corrección</a>
                          @endif
                           </td>



                          </tr>
                          @endforeach
                    @endif
                </tbody>
              </table>
          </div>
        @foreach ($dataDefinitiva as $indice => $tramite)
          <div class="modal fade" id="modal-xl{{$tramite->id}}">
            <div class="modal-dialog modal-xl"  style=" padding-top: 100px; ">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><b>Detalles de la Medición {{$tramite->id}}</b></h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <table id="example1" class="table table-striped">
                    <tr>
                        <td><b>Mes</b></td>
                        <td>{{$tramite->Calendario->Meses->nombre}}</td>
                    </tr>
                    <tr>
                        <td><b>Indicador</b></td>
                        <td>{{$tramite->Indicador->nombre}}</td>
                    </tr>
                    <tr>
                        <td><b>Fecha</b></td>
                        <td>{{$tramite->fecha}}</td>
                    </tr>
                    <tr>
                        <td><b>Valor numerador</b></td>
                        <td>{{$tramite->valor_numerador}}</td>
                    </tr>
                    <tr>
                        <td><b>Denominador</b></td>
                        @if($tramite->Indicador->categoria_id == 2)
                        <td>{{$tramite->denominador}}</td>
                        @else
                        <td>{{$tramite->Indicador->denominador}}</td>
                        @endif
                    </tr>
                    <tr>
                        <td><b>Analisis</b></td>
                        <td>{{$tramite->analisis}}</td>
                    </tr>
                    <tr>
                        <td><b>Acciones</b></td>
                        <td>{{$tramite->acciones}}</td>
                    </tr>
                    <tr>
                        <td><b>Medicion Estado</b></td>
                        <td>{{$tramite->MedicionEstado->nombre}}</td>
                    </tr>
                    <tr>
                        <td><b>Revision Fecha</b></td>
                        <td>{{$tramite->revision_fecha}}</td>
                    </tr>
                    <tr>
                        <td><b>Revision Obs.</b></td>
                        <td>{{$tramite->revision_obs}}</td>
                    </tr>
                  </table>
                  <table class='table table-striped'>
                      <thead>
                      <tr>
                      <th>Archivo(s)</th>
                      <th> </th>
                      <tr>
                      </thead>
                      <tbody>
                       @if ($tramite->archivos != null)
                        @if(count($tramite->archivos)>0)
                          @foreach($tramite->archivos as $key => $value)
                            @if($value->visible == 1)
                            <tr>
                            <td><small>{{$value->filename}}</small></td>
                            <td>

                            <a type="button" class="btn accion btn-success accion" href="{{route('sigc_descarga',$value->id)}}" target="_blank"><i class="fa fa-download"></i></a>
                            </td>
                            </tr>
                            @endif
                          @endforeach
                        @endif
                      @endif
                      </tbody>
                 </table>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /.modal -->
        @endforeach
        </div>
    </div>
  </div>


@endsection
@section('before-scripts-end')
@stop
