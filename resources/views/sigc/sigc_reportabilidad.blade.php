@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-list"></i><b> SIGC - Indicadores Reportados y Sin Reportar</b></h1><br>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')

@if(count($dataDefinitiva)>0)
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title"><b>SIGC - Indicadores - Estado Reportes</b></h5>

          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                      <th nowrap style="color:white">ID</th>
                        <th nowrap style="color:white">Año</th>
                        <th nowrap style="color:white">Tipo</th>
                        <th nowrap style="color:white">Nombre</th>
                        <th nowrap style="color:white">Reportador</th>
                        <th style="color:white">Estatus Medición</th>
                        <th style="color:white">Estatus</th>
                        <th nowrap style="color:white">Acciones</th>
                    </tr>
                </thead>
                <tbody class="panel">
                  @if(count($dataDefinitiva)>0)
                         @foreach ($dataDefinitiva as $indice => $tramite)
                          <tr>
                           <td nowrap>{{$tramite->id}}</td>
                           <td nowrap>{{$tramite->ano}}</td>
                           <td >{{$tramite->tipo->alias}}</td>
                           <td>{{$tramite->nombre}}</td>
                           <td>{{$tramite->reportador1->email}}</td>
                           <td>{{$tramite->estatus_medicion}}</td>
                           <td>@if($tramite->reportado) <b style="color:green">Reportado</b> @else <b style="color:red">Sin Reportar @endif</b></td>
                           <td><center>
                           @if($tramite->reportado == false)
                            <a class="btn accion btn-success accion" title="Enviar Aviso" href="{{route('sigc_no_reporto',$tramite->id)}}"><i class="fa fa-envelope-o"></i></a>
                           @endif
                           </center></td>
                          </tr>
                          @endforeach
                  @endif
                </tbody>
              </table>
          </div>
@endif

@endsection
@section('before-scripts-end')
@stop
