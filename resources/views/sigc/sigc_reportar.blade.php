@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-sliders"></i> SIGC - Reportar Indicador</h1><br>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h4 class="card-title"><b>DATOS INDICADOR</b></h4>
      </div>
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">
              <table class='table table-striped'>
                <thead>
                  <tr>
                    <th scope='col'>Año</th>
                    <th scope='col'>Nombre</th>
                    <th scope='col'>Meta</th>
                    <th scope='col'>Equipo</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>{{ $indicador->ano }}</td>
                    <td>{{ $indicador->nombre }}</td>
                    <td>{{ $indicador->meta }}</td>
                    <td>{{ $indicador->equipo->nombre }}</td>
                  </tr>
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

{!! Form::open(['route' => ['sigc_reportada' , $indicadorView], 'files' => true, 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','id'=>'form-create']) !!}
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-danger card-outline">
        <h4 class="card-title"><b>REPORTE</b></h4>
      </div>
      <div class="card-body">

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>

        <div class="callout col-sm-8 offset-sm-2" style="color: #737373;">
          <h4 style="font-size: 14px;"><i class="fa fa-info-circle" style="color: #ff0000;"></i> Nota:</h4>
          <span>Los campos marcados con un asterisco (*) son obligatorios.</span>
        </div><br>

        <div class="form-group row">
           {!! Form::label('Indicador', 'Indicador(*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
          {!! Form::select('indicador',$selectIndicador,$indicadorView,['id'=>'indicador','name'=>'indicador', 'class'=>'form-control' , 'disabled'=>'disabled']) !!}
          </div>
        </div>


        <!--Numerador y Nombre-->
        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            <h4>{{ $indicador->numerador}}</h4>
          </div>
        </div>
        <div class="form-group row">
          {!! Form::label('Numerador', 'Numerador(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
          {!! Form::text('valorNumerador', null, ['class' => 'form-control', 'placeholder' => 'Numerador' , 'id'=>'valorNumerador','name'=>'valorNumerador' , 'onchange'=>"calcular($indicador->id)"]) !!}
          </div>
        </div>

        <!--Denominador-->
        @if( $indicador->categoria_id == 1)
        <div class="form-group row">
          {!! Form::label('Denominador', 'Denominador(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
          {!! Form::text('denominador', $indicador->denominador , ['class' => 'form-control', 'placeholder' => 'Denominador', 'id'=>'denominador','name'=>'denominador', 'readonly'=>'readonly']) !!}
          </div>
        </div>
        @else
          <div class="form-group row">
          {!! Form::label('Denominador', 'Denominador(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
          {!! Form::text('denominador', null , ['class' => 'form-control', 'placeholder' => 'Denominador' ,'id'=>'denominador','name'=>'denominador' , 'onchange'=>"calcular($indicador->id)"]) !!}
          </div>
        </div>
        @endif
        <!--Resultado-->
        <div class="form-group row">
          {!! Form::label('Resultado', 'Resultado(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
          {!! Form::text('resultado', null, ['class' => 'form-control', 'placeholder' => 'Resultado', 'readonly' => 'readonly' , 'id'=>'resultado','name'=>'resultado']) !!}
          </div>
        </div>

        <!--Análisis-->
        <div class="form-group row">
          {!! Form::label('Análisis', 'Análisis(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
          {!! Form::textarea('analisis', null, ['class' => 'form-control', 'placeholder' => 'Análisis']) !!}
          </div>
        </div>

        <!--Acciones-->
        <div class="form-group row">
          {!! Form::label('Acciones', 'Acciones(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
          {!! Form::textarea('acciones', null, ['class' => 'form-control', 'placeholder' => 'Acciones']) !!}
          </div>
        </div>

        <!--¿Desea Cargar Medios de Verificación?-->
        <div class="form-group row">
          <!--{!! Form::label('status','¿Desea Cargar Medios de Verificación?', ['class' => 'col-sm-3 col-form-label']) !!}-->
          <div class="col-sm-6">
          <!--{!! Form::checkbox('status', '1', true) !!}-->
          </div>
        </div>

        <!--Medios de Verificación y Nombre-->
        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            <h4>{{ $indicador->medio_verif}}</h4>
          </div>
        </div>
        <div class="field_wrapper">
        <div class="form-group row col-md-12">
          {!! Form::label('filename', 'Medios de Verificación', ['class' => 'col-lg-3 col-form-label']) !!}
          <div class="col-md-6">
          {!! Form::file('filename[]', ['class' => 'form-control']) !!}
          <a href="javascript:void(0);" class="add_button" title="Add field"><i class="fa fa fa-plus"></i></a>
          </div>
        </div>
      </div>


        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! Form::submit('Guardar', ['class' => 'btn btn-success btn-sm','title'=>'Guardar']) !!}
              {!! link_to_route('sigc_indicadores_usuario', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) !!}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{!! Form::close() !!}
@stop
@section('after-scripts-end')
<script type="text/javascript">
 $("#valorNumerador").keypress(soloNumeros);
 $("#denominador").keypress(soloNumeros);

  function soloNumeros(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57  ) && charCode!=44)
      return false;

    return true;
  }

  function calcular(idIndicador){

      let num  = $("#valorNumerador").val();
      let den  = $("#denominador").val();
      let ind = idIndicador;

      if(num !== null && num !== '' && den !== null && den !== ''){
          let exitC = num.indexOf(',');
          if (exitC != -1){
            num = num.replace(',', '.');
          }
          num = parseFloat(num);

          let exitCd = den.indexOf(',');
          if (exitCd != -1){
            den = den.replace(',', '.');
          }
          den = parseFloat(den);

          $.get('/SIGC/Reportar/Calcular/Resultado/'+num+'/'+den+'/'+ind , function(resultado){
            let resultadoFinal = resultado;
            $("#resultado").val(resultadoFinal.toFixed(2));
          });
      }
  }


</script>

<script type="text/javascript">
$(document).ready(function(){
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    var fieldHTML = '<div class="form-group row col-md-12"><div class="col-md-3"></div><div class="col-md-6">{!! Form::file("filename[]",["class" => "form-control"]) !!}<a href="javascript:void(0);" class="remove_button" title="Remove field"><i class="fa fa fa-minus"></i></a></div></div>'; //New input field html
    var x = 1; //Initial field counter is 1
    $(addButton).click(function(){ //Once add button is clicked
        if(x < maxField){ //Check maximum number of input fields
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); // Add field html
        }
    });
    $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
</script>
@endsection
