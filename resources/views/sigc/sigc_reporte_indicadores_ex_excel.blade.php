<table>
  <tr>
   <th colspan="7" style="text-align: center;">MINISTERIO DEL DEPORTE</th>
  </tr>
  <tr>
   <th colspan="7" style="text-align: center;">DIVISIÓN DE PLANIFICACIÓN</th>
  </tr>
  <tr>
   <th colspan="7" style="text-align: center;">SIGC - {{$nombreMes}}</th>
  </tr>
</table >
@foreach ($indicadores as $tipo => $equipos)
<h4 colspan="7"><center><b>{{$tipo}}</b></center></h4>
  @foreach ($equipos as $nombreEquipo => $arrayindicadores)
  <?php
    $sumatoriaEquipo = 0;
   ?>
  <table>
    <thead >
      <tr>
        <th colspan="7" style="color:#ffff; text-align: center; background-color: #013273;"><b>{{$nombreEquipo}}</b></th>
        <th style="color:#ffff; text-align: center; background-color: #013273;"><b>Estado Mes Actual</b></th>
        <th style="color:#ffff; text-align: center; background-color: #013273;"><b>Meta 2022</b></th>
        <th style="color:#ffff; text-align: center; background-color: #013273;"><b>Avance Cumplimiento de la Meta</b></th>
        <th style="color:#ffff; text-align: center; background-color: #013273;"><b>Ponderación 2022</b></th>
        <th style="color:#ffff; text-align: center; background-color: #013273;"><b>Avance Ponderado de la Meta</b></th>
      </tr>
    </thead>
    <tbody>
      @foreach ($arrayindicadores as $indice => $tramite)
      <tr>
        <td colspan="7"><center>{{$tramite->nombre}}</center></td>
        <td ><center>{{$tramite->estado}}</center></td>
        <td ><center>
          @if($tramite->unidad_id == 2)
            @if($tramite->meta_calculada == 0)
            Mide (indicador sin meta asociada)
            @else
            {{$tramite->meta_calculada}} días
            @endif
          @else
            @if($tramite->meta_calculada == 0)
            Mide (indicador sin meta asociada)
            @else
            {{$tramite->meta_calculada}} %
            @endif
          @endif
        </center></td>
        <td ><center>
          @if($tramite->unidad_id_meta == 5)
            {{$arregloGraficaCumplimiento[$tramite->id][$mesO]}} %
          @else
            {{$arregloGraficaCumplimiento[$tramite->id][$mesO]}}
          @endif
        </center></td>
        <td ><center>
          @if($tramite->equipo_id == 64)
            No Aplica
          @else
            {{$tramite->ponderador}} %
          @endif
        </center></td>
        <td ><center>
        @if($tramite->equipo_id == 64)
          No Aplica
        @else
          @if($tramite->reportada == true)
           {{$tramite->ponderador_calculado_otro}} %
          @endif
        @endif
        <?php
          $sumatoriaEquipo = $tramite->ponderador_calculado + $sumatoriaEquipo;
         ?>
        </center></td>
      </tr>
      @endforeach
    </tbody>
  </table>
  <table>
    <tr>
        <td colspan="2" style="color:#ffff; text-align: center; background-color: #013273;"><b>Total Mes</b></td>
        <td colspan="1"><center>{{$sumatoriaEquipo}} %</center></td>
    </tr>
  </table>
  <br>
  @endforeach
  @endforeach
