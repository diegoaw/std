
<!DOCTYPE html>
<html>
<head>
  <title>Indicadores</title>

</head>
<style>
table {
   width: 100%;
   border: 1px solid #000;
}
th, td {

   text-align: left;
   vertical-align: middle;
   border: 1px solid #000;
   border-spacing: 0;
}
</style>
<body>
<div>
      <h2 align=center style="color:DarkBlue;">MINISTERIO DEL DEPORTE</h2>
      <h2 align=center style="color:DarkBlue;">PLANIFICACIÓN - SIGC</h2>
</div>
@foreach ($indicadores as $indice => $tramite)
<h3><center><b>  DATOS DEL INDICADOR {{$tramite->id}}</b></center></h3>

<table class="table table-striped">
    <tr>
        <td style="color:white; width:260px; text-align: center; background-color: #013273;"><b>ID</b></td>
        <td>{{$tramite->id}}</td>
    </tr>
    <tr>
        <td style="color:white; width:260px; text-align: center; background-color: #013273;"><b>Año</b></td>
        <td>{{$tramite->ano}}</td>
    </tr>
    <tr>
        <td style="color:white; width:300px; text-align: center; background-color: #013273;"><b>Nombre</b></td>
        <td>{{$tramite->nombre}}</td>
    </tr>
    <tr>
        <td style="color:white; width:260px; text-align: center; background-color: #013273;"><b>Tipo</b></td>
        <td>{{$tramite->tipo->alias}}</td>
    </tr>
    <tr>
        <td style="color:white; width:260px; text-align: center; background-color: #013273;"><b>Meta</b></td>
        <td>{{$tramite->meta}}</td>
    </tr>
    <tr>
        <td style="color:white; width:260px; text-align: center; background-color: #013273;"><b>Ponderación</b></td>
        <td>{{$tramite->ponderador}}</td>
    </tr>
    <tr>
        <td style="color:white; width:260px; text-align: center; background-color: #013273;"><b>Equipo / Objetivo de Gestión</b></td>
        <td>@if($tramite->equipo) {{$tramite->equipo->nombre}} @endif</td>
    </tr>
    <tr>
        <td style="color:white; width:260px; text-align: center; background-color: #013273;"><b>Medio Verificación</b></td>
        <td style="text-align: justify;">{{$tramite->medio_verif}}</td>
    </tr>
    <tr>
        <td style="color:white; width:260px; text-align: center; background-color: #013273;"><b>Nota</b></td>
        <td style="text-align: justify;">{{$tramite->notas}}</td>
    </tr>
  </table>
<br>
<h3><center><b>MEDICIONES POR MES</b></center></h3>

<table class="table table-striped">
  <tr>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;"></th>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;">Medición Enero </th>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;"></th>
  </tr>
  <tr>
    <td valign="middle" style="text-align: center; height:20px;" >@if($arregloGraficaCumplimiento[$tramite->id][1]['estatus'] != '') {{$arregloGraficaCumplimiento[$tramite->id][1]['estatus']}} @endif</td>
    <td valign="middle" style="text-align: center; height:20px;" >Resultado: {{$arregloGraficaCumplimiento[$tramite->id][1]['resultado']}}</td>
    <td valign="middle" style="text-align: center; height:20px;" >Ponderación: </td>
  </tr>
</table>
<br><br>
<table class="table table-striped">
  <tr>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;"></th>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;">Medición Febrero </th>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;"></th>
  </tr>
  <tr>
    <td valign="middle" style="text-align: center; height:20px;" >@if($arregloGraficaCumplimiento[$tramite->id][2]['estatus'] != '') {{$arregloGraficaCumplimiento[$tramite->id][2]['estatus']}} @endif</td>
    <td valign="middle" style="text-align: center; height:20px;" >Resultado: {{$arregloGraficaCumplimiento[$tramite->id][2]['resultado']}}</td>
    <td valign="middle" style="text-align: center; height:20px;" >Ponderación: </td>
  </tr>
</table>
<br><br>
<table class="table table-striped">
  <tr>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;"></th>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;">Medición Marzo </th>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;"></th>
  </tr>
  <tr>
    <td valign="middle" style="text-align: center; height:20px;" >@if($arregloGraficaCumplimiento[$tramite->id][3]['estatus'] != '') {{$arregloGraficaCumplimiento[$tramite->id][3]['estatus']}} @endif</td>
    <td valign="middle" style="text-align: center; height:20px;" >Resultado: {{$arregloGraficaCumplimiento[$tramite->id][3]['resultado']}}</td>
    <td valign="middle" style="text-align: center; height:20px;" >Ponderación: </td>
  </tr>
</table>
<br><br>
<table class="table table-striped">
  <tr>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;"></th>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;">Medición Abril </th>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;"></th>
  </tr>
  <tr>
    <td valign="middle" style="text-align: center; height:20px;" >@if($arregloGraficaCumplimiento[$tramite->id][4]['estatus'] != '') {{$arregloGraficaCumplimiento[$tramite->id][4]['estatus']}} @endif</td>
    <td valign="middle" style="text-align: center; height:20px;" >Resultado: {{$arregloGraficaCumplimiento[$tramite->id][4]['resultado']}}</td>
    <td valign="middle" style="text-align: center; height:20px;" >Ponderación: </td>
  </tr>
</table>
<br><br>
<table class="table table-striped">
  <tr>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;"></th>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;">Medición Mayo </th>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;"></th>
  </tr>
  <tr>
    <td valign="middle" style="text-align: center; height:20px;" >@if($arregloGraficaCumplimiento[$tramite->id][5]['estatus'] != '') {{$arregloGraficaCumplimiento[$tramite->id][5]['estatus']}} @endif</td>
    <td valign="middle" style="text-align: center; height:20px;" >Resultado: {{$arregloGraficaCumplimiento[$tramite->id][5]['resultado']}}</td>
    <td valign="middle" style="text-align: center; height:20px;" >Ponderación: </td>
  </tr>
</table>
<br><br>
<table class="table table-striped">
  <tr>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;"></th>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;">Medición Junio </th>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;"></th>
  </tr>
  <tr>
    <td valign="middle" style="text-align: center; height:20px;" >@if($arregloGraficaCumplimiento[$tramite->id][6]['estatus'] != '') {{$arregloGraficaCumplimiento[$tramite->id][6]['estatus']}} @endif</td>
    <td valign="middle" style="text-align: center; height:20px;" >Resultado: {{$arregloGraficaCumplimiento[$tramite->id][6]['resultado']}}</td>
    <td valign="middle" style="text-align: center; height:20px;" >Ponderación: </td>
  </tr>
</table>
<br><br>
<table class="table table-striped">
  <tr>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;"></th>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;">Medición Julio </th>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;"></th>
  </tr>
  <tr>
    <td valign="middle" style="text-align: center; height:20px;" >@if($arregloGraficaCumplimiento[$tramite->id][7]['estatus'] != '') {{$arregloGraficaCumplimiento[$tramite->id][7]['estatus']}} @endif</td>
    <td valign="middle" style="text-align: center; height:20px;" >Resultado: {{$arregloGraficaCumplimiento[$tramite->id][7]['resultado']}}</td>
    <td valign="middle" style="text-align: center; height:20px;" >Ponderación: </td>
  </tr>
</table>
<br><br>
<table class="table table-striped">
  <tr>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;"></th>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;">Medición Agosto </th>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;"></th>
  </tr>
  <tr>
    <td valign="middle" style="text-align: center; height:20px;" >@if($arregloGraficaCumplimiento[$tramite->id][8]['estatus'] != '') {{$arregloGraficaCumplimiento[$tramite->id][8]['estatus']}} @endif</td>
    <td valign="middle" style="text-align: center; height:20px;" >Resultado: {{$arregloGraficaCumplimiento[$tramite->id][8]['resultado']}}</td>
    <td valign="middle" style="text-align: center; height:20px;" >Ponderación: </td>
  </tr>
</table>
<br><br>
<table class="table table-striped">
  <tr>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;"></th>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;">Medición Septiembre </th>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;"></th>
  </tr>
  <tr>
    <td valign="middle" style="text-align: center; height:20px;" >@if($arregloGraficaCumplimiento[$tramite->id][9]['estatus'] != '') {{$arregloGraficaCumplimiento[$tramite->id][9]['estatus']}} @endif</td>
    <td valign="middle" style="text-align: center; height:20px;" >Resultado: {{$arregloGraficaCumplimiento[$tramite->id][9]['resultado']}}</td>
    <td valign="middle" style="text-align: center; height:20px;" >Ponderación: </td>
  </tr>
</table>
<br><br>
<table class="table table-striped">
  <tr>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;"></th>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;">Medición Octubre </th>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;"></th>
  </tr>
  <tr>
    <td valign="middle" style="text-align: center; height:20px;" >@if($arregloGraficaCumplimiento[$tramite->id][10]['estatus'] != '') {{$arregloGraficaCumplimiento[$tramite->id][10]['estatus']}} @endif</td>
    <td valign="middle" style="text-align: center; height:20px;" >Resultado: {{$arregloGraficaCumplimiento[$tramite->id][10]['resultado']}}</td>
    <td valign="middle" style="text-align: center; height:20px;" >Ponderación: </td>
  </tr>
</table>
<br><br>
<table class="table table-striped">
  <tr>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;"></th>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;">Medición Noviembre </th>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;"></th>
  </tr>
  <tr>
    <td valign="middle" style="text-align: center; height:20px;" >@if($arregloGraficaCumplimiento[$tramite->id][11]['estatus'] != '') {{$arregloGraficaCumplimiento[$tramite->id][11]['estatus']}} @endif</td>
    <td valign="middle" style="text-align: center; height:20px;" >Resultado: {{$arregloGraficaCumplimiento[$tramite->id][11]['resultado']}}</td>
    <td valign="middle" style="text-align: center; height:20px;" >Ponderación: </td>
  </tr>
</table>
<br><br>
<table class="table table-striped">
  <tr>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;"></th>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;">Medición Dciembre </th>
    <th style="color:white; width:260px; text-align: center; background-color: #013273;"></th>
  </tr>
  <tr>
    <td valign="middle" style="text-align: center; height:20px;" ></td>
    <td valign="middle" style="text-align: center; height:20px;" >Resultado: </td>
    <td valign="middle" style="text-align: center; height:20px;" >Ponderación: </td>
  </tr>
</table>
  <div style="page-break-after: always"></div>
  @endforeach
</body>
