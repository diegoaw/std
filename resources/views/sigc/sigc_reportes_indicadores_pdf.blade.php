<!DOCTYPE html>
<html>
<head>
  <title>Indicadores</title>
</head>
<style>
table {
   width: 100%;
   border: 1px solid #fff;
   border-spacing: 0 !important;
}
th, td {

   text-align: left;
   vertical-align: middle;
   border: 1px solid #fff;
   border-spacing: 0;
   height: 60px;
}
</style>
<body>
  <div>
        <h3 align=center style="color:DarkBlue;">MINISTERIO DEL DEPORTE</h3>
        <h4 align=center style="color:DarkBlue;">DIVISIÓN DE PLANIFICACIÓN</h4>
        <h4 align=center style="color:Black; text-transform: uppercase;">SIGC - {{$nombreMes}}</h4>
  </div>
  @foreach ($indicadores as $tipo => $equipos)
  <h4><center><b>{{$tipo}}</b></center></h4>
    @foreach ($equipos as $nombreEquipo => $arrayindicadores)
    <?php
      $sumatoriaEquipo = 0;
     ?>
    <table>
      <thead >
        <tr>
          <th style="color:white; width:500px; font-size:14px; text-align: center; background-color: #013273;"><b>{{$nombreEquipo}}</b></th>
          <th style="color:white; width:200px; font-size:14px; text-align: center; background-color: #013273;"><b>Estado Mes Actual</b></th>
          <th style="color:white; width:200px; font-size:14px; text-align: center; background-color: #013273;"><b>Meta 2022</b></th>
          <th style="color:white; width:200px; font-size:14px; text-align: center; background-color: #013273;"><b>Avance Cumplimiento de la Meta</b></th>
          <th style="color:white; width:200px; font-size:14px; text-align: center; background-color: #013273;"><b>Ponderación 2022</b></th>
          <th style="color:white; width:200px; font-size:14px; text-align: center; background-color: #013273;"><b>Avance Ponderado de la Meta</b></th>
        </tr>
      </thead>
      <tbody>
        @foreach ($arrayindicadores as $indice => $tramite)
        <tr>
          <td style="width:500px; font-size:13px; background-color: #96baeb;"><center>{{$tramite->nombre}}</center></td>
          <td style="width:200px; font-size:13px; background-color: #dae0e8;"><center>{{$tramite->estado}}</center></td>
          <td style="width:200px; font-size:13px; background-color: #dae0e8;"><center>
            @if($tramite->unidad_id == 2)
              @if($tramite->meta_calculada == 0)
              Mide (indicador sin meta asociada)
              @else
              {{$tramite->meta_calculada}} días
              @endif
            @else
              @if($tramite->meta_calculada == 0)
              Mide (indicador sin meta asociada)
              @else
              {{$tramite->meta_calculada}} %
              @endif
            @endif
          </center></td>
          <td style="width:200px; font-size:13px; background-color: #dae0e8;"><center>
            @if($tramite->unidad_id_meta == 5)
              {{$arregloGraficaCumplimiento[$tramite->id][$mesO]}} %

            @else
              {{$arregloGraficaCumplimiento[$tramite->id][$mesO]}}

            @endif
            @if($tramite->meta == 0)
            <br>Sin Meta Asociada
            @else
            <br>{{$tramite->formula_cumplimiento_string}}
            @endif
            
          </center></td>
          <td style="width:200px; font-size:13px; background-color: #dae0e8;"><center>
            @if($tramite->equipo_id == 64)
              No Aplica
            @else
              {{$tramite->ponderador}} %
            @endif
          </center></td>
          <td style="width:200px; font-size:13px; background-color: #dae0e8;"><center>
          @if($tramite->equipo_id == 64)
            No Aplica
          @else
            @if($tramite->reportada == true)
             {{$tramite->ponderador_calculado_otro}} %
            @endif
          @endif
          <?php
            $sumatoriaEquipo = $tramite->ponderador_calculado + $sumatoriaEquipo;
           ?>
          </center></td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <table>
      <tr>
          <td nowrap style="color:white; width:200px; font-size:13px; text-align: center;"> </td>
          <td nowrap style="color:white; width:200px; font-size:13px; text-align: center;"> </td>
          <td nowrap style="color:white; width:200px; font-size:13px; text-align: center;"> </td>
          <td nowrap style="color:white; width:200px; font-size:13px; text-align: center;"> </td>
          <td nowrap style="color:white; width:170px; font-size:13px; text-align: center; background-color: #013273;"><b>Total Mes</b></td>
          <td nowrap style="width:170px; font-size:13px; text-align: center;">{{$sumatoriaEquipo}} %</td>
      </tr>
    </table>
    <br>

    @endforeach
    <div style="page-break-after: always"></div>
    @endforeach
</body>
