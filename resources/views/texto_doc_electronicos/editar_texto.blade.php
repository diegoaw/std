@extends('layouts.out2')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-file-text"></i> Convertidor de Textos para Documentos Electrónicos en Simple</h1><br> 
    </div>  
  </div>
</div>
@endsection
@section('after-styles-end')
<style type="text/css">
.oculto{
  display: none;
}
</style>
@endsection
@section('content')
{!! Form::open(['route' => 'editado', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','id'=>'form-create']) !!}

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-danger card-outline">
        <h3 class="card-title">Convertir Texto</h3>
        <div class="card-tools">
                  </div>
      </div>
      <div class="card-body">
      
        
        <div class="form-group row">
          {!! Form::label('', '', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('texto', $textoeditado, ['class' => 'form-control ckeditor', 'placeholder' => 'Texto' , 'id'=>'texto' , 'name'=>'texto']) !!}
          </div>
        </div>

     
    

        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! Form::submit('Convertir', ['class' => 'btn btn-success','title'=>'Convertir']) !!}
              <button class="btn btn-primary" onclick="copiarAlPortapapeles('textoeditado')">Copiar</button>
            </div>
          </div>
        </div>

        


@if($sineditar==0)

        <div class="form-group row oculto">
          {!! Form::label('textoeditado', 'textoeditado', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('textoeditado', $textoeditado, ['placeholder' => 'textoeditado' , 'id'=>'textoeditado' , 'name'=>'textoeditado']) !!}
          </div>
        </div>

@endif

      </div>
    </div>
  </div>
</div>
{!! Form::close() !!}



@endsection

@section('after-scripts-end')
<script type="text/javascript">


function copiarAlPortapapeles(id_elemento) {
  var aux = document.createElement("input");
  aux.setAttribute("value", document.getElementById(id_elemento).value);
  document.body.appendChild(aux);
  aux.select();
  document.execCommand("copy");
  document.body.removeChild(aux);
  alert("Texto Copiado");
}
    
 
  </script>
  
@stop