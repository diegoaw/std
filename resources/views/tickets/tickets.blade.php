@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-ticket"></i> Consulta de Estado de Tickets MINDEP</h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        
        <h5 class="card-title">Filtros </h5>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">
            {!! Form::open(['route' => 'tickets', 'method' => 'get' , 'class'=>"form-horizontal"]) !!}

              <div class="form-row">

                <div class="col-sm-0">
                  <label for="nRegistro" class=" col-form-label">N° Trámite: </label>
                  <input type="text" class="form-control" id="nRegistro" name="nRegistro" value="{{ $idTramiteFiltro }}" placeholder="N° Trámite">
                </div>

                <div class="col-sm-12">
                  <div class="text-center">
                    <a class="btn btn-danger center" href="https://simple.mindep.cl/" target="_blank">Nuevo Ticket de Soporte MINDEP</a>
                  </div>
                </div>
                
              </div>
              <!--<button type="submit" class="btn btn-primary pull-right">Buscar</button>-->
              <input type="submit" class="btn btn-primary pull-right" name="Buscar" value="Buscar">

            {!! Form::close() !!}


          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header card-danger card-outline">
          <h5 class="card-title">Trámites de Tickets MINDEP</h5>

        </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
            <table class="table table-striped table-sm"  >
              <thead >
                  <tr style="background-color: #cd0b27;">
                      <th nowrap style="color:white">Trámite ID</th>
                      <th nowrap style="color:white"><center>Estado Trámite</center></th>
                      <th nowrap style="color:white"><center>Fecha Inicio</center></th>
                      <th nowrap style="color:white"><center>Etapa</center></th>
                      <th nowrap style="color:white"><center>Asignado A</center></th>
                      <th nowrap style="color:white"><center>Tipo</center></th>
                      <th nowrap style="color:white"><center>Estado Ticket</center></th>
                      <th nowrap style="color:white"><center>Fecha Actualización</center></th>
                      <th nowrap style="color:white"><center>Calificación</center></th>
                      <th nowrap style="color:white"><center>Detalles</center></th>
                  </tr>
              </thead>
              <tbody class="panel">
              @if(count($tramitesD)>0)
                @foreach ($tramitesD as $indice => $tramites)
                    <tr>
                         <td nowrap>{{$tramites->numero_tramite}}</td>
                         @if($tramites->pendiente == 1)
                         <td nowrap style="background-color: IndianRed; color: white;"><b><center>PENDIENTE</center></b></td>
                         @else
                         <td nowrap style="background-color: YellowGreen; color: white;"><b><center>COMPLETADO</center></b></td>
                         @endif
                         <td nowrap nowrap><center>{{$tramites->fecha_inicio}}</center></td>
                         <td><center>{{$tramites->etapa}}</center></td>
                         <!-- INICIO A CARGO -->
                        @if($tramites->ACargo)
                          @if($tramites->ACargo == 'Camila Fernanda Orellana Araya')
                          <td nowrap style="text-transform:uppercase; background-color:DarkOrange; color: white;"><b>{{$tramites->ACargo}}</b></td>
                          @elseif($tramites->ACargo == 'Daniel Alejandro Villafana Brown')
                          <td nowrap style="text-transform:uppercase; background-color:AppleGreen; color: white;"><b>{{$tramites->ACargo}}</b></td>
                          @elseif($tramites->ACargo == 'Diego Pablo Arriagada Walton')
                          <td nowrap style="text-transform:uppercase; background-color:BlueViolet; color: white;"><b>{{$tramites->ACargo}}</b></td>
                          @elseif($tramites->ACargo == 'Daniela Karolina Jimenez Coronado')
                          <td nowrap style="text-transform:uppercase; background-color:MediumVioletRed; color: white;"><b>{{$tramites->ACargo}}</b></td>
                          @else
                          <td nowrap style="text-transform:uppercase;"><b>{{$tramites->ACargo}}</b></td>
                          @endif
                        @else
                        <td nowrap style="text-transform:uppercase;"><b style="color:DarkRed">SIN ASIGNAR</b></td>
                        @endif
                          <!-- FIN A CARGO -->
                          <td ><center>
                          @if($tramites->Tipod){{$tramites->Tipod}}@endif
                          @if($tramites->Tipos){{$tramites->Tipos}}@endif
                         </center></td>
                         @if($tramites->Estado == 'En Proceso')
                         <td nowrap style="background-color: GoldenRod; color: white;"><b><center>EN PROCESO</center></b></td>
                         @elseif($tramites->Estado == 'Completado Responda Encuesta de Satisfaccion')
                         <td nowrap style="background-color: YellowGreen; color: white;"><b><center>COMPLETADO</center></b></td>
                         @else
                         <td nowrap><b style="color:DarkRed"><center>ABIERTO</center></b></td>
                         @endif
                         <td nowrap><center>{{$tramites->fecha_ultima_modificacion}}</center></td>
                         <td nowrap><center>
                         @if($tramites->Estrellas)
                            @if($tramites->Estrellas == 5)<b style="color: Gold;">★★★★★</b>@endif
                            @if($tramites->Estrellas == 4)<b style="color: GoldenRod;">★★★★</b>@endif
                            @if($tramites->Estrellas == 3)<b style="color: GoldenRod;">★★★</b>@endif
                            @if($tramites->Estrellas == 2)<b style="color: GoldenRod;">★★</b>@endif
                            @if($tramites->Estrellas == 1)<b style="color: DarkRed;">★</b>@endif
                         @else
                          <b style="color:DarkRed">SIN CLASIFICACIÓN</b>
                         @endif
                         </center></td>
                         <td><center>
                          <a class="btn accion btn-primary accion" href="{{route('ticketsDetalle',$tramites->numero_tramite)}}"><i class="fa fa-eye"></i></a>
                         </center></td>
                    </tr>
                @endforeach
              @endif
              </tbody>
            </table>
          </div>

    </div>
  </div>
</div>

@endsection
@section('before-scripts-end')
<script>
  setInterval("location.reload()",60000);
</script>
@stop
