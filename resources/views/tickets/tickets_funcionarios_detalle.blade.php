@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-5">
      <h1><i class="fa fa-ticket"></i> Sistema de Tickets Soporte MINDEP</h1>
    </div>
    <div class="col-sm-7">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-ticket"></i> Sistema de Tickets Soporte MINDEP</li>
        <li class="breadcrumb-item active">Detalles</li>
      </ol>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <!-- Line chart -->
    <div class="card card-danger card-outline">
      <div class="card-header">
        <h3 class="card-title">Detalles Sistema de Tickets Soporte MINDEP</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <br>
        <h4><b>Ticket N° {{$data->id}}</b></h4>
        <br>
        <br>
        <h4><b>Datos del Ticket</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Fecha Ticket:</b></td>
              <td nowrap>{{$data->fecha_actual}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Datos Funcionario:</b></td>
              <td nowrap>{{$data->cargo}} {{$data->division}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>E-Mail Funcionario:</b></td>
              <td nowrap>{{$data->email_a}}</td>
          </tr>
		      <tr>
              <td style="width:450px"><b>Área Soporte:</b></td>
              <td nowrap>{{$data->area}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Tipo Solicitud:</b></td>
              <td>{{$data->tipo_solicitud_d}} {{$data->tipo_solicitud_s}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Observaciones Solicitud:</b></td>
              <td>{{$data->observaciones}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Descargar Adjunto:</b></td>
              <td >
              @if($data->adjunto)
              <a href="{{$data->adjunto}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
              </td>
          </tr>
		  </table>
      <br>
      <h4><b>Datos de Resolución del Ticket</b></h4>
      <br>
      <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Nombre del Encargado del Área:</b></td>
              <td nowrap>{{$data->a_cargo}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Estado del Ticket:</b></td>
              <td>{{$data->estado}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Observación de Resolución:</b></td>
              <td>{{$data->observacion_soporte}}</td>
          </tr>
      </table>
      <br>
      <h4><b>Encuesta de Satisfacción</b></h4>
      <br>
      <table id="example1" class="table table-striped">
        <tr>
            <td style="width:450px"><b>Grado de Satisfacción</b></td>
            <td nowrap>{{$data->satisfaccion}}</td>
        </tr>
        <tr>
            <td style="width:450px"><b>Estrellas de Calificación:</b></td>
            <td nowrap>{{$data->estrellas}}</td>
        </tr>
        <tr>
            <td style="width:450px"><b>Comentarios:</b></td>
            <td nowrap>{{$data->comentarios}}</td>
        </tr>
        <tr>
            <td style="width:450px"><b>Descargar Comprobante de Ticket:</b></td>
            <td >
            @if($data->comprobante_descarga)
            <a href="{{$data->comprobante_descarga}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
            @endif
            </td>
        </tr>
      </table>
      </div>

        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! link_to_route('tickets2', 'Volver', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
            </div>
          </div>
        </div>
    </div>
  </div>
</div>


@endsection
@section('before-scripts-end')
@stop
