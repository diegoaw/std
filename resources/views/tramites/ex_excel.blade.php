<table>
  <tr>
   <th colspan="10" style="text-align: center;">MINISTERIO DEL DEPORTE</th>
  </tr>
  <tr>
   <th colspan="10" style="text-align: center;">Sistema de Trazabilidad Documental</th>
  </tr>
  <tr>
   <th colspan="10" style="text-align: center;">Trámites Ingresados</th>
  </tr>
   <tr>
   <th colspan="10" style="text-align: center;">{{Date::now()->format('d-m-Y')  }} </th>
  </tr>
</table >

    <table>
      <tr style="background-color: #cd0b27;">
        <th nowrap style="color:#ffff">N° Trámite</th>
        <th nowrap style="color:#ffff">Fecha de Ingreso</th>
        <th nowrap style="color:#ffff">N° Trámite Relacionado</th>
        <th nowrap style="color:#ffff">Ingresado por</th>
        <th nowrap style="color:#ffff">Origen</th>
        <th nowrap style="color:#ffff">¿Ingreso por DocDigital?</th>
        <th nowrap style="color:#ffff">Remitente</th>
        <th nowrap style="color:#ffff">Cargo/Función</th>
        <th nowrap style="color:#ffff">Área y Subarea Remitente</th>
        <th nowrap style="color:#ffff">Tipo de Documento</th>
        <th nowrap style="color:#ffff">N° de Docto</th>
        <th nowrap style="color:#ffff">Contenido</th>
        <th nowrap style="color:#ffff">Documento Cargado</th>
      </tr>
      @foreach ($tramites as $tramite)
      <tr>
        <td nowrap>{{$tramite->tramite_id}}</td>
        <td nowrap>{{$tramite->created_at}}</td>
        <td nowrap>{{$tramite->tramite_relacion_id}}</td>
        <td nowrap>{{$tramite->ingresado_por}}</td>
        <td nowrap>{{$tramite->origen}}</td>
        <td nowrap>{{$tramite->docdigital}}</td>
        <td nowrap>{{$tramite->remitente}}</td>
        <td nowrap>{{$tramite->cargo}}</td>
        <td>{{$tramite->area_remitente}}</td>
        <td nowrap>{{$tramite->tipo_documento}}</td>
        <td nowrap>{{$tramite->docto_n}}</td>
        <td>{{$tramite->contenido}}</td> 
        <td>{{$tramite->url}}</td>    
      </tr>
      @endforeach
    </table>  

               