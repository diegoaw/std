<table>
  <tr>
   <th colspan="10" style="text-align: center;">MINISTERIO DEL DEPORTE</th>
  </tr>
  <tr>
   <th colspan="10" style="text-align: center;">Sistema de Trazabilidad Documental</th>
  </tr>
  <tr>
   <th colspan="10" style="text-align: center;">Trámites Ingresados</th>
  </tr>
   <tr>
   <th colspan="10" style="text-align: center;">{{Date::now()->format('d-m-Y')  }} </th>
  </tr>
</table >

    <table>
      <tr style="background-color: #cd0b27;">
        <th  style="color:#ffff">N° Trámite</th>
        <th nowrap style="color:#ffff">Fecha de Ingreso</th>
        <th  style="color:#ffff">N° Trámite Relacionado</th>
        <th nowrap style="color:#ffff">Ingresado por</th>
        <th  style="color:#ffff">Origen</th>
        <th  style="color:#ffff">¿Ingreso por DocDigital?</th>
        <th  style="color:#ffff">A Nombre de</th>
        <th  style="color:#ffff">Cargo/Función</th>
        <th  style="color:#ffff">Área y Subarea Remitente</th>
        <th  style="color:#ffff">Tipo de Documento</th>
        <th  style="color:#ffff">N° de Docto</th>
        <th  style="color:#ffff">Contenido</th>
      </tr>
      @foreach ($tramites as $tramite)
      <tr>
       <td><center>{{$tramite->tramite_id}}</center></td>
       <td><center>{{$tramite->created_at}}</center></td>
       <td><center>
        @if($tramite->n_registro)
          {{$tramite->n_registro}}
        @endif
        @if($tramite->n_memo_e)
          {{$tramite->n_memo_e}}
        @endif
        @if($tramite->n_oficio_e)
          {{$tramite->n_oficio_e}}
        @endif
        @if($tramite->n_derivacion_d)
          {{$tramite->n_derivacion_d}}
        @endif
       </center></td>
       <td>{{$tramite->nombres_solicitante}}</td>
       <td><center>{{$tramite->origen}}</center></td>
       <td><center>{{$tramite->docdigital}}</center></td>
       <td><center>
         @if($tramite->remitente)
         {{$tramite->remitente}}
         @else
         {{$tramite->remitente_ext}}
         @endif
       </center></td>
       <td><center>
         @if($tramite->cargo)
         {{$tramite->cargo}}
         @else
         {{$tramite->cargo_ext}}
         @endif
       </center></td>
       <td><center>
         @if($tramite->subarea_origen)
         {{$tramite->subarea_origen}}
         @else
         {{$tramite->subarea_origen_ext}}
         @endif
       </center></td>
       <td><center>
         @if($tramite->documento)
         {{$tramite->documento}}
         @else
         {{$tramite->documento2}}
         @endif
       </center></td>
       <td ><center>{{$tramite->n_docto}}</center></td>
       <td><center>{{$tramite->contenido_origen}}</center></td>
      </tr>
      @endforeach
    </table>
