
@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-file-text"></i> Consulta Ingreso y Derivación de Documentos</h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title">Filtrar Trámites por </h5>

      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">

                {!! Form::open(['route' => 'tramitessFiltros', 'method' => 'post' , 'class'=>"form-horizontal"]) !!}


                  <div class="form-row">

                    <div class="col-sm-0">
                      <label for="nRegistro" class=" col-form-label">N° Trámite: </label>
                      <input type="text" class="form-control" id="nRegistro" name="nRegistro" value="{{ $requestNRegistro }}" placeholder="N° Trámite">
                    </div>


                    <div class="form-group   {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                    <div class="input-group date">
                      <div class="col">
                          <label for="FechaDesde" class=" col-form-label">Fecha Desde: </label>
                          <input type="text" class="form-control group-date" id="FechaDesde" name="FechaDesde" readonly="readonly" value="{!! date('Y-m-d', strtotime($feDesde))  !!}">
                      </div>
                    </div>
                    </div>

                    <div class="form-group   {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                    <div class="input-group date">
                      <div class="col">
                          <label for="FechaHasta" class=" col-form-label">Fecha Hasta: </label>
                          <input type="text" class="form-control group-date" id="FechaHasta" name="FechaHasta" readonly="readonly" value="{!! date('Y-m-d', strtotime($feHasta ))  !!}">
                      </div>
                    </div>
                    </div>

                    <div class="form-check">
                      <label for="contenido" class=" col-form-label">Origen: </label>
                    </div>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="origen" value='' @if($origenView == null) checked @endif>
                      <label class="form-check-label">Todos</label>
                    </div>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="origen" value="Interno" @if($origenView == 'Interno') checked @endif>
                      <label class="form-check-label">Interno</label>
                    </div>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="origen" value="Externo" @if($origenView == 'Externo') checked @endif>
                      <label class="form-check-label">Externo</label>
                    </div>

                    <div class="form-check">
                      <label for="contenido2" class=" col-form-label">DocDigital: </label>
                    </div>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="docdigital" value='' @if($docdigitalView == null) checked @endif>
                      <label class="form-check-label">Todos</label>
                    </div>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="docdigital" value="si" @if($docdigitalView == 'si') checked @endif>
                      <label class="form-check-label">Si</label>
                    </div>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="docdigital" value="no" @if($docdigitalView == 'no') checked @endif>
                      <label class="form-check-label">No</label>
                    </div>

                    <div class="col-sm-2">
                      <label for="tiposDocumentos" class=" col-form-label">Tipo Documento: </label>
                      {!! Form::select('tiposDocumentos',$tiposDocumentos,$tiposDocumentosView,['id'=>'tiposDocumentos','name'=>'tiposDocumentos', 'class'=>'form-control','onchange'=>"getValueSelect3()"]) !!}
                    </div>

                    <div class="col-sm-2">
                      <label for="contenido" class=" col-form-label">Contenido o Materia:</label>
                      <textarea class="form-control rounded-0" id="contenido" name="contenido"  rows="3" placeholder="Contenido o Materia">{{ $contenidoView }}</textarea>
                    </div>

                    <div class="col-sm-2">
                      <label for="docto_n" class=" col-form-label">N° Docto/Folio:</label>
                      <input class="form-control rounded-0" id="docto_n" name="docto_n"  rows="3" value="{{ $docto_nView }}" placeholder="N° Docto/Folio">
                    </div>

                    <div class="col-sm-3">
                      <label for="areaRemtiente" class=" col-form-label">Área Remitente: </label>
                      {!! Form::select('areaRemtiente',$selectA,$areRemitenteView,['id'=>'areaRemtiente','name'=>'areaRemtiente', 'class'=>'form-control' ,'onchange'=>"getValueSelect()"]) !!}
                    </div>

                    <div class="col-sm-3">
                      <label for="procedencia" class=" col-form-label">Procedencia Externa: </label>
                      <input type="text" class="form-control" id="procedencia" name="procedencia" value="{{ $procedenciaView }}" placeholder="Procedencia">
                    </div>


                  </div>
                  <button type="submit" class="btn btn-primary pull-right">Buscar</button>

                {!! Form::close() !!}

          </div>
        </div>
      </div>
    </div>
  </div>
</div>


@if(count($listado)>0)
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">Listado de Ingreso y Derivación de Documentos</h5>

          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th  style="color:white"><center>N° Trámite</center></th>
                        <th  style="color:white"><center>Fecha de Ingreso</center></th>
                        <!-- <th  style="color:white"><center>N° Trámite Relacionado</center></th> -->
                        <th  style="color:white"><center>Ingresado por</center></th>
                        <th  style="color:white"><center>Origen</center></th>
                        <th  style="color:white"><center>¿Ingreso por DocDigital?</center></th>
                        <th  style="color:white"><center>A Nombre de</center></th>
                        <th  style="color:white"><center>Cargo/Función</center></th>
                        <th  style="color:white"><center>Área y Subarea Remitente</center></th>
                        <th  style="color:white"><center>Tipo de Documento</center></th>
                        <th  style="color:white"><center>N° Docto/Folio</center></th>
                        <th  style="color:white"><center>Contenido o Materia</center></th>
                        <th  style="color:white"><center>Derivaciones</center></th>
                        <th  style="color:white"><center>Documento Cargado</center></th>
                    </tr>
                </thead>
                <tbody class="panel">

                @if(count($listado)>0)
                         @foreach ($listado as $indice => $tramite)
                          <tr>
                           <td><center>{{$tramite->tramite_id}}</center></td>
                           <td><center>{{$tramite->created_at}}</center></td>
                           <!-- <td><center>
                            @if($tramite->n_registro)
                              {{$tramite->n_registro}}
                            @endif
                            @if($tramite->n_memo_e)
                              {{$tramite->n_memo_e}}
                            @endif
                            @if($tramite->n_oficio_e)
                              {{$tramite->n_oficio_e}}
                            @endif
                            @if($tramite->n_derivacion_d)
                              {{$tramite->n_derivacion_d}}
                            @endif
                           </center></td> -->
                           <td>{{$tramite->nombres_solicitante}}</td>
                           <td><center>{{$tramite->origen}}</center></td>
                           <td><center>{{$tramite->docdigital}}</center></td>
                           <td><center>
                             @if($tramite->remitente)
                             {{$tramite->remitente}}
                             @else
                             {{$tramite->remitente_ext}}
                             @endif
                           </center></td>
                           <td><center>
                             @if($tramite->cargo)
                             {{$tramite->cargo}}
                             @else
                             {{$tramite->cargo_ext}}
                             @endif
                           </center></td>
                           <td><center>
                             @if($tramite->subarea_origen)
                             {{$tramite->subarea_origen}}
                             @else
                             {{$tramite->subarea_origen_ext}}
                             @endif
                           </center></td>
                           <td><center>
                             @if($tramite->documento)
                             {{$tramite->documento}}
                             @else
                             {{$tramite->documento2}}
                             @endif
                           </center></td>
                           <td ><center>{{$tramite->n_docto}}</center></td>
                           <td><center>{{$tramite->contenido_origen}}</center></td>
                           <td><center> <button type="button" class="btn btn-primary" data-toggle="collapse" {!! "data-target='#demo".$indice."'" !!}   {!!"data-parent='#myTable".$tramite->tramite_id."'" !!}
                           ><i class="fa fa-eye"></i></button></center></td>
                           <td>
                             @if($tramite->urlDoc)
                             <a href="{{$tramite->urlDoc}}" target="_blank" download> <button type="button" class="btn btn-success"><i class="fa fa-download"></i></button></a>
                             @endif
                           </td>
                          </tr>

                        <tr  {!! "id='demo".$indice."'"  !!} class="collapse">
                        <td colspan="10" class="hiddenRow">
                          <table class="table table-striped table-sm" {!!  "id='myTable".$tramite->tramite_id."'" !!}  >
                            <thead class="thead-primary">
                              <tr style="background-color: #fddadf;">
                              <th >Fecha Derivación</th>
                              <th >Derivación</th>
                              <th >Remitente</th>
                              <th >Departamento Remitente</th>
                              <th >Destinatario</th>
                              <th >Departamento Destinatario</th>
                              <th >Prioridad</th>
                              <th >Observación</th>
                              <th >N° Folio</th>
                              <th >Documento Cargado</th>
                              </tr>
                            </thead>
                          <tbody>
                             @foreach ($tramite->etapas_array_datos as $indice1 => $registro)
                            <tr>
                              <td>
                                @if($registro->fecha_derivacion)
                                {{$registro->fecha_derivacion}}
                                @else
                                {{$registro->fecha_cierre_a}}
                                @endif
                              </td>
                              <td>
                                @if($registro->fecha_cierre_a)
                                 Cerrado
                                @else
                                 {{$registro->num_derivacion}}
                                @endif
                              </td>
                              <td>
                                @if($registro->remitente2){{$registro->remitente2}}@endif
                                @if($registro->nombres_solicitante){{$registro->nombres_solicitante}} {{$registro->apellidos_solicitante}}@endif
                              </td>
                              <td>
                                @if($registro->subarea_remitente2){{$registro->subarea_remitente2}}@endif
                                @if($registro->subarea_remitente){{$registro->subarea_remitente}}@endif
                                @if($registro->subarea_origen){{$registro->subarea_origen}}@endif
                                @if($registro->subarea2){{$registro->subarea2}}@endif
                              </td>
                              <td>{{$registro->destinatario}}</td>
                              <td>
                                @if($registro->derivar22remitente){{$registro->derivar22remitente}}@endif
                                @if($registro->api_area){{$registro->api_area}}@endif
                                @if($registro->derivar2remitente){{$registro->derivar2remitente}}@endif
                                @if($registro->derivar2){{$registro->derivar2}}@endif
                              </td>
                              <td>{{$registro->prioridad}}</td>
                              <td>
                                @if($registro->observacion)
                                {{$registro->observacion}}
                                @else
                                {{$registro->observacion_final}}
                                @endif
                              </td>
                              <td>{{$registro->n_docto}}</td>
                              <td><center>
                                @if($registro->adjuntar_doc2)<a href="{{$registro->adjuntar_doc2}}" target="_blank" download><button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>@endif
                              </center></td>
                            </tr>
                              @endforeach
                          </tbody>
                        </table>
                      </td>
                    </tr>
                    @endforeach
              @endif
                </tbody>
            </table>
          <br>
        </div>
        <div class="card-footer">
            <div class="card-tools text-center">
              {!! Form::open(['route' => 'tramitesReportes2', 'method' => 'post' , 'class'=>"form-horizontal"]) !!}
              <input type="hidden" class="form-control" id="nRegistro" name="nRegistro" value="{{ $requestNRegistro }}" placeholder="N° Trámite">
              <input type="hidden" class="form-control group-date" id="FechaDesde" name="FechaDesde" readonly="readonly" value="{!! date('Y-m-d', strtotime($feDesde))  !!}">
              <input type="hidden" class="form-control group-date" id="FechaHasta" name="FechaHasta" readonly="readonly" value="{!! date('Y-m-d', strtotime($feHasta ))  !!}">
              <input type="hidden" class="form-control" id="origen1" name="origen1">
              <input type="hidden" class="form-control" id="docdigital1" name="docdigital1">
              <input type="hidden" class="form-control" id="areaRemtiente1" name="areaRemtiente1">
              <input type="hidden" class="form-control" id="tiposDocumentos1" name="tiposDocumentos1">
              <textarea style="display:none;" class="form-control rounded-0" id="contenido" name="contenido"  rows="3">{{ $contenidoView }} </textarea>
              <input type="hidden" class="form-control" id="procedencia" name="procedencia" value="{{ $procedenciaView }}" placeholder="Procedencia">
              <button type="submit" onclick="inload()" class="btn btn-primary btn-success" role="button">Exportar a excel</button>&nbsp;
              {!! Form::close() !!}
            </div>
          </div>

      </div>
    </div>
</div>
<br>
@else
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">Listado de Trámites Ingresados</h5>

          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <center>  No se encontraron resultados.</center>
          <br>
        </div>

      </div>
    </div>
</div>
@endif


@endsection
@section('before-scripts-end')
@include('includes.scripts.script_fecha.scripts_filtro_fecha')
<script type="text/javascript">
var radios = document.getElementsByName('origen');
  for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
          document.getElementById('origen1').value = radios[i].value;
          break;
        }
      }
var select1 = document.getElementById('areaRemtiente').value;
document.getElementById('areaRemtiente1').value = select1;

var select3 = document.getElementById('tiposDocumentos').value;
       document.getElementById('tiposDocumentos1').value = select3;

  function getValueRadio(){
      var radios = document.getElementsByName('origen');

      for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
          document.getElementById('origen1').value = radios[i].value;
          break;
        }
      }
}

 function getValueSelect(){
      var select1 = document.getElementById('areaRemtiente').value;
       document.getElementById('areaRemtiente1').value = select1;
}

function getValueSelec3(){
      var select3 = document.getElementById('tiposDocumentos').value;
       document.getElementById('tiposDocumentos1').value = select3;
}

function inload(){

  setTimeout(function(){ document.getElementById("load").style.display="none";
    swal("No Imprima este docuemento", " Su impresion no tiene validez , si desea guardelo para su respaldo")
    }, 10000);

}
</script>
@stop
