@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-file-text"></i> Transparencia - Ingreso Documentos Gestiones</h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title"> Ingreso de Documentos Gestiones</h5>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">


                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="text-center">
                      @if($habititarcrear)
                      {!! link_to_route('transparencia_crear_documentos_gestiones', 'Nuevo Documento', [], ['class' => 'btn btn-primary btn-sm','title'=>'Nuevo Documento']) !!}
                      @endif
                      {!! link_to_route('transparencia_crear_documentos_gestiones', 'Nuevo Documento', [], ['class' => 'btn btn-primary btn-sm','title'=>'Nuevo Documento']) !!}
                      </div>
                    </div>
                  </div>


          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@if(count($dataDefinitiva)>0)
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">Transparencia - Documentos Gestiones</h5>

          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th nowrap style="color:white">ID</th>
                        <th nowrap style="color:white">Tipo Gestión</th>
                        <th nowrap style="color:white">Nombre Documento</th>                        
                        <th nowrap style="color:white">Mes</th>
                        <th nowrap style="color:white">Año</th>
                        <th nowrap style="color:white">Editar</th>
                        <th nowrap style="color:white">Eliminar</th>
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($dataDefinitiva)>0)
                      @foreach ($dataDefinitiva as $indice => $tramite)
                          <tr>
                           <td nowrap>{{$tramite->id}}</td>
                           <td nowrap>{{$tramite->TipoGestiones->nombre}}</td>
                           <td nowrap>{{$tramite->nombre_documento_original}}</td>
                           <td nowrap>{{$tramite->mes}}</td>
                           <td nowrap>{{$tramite->ano}}</td>
                           <td nowrap><a class="btn accion btn-success accion" href="{{route('transparencia_editar_documentos_gestiones',$tramite->id)}}"><i class="fa fa-pencil"></i></a></td>
                           <td nowrap><a data-toggle="tooltip" class="btn btn-sm btn-danger" href="#" OnClick="Eliminar('{{$tramite->id}}')"><i class="fa fa-trash"></i></a></td>
                          </tr>
                      @endforeach
                @endif
                </tbody>
              </table>
          </div>
@endif
@endsection
@section('before-scripts-end')
@include('includes.scripts.transparencia_documentos')
@stop