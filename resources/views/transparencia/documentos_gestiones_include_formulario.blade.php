@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-file-text"></i> Transparencia - Documentos Gestiones formulario</h1><br> 
    </div>    
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
{!! Form::open(['route' => 'transparencia_nuevo_documentos_gestiones_guardar', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','id'=>'form-create']) !!}
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-danger card-outline">
        <h3 class="card-title">Crear Documentos Gestiones formulario</h3>
      </div>
      <div class="card-body">

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>

        <div class="callout col-sm-8 offset-sm-2" style="color: #737373;">
          <h4 style="font-size: 14px;"><i class="fa fa-info-circle" style="color: #ff0000;"></i> Nota:</h4>
          <span>Los campos marcados con un asterisco (*) son obligatorios.</span>
        </div><br>

        <div class="form-group row">
          {!! Form::label('Tipos Gestiones', 'Tipos Gestiones(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('tipos_gestiones_id',$selectTipoGestion, $gestionSeleccionada, ['id'=>'tipos_gestiones_id','name'=>'tipos_gestiones_id', 'class'=>'form-control' , 'disabled'=>'disabled']) !!}
            {!! Form::hidden('tipos_gestiones_id',  $gestionSeleccionada, ['class' => 'form-control']) !!}
          </div>
        </div>

        <?php /*   includes de vistas con campos de los diferentes tipos de gestiones*/ ?>
        @if($gestionSeleccionada == 1)
        @include('transparencia.formularios_tipos_gestiones.formulario_doc_actos_publicados_en_diario_oficial')
        @endif
        @if($gestionSeleccionada == 2)
        @include('transparencia.formularios_tipos_gestiones.formulario_doc_marco_normativo')
        @endif
        @if($gestionSeleccionada == 3)
        @include('transparencia.formularios_tipos_gestiones.formulario_doc_potestades_competencias_facultades')
        @endif
        @if($gestionSeleccionada == 4)
        @include('transparencia.formularios_tipos_gestiones.formulario_doc_facultades_funciones_atribuciones')
        @endif
        @if($gestionSeleccionada == 5)
        @include('transparencia.formularios_tipos_gestiones.formulario_doc_escala_remuneraciones')
        @endif
        @if($gestionSeleccionada == 6)
        @include('transparencia.formularios_tipos_gestiones.formulario_doc_personal_codigo_del_trabajo')
        @endif
        @if($gestionSeleccionada == 7)
        @include('transparencia.formularios_tipos_gestiones.formulario_doc_personal_contrata')
        @endif
        @if($gestionSeleccionada == 8)
        @include('transparencia.formularios_tipos_gestiones.formulario_doc_personal_planta')
        @endif
        @if($gestionSeleccionada == 9)
        @include('transparencia.formularios_tipos_gestiones.formulario_doc_personas_contratos_honorarios')
        @endif
        @if($gestionSeleccionada == 10)
        @include('transparencia.formularios_tipos_gestiones.formulario_doc_otras_compras_adquisiciones')
        @endif
        @if($gestionSeleccionada == 11)
        @include('transparencia.formularios_tipos_gestiones.formulario_doc_otras_transferencias')
        @endif
        @if($gestionSeleccionada == 12)
        @include('transparencia.formularios_tipos_gestiones.formulario_doc_actos_resoluciones_efectos_sobre_terceros')
        @endif
        @if($gestionSeleccionada == 13)
        @include('transparencia.formularios_tipos_gestiones.formulario_doc_tramites_ante_organismo')
        @endif
        @if($gestionSeleccionada == 14)
        @include('transparencia.formularios_tipos_gestiones.formulario_doc_nomina_beneficiarios')
        @endif
        @if($gestionSeleccionada == 15)
        @include('transparencia.formularios_tipos_gestiones.formulario_doc_subsidios_beneficios_intermediario')
        @endif
        @if($gestionSeleccionada == 16)
        @include('transparencia.formularios_tipos_gestiones.formulario_doc_subsidios_beneficios_propios')
        @endif
        @if($gestionSeleccionada == 17)
        @include('transparencia.formularios_tipos_gestiones.formulario_doc_consejo_consultivo')
        @endif
        @if($gestionSeleccionada == 18)
        @include('transparencia.formularios_tipos_gestiones.formulario_doc_mecanismos_participacion_ciudadana')
        @endif
        @if($gestionSeleccionada == 19)
        @include('transparencia.formularios_tipos_gestiones.formulario_doc_auditorias_ejercicio_presupuestario')
        @endif
        @if($gestionSeleccionada == 20)
        @include('transparencia.formularios_tipos_gestiones.formulario_doc_entidades_participacion_organismo')
        @endif
        @if($gestionSeleccionada == 21)
        @include('transparencia.formularios_tipos_gestiones.formulario_doc_antecedentes_afecten_empresas_menor_tamano')
        @endif
        
        <?php /*   fin includes */ ?>




        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! Form::submit('Guardar', ['class' => 'btn btn-success btn-sm','title'=>'Guardar']) !!}
              {!! link_to_route('transparencia_crear_documentos_gestiones', 'Volver', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{!! Form::close() !!}
@stop
@section('after-scripts-end')
@include('includes.scripts.script_fecha.scripts_filtro_fecha')
@endsection