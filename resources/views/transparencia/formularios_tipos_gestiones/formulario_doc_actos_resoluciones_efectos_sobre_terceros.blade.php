
        <div class="form-group row">
            {!! Form::label('Año', 'Año(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('ano',$selectAno, date('Y'), ['id'=>'ano','name'=>'ano', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Mes', 'Mes(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('mes',$selectMes, date('n'), ['id'=>'mes','name'=>'mes', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Tipología del Acto', 'Tipología del Acto(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('tipología_acto', null, ['class' => 'form-control', 'placeholder' => 'Tipología del acto']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Tipo de Acto', 'Tipo de Acto(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('tipo_acto', null, ['class' => 'form-control', 'placeholder' => 'Tipo de acto']) !!}
          </div>
        </div>

        <div class="form-group row">    
            {!! Form::label('Denominación Acto', 'Denominación Acto(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('denominacion_acto', null, ['class' => 'form-control', 'placeholder' => 'Denominación acto']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Número Acto', 'Número Acto(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('n_acto', null, ['class' => 'form-control', 'placeholder' => 'Número acto']) !!}
          </div>
        </div>

        <div class="form-group row" >   
            {!!Form::label('fechaActo', 'Fecha Acto(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            <div class="input-group date">        
            <input type="text" class="form-control group-date" id="fechaActo" name="fechaActo" value="{!! date('Y-m-d', strtotime($fe_desde))  !!}">
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
            </div>        
          </div>
        </div>

        <div class="form-group row" >   
            {!!Form::label('fechaPublicacion', 'Fecha Publicación en el DO(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            <div class="input-group date">        
            <input type="text" class="form-control group-date" id="fechaPublicacion" name="fechaPublicacion" value="{!! date('Y-m-d', strtotime($fe_desde))  !!}">
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
            </div>        
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Indicación del Medio y Forma de Publicidad ', 'Indicación del Medio y Forma de Publicidad(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('indicacion_forma', null, ['class' => 'form-control', 'placeholder' => 'Indicación del medio y forma de publicidad']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Tiene Efectos Generales', 'Tiene Efectos Generales(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('efectos_generales', null, ['class' => 'form-control', 'placeholder' => 'Tiene efectos generales']) !!}
          </div>
        </div>

        <div class="form-group row" >   
            {!!Form::label('fechaActualizacion', 'Fecha Última Actualización (si corresponde)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            <div class="input-group date">        
            <input type="text" class="form-control group-date" id="fechaActualizacion" name="fechaActualizacion" value="{!! date('Y-m-d', strtotime($fe_desde))  !!}">
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
            </div>        
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Breve Descripción del Objeto del Acto', 'Breve Descripción del Objeto del Acto(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'placeholder' => 'Escriba Breve descripción del objeto del acto']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Enlace a la Publicación o Archivo Correspondiente', 'Enlace a la Publicación o Archivo Correspondiente(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('enlace1', null, ['class' => 'form-control', 'placeholder' => 'Enlace a la publicación o archivo correspondiente']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Enlace Última Modificación o Archivo Correspondiente', 'Enlace Última Modificación o Archivo Correspondiente(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('enlace2', null, ['class' => 'form-control', 'placeholder' => 'Enlace última modificación o Archivo Correspondiente']) !!}
          </div>
        </div>


        <div class="form-group row">
          {!! Form::label('Nota Generador', 'Nota Generador', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('nota_generador', null, ['class' => 'form-control', 'placeholder' =>'Escriba una nota de descripción...']) !!}
          </div>
        </div> 
