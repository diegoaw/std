
        <div class="form-group row">
            {!! Form::label('Año', 'Año(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('ano',$selectAno, date('Y'), ['id'=>'ano','name'=>'ano', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Mes', 'Mes(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('mes',$selectMes, date('n'), ['id'=>'mes','name'=>'mes', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row" >   
            {!!Form::label('fechaPublicacion', 'Fecha de Publicación del Formulario(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            <div class="input-group date">        
            <input type="text" class="form-control group-date" id="fechaPublicacion" name="fechaPublicacion" value="{!! date('Y-m-d', strtotime($fe_desde))  !!}">
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
            </div>        
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Organismo que Dicta la Norma', 'Organismo que Dicta la Norma(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('organismo', null, ['class' => 'form-control', 'placeholder' => 'Organismo que dicta la norma']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Denominación, Título o Nombre de la Propuesta Normativa', 'Denominación, Título o Nombre de la Propuesta Normativa(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('titulo_denominacion', null, ['class' => 'form-control', 'placeholder' => 'Denominación, título o nombre de la propuesta normativa']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Tipo de Norma', 'Tipo de Norma(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('tipo_norma', null, ['class' => 'form-control', 'placeholder' => 'Tipo de norma']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Efectos de la Norma', 'Efectos de la Norma(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('efectos_norma', null, ['class' => 'form-control', 'placeholder' => 'Efectos de la norma']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Enlace al Formulario', 'Enlace al Formulario(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('enlace_formulario', null, ['class' => 'form-control', 'placeholder' => 'Enlace al formulario']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Enlace a Mayor Información', 'Enlace a Mayor Información(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('enlace', null, ['class' => 'form-control', 'placeholder' => 'Enlace a mayor información']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Nota Generador', 'Nota Generador', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('nota_generador', null, ['class' => 'form-control', 'placeholder' =>'Escriba una nota de descripción...']) !!}
          </div>
        </div> 