
        <div class="form-group row">
            {!! Form::label('Año', 'Año(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('ano',$selectAno, date('Y'), ['id'=>'ano','name'=>'ano', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Mes', 'Mes(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('mes',$selectMes, date('n'), ['id'=>'mes','name'=>'mes', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Título de la Auditoría', 'Título de la Auditoría(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('titulo_auditoria', null, ['class' => 'form-control', 'placeholder' => 'Título de la Auditoría']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Entidad que efectuó la Auditoría', 'Entidad que efectuó la Auditoría(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('entidad', null, ['class' => 'form-control', 'placeholder' => 'Entidad que efectuó la auditoría']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Materia', 'Materia(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('materia', null, ['class' => 'form-control', 'placeholder' => 'Materia']) !!}
          </div>
        </div>

        <div class="form-group row" >   
            {!!Form::label('FechaInicio', 'Fecha de Inicio de la Auditoría(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            <div class="input-group date">        
            <input type="text" class="form-control group-date" id="FechaInicio" name="FechaInicio" value="{!! date('Y-m-d', strtotime($fe_desde))  !!}">
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
            </div>        
          </div>
        </div>

        <div class="form-group row" >   
            {!!Form::label('FechaTermino', 'Fecha de Término de la Auditoría(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            <div class="input-group date">        
            <input type="text" class="form-control group-date" id="FechaTermino" name="FechaTermino"  value="{!! date('Y-m-d', strtotime($fe_hasta))  !!}">
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
            </div>        
          </div>
        </div>

        <div class="form-group row" >   
            {!!Form::label('InicioPeriodo', 'Inicio Periodo Auditado(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            <div class="input-group date">        
            <input type="text" class="form-control group-date" id="InicioPeriodo" name="InicioPeriodo" value="{!! date('Y-m-d', strtotime($fe_desde))  !!}">
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
            </div>        
          </div>
        </div>

        <div class="form-group row" >   
            {!!Form::label('FinPeriodo', 'Fin Periodo Auditado(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            <div class="input-group date">        
            <input type="text" class="form-control group-date" id="FinPeriodo" name="FinPeriodo"  value="{!! date('Y-m-d', strtotime($fe_hasta))  !!}">
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
            </div>        
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Respuesta del Servicio', 'Respuesta del Servicio(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('respuesta_servicio', null, ['class' => 'form-control', 'placeholder' =>'Escriba una nota de Respuesta del Servicio...']) !!}
          </div>
        </div> 

        <div class="form-group row" >   
            {!!Form::label('FechaPublicacion', 'Fecha Publicación(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            <div class="input-group date">        
            <input type="text" class="form-control group-date" id="FechaPublicacion" name="FechaPublicacion" value="{!! date('Y-m-d', strtotime($fe_desde))  !!}">
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
            </div>        
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Enlace al Informe Final de Auditoría', 'Enlace al Informe Final de Auditoría(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('enlace', null, ['class' => 'form-control', 'placeholder' => 'Enlace al Informe Final de Auditoría']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Nota Generador', 'Nota Generador', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('nota_generador', null, ['class' => 'form-control', 'placeholder' =>'Escriba una nota de descripción...']) !!}
          </div>
        </div> 



