
        <div class="form-group row">
            {!! Form::label('Año', 'Año(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('ano',$selectAno, date('Y'), ['id'=>'ano','name'=>'ano', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Mes', 'Mes(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('mes',$selectMes, date('n'), ['id'=>'mes','name'=>'mes', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Nombre del Consejo Consultivo', 'Nombre del Consejo Consultivo(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('nombre_consejo', null, ['class' => 'form-control', 'placeholder' => 'Nombre del Consejo Consultivo']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Forma de Integracion', 'Forma de Integracion(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('forma_integracion', null, ['class' => 'form-control', 'placeholder' => 'Escriba Forma de Integracion']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Nombre Consejero', 'Nombre Consejero(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('nombre_consejero', null, ['class' => 'form-control', 'placeholder' => 'Nombre Consejero']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Apellido Paterno Consejero', 'Apellido Paterno Consejero(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('apellido_paterno', null, ['class' => 'form-control', 'placeholder' => 'Apellido Paterno Consejero']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Apellido Materno Consejero', 'Apellido Materno Consejero(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('apellido_materno', null, ['class' => 'form-control', 'placeholder' => 'Apellido Materno Consejero']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Representacion o Calidades', 'Representacion o Calidades(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('representacion_calidades', null, ['class' => 'form-control', 'placeholder' => 'Escriba Representacion o Calidades']) !!}
          </div>
        </div>


        <div class="form-group row">
          {!! Form::label('Nota del Generador', 'Nota del Generador(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('nota_generador', null, ['class' => 'form-control', 'placeholder' =>'Escriba una nota de descripción...']) !!}
          </div>
        </div> 



