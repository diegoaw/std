
        <div class="form-group row">
            {!! Form::label('Año', 'Año(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('ano',$selectAno, date('Y'), ['id'=>'ano','name'=>'ano', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Mes', 'Mes(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('mes',$selectMes, date('n'), ['id'=>'mes','name'=>'mes', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Entidad con la que Existen Vínculos', 'Entidad con la que Existen Vínculos(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('entidad', null, ['class' => 'form-control', 'placeholder' => 'Entidad con la que existen vínculos']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Tipo de Vínculo', 'Tipo de Vínculo(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('tipo_vinculo', null, ['class' => 'form-control', 'placeholder' => 'Tipo de vínculo']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Descripcion o Detalle del Vínculo', 'Descripcion o Detalle del Vínculo(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'placeholder' => 'Escriba Descripcion o detalle del vínculo']) !!}
          </div>
        </div>

        <div class="form-group row" >   
            {!!Form::label('FechaInicio', 'Fecha de Inicio(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            <div class="input-group date">        
            <input type="text" class="form-control group-date" id="FechaInicio" name="FechaInicio" value="{!! date('Y-m-d', strtotime($fe_desde))  !!}">
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
            </div>        
          </div>
        </div>

        <div class="form-group row" >   
            {!!Form::label('FechaTermino', 'Fecha de Término(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            <div class="input-group date">        
            <input type="text" class="form-control group-date" id="FechaTermino" name="FechaTermino"  value="{!! date('Y-m-d', strtotime($fe_hasta))  !!}">
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
            </div>        
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Vinculo Indefinido', 'Vinculo Indefinido(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('vinculo_indefinido', null, ['class' => 'form-control', 'placeholder' => 'Vinculo indefinido']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Enlace a la Norma Jurídica o Convenio que Justifica el Vínculo', 'Enlace a la Norma Jurídica o Convenio que Justifica el Vínculo(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('enlace', null, ['class' => 'form-control', 'placeholder' => 'Enlace a la norma juridica o convenio que justifica el vínculo']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Nota Generador', 'Nota Generador', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('nota_generador', null, ['class' => 'form-control', 'placeholder' =>'Escriba una nota de descripción...']) !!}
          </div>
        </div> 