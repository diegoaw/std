
        <div class="form-group row">
            {!! Form::label('Año', 'Año(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('ano',$selectAno, date('Y'), ['id'=>'ano','name'=>'ano', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Mes', 'Mes(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('mes',$selectMes, date('n'), ['id'=>'mes','name'=>'mes', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Estamento', 'Estamento(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('estamento', null, ['class' => 'form-control', 'placeholder' => 'Estamento']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Grado', 'Grado(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('grado', null, ['class' => 'form-control', 'placeholder' => 'Grado']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Unidad Monetaria', 'Unidad Monetaria(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('unidad_monetaria', null, ['class' => 'form-control', 'placeholder' => 'Unidad Monetaria']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Sueldo Base', 'Sueldo Base(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('sueldo_base', null, ['class' => 'form-control', 'placeholder' => 'Sueldo Base']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Asignación', 'Asignación(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('asignacion', null, ['class' => 'form-control', 'placeholder' => 'Asignación']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Remuneración', 'Remuneración(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('remuneracion', null, ['class' => 'form-control', 'placeholder' => 'Remuneración']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Nota Generador', 'Nota Generador', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('nota_generador', null, ['class' => 'form-control', 'placeholder' =>'Escriba una nota de descripción...']) !!}
          </div>
        </div> 