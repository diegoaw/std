
        <div class="form-group row">
            {!! Form::label('Año', 'Año(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('ano',$selectAno, date('Y'), ['id'=>'ano','name'=>'ano', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Mes', 'Mes(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('mes',$selectMes, date('n'), ['id'=>'mes','name'=>'mes', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Unidad u Órgano Interno	Facultades', 'Unidad u Órgano Interno	Facultades(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('unidad_organo', null, ['class' => 'form-control', 'placeholder' => 'Unidad u órgano interno	facultades']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Funciones o Atribuciones', 'Funciones o Atribuciones(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('funciones_falcutades', null, ['class' => 'form-control', 'placeholder' => 'Funciones o atribuciones']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Fuente legal', 'Fuente legal(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('fuente_legal', null, ['class' => 'form-control', 'placeholder' => 'Fuente legal']) !!}
          </div>
        </div>

        <div class="form-group row" >   
            {!!Form::label('fechaPublicacion', 'Fecha Publicación(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            <div class="input-group date">        
            <input type="text" class="form-control group-date" id="fechaPublicacion" name="fechaPublicacion" value="{!! date('Y-m-d', strtotime($fe_desde))  !!}">
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
            </div>        
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Enlace a la Publicación o Archivo Correspondiente', 'Enlace a la Publicación o Archivo Correspondiente(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('enlace1', null, ['class' => 'form-control', 'placeholder' => 'Enlace a la publicación o archivo correspondiente']) !!}
          </div>
        </div>

        <div class="form-group row" >   
            {!!Form::label('FechaModificacion', 'Fecha Última Modificación(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            <div class="input-group date">        
            <input type="text" class="form-control group-date" id="FechaModificacion" name="FechaModificacion" value="{!! date('Y-m-d', strtotime($fe_desde))  !!}">
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
            </div>        
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Enlace Última Modificación', 'Enlace Última Modificación(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('enlace2', null, ['class' => 'form-control', 'placeholder' => 'Enlace última modificación']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Nota Generador', 'Nota Generador', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('nota_generador', null, ['class' => 'form-control', 'placeholder' =>'Escriba una nota de descripción...']) !!}
          </div>
        </div> 