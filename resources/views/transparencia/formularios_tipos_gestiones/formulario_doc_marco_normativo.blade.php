
        <div class="form-group row">
            {!! Form::label('Año', 'Año(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('ano',$selectAno, date('Y'), ['id'=>'ano','name'=>'ano', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Mes', 'Mes(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('mes',$selectMes, date('n'), ['id'=>'mes','name'=>'mes', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Tipo Marco Normativo', 'Tipo Marco Normativo(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('tipo_marco_normativo', null, ['class' => 'form-control', 'placeholder' => 'Tipo Marco normativo']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Tipo de Norma', 'Tipo de Norma(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('tipo_norma', null, ['class' => 'form-control', 'placeholder' => 'Tipo de Norma']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Número Norma', 'Número Norma(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('numero_norma', null, ['class' => 'form-control', 'placeholder' => 'Número Norma']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Denominación Norma', 'Denominación Norma(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('denominacion', null, ['class' => 'form-control', 'placeholder' => 'Denominación norma']) !!}
          </div>
        </div>

        <div class="form-group row" >   
            {!!Form::label('fechaPublicacion', 'Fecha Publicación en el DO(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            <div class="input-group date">        
            <input type="text" class="form-control group-date" id="fechaPublicacion" name="fechaPublicacion" value="{!! date('Y-m-d', strtotime($fe_desde))  !!}">
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
            </div>        
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Enlace a Texto Integro y Actualizado', 'Enlace a Texto Integro y Actualizado(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('enlace1', null, ['class' => 'form-control', 'placeholder' => 'Enlace a texto integro y actualizado']) !!}
          </div>
        </div>

        <div class="form-group row" >   
            {!!Form::label('FechaModificacion', 'Fecha Última Modificación o Derogación(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            <div class="input-group date">        
            <input type="text" class="form-control group-date" id="FechaModificacion" name="FechaModificacion" value="{!! date('Y-m-d', strtotime($fe_desde))  !!}">
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
            </div>        
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Enlace Última Modificación', 'Enlace Última Modificación(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('enlace2', null, ['class' => 'form-control', 'placeholder' => 'Enlace última modificación']) !!}
          </div>
        </div>


        <div class="form-group row">
          {!! Form::label('Nota Generador', 'Nota Generador', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('nota_generador', null, ['class' => 'form-control', 'placeholder' =>'Escriba una nota de descripción...']) !!}
          </div>
        </div> 