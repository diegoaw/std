
        <div class="form-group row">
            {!! Form::label('Año', 'Año(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('ano',$selectAno, date('Y'), ['id'=>'ano','name'=>'ano', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Mes', 'Mes(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('mes',$selectMes, date('n'), ['id'=>'mes','name'=>'mes', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Nombre del Mecanismo de Participación Ciudadana', 'Nombre del Mecanismo de Participación Ciudadana(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('nombre_mecanismo', null, ['class' => 'form-control', 'placeholder' => 'Nombre del mecanismo de participación ciudadana']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Breve Descripción de su Objetivo', 'Breve Descripción de su Objetivo(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'placeholder' =>'Escriba Breve descripción de su objetivo...']) !!}
          </div>
        </div> 

        <div class="form-group row">
          {!! Form::label('Requisitos para Participar', 'Requisitos para Participar(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('requisitos', null, ['class' => 'form-control', 'placeholder' =>'Escriba Requisitos para participar...']) !!}
          </div>
        </div> 

        <div class="form-group row">
            {!! Form::label('Enlace a Mayor Información', 'Enlace a Mayor Información(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('enlace', null, ['class' => 'form-control', 'placeholder' => 'Enlace a mayor información']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Nota Generador', 'Nota Generador', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('nota_generador', null, ['class' => 'form-control', 'placeholder' =>'Escriba una nota de descripción...']) !!}
          </div>
        </div> 