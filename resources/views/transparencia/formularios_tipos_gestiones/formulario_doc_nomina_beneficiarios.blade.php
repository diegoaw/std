
        <div class="form-group row">
            {!! Form::label('Año', 'Año(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('ano',$selectAno, date('Y'), ['id'=>'ano','name'=>'ano', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Mes', 'Mes(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('mes',$selectMes, date('n'), ['id'=>'mes','name'=>'mes', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Nombre Programa', 'Nombre Programa(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('nombre_programa', null, ['class' => 'form-control', 'placeholder' => 'Nombre programa']) !!}
          </div>
        </div>

        <div class="form-group row" >   
            {!!Form::label('fechaOtrogamiento', 'Fecha Otrogamiento(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            <div class="input-group date">        
            <input type="text" class="form-control group-date" id="fechaOtrogamiento" name="fechaOtrogamiento" value="{!! date('Y-m-d', strtotime($fe_desde))  !!}">
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
            </div>        
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Tipo de Acto', 'Tipo de Acto(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('tipo_acto', null, ['class' => 'form-control', 'placeholder' => 'Tipo de acto']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Denominación de Acto', 'Denominación de Acto(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('denominacion_acto', null, ['class' => 'form-control', 'placeholder' => 'Denominación de acto']) !!}
          </div>
        </div>

        <div class="form-group row" >   
            {!!Form::label('fechaActo', 'Fecha Acto(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            <div class="input-group date">        
            <input type="text" class="form-control group-date" id="fechaActo" name="fechaActo" value="{!! date('Y-m-d', strtotime($fe_desde))  !!}">
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
            </div>        
          </div>
        </div>        

        <div class="form-group row">
            {!! Form::label('Número del Acto', 'Número del Acto(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('n_acto', null, ['class' => 'form-control', 'placeholder' => 'Número del acto']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Razón Social', 'Razón Social(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('razon_social', null, ['class' => 'form-control', 'placeholder' => 'Razón social']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Nombre Beneficiario', 'Nombre Beneficiario(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Nombre beneficiario']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Apellido Paterno Beneficiario', 'Apellido Paterno Beneficiario(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('apellido_paterno', null, ['class' => 'form-control', 'placeholder' => 'Apellido Paterno beneficiario']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Apellido Materno Beneficiario', 'Apellido Materno Beneficiario(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('apellido_materno', null, ['class' => 'form-control', 'placeholder' => 'Apellido Materno beneficiario']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Nota Generador', 'Nota Generador', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('nota_generador', null, ['class' => 'form-control', 'placeholder' =>'Escriba una nota de descripción...']) !!}
          </div>
        </div> 