
        <div class="form-group row">
            {!! Form::label('Año', 'Año(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('ano',$selectAno, date('Y'), ['id'=>'ano','name'=>'ano', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Mes', 'Mes(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('mes',$selectMes, date('n'), ['id'=>'mes','name'=>'mes', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Tipo de Compra', 'Tipo de Compra(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('tipo_compra', null, ['class' => 'form-control', 'placeholder' => 'Tipo de compra']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Tipo de Acto Administrativo Aprobatorio', 'Tipo de Acto Administrativo Aprobatorio(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('tipo_acto', null, ['class' => 'form-control', 'placeholder' => 'Tipo de acto administrativo aprobatorio']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Denominación de Acto Administrativo Aprobatorio', 'Denominación de Acto Administrativo Aprobatorio(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('denominacion_acto', null, ['class' => 'form-control', 'placeholder' => 'Denominación de acto administrativo aprobatorio']) !!}
          </div>
        </div>

        <div class="form-group row" >   
            {!!Form::label('fechaContrato', 'Fecha del Acto Administrativo Aprobatorio del Contrato(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            <div class="input-group date">        
            <input type="text" class="form-control group-date" id="fechaContrato" name="fechaContrato" value="{!! date('Y-m-d', strtotime($fe_desde))  !!}">
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
            </div>        
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Número del Acto Administrativo Aprobatorio del Contrato', 'Número del Acto Administrativo Aprobatorio del Contrato(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('n_acto', null, ['class' => 'form-control', 'placeholder' => 'Número del acto administrativo aprobatorio']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Razón Social', 'Razón Social(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('razon_social', null, ['class' => 'form-control', 'placeholder' => 'Razón social']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Nombre', 'Nombre(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Nombre']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Apellido Paterno', 'Apellido Paterno(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('apellido_paterno', null, ['class' => 'form-control', 'placeholder' => 'Apellido Paterno']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Apellido Materno', 'Apellido Materno(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('apellido_materno', null, ['class' => 'form-control', 'placeholder' => 'Apellido Materno']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('RUT de la Persona Contratada', 'RUT de la Persona Contratada (si aplica)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('rut', null, ['class' => 'form-control', 'placeholder' => 'RUT de la persona contratada']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Socios y Accionistas Principales', 'Socios y Accionistas Principales (si corresponde)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('socio_accionista', null, ['class' => 'form-control', 'placeholder' => 'Socios y accionistas principale']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Objeto de la Contratación o Adquisición', 'Objeto de la Contratación o Adquisición(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('objeto_contratacion', null, ['class' => 'form-control', 'placeholder' => 'Escriba Objeto de la contratación o adquisición	']) !!}
          </div>
        </div>

        <div class="form-group row" >   
            {!!Form::label('FechaInicio', 'Fecha de Inicio del Contrato(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            <div class="input-group date">        
            <input type="text" class="form-control group-date" id="FechaInicio" name="FechaInicio" value="{!! date('Y-m-d', strtotime($fe_desde))  !!}">
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
            </div>        
          </div>
        </div>

        <div class="form-group row" >   
            {!!Form::label('FechaTermino', 'Fecha de Término del Contrato(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            <div class="input-group date">        
            <input type="text" class="form-control group-date" id="FechaTermino" name="FechaTermino"  value="{!! date('Y-m-d', strtotime($fe_hasta))  !!}">
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
            </div>        
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Monto Total de la Operación', 'Monto Total de la Operación(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('monto_operacion', null, ['class' => 'form-control', 'placeholder' => 'Monto total de la operación']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Tipo Unidad Monetaria', 'Tipo Unidad Monetaria(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('tipo_unidad', null, ['class' => 'form-control', 'placeholder' => 'Tipo unidad monetaria']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Observaciones', 'Observaciones(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('observaciones', null, ['class' => 'form-control', 'placeholder' =>'Escriba observaciones...']) !!}
          </div>
        </div> 

        <div class="form-group row">
            {!! Form::label('Enlace al Texto Integro del Contrato', 'Enlace al Texto Integro del Contrato(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('enlace_texto_contrato', null, ['class' => 'form-control', 'placeholder' => 'Enlace al texto integro del contrato']) !!}
          </div>
        </div>
        
        <div class="form-group row">
            {!! Form::label('Enlace al Texto Integr del Acto Administrativo Aprobatorio', 'Enlace al Texto Integr del Acto Administrativo Aprobatorio(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('enlace_texto_acto', null, ['class' => 'form-control', 'placeholder' => 'Enlace al texto integro del acto administrativo aprobatorio']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Enlace al Texto Integr del Acto Administrativo Aprobatorio de la Modificación', 'Enlace al Texto Integr del Acto Administrativo Aprobatorio de la Modificación(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('enlace_texto_acto_modificacion', null, ['class' => 'form-control', 'placeholder' => 'Enlace al texto integro del acto administrativo aprobatorio de la modificación']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Nota Generador', 'Nota Generador', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('nota_generador', null, ['class' => 'form-control', 'placeholder' =>'Escriba una nota de descripción...']) !!}
          </div>
        </div> 