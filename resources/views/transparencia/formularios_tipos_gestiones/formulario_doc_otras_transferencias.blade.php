
        <div class="form-group row">
            {!! Form::label('Año', 'Año(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('ano',$selectAno, date('Y'), ['id'=>'ano','name'=>'ano', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Mes', 'Mes(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('mes',$selectMes, date('n'), ['id'=>'mes','name'=>'mes', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row" >   
            {!!Form::label('fechaTransferencia', 'Fecha de la Transferencia(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            <div class="input-group date">        
            <input type="text" class="form-control group-date" id="fechaTransferencia" name="fechaTransferencia" value="{!! date('Y-m-d', strtotime($fe_desde))  !!}">
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
            </div>        
          </div>
        </div>    

        <div class="form-group row">
            {!! Form::label('Denominación de la Transferencia', 'Denominación de la Transferencia(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('denominacion_transferencia', null, ['class' => 'form-control', 'placeholder' => 'Denominación de la transferencia']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Monto', 'Monto(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('monto', null, ['class' => 'form-control', 'placeholder' => 'Monto']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Tipo Unidad Monetaria', 'Tipo Unidad Monetaria(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('tipo_unidad', null, ['class' => 'form-control', 'placeholder' => 'Tipo unidad monetaria']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Imputación Presupuestaria', 'Imputación Presupuestaria(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('imputacion', null, ['class' => 'form-control', 'placeholder' => 'Imputación presupuestaria']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Objeto de la Transferencia', 'Objeto de la Transferencia(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('objeto_transferencia', null, ['class' => 'form-control', 'placeholder' => 'Escriba Objeto de la transferencia']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Razón Social', 'Razón Social de la Persona Jurídica que Recibe la Transferencia(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('razon_social', null, ['class' => 'form-control', 'placeholder' => 'Razón social de la persona jurídica que recibe la Transferencia']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Nombre', 'Nombre de la Persona Natural que Recibe la Transferencia(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Nombre de la persona natural que recibe la transferencia']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Apellido Paterno', 'Apellido Paterno de la Persona Natural que Recibe la Transferencia(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('apellido_paterno', null, ['class' => 'form-control', 'placeholder' => 'Apellido Paterno de la persona natural que recibe la transferencia']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Apellido Materno', 'Apellido Materno de la Persona Natural que Recibe la Transferencia(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('apellido_materno', null, ['class' => 'form-control', 'placeholder' => 'Apellido Materno de la persona natural que recibe la transferencia']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Nota Generador', 'Nota Generador', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('nota_generador', null, ['class' => 'form-control', 'placeholder' =>'Escriba una nota de descripción...']) !!}
          </div>
        </div> 