
        <div class="form-group row">
            {!! Form::label('Año', 'Año(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('ano',$selectAno, date('Y'), ['id'=>'ano','name'=>'ano', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Mes', 'Mes(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('mes',$selectMes, date('n'), ['id'=>'mes','name'=>'mes', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Estamento', 'Estamento(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('estamento', null, ['class' => 'form-control', 'placeholder' => 'Estamento']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Tipo Personal', 'Tipo Personal(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('tipo_personal', null, ['class' => 'form-control', 'placeholder' => 'Tipo Personal']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Apellido Paterno', 'Apellido Paterno(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('apellido_paterno', null, ['class' => 'form-control', 'placeholder' => 'Apellido Paterno']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Apellido Materno', 'Apellido Materno(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('apellido_materno', null, ['class' => 'form-control', 'placeholder' => 'Apellido Materno']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Nombres', 'Nombres(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('nombres', null, ['class' => 'form-control', 'placeholder' => 'Nombres']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Grado EUS', 'Grado EUS (si corresponde)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('grado', null, ['class' => 'form-control', 'placeholder' => 'Grado EUS']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Calificación Profesional o Formación', 'Calificación Profesional o Formación(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('calificacion_profesional', null, ['class' => 'form-control', 'placeholder' => 'Calificación profesional o formación']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Cargo o Función', 'Cargo o Función(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('cargo', null, ['class' => 'form-control', 'placeholder' => 'Cargo o función']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Región', 'Región(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('region',$selectRegion, date('n'), ['id'=>'region','name'=>'region', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Asignaciones Especiales', 'Asignaciones Especiales(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('asignaciones_especiales', null, ['class' => 'form-control', 'placeholder' => 'Asignaciones especiales']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Unidad Monetaria', 'Unidad Monetaria(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('unidad_monetaria', null, ['class' => 'form-control', 'placeholder' => 'Unidad monetaria']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Remuneración Bruta Mensualizada', 'Remuneración Bruta Mensualizada(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('remuneracion_bruta', null, ['class' => 'form-control', 'placeholder' => 'Remuneración bruta mensualizada']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Remuneración Líquida Mensualizada', 'Remuneración Líquida Mensualizada(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('remuneracion_liquida', null, ['class' => 'form-control', 'placeholder' => 'Remuneración líquida mensualizada']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Horas Extraordinarias', 'Horas Extraordinarias(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('horas_extraordinarias', null, ['class' => 'form-control', 'placeholder' => 'Horas extraordinarias']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Nº Horas Diurnas', 'Nº Horas Diurnas(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('n_horas_diurnas', null, ['class' => 'form-control', 'placeholder' => 'Nº horas diurnas']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Remuneración Horas Diurnas', 'Remuneración Horas Diurnas(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('remuneracion_horas_diurnas', null, ['class' => 'form-control', 'placeholder' => 'Remuneración horas diurnas']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Nº Horas Nocturnas', 'Nº Horas Nocturnas(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('n_horas_nocturnas', null, ['class' => 'form-control', 'placeholder' => 'Nº horas nocturnas']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Remuneración Horas Nocturnas', 'Remuneración Horas Nocturnas(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('remuneracion_horas_nocturnas', null, ['class' => 'form-control', 'placeholder' => 'Remuneración horas nocturnas']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Nº Horas Festivas', 'Nº Horas Festivas(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('n_horas_festivas', null, ['class' => 'form-control', 'placeholder' => 'Nº horas festivas']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Remuneración Horas Festivas', 'Remuneración Horas Festivas(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('remuneracion_horas_festivas', null, ['class' => 'form-control', 'placeholder' => 'Remuneración horas festivas']) !!}
          </div>
        </div>

        <div class="form-group row" >   
            {!!Form::label('FechaInicio', 'Fecha de Inicio(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            <div class="input-group date">        
            <input type="text" class="form-control group-date" id="FechaInicio" name="FechaInicio" value="{!! date('Y-m-d', strtotime($fe_desde))  !!}">
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
            </div>        
          </div>
        </div>

        <div class="form-group row" >   
            {!!Form::label('FechaTermino', 'Fecha de Término(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            <div class="input-group date">        
            <input type="text" class="form-control group-date" id="FechaTermino" name="FechaTermino"  value="{!! date('Y-m-d', strtotime($fe_hasta))  !!}">
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
            </div>        
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Observaciones', 'Observaciones(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('observaciones', null, ['class' => 'form-control', 'placeholder' =>'Escriba observaciones...']) !!}
          </div>
        </div> 

        <div class="form-group row">
            {!! Form::label('Declaración de Patrimonio', 'Declaración de Patrimonio(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('declaracion_patrimonio', null, ['class' => 'form-control', 'placeholder' => 'Declaración de Patrimonio']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Declaración de Intereses', 'Declaración de Intereses(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('declaracion_intereses', null, ['class' => 'form-control', 'placeholder' => 'Declaración de Intereses']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Viaticos', 'Viaticos(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('viaticos', null, ['class' => 'form-control', 'placeholder' => 'Viaticos']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Nota Generador', 'Nota Generador', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('nota_generador', null, ['class' => 'form-control', 'placeholder' =>'Escriba una nota de descripción...']) !!}
          </div>
        </div> 