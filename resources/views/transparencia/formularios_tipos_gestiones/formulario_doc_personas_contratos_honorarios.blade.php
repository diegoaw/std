
        <div class="form-group row">
            {!! Form::label('Año', 'Año(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('ano',$selectAno, date('Y'), ['id'=>'ano','name'=>'ano', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Mes', 'Mes(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('mes',$selectMes, date('n'), ['id'=>'mes','name'=>'mes', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Apellido Paterno', 'Apellido Paterno(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('apellido_paterno', null, ['class' => 'form-control', 'placeholder' => 'Apellido Paterno']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Apellido Materno', 'Apellido Materno(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('apellido_materno', null, ['class' => 'form-control', 'placeholder' => 'Apellido Materno']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Nombres', 'Nombres(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('nombres', null, ['class' => 'form-control', 'placeholder' => 'Nombres']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Grado EUS', 'Grado EUS (si corresponde)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('grado', null, ['class' => 'form-control', 'placeholder' => 'Grado EUS']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Descripción de la Función', 'Descripción de la Función(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('descripcion_funcion', null, ['class' => 'form-control', 'placeholder' => 'Escriba Descripción de la función']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Calificación Profesional o Formación', 'Calificación Profesional o Formación(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('calificacion_profesional', null, ['class' => 'form-control', 'placeholder' => 'Calificación profesional o formación']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Región', 'Región(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('region',$selectRegion, date('n'), ['id'=>'region','name'=>'region', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Unidad Monetaria', 'Unidad Monetaria(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('unidad_monetaria', null, ['class' => 'form-control', 'placeholder' => 'Unidad monetaria']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Honorario Total Bruto', 'Honorario Total Bruto(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('honorario_total_bruto', null, ['class' => 'form-control', 'placeholder' => 'Honorario total bruto']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Remuneración Líquida Mensualizada', 'Remuneración Líquida Mensualizada(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('remuneracion_liquida', null, ['class' => 'form-control', 'placeholder' => 'Remuneración líquida mensualizada']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Pago Mensual', 'Pago Mensual(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('pago_mensual', null, ['class' => 'form-control', 'placeholder' => 'Pago mensual']) !!}
          </div>
        </div>

        <div class="form-group row" >   
            {!!Form::label('FechaInicio', 'Fecha de Inicio(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            <div class="input-group date">        
            <input type="text" class="form-control group-date" id="FechaInicio" name="FechaInicio" value="{!! date('Y-m-d', strtotime($fe_desde))  !!}">
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
            </div>        
          </div>
        </div>

        <div class="form-group row" >   
            {!!Form::label('FechaTermino', 'Fecha de Término(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            <div class="input-group date">        
            <input type="text" class="form-control group-date" id="FechaTermino" name="FechaTermino"  value="{!! date('Y-m-d', strtotime($fe_hasta))  !!}">
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
            </div>        
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Observaciones', 'Observaciones(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('observaciones', null, ['class' => 'form-control', 'placeholder' =>'Escriba observaciones...']) !!}
          </div>
        </div> 

        <div class="form-group row">
            {!! Form::label('Declaración de Patrimonio', 'Declaración de Patrimonio(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('declaracion_patrimonio', null, ['class' => 'form-control', 'placeholder' => 'Declaración de Patrimonio']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Declaración de Intereses', 'Declaración de Intereses(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('declaracion_intereses', null, ['class' => 'form-control', 'placeholder' => 'Declaración de Intereses']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Viaticos', 'Viaticos(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('viaticos', null, ['class' => 'form-control', 'placeholder' => 'Viaticos']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Nota Generador', 'Nota Generador', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('nota_generador', null, ['class' => 'form-control', 'placeholder' =>'Escriba una nota de descripción...']) !!}
          </div>
        </div> 