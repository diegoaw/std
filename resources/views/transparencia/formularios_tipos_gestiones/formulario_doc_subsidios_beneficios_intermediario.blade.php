
        <div class="form-group row">
            {!! Form::label('Año', 'Año(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('ano',$selectAno, date('Y'), ['id'=>'ano','name'=>'ano', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Mes', 'Mes(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('mes',$selectMes, date('n'), ['id'=>'mes','name'=>'mes', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Nombre Subsidio Beneficio', 'Nombre Subsidio Beneficio(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('nombre_subsidio', null, ['class' => 'form-control', 'placeholder' => 'Nombre subsidio beneficio']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Número de Beneficiarios', 'Número de Beneficiarios(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('n_beneficiarios', null, ['class' => 'form-control', 'placeholder' => 'Número de beneficiarios']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Razones de la Exclusión de Datos', 'Razones de la Exclusión de Datos(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('razon', null, ['class' => 'form-control', 'placeholder' => 'Razones de la exclusión de datos']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Nombre del Programa', 'Nombre del Programa(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('nombre_programa', null, ['class' => 'form-control', 'placeholder' => 'Nombre del programa']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Enlace a Mayor Información', 'Enlace a Mayor Información(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('enlace', null, ['class' => 'form-control', 'placeholder' => 'Enlace a mayor información']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Nota Generador', 'Nota Generador', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('nota_generador', null, ['class' => 'form-control', 'placeholder' =>'Escriba una nota de descripción...']) !!}
          </div>
        </div> 