
        <div class="form-group row">
            {!! Form::label('Año', 'Año(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('ano',$selectAno, date('Y'), ['id'=>'ano','name'=>'ano', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Mes', 'Mes(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('mes',$selectMes, date('n'), ['id'=>'mes','name'=>'mes', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Tipo Subsidio o Beneficio', 'Tipo Subsidio o Beneficio(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('tipo_subsidio', null, ['class' => 'form-control', 'placeholder' => 'Tipo subsidio o beneficio']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Unidad, Órgano Interno o Dependencia que lo Gestiona', 'Unidad, Órgano Interno o Dependencia que lo Gestiona(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('unidad_organo_dependencia', null, ['class' => 'form-control', 'placeholder' => 'Unidad, órgano interno o dependencia que lo gestiona']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Requisitos y Antecedentes para Postular', 'Requisitos y Antecedentes para Postular(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('requisitos_antecedentes', null, ['class' => 'form-control', 'placeholder' => 'Requisitos y antecedentes para postular	']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Monto Global Asignado', 'Monto Global Asignado(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('monto_global', null, ['class' => 'form-control', 'placeholder' => 'Monto global asignado']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Tipo Unidad Monetaria', 'Tipo Unidad Monetaria(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('tipo_unidad_monetaria', null, ['class' => 'form-control', 'placeholder' => 'Tipo unidad monetaria']) !!}
          </div>
        </div>

        <div class="form-group row" >   
            {!!Form::label('inicioPostulacion', 'Inicio Período o Plazo de Postulación(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            <div class="input-group date">        
            <input type="text" class="form-control group-date" id="inicioPostulacion" name="inicioPostulacion" value="{!! date('Y-m-d', strtotime($fe_desde))  !!}">
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
            </div>        
          </div>
        </div>

        <div class="form-group row" >   
            {!!Form::label('finPostulacion', 'Fin Período o Plazo de Postulación(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            <div class="input-group date">        
            <input type="text" class="form-control group-date" id="finPostulacion" name="finPostulacion"  value="{!! date('Y-m-d', strtotime($fe_hasta))  !!}">
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
            </div>        
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Criterio de Evaluación y Asignación', 'Criterio de Evaluación y Asignación(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('criterio_evaluacion', null, ['class' => 'form-control', 'placeholder' => 'Criterio de evaluación y asignación']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Plazos Asociados al Procedimiento de Evaluación y Asignación', 'Plazos Asociados al Procedimiento de Evaluación y Asignación(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('plazos_asociados', null, ['class' => 'form-control', 'placeholder' => 'Plazos asociados al procedimiento de evaluación y asignación']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Objetivo del Subsidio o Beneficio', 'Objetivo del Subsidio o Beneficio(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('objetivo_subsidio', null, ['class' => 'form-control', 'placeholder' => 'Objetivo del subsidio o beneficio']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Tipo', 'Tipo(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('tipo', null, ['class' => 'form-control', 'placeholder' => 'Tipo']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Denominación', 'Denominación(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('denominacion', null, ['class' => 'form-control', 'placeholder' => 'Denominación']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Número', 'Número(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('numero', null, ['class' => 'form-control', 'placeholder' => 'Número']) !!}
          </div>
        </div>

        <div class="form-group row" >   
            {!!Form::label('fecha', 'Fecha(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            <div class="input-group date">        
            <input type="text" class="form-control group-date" id="fecha" name="fecha" value="{!! date('Y-m-d', strtotime($fe_desde))  !!}">
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
            </div>        
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Link Texto Integro', 'Link Texto Integro(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('link_texto_integro', null, ['class' => 'form-control', 'placeholder' => 'Link texto integro']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Número de Beneficiarios', 'Número de Beneficiarios(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('numero_beneficiarios', null, ['class' => 'form-control', 'placeholder' => 'Número de beneficiarios']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Razones de la Exclusión de Datos', 'Razones de la Exclusión de Datos(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('razon', null, ['class' => 'form-control', 'placeholder' => 'Razones de la exclusión de datos']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Nombre del Programa', 'Nombre del Programa(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('nombre_programa', null, ['class' => 'form-control', 'placeholder' => 'Nombre del programa']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Enlace a Mayor Información', 'Enlace a Mayor Información(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('enlace', null, ['class' => 'form-control', 'placeholder' => 'Enlace a mayor información']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Nota Generador', 'Nota Generador', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('nota_generador', null, ['class' => 'form-control', 'placeholder' =>'Escriba una nota de descripción...']) !!}
          </div>
        </div> 