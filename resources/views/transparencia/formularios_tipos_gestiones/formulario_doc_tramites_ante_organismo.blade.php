
        <div class="form-group row">
            {!! Form::label('Año', 'Año(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('ano',$selectAno, date('Y'), ['id'=>'ano','name'=>'ano', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Mes', 'Mes(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('mes',$selectMes, date('n'), ['id'=>'mes','name'=>'mes', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Descripción del Servicio', 'Descripción del Servicio(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'placeholder' =>'Escriba una Descripción del servicio...']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Requisitos y Antecedentes', 'Requisitos y Antecedentes(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('requisitos_antecedentes', null, ['class' => 'form-control', 'placeholder' =>'Escriba Requisitos y antecedentes...']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Trámites en Línea', 'Trámites en Línea(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('tramites_enlinea', null, ['class' => 'form-control', 'placeholder' =>'Escriba Trámites en línea...']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Trámites a Realizar o Etapas', 'Trámites a Realizar o Etapas(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('tramites_arealizar', null, ['class' => 'form-control', 'placeholder' =>'Escriba Trámites a realizar o etapas...']) !!}
          </div>
        </div>
        
        <div class="form-group row">
            {!! Form::label('Valor', 'Valor(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('valor', null, ['class' => 'form-control', 'placeholder' => 'Valor']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Lugar Donde se Realiza', 'Lugar Donde se Realiza(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('donde_realizan', null, ['class' => 'form-control', 'placeholder' => 'Lugar donde se realiza']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Información Complementaria', 'Información Complementaria(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('informacion_complementaria', null, ['class' => 'form-control', 'placeholder' =>'Escriba Información complementaria...']) !!}
          </div>
        </div> 

        <div class="form-group row">
          {!! Form::label('Nota Generador', 'Nota Generador', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('nota_generador', null, ['class' => 'form-control', 'placeholder' =>'Escriba una nota de descripción...']) !!}
          </div>
        </div> 