@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-link"></i><b> Transparencia - Enlaces de Documentos Gestiones</b></h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title"><b> Generar Enlace de Documentos Gestiones</b></h5>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">


                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="text-center">
                        {!! link_to_route('transparencia_crear_generar_enlace', 'Generar Nuevo Enlace', [], ['class' => 'btn btn-primary btn-sm','title'=>'Generar Nuevo Enlace']) !!}
                      </div>
                    </div>
                  </div>


          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@if(count($dataDefinitiva)>0)
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">Transparencia - Enlaces de Documentos Gestiones</h5>

          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th nowrap style="color:white">ID</th>                        
                        <th nowrap style="color:white">Mes</th>
                        <th nowrap style="color:white">Año</th>
                        <th nowrap style="color:white">Items</th>
                        <th nowrap style="color:white">Descripción</th>
                        <th nowrap style="color:white"><center>Enlace Generado</center></th>
                        <th nowrap style="color:white"><center>Editar</center></th>
                        <th nowrap style="color:white"><center>Descargar Documento</center></th>
                        <th nowrap style="color:white"><center>Eliminar</center></th>
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($dataDefinitiva)>0)
                      @foreach ($dataDefinitiva as $indice => $tramite)
                          <tr>
                           <td nowrap>{{$tramite->id}}</td>
                           <td nowrap>{{$tramite->mes}}</td>
                           <td nowrap>{{$tramite->ano}}</td>
                           <td nowrap>{{$tramite->item->nombre}}</td>
                           <td nowrap>{{$tramite->descripcion}}</td>
                           <td nowrap><center><a class="btn accion btn-info accion" data-toggle="collapse" {!! "data-target='#demo".$indice."'" !!}   {!!"data-parent='#myTable".$tramite->id."'" !!}><i class="fa fa-eye"></i></a></center></td>
                           <td nowrap><center><a class="btn accion btn-warning accion" href="{{route('transparencia_editar_generar_enlace',$tramite->id)}}"><i class="fa fa-pencil"></i></a></center></td>
                           <td nowrap><center><a class="btn accion btn-success accion" href="{{$tramite->url_aws}}"  target=" "><i class="fa fa-download"></i></a></center></td>
                           <td nowrap><center><a data-toggle="tooltip" class="btn btn-sm btn-danger" href="#" OnClick="Eliminar('{{$tramite->id}}')"><i class="fa fa-trash"></i></a></center></td>
                          </tr>

                          <tr  {!! "id='demo".$indice."'"  !!} class="collapse">
                            <td colspan="10" class="hiddenRow">
                              <table class="table table-striped table-sm" {!!  "id='myTable".$tramite->id."'" !!}  >
                                <thead class="thead-primary">
                                  <tr style="background-color: #fddadf;">
                                  <tr style="background-color: #cd0b27;">
                                  <th style="color:white">Enlace Generado</th>
                                  </tr>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                  <tr>
                                  <td >{{$tramite->url_aws}}</td>
                                  </tr>                    
                                </tbody>
                              </table>
                            </td>
                          </tr>


                      @endforeach
                @endif
                </tbody>
              </table>
          </div>
@endif
@endsection
@section('before-scripts-end')
@include('includes.scripts.transparencia_generar_enlace')
@stop