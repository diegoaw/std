@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-link"></i><b> Transparencia - Enlaces de Documentos Gestiones</b></h1><br> 
    </div>    
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
{!! Form::open(['route' => 'transparencia_nuevo_generar_enlace', 'files' => true, 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','id'=>'form-create']) !!}
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-danger card-outline">
        <h3 class="card-title"><b>Crear Enlaces de Documentos Gestiones</b></h3>
      </div>
      <div class="card-body">

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>

        <div class="callout col-sm-8 offset-sm-2" style="color: #737373;">
          <h4 style="font-size: 14px;"><i class="fa fa-info-circle" style="color: #ff0000;"></i> Nota:</h4>
          <span>Los campos marcados con un asterisco (*) son obligatorios.</span>
        </div><br>


        <div class="form-group row">
          {!! Form::label('id_items', 'Items(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('id_items',$selectItems, null, ['id'=>'id_items','name'=>'id_items', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('ano', 'Año(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('ano',$selectAno, date('Y'), ['id'=>'ano','name'=>'ano', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('mes', 'Mes(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('mes',$selectMes, date('n'), ['id'=>'mes','name'=>'mes', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('descripcion', 'Descripción(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'placeholder' =>'Escriba una descripción...']) !!}
          </div>
        </div> 

        <div class="field_wrapper">
        <div class="form-group row col-md-12">
          {!! Form::label('filename', 'Cargar Archivo(*)', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-md-6">
          {!! Form::file('filename', ['class' => 'form-control']) !!}
          </div>
        </div>
      </div>

        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! Form::submit('Generar Enlace', ['class' => 'btn btn-success btn-sm','title'=>'Generar Enlace']) !!}
              {!! link_to_route('transparencia_generar_enlace', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) !!}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{!! Form::close() !!}
@stop
@section('after-scripts-end')
@endsection