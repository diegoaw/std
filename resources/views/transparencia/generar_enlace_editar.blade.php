@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-link"></i><b> Transparencia - Enlaces de Documentos Gestiones</b></h1><br> 
    </div>    
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
{!! Form::open(['route' => ['transparencia_update_generar_enlace', $editEnlace->id], 'files' => true, 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','id'=>'form-create']) !!}

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-danger card-outline">
        <h3 class="card-title"><b>Editar Enlaces de Documentos Gestiones</b></h3>
      </div>
      <div class="card-body">

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('id_items', 'Items', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('id_items',$selectItems, $editEnlace->id_items, ['id'=>'id_items','name'=>'id_items', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('ano', 'Año', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('ano',$selectAno, $editEnlace->ano, ['id'=>'ano','name'=>'ano', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
            {!! Form::label('mes', 'Mes', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::select('mes',$selectMes, $editEnlace->mes, ['id'=>'mes','name'=>'mes', 'class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('descripcion', 'Descripción', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::textarea('descripcion', $editEnlace->descripcion, ['class' => 'form-control']) !!}
          </div>
        </div> 

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            <h4><b>Archivo Cargado</b></h4>
            <a href="{{$editEnlace->url_aws}}"  target=" "><b>{{$editEnlace->filename}}</b></a>
          </div>
        </div>

        <div class="field_wrapper">
        <div class="form-group row col-md-12">
          {!! Form::label('filename', 'Cargar Archivo', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-md-6">
          {!! Form::file('filename', ['class' => 'form-control']) !!}

          <p style="color:Red;">* Cargue adjunto si desea reemplazar el existente, de no ser asi deje este campo en blanco.</p>
          </div>
        </div>
      </div>

        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! Form::submit('Guardar y Generar Enlace', ['class' => 'btn btn-success btn-sm','title'=>'Generar Enlace']) !!}
              {!! link_to_route('transparencia_generar_enlace', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) !!}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{!! Form::close() !!}
@stop
@section('after-scripts-end')
@endsection