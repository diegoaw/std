@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-file-text"></i> Transparencia - Periodos</h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title">Generar Fechas de Periodos</h5>
      </div>
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">
                  <div class="form-group">
                    <div class="col-sm-12">
                    {!! Form::open(['route' => 'transparencia_generar_fecha_periodo', 'method' => 'get' , 'class'=>"form-horizontal"]) !!}
                      <div class="text-center">
                      <div class="col-sm-3">
                      <label for="tipo" class=" col-form-label"> Año a Generar: </label>
                      {!! Form::select('ano',$arrayAnoDis,['id'=>'ano','name'=>'ano', 'class'=>'form-control']) !!}
                     </div>
                     {!! Form::submit('Generar', ['class' => 'btn btn-success btn-sm','title'=>'Generar']) !!}
                     {!! link_to_route('Periodos.index', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) !!}
                     </div>
                     {!! Form::close() !!}
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">Transparencia - Fechas Periodos</h5>
          </div>
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm">
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th nowrap style="color:white">ID</th>
                        <th nowrap style="color:white">periodo</th>
                        <th nowrap style="color:white">mes</th>
                        <th nowrap style="color:white">fecha desde</th>
                        <th nowrap style="color:white">fecha hasta</th>
                        
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($dataDefinitiva)>0)
                      @foreach ($dataDefinitiva as $indice => $tramite)
                          <tr>
                           <td nowrap style="width:5%">{{$tramite->id}}</td>
                           <td nowrap style="width:40%" >{{$tramite->periodo->nombre}}
                           <td nowrap style="width:20%">{{$tramite->mes}}</td>
                           <td nowrap style="width:10%">{{$tramite->fecha_desde}}</td>
                           <td nowrap style="width:10%">{{$tramite->fecha_hasta}}</td>
                           </td>
                          </tr>
                      @endforeach
                @endif
                </tbody>
              </table>
          </div>
        </div>
      </div>

@endsection
@section('before-scripts-end')
@include('includes.scripts.transparencia_periodos')
@stop
