@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-list"></i> Transparencia - Items</h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title">Nuevo Item</h5>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">


                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="text-center">
                      {!! link_to_route('transparencia_crear_items', 'Nuevo Item', [], ['class' => 'btn btn-primary btn-sm','title'=>'Nuevo Item']) !!}
                      </div>
                    </div>
                  </div>



          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@if(count($dataDefinitiva)>0)
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">Transparencia - Items</h5>

          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th nowrap style="color:white">ID</th>
                        <th nowrap style="color:white">Nombre</th>
                        <th nowrap style="color:white">División</th>
                        <th nowrap style="color:white"><center>Editar</center></th>
                        <th nowrap style="color:white"><center>Asignación Usuario</center></th>
                        <th nowrap style="color:white"><center>Eliminar</center></th>
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($dataDefinitiva)>0)
                      @foreach ($dataDefinitiva as $indice => $tramite)
                          <tr>
                           <td nowrap>{{$tramite->id}}</td>
                           <td nowrap>{{$tramite->nombre}}</td>
                           <td nowrap>{{$tramite->division}}</td>
                           <td nowrap><center><a class="btn accion btn-success accion" href="{{route('transparencia_editar_items',$tramite->id)}}"><i class="fa fa-pencil"></i></a></center></td>
                           <td nowrap><center><a class="btn accion btn-warning accion" href="{{route('transparencia_items_usuario',$tramite->id)}}"><i class="fa fa-user"></i></a></center></td>
                           <td nowrap><center><a data-toggle="tooltip" class="btn btn-sm btn-danger" href="#" OnClick="Eliminar('{{$tramite->id}}')"><i class="fa fa-trash"></i></a></center></td>
                          </tr> 
                          
                        

                      @endforeach
                @endif
                </tbody>
              </table>
          </div>
@endif
@endsection
@section('before-scripts-end')
@include('includes.scripts.transparencia_items')
@stop
