@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-file-text"></i> Transparencia - Periodos</h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title">Nuevo Periodo</h5>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">


                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="text-center">
                      <a title="Crear usuario" class="btn btn-success" href="{{route('Periodos.create')}}"><i class="fa fa-plus"></i> Crear Periodo</a>
                      <a title="Crear usuario" class="btn btn-success" href="{{route('transparencia_generar_fecha')}}"><i class="fa fa-plus"></i> Generar Fechas</a>
                      </div>
                    </div>
                  </div>


          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">Transparencia - Periodos</h5>
          </div>
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th nowrap style="color:white">ID</th>
                        <th nowrap style="color:white">Nombre</th>
                        <th nowrap style="color:white">Cantidad de dias</th>
                        <th nowrap style="color:white">Acciones</th>
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($dataDefinitiva)>0)
                      @foreach ($dataDefinitiva as $indice => $tramite)
                          <tr>
                           <td nowrap style="width:5%">{{$tramite->id}}</td>
                           <td nowrap style="width:70%">{{$tramite->nombre}}</td>
                           <td nowrap style="width:10%">{{$tramite->cant_dias}}</td>
                           <td nowrap style="width:10%" ><a class="btn accion btn-success accion" href="{{route('Periodos.edit',$tramite->id)}}"><i class="fa fa-pencil"></i></a>
                           <a data-toggle="tooltip" class="btn btn-sm btn-danger" href="#" OnClick="Eliminar('{{$tramite->id}}')"><i class="fa fa-trash"></i></a></td>
                          </tr>
                      @endforeach
                @endif
                </tbody>
              </table>
          </div>
        </div>
      </div>

@endsection
@section('before-scripts-end')
@include('includes.scripts.transparencia_periodos')
@stop
