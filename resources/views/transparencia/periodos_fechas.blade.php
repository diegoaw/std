@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-file-text"></i> Transparencia - Fechas de Periodos</h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
       
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">




          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@if(count($dataDefinitiva)>0)
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">Transparencia - Fechas de Periodos</h5>

          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th nowrap style="color:white">ID</th>
                        <th nowrap style="color:white">Mes</th>
                        <th nowrap style="color:white">Fecha Desde</th>
                        <th nowrap style="color:white">Fecha Hasta</th>
                        <th nowrap style="color:white">Periodo</th>
                        <th nowrap style="color:white">Editar</th>
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($dataDefinitiva)>0)
                      @foreach ($dataDefinitiva as $indice => $tramite)
                          <tr>
                           <td nowrap>{{$tramite->id}}</td>
                           <td nowrap>{{$tramite->mes}}</td>
                           <td nowrap>{{$tramite->fecha_desde}}</td>
                           <td nowrap>{{$tramite->fecha_hasta}}</td>
                           <td nowrap>{{$tramite->Periodo->nombre}}</td>
                           <td nowrap><a class="btn accion btn-success accion" href="{{route('periodosfechas_editar',$tramite->id)}}"><i class="fa fa-pencil"></i></a></td>
                           </tr>
                      @endforeach
                @endif
                </tbody>
              </table>
          </div>
@endif
@endsection
@section('before-scripts-end')
@include('includes.scripts.transparencia_tipos')
@stop
