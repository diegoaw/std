@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-file-text"></i> Transparencia - Editar Fechas de Periodo</h1><br>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
{!! Form::open(['route' => ['periodosfechas_update', $editFechasPeriodos->id], 'files' => true, 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','id'=>'form-create']) !!}
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-danger card-outline">
        <h3 class="card-title">Editar Fechas de Periodo</h3>
      </div>
      <div class="card-body">

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>

        <div class="callout col-sm-8 offset-sm-2" style="color: #737373;">
          <h4 style="font-size: 14px;"><i class="fa fa-info-circle" style="color: #ff0000;"></i> Nota:</h4>
          <span>Los campos marcados con un asterisco (*) son obligatorios.</span>
        </div><br>

  
        <div class="form-group row">
          {!! Form::label('periodo_id', 'Periodo', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('periodo_id', $editFechasPeriodos->Periodo->nombre,  ['class' => 'form-control', 'placeholder' => 'Periodo', 'readonly' => 'readonly', 'id'=>'periodo_id','name'=>'periodo_id']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('mes', 'Mes', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('mes', $editFechasPeriodos->mes ,['class' => 'form-control', 'placeholder' => 'Mes', 'readonly' => 'readonly', 'id'=>'mes','name'=>'mes']) !!}
          </div>
        </div>
      

                 <div class="form-group row" >
                    <div class="col-sm-2"></div>
                      <div class="form-group row {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                      {!!Form::label('fecha_desde', 'Fecha Desde', array('class' => 'col-sm-6 col-form-label'))!!}
                      <div class="col-sm-6">
                        <div class="input-group date">
                          <input type="text" class="form-control group-date" id="fecha_desde" name="fecha_desde" value="{!! date('Y-m-d', strtotime($editFechasPeriodos->fecha_desde))  !!}">
                          <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                 <div class="form-group row">
                    <div class="col-sm-2"></div>
                      <div class="form-group row {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                      {!!Form::label('fecha_hasta', 'Fecha Hasta', array('class' => 'col-sm-6 col-form-label'))!!}
                      <div class="col-sm-6">
                        <div class="input-group date">
                          <input type="text" class="form-control group-date" id="fecha_hasta" name="fecha_hasta" value="{!! date('Y-m-d', strtotime($editFechasPeriodos->fecha_hasta))  !!}">
                          <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! Form::submit('Guardar', ['class' => 'btn btn-success btn-sm','title'=>'Guardar']) !!}
              {!! link_to_route('periodosfechas', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) !!}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{!! Form::close() !!}
@stop
@section('after-scripts-end')

@endsection