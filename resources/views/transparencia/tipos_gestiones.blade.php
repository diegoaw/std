@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-file-text"></i> Transparencia - Tipos Gestiones</h1><br>
    </div>

  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title">Nuevo Tipo Gestiones</h5>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">


                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="text-center">
                      {!! link_to_route('transparencia_crear_tipo', 'Nuevo Tipo', [], ['class' => 'btn btn-primary btn-sm','title'=>'Nuevo Tipo']) !!}
                      </div>
                    </div>
                  </div>



          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@if(count($dataDefinitiva)>0)
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">Transparencia - Tipos Gestiones</h5>

          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th nowrap style="color:white">ID</th>
                        <th nowrap style="color:white">Nombre</th>
                        <th nowrap style="color:white"><center>Detalles</center></th>
                        <th nowrap style="color:white"><center>Editar</center></th>
                        <th nowrap style="color:white"><center>Asignación Usuario</center></th>
                        <th nowrap style="color:white"><center>Eliminar</center></th>
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($dataDefinitiva)>0)
                      @foreach ($dataDefinitiva as $indice => $tramite)
                          <tr>
                           <td nowrap>{{$tramite->id}}</td>
                           <td nowrap>{{$tramite->nombre}}</td>
                           <td nowrap><center><a class="btn accion btn-info accion" data-toggle="collapse" {!! "data-target='#demo".$indice."'" !!}   {!!"data-parent='#myTable".$tramite->id."'" !!}><i class="fa fa-eye"></i></a></center></td>
                           <td nowrap><center><a class="btn accion btn-success accion" href="{{route('transparencia_editar_tipo',$tramite->id)}}"><i class="fa fa-pencil"></i></a></center></td>
                           <td nowrap><center><a class="btn accion btn-warning accion" href="{{route('transparencia_tipo_usuario',$tramite->id)}}"><i class="fa fa-user"></i></a></center></td>
                           <td nowrap><center><a data-toggle="tooltip" class="btn btn-sm btn-danger" href="#" OnClick="Eliminar('{{$tramite->id}}')"><i class="fa fa-trash"></i></a></center></td>
                          </tr>

                      <tr  {!! "id='demo".$indice."'"  !!} class="collapse">
                        <td colspan="10" class="hiddenRow">
                          <table class="table table-striped table-sm" {!!  "id='myTable".$tramite->id."'" !!}  >
                            <thead class="thead-primary">
                              <tr style="background-color: #fddadf;">
                              <tr style="background-color: #cd0b27;">
                              <th style="color:white">Detalle Tipos Gestiones</th>
                              <th style="color:white">Usuarios Asignados</th>
                              </tr>

                              </tr>
                            </thead>
                            <tbody>                            
                            @foreach ($tramite->DetallesTipos as $indice => $tramites)
                            <tr>
                            <tr>
                            <td >{{$tramites->nombre}}</td>
                            <td >
                            @if($tramites->usuarios()->count()>0)
                              @foreach ($tramites->usuarios() as $indice3 => $usuario)
                              {{$usuario->name}},
                              @endforeach
                            @endif
                            </td>
                            </tr>
                            @endforeach                     
                           </tbody>
                        </table>
                      </td>
                    </tr>

                      @endforeach
                @endif
                </tbody>
              </table>
          </div>
@endif
@endsection
@section('before-scripts-end')
@include('includes.scripts.transparencia_tipos')
@stop
