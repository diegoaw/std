@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-user"></i> Transparencia - Tipos Gestiones Asignación Usuario - {{$asignacionTipo->nombre}}</h1><br> 
    </div>    
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
@foreach ($DetalleTipo as $indiced => $detalleTipo)
  {!! Form::open(['route' => 'transparencia_tipo_usuario_store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','id'=>'form-create']) !!}
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header card-danger card-outline">
          <h3 class="card-title">Asignación Usuario {{$detalleTipo->nombre}} </h3>
        </div>
        <div class="card-body">

          <div class="form-group row">
            <div class="col-sm-6 offset-sm-3">
              @include('includes.messages')
            </div>
          </div>

          <div class="form-group row">
          <div class="col-sm-6">
            {!! Form::hidden('detalle_tipo_id', $detalleTipo->id,  ['class' => 'form-control', 'placeholder' => 'detalle gestion', 'readonly' => 'readonly', 'id'=>'detalle_tipo_id','name'=>'detalle_tipo_id']) !!}
            {!! Form::hidden('gestion_id', $asignacionTipo->id,  ['class' => 'form-control', 'placeholder' => 'detalle gestion', 'readonly' => 'readonly', 'id'=>'gestion_id','name'=>'gestion_id']) !!}
          </div>
         </div>
        @if($detalleTipo->usuarios()->count()>0)
         <b>Usuarios ya Asignados a esta Tarea:</b> 
         @foreach ($detalleTipo->usuarios() as $indice3 => $usuario)
            <li>{{$usuario->name}}  <td nowrap><a data-toggle="tooltip" class="btn btn-sm btn-danger" href="#" OnClick="Eliminar('{{$usuario->id}}', '{{$detalleTipo->id}}', '{{$asignacionTipo->id}}')"><i class="fa fa-trash"></i></a></td></li>
         @endforeach
        @endif

         <div class="form-group row">
          {!! Form::label('nombre_detalle', 'Detalle de Gestión', ['class' => 'col-sm-3 col-form-label']) !!}
          <div class="col-sm-6">
            {!! Form::text('nombre_detalle', $detalleTipo->nombre,  ['class' => 'form-control', 'placeholder' => 'Detalle de Gestión', 'readonly' => 'readonly', 'id'=>'nombre_detalle','name'=>'nombre_detalle']) !!}
          </div>
         </div>

          <div class="form-group row">
            {!! Form::label('Seleccione Usuario', 'Seleccione Usuario(*)', ['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-6">
              {!! Form::select('usuario_id',$selectUsuariosid , null ,  ['id'=>'usuario_id','name'=>'usuario_id', 'class'=>'form-control']) !!}
            </div>
          </div>
          
          <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! Form::submit('Guardar', ['class' => 'btn btn-success btn-sm','title'=>'Guardar']) !!}
            </div>
          </div>
        </div>

        </div>
      </div>
    </div>
  </div>
  {!! Form::close() !!}
@endforeach

<div class="form-group">
    <div class="col-sm-12">
        <div class="text-center">
        {!! link_to_route('transparencia_tipos', 'Volver', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
        </div>
    </div>
</div>


@stop
@section('after-scripts-end')
@include('includes.scripts.transparencia_usuarios')
@endsection