@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-5">
      <h1><i class="fa fa-car"></i> Solicitud de Traslados</h1>
    </div>
    <div class="col-sm-7">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-car"></i> Solicitud de Traslado</li>
        <li class="breadcrumb-item active">Detalles</li>
      </ol>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <!-- Line chart -->
    <div class="card card-danger card-outline">
      <div class="card-header">
        <h3 class="card-title">Detalles Solicitud de Traslado</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <br>
        <h4><b>Trámite N° {{$data->id}}</b></h4>
        <br>
        <br>
        <h4><b>Datos de la Solicitud</b></h4>
        <br>
        <table id="example1" class="table table-striped">
          <tr>
              <td style="width:450px"><b>Declara haber leído y aceptado los términos y condiciones informados.</b></td>
              <td >{{$data->declaro}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>1.- Motivo de Traslado:</b></td>
              <td >{{$data->motivo_traslado}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>2.- Documento que Respalda la Necesidad del Traslado:</b></td>
              <td >
              @if($data->adjunta_respaldo)
                <a href="{{$data->adjunta_respaldo}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
              </td>
          </tr> 
		      <tr>
              <td style="width:450px"><b>3.- Centro de Costo:</b></td>
              <td >{{$data->centro_costos}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>4.- Nombre y Cargo de Quien Autoriza su Solicitud:</b></td>
              <td>{{$data->destinatario}} {{$data->api_area}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>5.- Cantidad de Pasajero/s:</b></td>
              <td>{{$data->cantidad_pasajeros}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>6.- Fecha del Traslado:</b></td>
              <td>{{$data->fecha_traslado}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>7.- Nombre del Pasajero/a (Contacto):</b></td>
              <td>{{$data->nombre_pasajero}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>8.- Número de Teléfono (Contacto):</b></td>
              <td>{{$data->numero_telefono}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>9.- Lugar de Salida (Dirección):</b></td>
              <td>{{$data->lugar_salida}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>10.- Hora de Salida (Formato 24Hrs):</b></td>
              <td>{{$data->hora_salida}}</td>
          </tr>
		      <tr>
              <td style="width:450px"><b>11.- Lugar de Destino (Dirección):</b></td>
              <td>{{$data->lugar_destino}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>12.- ¿Requiere Servicio de Regreso?:</b></td>
              <td>{{$data->servicio_regreso}}  @if($data->indique_hora_regreso) {{$data->indique_hora_regreso}} @endif</td>
          </tr>
          <tr>
              <td style="width:450px"><b>13.- Para Viajes Desde y Hacia el Aeropuerto Itinerario de Vuelo del Pasajero:</b></td>
              <td >
              @if($data->itinerario_vuelo)
                <a href="{{$data->itinerario_vuelo}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
              </td>
          </tr> 
          <tr>
              <td style="width:450px"><b>14.- Observaciones (Consideraciones sobre el traslado):</b></td>
              <td>{{$data->observaciones}}</td>
          </tr>
          <tr>
              <td style="width:450px"><b>Descargar Comprobante de Solicitud de Traslado en Simple:</b></td>
              <td >
              @if($data->comprobante_descarga)
              <a href="{{$data->comprobante_descarga}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button></a>
              @endif
              </td>
          </tr>
		  </table>
      <br>
      <h4><b>Aprobación de Jefatura</b></h4>
      <br>
      <table id="example1" class="table table-striped">
        <tr>
            <td style="width:450px"><b>¿Aprueba Solicitud de Traslado?</b></td>
            <td >{{$data->prueba_solicitud}}</td>
        </tr>
        <tr>
            <td style="width:450px"><b>Observaciones:</b></td>
            <td >{{$data->observaciones_j}}</td>
        </tr>
      </table>
      <br>
      <h4><b>Aprobación de Servicios Generales</b></h4>
      <br>
      <table id="example1" class="table table-striped">
        <tr>
            <td style="width:450px"><b>¿Aprueba Solicitud de Traslado?</b></td>
            <td >{{$data->aprueba_solicitud_servicios}}</td>
        </tr>
        <tr>
            <td style="width:450px"><b>Observaciones:</b></td>
            <td >{{$data->observaciones_s}}</td>
        </tr>
      </table>
      </div>

        <div class="form-group">
          <div class="col-sm-12">
            <div class="text-center">
              {!! link_to_route('traslados', 'Volver', [], ['class' => 'btn btn-danger btn-sm','title'=>'Volver']) !!}
            </div>
          </div>
        </div>
    </div>
  </div>
</div>


@endsection
@section('before-scripts-end')
@stop
