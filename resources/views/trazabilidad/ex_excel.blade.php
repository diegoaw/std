<table>
  <tr>
   <th colspan="10" style="text-align: center;">MINISTERIO DEL DEPORTE</th>
  </tr>
  <tr>
   <th colspan="10" style="text-align: center;">Sistema de Trazabilidad Documental</th>
  </tr>
  <tr>
   <th colspan="10" style="text-align: center;">Trazabilidad de Derivaciones</th>
  </tr>
   <tr>
   <th colspan="10" style="text-align: center;">{{Date::now()->format('d-m-Y')  }} </th>
  </tr>
</table >

	<table>
        <tr style="background-color: #fddadf;">
          <th nowrap>N° Trámite</th>
          <th nowrap>Fecha Derivación</th>
          <th nowrap>Derivación</th>
          <th nowrap>Remitente</th>
          <th nowrap>Departamento Remitente</th>
          <th nowrap>Destinatario</th>
          <th nowrap>Departamento Destinatario</th>
          <th nowrap>Prioridad</th>
          <th nowrap>Observación</th>
          <th nowrap>Documento Cargado</th> 
        </tr>
        @foreach ($tramites as $tramite)
        @foreach ($tramite->derivaciones as $registro)
        <tr>
          <td>{{$registro->tramite_id}}</td>
          <td>{{$registro->fecha}}</td>
          <td>{{$registro->derivacion_n}}</td>
          <td>{{$registro->remitente}}</td>
          <td>{{$registro->area_remitente}}</td>
          <td>{{$registro->destinatario}}</td>
          <td>{{$registro->area_destinatario}}</td>
          <td>{{$registro->prioridad}}</td>
          <td>{{$registro->observacion}}</td>
          <td>{{$registro->documento}}</td> 
        </tr>
        @endforeach
        @endforeach       
  </table>
               