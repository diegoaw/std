
@extends('layouts.master')
@section('page-header') 
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1><i class="fa fa-tasks"></i> Consulta Derivaciones</h1><br> 
    </div>
    
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header ">
        <h5 class="card-title">Filtrar Derivaciones por </h5>
            
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">

                {!! Form::open(['route' => 'trazabilidadFiltros', 'method' => 'post' , 'class'=>"form-horizontal"]) !!}
                  

                  <div class="form-row">
                   
                    <div class="col-sm-0">
                      <label for="nRegistro" class=" col-form-label">N° Trámite: </label>
                      <input type="text" class="form-control" id="nRegistro" name="nRegistro" value="{{ $requestNRegistro }}" placeholder="N° Trámite">
                    </div>
                  

                    <div class="form-group   {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                    <div class="input-group date">
                      <div class="col"> 
                          <label for="FechaDesde" class=" col-form-label">Fecha Desde: </label>
                          <input type="text" class="form-control group-date" id="FechaDesde" name="FechaDesde" readonly="readonly" value="{!! date('Y-m-d', strtotime($feDesde))  !!}">   
                      </div>
                    </div>
                    </div>

                    <div class="form-group   {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                    <div class="input-group date">
                      <div class="col">
                          <label for="FechaHasta" class=" col-form-label">Fecha Hasta: </label>
                          <input type="text" class="form-control group-date" id="FechaHasta" name="FechaHasta" readonly="readonly" value="{!! date('Y-m-d', strtotime($feHasta ))  !!}">
                      </div>
                    </div>
                    </div>

                    <div class="form-check">
                      <label for="contenido" class=" col-form-label">Origen: </label>
                    </div>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="origen" value='' onchange="getValueRadio()" @if($origenView == null) checked @endif>
                      <label class="form-check-label">Todos</label>
                    </div>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="origen" value="interno" onchange="getValueRadio()" @if($origenView == 'interno') checked @endif>
                      <label class="form-check-label">Interno</label>
                    </div>
                    <div class="form-check form-check-inline">                         
                      <input class="form-check-input" type="radio" name="origen" value="externo" onchange="getValueRadio()" @if($origenView == 'externo') checked @endif>
                      <label class="form-check-label">Externo</label>
                    </div>

                    <div class="col-sm-2">
                      <label for="contenido" class=" col-form-label">Contenido o Materia:</label>
                      <textarea class="form-control rounded-0" id="contenido" name="contenido"  rows="3">{{ $contenidoView }} </textarea>
                    </div>
  
                    <div class="col-sm-2">
                      <label for="observacion" class=" col-form-label">Observación:</label>
                      <textarea class="form-control rounded-0" id="observacion" name="observacion"  rows="3">{{ $observacionView }} </textarea>
                    </div>

                      
                  </div>

                  <div class="form-row">
                    
                    <div class="col-sm-3">
                      <label for="areaRemtiente" class=" col-form-label">Área Remitente: </label>
                      {!! Form::select('areaRemtiente',$selectA,$areRemitenteView,['id'=>'areaRemtiente','name'=>'areaRemtiente', 'class'=>'form-control' ,'onchange'=>"getValueSelect()"]) !!}
                    </div>

                    <div class="col-sm-3">
                      <label for="areaDesti" class=" col-form-label">Área Destinatario: </label>
                      {!! Form::select('areaDesti',$selectA,$areDestitenteView,['id'=>'areaDesti','name'=>'areaDesti', 'class'=>'form-control' , 'onchange'=>"getValueSelec2()"]) !!}
                    </div>

                    <div class="col-sm-3">
                      <label for="procedencia" class=" col-form-label">Procedencia Externa: </label>
                      <input type="text" class="form-control" id="procedencia" name="procedencia" value="{{ $procedenciaView }}" placeholder="Procedencia">
                    </div>

                    <div class="col-sm-2">
                      <label for="tiposDocumentos" class=" col-form-label">Tipo Documento: </label>
                      {!! Form::select('tiposDocumentos',$tiposDocumentos,$tiposDocumentosView,['id'=>'tiposDocumentos','name'=>'tiposDocumentos', 'class'=>'form-control','onchange'=>"getValueSelect3()"]) !!}
                    </div>
                    

                  </div>   
                  <button type="submit" class="btn btn-primary pull-right">Buscar</button>
        
                {!! Form::close() !!}   
              
          </div>  
        </div> 
      </div>  
    </div>
  </div>
</div>



@if(count($tramites)>0)
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">Listado de Trámites y sus Derivaciones</h5>
            
          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table table-striped table-sm"  >
                <thead >
                    <tr style="background-color: #cd0b27;">
                        <th nowrap style="color:white">N° Trámite</th>
                        <th nowrap style="color:white">Fecha de Ingreso</th>
                        <th nowrap style="color:white">N° Trámite Relacionado</th>
                        <th nowrap style="color:white">Origen</th>
                        <th nowrap style="color:white">Remitente</th>
                        <th nowrap style="color:white">Cargo</th>
                        <th nowrap style="color:white">Área y Subarea Remitente</th>
                        <th nowrap style="color:white">Tipo de Documento</th>
                        <th nowrap style="color:white">Contenido</th>
                        <th nowrap style="color:white">Ver Derivaciones</th>
                    </tr>
                </thead>
                <tbody class="panel">
                @if(count($tramites)>0)
                         @foreach ($tramites as $indice => $tramite)
                          <tr>
                           <td nowrap>{{$tramite->tramite_id}}</td>
                           <td nowrap>{{$tramite->created_at}}</td>
                           <td nowrap>{{$tramite->tramite_relacion_id}}</td>
                           <td nowrap>{{$tramite->origen}}</td>
                           <td >{{$tramite->remitente}}</td>
                           <td >{{$tramite->cargo}}</td>
                           <td>{{$tramite->area_remitente}}</td>
                           <td nowrap>{{$tramite->tipo_documento}}</td>
                           <td>{{$tramite->contenido}}</td> 
                           <th><center> <button type="button" class="btn btn-primary" data-toggle="collapse" {!! "data-target='#demo".$indice."'" !!}   {!!"data-parent='#myTable".$tramite->tramite_id."'" !!} 
                           ><i class="fa fa-eye"></i></button></center></th>
                          </tr>
                        <tr  {!! "id='demo".$indice."'"  !!} class="collapse">
                        <td colspan="10" class="hiddenRow">
                          <table class="table table-striped table-sm" {!!  "id='myTable".$tramite->tramite_id."'" !!}  >
                            <thead class="thead-primary">
                              <tr style="background-color: #fddadf;">
                              <th nowrap>N° Trámite</th>
                              <th nowrap="">Fecha Derivación</th>
                              <th nowrap>Derivación</th>
                              <th nowrap>Remitente</th>
                              <th nowrap>Departamento Remitente</th>
                              <th nowrap>Destinatario</th>
                              <th nowrap>Departamento Destinatario</th>
                              <th nowrap>Prioridad</th>
                              <th nowrap>Observación</th>
                              <th nowrap>N° Folio</th>
                              <th nowrap>Documento Cargado</th>

                              </tr>
                            </thead>
                          <tbody>
                             @foreach ($tramite->derivaciones as $indice1 => $registro)
                            <tr>
                              <td>{{$registro->tramite_id}}</td>
                              <td>{{$registro->fecha}}</td>
                              <td>{{$registro->derivacion_n}}</td>
                              <td>{{$registro->remitente}}</td>
                              <td>{{$registro->area_remitente}}</td>
                              <td>{{$registro->destinatario}}</td>
                              <td>{{$registro->area_destinatario}}</td>
                              <td>{{$registro->prioridad}}</td>
                              <td>{{$registro->observacion}}</td>
                              <td>@if($registro->correlativo){{$registro->correlativo}}@endif</td>
                              <td>@if($registro->documento)<center> <a href="{{$registro->documento}}" target="_blank" download> <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button>
                           </a></center>@endif</td> 
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </td>
                    </tr>
                          @endforeach
                        @endif
                </tbody>
            </table>
          <br>
        </div>
        <div class="card-footer">
            <div class="card-tools text-center">
              {!! Form::open(['route' => 'trazabilidadReporte', 'method' => 'post' , 'class'=>"form-horizontal"]) !!}
              <input type="hidden" class="form-control" id="nRegistro" name="nRegistro" value="{{ $requestNRegistro }}" placeholder="N° Trámite">
              <input type="hidden" class="form-control group-date" id="FechaDesde" name="FechaDesde" readonly="readonly" value="{!! date('Y-m-d', strtotime($feDesde))  !!}">
              <input type="hidden" class="form-control group-date" id="FechaHasta" name="FechaHasta" readonly="readonly" value="{!! date('Y-m-d', strtotime($feHasta ))  !!}">
              <input type="hidden" class="form-control" id="origen1" name="origen1">
              <input type="hidden" class="form-control" id="areaRemtiente1" name="areaRemtiente1">
              <input type="hidden" class="form-control" id="areaDesti1" name="areaDesti1">
              <input type="hidden" class="form-control" id="tiposDocumentos1" name="tiposDocumentos1">
              <textarea style="display:none;" class="form-control rounded-0" id="contenido" name="contenido"  rows="3">{{ $contenidoView }} </textarea>
              <input type="hidden" class="form-control" id="procedencia" name="procedencia" value="{{ $procedenciaView }}" placeholder="Procedencia">
              <textarea style="display:none;" class="form-control rounded-0" id="observacion" name="observacion"  rows="3">{{ $observacionView }} </textarea>
              <button type="submit" onclick="inload()" class="btn btn-primary btn-success" role="button">Exportar a excel</button>&nbsp;
              {!! Form::close() !!}
            </div>
          </div>
      </div>
    </div>
</div>
 



            <br>

  @else
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-danger card-outline">
            <h5 class="card-title">Listado de Trámites y sus Derivaciones</h5>
            
          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <center>  No se encontraron resultados. </center>
          <br>
        </div>
      
      </div>
    </div>
</div>
@endif




    


@endsection
@section('before-scripts-end')
@include('includes.scripts.script_fecha.scripts_filtro_fecha')

<script type="text/javascript">
var radios = document.getElementsByName('origen');
  for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
          document.getElementById('origen1').value = radios[i].value;
          break;
        }
      }
var select1 = document.getElementById('areaRemtiente').value;
document.getElementById('areaRemtiente1').value = select1;

var select2 = document.getElementById('areaDesti').value;
       document.getElementById('areaDesti1').value = select2;

var select3 = document.getElementById('tiposDocumentos').value;
       document.getElementById('tiposDocumentos1').value = select3;

  function getValueRadio(){
      var radios = document.getElementsByName('origen');

      for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
          document.getElementById('origen1').value = radios[i].value;
          break;
        }
      }
}

 function getValueSelect(){
      var select1 = document.getElementById('areaRemtiente').value;
       document.getElementById('areaRemtiente1').value = select1;
}
function getValueSelec2(){
      var select2 = document.getElementById('areaDesti').value;
       document.getElementById('areaDesti1').value = select2;
}

function getValueSelec3(){
      var select3 = document.getElementById('tiposDocumentos').value;
       document.getElementById('tiposDocumentos1').value = select3;
}

function inload(){

  setTimeout(function(){ document.getElementById("load").style.display="none";
    swal("No Imprima este docuemento", " Su impresion no tiene validez , si desea guardelo para su respaldo")
   }, 10000);
  
}
</script>
@stop
