@extends('layouts.out2')
@section('after-styles-end')
<style type="text/css">
  body{
    background-color: #071221 !important;
  }
  .letrasTitulo {
    color: #FFF !important;
  }
</style>
@endsection
@section('content')

<center><h3 class="letrasTitulo">Detalles de Firma y Documento</h3></center>



@if($documento->reservado ==0)
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                 <!--  <img class="profile-user-img img-fluid img-circle"
                       src="https://lh3.googleusercontent.com/proxy/CwephsXIxpl1DjTtoWYDvJcUyKE317EzeZTj_0Z22jI2t28I8dNme2hovx_UxLCEdPA-crJXP1RoeW7zc18_6ZLHp8ALWMp86reBP98FJypctRieJjMTBIKDirP9NnsB7A"
                       alt="User profile picture"> -->
                </div>
                <h3 class="profile-username text-center"></h3>
                <p class="text-muted text-center">{{$documento->nombre}}</p>
                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Tipo Documento</b> <a class="float-right">{{$documento->tipoDocumento->nombre}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Folio</b> <a class="float-right">{{$documento->folio_documento}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Reservado</b> <a class="float-right">{{$documento->ReservadoC}}</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Documento</a></li>
                  <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Información del Documento</a></li>
                  <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Firma</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                    <embed src="{{ asset('firmados/'.$documento->url_documento_firmado) }}" type="application/pdf" width="100%" height="800px" />
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="timeline">
                    <!-- The timeline -->
                    <div class="timeline timeline-inverse">
                    

                       <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Nombre del Documento</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" placeholder="" value="{{$documento->nombre}}" disabled="">
                        </div>
                      </div> 

                        <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Descripción</label>
                        <div class="col-sm-10">
                         <textarea class="form-control" rows="5" placeholder="{{$documento->descripcion}}" value="" disabled=""></textarea>
                        </div>
                      </div> 

                       <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Folio</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" placeholder="" value="{{$documento->folio_documento}}" disabled="">
                        </div>
                      </div> 

                       <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Tipo Documento</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" placeholder="" value="{{$documento->tipoDocumento->nombre}}" disabled="">
                        </div>
                      </div> 
                     
                       <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Reservado</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" placeholder="" value="{{$documento->ReservadoC}}" disabled="">
                        </div>
                      </div>
                    
                  </div>
                  </div>
                  <div class="tab-pane" id="settings">
                    <table>
                      <thead>
                        <tr>
                          <th>Nombre<th>
                        <tr>
                      </thead>
                      <tbody>
                        <tr>
                          @foreach ($firmantes as $indice => $registro)
                          <td>
                           {{$registro->nombre_firmante}} 
                          <td>
                          @endforeach
                        <tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
@else
<section class="content">
      <div class="container-fluid">
        <div class="row">
        <div class="card-body" style="background-color: #FFF">
                <center><h5 class="card-title">Validacion firma Digital Correcta </h5></center>

                <center><h5 class="card-title">
                  Documento Firmado por la Subsecretaria del Deporte Documento Reservado
                </h5></center> 
              </div>
        </div>
      </div>
    </section>
@endif

  </div>
</body>
</html>

@endsection