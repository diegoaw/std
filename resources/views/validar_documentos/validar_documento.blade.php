@extends('layouts.out')
<style type="text/css">
   body { background-color: #FFF !important; }
   login-box { background-color: #FFF !important; }
</style>
@section('content')
<div class="login-box">
  <div id="card">
    <div class="card-body login-card-body">
      <div class="col-sm-12 text-center">
      </div>
    <br>
    
      <div class="col-sm-12">
        @include('includes.messages')
      </div>
      <div class="login-box-body bg-dark">
        
        <div class="logos-container">
          <center><img class="ft-logo-dgd" src="{{ asset('assets/mindep.png') }}" alt="Logo MINDEP" width="190" height="190"></center><br>
        </div>
        <h3 class="login-box-msg">Validacion de Firmas de Documentos</h3>
        <h5 class="login-box-msg">Validar Firma</h5>
        {!! Form::open(['route' => 'validarPost', 'method' => 'post']) !!}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="input-group input-group-login mb-3">
          <input type="text" class="form-control" placeholder="ID" name="id" id="id" autocomplete="off">
          
        </div>
        <div class="input-group input-group-login mb-3">
          <input type="text" class="form-control" placeholder="CODIGO" name="hash" id="hash" placeholder="">
         
        </div>
        <div class="input-group input-group-login mb-3">
            <button type="submit" class="btn btn-danger btn-block btn-flat">Validar</button>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@endsection
@section('after-scripts-end')
<script type="text/javascript">

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&amp;amp;]" + name + "=([^&amp;amp;#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

//var valor = getParameterByName('prodId');
//alert(valor);

</script>
@endsection