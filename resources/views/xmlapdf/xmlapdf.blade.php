{{-- @Nombre del programa: Vista Principal--}}
@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1> Visualizar xml </h1><br>
    </div>
    <div class="col-sm-1">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item active"><i class="fa fa fa-home"></i> Visualizar xml</li>
      </ol>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-danger card-outline">
      <div class="card-header ">
        <h5 class="card-title"> Cargar archivo xml </h5>
            
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="box-body">
          <div class="col-sm-12">

                {!! Form::open(['route' => 'converHtml', 'method' => 'post' , 'class'=>"form-horizontal" , 'files' => true]) !!}
                  

                <div class="form-row">
                  <div class="form-group row col-md-12">
                      {!! Form::label('documento', 'Documento(*)', ['class' => 'col-lg-3 col-form-label']) !!}
                    <div class="col-md-6">
                      {!! Form::file('documento', ['class' => 'form-control', 'required' => 'required' , 'id'=>'documento' , 'name'=>'documento']) !!}
                    </div>
                  </div>
                </div>
 
                  <button type="submit" class="btn btn-primary pull-right">Generar vista</button>
        
                {!! Form::close() !!}   
              
          </div>  
        </div> 
      </div>  
    </div>
  </div>
</div>
@endsection
@section('before-scripts-end')

@stop