{{-- @Nombre del programa: Vista Principal--}}
@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-11">
      <h1>formato factura Generico</h1><br>
    </div>
    <div class="col-sm-1">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item active"><i class="fa fa fa-home"></i> formato factura Generico</li>
      </ol>
    </div>
  </div>
</div>
@endsection
@section('after-styles-end')
<style type="text/css">
   .padindP0{
    padding-left: 50px;
    padding-right: 50px;
  }
  .padindP{
    padding: 35px !important ;
  }
  .padindP2{
    padding: 35px !important ;
  }
  .recuadro {
    border: 2px solid #FF0000; 
    font-family: Courier New,serif;
    font-size: 14.4px;
    color: rgb(255,0,0);
    font-weight: bold;
    font-style: normal;
    text-decoration: none;
    text-align: center;
  }
  .tableP{
    border: 0.5px solid #000 !important ; 
  }
</style>
@endsection
@section('content')

<div class="row">
      <div class="col-md-12 padindP0">
        <div class="card padindP">
         <div class="row padindP2"> 
          <div class="col-md-6" >
          <h1>{{$dataEmisor['RznSoc']}}</h1>
          <h6>{{$dataEmisor['GiroEmis']}}</h6>
          <h6>{{$dataEmisor['DirOrigen']}} - {{$dataEmisor['CmnaOrigen']}}- @if(array_key_exists('CiudadOrigen' ,$dataEmisor)){{$dataEmisor['CiudadOrigen']}}@endif </h6>
          <h5>Fecha de Emisión : @if(array_key_exists('FchEmis', $dataDocumento)) {{$dataDocumento['FchEmis']}} @endif - Fecha de Vencimiento :  @if(array_key_exists('FchVenc', $dataDocumento)) {{$dataDocumento['FchVenc']}}@endif</h5>
        </div>
        <div class="col-md-4 recuadro">
          <div> R.U.T  {{$dataEmisor['RUTEmisor']}} </div>
          <div> FACTURA ELECTRONICA (NO TIENE VALIDEZ) </div>
          <div> N°  de FACTURA O FOLIO {{$dataDocumento['Folio']}}  </div>
        </div>
      </div>
       <div class="row "> 
          <div class="col-md-12" >
          <div class="card-header card-primary card-outline" >
            <h5 class="card-title">Datos del emisor </h5>
          </div>
          <div class="card-body table-responsive p-0">
            <table class="table  table-sm">
              <tr>
                <td>Razon social</td>
                <td>{{$dataEmisor['RznSoc']}}</td>
              </tr>
              <tr>
                <td >rut emisor</td>
                <td >{{$dataEmisor['RUTEmisor']}}</td>
              </tr>
               <tr>
                <td >Giro emisor</td>
                <td >{{$dataEmisor['GiroEmis']}}</td>
              </tr>
                <tr>
                <td >Direccion emisor</td>
                <td >{{$dataEmisor['DirOrigen']}} - {{$dataEmisor['CmnaOrigen']}} - @if(array_key_exists('CiudadOrigen' , $dataEmisor)){{$dataEmisor['CiudadOrigen']}} @endif</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
       <div class="row "> 
          <div class="col-md-6" >
           <div class="card-header card-primary card-outline" >
            <h5 class="card-title">Datos del receptor </h5>
          </div>
           <div class="card-body table-responsive p-0">
            <table class="table  table-sm ">
              <tr>
                <td>Razon social</td>
                <td>{{$dataReceptor['RznSocRecep']}}</td>
              </tr>
              <tr>
                <td>rut receptor</td>
                <td>{{$dataReceptor['RUTRecep']}}</td>
              </tr>
               <tr>
                <td>Giro receptor</td>
                <td>{{$dataReceptor['GiroRecep']}}</td>
              </tr>
                <tr>
                <td>Direccion receptor</td>
                <td>{{$dataReceptor['DirRecep']}} - {{$dataReceptor['CmnaRecep']}} - {{$dataReceptor['CiudadRecep']}}</td>
              </tr>
            </table>
          </div>
        </div>

        <div class="col-md-6" >
           <div class="card-header card-primary card-outline" >
            <h5 class="card-title"> Pago </h5>
          </div>
           <div class="card-body table-responsive p-0">
            <table class="table  table-sm ">
              <tr>
                <td>Forma de pago</td>
                <td>@if (array_key_exists('FmaPago' ,$dataDocumento)) {{$dataDocumento['FmaPago']}} @endif</td>
              </tr>
              <tr>
                <td>Condiciones de venta </td>
                <td> @if(array_key_exists('TermPagoGlosa', $dataDocumento)) {{$dataDocumento['TermPagoGlosa']}} @endif</td>
              </tr>
                <tr>
                <td>Vencimiento</td>
                <td> @if(array_key_exists('FchVenc', $dataDocumento)) {{$dataDocumento['FchVenc']}}@endif</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
        <div class="row "> 
                <div class="col-md-12" >
                     <div class="card-header card-primary card-outline" >
                     <h5 class="card-title"> Detalle </h5>
                    </div>

                <div class="card-body table-responsive p-0">
                    <table class="table table-striped table-sm"  >
                      <thead >
                          <tr >
                              <th nowrap style="color:black">Código</th>
                              <th nowrap style="color:black">Cantidad</th>
                              <th nowrap style="color:black">Descripcion</th>
                              <th nowrap style="color:black">Precio Unitario</th>
                              <th nowrap style="color:black">Descuento</th>
                              <th nowrap style="color:black">Total</th>
                          </tr>
                      </thead>
                      <tbody class="panel">
                     @if(array_key_exists('NmbItem', $detalleProducto))
                     <tr >
                              <td nowrap >@if(array_key_exists('CdgItem',$detalleProducto))@if(array_key_exists('TpoCodigo',$detalleProducto['CdgItem'])){{$detalleProducto['CdgItem']['TpoCodigo']}}@endif @endif- @if(array_key_exists('CdgItem',$detalleProducto)) @if(array_key_exists('VlrCodigo',$detalleProducto['CdgItem'])) {{$detalleProducto['CdgItem']['VlrCodigo']}} @endif @endif</td>
                              <td nowrap >@if(array_key_exists('QtyItem' , $detalleProducto)){{$detalleProducto['QtyItem']}} @endif</td>
                              <td nowrap >{{$detalleProducto['NmbItem']}} @if(array_key_exists('DscItem' ,$detalleProducto )){{$detalleProducto['DscItem']}}@endif </td>
                              <td nowrap >{{$detalleProducto['PrcItem']}}</td>
                              <td nowrap ></td>
                              <td nowrap >{{$detalleProducto['MontoItem']}}</td>
                      </tr>
                     @else
                      @foreach ($detalleProducto as $indice => $registro)
                      @if($registro)
                      <tr>
                              <td nowrap > @if(array_key_exists('CdgItem', $registro))   @if(array_key_exists('TpoCodigo',$registro['CdgItem']))  {{$registro['CdgItem']['TpoCodigo']}} @endif @endif - @if(array_key_exists('CdgItem', $registro)) {{$registro['CdgItem']['VlrCodigo']}}@endif</td>
                              <td nowrap >@if(array_key_exists('QtyItem' , $registro)){{$registro['QtyItem']}}@endif</td>
                              <td nowrap >{{$registro['NmbItem']}} @if(array_key_exists('DscItem' ,$registro )){{$registro['DscItem']}} @endif</td>
                              <td nowrap >@if(array_key_exists('PrcItem',$registro)){{$registro['PrcItem']}}@endif</td>
                              <td nowrap ></td>
                              <td nowrap >@if($registro['MontoItem']>0){{$registro['MontoItem']}}@endif</td>
                      </tr>
                      @endif
                      @endforeach
                     @endif
                      </tbody>
                  </table>
                <br>
              </div>
          </div>
      </div>
       <div class="row "> 
          <div class="col-md-6" >
           <div class="card-header card-primary card-outline" >
            <h5 class="card-title">Otros Datos </h5>
          </div>
           <div class="card-body table-responsive p-0">
            <table class="table  table-sm ">
              @if($referencia)
              <tr>
                <td>Orden de Compra </td>
                <td>@if(array_key_exists('FolioRef' ,$referencia)){{$referencia['FolioRef']}} @endif</td>
                <td>Fecha de referencia</td>
                <td>@if(array_key_exists('FchRef' ,$referencia)){{$referencia['FchRef']}}@endif</td>
              </tr>
             @endif
            </table>
          </div>
        </div>

        <div class="col-md-6" >
           <div class="card-header card-primary card-outline" >
            <h5 class="card-title"> Total  </h5>
          </div>
           <div class="card-body table-responsive p-0">
            <table class="table  table-sm ">
              <tr>
                <td>Neto</td>
                <td>@if (array_key_exists('MntNeto' ,$totales )){{$totales['MntNeto']}}@endif</td>
              </tr>
              <tr>
                <td>Exento </td>
                <td> </td>
              </tr>
                <tr>
                <td>Dcto</td>
                <td> </td>
              </tr>
                 <tr>
                <td> IVA @if (array_key_exists('TasaIVA' ,$totales )) {{$totales['TasaIVA']}} @endif % </td>
                <td> @if (array_key_exists('IVA' ,$totales )) {{$totales['IVA']}} @endif</td>
              </tr>
                 <tr>
                <td>TOTAL</td>
                <td>@if (array_key_exists('MntTotal' ,$totales )) {{$totales['MntTotal']}}  @endif </td>
              </tr>
            </table>
          </div>
        </div>
      </div>

      </div>
    </div>
</div> 
@endsection
@section('before-scripts-end')

@stop