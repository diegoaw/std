<?php

/*Route::group(['middleware' => 'web'], function() {
  Route::group(['namespace' => 'Frontend'], function () {
    require (__DIR__ . '/Routes/Frontend/Access.php');
  });
});*/

require (__DIR__ . '/web/validador_documento/validador_documento.php');
require (__DIR__ . '/web/texto_doc_electronicos/texto_doc_electronicos.php');
require (__DIR__ . '/web/colaboracion/colaboracion.php');




Auth::routes();


Route::get('/', function () {
    return view('auth.login');
})->middleware('guest');


Route::group(['middleware' => ['admin']], function() {

//Route::any('/claveunica/autenticar', 'Auth\LoginController@redirectToProvider')->name('login.claveunica');
//Route::any('/claveunica/validar', 'Auth\LoginController@handleProviderCallback')->name('login.claveunica.callback');

  /* INICIO */
  require (__DIR__ . '/web/inicio/inicio.php');
  /* FIN INICIO */

  /* OPA */
  require (__DIR__ . '/web/opa/opa.php');
  /* FIN OPA */

  /* buscador global */
  require (__DIR__ . '/web/buscador_global/buscador_global.php');
  /* FIN buscador global */

  /* HISTORICO GD */
  require (__DIR__ . '/web/historico_gd/historico_gd.php');
  /* FIN HISTORICO GD  */

  /* LEY */
  require (__DIR__ . '/web/ley/ley.php');
  /* FIN LEY */

  /* SEGURIDAD */
    require (__DIR__ . '/web/seguridad/seguridad.php');
  /* FIN SEGURIDAD */

    /* TRAZABILIDAD (DERIVACION TRAMITES) */
    require (__DIR__ . '/web/trazabilidad/trazabilidad.php');
  /* FIN TRAZABILIDAD */

    /* TRAMITES (DERIVACION TRAMITES) */
    require (__DIR__ . '/web/tramites/tramites.php');
  /* FIN TRAZABILIDAD */

    /* TRAMITES (XML) */
    require (__DIR__ . '/web/xmlapdf/xmlapdf.php');
    /* TRAMITES (XML) */

    /* TRAMITES (COMPRAS) */
    require (__DIR__ . '/web/requerimientos_compras/requerimientos_compras.php');
    /* TRAMITES (COMPRAS) */

    /* TRAMITES (TRASLADOS) */
    require (__DIR__ . '/web/traslados/traslados.php');
    /* TRAMITES (TRASLADOS) */

    /* TRAMITES (TICKETS) */
    require (__DIR__ . '/web/tickets/tickets.php');
    /* TRAMITES (TICKETS) */

  /* RECONOCIMIENTO */
  require (__DIR__ . '/web/reconocimiento/reconocimiento.php');
  /* RECONOCIMIENTO */

  /* SIAC */
  require (__DIR__ . '/web/siac/siac.php');
  /* SIAC */

  /* CEVS */
  require (__DIR__ . '/web/cevs/cevs.php');
  /* CEVS */

  /* PATROCINIO */
  require (__DIR__ . '/web/patrocinio/patrocinio.php');
  /* PATROCINIO */

  /* RECLAMOS */
  require (__DIR__ . '/web/reclamos/reclamos.php');
  /* RECLAMOS */

  /* PROTOCOLO */
  require (__DIR__ . '/web/protocolo/protocolo.php');
  /* PROTOCOLO */

   /* TRAMITES (DERIVACION TRAMITES) */
    require (__DIR__ . '/web/memos/memos.php');
  /* FIN TRAZABILIDAD */

  /* TRAMITES (DERIVACION TRAMITES) */
    require (__DIR__ . '/web/resolucionexenta/resolucionexenta.php');
  /* FIN TRAZABILIDAD */



  /* TRAMITES (DERIVACION TRAMITES) */
    require (__DIR__ . '/web/oficio/oficio.php');
  /* FIN TRAZABILIDAD */


 /* INICIO */
  require (__DIR__ . '/web/firmar_documentos/firmar_documentos.php');
  /* FIN INICIO */


  /* PERFIL DE USUARIO */
    require (__DIR__ . '/web/perfil_usuario/perfil_usuario.php');
  /* FIN PERFIL DE USUARIOS */


  /* TRAMITES (SIGC) */
    require (__DIR__ . '/web/sigc/sigc.php');
  /* FIN SIGC */


  /* TRAMITES (Transparencia) */
   require (__DIR__ . '/web/transparencia/transparencia.php');
  /* FIN Transparencia */


  /* TRAMITES (integridad) */
  require (__DIR__ . '/web/integridad/integridad.php');
  /* FIN integridad */


   /* TRAMITES (beneficios) */
   require (__DIR__ . '/web/beneficios/maternidad.php');
   require (__DIR__ . '/web/beneficios/escolaridad.php');
   /* FIN beneficios */

   /* TRAMITES (Incentivo Retiro) */
   require (__DIR__ . '/web/incentivo_retiro/incentivo_retiro.php');
   /* FIN Incentivo Retiro */

    /* DOC DIGITALES*/
    require (__DIR__ . '/web/documentos_digitales/documentos_digitales.php');
    /* FIN DOC DIGITALES */

    /* GESTIÓN DOCUMENTAL*/
    require (__DIR__ . '/web/derivacion_documental/derivacion_documental.php');
    /* FIN GESTIÓN DOCUMENTAL */

    /* VIAJES PARLAMENTARIOS*/
    require (__DIR__ . '/web/parlamentarios/parlamentarios.php');
    /* FIN VIAJES PARLAMENTARIOS */

    /* Agenda Seremis*/
    require (__DIR__ . '/web/agendaseremis/agendaseremis.php');
    /* FIN Agenda Seremis */
    

  /* ROUTE ERROR */
  Route::get('500', function() {abort(500);});
  /* FIN ROUTE ERROR */
});
