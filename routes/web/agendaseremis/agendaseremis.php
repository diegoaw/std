<?php

  //AGENDA SEREMIS
  Route::get('/AgendaSeremis', 'AgendaSeremis\AgendaSeremisController@index')->name('agendaseremis');
  Route::get('/AgendaSeremis/Crear', 'AgendaSeremis\AgendaSeremisController@create')->name('agendaseremis_crear');
  Route::post('/AgendaSeremis/Nuevo', 'AgendaSeremis\AgendaSeremisController@store')->name('agendaseremis_nuevo');
  Route::get('/AgendaSeremis/Editar/{id}', 'AgendaSeremis\AgendaSeremisController@edit')->name('agendaseremis_editar');
  Route::post('/AgendaSeremis/Update/{id}', 'AgendaSeremis\AgendaSeremisController@update')->name('agendaseremis_update');
  Route::get('/AgendaSeremis/Borrar/{id}', 'AgendaSeremis\AgendaSeremisController@destroy')->name('agendaseremis_destroy');
  //REPORTE
  Route::get('/AgendaSeremis/Reporte','AgendaSeremis\AgendaSeremisController@reporte')->name('agendaseremis_reporte');
  //SELECT COMUNAS
  Route::get('/AgendaSeremis/SelectComunas/{region}', 'AgendaSeremis\AgendaSeremisController@getComunasForRegion')->name('agendaseremis_select_comuna');
  Route::get('/AgendaSeremis/SelectComunas', 'AgendaSeremis\AgendaSeremisController@TodasComunas');
