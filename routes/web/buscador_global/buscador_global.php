<?php
Route::get('BuscadorGlobal', 'BuscadorGlobal\BuscadorGlobalController@index')->name('buscador_global');

Route::post('BuscadorGlobalEjecutar', 'BuscadorGlobal\BuscadorGlobalController@buscar')->name('buscador_global_buscar');