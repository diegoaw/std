<?php
  //Registros
  Route::get('/DerivacionDocumental/Registros', 'DerivacionDocumental\RegistroController@index')->name('registros');
  Route::get('/DerivacionDocumental/Crear/Registro', 'DerivacionDocumental\RegistroController@create')->name('registrogd_crear');
  Route::post('/DerivacionDocumental/Nuevo/Registro', 'DerivacionDocumental\RegistroController@store')->name('registrogd_nuevo');

