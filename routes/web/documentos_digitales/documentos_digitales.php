<?php
  //Registro Documentos Digitales
  Route::get('/DocumentosDigitales/Registro/Oficios', 'DocumentosDigitales\RegistroDocumentosDigitalesController@indexOficios')->name('registro_ofi');
  //Route::post('/DocumentosDigitales/Registro/Oficios/Filtros', 'DocumentosDigitales\RegistroDocumentosDigitalesController@indexOficios')->name('registro_ofi_filtros');
  Route::get('/DocumentosDigitales/Registro/ResolucionesExentas', 'DocumentosDigitales\RegistroDocumentosDigitalesController@indexResex')->name('registro_resex');
  //Route::post('/DocumentosDigitales/Registro/ResolucionesExentas/Filtros', 'DocumentosDigitales\RegistroDocumentosDigitalesController@indexResex')->name('registro_resex_filtros');
  Route::get('/DocumentosDigitales/Registro/Memorandums', 'DocumentosDigitales\RegistroDocumentosDigitalesController@indexMemos')->name('registro_memos');
  //Route::post('/DocumentosDigitales/Registro/Memorandums/Filtros', 'DocumentosDigitales\RegistroDocumentosDigitalesController@indexMemos')->name('registro_memos_filtros');
  Route::get('/DocumentosDigitales/Registro/DecretosExentos', 'DocumentosDigitales\RegistroDocumentosDigitalesController@indexDecretosExentos')->name('registro_decretosexentos');
  Route::get('/DocumentosDigitales/Registro/DecretosSupremos', 'DocumentosDigitales\RegistroDocumentosDigitalesController@indexDecretosSupremos')->name('registro_decretossupremos');
  Route::get('/DocumentosDigitales/Registro/Decretos', 'DocumentosDigitales\RegistroDocumentosDigitalesController@indexDecretos')->name('registro_decretos');
  Route::get('/DocumentosDigitales/Registro/Logs', 'DocumentosDigitales\RegistroDocumentosDigitalesController@indexlog')->name('registro_log');
  Route::get('/DocumentosDigitales/RegistroDetalleDerivaciones/Oficios/{id}', 'DocumentosDigitales\RegistroDocumentosDigitalesController@detallederivacionesofi')->name('registro_detalles_derivaciones_oficios');
  Route::get('/DocumentosDigitales/RegistroDetalleDerivaciones/ResolucionesExentas/{id}', 'DocumentosDigitales\RegistroDocumentosDigitalesController@detallederivacionesresex')->name('registro_detalles_derivaciones_resex');
  Route::get('/DocumentosDigitales/RegistroDetalleDerivaciones/Memorandums/{id}', 'DocumentosDigitales\RegistroDocumentosDigitalesController@detallederivacionesmemo')->name('registro_detalles_derivaciones_memo');
  Route::get('/DocumentosDigitales/RegistroDetalleDerivaciones/DecretoExento/{id}', 'DocumentosDigitales\RegistroDocumentosDigitalesController@detallederivacionesdecretoexento')->name('registro_detalles_derivaciones_decretoexento');
  Route::get('/DocumentosDigitales/RegistroDetalleDerivaciones/DecretoSupremo/{id}', 'DocumentosDigitales\RegistroDocumentosDigitalesController@detallederivacionesdecretosupremo')->name('registro_detalles_derivaciones_decretosupremo');
  Route::get('/DocumentosDigitales/RegistroDetalleDerivaciones/Decreto/{id}', 'DocumentosDigitales\RegistroDocumentosDigitalesController@detallederivacionesdecreto')->name('registro_detalles_derivaciones_decreto');
  Route::get('/DocumentosDigitales/Crear/Registro/{idTipoDcoumento}', 'DocumentosDigitales\RegistroDocumentosDigitalesController@create')->name('registro_crear');
  Route::post('/DocumentosDigitales/Nuevo/Registro', 'DocumentosDigitales\RegistroDocumentosDigitalesController@store')->name('registro_nuevo');
  Route::get('/DocumentosDigitales/RegistroBorrar/{id}', 'DocumentosDigitales\RegistroDocumentosDigitalesController@destroy')->name('registro_destroy');
 

  //Derivación de Registro Documentos Digitales
  Route::get('/DocumentosDigitales/ColaboracionesPendientes', 'DocumentosDigitales\RegistroDocumentosDigitalesController@index2')->name('registros_derivados');
  Route::get('/DocumentosDigitales/Editar/RegistroColaboracionFirmante/{id}', 'DocumentosDigitales\RegistroDocumentosDigitalesController@editFirmante')->name('registro_derivado_editar_firmante');
  Route::post('/DocumentosDigitales/Update/RegistroColaboracionFirmante/{id}', 'DocumentosDigitales\RegistroDocumentosDigitalesController@updateFirmante')->name('registro_derivado_update_firmante'); 
  Route::get('/DocumentosDigitales/Editar/RegistroColaboracion/{id}', 'DocumentosDigitales\RegistroDocumentosDigitalesController@edit')->name('registro_derivado_editar');
  Route::post('/DocumentosDigitales/Update/RegistroColaboracion/{id}', 'DocumentosDigitales\RegistroDocumentosDigitalesController@update')->name('registro_derivado_update'); 
  Route::get('/DocumentosDigitales/FirmantesBorrar/{id}', 'DocumentosDigitales\RegistroDocumentosDigitalesController@destroyFir')->name('destroy_firmante');
  Route::get('/DocumentosDigitales/DestinatarioBorrar/{id}', 'DocumentosDigitales\RegistroDocumentosDigitalesController@destroyDes')->name('destroy_destinatario');

  
  //Corregir Documento por Error de Plantilla y no Firma
  Route::get('/DocumentosDigitales/CorregirDocumento/{id}', 'DocumentosDigitales\RegistroDocumentosDigitalesController@corregirDoc')->name('corregir_documento');
  Route::post('/DocumentosDigitales/CorregidoDocumento/{id}', 'DocumentosDigitales\RegistroDocumentosDigitalesController@corregidoDoc')->name('corregido_documento');


  //Firma de Registro Documentos Digitales 
  Route::get('/DocumentosDigitales/FirmasPendientes', 'DocumentosDigitales\RegistroDocumentosDigitalesController@firmaspendientes')->name('firmas_pendientes');
  Route::get('/DocumentosDigitales/FirmasPendientes/Detalles/{id}', 'DocumentosDigitales\RegistroDocumentosDigitalesController@firmaspendientesdetalles')->name('firmas_pendientes_detalles');
  Route::post('/DocumentosDigitales/Firmando/{id}', 'DocumentosDigitales\RegistroDocumentosDigitalesController@firmando')->name('firmando'); 
  Route::get('/DocumentosDigitales/Firmado/{id}', 'DocumentosDigitales\RegistroDocumentosDigitalesController@firmado')->name('firmado_doc');

 
  //CLAVE UNICA COMENTADOS
  Route::get('/FirmarDocumentos/LoginClaveUnica', 'DocumentosDigitales\RegistroDocumentosDigitalesController@claveUnica')->name('firmarDocumentos.LoginClaveUnica');
  Route::get('/FirmarDocumentos/Crear', 'DocumentosDigitales\RegistroDocumentosDigitalesController@firmaspendientes')->name('firmas_pendientes');

 
  //FOLIOS SIMPLE
  Route::get('/Folios', 'DocumentosDigitales\FoliosController@index')->name('folios');

  //GESTIÓN FOLIOS
  Route::get('/GestionFolios', 'DocumentosDigitales\FoliosController@index2')->name('folios2');
  Route::get('/ReservaFolios', 'DocumentosDigitales\FoliosController@reserva')->name('reserva_folios');
  Route::post('/ReservarFolios', 'DocumentosDigitales\FoliosController@reservar')->name('reservar_folios');
  Route::get('/ReservaFolios/Editar/{id}', 'DocumentosDigitales\FoliosController@editfolio')->name('reserva_folio_editar');
  Route::post('/ReservaFolios/Update/{id}', 'DocumentosDigitales\FoliosController@updatefolio')->name('reserva_folio_update');
  Route::get('/OcuparFolio/{id}', 'DocumentosDigitales\FoliosController@ocupar')->name('folio_ocupar');
  Route::post('/OcupadoFolio/{id}', 'DocumentosDigitales\FoliosController@ocupado')->name('folio_ocupado');

  //GENERAR ENLACE ADJUNTOS
  Route::get('/DocumentosDigitales/GenerarEnlaceAdjunto', 'DocumentosDigitales\RegistroDocumentosDigitalesController@indexAdjunto')->name('generar_enlace');
  Route::get('/DocumentosDigitales/Nuevo/GenerarEnlaceAdjunto', 'DocumentosDigitales\RegistroDocumentosDigitalesController@createAdjunto')->name('crear_generar_enlace');
  Route::post('/DocumentosDigitales/Nuevo/GenerarEnlaceAdjunto/Guardar', 'DocumentosDigitales\RegistroDocumentosDigitalesController@storeAdjunto')->name('nuevo_generar_enlace');
  Route::get('/DocumentosDigitales/Editar/GenerarEnlaceAdjunto/{id}', 'DocumentosDigitales\RegistroDocumentosDigitalesController@editAdjunto')->name('editar_generar_enlace');
  Route::post('/DocumentosDigitales/Update/GenerarEnlaceAdjunto/{id}', 'DocumentosDigitales\RegistroDocumentosDigitalesController@updateAdjunto')->name('update_generar_enlace');
  Route::get('/DocumentosDigitales/Borrar/GenerarEnlaceAdjunto/{id}', 'DocumentosDigitales\RegistroDocumentosDigitalesController@destroyAdjunto')->name('destroy_generar_enlace');
  Route::get('/DocumentosDigitales/Enlace/Adjunto/{id}', 'DocumentosDigitales\RegistroDocumentosDigitalesController@mostrarEnalaceAdjunto')->name('mostrar_enlace');

  //DISTRIBUCIÓN
  Route::get('/DocumentosDigitales/Distribuir/{id}', 'DocumentosDigitales\FoliosController@distribucion')->name('distribucion.folios');
  Route::post('/DocumentosDigitales/EnvioDistribuir/{id}', 'DocumentosDigitales\FoliosController@distribuir')->name('enviodistribucion.folios');

  //REPORTE FOLIOS APARTADOS MES
  Route::get('/GestionFolios/ReporteMes','DocumentosDigitales\FoliosController@reporteFolios')->name('reporte_folios');

  //GALERIA IMAGENES
  Route::get('/DocumentosDigitales/GaleriaFirmas','DocumentosDigitales\RegistroDocumentosDigitalesController@galeriaFirma')->name('galeria_firmas');

  //RECURSOS DE APOYO
  Route::get('/DocumentosDigitales/RecursosDeApoyo', 'DocumentosDigitales\RegistroDocumentosDigitalesController@recursosApoyo')->name('recursos_apoyo');
  Route::get('/DocumentosDigitales/Manuales', 'DocumentosDigitales\RegistroDocumentosDigitalesController@manuales')->name('manuales');
  Route::get('/DocumentosDigitales/PreguntasFrecuentes', 'DocumentosDigitales\RegistroDocumentosDigitalesController@preguntas')->name('preguntas');
  Route::get('/DocumentosDigitales/PlantillasPersonalizadas', 'DocumentosDigitales\RegistroDocumentosDigitalesController@plantillasPersonalizadas')->name('plantillas_personalizadas');
  Route::get('/DocumentosDigitales/PlantillasGenerales', 'DocumentosDigitales\RegistroDocumentosDigitalesController@plantillasGenerales')->name('plantillas_generales');




  


  