<?php

  Route::get('/FirmarDocumentos', 'FirmarDocumentos\FirmarDocumentosController@index')->name('firmarDocumentos');
  Route::get('/FirmarDocumentosIndividual/Crear', 'FirmarDocumentos\FirmarDocumentosController@crear00')->name('firmarDocumentosInvividual.create');
  Route::post('/FirmarDocumentos/CrearC', 'FirmarDocumentos\FirmarDocumentosController@crear')->name('firmarDocumentos.createC');
  Route::post('/FirmarDocumentos/Firmar', 'FirmarDocumentos\FirmarDocumentosController@firmar')->name('firmarDocumentos.firmar');
  Route::post('/FirmarDocumentos/FirmarV2', 'FirmarDocumentos\FirmarDocumentosController@firmarv2')->name('firmarDocumentos.firmarv2');

  Route::get('/FirmarDocumentos/TestDoc', 'FirmarDocumentos\FirmarDocumentosController@testDoc')->name('firmarDocumentos.testDoc');
  //Route::get('/FirmarDocumentos/LoginClaveUnica', 'FirmarDocumentos\FirmarDocumentosController@claveUnica1')->name('firmarDocumentos.LoginClaveUnica');
  Route::any('/claveunica/autenticar', 'FirmarDocumentos\FirmarDocumentosController@redirectToProvider')->name('login.claveunica'); 
  Route::any('/claveunica/validar', 'FirmarDocumentos\FirmarDocumentosController@handleProviderCallback')->name('login.claveunica.callback');
