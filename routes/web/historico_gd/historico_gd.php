<?php
Route::get('HistoricoGD', 'HistoricoGd\HistoricoGdController@index')->name('historico_gd');
Route::post('/HistoricoGD/Filtros', 'HistoricoGd\HistoricoGdController@index')->name('historico_gd_Filtros');
Route::get('HistoricoGdArchivos', 'HistoricoGd\HistoricoGdController@indexarchivos')->name('historico_gd_archivos');
Route::post('/HistoricoGdArchivos/Filtros', 'HistoricoGd\HistoricoGdController@indexarchivos')->name('historico_gd_archivos_Filtros');
Route::get('VerificarArchivos', 'HistoricoGd\HistoricoGdController@verificararchivo')->name('verificararchivo');
