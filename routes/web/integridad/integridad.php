<?php

// CONSULTA
  Route::get('Consulta', 'Integridad\IntegridadController@consulta')->name('consulta');
  Route::get('Consulta/Detalles/{id}', 'Integridad\IntegridadController@consultaShow')->name('consultaDetalle'); 

// DENUNCIA
  Route::get('Denuncia', 'Integridad\IntegridadController@denuncia')->name('denuncia');
  Route::get('Denuncia/Detalles/{id}', 'Integridad\IntegridadController@denunciaShow')->name('denunciaDetalle'); 
