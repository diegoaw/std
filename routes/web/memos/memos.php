<?php
Route::get('/Memos', 'Memos\MemosController@index')->name('Memos');
Route::post('/Memos/Filtros', 'Memos\MemosController@index')->name('MemosFiltros');
Route::get('/Memo/Detalle/{id}', 'Memos\MemosController@show')->name('Memosshow');

Route::get('/Memos/Colaboraciones', 'Memos\MemosColaboracionesController@index')->name('MemosColaboraciones');
Route::post('/Memos/Colaboraciones/Filtros', 'Memos\MemosColaboracionesController@index')->name('MemosCobaloracionesFiltros');
Route::get('/Memo/Colaboraciones/Detalle/{id}', 'Memos\MemosColaboracionesController@show')->name('MemosshowColaboraciones');

Route::get('/Memo/DistribucionSTD/{id}/{tramite_id}', 'Memos\MemosController@envioCorreo')->name('MemosDistribucion');
Route::post('/Memo/EnvioDistribucionSTD/{id}', 'Memos\MemosController@distribucionStd')->name('MemosEnvioDistribucion');

//STORE PROCEDURE
Route::get('/Memoss', 'Memos\MemosController@index2')->name('Memos2');
Route::post('/Memoss/Filtross', 'Memos\MemosController@index2')->name('MemossFiltros');
Route::get('/Memoss/Detalle/{id}', 'Memos\MemosController@show2')->name('Memosshow2');

Route::get('/Memoss/DistribucionSTD/{tramite_id}', 'Memos\MemosController@envioCorreo2')->name('MemosDistribucion2');
Route::post('/Memoss/EnvioDistribucionSTD/{id}', 'Memos\MemosController@distribucionStd2')->name('MemosEnvioDistribucion2');
