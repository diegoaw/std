<?php
Route::get('/Oficios', 'Oficios\OficiosController@index')->name('Oficios');
Route::post('/Oficios/Filtros', 'Oficios\OficiosController@index')->name('OficiosFiltros');
Route::get('/Oficios/Detalle/{id}', 'Oficios\OficiosController@show')->name('Oficiosshow');
Route::get('/Oficios/Borrar/{id}', 'Oficios\OficiosController@destroy')->name('Oficiosdestroy');
Route::get('/Oficios/Restaurar/{id}', 'Oficios\OficiosController@restaurar')->name('Oficiosrestaurar');

Route::get('/Oficios/Colaboraciones', 'Oficios\OficiosColaboracionesController@index')->name('OficiosColaboraciones');
Route::post('/Oficios/Colaboraciones/Filtros', 'Oficios\OficiosColaboracionesController@index')->name('OficiosCobaloracionesFiltros');
Route::get('/Oficios/Colaboraciones/Detalle/{id}', 'Oficios\OficiosColaboracionesController@show')->name('OficiosshowColaboraciones');

Route::get('/Oficio/DistribucionSTD/{id}/{tramite_id}', 'Oficios\OficiosController@envioCorreo')->name('OficiosDistribucion');
Route::post('/Oficio/EnvioDistribucionSTD/{id}', 'Oficios\OficiosController@distribucionStd')->name('OficiosEnvioDistribucion');

//STORE PROCEDURE
Route::get('/Oficioss', 'Oficios\OficiosController@index2')->name('Oficios2');
Route::post('/Oficioss/Filtross', 'Oficios\OficiosController@index2')->name('OficiossFiltros');
Route::get('/Oficioss/Detalle/{id}', 'Oficios\OficiosController@show2')->name('Oficioshow2');

Route::get('/Oficioss/DistribucionSTD/{tramite_id}', 'Oficios\OficiosController@envioCorreo2')->name('OficiosDistribucion2');
Route::post('/Oficioss/EnvioDistribucionSTD/{id}', 'Oficios\OficiosController@distribucionStd2')->name('OficiosEnvioDistribucion2');
