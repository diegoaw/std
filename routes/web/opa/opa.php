<?php

Route::get('/GestionDocumental', 'Opa\OpaController@index')->name('opa');
Route::get('/GestionDocumental/CrearIngresoSalida', 'Opa\OpaController@create')->name('opa_crear');
Route::post('/GestionDocumental/NuevoIngresoSalida', 'Opa\OpaController@store')->name('opa_nuevo');
Route::get('/GestionDocumental/ActualizarIngresoSalida/{id}', 'Opa\OpaController@edit')->name('opa_actualizar');
Route::post('/GestionDocumental/UpdateIngresoSalida/{id}', 'Opa\OpaController@update')->name('opa_update');
