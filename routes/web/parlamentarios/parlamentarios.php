<?php

  //AMBITOS
  Route::get('/AgendaViajesParlamentos', 'Parlamentarios\ParlamentariosController@index')->name('parlamentarios');
  Route::get('/AgendaViajesParlamentos/Crear', 'Parlamentarios\ParlamentariosController@create')->name('parlamento_crear');
  Route::post('/AgendaViajesParlamentos/Nuevo', 'Parlamentarios\ParlamentariosController@store')->name('parlamento_nuevo');
  Route::get('/AgendaViajesParlamentos/Editar/{id}', 'Parlamentarios\ParlamentariosController@edit')->name('parlamento_editar');
  Route::post('/AgendaViajesParlamentos/Update/{id}', 'Parlamentarios\ParlamentariosController@update')->name('parlamento_update');
  Route::get('/AgendaViajesParlamentos/Borrar/{id}', 'Parlamentarios\ParlamentariosController@destroy')->name('parlamento_destroy');
  Route::get('/AgendaViajesParlamentos/Reporte','Parlamentarios\ParlamentariosController@reporte')->name('parlamento_reporte');
