<?php
Route::get('Reconocimiento', 'Reconocimiento\ReconocimientoController@index')->name('reconocimiento');
Route::get('Reconocimiento/Detalles/{id}', 'Reconocimiento\ReconocimientoController@show')->name('reconocimientoDetalle'); 
Route::get('Reconocimiento/Borrar/{id}', 'Reconocimiento\ReconocimientoController@destroy')->name('reconocimientdestroy');
Route::get('Reconocimiento/Notificacion/{data}', 'Reconocimiento\ReconocimientoController@notificacion')->name('reconocimientonoti');


