<?php


  Route::get('/RequerimientosCompras', 'RequerimientosCompras\RequerimientosComprasController@index')->name('requecompras');
  Route::get('/RequerimientosCompras/Filtros', 'RequerimientosCompras\RequerimientosComprasController@index')->name('requecomprasFiltros');
  Route::get('/RequerimientosCompras/Detalle/{id}', 'RequerimientosCompras\RequerimientosComprasController@show')->name('requecomprasshow');
  Route::get('/RequerimientosCompras/Reporte/Compras/{tipoReporte}','RequerimientosCompras\RequerimientosComprasController@reporteCompras')->name('reporte_compras');
  Route::get('/RequerimientosCompras/Dashboard', 'RequerimientosCompras\RequerimientosComprasController@dashboard')->name('dashboardrequecompras');
  Route::get('/RequerimientosCompras/Borrar/{id}', 'RequerimientosCompras\RequerimientosComprasController@destroy')->name('requecomprasdestroy');

  Route::get('ActaRecepcionConforme', 'RequerimientosCompras\RequerimientosComprasController@acta')->name('acta');
  Route::get('ActaRecepcionConforme/Detalles/{id}', 'RequerimientosCompras\RequerimientosComprasController@actaShow')->name('actaDetalle'); 
  Route::get('ActaRecepcionConforme/Borrar/{id}', 'RequerimientosCompras\RequerimientosComprasController@destroyActas')->name('actasdestroy');
