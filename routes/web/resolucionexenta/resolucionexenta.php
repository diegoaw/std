<?php
Route::get('/ResolucionesExentas', 'ResolucionesExentas\ResolucionesExentasController@index')->name('ResolucionesExentas');
Route::post('/ResolucionesExentas/Filtros', 'ResolucionesExentas\ResolucionesExentasController@index')->name('ResolucionesExentasFiltros');
Route::get('/ResolucionesExentas/Detalle/{id}', 'ResolucionesExentas\ResolucionesExentasController@show')->name('ResolucionesExentasshow');

Route::get('/ResolucionesExentas/Colaboraciones', 'ResolucionesExentas\ResolucionesExentasColaboracionesController@index')->name('ResolucionesExentasColaboraciones');
Route::post('/ResolucionesExentas/Colaboraciones/Filtros', 'ResolucionesExentas\ResolucionesExentasColaboracionesController@index')->name('ResolucionesExentasCobaloracionesFiltros');
Route::get('/ResolucionesExentas/Colaboraciones/Detalle/{id}', 'ResolucionesExentas\ResolucionesExentasColaboracionesController@show')->name('ResolucionesExentasshowColaboraciones');


Route::get('/ResolucionesExentas/DistribucionSTD/{id}/{tramite_id}', 'ResolucionesExentas\ResolucionesExentasController@envioCorreo')->name('ResolucionesExentasDistribucion');
Route::post('/ResolucionesExentas/EnvioDistribucionSTD/{id}', 'ResolucionesExentas\ResolucionesExentasController@distribucionStd')->name('ResolucionesExentasEnvioDistribucion');

