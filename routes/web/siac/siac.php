<?php
Route::get('SIAC', 'Siac\SiacController@index')->name('siac');
Route::get('SIAC/Detalles/{id}', 'Siac\SiacController@show')->name('siacDetalle'); 
Route::get('SIAC/Borrar/{id}', 'Siac\SiacController@destroy')->name('siacdestroy');
Route::get('SIAC/Notificacion/{data}', 'Siac\SiacController@notificacion')->name('siacnoti');