<?php

  //CALENDARIO
  Route::get('/SIGC/Calendario', 'Sigc\CalendarioController@index')->name('sigc_calendario');
  Route::get('/SIGC/Calendario/Crear', 'Sigc\CalendarioController@create')->name('sigc_calendario_crear');
  Route::post('/SIGC/Calendario/Nuevo', 'Sigc\CalendarioController@store')->name('sigc_calendario_nuevo');
  Route::get('/SIGC/Calendario/Editar/{id}', 'Sigc\CalendarioController@edit')->name('sigc_calendario_editar');
  Route::post('/SIGC/Calendario/Update/{id}', 'Sigc\CalendarioController@update')->name('sigc_calendario_update');

  //INICIO
  Route::get('/SIGC/Inicio', 'Sigc\InicioController@index')->name('sigc_inicio');
  Route::get('/SIGC/Inicio/Reporte/Indicadores','Sigc\InicioController@reporteIndicadores')->name('sigc_reporte_indicadores');
  Route::get('/SIGC/Inicio/{mes}', 'Sigc\InicioController@index_month')->name('sigc_inicio_mont');
  Route::get('/SIGC/Inicio/SelectEquipo/{tipo}', 'Sigc\InicioController@getEquiposForTipo')->name('sigc_select_equipo');
  Route::get('/SIGC/SelectEquipos', 'Sigc\InicioController@TodosEquipos');
  Route::get('/SIGC/SelectDivision', 'Sigc\InicioController@TodasDivisiones');
  Route::get('/SIGC/SelectDivisionPT/{tipo}', 'Sigc\InicioController@getDivisionesForTipo');
  Route::get('/SIGC/SelectDivisionPE/{equipo}', 'Sigc\InicioController@getDivisionesForEquipo');

  //REPORTAR Y REVISIÓN
  Route::get('/SIGC/Indicadores/Usuario', 'Sigc\ReportesController@indicators')->name('sigc_indicadores_usuario');
  Route::get('/SIGC/Mediciones/Revision', 'Sigc\ReportesController@mediciones')->name('sigc_mediciones_revision');
  Route::get('/SIGC/Reportar/{id}', 'Sigc\ReportesController@report')->name('sigc_reportar');
  Route::get('/SIGC/Reportar/Calcular/Resultado/{num}/{den}/{ind}', 'Sigc\ReportesController@calcular')->name('sigc_reportar_calcular');
  Route::post('SIGC/Reportada/{id}', 'Sigc\ReportesController@reported')->name('sigc_reportada');
  Route::get('/SIGC/Editar/Reporte/{id}', 'Sigc\ReportesController@edit')->name('sigc_editar_reporte');
  Route::get('/SIGC/Editar/ReporteBorrador/{id}', 'Sigc\ReportesController@editBorrador')->name('sigc_editar_borrador_reporte');
  Route::get('/SIGC/Enviar/Reporte/{id}', 'Sigc\ReportesController@enviarReporte')->name('sigc_enviar_reporte');
  Route::post('SIGC/Update/Reporte/{id}', 'Sigc\ReportesController@update')->name('sigc_update_reporte');
  Route::post('SIGC/UpdateBorrador/Reporte/{id}', 'Sigc\ReportesController@updateBorrador')->name('sigc_update_borrador_reporte');
  Route::get('/SIGC/Revision/{id}', 'Sigc\ReportesController@revision')->name('sigc_revision');
  Route::post('SIGC/Revisada/{id}', 'Sigc\ReportesController@revisada')->name('sigc_revisada');
  Route::get('/SIGC/Editar/Revision/{id}', 'Sigc\ReportesController@revisioncorreccion')->name('sigc_editar_revision');
  Route::post('SIGC/Update/Revisada/{id}', 'Sigc\ReportesController@correccionrevisada')->name('sigc_update_revisada');
  Route::get('/SIGC/Editar/Medición/{id}', 'Sigc\ReportesController@editmedicion')->name('sigc_editar_medicion');
  Route::post('SIGC/Update/Medición/{id}', 'Sigc\ReportesController@updatetmedicion')->name('sigc_update_medicion');

  //MEDICIONES
  Route::get('/SIGC/Mediciones', 'Sigc\MedicionesController@index')->name('sigc_mediciones');

  Route::get('/SIGC/Mediciones/GetTipoPorAno/{ano}', 'Sigc\MedicionesController@getTipoPorAno')->name('sigc_mediciones_get_tipo_ano');
  Route::get('/SIGC/Mediciones/GetEquiposPorAno/{ano}', 'Sigc\MedicionesController@getEquipoPorAno')->name('sigc_mediciones_get_equipo_ano');
  Route::get('/SIGC/Mediciones/GetIndicadoresSelect/{ano}/{tipo}/{equipo}', 'Sigc\MedicionesController@getIndicadoresSelect')->name('sigc_mediciones_get_indicadores_select');

  Route::get('/SIGC/Mediciones/Descargar/{id}', 'Sigc\MedicionesController@descargar')->name('sigc_descarga');
  Route::get('/SIGC/Mediciones/Detalle/{id}', 'Sigc\MedicionesController@detalle')->name('sigc_mediciones_detalle');
  Route::get('/SIGC/Mediciones/Historico', 'Sigc\MedicionesController@historico')->name('sigc_mediciones_historico');
  Route::get('/SIGC/Mediciones/Historico/Detalle/{id}', 'Sigc\MedicionesController@historico_detalle')->name('sigc_mediciones_historico_detalle');
  Route::get('/SIGC/Mediciones/Estado/{id}', 'Sigc\MedicionesController@estado')->name('sigc_mediciones_estado');
  Route::get('/SIGC/Mediciones/ArchivosAsociados/{id}', 'Sigc\ReportesController@editMedicionArchivos')->name('sigc_mediciones_archivos_asociados');
  Route::get('/SIGC/Mediciones/ArchivosAsociados/Eliminar/{id}', 'Sigc\ReportesController@chageStatusFile')->name('sigc_mediciones_archivos_asociados_eliminar');

  //INDICADORES
  Route::get('/SIGC/Indicadores', 'Sigc\IndicadoresController@index')->name('sigc_indicadores');


  Route::get('/SIGC/Indicadores/GetTipoPorAno/{ano}', 'Sigc\IndicadoresController@getTipoPorAno')->name('sigc_indicadores_get_tipo_ano');
  Route::get('/SIGC/Indicadores/GetEquiposPorAno/{ano}', 'Sigc\IndicadoresController@getEquipoPorAno')->name('sigc_indicadores_get_equipo_ano');
  Route::get('/SIGC/Indicadores/GetIndicadoresSelect/{ano}/{tipo}/{equipo}', 'Sigc\IndicadoresController@getIndicadoresSelect')->name('sigc_indicadores_get_indicadores_select');


  Route::get('/SIGC/Indicadores/Crear', 'Sigc\IndicadoresController@create')->name('sigc_indicadores_crear');
  Route::post('/SIGC/Indicadores/Guardar', 'Sigc\IndicadoresController@store')->name('sigc_indicadores_guardar');
  Route::get('/SIGC/Indicadores/editar/{id}', 'Sigc\IndicadoresController@edit')->name('sigc_indicadores_editar');
  Route::get('/SIGC/Habilitar/Suplencia/{id}', 'Sigc\IndicadoresController@habilitarSuplencia')->name('sigc_habilitar_suplencia');
  Route::get('/SIGC/Deshabilitar/Suplencia/{id}', 'Sigc\IndicadoresController@deshabilitarSuplencia')->name('sigc_deshabilitar_suplencia');
  Route::post('/SIGC/Indicadores/Actualizar/{id}', 'Sigc\IndicadoresController@update')->name('sigc_indicadores_actualizar');
  Route::get('/SIGC/Reportabilidad', 'Sigc\IndicadoresController@reportabilidad')->name('sigc_reportabilidad');
  Route::get('/SIGC/NoReporto/{id}', 'Sigc\IndicadoresController@notificacionNoReporto')->name('sigc_no_reporto');

  //DOCUMENTOS
  Route::get('/SIGC/Documentos', 'Sigc\DocumentosController@index')->name('sigc_documentos');
  Route::get('/SIGC/Crear/Documento', 'Sigc\DocumentosController@create')->name('sigc_crear_documento');
  Route::post('/SIGC/Nuevo/Documento', 'Sigc\DocumentosController@store')->name('sigc_nuevo_documento');
  Route::get('/SIGC/Editar/Documento/{id}', 'Sigc\DocumentosController@edit')->name('sigc_editar_documento');
  Route::post('/SIGC/Update/Documento/{id}', 'Sigc\DocumentosController@update')->name('sigc_update_documento');

  //DOCUMENTOS TIPOS
  Route::get('/SIGC/DocumentoTipos', 'Sigc\DocumentoTiposController@index')->name('sigc_documento_tipos');
  Route::get('/SIGC/Crear/DocumentoTipo', 'Sigc\DocumentoTiposController@create')->name('sigc_crear_documento_tipo');
  Route::post('/SIGC/Nuevo/DocumentoTipo', 'Sigc\DocumentoTiposController@store')->name('sigc_nuevo_documento_tipo');
  Route::get('/SIGC/Editar/DocumentoTipo/{id}', 'Sigc\DocumentoTiposController@edit')->name('sigc_editar_documento_tipo');
  Route::post('/SIGC/Update/DocumentoTipo/{id}', 'Sigc\DocumentoTiposController@update')->name('sigc_update_documento_tipo');

  //AMBITOS
  Route::get('/SIGC/Ambitos', 'Sigc\AmbitosController@index')->name('sigc_ambitos');
  Route::get('/SIGC/Crear/Ambito', 'Sigc\AmbitosController@create')->name('sigc_crear_ambito');
  Route::post('/SIGC/Nuevo/Ambito', 'Sigc\AmbitosController@store')->name('sigc_nuevo_ambito');
  Route::get('/SIGC/Editar/Ambito/{id}', 'Sigc\AmbitosController@edit')->name('sigc_editar_ambito');
  Route::post('/SIGC/Update/Ambito/{id}', 'Sigc\AmbitosController@update')->name('sigc_update_ambito');

  //CATEGORIAS
  Route::get('/SIGC/Categorias', 'Sigc\CategoriasController@index')->name('sigc_categorias');
  Route::get('/SIGC/Crear/Categoria', 'Sigc\CategoriasController@create')->name('sigc_crear_categoria');
  Route::post('/SIGC/Nuevo/Categoria', 'Sigc\CategoriasController@store')->name('sigc_nueva_categoria');
  Route::get('/SIGC/Editar/Categoria/{id}', 'Sigc\CategoriasController@edit')->name('sigc_editar_categoria');
  Route::post('/SIGC/Update/Categoria/{id}', 'Sigc\CategoriasController@update')->name('sigc_update_categoria');

  //EQUIPOS
  Route::get('/SIGC/Equipos', 'Sigc\EquiposController@index')->name('sigc_equipos');
  Route::get('/SIGC/Crear/Equipo', 'Sigc\EquiposController@create')->name('sigc_crear_equipo');
  Route::post('/SIGC/Nuevo/Equipo', 'Sigc\EquiposController@store')->name('sigc_nuevo_equipo');
  Route::get('/SIGC/Editar/Equipo/{id}', 'Sigc\EquiposController@edit')->name('sigc_editar_equipo');
  Route::post('/SIGC/Update/Equipo/{id}', 'Sigc\EquiposController@update')->name('sigc_update_equipo');
  Route::get('/SIGC/Agregar/Miembro/{id}', 'Sigc\EquiposController@agregarmiembro')->name('sigc_agregar_miembro');
  Route::post('/SIGC/Guardar/Miembro/{id}', 'Sigc\EquiposController@guardarmiembro')->name('sigc_guardar_miembro');

  //EQUIPOS MIEMBRO ROLES
  Route::get('/SIGC/EquipoMiembroRoles', 'Sigc\EquipoMiembroRolesController@index')->name('sigc_equipo_miembro_roles');
  Route::get('/SIGC/Crear/EquipoMiembroRol', 'Sigc\EquipoMiembroRolesController@create')->name('sigc_crear_equipo_miembro_rol');
  Route::post('/SIGC/Nuevo/EquipoMiembroRol', 'Sigc\EquipoMiembroRolesController@store')->name('sigc_nuevo_equipo_miembro_rol');
  Route::get('/SIGC/Editar/EquipoMiembroRol/{id}', 'Sigc\EquipoMiembroRolesController@edit')->name('sigc_editar_equipo_miembro_rol');
  Route::post('/SIGC/Update/EquipoMiembroRol/{id}', 'Sigc\EquipoMiembroRolesController@update')->name('sigc_update_equipo_miembro_rol');

  //DIMENSIONES
  Route::get('/SIGC/Dimensiones', 'Sigc\DimensionesController@index')->name('sigc_dimensiones');
  Route::get('/SIGC/Crear/Dimension', 'Sigc\DimensionesController@create')->name('sigc_crear_dimension');
  Route::post('/SIGC/Nueva/Dimension', 'Sigc\DimensionesController@store')->name('sigc_nueva_dimension');
  Route::get('/SIGC/Editar/Dimension/{id}', 'Sigc\DimensionesController@edit')->name('sigc_editar_dimension');
  Route::post('/SIGC/Update/Dimension/{id}', 'Sigc\DimensionesController@update')->name('sigc_update_dimension');

  //PRODUCTOS
  Route::get('/SIGC/Productos', 'Sigc\ProductosController@index')->name('sigc_productos');
  Route::get('/SIGC/Crear/Producto', 'Sigc\ProductosController@create')->name('sigc_crear_producto');
  Route::post('/SIGC/Nuevo/Producto', 'Sigc\ProductosController@store')->name('sigc_nuevo_producto');
  Route::get('/SIGC/Editar/Producto/{id}', 'Sigc\ProductosController@edit')->name('sigc_editar_producto');
  Route::post('/SIGC/Update/Producto/{id}', 'Sigc\ProductosController@update')->name('sigc_update_producto');

  //TIPOS
  Route::get('/SIGC/Tipos', 'Sigc\TiposController@index')->name('sigc_tipos');
  Route::get('/SIGC/Crear/Tipo', 'Sigc\TiposController@create')->name('sigc_crear_tipo');
  Route::post('/SIGC/Nuevo/Tipo', 'Sigc\TiposController@store')->name('sigc_nuevo_tipo');
  Route::get('/SIGC/Editar/Tipo/{id}', 'Sigc\TiposController@edit')->name('sigc_editar_tipo');
  Route::post('/SIGC/Update/Tipo/{id}', 'Sigc\TiposController@update')->name('sigc_update_tipo');

  //UNIDADES
  Route::get('/SIGC/Unidades', 'Sigc\UnidadesController@index')->name('sigc_unidades');
  Route::get('/SIGC/Crear/Unidad', 'Sigc\UnidadesController@create')->name('sigc_crear_unidad');
  Route::post('/SIGC/Nueva/Unidad', 'Sigc\UnidadesController@store')->name('sigc_nueva_unidad');
  Route::get('/SIGC/Editar/Unidad/{id}', 'Sigc\UnidadesController@edit')->name('sigc_editar_unidad');
  Route::post('/SIGC/Update/Unidad/{id}', 'Sigc\UnidadesController@update')->name('sigc_update_unidad');

  //USUARIOS
  Route::get('/SIGC/Usuarios', 'Sigc\UsuariosController@index')->name('sigc_usuarios');
  Route::get('/SIGC/Crear/Usuario/UsuarioMindep/{id}', 'Sigc\UsuariosController@getUsuarioMindep')->name('sigc_usuario_mindep');
  Route::get('/SIGC/Crear/Usuario', 'Sigc\UsuariosController@create')->name('sigc_crear_usuario');
  Route::post('/SIGC/Nuevo/Usuario', 'Sigc\UsuariosController@store')->name('sigc_nuevo_usuario');
  Route::get('/SIGC/Editar/Usuario/{id}', 'Sigc\UsuariosController@edit')->name('sigc_editar_usuario');
  Route::post('/SIGC/Update/Usuario/{id}', 'Sigc\UsuariosController@update')->name('sigc_update_usuario');



  /* PROGRAMAS SOCIALES */

  //PROGRAMAS SOCIALES BENEFICIARIOS
  Route::get('/SIGC/ProgramasSociales/Beneficiarios', 'Sigc\ProgramasSocialesController@index')->name('beneficiarios');
  //Route::get('/SIGC/ProgramasSociales/Beneficiarios/Reporte/{tipoReporte}','Sigc\ProgramasSocialesController@reporteBeneficiarios')->name('reporte_beneficiarios');

  //INICIO PROGRAMAS
  Route::get('/SIGC/ProgramasSociales/Inicio', 'Sigc\ProgramasInicioController@index')->name('programas_inicio');
  Route::get('/SIGC/ProgramasSociales/Inicio/{mes}', 'Sigc\ProgramasInicioController@index_month')->name('programas_inicio_mont');
  Route::get('/SIGC/SelectProgramasSocialesCS', 'Sigc\ProgramasInicioController@TodosComponentes');
  Route::get('/SIGC/SelectProgramasSocialesC/{programa}', 'Sigc\ProgramasInicioController@getComponentesForPrograma');
  Route::get('/SIGC/SelectProgramasSocialesPS', 'Sigc\ProgramasInicioController@TodosPropositos');
  Route::get('/SIGC/SelectProgramasSocialesP/{programa}', 'Sigc\ProgramasInicioController@getPropositoForPrograma');

  //CALENDARIO EVENTOS PROGRAMAS
  Route::get('/SIGC/ProgramasSociales/CalendarioEventos', 'Sigc\ProgramasCalendarioEventosController@index')->name('programas_calendario_eventos');
  Route::get('/SIGC/ProgramasSociales/CalendarioEventos/Crear', 'Sigc\ProgramasCalendarioEventosController@create')->name('programas_calendario_eventos_crear');
  Route::post('/SIGC/ProgramasSociales/CalendarioEventos/Nuevo', 'Sigc\ProgramasCalendarioEventosController@store')->name('programas_calendario_eventos_nuevo');
  Route::get('/SIGC/ProgramasSociales/CalendarioEventos/Editar/{id}', 'Sigc\ProgramasCalendarioEventosController@edit')->name('programas_calendario_eventos_editar');
  Route::post('/SIGC/ProgramasSociales/CalendarioEventos/Update/{id}', 'Sigc\ProgramasCalendarioEventosController@update')->name('programas_calendario_eventos_update');
  Route::get('/SIGC/ProgramasSociales/Borrar/CalendarioEventos/{id}', 'Sigc\ProgramasCalendarioEventosController@destroy')->name('programas_calendario_eventos_borrar');


  //EVENTOS PROGRAMAS
  Route::get('/SIGC/ProgramasSociales/Eventos', 'Sigc\ProgramasEventosController@index')->name('programas_eventos');
  Route::get('/SIGC/ProgramasSociales/Crear/Evento', 'Sigc\ProgramasEventosController@create')->name('programas_eventos_crear');
  Route::post('/SIGC/ProgramasSociales/Nuevo/Evento', 'Sigc\ProgramasEventosController@store')->name('programas_eventos_nuevo');
  Route::get('/SIGC/ProgramasSociales/Editar/Evento/{id}', 'Sigc\ProgramasEventosController@edit')->name('programas_eventos_editar');
  Route::post('/SIGC/ProgramasSociales/Update/Evento/{id}', 'Sigc\ProgramasEventosController@update')->name('programas_eventos_update');
  Route::get('/SIGC/ProgramasSociales/Borrar/Evento/{id}', 'Sigc\ProgramasEventosController@destroy')->name('programas_eventos_borrar');
  Route::get('/SIGC/ProgramasSociales/HabilitarSuplencia/{id}', 'Sigc\ProgramasEventosController@habilitarSuplencia')->name('sigc_eventos_habilitar_suplencia');
  Route::get('/SIGC/ProgramasSociales/DeshabilitarSuplencia/{id}', 'Sigc\ProgramasEventosController@deshabilitarSuplencia')->name('sigc_eventos_deshabilitar_suplencia');

  //PROGRAMA MES REPORTES
  Route::get('/SIGC/ProgramasSociales/ProgramasMesReportes', 'Sigc\ProgramasMesesReportesController@index')->name('programas_mes');
  Route::get('/SIGC/ProgramasSociales/Crear/ProgramasMesReportes', 'Sigc\ProgramasMesesReportesController@create')->name('programas_mes_crear');
  Route::post('/SIGC/ProgramasSociales/Nuevo/ProgramasMesReportes', 'Sigc\ProgramasMesesReportesController@store')->name('programas_mes_nuevo');
  Route::get('/SIGC/ProgramasSociales/Editar/ProgramasMesReportes/{id}', 'Sigc\ProgramasMesesReportesController@edit')->name('programas_mes_editar');
  Route::post('/SIGC/ProgramasSociales/Update/ProgramasMesReportes/{id}', 'Sigc\ProgramasMesesReportesController@update')->name('programas_mes_update');
  Route::get('/SIGC/ProgramasSociales/Borrar/ProgramasMesReportes/{id}', 'Sigc\ProgramasMesesReportesController@destroy')->name('programas_mes_borrar');
  
  //PROGRAMAS
  Route::get('/SIGC/ProgramasSociales/Programas', 'Sigc\ProgramasController@index')->name('programas');
  Route::get('/SIGC/ProgramasSociales/Crear/Programa', 'Sigc\ProgramasController@create')->name('programas_crear');
  Route::post('/SIGC/ProgramasSociales/Nuevo/Programa', 'Sigc\ProgramasController@store')->name('programas_nuevo');
  Route::get('/SIGC/ProgramasSociales/Editar/Programa/{id}', 'Sigc\ProgramasController@edit')->name('programas_editar');
  Route::post('/SIGC/ProgramasSociales/Update/Programa/{id}', 'Sigc\ProgramasController@update')->name('programas_update');
  Route::get('/SIGC/ProgramasSociales/Borrar/Programa/{id}', 'Sigc\ProgramasController@destroy')->name('programas_borrar');

  //COMPONENTES PROGRAMAS
  Route::get('/SIGC/ProgramasSociales/Componentes', 'Sigc\ProgramasComponentesController@index')->name('programas_componentes');
  Route::get('/SIGC/ProgramasSociales/Crear/Componente', 'Sigc\ProgramasComponentesController@create')->name('programas_componentes_crear');
  Route::post('/SIGC/ProgramasSociales/Nuevo/Componente', 'Sigc\ProgramasComponentesController@store')->name('programas_componentes_nuevo');
  Route::get('/SIGC/ProgramasSociales/Editar/Componente/{id}', 'Sigc\ProgramasComponentesController@edit')->name('programas_componentes_editar');
  Route::post('/SIGC/ProgramasSociales/Update/Componente/{id}', 'Sigc\ProgramasComponentesController@update')->name('programas_componentes_update');
  Route::get('/SIGC/ProgramasSociales/Borrar/Componente/{id}', 'Sigc\ProgramasComponentesController@destroy')->name('programas_componentes_borrar');

  //INDICADORES PROGRAMAS
  Route::get('/SIGC/ProgramasSociales/Indicadores', 'Sigc\ProgramasIndicadoresController@index')->name('programas_indicadores');
  Route::get('/SIGC/ProgramasSociales/Crear/Indicador', 'Sigc\ProgramasIndicadoresController@create')->name('programas_indicadores_crear');
  Route::post('/SIGC/ProgramasSociales/Nuevo/Indicador', 'Sigc\ProgramasIndicadoresController@store')->name('programas_indicadores_nuevo');
  Route::get('/SIGC/ProgramasSociales/Editar/Indicador/{id}', 'Sigc\ProgramasIndicadoresController@edit')->name('programas_indicadores_editar');
  Route::post('/SIGC/ProgramasSociales/Update/Indicador/{id}', 'Sigc\ProgramasIndicadoresController@update')->name('programas_indicadores_update');
  Route::get('/SIGC/ProgramasSociales/Borrar/Indicador/{id}', 'Sigc\ProgramasIndicadoresController@destroy')->name('programas_indicadores_borrar');

  //COBERTURA O POBLACION PROGRAMAS
  Route::get('/SIGC/ProgramasSociales/Poblacion', 'Sigc\ProgramasPoblacionController@index')->name('programas_poblacion');
  Route::get('/SIGC/ProgramasSociales/Crear/Poblacion', 'Sigc\ProgramasPoblacionController@create')->name('programas_poblacion_crear');
  Route::post('/SIGC/ProgramasSociales/Nuevo/Poblacion', 'Sigc\ProgramasPoblacionController@store')->name('programas_poblacion_nuevo');
  Route::get('/SIGC/ProgramasSociales/Editar/Poblacion/{id}', 'Sigc\ProgramasPoblacionController@edit')->name('programas_poblacion_editar');
  Route::post('/SIGC/ProgramasSociales/Update/Poblacion/{id}', 'Sigc\ProgramasPoblacionController@update')->name('programas_poblacion_update');
  Route::get('/SIGC/ProgramasSociales/Borrar/Poblacion/{id}', 'Sigc\ProgramasPoblacionController@destroy')->name('programas_poblacion_borrar');

  //PRESUPUESTO O GASTOS PROGRAMAS
  Route::get('/SIGC/ProgramasSociales/Gastos', 'Sigc\ProgramasGastosController@index')->name('programas_presupuesto');
  Route::get('/SIGC/ProgramasSociales/Crear/Gasto', 'Sigc\ProgramasGastosController@create')->name('programas_presupuesto_crear');
  Route::post('/SIGC/ProgramasSociales/Nuevo/Gasto', 'Sigc\ProgramasGastosController@store')->name('programas_presupuesto_nuevo');
  Route::get('/SIGC/ProgramasSociales/Editar/Gasto/{id}', 'Sigc\ProgramasGastosController@edit')->name('programas_presupuesto_editar');
  Route::post('/SIGC/ProgramasSociales/Update/Gasto/{id}', 'Sigc\ProgramasGastosController@update')->name('programas_presupuesto_update');
  Route::get('/SIGC/ProgramasSociales/Borrar/Gasto/{id}', 'Sigc\ProgramasGastosController@destroy')->name('programas_presupuesto_borrar');

  //INDICADORES DPS
  Route::get('/SIGC/ProgramasSociales/DPS', 'Sigc\ProgramasSocialesController@indexDPS')->name('programas_dps');
  //REPORTE PDF
  Route::get('/SIGC/ProgramasSociales/Reporte/DPS','Sigc\ProgramasSocialesController@reporteDPS')->name('programas_reporte_dps');

  //INDICADORES CEM
  Route::get('/SIGC/ProgramasSociales/CEM', 'Sigc\ProgramasSocialesController@indexCEM')->name('programas_cem');
  //REPORTE PDF
  Route::get('/SIGC/ProgramasSociales/Reporte/CEM','Sigc\ProgramasSocialesController@reporteCEM')->name('programas_reporte_cem');

  //INDICADORES FONDEPORTE
  Route::get('/SIGC/ProgramasSociales/FONDEPORTE', 'Sigc\ProgramasSocialesController@indexFONDEPORTE')->name('programas_fondeporte');
  //REPORTE PDF
  Route::get('/SIGC/ProgramasSociales/Reporte/FONDEPORTE','Sigc\ProgramasSocialesController@reporteFONDEPORTE')->name('programas_reporte_fondeporte');

  //INDICADORES ACD
  Route::get('/SIGC/ProgramasSociales/ACD', 'Sigc\ProgramasSocialesController@indexACD')->name('programas_acd');
  //REPORTE PDF
  Route::get('/SIGC/ProgramasSociales/Reporte/ACD','Sigc\ProgramasSocialesController@reporteACD')->name('programas_reporte_acd');

  //INDICADORES FDRCP
  Route::get('/SIGC/ProgramasSociales/FDRCP', 'Sigc\ProgramasSocialesController@indexFDRCP')->name('programas_fdrcp');
  //REPORTE PDF
  Route::get('/SIGC/ProgramasSociales/Reporte/FDRCP','Sigc\ProgramasSocialesController@reporteFDRCP')->name('programas_reporte_fdrcp');

  //INDICADORES SNCD
  Route::get('/SIGC/ProgramasSociales/SNCD', 'Sigc\ProgramasSocialesController@indexSNCD')->name('programas_sncd');
  //REPORTE PDF
  Route::get('/SIGC/ProgramasSociales/Reporte/SNCD','Sigc\ProgramasSocialesController@reporteSNCD')->name('programas_reporte_sncd');

  //INDICADORES SCCD
  Route::get('/SIGC/ProgramasSociales/SCCD', 'Sigc\ProgramasSocialesController@indexSCCD')->name('programas_sccd');
  //REPORTE PDF
  Route::get('/SIGC/ProgramasSociales/Reporte/SCCD','Sigc\ProgramasSocialesController@reporteSCCD')->name('programas_reporte_sccd');

  //PROGRAMAS REPORTAR
  Route::get('/SIGC/ProgramasSociales/ProgramasUsuario', 'Sigc\ProgramasReportesController@programas')->name('programas_reportar_usuario');
  //REPORTAR
  Route::get('/SIGC/ProgramasSociales/ProgramasUsuario/Evento/{evento}/{programa}', 'Sigc\ProgramasReportesController@datosPro')->name('programas_reportar_reportar');
  Route::post('/SIGC/ProgramasSociales/Reportar/{programa}', 'Sigc\ProgramasReportesController@reportarPrograma')->name('programas_reportar_programa');
  Route::get('/SIGC/ProgramasSociales/ProgramasUsuario/EditarEvento/{evento}/{programa}', 'Sigc\ProgramasReportesController@datosProEditar')->name('programas_editar_reporte');
  Route::post('/SIGC/ProgramasSociales/Editado/{programa}', 'Sigc\ProgramasReportesController@editarReportePrograma')->name('programas_update_reporte');
  Route::get('/SIGC/ProgramasSociales/EnviarReporte/{programa}', 'Sigc\ProgramasReportesController@enviarReporte')->name('programas_enviar_reporte');
  //REVISIÓN
  Route::get('/SIGC/ProgramasSociales/Revision/{evento}/{programa}', 'Sigc\ProgramasReportesController@revisionReporte')->name('programas_revision_reporte');
  Route::get('/SIGC/ProgramasSociales/EnviarRevision/{programa}', 'Sigc\ProgramasReportesController@enviarRevision')->name('programas_enviar_revision_reporte');
  Route::post('/SIGC/ProgramasSociales/Revisado/{programa}', 'Sigc\ProgramasReportesController@reporteRevisado')->name('programas_revisado_reporte');
   //CORRECCIÓN
  Route::get('/SIGC/ProgramasSociales/ProgramasUsuario/CorreccionEvento/{evento}/{programa}', 'Sigc\ProgramasReportesController@datosProCorreccion')->name('programas_correccion_reporte');
  Route::post('/SIGC/ProgramasSociales/Corregido/{programa}', 'Sigc\ProgramasReportesController@correccionReportePrograma')->name('programas_corregido_reporte');
  //REVISIÓN CORRECCIÓN
  Route::get('/SIGC/ProgramasSociales/ProgramasUsuario/RevisarCorreccion/{evento}/{programa}', 'Sigc\ProgramasReportesController@revisionCorreccion')->name('programas_revisarcorreccion_reporte');
  Route::post('/SIGC/ProgramasSociales/RevisadaCorreccion/{programa}', 'Sigc\ProgramasReportesController@correccionRevisada')->name('programas_revisadocorreccion_reporte');

  
