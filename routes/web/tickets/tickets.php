<?php
  Route::get('TicketsMINDEP', 'Tickets\TicketsController@index')->name('tickets');
  Route::get('TicketsMINDEP/Detalles/{id}', 'Tickets\TicketsController@show')->name('ticketsDetalle'); 

  Route::get('TicketsSoporteMINDEP', 'Tickets\TicketsController@index2')->name('tickets2');
  Route::get('TicketsSoporteMINDEP/Detalles/{id}', 'Tickets\TicketsController@show2')->name('ticketsDetalle2'); 
