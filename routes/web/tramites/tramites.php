<?php
Route::get('Tramites', 'Tramites\TramitesController@index')->name('tramites');
Route::post('/Tramites/Filtros', 'Tramites\TramitesController@index')->name('tramitesFiltros');
Route::post('Tramites/Reporte', 'Tramites\TramitesController@reporteTramite')->name('tramitesReportes');

//STORE PROCEDURE
Route::get('Tramitess', 'Tramites\TramitesController@index2')->name('tramitess');
Route::post('/Tramitess/Filtross', 'Tramites\TramitesController@index2')->name('tramitessFiltros');
Route::post('Tramitess/Reportes', 'Tramites\TramitesController@reporteTramite2')->name('tramitesReportes2');
