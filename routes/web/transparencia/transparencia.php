<?php
//TIPOS GESTIONES
  Route::get('/Transparencia/TiposGestiones', 'Transparencia\TiposGestionesController@index')->name('transparencia_tipos');
  Route::get('/Transparencia/Crear/TipoGestiones', 'Transparencia\TiposGestionesController@create')->name('transparencia_crear_tipo');
  Route::post('/Transparencia/Nuevo/TipoGestiones', 'Transparencia\TiposGestionesController@store')->name('transparencia_nuevo_tipo');
  Route::get('/Transparencia/Editar/TipoGestiones/{id}', 'Transparencia\TiposGestionesController@edit')->name('transparencia_editar_tipo');
  Route::post('/Transparencia/Update/TipoGestiones/{id}', 'Transparencia\TiposGestionesController@update')->name('transparencia_update_tipo');
  Route::get('/Transparencia/Borrar/{id}', 'Transparencia\TiposGestionesController@destroy')->name('transparencia_destroy_tipo');
  Route::get('/Transparencia/TipoGestionesAsignacion/{id}', 'Transparencia\TiposGestionesController@asignacion')->name('transparencia_tipo_usuario');
  Route::post('/Transparencia/TipoGestionesAsignacion/Guardar', 'Transparencia\TiposGestionesController@asignacionstore')->name('transparencia_tipo_usuario_store');
  Route::get('/Transparencia/Borrar/Usuario/{id}/{detalleTipoGestion}', 'Transparencia\TiposGestionesController@asignaciondestroy')->name('transparencia_destroy_usuario');

  //PERIODOS
  Route::resource('Transparencia/Periodos', 'Transparencia\PeriodosController');
  Route::get('/Transparencia/Periodos/Borrar/{id}', 'Transparencia\PeriodosController@destroy')->name('transparencia_destroy_periodo');
  Route::get('/Transparencia/Generar/Fechas', 'Transparencia\PeriodosController@vistaGenerarFechas')->name('transparencia_generar_fecha');
  Route::get('/Transparencia/Generar/FechasPeriodos', 'Transparencia\PeriodosController@generarFechas')->name('transparencia_generar_fecha_periodo');

  //PERIODOS FECHAS
  Route::get('/Transparencia/PeriodosFechas', 'Transparencia\PeriodosFechasController@index')->name('periodosfechas');
  Route::get('/Transparencia/Editar/PeriodosFechas/{id}', 'Transparencia\PeriodosFechasController@edit')->name('periodosfechas_editar');
  Route::post('/Transparencia/Update/PeriodosFechas/{id}', 'Transparencia\PeriodosFechasController@update')->name('periodosfechas_update');

  //DOCUMENTOS GESTIONES
  Route::get('/Transparencia/DocumentosGestiones', 'Transparencia\DocumentosGestionesController@index')->name('transparencia_documentos_gestiones');
  Route::get('/Transparencia/Crear/DocumentosGestiones', 'Transparencia\DocumentosGestionesController@SeleccionGestion')->name('transparencia_crear_documentos_gestiones');
  Route::get('/Transparencia/Nuevo/DocumentosGestiones', 'Transparencia\DocumentosGestionesController@create')->name('transparencia_nuevo_documentos_gestiones');
  Route::post('/Transparencia/Nuevo/DocumentosGestiones/Guardar', 'Transparencia\DocumentosGestionesController@store')->name('transparencia_nuevo_documentos_gestiones_guardar');
  Route::get('/Transparencia/Editar/DocumentosGestiones/{id}', 'Transparencia\DocumentosGestionesController@edit')->name('transparencia_editar_documentos_gestiones');
  Route::post('/Transparencia/Update/DocumentosGestiones/{id}', 'Transparencia\DocumentosGestionesController@update')->name('transparencia_update_documentos_gestiones');
  Route::get('/Transparencia/Borrar/DocumentosGestiones/{id}', 'Transparencia\DocumentosGestionesController@destroy')->name('transparencia_destroy_documentos_gestiones');

  //GENERAR ENLACE
  Route::get('/Transparencia/GenerarEnlace', 'Transparencia\GenerarEnlaceController@index')->name('transparencia_generar_enlace');
  Route::get('/Transparencia/Nuevo/GenerarEnlace', 'Transparencia\GenerarEnlaceController@create')->name('transparencia_crear_generar_enlace');
  Route::post('/Transparencia/Nuevo/GenerarEnlace/Guardar', 'Transparencia\GenerarEnlaceController@store')->name('transparencia_nuevo_generar_enlace');
  Route::get('/Transparencia/Editar/GenerarEnlace/{id}', 'Transparencia\GenerarEnlaceController@edit')->name('transparencia_editar_generar_enlace');
  Route::post('/Transparencia/Update/GenerarEnlace/{id}', 'Transparencia\GenerarEnlaceController@update')->name('transparencia_update_generar_enlace');
  Route::get('/Transparencia/Borrar/GenerarEnlace/{id}', 'Transparencia\GenerarEnlaceController@destroy')->name('transparencia_destroy_generar_enlace');
  Route::get('/Transparencia/Enlace/Archivo/{id}', 'Transparencia\GenerarEnlaceController@mostrarEnalace')->name('mostrar_enlace');

  //ITEMS
  Route::get('/Transparencia/Items', 'Transparencia\ItemsController@index')->name('transparencia_items');
  Route::get('/Transparencia/Crear/Items', 'Transparencia\ItemsController@create')->name('transparencia_crear_items');
  Route::post('/Transparencia/Nuevo/Items', 'Transparencia\ItemsController@store')->name('transparencia_nuevo_items');
  Route::get('/Transparencia/Editar/Items/{id}', 'Transparencia\ItemsController@edit')->name('transparencia_editar_items');
  Route::post('/Transparencia/Update/Items/{id}', 'Transparencia\ItemsController@update')->name('transparencia_update_items');  
  Route::get('/Transparencia/ItemsBorrar/{id}', 'Transparencia\ItemsController@destroy')->name('transparencia_destroy_items');
  Route::get('/Transparencia/ItemsAsignacion/{id}', 'Transparencia\ItemsController@asignacion')->name('transparencia_items_usuario');  
  Route::post('/Transparencia/ItemsAsignacion/Guardar/{id}', 'Transparencia\ItemsController@asignacionstore')->name('transparencia_items_usuario_store');

  //MATERIAS
  Route::get('/Transparencia/Materias', 'Transparencia\MateriasController@index')->name('transparencia_materias');
  Route::get('/Transparencia/Crear/Materias', 'Transparencia\MateriasController@create')->name('transparencia_crear_materias');
  Route::post('/Transparencia/Nuevo/Materias', 'Transparencia\MateriasController@store')->name('transparencia_nuevo_materias');
  Route::get('/Transparencia/Editar/Materias/{id}', 'Transparencia\MateriasController@edit')->name('transparencia_editar_materias');
  Route::post('/Transparencia/Update/Materias/{id}', 'Transparencia\MateriasController@update')->name('transparencia_update_materias');  
  Route::get('/Transparencia/MateriasBorrar/{id}', 'Transparencia\MateriasController@destroy')->name('transparencia_destroy_materias');
