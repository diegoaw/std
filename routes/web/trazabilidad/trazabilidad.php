<?php
Route::get('Trazabilidad', 'Trazabilidad\TrazabilidadController@index')->name('trazabilidad');
Route::post('/Trazabilidad/Filtros', 'Trazabilidad\TrazabilidadController@index')->name('trazabilidadFiltros');

Route::post('Trazabilidad/Reporte', 'Trazabilidad\TrazabilidadController@reporteTrazabilidad')->name('trazabilidadReporte');